(function (app) {

    app.directive('inputBoolean', [
        function inputBoolean() {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModel) {
                    ngModel.$parsers.push(function (value) {
                        return '' + value;
                    });
                    ngModel.$formatters.push(function (value) {
                        return undefined !== value && null !== value && '' !== value && !!JSON.parse(value);
                    });
                }
            }
        }
    ]);

})(window.EVA.app);