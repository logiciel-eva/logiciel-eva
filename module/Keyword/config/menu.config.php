<?php

$keyword = [
    'icon'     => 'keyword',
    'title'    => 'keyword_module_title',
    'right'    => 'keyword:e:group:r',
    'href'     => 'keyword',
    'priority' => 999,
];

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'keywords' => $keyword,
                ],
            ],
        ],
        'client-menu-parc' => [
            'administration' => [
                'children' => [
                    'keywords' => array_replace($keyword, ['title' => 'keyword_module_title2']),
                ],
            ],
        ],
    ],
];
