<?php

namespace Core\Utils;

use Laminas\Http\Request;

class RequestUtils
{

    public static function isCommandRequest($request)
    {
        if(!($request instanceof Request)) {
            return true;
        }
        return null === $request->getUri()->getHost();
    }

}