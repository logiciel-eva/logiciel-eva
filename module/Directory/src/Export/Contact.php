<?php

namespace Directory\Export;

use Core\Export\IExporter;
use Field\Entity\Field;

abstract class Contact implements IExporter
{
    public static function getConfig()
    {
        $arr = [
            'vip'                => [
                'translation' => 'contact_field_vip',
                'format'      => function ($value, $row, $translator) {
                    return $value
                        ? $translator('yes')
                        : $translator('no');
                },
            ],
            'subscriptionStatus' => [
                'translation' => 'contact_field_subscriptionStatus',
                'format'      => function ($value, $row, $translator) {
                    return $translator('contact_subscription_status_' . $value);
                },
            ],
            'gender' => [
                'translation' => 'contact_field_gender',
                'format' => function ($value, $row, $translator) {
                    if ($value) {
                        return $translator('contact_gender_' . $value);
                    } else {
                        return '';
                    }
                }
            ],
           '/customField\d+$/' => [
                'format' => function ($value, $row, $translator) {
                    if ($value) {
                        return $translator($value);
                    } else {
                        return '';
                    }
                }
            ],
            'contactEmails'    => [
                'name'     => 'contactEmails',
                'property' => 'contactEmails.email',
                'format' => function ($value, $row) {
                    $str = '';
                    foreach ($row->getContactEmails() as $contactEmail) {
                        $str = $str . $contactEmail->getName() .' '. $contactEmail->getEmail() . ',';
                    }
                    return $str;    
                },
            ],
        ];

        return $arr;
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
