(function(app) {

    app.controller('roleListController', [
        'roleService', '$scope', 'flashMessenger', 'translator',
        function roleListController(roleService, $scope, flashMessenger, translator) {
            var self        = this;

            self.roles = [];
            self.options = {
                name: 'roles-list',
                col: [
                    'id',
                    'name',
                    'createdAt',
                    'updatedAt',
                    'rights'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    roleService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(role) {
                self.editedLineErrors = {};
                self.editedLine       = role;
                self.editedLineFields = angular.copy(role);
            };

            self.saveEditedLine = function saveEditedLine(role) {
                self.editedLineErrors = {};
                roleService.save(angular.extend({}, role, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        role = angular.extend(role, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('role_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteRole = function deleteRole(role) {
                if (confirm(translator('role_question_delete').replace('%1', role.name))) {
                    roleService.remove(role).then(function(res) {
                        if (res.success) {
                            self.roles.splice(self.roles.indexOf(role), 1);
                            flashMessenger.success(translator('role_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
