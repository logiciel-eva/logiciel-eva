<?php

$home = [
    'icon' => 'home',
    'module' => 'home',
    'title' => 'home_module_title',
    'right' => 'home:e:home:r',
    'href' => 'home/form',
    'priority' => 1002,
];

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'home' => $home,
                ],
            ],
        ],
        'client-menu-parc' => [
            'data' => [
                'icon' => 'database',
                'title' => 'menu_data_title',
                'priority' => 3,
            ],
            'suivi' => [
                'icon' => 'folder',
                'title' => 'menu_suivi_title',
                'priority' => 999999,
            ],
            'administration' => [
                'children' => [
                    'home' => $home,
                ],
            ],
        ],
    ],
];
