(function(app) {
    app.controller('organizationListController', [
        'organizationService',
        '$scope',
        'flashMessenger',
        'translator',
        function (organizationService, $scope, flashMessenger, translator) {
            var self = this;

            self.organizations = [];
            self.options = {
                name: 'organizations-list',
                col: [
                    'id',
                    'code',
                    'name',
                    'parent.id',
                    'parent.code',
                    'parent.name',
                    'createdAt',
                    'updatedAt',
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: false,
                getItems: function (params, callback, abort) {
                    organizationService.findAll(params, abort.promise).then(function(res) {
                        callback(res);
                    });
                },
            };

            self.fullTextSearch = function () {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function () {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if (event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function (organization) {
                if (angular.isArray(organization.keywords) || organization.keywords == null) {
                    organization.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine = organization;
                self.editedLineFields = angular.copy(organization);
            };

            self.saveEditedLine = function (organization) {
                self.editedLineErrors = {};
                organizationService
                    .save(angular.extend({}, organization, self.editedLineFields), {
                        col: self.options.col,
                    })
                    .then(
                        function(res) {
                            if (res.success) {
                                organization = angular.extend(organization, res.object);
                                self.editedLine = null;
                                self.editedLineFields = null;

                                flashMessenger.success(translator('organization_message_saved'));
                            }
                        },
                        function(err) {
                            for (var field in err.fields) {
                                self.editedLineErrors[field] = err.fields[field];
                            }
                        },
                    );
            };

            self.deleteOrganization = function (organization) {
                if (confirm(translator('organization_question_delete').replace('%1', organization.name))) {
                    organizationService.remove(organization).then(function(res) {
                        if (res.success) {
                            self.organizations.splice(self.organizations.indexOf(organization), 1);
                            flashMessenger.success(translator('organization_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        },
    ]);
})(window.EVA.app);
