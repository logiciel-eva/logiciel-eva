(function (app) {

    app.controller('ageSynchroController', [
        'ageSynchroService', '$scope', 'flashMessenger', 'translator',
        function ageSynchroController(ageSynchroService, $scope, flashMessenger, translator) {

            var self = this;

            self.projectId = null;
            self.synchro = null;
            self.synchroValidationErrors = [];


            self.init = function init() {
                self.projectId = $scope.$parent.$parent.$parent.self.project.id;
                ageSynchroService.getAgeSynchronisation(self.projectId).then(function(res) {
                    self.synchro = res.synchro;
                }, function(err) {
                    console.error(err);
                    flashMessenger.error(translator('error_occured'));
                });                
            }

            self.synchroniseProjectWithAge = function synchroniseProjectWithAge() {
                self.synchroValidationErrors = [];
                ageSynchroService.synchroniseWithAge(self.projectId, self.synchro).then(function (res) {
                    if(res.errors.length > 0) {
                        self.synchroValidationErrors = res.errors;
                        flashMessenger.warning(translator('error_occured'));
                    } else {
                        self.synchro = res.synchro;
                        flashMessenger.success(translator('age_synchro_state_synchronised'));
                    }                    
                }, function(err) {
                    console.error(err);
                    flashMessenger.error(translator('error_occured'));
                });
            };

            self.preventAgeSynchronisationWithAge = function preventAgeSynchronisationWithAge() {
                self.synchroValidationErrors = [];
                ageSynchroService.preventAgeSynchronisation(self.projectId).then(function (res) {
                    self.synchro = res;
                    flashMessenger.success(translator('age_synchro_state_prevent'));
                }, function(err) {
                    console.error(err);
                    flashMessenger.error(translator('error_occured'));
                });
            };

        }
    ]);
})(window.EVA.app);