(function(app) {

    app.factory('homeContainerService', [
        '$http', '$q', 'restFactoryService',
        function homeContainerService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/homeContainer'));
        }
    ])

})(window.EVA.app);
