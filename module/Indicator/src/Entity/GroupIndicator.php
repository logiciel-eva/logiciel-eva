<?php

namespace Indicator\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Indicator
 *
 * @package Indicator\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="groupindicator_group")
 */
class GroupIndicator extends BasicRestEntityAbstract
{
    const OPERATOR_SUM = 'sum';
    const OPERATOR_AVG = 'avg';
    const OPERATOR_MED = 'med';
    const OPERATOR_MAX = 'max';
    const OPERATOR_MIN = 'min';
    const OPERATOR_LAST = 'last';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $operator;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Indicator\Entity\Indicator", inversedBy="groupIndicators")
     * @ORM\JoinTable(
     *  name="groupindicator_indicators",
     *  joinColumns={
     *      @ORM\JoinColumn(name="groupindicator_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="indicator_id", referencedColumnName="id")
     *  }
     * )
     */
    protected $indicators;

    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Indicator\Entity\Measure", mappedBy="groupIndicator", cascade={"remove"})
     */
    protected $measures;


        /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
        $this->indicators = new ArrayCollection();
        $this->measures = new ArrayCollection();
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return ArrayCollection
     */
    public function getIndicators()
    {
        return $this->indicators;
    }

    /**
     * @param ArrayCollection $indicators
     */
    public function setIndicators($indicators)
    {
        $this->indicators = $indicators;
    }

    /**
     * @param ArrayCollection $indicators
     */
    public function addIndicators(ArrayCollection $indicators)
    {
        foreach ($indicators as $indicator) {
            $this->getIndicators()->add($indicator);
        }
    }

    /**
     * @param ArrayCollection $indicators
     */
    public function removeIndicators(ArrayCollection $indicators)
    {
        foreach ($indicators as $indicator) {
            $this->getIndicators()->removeElement($indicator);
        }
    }

        /**
     * @return ArrayCollection
     */
    public function getMeasures()
    {
        return $this->measures;
    }

    /**
     * @param ArrayCollection $indicators
     */
    public function setMeasures($measures)
    {
        $this->measures = $measures;
    }

    /**
     * @param ArrayCollection $measures
     */
    public function addMeasures(ArrayCollection $measures)
    {
        foreach ($measures as $measure) {
            $this->getMeasures()->add($measure);
        }
    }

    /**
     * @param ArrayCollection $measures
     */
    public function removeMeasures(ArrayCollection $measures)
    {
        foreach ($measures as $measure) {
            $this->getMeasures()->removeElement($measure);
        }
    }
    
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
			[
				'name'     => 'indicators',
				'required' => false
			],
            [
                'name'     => 'operator',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getOperators()
                        ],
                    ]
                ],
            ]
        ]);
        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getOperators()
    {
        return [
            self::OPERATOR_SUM,
            self::OPERATOR_AVG,
            self::OPERATOR_MED,
            self::OPERATOR_MAX,
            self::OPERATOR_MIN,
            self::OPERATOR_LAST
        ];
    }
}
