(function(app) {

    app.directive('contactEmailSelect', [
        'contactEmailService', 'selectFactoryService',
        function (contactEmailService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: contactEmailService
                },
                selectize: {
                    labelField: 'email',
                    valueField: 'id',
                    searchField: ['email', 'name']
                }
            });
        }
    ])

})(window.EVA.app);
