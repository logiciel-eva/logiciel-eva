(function(app) {

    app.factory('profileService', [
        'restFactoryService',
        function profileService(restFactoryService) {
            return restFactoryService.create('/api/profile');
        }
    ])

})(window.EVA.app);
