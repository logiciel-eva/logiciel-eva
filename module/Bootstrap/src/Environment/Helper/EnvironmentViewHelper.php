<?php

namespace Bootstrap\Environment\Helper;

use Bootstrap\Environment\EnvironmentInterface;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class EnvironmentViewHelper
 *
 * Access environment from view.
 *
 * @package Bootstrap\Environment\Helper
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class EnvironmentViewHelper extends AbstractHelper
{
    /**
     * @var EnvironmentInterface
     */
    protected $environment;

    /**
     * @param Environment EnvironmentInterface
     */
    public function __construct(EnvironmentInterface $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @return EnvironmentInterface
     */
    public function __invoke()
    {
        return $this->environment;
    }
}
