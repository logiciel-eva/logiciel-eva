<?php
namespace Time\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;
use Laminas\InputFilter\InputFilterInterface;

/**
 * Class Export
 * @package Time\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="time_export")
 */
class Export extends BasicRestEntityAbstract
{
    const ORIENTATION_PORTRAIT  = 'portrait';
    const ORIENTATION_LANDSCAPE = 'landscape';

    const TOTAL_UNIT_HOURS = 'hours';
    const TOTAL_UNIT_DAYS  = 'days';
    const TOTAL_UNIT_COST  = 'cost';

    const GROUP_BY_UNIT_YEARS  = 'years';
    const GROUP_BY_UNIT_MONTHS = 'months';
    const GROUP_BY_UNIT_WEEKS  = 'weeks';
    const GROUP_BY_UNIT_DAYS   = 'days';

    const SORT_BY_PROPERTY_NAME = 'name';
    const SORT_BY_PROPERTY_CODE = 'code';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $orientation;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $hoursByDay;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $cost;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    protected $totalUnit;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $groupByUnit;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", options={"default": "name"})
     */
    protected $sortByProperty;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $addMissingDays;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $completeMonths;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $allYear;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
    }

    /**
     * @return float
     */
    public function getHoursByDay()
    {
        return $this->hoursByDay;
    }

    /**
     * @param float $hoursByDay
     */
    public function setHoursByDay($hoursByDay)
    {
        $this->hoursByDay = $hoursByDay;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return array
     */
    public function getTotalUnit()
    {
        return $this->totalUnit;
    }

    /**
     * @param array $totalUnit
     */
    public function setTotalUnit($totalUnit)
    {
        $this->totalUnit = $totalUnit;
    }

    /**
     * @return string
     */
    public function getGroupByUnit()
    {
        return $this->groupByUnit;
    }

    /**
     * @param string $groupByUnit
     */
    public function setGroupByUnit($groupByUnit)
    {
        $this->groupByUnit = $groupByUnit;
    }

    /**
     * @return string
     */
    public function getSortByProperty()
    {
        return $this->sortByProperty;
    }

    /**
     * @param string $sortByProperty
     */
    public function setSortByProperty($sortByProperty)
    {
        $this->sortByProperty = $sortByProperty;
    }

    /**
     * @return bool
     */
    public function isAddMissingDays()
    {
        return $this->addMissingDays;
    }

    /**
     * @return bool
     */
    public function getAddMissingDays()
    {
        return $this->addMissingDays;
    }

    /**
     * @param bool $addMissingDays
     */
    public function setAddMissingDays($addMissingDays)
    {
        $this->addMissingDays = $addMissingDays;
    }

    /**
     * @return bool
     */
    public function isCompleteMonths()
    {
        return $this->completeMonths;
    }

    /**
     * @return bool
     */
    public function getCompleteMonths()
    {
        return $this->completeMonths;
    }

    /**
     * @param bool $completeMonths
     */
    public function setCompleteMonths($completeMonths)
    {
        $this->completeMonths = $completeMonths;
    }

    /**
     * @return bool
     */
    public function isAllYear()
    {
        return $this->allYear;
    }

    /**
     * @return bool
     */
    public function getAllYear()
    {
        return $this->allYear;
    }

    /**
     * @param bool $allYear
     */
    public function setAllYear($allYear)
    {
        $this->allYear = $allYear;
    }

    /**
     * @param EntityManager $entityManager
     * @return InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'orientation',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => self::getOrientations(),
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'hoursByDay',
                'required'   => true,
                'filters'    => [
                    ['name' => 'NumberParse'],
                ],
                'validators' => [
                    [
                        'name'    => 'Between',
                        'options' => [
                            'min' => 0,
                            'max' => 24,
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'cost',
                'required'   => true,
                'filters'    => [
                    ['name' => 'NumberParse'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return $value > 0;
                            },
                            'message'  => 'time_export_field_cost_error_non_positive',
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'totalUnit',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                if (is_array($value)) {
                                    foreach ($value as $totalUnit) {
                                        if (!in_array($totalUnit, self::getTotalUnits(), true)) {
                                            return false;
                                        }
                                    }
                                    return true;
                                } else {
                                    if (!in_array($value, self::getTotalUnits(), true)) {
                                        return false;
                                    }
                                    return true;
                                }
                            },
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'groupByUnit',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => self::getGroupByUnits(),
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'sortByProperty',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => self::getSortByProperties(),
                        ],
                    ],
                ],
            ],
            [
                'name'        => 'addMissingDays',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'        => 'completeMonths',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'        => 'allYear',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getOrientations()
    {
        return [
            self::ORIENTATION_PORTRAIT,
            self::ORIENTATION_LANDSCAPE,
        ];
    }

    /**
     * @return array
     */
    public static function getTotalUnits()
    {
        return [
            self::TOTAL_UNIT_HOURS,
            self::TOTAL_UNIT_DAYS,
            self::TOTAL_UNIT_COST,
        ];
    }

    /**
     * @return array
     */
    public static function getGroupByUnits()
    {
        return [
            self::GROUP_BY_UNIT_YEARS,
            self::GROUP_BY_UNIT_MONTHS,
            self::GROUP_BY_UNIT_WEEKS,
            self::GROUP_BY_UNIT_DAYS,
        ];
    }

    /**
     * @return array
     */
    public static function getSortByProperties()
    {
        return [
            self::SORT_BY_PROPERTY_NAME,
            self::SORT_BY_PROPERTY_CODE,
        ];
    }
}