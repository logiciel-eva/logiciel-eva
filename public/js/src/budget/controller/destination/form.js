(function(app) {
    app.controller('destinationFormController', [
        'destinationService',
        'flashMessenger',
        'translator',
        function (destinationService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: ['id', 'code', 'name', 'parent.id', 'parent.code', 'parent.name'],
            };

            self.loading = {
                save: false,
            };

            self.errors = {
                fields: {},
            };

            self.destination = {
                _rights: {
                    update: true,
                },
            };

            self.init = function (id) {
                if (id) {
                    destinationService.find(id, apiParams).then(function(res) {
                        self.destination = res.object;
                        self.panelTitle = self.destination.name;
                    });
                }
            };

            self.saveDestination = function () {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.destination.id;

                destinationService.save(self.destination, apiParams).then(
                    function(res) {
                        self.destination = res.object;
                        self.panelTitle = self.destination.name;
                        self.loading.save = false;

                        if (isCreation) {
                            var url = window.location.pathname + '/' + self.destination.id;
                            history.replaceState('', '', url);
                        }

                        flashMessenger.success(translator('destination_message_saved'));
                    },
                    function(err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }

                        self.loading.save = false;
                    },
                );
            };

            self.deleteDestination = function () {
                if (confirm(translator('destination_question_delete').replace('%1', self.destination.name))) {
                    destinationService.remove(self.destination).then(function(res) {
                        if (res.success) {
                            window.location = '/budget/destination';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        },
    ]);
})(window.EVA.app);
