<?php

namespace Map\Entity\Data;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Definition
 * @package Map\Entity\Data
 *
 * @ORM\Entity
 * @ORM\Table(name="map_data_definition")
 */
class Definition extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
        ]);

        return $inputFilter;
    }
}
