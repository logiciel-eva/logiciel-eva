(function(app) {

    app.controller('territoryImportController', [
        'leafletData', 'FileUploader', 'toGeoJson', 'territoryService', 'flashMessenger', 'translator',
        function territoryImportController(leafletData, FileUploader, toGeoJson, territoryService, flashMessenger, translator) {
            var self = this;

            self.layer = null;
            self.properties = [];
            self.map = {
                code: null,
                name: null
            };

            self.uploader = new FileUploader();
            self.uploader.filters.push({
                name: 'geoFilter',
                fn: function(item) {
                    var type = self.getFileExtension(item.name);
                    return '|kml|csv|geojson|json|gpx|wkt|'.indexOf(type) !== -1;
                }
            });

            self.uploader.onAfterAddingFile = function(fileItem) {
                self.loading = true;
                self.error   = false;

                self.properties = [];
                self.map.code = null;
                self.map.name = null;

                toGeoJson.fromFile(fileItem._file).then(function(res) {
                    leafletData.getMap('map').then(function(map) {
                        if (self.layer !== null) {
                            map.removeLayer(self.layer);
                        }
                        self.layer = L.geoJson(res, {
                            fillColor: '#ff2424',
                            color: '#ff2424',
                            weight: 2,
                            fillOpacity: 0.5,
                            onEachFeature: function (feature, layer) {
                                if (typeof feature.properties !== 'undefined') {
                                    var properties = Object.keys(feature.properties);
                                    for (var i in properties) {
                                        if (self.properties.indexOf(properties[i]) == -1) {
                                            self.properties.push(properties[i]);
                                        }
                                    }
                                }
                            }
                        }).addTo(map);

                        if (self.properties.length < 2) {
                            self.properties.push('data_0');
                            self.map.code = 'data_0';

                            self.properties.push('data_1');
                            self.map.name = 'data_1';
                        }

                        map.fitBounds(self.layer.getBounds());

                        self.loading = false;
                    });
                });
            };
            self.uploader.onWhenAddingFileFailed = function() {
                self.error   = true;
                self.loading = false;
            };

            self.getFileExtension = function getFileExtension(filename) {
                return filename.split('.').pop();
            };

            self.layerFocused = null;
            self.focusOn = function (layer) {
                if (self.layerFocused !== null) {
                    self.layerFocused.setStyle({
                        fillColor: '#ff2424'
                    });
                }

                leafletData.getMap('map').then(function(map) {
                    if (self.layerFocused !== layer) {
                        self.layerFocused = layer;

                        layer.setStyle({
                            fillColor: '#00FF00'
                        });
                        map.fitBounds(layer.getBounds());
                    } else {
                        self.layerFocused = null;
                        map.fitBounds(self.layer.getBounds());
                    }
                });
            };

            self.deleteLayer = function (layer) {
                self.layer.removeLayer(layer);
            };

            self.save = function () {
                self.loadingSave       = true;
                self.errorNameRequired = false;

                if (self.map.name !== null) {
                    var territories = [];
                    var layers = self.layer.getLayers();
                    for (var i in layers) {
                        var layer = layers[i];

                        var name = layer.feature.properties[self.map.name];
                        var code = layer.feature.properties[self.map.code];

                        if (!name) {
                            self.errorNameRequired = true;
                        }

                        var territory = {
                            name: name,
                            code: code,
                            json: layer.toGeoJSON()
                        };

                        territories.push(territory);
                    }
                } else {
                    self.errorNameRequired = true;
                }

                if (!self.errorNameRequired) {
                    territoryService.saveAll(territories).then(function (res) {
                        self.saveError = false;
                        for (var i in layers) {
                            if (res[i].success === true) {
                                self.deleteLayer(layers[i]);
                            } else {
                                self.saveError = true;
                            }
                        }

                        if (!self.saveError) {
                            flashMessenger.success(translator('territories_message_saved'));
                        } else {
                            flashMessenger.error(translator('territory_error_on_some_territories'));
                        }

                        self.loadingSave = false;
                    });
                }
            };
        }
    ]);

})(window.EVA.app);
