<?php

return [
    'icons' => [
        'budget' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-eur',
        ],
        'account' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-tags',
        ],
        'budgetStatus' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-tags',
        ],
        'account-parc' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-credit-card',
        ],
        'budgetStatus-parc' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-flag',
        ],
        'envelope' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-envelope-o',
        ],
        'nature' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-leaf',
        ],
        'organization' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-building-o',
        ],
        'managementUnit' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-building-o',
        ],
        'destination' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-exchange',
        ],
        'database' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-database'
        ],
    ]
];
