(function(app) {

    app.factory('accountService', [
        'restFactoryService', '$http', '$q',
        function (restFactoryService, $http, $q) {
            var url = '/api/budget/account';

            var methods = restFactoryService.create(url);

            methods.fusion = function (keep_id, ids) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: url + '/' + keep_id + '/fusion',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ ids : ids })
                }).then(function(res) {
                    deferred.resolve(res.data)
                }, function(err) {
                    deferred.reject(err)
                });

                return deferred.promise;
            };

            return methods;
        }
    ])

})(window.EVA.app);
