<?php

namespace Alert;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'alert' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/alert',
                    'defaults' => [
                        'controller' => 'Alert\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'Alert\Controller\Index',
                                'action'     => 'form',
                            ],
                        ],
                    ],
                    'formcampain' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/form-campain[/:id]',
                            'defaults' => [
                                'controller' => 'Alert\Controller\Index',
                                'action'     => 'formCampain',
                            ],
                        ],
                    ],
                    'send' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/send[/:id]',
                            'defaults' => [
                                'controller' => 'Alert\Controller\Index',
                                'action'     => 'send',
                            ],
                        ],
                    ],
                ],
            ],
            'api'   => [
                'child_routes' => [
                    'alert' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/alert[/:id]',
                            'defaults' => [
                                'controller' => 'Alert\Controller\API\Alert',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class     => ServiceLocatorFactory::class,
            Controller\API\AlertController::class => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Alert\Controller\Index'     => Controller\IndexController::class,
            'Alert\Controller\API\Alert' => Controller\API\AlertController::class,
        ]
    ],
];
