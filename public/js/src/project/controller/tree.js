(function (app) {
	app.controller('projectTreeController', [
		'$scope',
		'$q',
		'projectService',
		'keywordService',
		'queryService',
		'userConfigurationService',
		'$timeout',
		'$window',
		'moment',
		function projectTreeController(
			$scope,
			$q,
			projectService,
			keywordService,
			queryService,
			userConfigurationService,
			$timeout,
			$window,
			moment,
		) {
			var self = this;

			self.view = 'tree';

			self.isExpanded = false;
			self.reference = 'initial';
			self.tree = [];
			self.gantt = [];
			self.ganttOptions = {
				scale: 'quarter',
				type: 'planned',
			};
			self.loading = {
				tree: false,
				export: false,
			};

			self.exportModel = null;
			self.customExport = {};
			self.nodes = [];
			self.nodeIds = [];
			self.haveNodeIds = false;

			//function letsWatch() {
			$scope.$watch(
				function () {
					return self.reference;
				},
				function () {
					if (!self.fromQuery) {
						self.tree = [];
						self.isExpanded = false;

						self.initFilter();

						if (self.reference !== 'initial') {
							//self.loadKeywords(self.reference, null, null);
							var maxLevel = angular
								.element('#treeSelect option[ng-value="' + self.reference + '"]')
								.data('level');
							self.levels = [];
							for (var i = 0; i <= maxLevel; i++) {
								self.levels.push({
									value: i,
									name: 'Niveau ' + i,
									filters: {},
									mode: 'parent',
								});
							}
							self.filters.max = maxLevel;
						} else {
							self.levels = self.initialLevels;
						}

						self.computeLevels();
						self.filters.tree = self.reference;
						if (self.reference === 'initial' && !self.fromProject) {
							self.filters.min = 0;
							self.filters.max = self.levels.length - 1;
							self.computeLevels();

							self.filters.reference = 0;
						}
					}
				},
			);
			//}

			self.loadKeywords = function loadKeywords(group, parent, node) {
				if (!parent || !node.loaded) {
					if (parent) {
						node.loading = true;
					}

					keywordService
						.findAll({
							col: ['id', 'name'],
							search: {
								type: 'list',
								data: {
									filters: {
										group: {
											op: 'eq',
											val: group,
										},
										'parents.id': {
											op: parent ? 'eq' : 'isnull',
											val: parent ? [parent] : '',
										},
									},
									sort: 'name',
									order: 'asc',
								},
							},
						})
						.then(function (res) {
							for (var i in res.rows) {
								var childNode = {
									element: res.rows[i],
									type: 'keyword',
									loaded: false,
									children: [],
								};

								if (parent) {
									node.children.push(childNode);
								} else {
									self.tree.push(childNode);
								}

								if (self.isExpanded) {
									childNode.open = true;
									self.loadKeywords(group, childNode.element.id, childNode);
								}
							}

							if (parent) {
								node.loaded = true;
								node.loading = false;

								self.loadProjects(node.element.id, null, node);
							} else {
								self.loading.tree = false;
							}
						});
				}
			};

			self.isCustomExport = function () {
				return /[a-zA-Z]+/.test(self.exportModel) && self.exportModel !== 'default';
			};

			self.columns = {
				publicationStatus: {
					name: 'publicationStatus',
					visible: false,
				},
				advancement: {
					name: 'advancement',
					visible: false,
				},
				code: {
					name: 'code',
					visible: true,
				},
			};

			self.getTableConfiguration = function () {
				userConfigurationService.getTable('projects-tree').then(function (res) {
					for (var i in res) {
						self.columns[i].visible = res[i] == 'true';
					}
				});
			};

			self.saveTableConfiguration = function () {
				var cols = {};
				for (var i in self.columns) {
					var column = self.columns[i];
					cols[column.name] = column.visible;
				}

				userConfigurationService.saveTable('projects-tree', cols).then(function () { });
			};

			self.loadProjects = function loadProjects(keyword, parent, node) {
				if (!parent || node == null || !node.loaded) {
					if ((parent || keyword) && node) {
						node.loading = true;
					}

					var params = {
						col: [
							'id',
							'name',
							'code',
							'parent.id',
							'level',
							'stackedAdvancement',
							'publicationStatus',
							'measuresT',
						],
						search: {
							type: 'list',
							data: {
								filters: {},
								sort: self.filters.sort,
								order: 'asc',
							},
						},
					};

					if (keyword) {
						params.search.data.filters['keyword.' + self.reference] = {
							op: 'eq',
							val: [keyword],
						};
					} else {
						params.search.data.filters.parent = {
							op: parent ? 'eq' : 'isnull',
							val: parent ? parent : '',
						};
					}

					projectService.findAll(params).then(function (res) {
						for (var i in res.rows) {
							var childNode = {
								element: res.rows[i],
								type: 'project',
								loaded: false,
								children: [],
							};

							if ((parent || keyword) && node) {
								node.children.push(childNode);
							} else {
								self.tree.push(childNode);
							}

							if (self.isExpanded && self.reference === 'initial') {
								childNode.open = true;
								self.loadProjects(null, childNode.element.id, childNode);
							}
						}

						if ((parent || keyword) && node) {
							node.loaded = true;
							node.loading = false;
						}

						self.loading.tree = false;
					});
				}
			};

			/**
			 * Expand an node and his children recursively.
			 * @param node
			 * @param state
			 */
			self.expand = function expand(node, state) {
				if (typeof node !== 'undefined' && typeof node.open !== 'undefined') {
					if (typeof state !== 'boolean') {
						state = false;
					}

					node.open = state;
					if (typeof node.children !== 'undefined') {
						for (var i = 0; i < node.children.length; ++i) {
							self.expand(node.children[i], state);
						}
					}
				}
			};

			/**
			 * Expand all nodes or close all.
			 */
			self.expandAll = function expandAll() {
				self.isExpanded = !self.isExpanded;

				if (self.isExpanded) {
					for (var i = 0; i < self.tree.length; ++i) {
						self.tree[i].open = true;
						if (self.tree[i].children.length > 0) {
							self.expand(self.tree[i], true);
						} else {
							if (self.reference !== 'initial') {
								self.loadKeywords(self.reference, self.tree[i].element.id, self.tree[i]);
							} else {
								self.loadProjects(null, self.tree[i].element.id, self.tree[i]);
							}
						}
					}
				} else {
					for (var i = 0; i < self.tree.length; ++i) {
						self.expand(self.tree[i], false);
					}
				}
			};

			/**** QUERIES ****/
			self.queries = {};
			self.query = {};
			self.errors = {
				fields: {},
			};

			self.initQueries = function () {
				self.loadingQueries = true;

				queryService
					.findAll({
						col: ['id', 'name', 'shared','filters', 'list'],
						search: {
							data: {
								filters: {
									list: {
										op: 'eq',
										val: 'node-tree',
									},
								},
							},
						},
					})
					.then(
						function (res) {
							self.queries = res.rows;
							self.loadingQueries = false;
						},
						function () {
							self.loadingQueries = false;
						},
					);
			};

			self.loadQuery = function (query) {
				self.fromQuery = true;
				self.reference = angular.copy(query.filters.tree);
				self.filters = angular.copy(query.filters);

				self.filter();

				$timeout(function () {
					self.fromQuery = false;
					$scope.$broadcast('query-loaded');
				});
			};

			self.saveQuery = function () {
				self.loadingSaveQuery = true;

				self.query.filters = self.filters;
				self.query.list = 'node-tree';

				queryService
					.save(self.query, {
						col: ['id', 'name', 'shared','filters', 'list'],
					})
					.then(
						function (res) {
							self.queries.push(res.object);

							self.query = {};
							self.loadingSaveQuery = false;
						},
						function (err) {
							for (var field in err.fields) {
								self.errors.fields[field] = err.fields[field];
							}
							self.loadingSaveQuery = false;
						},
					);
			};

			self.deleteQuery = function (query) {
				if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
					self.loadingSaveQuery = true;
					queryService.remove(query).then(
						function () {
							self.queries.splice(self.queries.indexOf(query), 1);
							self.loadingSaveQuery = false;
						},
						function () {
							self.loadingSaveQuery = false;
						},
					);
				}
			};

			/**** FILTERS ****/
			self.levels = [];
			self.initialLevels = [];
			self.initLevels = function (levels) {
				for (var i = 0; i < levels.length; i++) {
					self.initialLevels.push({
						value: i,
						name: levels[i].name,
						filters: {},
						mode: 'parent',
					});
				}
			};

			self.initFilter = function () {
				self.filters = {
					min: 0,
					max: 0,
					reference: 0,
					levels: [],
					sort: 'code',
				};
			};

			self.computeLevels = function () {
				if (!self.fromQuery) {
					if (self.filters.max < self.filters.min) {
						self.filters.max = self.filters.min;
					}

					self.filters.levels = [];
					for (var i = 0; i < self.levels.length; i++) {
						if (
							self.levels[i].value >= self.filters.min &&
							self.levels[i].value <= self.filters.max
						) {
							self.filters.levels.push(self.levels[i]);
						}
					}

					self.filters.reference = self.filters.min;
				}
			};
			$scope.$watch(function () {
				return self.filters.min;
			}, self.computeLevels);
			$scope.$watch(function () {
				return self.filters.max;
			}, self.computeLevels);

			self.filter = function () {
				self.loading.tree = true;
				projectService.getTree({ filters: self.filters }).then(function (res) {
					self.haveNodeIds = false;
					self.nodeIds = [];
					self.nodes = res;
					self.tree = self.filtersResultsToTree(res);
					self.gantt = self.filtersResultsToGantt(res);

					if (self.fromProject) {
						self.nodeIds = self.nodeIds.filter(nodeId => nodeId !== self.project_id);
						self.haveNodeIds = true;
					}

					self.loading.tree = false;

					if (self.fromProject) {
						$timeout(function () {
							$('.node-bold').removeClass('node-bold');
							$('#node-' + $scope.$parent.$parent.self.project.id).addClass('node-bold');
						});
					}
				});
			};

			self.filtersResultsToTree = function (nodes) {
				var tree = [];
				for (var i in nodes) {
					self.nodeIds.push(nodes[i].id);
					var childNode = {
						element: nodes[i],
						type: typeof nodes[i].__type !== 'undefined' ? nodes[i].__type : 'project',
						loaded: true,
						open: true,
						children: self.filtersResultsToTree(nodes[i].children),
					};
					tree.push(childNode);
				}

				return tree;
			};

			self.filtersResultsToGantt = function (nodes) {
				let gantt = [];

				for (let i in nodes) {
					let row = {
						id: nodes[i].id,
						name: nodes[i].name,
						code: nodes[i].code,
						children: [],
						tasks: [
							{
								id: nodes[i].id,
								name: nodes[i].name,
								code: nodes[i].code,
								dependencies: [],
							},
						],
					};

					if (
						nodes[i][self.ganttOptions.type + 'DateStart'] &&
						moment(nodes[i][self.ganttOptions.type + 'DateStart'], 'DD/MM/YYYY HH:mm').isValid()
					) {
						row.tasks[0].from = moment(
							nodes[i][self.ganttOptions.type + 'DateStart'],
							'DD/MM/YYYY HH:mm',
						);
					}

					if (
						nodes[i][self.ganttOptions.type + 'DateEnd'] &&
						moment(nodes[i][self.ganttOptions.type + 'DateEnd'], 'DD/MM/YYYY HH:mm').isValid()
					) {
						row.tasks[0].to = moment(
							nodes[i][self.ganttOptions.type + 'DateEnd'],
							'DD/MM/YYYY HH:mm',
						);
					}

					gantt.push(row);

					if (typeof nodes[i].children !== 'undefined') {
						for (let j = 0; j < nodes[i].children.length; j++) {
							row.children.push(nodes[i].children[j].id);
							row.tasks[0].dependencies.push({
								to: nodes[i].children[j].id,
							});
						}

						let children = self.filtersResultsToGantt(nodes[i].children);
						for (let k = 0; k < children.length; k++) {
							gantt.push(children[k]);
						}
					}
				}

				return gantt;
			};

			self.showExportModal = function (type) {
				self.exportModel = null;
				self.exportCols = null;
				angular.element('#export' + type + 'ModalTree').modal('show');
			};

			self.export = function (type) {
				if (self.filters.levels.length === 0) {
					self.computeLevels();
				}

				$timeout(function () {
					angular.element('#export' + type + 'FormTree').submit();
					self.exportModel = null;
					angular.element('#export' + type + 'ModalTree').modal('hide');
				});
			};

			$scope.$on('export-xls-tree', function (e, cols, type) {
				self.exportModel = 'xls';
				self.exportCols = cols;
				self.export(type);
			});

			self.loadArboProject = function () {
				$timeout(function () {
					if ($scope.$parent.$parent.self.project.id) {
						self.filters.min = 0;
						self.filters.max = self.levels.length - 1;
						self.computeLevels();

						self.filters.reference = $scope.$parent.$parent.self.project.level;
						let filters = {};
						if ($scope.$parent.$parent.self.project.parent) {
							filters = {
								parent: {
									op: 'eq',
									val: $scope.$parent.$parent.self.project.parent,
								},
							};
						} else {
							filters = {
								id: {
									op: 'eq',
									val: $scope.$parent.$parent.self.project.id,
								},
							};
						}
						self.filters.levels[self.filters.reference].filters = filters;

						self.filter();
					}
				});
			};

			self.init = function (fromProject, project_id) {
				self.fromProject = fromProject;
				self.project_id = project_id;

				$timeout(function () {
					self.getTableConfiguration();
				});
				if (self.fromProject) {
					self.ganttOptions.scale = 'month';

					$scope.$watch(function () {
						return $scope.$parent.$parent.self.project.parent;
					}, self.loadArboProject);
				}

				//letsWatch();
			};

			self.changeGanttType = function () {
				self.gantt = self.filtersResultsToGantt(self.nodes);
			};

			self.ganttFullScreen = function () {
				$('.gantt-container').addClass('fullscreen');
			};

			self.ganttLeaveFullScreen = function () {
				$('.gantt-container').removeClass('fullscreen');
			};
		},
	]);

	app.controller('treeLevelController', [
		'$scope',
		function treeLevelController($scope) {
			var self = this;
			$scope.__filters = {};
			$scope.$on('query-loaded', function () {
				$scope.__filters = {};

				for (var i in self.level.filters) {
					if (
						self.level.filters[i] !== null &&
						(typeof self.level.filters[i].val !== 'undefined' ||
							self.level.filters[i].op == 'isNull')
					) {
						$scope.__filters[i] = true;
					} else {
						$scope.__filters[i] = false;
					}
				}
			});
		},
	]);
})(window.EVA.app);
