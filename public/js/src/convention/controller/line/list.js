(function(app) {

    app.controller('conventionLineListController', [
        'conventionLineService', 'flashMessenger', 'translator', '$scope', '$filter',
        function conventionLineListController(conventionLineService, flashMessenger, translator, $scope, $filter) {
            var self = this;

            self.from    = {
                type: null,
                id: null
            };
            self.lines = [];

            var apiParams = {
                col: [
                    'id',
                    'convention.id',
                    'convention.name',
                    'percentage',
                    'project.id',
                    'project.amountIncomeArboRequested',
                    'project.financerRepartitionIncomeArboRequested',
                    'project.timeSpent',
                    'project.amountIncomeAutofinancementArboRequested',
                    'project.financers.id',
                    'project.financers.name',
                ],
                search: {
                    type: 'list',
                    data: {
                        filters: {},
                        sort: 'convention.name',
                        order: 'asc'
                    }
                }
            };

            self.expenseTypes = [];
            self.init = function init(from, id, expenseTypes) {
                self.from.type    = from;
                self.from.id      = id;
                self.expenseTypes = expenseTypes;

                for (var i in expenseTypes) {
                    apiParams.col.push('project.amountExpenseArbo' + expenseTypes[i].ucfirst())
                }

                if (self.from.id && self.from.type == 'project') {
                    apiParams.search.data.filters.project = {
                        op: 'eq',
                        val: self.from.id
                    };
                    conventionLineService.findAll(apiParams).then(function (res) {
                        self.lines = res.rows;
                        if (res.total > 0) {
                            self.financers = res.rows[0].project.financers;
                        }
                    });
                }
            };

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(line) {
                if (angular.isArray(line.keywords) || line.keywords == null) {
                    line.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = line;
                self.editedLineFields = angular.copy(line);
            };

            self.saveEditedLine = function saveEditedLine(line) {
                self.editedLineErrors = {};
                conventionLineService.save(angular.extend({}, line, self.editedLineFields), {
                    col: apiParams.col
                }).then(function (res) {
                    if (res.success) {
                        line = angular.extend(line, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('convention_line_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteLine = function deleteLine(line) {
                if (confirm(translator('convention_line_question_delete').replace('%1', line.convention.name))) {
                    conventionLineService.remove(line).then(function (res) {
                        if (res.success) {
                            self.lines.splice(self.lines.indexOf(line), 1);
                            flashMessenger.success(translator('convention_line_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.getAmountFinancerProject = function getAmountFinancerProject(percentage, total, part) {
                var lineAmount = (total * percentage) / 100;
                return (lineAmount * part) / 100;
            };

            self.getAmountFinancerTotal = function getAmountFinancerTotal(financer) {
                var amount = 0;
                for (var i in self.lines) {
                    var line = self.lines[i];
                    if (typeof line.project.financerRepartitionIncomeArboRequested[financer.id] !== 'undefined') {
                        amount += self.getAmountFinancerProject(line.percentage, line.project.amountIncomeArboRequested, line.project.financerRepartitionIncomeArboRequested[financer.id].percentage);
                    }
                }
                return amount;
            };

            self.getPartTotalAmountIncomeRequested = function () {
                var amount = 0;
                for (var i in self.lines) {
                    amount += (self.lines[i].percentage * self.lines[i].project.amountIncomeArboRequested) / 100;
                }
                return amount;
            };

            self.getTotalTimeSpent = function getAmountFinancerTotal() {
                var amount = 0;
                for (var i in self.lines) {
                    var line = self.lines[i];
                    amount += (line.project.timeSpent * line.percentage) / 100;
                }
                return amount;
            };

            self.getTotalAmountExpense = function getTotalAmountExpense(type) {
                var amount = 0;
                for (var i in self.lines) {
                    var line = self.lines[i];
                    amount += (line.project['amountExpenseArbo' + type.ucfirst()] * line.percentage) / 100;
                }
                return amount;
            };

            self.getTotalAmountIncomeAutofinancementRequested = function getTotalAmountIncomeAutofinancementRequested() {
                var amount = 0;
                for (var i in self.lines) {
                    var line = self.lines[i];
                    amount += (line.project.amountIncomeAutofinancementArboRequested * line.percentage) / 100;
                }
                return amount;
            };

            self.isManager = function isManager(user) {
                for (var i in $scope.$parent.$parent.self.project.members) {
                    var member = $scope.$parent.$parent.self.project.members[i];
                    if (member.id && member.user.id == user && member.role == 'manager') {
                        return true;
                    }
                }

                return false;
            };
        }
    ]);

})(window.EVA.app);
