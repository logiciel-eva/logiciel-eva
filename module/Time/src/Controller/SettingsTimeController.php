<?php

namespace Time\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

/**
 * Class SettingsTimeController
 * @package Time\Controller
 */
class SettingsTimeController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('time:e:settings-time:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }
}