<?php

namespace Keyword;

return [
    'doctrine' => [
        'driver' => [
            'keyword_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Keyword\Entity' => 'keyword_entities'
                ]
            ],
            'keyword_entities2' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_default' => [
                'drivers' => [
                    'Keyword\Entity' => 'keyword_entities2'
                ]
            ]
        ],
    ]
];
