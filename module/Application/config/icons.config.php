<?php

return [
    'icons' => [
        'home' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-home'
        ],
        'logout' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-power-off'
        ],
        'refresh' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-refresh'
        ],
        'edit' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-edit'
        ],
        'save' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-save'
        ],
        'delete' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-trash'
        ],
        'duplicate' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-files-o',
        ],
        'archive' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-archive',
        ],
        'unarchive' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-undo',
        ],
        'module' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-archive'
        ],
        'translation' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-universal-access'
        ],
        'list' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-list'
        ],
        'tree' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-sitemap'
        ],
        'create' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-plus'
        ],
        'configuration' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-cogs'
        ],
        'configuration2' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-toggle-on'
        ],
        'filters' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-filter'
        ],
        'search' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-search'
        ],
        'columns' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-columns'
        ],
        'loading' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-circle-o-notch fa-spin'
        ],
        'query' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-shopping-basket'
        ],
        'inline-edit' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-pencil inline-edit-button'
        ],
        'close' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-close'
        ],
        'timeline' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-code-fork'
        ],
        'export' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-download',
        ],
        'excel' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-file-excel-o',
        ],
        'plus' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-plus',
        ],
        'minus' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-minus',
        ],
        'eraser' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-eraser',
        ],
        'command' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-terminal',
        ],
        'show' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-eye'
        ],
        'retweet' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-retweet'
        ],
        'fusion' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-compress'
        ],
        'select-all' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-check-square-o',
        ],
        'check' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-check',
        ],
    ]
];
