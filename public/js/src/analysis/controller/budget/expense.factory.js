(app => {
	app.expenseControllerFactory = (name, exportModalId, isSubvention) => {
		return function (queryService, $rootScope, $scope, translator, $http, flashMessenger) {
			const self = this;

			self.projects = [];

			self.filters = {
				project: {},
				posteExpense: {},
				expense: {},
			};

			self.exportModalIdentifier = exportModalId;

			self.ht = false;
			self.arbo = false;
			self.monthCumulated = true;
			self.exportModel = 'default';

			self.nbFilters = 2;

			self.loading = false;

			self.timesArr = [];

			self.options = {
				name: name,
				autoload: false,
				getItems: () => { },
			};

			self.init = ht => {
				self.ht = ht;
				self.initQueries();
			};

			self.applyFilters = () => {
				self.loading = true;
				self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.posteExpense).length + Object.keys(self.filters.expense).length
				$http({
					url:
						'/analysis/budget/expense?' +
						$.param({ filters: self.filters, arbo: self.arbo, isSubvention }),
					method: 'GET',
				})
					.then(res => {
						self.projects = res.data;
						self.getChartData();
						self.loading = false;
					})
					.catch(() => {
						self.loading = false;
						flashMessenger.error(translator('error_occured'));
					});
			};

			self.getTotal = (type, projects) => {
				let total = 0;
				if (type === 'amountPosteExpenseArboAeRateBalance') {
					total = self.getTotal('amountExpenseArboAe', projects) > 0 ? self.getTotal('amountExpenseArboAeCommitted', projects) * 100 / self.getTotal('amountExpenseArboAe', projects) : 0;
				} else if (type === 'amountPosteExpenseArboCpCommittedAeCommittedRateBalance') {
					var aeCommitted = self.getTotal('amountExpenseArboAeCommitted', projects);
					total = aeCommitted > 0 ? self.getTotal('amountExpenseArboCpCommitted', projects) / aeCommitted * 100 : 0;
				} else if (type === 'amountHTPosteExpenseArboCpCommittedAeCommittedRateBalance') {
					var aeCommitted = self.getTotal('amountHTExpenseArboAeCommitted', projects);
					total = aeCommitted > 0 ? self.getTotal('amountHTExpenseArboCpCommitted', projects) / aeCommitted * 100 : 0;
				} else {
					for (const project of projects) {
						if (project['_level'] === 0) {
							total += project[type] || 0;
						}
					}
				}

				return total;
			};

			self.resetFilters = () => {
				self.filters.project = {};
				self.filters.posteExpense = {};
				self.filters.expense = {};

				self.applyFilters();
			};

			self.showExportModal = () => {
				angular.element('#' + exportModalId).modal('show');
			};

			self.export = () => {
				let columns = [];

				for (const key of Object.keys($scope.__tb.columns)) {
					if ($scope.__tb.columns[key].visible && !['selection', 'actions', 'tree'].includes(key)) {
						columns = [...columns, key];
					}
				}

				window.open(
					'/analysis/budget/expense?' +
					$.param({
						filters: self.filters,
						arbo: self.arbo,
						isSubvention,
						export: self.exportModel,
						columns,
					}),
				);

				angular.element('#exportModalExpense').modal('hide');
			};

			/*** CHART ***/
			self.getChartData = function () {
				var data = [];
				var preData = {};

				for (var i in self.projects) {
					for (var k in self.projects[i].posteExpenses) {
						var posteExpense = self.projects[i].posteExpenses[k];

						for (var j in posteExpense.expenses) {
							var expense = posteExpense.expenses[j];
							var time = moment(expense.date, 'DD/MM/YYYY HH:mm');

							time.set({
								date: 1,
								hour: 0,
								minute: 0,
								second: 0,
								millisecond: 0,
							});

							if (typeof preData[expense.type] === 'undefined') {
								preData[expense.type] = {};

								if (self.ht) {
									preData[expense.type + '_ht'] = {};
								}
							}

							if (typeof preData[expense.type][time.format('X')] === 'undefined') {
								preData[expense.type][time.format('X')] = 0;

								if (self.ht) {
									preData[expense.type + '_ht'][time.format('X')] = 0;
								}
							}

							preData[expense.type][time.format('X')] += expense.amount;

							if (self.ht) {
								preData[expense.type + '_ht'][time.format('X')] += expense.amountHT;
							}
						}
					}
				}

				if (self.monthCumulated) {
					for (type in preData) {
						var totalYears = {};

						for (time in preData[type]) {
							var year = moment(time, 'X').year();

							if (typeof totalYears[year] === 'undefined') {
								totalYears[year] = preData[type][time];
							} else {
								totalYears[year] += preData[type][time];
								preData[type][time] = totalYears[year];
							}
						}
					}
				}

				var timesObj = {};

				for (var type in preData) {
					var typeData = {
						key: translator('expense_type_' + type),
						values: [],
					};

					for (var time in preData[type]) {
						if (typeof timesObj[time] === 'undefined') {
							timesObj[time] = 0;
						}
						typeData.values.push({
							x: parseInt(time),
							y: Math.round(preData[type][time] * 100) / 100,
						});
					}

					data.push(typeData);
				}

				self.timesArr = [];

				for (var time in timesObj) {
					self.timesArr.push(parseInt(time));
				}

				$scope.chart.data = data;
				self.orderOnlyMultiBarChart();
			};

			self.orderOnlyMultiBarChart = function () {
				if ($scope.chart.options.chart.type === 'multiBarChart') {
					$scope.chart.options.chart.xDomain = self.timesArr;
				} else {
					if (typeof $scope.chart.options.chart.xDomain !== 'undefined') {
						delete $scope.chart.options.chart.xDomain;
					}
				}
			};

			$scope.chart = {
				api: {},
				options: {
					chart: {
						type: 'multiBarChart',
						showControls: false,
						height: 300,
						xAxis: {
							tickFormat: function (d) {
								return moment(d, 'X').format('MM/YYYY');
							},
						},
						yAxis: {
							tickFormat: function (d) {
								return d3.format(',')(d) + ' €';
							},
						},
					},
				},
				data: [],
			};

			// QUERIES
			self.queries = [];
			self.query = {
				list: name,
			};
			self.errors = {
				fields: {},
			};
			self.loadingQueries = false;
			self.loadingSaveQuery = false;

			const queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

			self.initQueries = () => {
				self.loadingQueries = true;

				queryService
					.findAll({
						col: queryApiCol,
						search: {
							data: {
								filters: {
									list: {
										op: 'eq',
										val: self.query.list,
									},
								},
							},
						},
					})
					.then(
						res => {
							self.queries = res.rows;
							self.loadingQueries = false;
						},
						() => {
							self.loadingQueries = false;
						},
					);
			};

			self.loadQuery = query => {
				self.filters = angular.copy(query.filters);

				if (angular.isArray(self.filters.project)) {
					self.filters.project = {};
				}
				if (angular.isArray(self.filters.posteExpense)) {
					self.filters.posteExpense = {};
				}
				if (angular.isArray(self.filters.expense)) {
					self.filters.expense = {};
				}

				self.applyFilters();

				$rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
			};

			self.saveQuery = () => {
				self.loadingSaveQuery = true;
				self.query.filters = self.filters;
				queryService.save(self.query, { col: queryApiCol }).then(
					function (res) {
						self.queries.push(res.object);

						self.query.name = '';
						self.loadingSaveQuery = false;
					},
					function (err) {
						for (const field in err.fields) {
							self.errors.fields[field] = err.fields[field];
						}
						self.loadingSaveQuery = false;
					},
				);
			};

			self.deleteQuery = query => {
				if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
					self.loadingSaveQuery = true;
					queryService.remove(query).then(
						function () {
							self.queries.splice(self.queries.indexOf(query), 1);
							self.loadingSaveQuery = false;
						},
						function () {
							self.loadingSaveQuery = false;
						},
					);
				}
			};
		};
	};
})(window.EVA.app);
