(function(app) {

    app.controller('indicatorTreeController', [
        '$scope', '$q', 'groupIndicatorService', 'treeBuilderService',
        function territoryTreeController($scope, $q, groupIndicatorService, treeBuilderService) {
            var self = this;

            self.tree = [];
            self.loading = {
                tree: true
            };

            groupIndicatorService.findAll({
                col: [
                    'id',
                    'name',
                    'indicators.id',
                    'indicators.name'
                ]
            }).then(function (res) {
                self.tree = res.rows;
                self.loading.tree = false;
            });

        }
    ])

})(window.EVA.app);
