<?php

namespace Home\Controller;

use Core\Controller\AbstractActionSLController;
use Home\Entity\Container;
use Home\Entity\ContainerGraphic;
use Home\Entity\Home;
use Home\Entity\HomeAdmin;
use User\Entity\User;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        $types        = Container::getTypes();
        $graphicTypes = ContainerGraphic::getGraphicTypes();
        $widths       = Container::getWidths();

        /** @var HomeAdmin $homeAdmin */
        $homeAdmin = $this->entityManager()->getRepository('Home\Entity\HomeAdmin')->find(1);

        if (!$homeAdmin) {
            $homeAdmin = new HomeAdmin();
            $homeAdmin->setEnabled(false);

            $this->entityManager()->persist($homeAdmin);
            $this->entityManager()->flush();
        }

        $timeGroups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $timeGroups = $this->allowedKeywordGroups('timesheet', false);
        }

        $taskGroups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $taskGroups = $this->allowedKeywordGroups('task', false);
        }

        // If the user doesn't have the right to see his home, he will see the HomeAdmin if it's enabled
        if (!$this->acl()->isAllowed('home:e:home:r') && !$homeAdmin->isEnabled()) {
            if ($this->moduleManager()->isActive('project')) {
                return $this->redirect()->toRoute('project');
            } else {
                return $this->redirect()->toRoute('directory/contact');
            }
        } else if (!$this->acl()->isAllowed('home:e:home:r') && $homeAdmin->isEnabled()) {
            return new ViewModel([
                'home'         => false,
                'homeAdmin'    => $homeAdmin,
                'types'        => $types,
                'graphicTypes' => $graphicTypes,
                'widths'       => $widths,
                'timeGroups'   => $timeGroups,
                'taskGroups'   => $taskGroups,
            ]);
        }

        /** @var User $user */
        $user = $this->authStorage()->getUser();

        /** @var Home $home */
        $home = $this->entityManager()->getRepository('Home\Entity\Home')->findOneByUser($user);

        if (!$home) {
            $home = new Home();
            $home->setUser($user);
            $home->setEnabled(false);

            $this->entityManager()->persist($home);
            $this->entityManager()->flush();
        }

        if (!$home->isEnabled() && !$homeAdmin->isEnabled()) {
            return $this->redirect()->toRoute('project');
        }

        return new ViewModel([
            'home'         => $home,
            'homeAdmin'    => $homeAdmin,
            'types'        => $types,
            'graphicTypes' => $graphicTypes,
            'widths'       => $widths,
            'timeGroups'   => $timeGroups,
            'taskGroups'   => $taskGroups,
        ]);
    }

    public function formAction()
    {
        $types        = Container::getTypes();
        $graphicTypes = ContainerGraphic::getGraphicTypes();
        $widths       = Container::getWidths();

        /** @var HomeAdmin $homeAdmin */
        $homeAdmin = $this->entityManager()->getRepository('Home\Entity\HomeAdmin')->find(1);

        if (!$homeAdmin) {
            $homeAdmin = new HomeAdmin();
            $homeAdmin->setEnabled(false);

            $this->entityManager()->persist($homeAdmin);
            $this->entityManager()->flush();
        }

        $timeGroups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $timeGroups = $this->allowedKeywordGroups('timesheet', false);
        }

        $taskGroups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $taskGroups = $this->allowedKeywordGroups('task', false);
        }

        // If the user doesn't have the right to see his home, he will see the HomeAdmin if it's enabled
        if (!$this->acl()->isAllowed('home:e:home:r') && !$this->acl()->isAllowed('home:e:homeAdmin:r')) {
            return $this->notFoundAction();
        } else if (!$this->acl()->isAllowed('home:e:home:r') && $this->acl()->isAllowed('home:e:homeAdmin:r')) {
            return new ViewModel([
                'home'         => false,
                'homeAdmin'    => $homeAdmin,
                'types'        => $types,
                'graphicTypes' => $graphicTypes,
                'widths'       => $widths,
                'timeGroups'   => $timeGroups,
                'taskGroups'   => $taskGroups,
            ]);
        }

        /** @var User $user */
        $user = $this->authStorage()->getUser();

        /** @var Home $home */
        $home = $this->entityManager()->getRepository('Home\Entity\Home')->findOneByUser($user);

        if (!$home) {
            $home = new Home();
            $home->setUser($user);
            $home->setEnabled(false);

            $this->entityManager()->persist($home);
            $this->entityManager()->flush();
        }

        return new ViewModel([
            'home'         => $home,
            'homeAdmin'    => $this->acl()->isAllowed('home:e:homeAdmin:r') ? $homeAdmin : false,
            'types'        => $types,
            'graphicTypes' => $graphicTypes,
            'widths'       => $widths,
            'timeGroups'   => $timeGroups,
            'taskGroups'   => $taskGroups,
        ]);
    }
}
