<?php

return [
    'menu_affichage_title' => 'Affichage',

    'text_entity'     => 'Texte',
    'list_entity'     => 'Liste',
    'calendar_entity' => 'Calendrier',
    'chat_entity'     => 'Note',
    'graphic_entity'  => 'Graphique',

    'home_entity'        => 'accueil',
    'homeAdmin_entity'   => 'accueil Admin',
    'home_module_title'  => 'Accueil',
    'home_nav_form'      => 'Modifier l\'[[home_module_title]]',
    'home_field_enabled' => 'Activé ?',
    'menu_help_title'    => 'Aide',

    'home_field_user_error_non_exists' => 'Cet utilisateur n\'existe pas ou plus',

    'home_message_enabled'  => '[[ home_entity | ucfirst ]] activé',
    'home_message_disabled' => '[[ home_entity | ucfirst ]] désactivé',

    'container_entity'       => 'bloc',
    'container_module_title' => 'Bloc',

    'container_variables'         => 'Variables disponibles',
    'container_field_title'       => 'Titre',
    'container_field_type'        => 'Type',
    'container_field_graphicType' => 'Type de graphique',
    'container_field_width'       => 'Largeur',
    'container_field_content'     => 'Contenu',
    'container_field_imageGlobal' => 'Images Globales',
    'container_field_entity'      => 'Entité',
    'container_field_query'       => 'Requête',
    'container_field_order'       => 'Ordre',

    'graphicType_by_month' => 'Par mois',
    'graphicType_pie' => 'Ventilation temps par agent/projet',

    'container_message_saved'   => '[[container_module_title]] enregistré',
    'container_question_delete' => 'Voulez-vous réellement supprimer ce [[ container_entity ]] ?',
    'container_message_deleted' => '[[ container_entity | ucfirst ]] supprimé',

    'container_nav_form'    => 'Modifier le [[container_entity]]',
    'container_create_form' => 'Créer un [[container_entity]]',

    'container_field_home_error_non_exists'  => 'Cet [[home_entity]] n\'existe pas ou plus',
    'container_field_query_error_non_exists' => 'Cette [[query_entity]] n\'existe pas ou plus',
];
