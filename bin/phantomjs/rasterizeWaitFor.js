/**
 * Wait until the test condition is true or a timeout occurs. Useful for waiting
 * on a server response or for a ui change (fadeIn, etc.) to occur.
 *
 * @param testFx javascript condition that evaluates to a boolean,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param onReady what to do when testFx condition is fulfilled,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param timeOutMillis the max amount of time to wait. If not specified, 3 sec is used.
 */

"use strict";

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 120000, //< Default Max Timout is 120s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function () {
            if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if (!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};

var page = require('webpage').create(),
    system = require('system'),
    address, output, size, authCookie;

if (system.args.length < 3 || system.args.length > 6) {
    console.error('Usage: rasterize.js URL filename [paperwidth*paperheight|paperformat] [auth Cookie]');
    console.error('  paper (pdf output) examples: "5in*7.5in", "10cm*20cm", "A4", "Letter"');
    console.error('  image (png/jpg output) examples: "1920px" entire page, window width 1920px');
    console.error('                                   "800px*600px" window, clipped to 800x600');
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    // 3 = size
    authCookie = system.args[4];

    if (authCookie) {
        page.customHeaders = {
            'Cookie': authCookie,
        }
    }

    page.viewportSize = {width: 600, height: 600};
    if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
        size = system.args[3].split('*');
        page.paperSize = size.length === 2 ? {width: size[0], height: size[1], margin: '0px'}
            : {format: system.args[3], orientation: 'portrait', margin: '1cm'};
    } else if (system.args.length > 3 && system.args[3].substr(-2) === "px") {
        size = system.args[3].split('*');
        var pageWidth;
        var pageHeight;
        if (size.length === 2) {
            pageWidth = parseInt(size[0], 10);
            pageHeight = parseInt(size[1], 10);
            page.viewportSize = {width: pageWidth, height: pageHeight};
            page.clipRect = {top: 0, left: 0, width: pageWidth, height: pageHeight};
        } else {
            pageWidth = parseInt(system.args[3], 10);
            pageHeight = parseInt(pageWidth * 3 / 4, 10); // it's as good an assumption as any
            page.viewportSize = {width: pageWidth, height: pageHeight};
        }
    }

    phantom.onError = function (msg, trace) {
        console.log('Error with Phantom :: ', msg);
        phantom.exit(1);
    };

    page.onError = function (msg, trace) {
        console.log('Error with Page :: ', msg);
    };

    // page.onLoadStarted = function() {
    //     console.log('onLoadStarted : ' + address);
    // }
    //
    // page.onLoadFinished = function(status) {
    //     console.log('onLoadFinished : ' + status);
    //     // Do other things here...
    // };

    // page.onResourceRequested = function(requestData, networkRequest) {
    //     console.log('Request (#' + requestData.id + '): ' + JSON.stringify(requestData));
    // };
    //
    // page.onResourceReceived = function(response) {
    //     console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
    // };

    page.open(address, function (status) {
        if (status !== 'success') {
            console.log('Unable to load the address!');
            phantom.exit(1);
        } else {
            waitFor(function () {
                // Check in the page if a specific element is now visible
                return page.evaluate(function () {
                    return  $(".partial-login") || $("#phantom").text() === 'ok';
                });
            }, function () {
                window.setTimeout(function () {
                    page.render(output);
                    phantom.exit();
                }, 500);
            });
        }
    });
}

