<?php

namespace Indicator;

return [
    'doctrine' => [
        'driver' => [
            'indicator_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Indicator\Entity' => 'indicator_entities'
                ]
            ]
        ],
    ]
];
