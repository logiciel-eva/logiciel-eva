<?php

namespace Analysis\View\Helper;

use Bootstrap\Entity\Client;
use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use Indicator\Entity\Measure;

class MeasureFilters extends EntityFilters
{

    public function __construct(Client $client, Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $typeOptions = [];
        foreach (Measure::getTypes() as $type) {
            $typeOptions[] = [
                'value' => $type,
                'text'  => $this->translator->__invoke('measure_type_' . $type),
            ];
        }

        $periodOptions = [];
        foreach (Measure::getPeriods() as $period) {
            $periodOptions[] = [
                'value' => $period,
                'text'  => $this->translator->__invoke('measure_period_' . $period),
            ];
        }

        $filters = [
            'type'      => [
                'label'   => $this->translator->__invoke('measure_field_type'),
                'type'    => 'manytoone',
                'options' => $typeOptions,
            ],
            'period'    => [
                'label'     => $this->translator->__invoke('measure_field_period'),
                'type'      => 'manytoone',
                'options'   => $periodOptions
            ],
            'date'      => [
                'label' => $this->translator->__invoke('measure_field_date'),
                'type'  => 'date',
            ],
            'value'     => [
                'label' => $this->translator->__invoke('measure_field_value'),
                'type'  => 'number',
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('measure_field_createdAt'),
                'type'  => 'date',
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('measure_field_updatedAt'),
                'type'  => 'date',
            ],
            'indicator' => [
                'label' => $this->translator->__invoke('measure_field_indicator'),
                'type'  => 'indicator-select',
            ],
            'campain' => [
                'label' => $this->translator->__invoke('measure_field_campain'),
                'type'  => 'campain-select',
            ],
            'campainMeasure.supposedDate'      => [
                'label' => $this->translator->__invoke('measure_field_supposedDate'),
                'type'  => 'date',
            ],
        ];
		if ($client->isMaster()) {			
            $clientsOptions = [];
			foreach ($client->getNetwork()->getClients() as $clientLoop) {
				if (!$clientLoop->isMaster()) {
					$clientsOptions[] = [
						'value' => $clientLoop->getId(),
						'text' => $clientLoop->getName(),
					];
				}
			}

            $filters['client'] = [
                'label' => $this->translator->__invoke('measure_field_client'),				
                'type' => 'manytoone',
                'options' => $clientsOptions,
            ];
        }
        $transverse = $moduleManager->getClientModuleConfiguration('indicator', 'transverse');
        if ($transverse) {
            $filters['transverse'] = [
                'label' => $this->translator->__invoke('measure_field_transverse'),
                'type'  => 'boolean',
            ];
        }

        if ($this->moduleManager->isActive('project')) {
            $filters['project'] = [
                'label' => $this->translator->__invoke('measure_field_project'),
                'type'  => 'project-select',
            ];
        }

        if ($this->moduleManager->isActive('map')) {
            $filters['territory'] = [
                'label' => ucfirst($this->translator->__invoke('territory_entities')),
                'type'  => 'territory-select',
            ];
        }


        $this->filters = $filters;
    }
}
