<?php

namespace Field\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Value
 *
 * @package Field\Value
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="field_value")
 */
class Value extends BasicRestEntityAbstract
{
    const CONSENT_ACCEPTED = 'accepted';
    const CONSENT_DECLINED = 'declined';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $uuid;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $entity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="`primary`")
     */
    protected $primary;

    /**
     * @var Field
     *
     * @ORM\ManyToOne(targetEntity="Field\Entity\Field")
     */
    protected $field;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $value;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @param int $primary
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
    }

    /**
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param Field $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        if (null !== $this->value && $this->field->getType() == Field::TYPE_CHECKBOX) {
            return json_decode($this->value);
        }
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        if ($this->field->getType() == Field::TYPE_CHECKBOX && is_array($value)) {
            $this->value = json_encode($value);
        } else {
            $this->value = $value;
        }
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'entity',
                'required' => true,
            ],
            [
                'name'     => 'primary',
                'required' => true,
            ],
            [
                'name'       => 'field',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $field = $entityManager->getRepository('Field\Entity\Field')->find($value);

                                return $field !== null;
                            },
                            'message'  => 'fieldValue_field_field_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'              => 'value',
                'required'          => true,
                'allow_empty'       => true,
                'continue_if_empty' => false,
                'validators'        => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $field = $context['field'];
                                if (is_array($context['field'])) {
                                    $field = $context['field']['id'];
                                }
                                /* @var Field $field */
                                $field = $entityManager->getRepository('Field\Entity\Field')->find($field);

                                if ($field->isUnique()) {
                                    $values = $entityManager->getRepository('Field\Entity\Value')->findBy([
                                        'entity' => $context['entity'],
                                        'value'  => $value,
                                        'field'  => $field,
                                    ]);

                                    if (sizeOf($values) > 0) {
                                        // Should be max one value but...
                                        foreach ($values as $value) {
                                            /* @var \Field\Entity\Value $value */
                                            if ($value->getPrimary() !== $context['primary']) {
                                                return false;
                                            }
                                        }
                                    }
                                }

                                return true;
                            },
                            'message'  => 'fieldValue_field_value_should_be_unique',
                        ],
                    ],
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $field = $context['field'];
                                if (is_array($context['field'])) {
                                    $field = $context['field']['id'];
                                }
                                /* @var Field $field */
                                $field = $entityManager->getRepository('Field\Entity\Field')->find($field);

                                if ($field->isRequired()) {
                                    if (!isset($value) || $value == '') {
                                        return false;
                                    }
                                }

                                return true;
                            },
                            'message'  => 'fieldValue_field_value_is_required',
                        ],
                    ],
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                // Cas spécial permettant lorsqu'un champ est vide de ne pas créer de données en BDD
                                $field = $context['field'];
                                if (is_array($context['field'])) {
                                    $field = $context['field']['id'];
                                }
                                /* @var Field $field */
                                $field = $entityManager->getRepository('Field\Entity\Field')->find($field);

                                if ($value == '' || $value === null || $value == []) {
                                    $valuesToRemove = $entityManager->getRepository('Field\Entity\Value')->findBy([
                                        'entity' => $context['entity'],
                                        'field'  => $field,
                                        'primary'  => $context['primary'],
                                    ]);

                                    if (sizeOf($valuesToRemove) === 0) {
                                        return false;
                                    }

                                    if (sizeOf($valuesToRemove) > 0) {
                                        // Should be max one value but...
                                        foreach ($valuesToRemove as $valueToRemove) {
                                            $entityManager->remove($valueToRemove);
                                        }
                                    }
                                }

                                return true;
                            },
                            'message'  => 'fieldValue_field_value_is_null',
                        ],
                    ],
                ],
                'filters'           => [
                    ['name' => 'StringTrim'],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
