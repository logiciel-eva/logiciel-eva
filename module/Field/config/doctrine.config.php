<?php

namespace Field;

return [
    'doctrine' => [
        'driver' => [
            'field_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Field\Entity' => 'field_entities'
                ]
            ]
        ]
    ]
];
