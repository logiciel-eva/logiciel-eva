(function (app) {

    app.directive('templateSelect', [
        'templateService', 'selectFactoryService',
        function templateSelect(templateService, selectFactoryService) {
            return selectFactoryService.create({
                service  : {
                    object: templateService
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
