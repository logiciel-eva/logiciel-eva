(function(app) {

    app.controller('roleFormController', [
        'roleService', 'flashMessenger', 'translator',
        function roleFormController(roleService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'rights'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.role = {
                rights: {},
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if(id) {
                    roleService.find(id, apiParams).then(function(res) {
                        self.role       = res.object;
                        self.panelTitle = self.role.name;
                    })
                }
            };

            self.saveRole = function saveRole() {
                self.errors.fields  = {};
                self.loading.save   = true;

                var isCreation = !self.role.id;

                roleService.save(self.role, apiParams).then(function(res) {
                    self.role         = res.object;
                    self.panelTitle   = self.role.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.role.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('role_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.rightReadChanged = function rightReadChanged(module, entity) {
                for (var key in self.role.rights) {
                    if (
                        !key.match('^' + module + ':[a-z]:' + entity) ||
                        key.match('^' + module + ':[a-z]:' + entity + ':r$')
                    ) {
                        continue;
                    }

                    self.role.rights[key].value = false;
                }
            };

            self.deleteRole = function deleteRole() {
                if (confirm(translator('role_question_delete').replace('%1', self.role.name))) {
                    roleService.remove(self.role).then(function(res) {
                        if (res.success) {
                            window.location = '/role';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
