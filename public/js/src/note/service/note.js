(function(app) {

    app.factory('noteService', [
        'restFactoryService',
        function noteService(restFactoryService) {
            return restFactoryService.create('/api/note');
        }
    ])

})(window.EVA.app);
