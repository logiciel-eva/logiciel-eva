<?php

namespace Bootstrap\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Client
 *
 * A "Client" represents a customer using the application.
 * Each client has its own database and its own theme.
 *
 * @package Bootstrap\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="eva_client")
 */
class Client extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="`key`", unique=true)
     */
    protected $key;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    protected $host;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $dbHost;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $dbPort;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $dbUser;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $dbPassword;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $dbName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $theme;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $modules;

    /**
     * @var array
     *
     * @ORM\Column(type="json", nullable=true)
     */
    protected $icons;

    /**
     * @var array
     *
     * @ORM\Column(type="json", nullable=true)
     */
    protected $translations;

    /**
     * @var Network
     *
     * @ORM\ManyToOne(targetEntity="Bootstrap\Entity\Network", inversedBy="clients")
     */
    protected $network;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * @return string
     */
    public function getFolder()
    {
        return getcwd() . '/public/clients/' . $this->getKey();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getDbHost()
    {
        return $this->dbHost;
    }

    /**
     * @param string $dbHost
     */
    public function setDbHost($dbHost)
    {
        $this->dbHost = $dbHost;
    }

    /**
     * @return string
     */
    public function getDbPort()
    {
        return $this->dbPort;
    }

    /**
     * @param string $dbPort
     */
    public function setDbPort($dbPort)
    {
        $this->dbPort = $dbPort;
    }

    /**
     * @return string
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * @param string $dbUser
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;
    }

    /**
     * @return string
     */
    public function getDbPassword()
    {
        return $this->dbPassword;
    }

    /**
     * @param string $dbPassword
     */
    public function setDbPassword($dbPassword)
    {
        $this->dbPassword = $dbPassword;
    }

    /**
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * @param string $dbName
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

    /**
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param string $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        if (is_file(getcwd() . '/public/clients/' . $this->getKey() . '/logo.png')) {
            return 'clients/' . $this->getKey() . '/logo.png';
        }

        return false;
    }

    /**
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @param array $modules
     */
    public function setModules($modules)
    {
        $this->modules = $modules;
    }

    /**
     * @return array
     */
    public function getIcons()
    {
        return $this->icons;
    }

    /**
     * @param array $icons
     */
    public function setIcons($icons)
    {
        $this->icons = $icons;
    }

    /**
     * @return array
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     */
    public function setTranslations($translations)
    {
        $this->translations = $translations;
    }

    /**
     * @return Network
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * @param Network $network
     */
    public function setNetwork($network)
    {
        $this->network = $network;
    }

    /**
     * @return bool
     */
    public function hasNetwork()
    {
        return $this->network !== null;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isMaster()
    {
        if ($this->hasNetwork()) {
            return $this == $this->network->getMaster();
        }

        return false;
    }

    /**
     * For API
     * @return bool
     */
    public function getMaster()
    {
        return $this->isMaster();
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'dbHost',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'dbName',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'dbPassword',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'dbPort',
                'required'   => true,
                'filters'    => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
            [
                'name'     => 'dbUser',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'host',
                'required'   => true,
                'filters'    => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $queryBuilder = $entityManager->createQueryBuilder();
                                $queryBuilder->select('c')
                                    ->from('Bootstrap\Entity\Client', 'c')
                                    ->where('c.host = :host')
                                    ->andWhere('c.id != :id')
                                    ->setParameters([
                                        'id'   => !isset($context['id']) ?: $context['id'],
                                        'host' => $value,
                                    ]);

                                try {
                                    $client = $queryBuilder->getQuery()->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $client = null;
                                }

                                return $client === null;
                            },
                            'message'  => 'client_field_host_error_unique',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'icons',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'key',
                'required'   => true,
                'filters'    => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $queryBuilder = $entityManager->createQueryBuilder();
                                $queryBuilder->select('c')
                                    ->from('Bootstrap\Entity\Client', 'c')
                                    ->where('c.key = :key')
                                    ->andWhere('c.id != :id')
                                    ->setParameters([
                                        'id'  => !isset($context['id']) ? '' : $context['id'],
                                        'key' => $value,
                                    ]);

                                try {
                                    $client = $queryBuilder->getQuery()->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $client = null;
                                }
                                return $client === null;
                            },
                            'message'  => 'client_field_key_error_unique',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'modules',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'theme',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'translations',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'email',
                'required'   => false,
                'validators' => [
                    ['name' => 'EmailAddress'],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
