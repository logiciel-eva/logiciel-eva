<?php

namespace Keyword\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Association
 * @package Keyword\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="keyword_association")
 */
class Association
{
    /**
     * @var Keyword
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Keyword\Entity\Keyword")
     */
    protected $keyword;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $entity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="`primary`")
     */
    protected $primary;

    /**
     * @return Keyword
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param Keyword $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @param int $primary
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
    }
}
