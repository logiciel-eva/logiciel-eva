<?php

namespace Budget\Controller\API;

use Budget\Entity\ImportConfiguration;
use Budget\Helper\ImportBudgetHelper;
use Budget\Service\ImportBudgetService;
use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\View\Model\JsonModel;

class ImportConfigurationController extends BasicRestController
{
    protected $entityClass = 'Budget\Entity\ImportConfiguration';
    protected $repository  = 'Budget\Entity\ImportConfiguration';
    protected $entityChain = 'budget:e:importConfiguration';

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }
    }

    public function runAction()
    {
        $id = $this->params()->fromRoute('id', false);

        $importConfiguration = $this->entityManager()->getRepository('Budget\Entity\ImportConfiguration')->findOneBy(['id' => $id]);
        if (!$importConfiguration) {
            return $this->notFoundAction();
        }

        $serviceManager = $this->serviceLocator->get('ServiceManager');

        $importBudgetHelper = new ImportBudgetHelper(
            $importConfiguration->getPath()
        );

        if($importBudgetHelper->hasFiles() === false){
            $importConfiguration->setLastImportAt(new \DateTime());
            $this->entityManager()->persist($importConfiguration);
            $this->getEntityManager()->flush();

            return new JsonModel(['error' => 'No files to imports']);
        }

        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $importController = $controllerManager->get('Budget\Controller\ImportController');

        $routeMatch = new RouteMatch([
            'controller' => $importController,
            'action' => 'save',
            'entity' => $importConfiguration->getBudgetEntity()
        ]);
        $e = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $e->setApplication($serviceManager->get('Application'));
        $importController->setEvent($e);
        $translator = $this->serviceLocator->get('Translator');

        foreach($importBudgetHelper->getFiles() as $importFile) {
            $importService = new ImportBudgetService(
                $importFile,
                $importBudgetHelper->getCsvFolderDonePath(),
                $translator,
            );

            $content = [
                'options' => [
                    'type' => $importConfiguration->getBudgetType(),
                    'mode' => $importConfiguration->getImportType(),
                    'year' => $importConfiguration->getYear(),
                    'inPosteAccountParent' => $importConfiguration->getInPosteAccountParent(),
                ],
                'properties' => $importConfiguration->getQuery()->getFilters(),
                'lines' => [],
            ];

            $content['lines'] = iterator_to_array(
                $importService->getCsvData(
                    $importConfiguration->getDelimiter()
                )
            );

            $request = new Request();
            $request->setMethod(Request::METHOD_POST);
            $request->setContent(
                json_encode($content)
            );

            $response = null;

            try {
                $response = $importController->dispatch($request);
            } catch (\Throwable $t) {
                $importService->logger->error($t->getMessage());
                $importService->logger->error('Something went wrong and this file cannot be imported, moving it.');
            }

            if ($response instanceof JsonModel) {
                $importService->writeLogs($response);
            }

            $importService->sendEmail($this->client, $this->clientPrivateConfiguration());
            $importService->moveFileToDonePath();

            $importConfiguration->setLastImportAt(new \DateTime());
            $this->entityManager()->persist($importConfiguration);
        }

        $this->getEntityManager()->flush();

        return new JsonModel([
            'success' => true,
        ]);
    }
}