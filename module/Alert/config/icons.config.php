<?php

return [
    'icons' => [
        'alert' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-bell-o',
        ],
        'send' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-paper-plane',
        ],
    ]
];
