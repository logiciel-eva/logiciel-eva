<?php

namespace User\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\InputFilter;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_configuration")
 */
class Configuration extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User", cascade={"all"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $tables = [];

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    protected $resizableTable = true;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getTables()
    {
        return $this->tables;
    }

    /**
     * @param array $tables
     */
    public function setTables($tables)
    {
        $this->tables = $tables;
    }

    public function getTable($name)
    {
        return isset($this->tables[$name]) ? $this->tables[$name] : [];
    }

    public function setTable($name, $cols)
    {
        $this->tables[$name] = $cols;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        // TODO: Implement getInputFilter() method.
    }

    /**
     * Get the value of resizableTable
     *
     * @return  boolean
     */ 
    public function getResizableTable()
    {
        return $this->resizableTable;
    }

    /**
     * Set the value of resizableTable
     *
     * @param  boolean  $resizableTable
     *
     * @return  self
     */ 
    public function setResizableTable($resizableTable)
    {
        $this->resizableTable = $resizableTable;

        return $this;
    }
}
