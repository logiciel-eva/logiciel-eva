(function(app) {

    app.factory('campain-measureService', [
        'restFactoryService',
        function campainindicatorService(restFactoryService) {
            return restFactoryService.create('/api/campain-measure');
        }
    ])

})(window.EVA.app);
