<?php

namespace Convention;

use Convention\View\Helper\ConventionFilters;
use Core\Module\AbstractModule;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
	/**
	 * @return array
	 */
	public function getViewHelperConfig()
	{
		return [
			'factories' => [
				'conventionFilters' => function (ServiceManager $sm) {
					$hpm = $sm->get('ViewHelperManager');
					$moduleManager = $sm->get('MyModuleManager');
					$client = $sm->get('Environment')->getClient();

					$translateViewHelper = $hpm->get('translate');
					$entityManager = $hpm->get('entityManager')->__invoke();

					$allowedKeywordGroupsViewHelper = $hpm->get('allowedKeywordGroups');

					return new ConventionFilters(
						$translateViewHelper,
						$moduleManager,
						$entityManager,
						$client,
						$allowedKeywordGroupsViewHelper
					);
				},
			],
		];
	}
}
