<?php

namespace Bootstrap\Environment;

interface EnvironmentInterface
{
    public function getEntityManager();
}
