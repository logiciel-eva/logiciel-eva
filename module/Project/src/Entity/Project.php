<?php

namespace Project\Entity;

use Advancement\Entity\Advancement;
use Budget\Entity\BudgetStatus;
use Budget\Entity\BudgetCode;
use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Budget\Helper\IBudgetComputable;
use Core\Entity\BasicRestEntityAbstract;
use Directory\Entity\Structure;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Keyword\Entity\Keyword;
use Time\Entity\Timesheet;
use User\Entity\User;
use Laminas\InputFilter\Factory;
use \DateTime;

/**
 * Class Project
 *
 * @package Project\Entity
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="project_project")
 * @ORM\Entity(repositoryClass="Project\Repository\ProjectRepository")
 */
class Project extends BasicRestEntityAbstract implements IBudgetComputable
{
	const PUBLICATION_STATUS_DRAFT = 'draft';
	const PUBLICATION_STATUS_PENDING = 'pending';
	const PUBLICATION_STATUS_PUBLISHED = 'published';
	const PUBLICATION_STATUS_ARCHIVED = 'archived';
	const PUBLICATION_STATUS_REJECTED = 'rejected';
	const PUBLICATION_STATUS_VALIDATED = 'validated';
	const PUBLICATION_STATUS_WAITING_VALIDATION = 'waiting_validation';

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean")
	 */
	protected $external = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean")
	 */
	protected $networkAccessible = false;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", unique=true, nullable=true)
	 */
	protected $code;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * @var Project
	 *
	 * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $parent;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Project\Entity\Project", mappedBy="parent")
	 */
	protected $children;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Project\Entity\Member", mappedBy="project", orphanRemoval=true, cascade={"all"})
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $members;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Project\Entity\Actor", mappedBy="project", orphanRemoval=true, cascade={"all"})
	 */
	protected $actors;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $result;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $context;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $objectifs;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $prevResult;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text")
	 */
	protected $publicationStatus;

	/**
	 * Programming Date
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $programmingDate;

	/**
	 * Planned Date Start
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $plannedDateStart;

	/**
	 * Planned Date End
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $plannedDateEnd;

	/**
	 * Real Date Start
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $realDateStart;

	/**
	 * Real Date End
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $realDateEnd;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Budget\Entity\PosteIncome", mappedBy="project", cascade={"all"})
	 */
	protected $posteIncomes;

	protected $posteIncomesArbo;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Budget\Entity\PosteExpense", mappedBy="project", cascade={"all"})
	 */
	protected $posteExpenses;

	protected $posteExpensesArbo;

	/**
	 * @var ArrayCollection | Timesheet[]
	 *
	 * @ORM\OneToMany(targetEntity="Time\Entity\Timesheet", mappedBy="project", cascade={"all"})
	 */
	protected $timesheets;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Advancement\Entity\Advancement", mappedBy="project", cascade={"all"})
	 * @ORM\OrderBy({"date" = "ASC"})
	 */
	protected $advancements;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Indicator\Entity\Measure", mappedBy="project", cascade={"all"})
	 */
	protected $measures;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Task\Entity\Task", mappedBy="project", cascade={"all"})
	 */
	protected $tasks;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Convention\Entity\Line", mappedBy="project", cascade={"all"})
	 */
	protected $conventions;

	/**
	 * @var BudgetCode[] | ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Budget\Entity\BudgetCode", mappedBy="project", cascade={"remove"})
	 */
	protected $budgetCodes;

	/**
	 * @var Structure (Maitrise d'ouvrage)
	 *
	 * @ORM\ManyToOne(targetEntity="Directory\Entity\Structure")
	 */
	protected $ownership;

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 */
	protected $level;

	/**
	 * @var BudgetStatus
	 *
	 * @ORM\ManyToOne(targetEntity="Budget\Entity\BudgetStatus")
	 */
	protected $budgetStatus;

	/**
	 * @var Template
	 *
	 * @ORM\ManyToOne(targetEntity="Project\Entity\Template")
	 */
	protected $template;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $globalTimeTarget;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", options={"default": 0})
	 */
	protected $rateExpenseRealized = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", options={"default": 0})
	 */
	protected $rateIncomeRealized = 0;

	/**
	 * @var AgeSynchronisation
	 *
	 * @ORM\OneToOne(targetEntity="AgeSynchronisation", mappedBy="project", cascade={"remove"})
	 */
	protected $ageSynchronisation;


	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="ProjectUpdate", mappedBy="project", orphanRemoval=true, cascade={"all"})
	 */
	protected $updates;

	public function __construct()
	{
		$this->publicationStatus = self::PUBLICATION_STATUS_DRAFT;

		$this->children = new ArrayCollection();
		$this->members = new ArrayCollection();
		$this->actors = new ArrayCollection();
		$this->posteExpenses = new ArrayCollection();
		$this->posteIncomes = new ArrayCollection();
		$this->timesheets = new ArrayCollection();
		$this->advancements = new ArrayCollection();
		$this->measures = new ArrayCollection();
		$this->tasks = new ArrayCollection();
		$this->conventions = new ArrayCollection();
		$this->budgetCodes = new ArrayCollection();
		$this->updates = new ArrayCollection();
	}

	/**
	 * Clone function
	 */
	public function __clone()
	{
		$this->id = null;

		$this->code .= date(' (Y-m-d H:i:s)');
		$this->name = rtrim($this->name) . ' du ' . date('d/m/Y H\hi');

		$this->children = new ArrayCollection();

		$members = new ArrayCollection();
		foreach ($this->members as $member) {
			$m = clone $member;
			$m->setProject($this);
			$members->add($m);
		}
		$this->members = $members;

		$actors = new ArrayCollection();
		foreach ($this->actors as $actor) {
			$a = clone $actor;
			$a->setProject($this);
			$actors->add($a);
		}
		$this->actors = $actors;

		$this->posteIncomes = new ArrayCollection();
		$this->posteExpenses = new ArrayCollection();
		$this->timesheets = new ArrayCollection();
		$this->advancements = new ArrayCollection();
		$this->measures = new ArrayCollection();
		$this->tasks = new ArrayCollection();
		$this->conventions = new ArrayCollection();
		$this->updates = new ArrayCollection();

		$this->createdAt = new \DateTime();
		$this->updatedAt = new \DateTime();
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return boolean
	 */
	public function isExternal()
	{
		return $this->getExternal();
	}

	/**
	 * @return boolean
	 */
	public function getExternal()
	{
		return $this->external;
	}

	/**
	 * @param boolean $external
	 */
	public function setExternal($external)
	{
		$this->external = $external ?? false;
	}

	/**
	 * @return boolean
	 */
	public function isNetworkAccessible()
	{
		return $this->getNetworkAccessible();
	}

	/**
	 * @return boolean
	 */
	public function getNetworkAccessible()
	{
		return $this->networkAccessible;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getUpdates()
	{
		return $this->updates;
	}

	/**
	 * @param ArrayCollection $updates
	 */
	public function setUpdates($updates)
	{
		$this->updates = $updates;
	}

	/**
	 * @param ArrayCollection $updates
	 */
	public function addUpdates(ArrayCollection $updates)
	{
		foreach ($updates as $update) {
			$update->setProject($this);
			$this->getUpdates()->add($update);
		}
	}

	/**
	 * @param ArrayCollection $updates
	 */
	public function removeUpdates(ArrayCollection $updates)
	{
		foreach ($updates as $update) {
			$update->setProject(null);
			$this->getUpdates()->removeElement($update);
		}
	}


	/**
	 * @param boolean $networkAccessible
	 */
	public function setNetworkAccessible($networkAccessible)
	{
		$this->networkAccessible = $networkAccessible ?? false;
	}

	/**
	 * @return Structure
	 */
	public function getOwnership()
	{
		return $this->ownership;
	}

	/**
	 * @param Structure $ownership
	 */
	public function setOwnership($ownership)
	{
		$this->ownership = $ownership;
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * @param string $code
	 */
	public function setCode($code)
	{
		if ($code === '') {
			$code = null;
		}
		$this->code = $code;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return Project
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param Project $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

	/**
	 * @return Project
	 */
	public function getRootParent()
	{
		return $this->getParentRecursive($this->getParent());
	}

	/**
	 * @param Project $project
	 * @return Project
	 */
	private function getParentRecursive($project)
	{
		if(null === $project) {
			return null;
		}
		if(null === $project->getParent()) {
			return $project;
		}
		return $this->getParentRecursive($project->getParent());
	}

	/**
	 * @param ArrayCollection $children
	 */
	public function addChildren(ArrayCollection $children)
	{
		foreach ($children as $child) {
			$child->setParent($this);
			$this->getChildren()->add($child);
		}
	}

	/**
	 * @param ArrayCollection $children
	 */
	public function removeChildren(ArrayCollection $children)
	{
		foreach ($children as $child) {
			$child->setParent(null);
			$this->getChildren()->removeElement($child);
		}
	}

	/**
	 * @return ArrayCollection
	 */
	public function getChildren()
	{
		return $this->children;
	}

	/**
	 * @param ArrayCollection $children
	 */
	public function setChildren($children)
	{
		$this->children = $children;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getManagers()
	{
		return $this->members->filter(function ($member) {
			return $member->getRole() === Member::ROLE_MANAGER;
		});
	}

	/**
	 * @return ArrayCollection
	 */
	public function getValidators()
	{
		return $this->members->filter(function ($member) {
			return $member->getRole() === Member::ROLE_VALIDATOR;
		});
	}

	public function isValidator(User $user)
	{
		$validators = $this->getValidators();
		foreach ($validators as $validator) {
			if ($validator->getUser() === $user) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getMembers()
	{
		return $this->members;
	}

	/**
	 * @param ArrayCollection $members
	 */
	public function setMembers($members)
	{
		$this->members = $members;
	}

	/**
	 * @param ArrayCollection $members
	 */
	public function addMembers(ArrayCollection $members)
	{
		foreach ($members as $member) {
			$member->setProject($this);
			$this->getMembers()->add($member);
		}
	}

	/**
	 * @param ArrayCollection $members
	 */
	public function removeMembers(ArrayCollection $members)
	{
		foreach ($members as $member) {
			$member->setProject(null);
			$this->getMembers()->removeElement($member);
		}
	}

	/**
	 * @return ArrayCollection
	 */
	public function getActors()
	{
		return $this->actors;
	}

	/**
	 * @param ArrayCollection $actors
	 */
	public function setActors($actors)
	{
		$this->actors = $actors;
	}

	/**
	 * @param ArrayCollection $actors
	 */
	public function addActors(ArrayCollection $actors)
	{
		foreach ($actors as $actor) {
			$actor->setProject($this);
			$this->getActors()->add($actor);
		}
	}

	/**
	 * @param ArrayCollection $actors
	 */
	public function removeActors(ArrayCollection $actors)
	{
		foreach ($actors as $actor) {
			$actor->setProject(null);
			$this->getActors()->removeElement($actor);
		}
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getResult()
	{
		return $this->result;
	}

	/**
	 * @param string $result
	 */
	public function setResult($result)
	{
		$this->result = $result;
	}

	/**
	 * @return string
	 */
	public function getContext()
	{
		return $this->context;
	}

	/**
	 * @param string $context
	 */
	public function setContext($context)
	{
		$this->context = $context;
	}

	/**
	 * @return string
	 */
	public function getObjectifs()
	{
		return $this->objectifs;
	}

	/**
	 * @param string $objectifs
	 */
	public function setObjectifs($objectifs)
	{
		$this->objectifs = $objectifs;
	}

	/**
	 * @return string
	 */
	public function getPrevResult()
	{
		return $this->prevResult;
	}

	/**
	 * @param string $prevResult
	 */
	public function setPrevResult($prevResult)
	{
		$this->prevResult = $prevResult;
	}

	/**
	 * @return string
	 */
	public function getPublicationStatus()
	{
		if (!$this->publicationStatus) {
			$this->publicationStatus = self::PUBLICATION_STATUS_DRAFT;
		}

		return $this->publicationStatus;
	}

	/**
	 * @param string $publicationStatus
	 */
	public function setPublicationStatus($publicationStatus)
	{
		if (!$publicationStatus || !in_array($publicationStatus, self::getPublicationStatutes())) {
			$publicationStatus = self::PUBLICATION_STATUS_DRAFT;
		}

		$this->publicationStatus = $publicationStatus;
	}

	/**
	 * @return \DateTime
	 */
	public function getRealDateEnd()
	{
		return $this->realDateEnd;
	}

	/**
	 * @param \DateTime $realDateEnd
	 */
	public function setRealDateEnd($realDateEnd)
	{
		$this->realDateEnd = $realDateEnd;
	}

	/**
	 * @return \DateTime
	 */
	public function getProgrammingDate()
	{
		return $this->programmingDate;
	}

	/**
	 * @param \DateTime $programmingDate
	 */
	public function setProgrammingDate($programmingDate)
	{
		$this->programmingDate = $programmingDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getPlannedDateStart()
	{
		return $this->plannedDateStart;
	}

	/**
	 * @param \DateTime $plannedDateStart
	 */
	public function setPlannedDateStart($plannedDateStart)
	{
		$this->plannedDateStart = $plannedDateStart;
	}

	/**
	 * @return \DateTime
	 */
	public function getPlannedDateEnd()
	{
		return $this->plannedDateEnd;
	}

	/**
	 * @param \DateTime $plannedDateEnd
	 */
	public function setPlannedDateEnd($plannedDateEnd)
	{
		$this->plannedDateEnd = $plannedDateEnd;
	}

	/**
	 * @return \DateTime
	 */
	public function getRealDateStart()
	{
		return $this->realDateStart;
	}

	/**
	 * @param \DateTime $realDateStart
	 */
	public function setRealDateStart($realDateStart)
	{
		$this->realDateStart = $realDateStart;
	}

	/**
	 * @return int
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 * @param int $level
	 */
	protected function setLevel($level)
	{
		$this->level = $level;
	}

	public function computeLevel()
	{
		$level = 0;
		if ($this->parent !== null) {
			$level = $this->getParent()->getLevel() + 1;
		}

		$this->setLevel($level);

		if ($this->children->count() > 0) {
			foreach ($this->children as $children) {
				$children->computeLevel();
			}
		}

		return $level;
	}

	/**
	 * @param ArrayCollection $posteExpenses
	 */
	public function setPosteExpenses($posteExpenses)
	{
		$this->posteExpenses = $posteExpenses;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getPosteExpenses()
	{
		return $this->posteExpenses;
	}

	public function setPosteExpensesArbo($postes)
	{
		$this->posteExpensesArbo = $postes;
	}

	/**
	 * @return ArrayCollection | PosteExpense[]
	 */
	public function getPosteExpensesArbo()
	{
		// For filter
		if ($this->posteExpensesArbo) {
			return $this->posteExpensesArbo;
		}

		$postes = $this->posteExpenses ? clone $this->posteExpenses : new ArrayCollection();
		foreach ($this->getChildren() as $child) {
			foreach ($child->getPosteExpensesArbo() as $posteExpense) {
				if (!$postes->contains($posteExpense) && !$posteExpense->getParent()) {
					$postes->add($posteExpense);
				}
			}
		}

		return $postes;
	}

	/**
	 * @param ArrayCollection $posteIncomes
	 */
	public function setPosteIncomes($posteIncomes)
	{
		$this->posteIncomes = $posteIncomes;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getPosteIncomes()
	{
		return $this->posteIncomes;
	}

	public function setPosteIncomesArbo($postes)
	{
		$this->posteIncomesArbo = $postes;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getPosteIncomesArbo()
	{
		// For filter
		if ($this->posteIncomesArbo) {
			return $this->posteIncomesArbo;
		}

		$postes = $this->posteIncomes ? clone $this->posteIncomes : new ArrayCollection();
		foreach ($this->getChildren() as $child) {
			foreach ($child->getPosteIncomesArbo() as $posteIncome) {
				if (!$postes->contains($posteIncome) && !$posteIncome->getParent()) {
					$postes->add($posteIncome);
				}
			}
		}

		return $postes;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getTimesheets()
	{
		return $this->timesheets;
	}

	/**
	 * @param ArrayCollection|Timesheet[] $timesheets
	 * @param bool                        $force
	 */
	public function setTimesheets($timesheets, $force = false)
	{
		// To avoid using it accidentely (now it's use to apply filters on timesheets)
		if ($force) {
			$this->timesheets = $timesheets;
		}
	}

	/**
	 * @return ArrayCollection
	 */
	public function getTimesheetsArbo()
	{
		$timesheets = new ArrayCollection();
		foreach ($this->timesheets as $timesheet) {
			$timesheets->add($timesheet);
		}

		foreach ($this->getChildren() as $child) {
			$childTimesheets = $child->getTimesheetsArbo();
			foreach ($childTimesheets as $timesheet) {
				$timesheets->add($timesheet);
			}
		}

		return $timesheets;
	}

	/**
	 * @return BudgetStatus
	 */
	public function getBudgetStatus()
	{
		return $this->budgetStatus;
	}

	/**
	 * @param BudgetStatus $budgetStatus
	 */
	public function setBudgetStatus($budgetStatus)
	{
		$this->budgetStatus = $budgetStatus;
	}

	/**
	 * Returns the financers of the project
	 *
	 * @return ArrayCollection
	 */
	public function getFinancers()
	{
		$financers = new ArrayCollection();
		foreach ($this->getPosteIncomesArbo() as $poste) {
			if ($poste->getEnvelope()) {
				$financer = $poste->getEnvelope()->getFinancer();
				if (!$financers->contains($financer)) {
					$financers->add($financer);
				}
			}
		}

		return $financers;
	}

	public function getAmountPosteExpense($em = null, $balance = '')
	{
		if ($balance === PosteExpense::BALANCE_AE_RATE_BALANCE) {
			return $this->getAmountExpenseAe() > 0
				? ($this->getAmountExpenseAeCommitted() * 100) / $this->getAmountExpenseAe()
				: 0;
		}
		if ($balance === PosteExpense::BALANCE_CP_COMMITTED_AE_COMMITTED_RATE_BALANCE) {
			return $this->getAmountExpenseAeCommitted() > 0
				? ($this->getAmountExpenseCpCommitted() / $this->getAmountExpenseAeCommitted() * 100)
				: 0;
		}

		$amount = 0;
		foreach ($this->getPosteExpenses() as $poste) {
			$amount += $poste->{'getAmount' . ucfirst($balance)}();
		}

		return $amount;
	}

	public function getAmountHTPosteExpense($em = null, $balance = '')
	{
		if ($balance === PosteExpense::BALANCE_AE_RATE_BALANCE) {
			return $this->getAmountHTExpenseAe() > 0
				? ($this->getAmountHTExpenseAeCommitted() * 100) / $this->getAmountHTExpenseAe()
				: 0;
		}
		if ($balance === PosteExpense::BALANCE_CP_COMMITTED_AE_COMMITTED_RATE_BALANCE) {
			return $this->getAmountHTExpenseAeCommitted() > 0
				? ($this->getAmountHTExpenseCpCommitted() / $this->getAmountHTExpenseAeCommitted() * 100)
				: 0;
		}

		$amount = 0;
		foreach ($this->getPosteExpenses() as $poste) {
			$amount += $poste->{'getAmountHT' . ucfirst($balance)}();
		}

		return $amount;
	}

	public function getAmountPosteExpenseArbo($em = null, $balance = '')
	{
		if ($balance === PosteExpense::BALANCE_AE_RATE_BALANCE) {
			return $this->getAmountExpenseArboAe() > 0
				? ($this->getAmountExpenseArboAeCommitted() * 100) / $this->getAmountExpenseArboAe()
				: 0;
		}
		if ($balance === PosteExpense::BALANCE_CP_COMMITTED_AE_COMMITTED_RATE_BALANCE) {
			return $this->getAmountExpenseArboAeCommitted() > 0
				? ($this->getAmountExpenseArboCpCommitted() / $this->getAmountExpenseArboAeCommitted() * 100)
				: 0;
		}

		$amount = 0;
		foreach ($this->getPosteExpensesArbo() as $poste) {
			$amount += $poste->{'getAmountArbo' . ucfirst($balance)}();
		}

		return $amount;
	}

	public function getAmountHTPosteExpenseArbo($em = null, $balance = '')
	{
		if ($balance === PosteExpense::BALANCE_AE_RATE_BALANCE) {
			return $this->getAmountHTExpenseArboAe() > 0
				? ($this->getAmountHTExpenseArboAeCommitted() * 100) / $this->getAmountHTExpenseArboAe()
				: 0;
		}
		if ($balance === PosteExpense::BALANCE_CP_COMMITTED_AE_COMMITTED_RATE_BALANCE) {
			return $this->getAmountHTExpenseArboAeCommitted() > 0
				? ($this->getAmountHTExpenseArboCpCommitted() / $this->getAmountHTExpenseArboAeCommitted() * 100)
				: 0;
		}

		$amount = 0;
		foreach ($this->getPosteExpensesArbo() as $poste) {
			$amount += $poste->{'getAmountHTArbo' . ucfirst($balance)}();
		}

		return $amount;
	}

	public function getAmountPosteIncome($em = null, $balance = '')
	{
		$amount = 0;
		foreach ($this->getPosteIncomes() as $poste) {
			$amount += $poste->{'getAmount' . ucfirst($balance)}();
		}

		return $amount;
	}

	public function getAmountHTPosteIncome($em = null, $balance = '')
	{
		$amount = 0;
		foreach ($this->getPosteIncomes() as $poste) {
			$amount += $poste->{'getAmountHT' . ucfirst($balance)}();
		}

		return $amount;
	}

	public function getAmountPosteIncomeArbo($em = null, $balance = '')
	{
		$amount = 0;
		foreach ($this->getPosteIncomesArbo() as $poste) {
			$amount += $poste->{'getAmountArbo' . ucfirst($balance)}();
		}

		return $amount;
	}

	public function getAmountHTPosteIncomeArbo($em = null, $balance = '')
	{
		$amount = 0;
		foreach ($this->getPosteIncomesArbo() as $poste) {
			$amount += $poste->{'getAmountHTArbo' . ucfirst($balance)}();
		}

		return $amount;
	}

	/**
	 * Returns the sum of incomes for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountIncome($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;
		foreach ($this->getPosteIncomes() as $poste) {
			$amount += $poste->{'getAmount' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the sum of incomes for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountHTIncome($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;
		foreach ($this->getPosteIncomes() as $poste) {
			$amount += $poste->{'getAmountHT' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the sum of incomes for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountIncomeArbo($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;
		foreach ($this->getPosteIncomesArbo() as $poste) {
			$amount += $poste->{'getAmountArbo' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the sum of incomes for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountHTIncomeArbo($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;
		foreach ($this->getPosteIncomesArbo() as $poste) {
			$amount += $poste->{'getAmountHTArbo' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the sum of autofi. incomes for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountIncomeAutofinancement($type)
	{
		$amount = 0;
		foreach ($this->getPosteIncomes() as $poste) {
			if ($poste->isAutofinancement()) {
				$amount += $poste->{'getAmount' . ucfirst($type)}();
			}
		}

		return $amount;
	}

	/**
	 * Returns the sum of autofi. incomes for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountIncomeAutofinancementArbo($type)
	{
		$amount = 0;
		foreach ($this->getPosteIncomesArbo() as $poste) {
			if ($poste->isAutofinancement()) {
				$amount += $poste->{'getAmountArbo' . ucfirst($type)}();
			}
		}

		return $amount;
	}

	/**
	 * Returns the sum of expenses for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountExpense($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;

		foreach ($this->getPosteExpenses() as $poste) {
			$amount += $poste->{'getAmount' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the sum of expenses HT for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountHTExpense($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;
		foreach ($this->getPosteExpenses() as $poste) {
			$amount += $poste->{'getAmountHT' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the sum of expenses for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountExpenseArbo($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;
		foreach ($this->getPosteExpensesArbo() as $poste) {
			$amount += $poste->{'getAmountArbo' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the sum of expenses HT for a given type
	 *
	 * @param $type
	 * @return int
	 */
	protected function getAmountHTExpenseArbo($type, $em = null, $year = null, $evolution = null)
	{
		$amount = 0;
		foreach ($this->getPosteExpensesArbo() as $poste) {
			$amount += $poste->{'getAmountHTArbo' . ucfirst($type)}($year, $evolution);
		}

		return $amount;
	}

	/**
	 * Returns the percentage/amount repartition of financers on the amount of the poste incomes
	 * @return array
	 */
	public function getFinancerRepartitionPosteIncome($em)
	{
		$amount = $this->getAmountPosteIncome($em);
		$repartition = [];
		foreach ($this->getPosteIncomes() as $poste) {
			if ($poste->getEnvelope()) {
				$financer = $poste->getEnvelope()->getFinancer();
				if (!array_key_exists($financer->getId(), $repartition)) {
					$repartition[$financer->getId()] = [
						'percentage' => 0,
						'amount' => 0,
						'name' => $financer->getName(),
					];
				}

				$repartition[$financer->getId()]['amount'] += $poste->getAmount();
			}
		}

		foreach ($repartition as $financerId => $data) {
			$repartition[$financerId]['percentage'] =
				$amount > 0 ? ($repartition[$financerId]['amount'] * 100) / $amount : 0;
		}

		return $repartition;
	}

	/**
	 * Returns the percentage/amount repartition of financers on the amount of the poste incomes
	 * @return array
	 */
	public function getFinancerRepartitionPosteIncomeArbo($em)
	{
		$amount = $this->getAmountPosteIncomeArbo($em);
		$repartition = [];
		foreach ($this->getPosteIncomesArbo() as $poste) {
			if ($poste->getEnvelope()) {
				$financer = $poste->getEnvelope()->getFinancer();
				if (!array_key_exists($financer->getId(), $repartition)) {
					$repartition[$financer->getId()] = [
						'percentage' => 0,
						'amount' => 0,
						'name' => $financer->getName(),
					];
				}

				$repartition[$financer->getId()]['amount'] += $poste->getAmount();
			}
		}

		foreach ($repartition as $financerId => $data) {
			$repartition[$financerId]['percentage'] =
				$amount > 0 ? ($repartition[$financerId]['amount'] * 100) / $amount : 0;
		}

		return $repartition;
	}

	/**
	 * Returns the percentage/amount repartition of financers on the incomes for a given type
	 * @return array
	 */
	public function getFinancerRepartitionIncome($type, $year = null)
	{
		$amountIncome = $this->{'getAmountIncome' . ucfirst($type)}($year);
		$repartition = [];
		foreach ($this->getPosteIncomes() as $poste) {
			if ($poste->getEnvelope()) {
				$financer = $poste->getEnvelope()->getFinancer();
				if (!array_key_exists($financer->getId(), $repartition)) {
					$repartition[$financer->getId()] = [
						'percentage' => 0,
						'amount' => 0,
						'name' => $financer->getName(),
					];
				}

				$repartition[$financer->getId()]['amount'] += $poste->{'getAmount' . ucfirst($type)}($year);
			}
		}

		foreach ($repartition as $financerId => $data) {
			$repartition[$financerId]['percentage'] =
				$amountIncome > 0 ? ($repartition[$financerId]['amount'] * 100) / $amountIncome : 0;
		}

		return $repartition;
	}

	/**
	 * Returns the percentage/amount repartition of financers on the incomes for a given type
	 * @return array
	 */
	public function getFinancerRepartitionIncomeArbo($type, $year = null)
	{
		$amountIncome = $this->{'getAmountInfvcomeArbo' . ucfirst($type)}($year);
		$repartition = [];
		foreach ($this->getPosteIncomesArbo() as $poste) {
			if ($poste->getEnvelope()) {
				$financer = $poste->getEnvelope()->getFinancer();
				if (!array_key_exists($financer->getId(), $repartition)) {
					$repartition[$financer->getId()] = [
						'percentage' => 0,
						'amount' => 0,
						'name' => $financer->getName(),
					];
				}

				$repartition[$financer->getId()]['amount'] += $poste->{'getAmountArbo' . ucfirst($type)}(
					$year
				);
			}
		}

		foreach ($repartition as $financerId => $data) {
			$repartition[$financerId]['percentage'] =
				$amountIncome > 0 ? ($repartition[$financerId]['amount'] * 100) / $amountIncome : 0;
		}

		return $repartition;
	}

	public function getUserTimeRepartition($em = null, $year = null)
	{
		$lines = [];
		foreach ($this->timesheets as $timesheet) {
			if ($timesheet->isYear($year)) {
				$user = $timesheet->getUser();
				if ($user) {
					$key = $user->getId();
				} else {
					$key = '0';
				}

				if (!isset($lines[$key])) {
					$lines[$key] = [
						'user' => [
							'id' => $user->getId(),
							'name' => $user->getName(),
							'profile' => $user->getProfile() ? $user->getProfile()->getName() :  null,
							'price_per_hour' => $user->getProfile() ?  $user->getProfile()->getPrice_per_hour() : null,
							'price_per_day'  => $user->getProfile() ?  $user->getProfile()->getPrice_per_day() : null,
							'profile_hour_per_day' => $user->getProfile() ?  $user->getProfile()->getHour_per_day() : null,
							'hour_per_day' => $user->getWorked_hours(),
						],
						'done' => 0,
						'target' => 0,
						'left' => 0,
						'target_cost' => 0,
						'done_cost' => 0,
						'left_cost' => 0,
					];
				}

				if ($timesheet->getType() === Timesheet::TYPE_DONE) {
					$lines[$key]['done'] += $timesheet->getHours();
				} elseif ($timesheet->getType() === Timesheet::TYPE_TARGET) {
					$lines[$key]['target'] += $timesheet->getHours();
				}

				$lines[$key]['left'] = $lines[$key]['target'] - $lines[$key]['done'];
				$lines[$key]['left_cost'] = $user->calculTimeCost($lines[$key]['left']);
			}
		}

		$lines =  array_map(function ($item) {
			$done = $item['done'];
			$item['done_cost'] = $item['user']['price_per_hour'] * $done;
			$item['target_cost'] = $item['user']['price_per_hour']  * $item['target'];

			return $item;
		}, $lines);

		return $lines;
	}

	public function getUserTimeRepartitionByYear($year = null)
	{
		$lines = [];

		foreach ($this->timesheets as $timesheet) {
			if (!$timesheet->isYear($year)) {
				continue;
			}

			$user = $timesheet->getUser();
			if ($user) {
				$key = $user->getId();
			} else {
				$key = '0';
			}

			if (!isset($lines[$key])) {
				$lines[$key] = [
					'user' => [
						'id' => $user->getId(),
						'name' => $user->getName(),
						'profile' => $user->getProfile() ? $user->getProfile()->getName() :  null,
						'price_per_hour' => $user->getProfile() ?  $user->getProfile()->getPrice_per_hour() : null,
						'price_per_day'  => $user->getProfile() ?  $user->getProfile()->getPrice_per_day() : null,
						'profile_hour_per_day' => $user->getProfile() ?  $user->getProfile()->getHour_per_day() : null,
						'hour_per_day' => $user->getWorked_hours(),
					],
					'years' => [],
				];
			}

			$timesheetYear = $timesheet->getEnd()->format('Y');

			if (!isset($lines[$key]['years'][$timesheetYear])) {
				$lines[$key]['years'][$timesheetYear] = [
					'done' => 0,
					'target' => 0,
					'left' => 0,
					'target_cost' => 0.00,
					'done_cost' => 0.00,
					'left_cost' => 0.00,
				];
			}

			if ($timesheet->getType() === Timesheet::TYPE_DONE) {
				$lines[$key]['years'][$timesheetYear]['done'] += $timesheet->getHours();
				$lines[$key]['years'][$timesheetYear]['done_cost'] += $user->calculTimeCost($timesheet->getHours());
			} elseif ($timesheet->getType() === Timesheet::TYPE_TARGET) {
				$lines[$key]['years'][$timesheetYear]['target'] += $timesheet->getHours();
				$lines[$key]['years'][$timesheetYear]['target_cost'] += $user->calculTimeCost($timesheet->getHours());
			}

			$lines[$key]['years'][$timesheetYear]['left'] =
				$lines[$key]['years'][$timesheetYear]['target'] -
				$lines[$key]['years'][$timesheetYear]['done'];
		}

		return $lines;
	}

	public function getUserTimeRepartitionArbo($em = null, $year = null)
	{
		$lines = $this->getUserTimeRepartition($em, $year);
		foreach ($this->children as $child) {
			$childLines = $child->getUserTimeRepartitionArbo($em, $year);
			foreach ($childLines as $key => $line) {
				if (!isset($lines[$key])) {
					$lines[$key] = $line;
				} else {
					$lines[$key]['done'] += $line['done'];
					$lines[$key]['done_cost'] += $line['done_cost'];
					$lines[$key]['target'] += $line['target'];
					$lines[$key]['target_cost'] += $line['target_cost'];
					$lines[$key]['left'] += $line['left'];
				}
			}
		}
		return $lines;
	}

	public function getUserTimeRepartitionByYearArbo($year = null)
	{
		$lines = $this->getUserTimeRepartitionByYear($year);

		foreach ($this->children as $child) {
			$childLines = $child->getUserTimeRepartitionArboByYear($year);

			foreach ($childLines as $key => $line) {
				if (!isset($lines[$key])) {
					$lines[$key] = $line;
					continue;
				}

				foreach ($line['years'] as $year => $values) {
					if (!isset($lines[$key]['years'][$year])) {
						$lines[$key]['years'][$year] = [
							'done' => 0,
							'target' => 0,
							'left' => 0,
						];
					}

					$lines[$key]['years'][$year]['done'] += $values['done'];
					$lines[$key]['years'][$year]['target'] += $values['target'];
					$lines[$key]['years'][$year]['left'] += $values['left'];
				}
			}
		}

		return $lines;
	}

	public function getCaduciteStart()
	{
		$date = null;
		foreach ($this->getPosteIncomes() as $poste) {
			if ($poste->getCaduciteStart() !== null) {
				if ($date == null || $date > $poste->getCaduciteStart()) {
					$date = $poste->getCaduciteStart();
				}
			}
		}

		return $date;
	}

	public function getCaduciteEnd()
	{
		$date = null;
		foreach ($this->getPosteIncomes() as $poste) {
			if ($poste->getCaduciteEnd() !== null) {
				if ($date == null || $date > $poste->getCaduciteEnd()) {
					$date = $poste->getCaduciteEnd();
				}
			}
		}

		return $date;
	}

	/**
	 * Returns the time spent from the timesheets entities
	 *
	 * @param int $year
	 * @return float
	 */
	public function getTimeSpent($em = null,$year = null)
	{
		$timeSpent = 0;
		foreach ($this->timesheets as $timesheet) {
			if ($timesheet->getType() === Timesheet::TYPE_DONE && $timesheet->isYear($year)) {
				$timeSpent += $timesheet->getHours();
			}
		}

		return $timeSpent;
	}

	/**
	 * Returns the time spent from the timesheets entities (arbo)
	 *
	 * @param int $year
	 * @return float
	 */
	public function getTimeSpentArbo($em = null, $year = null)
	{
		$timeSpent = 0;
		foreach ($this->children as $child) {
			$timeSpent += $child->getTimeSpentArbo($em, $year);
		}

		return $timeSpent + $this->getTimeSpent($em,$year);
	}

	/**
	 * @param null $year
	 * @return float
	 */
	public function getCostSpent($em, $year = null)
	{
		$costs = array_column($this->getUserTimeRepartition($em, $year), 'done_cost');

		return array_sum($costs);
	}

	/**
	 * @param null $year
	 * @return float
	 */
	public function getCostSpentArbo($em, $year = null)
	{
		$costs = array_column($this->getUserTimeRepartitionArbo($em, $year), 'done_cost');

		return array_sum($costs);
	}

	/**
	 * @param null $year
	 * @return float
	 */
	public function getCostTarget($year = null)
	{
		$costs = array_column($this->getUserTimeRepartition($year), 'target_cost');

		return array_sum($costs);
	}

	/**
	 * @param null $year
	 * @return float
	 */
	public function getCostTargetArbo($year = null)
	{
		$costs = array_column($this->getUserTimeRepartitionArbo($year), 'target_cost');

		return array_sum($costs);
	}

	/**
	 * Returns the time target from the timesheets entities
	 *
	 * @param int $year
	 * @return float
	 */
	public function getTimeTarget($em = null,  $year = null)
	{
		$timeTarget = 0;
		foreach ($this->timesheets as $timesheet) {
			if ($timesheet->getType() === Timesheet::TYPE_TARGET && $timesheet->isYear($year)) {
				$timeTarget += $timesheet->getHours();
			}
		}

		return $timeTarget;
	}

	/**
	 * Returns the time target from the timesheets entities (arbo)
	 *
	 * @param int $year
	 * @return float
	 */
	public function getTimeTargetArbo($em = null,$year = null)
	{
		$timeTarget = 0;
		foreach ($this->children as $child) {
			$timeTarget += $child->getTimeTargetArbo($em = null,$year);
		}

		return $timeTarget + $this->getTimeTarget($em = null,$year);
	}

	/**
	 * Returns the time left from the timesheets entities
	 *
	 * @param int $year
	 * @return float
	 */
	public function getTimeLeft($year = null)
	{
		return $this->getTimeTarget($year) - $this->getTimeSpent($year);
	}

	/**
	 * Returns the time left from the timesheets entities (arbo)
	 *
	 * @param int $year
	 * @return float
	 */
	public function getTimeLeftArbo($year = null)
	{
		return $this->getTimeTargetArbo($year) - $this->getTimeSpentArbo($year);
	}

	/**
	 * @param string $type
	 * @return Advancement|null
	 */
	public function getLastAdvancement($type)
	{
		$last = null;
		foreach ($this->advancements as $advancement) {
			if ($advancement->getType() === $type) {
				if ($last === null) {
					$last = $advancement;
				} elseif ($advancement->getDate() > $last->getDate()) {
					$last = $advancement;
				}
			}
		}

		return $last;
	}

	/**
	 * @param string $type
	 * @return Advancement|null
	 */
	public function getLastAddAdvancement($type, $year = null)
	{
		$last = null;
		foreach ($this->advancements as $advancement) {
			if ($advancement->getType() === $type && $advancement->isYear($year)) {
				if ($last === null) {
					$last = $advancement;
				} elseif ($advancement->getCreatedAt() > $last->getCreatedAt()) {
					$last = $advancement;
				}
			}
		}

		return $last;
	}

	public function getClosestAdvancement($type, $year = null)
	{
		$_diff = null;
		$now = new \DateTime();
		$closest = null;

		foreach ($this->advancements as $advancement) {
			if ($type === $advancement->getType() && $advancement->isYear($year)) {
				$diff = abs(
					strtotime($now->format('Y-m-d H:i:s')) -
						strtotime($advancement->getDate()->format('Y-m-d H:i:s'))
				);
				if ($_diff === null || $diff <= $_diff) {
					$closest = $advancement;
					$_diff = $diff;
				}
			}
		}

		return $closest;
	}

	public function getFarthestAdvancement($type, $year = null)
	{
		$_diff = null;
		$now = new \DateTime();
		$farthest = null;

		foreach ($this->advancements as $advancement) {
			if ($type === $advancement->getType() && $advancement->isYear($year)) {
				$diff = abs(
					strtotime($now->format('Y-m-d H:i:s')) -
						strtotime($advancement->getDate()->format('Y-m-d H:i:s'))
				);
				if ($_diff === null || $diff >= $_diff) {
					$farthest = $advancement;
					$_diff = $diff;
				}
			}
		}

		return $farthest;
	}

	/**
	 * @param Advancement $advancement
	 * @param string      $type
	 * @return array
	 */
	public function getClosestsAdvancements($type, Advancement $advancement = null)
	{
		$lower = null;
		$lowerDiff = null;
		$upper = null;
		$upperDiff = null;

		if ($advancement !== null) {
			foreach ($this->advancements as $_advancement) {
				if ($type === $_advancement->getType()) {
					$diff =
						strtotime($advancement->getDate()->format('Y-m-d H:i:s')) -
						strtotime($_advancement->getDate()->format('Y-m-d H:i:s'));
					if ($diff > 0) {
						if ($lowerDiff === null || $diff < $lowerDiff) {
							$lowerDiff = $diff;
							$lower = $_advancement;
						}
					} else {
						if ($upperDiff === null || $diff > $upperDiff) {
							$upperDiff = $diff;
							$upper = $_advancement;
						}
					}
				}
			}
		}

		return [
			'upper' => $upper,
			'lower' => $lower,
		];
	}

	public function getStackedAdvancement()
	{
		$lastDone = $this->getLastAdvancement(Advancement::TYPE_DONE);
		$lastTarget = $this->getLastAdvancement(Advancement::TYPE_TARGET);
		$closests = $this->getClosestsAdvancements(Advancement::TYPE_TARGET, $lastDone);

		$_done = $lastDone ? $lastDone->getValue() : 0;
		$_upperTarget = $closests['upper'] ? $closests['upper']->getValue() : 0;
		$_late = $closests['lower']
			? ($closests['lower']->getValue() > $_done
				? $closests['lower']->getValue()
				: 0)
			: 0;
		$_totalTarget = $lastTarget ? $lastTarget->getValue() : 0;

		$done = $_done;
		$late = $_late - $_done > 0 ? $_late - $_done : 0;
		$upperTarget = $_upperTarget - ($late + $done) > 0 ? $_upperTarget - ($late + $done) : 0;
		$totalTarget = $_totalTarget - ($done + $late + $upperTarget);

		return [
			'done' => $done,
			'late' => $late,
			'upperTarget' => $upperTarget,
			'totalTarget' => $totalTarget,
		];
	}

	public function getHierarchySup()
	{
		$hierarchy = new ArrayCollection();
		if ($this->parent !== null) {
			$hierarchy[] = $this->parent;
			$hierarchy = array_merge($this->parent->getHierarchySup()->toArray(), $hierarchy->toArray());
			$hierarchy = new ArrayCollection($hierarchy);
		}

		return $hierarchy;
	}

	public function getHierarchySub()
	{
		$hierarchy = new ArrayCollection();
		if ($this->children) {
			foreach ($this->children as $children) {
				$hierarchy[] = $children;
				$hierarchy = array_merge($hierarchy->toArray(), $children->getHierarchySub()->toArray());
				$hierarchy = new ArrayCollection($hierarchy);
			}
		}

		return $hierarchy;
	}

	public function getTerritories($em)
	{
		$associations = $em->getRepository('Map\Entity\Association')->findBy([
			'entity' => 'Project\Entity\Project',
			'primary' => $this->id,
		]);

		$territories = new ArrayCollection();
		foreach ($associations as $association) {
			$territories->add($association->getTerritory());
		}
		return $territories;
	}

	public function getKeywords($em)
	{
		$associations = $em->getRepository('Keyword\Entity\Association')->findBy([
			'entity' => 'Project\Entity\Project',
			'primary' => $this->id,
		]);

		$keywords = new ArrayCollection();
		foreach ($associations as $association) {
			$keywords->add($association->getKeyword());
		}
		return $keywords;
	}

	public function getTerritoriesArbo($em)
	{
		$territories = $this->getTerritories($em);

		foreach ($this->children as $child) {
			$childTerritories = $child->getTerritoriesArbo($em);
			foreach ($childTerritories as $territory) {
				if (!$territories->contains($territory)) {
					$territories->add($territory);
				}
			}
		}

		return $territories;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getAdvancements()
	{
		return $this->advancements;
	}

	/**
	 * @param ArrayCollection $advancements
	 */
	public function setAdvancements($advancements)
	{
		$this->advancements = $advancements;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getMeasures($indicator = null, $year = null)
	{
		return $this->measures->filter(function ($measure) use ($indicator, $year) {
            $indicatorFilter = $indicator == null || (null !== $measure->getIndicator() && $measure->getIndicator()->getId() == $indicator);
			return  $indicatorFilter && $measure->isYear($year);
		});
	}

	/**
	 * @param ArrayCollection $measures
	 */
	public function setMeasures($measures)
	{
		$this->measures = $measures;
	}

	public function getMeasuresT()
	{
		$res = [];

		$measures = $this->measures->filter(function ($measure) {
			return $measure->getTransverse();
		});

		foreach ($measures as $measure) {
			if (!isset($res[$measure->getIndicator()->getId()])) {
				$res[$measure->getIndicator()->getId()] = $measure;
			}

			if ($res[$measure->getIndicator()->getId()]->getDate() <= $measure->getDate()) {
				$res[$measure->getIndicator()->getId()] = $measure;
			}
		}

		/**
		 * @var  $measure Measure
		 */
		foreach ($res as $indicator => $measure) {
			$res[$indicator] = $measure->getFixedValue();
		}

		return $res;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getIndicators($year = null)
	{
		$indicators = new ArrayCollection();
		$measures = $this->getMeasures(null, $year);
		foreach ($measures as $measure) {
			if (!$indicators->contains($measure->getIndicator())) {
				$indicators->add($measure->getIndicator());
			}
		}

		return $indicators;
	}

	/**
	 * @param null $year
	 * @return array
	 */
	public function getIndicatorsIds($year = null)
	{
		$ids = [];
		foreach ($this->getIndicators($year) as $indicator) {
            if (null !== $indicator) {
                $ids[] = $indicator->getId();
            }
		}
		return $ids;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getTasks()
	{
		return $this->tasks;
	}

	/**
	 * @param ArrayCollection $tasks
	 */
	public function setTasks($tasks)
	{
		$this->tasks = $tasks;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getConventions()
	{
		return $this->conventions;
	}

	/**
	 * @param ArrayCollection $conventions
	 */
	public function setConventions($conventions)
	{
		$this->conventions = $conventions;
	}

	/**
	 * @return BudgetCode[] | ArrayCollection
	 */
	public function getBudgetCodes()
	{
		return $this->budgetCodes;
	}

	/**
	 * @param BudgetCode[] | ArrayCollection $budgetCodes
	 */
	public function setBudgetCodes($budgetCodes)
	{
		$this->budgetCodes = $budgetCodes;
	}

	/**
	 * @param BudgetCode[] | ArrayCollection $budgetCodes
	 */
	public function addBudgetCodes($budgetCodes)
	{
		foreach ($budgetCodes as $budgetCode) {
			$budgetCode->setProject($this);
			$this->getBudgetCodes()->add($budgetCode);
		}
	}

	/**
	 * @param BudgetCode[] | ArrayCollection $budgetCodes
	 */
	public function removeBudgetCodes($budgetCodes)
	{
		foreach ($budgetCodes as $budgetCode) {
			$budgetCode->setProject(null);
			$this->getBudgetCodes()->removeElement($budgetCode);
		}
	}

	/**
	 * @return Template
	 */
	public function getTemplate()
	{
		return $this->template;
	}

	/**
	 * @param Template $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

	/**
	 * @return float
	 */
	public function getGlobalTimeTarget()
	{
		return (float) $this->globalTimeTarget;
	}

	/**
	 * @param float $globalTimeTarget
	 */
	public function setGlobalTimeTarget($globalTimeTarget)
	{
		$this->globalTimeTarget = $globalTimeTarget;
	}

	/**
	 * @return float
	 */
	public function getGlobalTimeTargetArbo()
	{
		$globalTimeTarget = $this->getGlobalTimeTarget();

		foreach ($this->children as $child) {
			$globalTimeTarget += $child->getGlobalTimeTargetArbo();
		}

		return $globalTimeTarget;
	}

	/**
	 * @return float
	 */
	public function getRateExpenseRealized()
	{
		return $this->rateExpenseRealized;
	}

	public function setRateExpenseRealized()
	{
		/** @var ArrayCollection|PosteExpense[] $postes */
		$postes = $this->getPosteExpensesArbo();

		if ($postes->count() === 0) {
			$this->rateExpenseRealized = 0;
			return;
		}

		$amountPaid = 0;
		$amount = 0;

		foreach ($postes as $poste) {
			$amountPaid += $poste->getAmountArboPaid();
			$amount += $poste->getAmountArbo();
		}

		if ($amount == 0) {
			$this->rateExpenseRealized = 0;
			return;
		}

		$this->rateExpenseRealized = ($amountPaid / $amount) * 100;
	}

	/**
	 * @return float
	 */
	public function getRateIncomeRealized()
	{
		return $this->rateIncomeRealized;
	}

	public function setRateIncomeRealized()
	{
		/** @var ArrayCollection|PosteIncome[] $postes */
		$postes = $this->getPosteIncomesArbo();

		if ($postes->count() === 0) {
			$this->rateIncomeRealized = 0;
			return;
		}

		$amountPerceived = 0;
		$amount = 0;

		foreach ($postes as $poste) {
			$amountPerceived += $poste->getAmountArboPerceived();
			$amount += $poste->getAmountArbo();
		}

		if ($amount == 0) {
			$this->rateIncomeRealized = 0;
			return;
		}

		$this->rateIncomeRealized = ($amountPerceived / $amount) * 100;
	}

	// TODO : method should be moved out of Entity file
	// to app/module/Project/src/Service/ProjectfieldDescriptorService
	public function getInputFilter(EntityManager $entityManager, User $user = null)
	{
		$inputFilterFactory = new Factory();
		$inputFilter = $inputFilterFactory->createInputFilter([
			[
				'name' => 'code',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value, $context) use ($entityManager) {
								$queryBuilder = $entityManager->createQueryBuilder();
								$queryBuilder
									->select('p')
									->from('Project\Entity\Project', 'p')
									->where('p.code = :code')
									->andWhere('p.id != :id')
									->setParameters([
										'id' => !isset($context['id']) ? '' : $context['id'],
										'code' => $value,
									]);

								try {
									$project = $queryBuilder->getQuery()->getOneOrNullResult();
								} catch (\Exception $e) {
									$project = null;
								}

								return $project === null;
							},
							'message' => 'project_field_code_error_unique',
						],
					],
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value, $context) use ($entityManager) {
								$queryBuilder = $entityManager->createQueryBuilder();

								$queryBuilder
									->select('o')
									->from(BudgetCode::class, 'o')
									->where('o.name = :code')
									->setParameters([
										'code' => $value,
									]);

								try {
									$entity = $queryBuilder->getQuery()->getOneOrNullResult();
								} catch (Exception $e) {
									$entity = null;
								}

								return $entity === null;
							},
							'message' => 'project_field_code_error_unique',
						],
					],
				],
			],
			[
				'name' => 'name',
				'required' => true,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'parent',
				'required' => false,
				'filters' => [['name' => 'Core\Filter\IdFilter']],
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value, $context) use ($entityManager) {
								if (is_array($value)) {
									$value = $value['id'];
								}
								$project = $entityManager->getRepository('Project\Entity\Project')->find($value);

								return $project !== null;
							},
							'message' => 'project_field_parent_error_non_exists',
						],
					],
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value, $context) use ($entityManager) {
								if (is_array($value)) {
									$value = $value['id'];
								}

								if (isset($context['id'])) {
									$project = $entityManager->getRepository('Project\Entity\Project')->find($value);

									$same = $value == $context['id'];
									$hierarchy = false;
									while ($project->getParent() !== null && !$hierarchy) {
										$hierarchy = $project->getParent()->getId() == $context['id'];
										$project = $project->getParent();
									}

									return !$same && !$hierarchy;
								}

								return true;
							},
							'message' => 'project_field_parent_error_hierarchy',
						],
					],
				],
			],
			[
				'name' => 'description',
				'required' => false,
				'filters' => [['name' => 'StringTrim']],
			],
			[
				'name' => 'result',
				'required' => false,
				'filters' => [['name' => 'StringTrim']],
			],
			[
				'name' => 'objectifs',
				'required' => false,
				'filters' => [['name' => 'StringTrim']],
			],
			[
				'name' => 'prevResult',
				'required' => false,
				'filters' => [['name' => 'StringTrim']],
			],
			[
				'name' => 'context',
				'required' => false,
				'filters' => [['name' => 'StringTrim']],
			],
			[
				'name' => 'members',
				'required' => false,
			],
            [
                'name' => 'team_members',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager, $user) {
                                if (is_string($value)) {
                                    foreach (explode(" -- ", $value) as $val) {
                                        if (strpos($val, ',') !== false) {
                                            $roleName = explode(",", trim($val))[1];

                                            $queryBuilder = $entityManager->createQueryBuilder();

                                            $queryBuilder
                                                ->select('k.id')
                                                ->from(Keyword::class, 'k')
                                                ->leftJoin('k.group', 'g')
                                                ->where('LOWER(k.name) LIKE LOWER(:role)')
                                                ->andWhere("g.entities LIKE '%member%'")
                                                ->setParameters([
                                                    'role' => $roleName,
                                                ]);

                                            try {
                                                $role = $queryBuilder->getQuery()->getOneOrNullResult();
                                            } catch (Exception $e) {
                                                $role = null;
                                            }

                                            if ($role == null) {
                                                return false;
                                            }
                                        }
                                    }
                                }
                                return true;
                            },
                            'message' => 'project_field_members_role_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager, $user) {
                                if (is_string($value)) {
                                    foreach (explode(" -- ", $value) as $val) {
                                        if (count(explode(",", trim($val))) !== 2) {
                                            $name = $val;
                                        } else {
                                            $name = explode(",", trim($val))[0];
                                        }
                                        $queryBuilder = $entityManager->createQueryBuilder();

                                        $queryBuilder
                                            ->select('u.id')
                                            ->from(User::class, 'u')
                                            ->where('LOWER(u.name) LIKE LOWER(:name)')
                                            ->setParameters([
                                                'name' => $name,
                                            ]);

                                        try {
                                            $user = $queryBuilder->getQuery()->getOneOrNullResult();
                                        } catch (Exception $e) {
                                            $user = null;
                                        }
                                        if ($user == null) {
                                            return false;
                                        }
                                    }
                                }
                                return true;
                            },
                            'message' => 'project_field_members_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager, $user) {
                                $names = [];
                                foreach (explode(" -- ", $value) as $val) {
                                    if (count(explode(",", trim($val))) !== 2) {
                                        $name = $val;
                                    } else {
                                        $name = explode(",", trim($val))[0];
                                    }
                                    if (!in_array($name, $names))
                                        $names[] = $name;
                                    else {
                                        return false;
                                    }
                                }
                                return true;
                            },
                            'message' => 'project_field_members_error_duplicate',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager, $user) {
                                $isCorrect = true;
                                if (is_string($value)) {
                                    $value = ' ' . $value . ' ';
                                    foreach(explode("--", $value) as $val) {
                                        if($val[0] == " " && $val[strlen($val)-1] == " ") {
                                            if(strpos($val, ',') !== false) {
                                                $val2 = explode(',', $val);
                                                if($val2[0][strlen($val2[0])-1] == " " || $val2[1][0] == " ") {
                                                    $isCorrect = false;
                                                }
                                            }
                                        } else {
                                            $isCorrect = false;
                                        }
                                    }
                                }
                                return $isCorrect;
                            },
                            'message' => 'project_field_members_error_syntax',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'actors',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager, $user) {
                                if (is_string($value)) {
                                    foreach (explode(" -- ", $value) as $val) {
                                        if (strpos($val, ',') !== false) {
                                            $roleName = explode(",", trim($val))[1];

                                            $queryBuilder = $entityManager->createQueryBuilder();

                                            $queryBuilder
                                                ->select('k.id')
                                                ->from(Keyword::class, 'k')
                                                ->leftJoin('k.group', 'g')
                                                ->where('LOWER(k.name) LIKE LOWER(:role)')
                                                ->andWhere("g.entities LIKE '%actor%'")
                                                ->setParameters([
                                                    'role' => $roleName,
                                                ]);
                                            try {
                                                $role = $queryBuilder->getQuery()->getOneOrNullResult();
                                            } catch (Exception $e) {
                                                $role = null;
                                            }

                                            if ($role == null) {
                                                return false;
                                            }
                                        }
                                    }
                                }
                                return true;
                            },
                            'message' => 'project_field_members_role_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager, $user) {
                                if (is_string($value)) {
                                    foreach (explode(" -- ", $value) as $val) {
                                        if (count(explode(",", trim($val))) !== 2) {
                                            $structure = $val;
                                        } else {
                                            $structure = explode(",", trim($val))[0];
                                        }
                                        $queryBuilder = $entityManager->createQueryBuilder();

                                        $queryBuilder
                                            ->select('u.id')
                                            ->from(Structure::class, 'u')
                                            ->where('LOWER(u.name) LIKE LOWER(:name)')
                                            ->setParameters([
                                                'name' => $structure,
                                            ]);

                                        try {
                                            $user = $queryBuilder->getQuery()->getOneOrNullResult();
                                        } catch (Exception $e) {
                                            $user = null;
                                        }

                                        if ($user == null) {
                                            return false;
                                        }
                                    }
                                }
                                return true;
                            },
                            'message' => 'project_field_actors_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager, $user) {
                                $isCorrect = true;
                                if (is_string($value)) {
                                    $value = ' ' . $value . ' ';
                                    foreach(explode("--", $value) as $val) {
                                        if($val[0] == " " && $val[strlen($val)-1] == " ") {
                                            if(strpos($val, ',') !== false) {
                                                $val2 = explode(',', $val);
                                                if($val2[0][strlen($val2[0])-1] == " " || $val2[1][0] == " ") {
                                                    $isCorrect = false;
                                                }
                                            }
                                        } else {
                                            $isCorrect = false;
                                        }
                                    }
                                }
                                return $isCorrect;
                            },
                            'message' => 'project_field_members_error_syntax',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'managers',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) { 
                                return (is_array($value) && isset($value[0]) &&
                                    ((isset($value[0]['user']) && is_int($value[0]['user']))) ||
                                    (isset($value[0]['id']) && is_int($value[0]['id'])) ||
                                    is_int($value[0])
                                );
                            },
                            'message' => 'project_field_user_no_exist',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'validators',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return (is_array($value) && isset($value[0]) && (
                                    (isset($value[0]['user']) && is_int($value[0]['user']))) ||
                                    (isset($value[0]['id']) && is_int($value[0]['id'])));
                            },
                            'message' => 'project_field_user_no_exist',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'advancement',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager, $user) {
                                if (is_string($value)) {
                                    if (count(explode(",", trim($value))) !== 2) {
                                        return false;
                                    }
                                    $date = str_replace(' ', '', explode(",", trim($value))[0]);
                                    if (strlen($date) != 10) {
                                        return false;
                                    }
                                    $tempDate = explode('-', $date);
                                    if (!checkdate($tempDate[1], $tempDate[2], $tempDate[0])) {
                                        return false;
                                    }
                                }
                                return true;
                            },
                            'message' => 'project_field_advancement_date_percent_error',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager, $user) {
                                if (count(explode(",", trim($value))) !== 2) {
                                    return false;
                                }
                                $percentage = str_replace(' ', '', explode(",", trim($value))[1]);
                                if (!preg_match(' (\d+(\.\d+)?%)', $percentage)) {
                                    return false;
                                }
                                $percentNumber = (int)filter_var($percentage, FILTER_SANITIZE_NUMBER_INT);
                                if ($percentage < 0 || $percentNumber > 100) {
                                    return false;
                                }
                                return true;
                            },
                            'message' => 'project_field_advancement_date_percent_error',
                        ],
                    ]
                ],
            ],
			[
				'name' => 'publicationStatus',
				'required' => false,
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value, $context) use ($entityManager, $user) {
								if (!$user) {
									return false;
								}

								if (!isset($context['id'])) {
									return true;
								}

								if ($user->getRole()->getRights()['project:e:project:u']['owner'] === 'own') {
									if (
										$value == self::PUBLICATION_STATUS_REJECTED ||
										$value == self::PUBLICATION_STATUS_VALIDATED
									) {
										foreach (self::getValidators() as $member) {
											if ($member->getUser() === $user) {
												return true;
											}
										}

										return false;
									} else {
										foreach (self::getManagers() as $member) {
											if ($member->getUser() === $user) {
												return true;
											}
										}

										foreach (self::getValidators() as $member) {
											if ($member->getUser() === $user) {
												return true;
											}
										}

										return false;
									}
								}

                                return true;
                            },
                            'message' => 'project_field_publicationStatus_error_right',
                        ],
                    ],
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getPublicationStatutes(),
                        ],
                    ],
                ],
            ],
            [
                'name' => 'programmingDate',
                'required' => false,
                'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {

                                return ($value instanceof DateTime);
                            },
                            'message' => 'project_incorrect_date'
                        ],
                    ],
                ],
            ],
            [
                'name' => 'plannedDateStart',
                'required' => false,
                'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return ($value instanceof DateTime);
                            },
                            'message' => 'project_incorrect_date'
                        ],
                    ],
                ],
            ],
            [
                'name' => 'plannedDateEnd',
                'required' => false,
                'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return ($value instanceof DateTime);
                            },
                            'message' => 'project_incorrect_date'
                        ],
                    ],
                ],
            ],
            [
                'name' => 'realDateStart',
                'required' => false,
                'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return ($value instanceof DateTime);
                            },
                            'message' => 'project_incorrect_date'
                        ],
                    ],
                ],
            ],
            [
                'name' => 'realDateEnd',
                'required' => false,
                'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return ($value instanceof DateTime);
                            },
                            'message' => 'project_incorrect_date'
                        ],
                    ],
                ],
            ],
            [
                'name' => 'external',
                'required' => false,
                'allow_empty' => true,
                'filters' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {

                                if ((is_string($value) && (strtolower($value) == 'oui' || $value === '1' )) ||  $value === 1) {
                                    return true;
                                }
                                if ((is_string($value) && (strtolower($value) == 'non' || $value === '0')) || $value === 0 ) {
                                    return false;
                                }

                                return $value;
                            },
                        ],
                    ],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return  is_bool($value);
                            },
                            'message' => 'structure_field_type_boolean'
                        ],
                    ],
                ],
            ],
            [
                'name' => 'networkAccessible',
                'required' => false,
                'allow_empty' => true,
                'filters' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {

                                if ((is_string($value) && (strtolower($value) == 'oui' || $value === '1' )) ||  $value === 1) {
                                    return true;
                                }
                                if ((is_string($value) && (strtolower($value) == 'non' || $value === '0')) || $value === 0 ) {
                                    return false;
                                }

                                return $value;
                            },
                        ],
                    ],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return  is_bool($value);
                            },
                            'message' => 'structure_field_type_boolean'
                        ],
                    ],
                ],
            ],
            [
                'name' => 'ownership',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

								if ($context['external'] && !$value) {
									return false;
								}

								if (!$context['external'] && $value) {
									$value = null;
									return true;
								}

								$ownership = $entityManager
									->getRepository('Directory\Entity\Structure')
									->find($value);

                                return $ownership !== null;
                            },
                            'message' => 'project_field_ownership_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                return (is_numeric($value) ||
                                    (is_array($value) && isset($value['id']) && is_int($value['id'])) ||
                                    (is_object($value) && method_exists($value, 'getId') && is_numeric($value->getId()))) ;
                            },
                            'message' => 'project_field_id_expected',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'budgetStatus',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $status = $entityManager->getRepository('Budget\Entity\BudgetStatus')->find($value);

                                return $status !== null;
                            },
                            'message' => 'project_field_budgetStatus_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                return (is_numeric($value) ||
                                    (is_array($value) && isset($value['id']) && is_int($value['id'])) ||
                                    (is_object($value) && method_exists($value, 'getId') && is_numeric($value->getId()))) ;
                            },
                            'message' => 'project_field_id_expected',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'budgetCodes',
                'required' => false,
            ],
            [
                'name' => 'template',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $template = $entityManager->getRepository('Project\Entity\Template')->find($value);


                                return $template !== null;
                            },
                            'message' => 'project_field_template_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                return (is_numeric($value) ||
                                    (is_array($value) && isset($value['id']) && is_int($value['id'])) ||
                                    (is_object($value) && method_exists($value, 'getId') && is_numeric($value->getId()))) ;
                            },
                            'message' => 'project_field_id_expected',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'globalTimeTarget',
                'required' => false,
            ],
            [
                'name' => 'ageSynchronisation',
                'required' => false,
            ],
            [
                'name' => 'territories',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                return (is_array($value));
                            },
                            'message' => 'project_field_check_territories',
                        ],
                    ],
                ],
            ]
        ]);

		return $inputFilter;
	}

	public function __call($name, $arguments)
	{
		foreach (Expense::getTypes() as $type) {
			if (
				$name === 'getAmountExpense' . ucfirst($type) ||
				$name === 'getAmountHTExpense' . ucfirst($type) ||
				$name === 'getAmountExpenseArbo' . ucfirst($type) ||
				$name === 'getAmountHTExpenseArbo' . ucfirst($type)
			) {
				$method = str_replace(ucfirst($type), '', $name);
				array_unshift($arguments, $type);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

		foreach (Income::getTypes() as $type) {
			if (
				$name === 'getAmountIncome' . ucfirst($type) ||
				$name === 'getAmountHTIncome' . ucfirst($type) ||
				$name === 'getAmountIncomeArbo' . ucfirst($type) ||
				$name === 'getAmountHTIncomeArbo' . ucfirst($type) ||
				$name === 'getFinancerRepartitionIncome' . ucfirst($type) ||
				$name === 'getFinancerRepartitionIncomeArbo' . ucfirst($type) ||
				$name === 'getAmountIncomeAutofinancement' . ucfirst($type) ||
				$name === 'getAmountIncomeAutofinancementArbo' . ucfirst($type)
			) {
				$method = str_replace(ucfirst($type), '', $name);
				array_unshift($arguments, $type);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

		foreach (PosteExpense::getBalances() as $balance) {
			if (
				$name === 'getAmountPosteExpense' . ucfirst($balance) ||
				$name === 'getAmountHTPosteExpense' . ucfirst($balance) ||
				$name === 'getAmountPosteExpenseArbo' . ucfirst($balance) ||
				$name === 'getAmountHTPosteExpenseArbo' . ucfirst($balance)
			) {
				$method = str_replace(ucfirst($balance), '', $name);
				array_push($arguments, $balance);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

		foreach (PosteIncome::getBalances() as $balance) {
			if (
				$name === 'getAmountPosteIncome' . ucfirst($balance) ||
				$name === 'getAmountHTPosteIncome' . ucfirst($balance) ||
				$name === 'getAmountPosteIncomeArbo' . ucfirst($balance) ||
				$name === 'getAmountHTPosteIncomeArbo' . ucfirst($balance)
			) {
				$method = str_replace(ucfirst($balance), '', $name);
				array_push($arguments, $balance);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

		return null;
	}

	/**
	 * @param User   $user
	 * @param string $crudAction
	 * @param null   $owner
	 * @param null   $entityManager
	 *
	 * @return bool
	 */
	public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
	{
		$isMember = false;
		$right = false;

		foreach ($this->getMembers() as $member) {
			if ($member->getUser() == $user) {
				$isMember = true;
				$role = $member->getRole();

				switch ($crudAction) {
					case 'c':
					case 'r':
						$right = true;
						break;
					case 'u':
					case 'd':
						if ($role === Member::ROLE_MANAGER || $role === Member::ROLE_VALIDATOR) {
							$right = true;
						}
						break;
				}
			}
		}

		return $right;
	}

	/**
	 * @param QueryBuilder $queryBuilder
	 * @param User $user
	 * @param string $owner
	 */
	public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, $owner = '', $status = [])
	{
		$ownCondition =
			':ownerControl IN ( SELECT u.id FROM Project\Entity\Member m JOIN m.user u WHERE m.project = object)';

		if ($owner === 'own') {
			$queryBuilder->andWhere($ownCondition);
			$queryBuilder->setParameter('ownerControl', $user);
		}

		$rights = [];
		$publicationCondition = "(";
		$publication = false;
		$i = 0;
		foreach ($status as $right) {
			if (in_array($right, self::getPublicationStatutes())) {
				$publication = true;
				$publicationCondition .= 'object.publicationStatus = :right' . $i++ . ' OR ';
				$rights[] = $right;
			}
		}
		if ($publication) {
			$queryBuilder->andWhere(substr($publicationCondition, 0, -4) . ')');
			$i = 0;
			foreach ($rights as $right) {
				$queryBuilder->setParameter('right' . $i++, $right);
			}	
		}

	}

	/**
	 * @return array
	 */
	public static function getPublicationStatutes(): array
    {
		return [
			self::PUBLICATION_STATUS_DRAFT,
			self::PUBLICATION_STATUS_PENDING,
			self::PUBLICATION_STATUS_WAITING_VALIDATION,
			self::PUBLICATION_STATUS_VALIDATED,
			self::PUBLICATION_STATUS_PUBLISHED,
			self::PUBLICATION_STATUS_REJECTED,
			self::PUBLICATION_STATUS_ARCHIVED,
		];
	}


	/**
	 * Get the value of ageSynchronisation
	 *
	 * @return  AgeSynchronisation
	 */
	public function getAgeSynchronisation()
	{
		return $this->ageSynchronisation;
	}

	/**
	 * Set the value of ageSynchronisation
	 *
	 * @param  AgeSynchronisation  $ageSynchronisation
	 *
	 * @return  self
	 */
	public function setAgeSynchronisation(AgeSynchronisation $ageSynchronisation)
	{
		$this->ageSynchronisation = $ageSynchronisation;

		return $this;
	}

	/**
	 * @return boolean
	 */
	public function hasAgeSynchronisation()
	{
		return isset($this->ageSynchronisation);
	}


	/**
	 * Get the accounting code
	 * in "code" or "budget codes"
	 *
	 * @return string
	 */
	public function getAccountingCode()
	{
		if (isset($this->code)) {
			return $this->code;
		}
		if (isset($this->budgetCodes) && $this->budgetCodes->count() > 0) {
			return $this->budgetCodes->first()->getName();
		}
		return null;
	}
}
