<?php

namespace Campain\Entity;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Laminas\InputFilter\Factory;

/**
 * Class Measure
 *
 * @package Campain\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="campain_indicator")
 */
class Indicator extends BasicRestEntityAbstract
{

    const FREQUENCY_WEEKLY  = 'Hebomadaire';
	const FREQUENCY_MONTHLY  = 'Mensuelle';
	const FREQUENCY_TRIMONTHLY  = 'Trimestrielle';
	const FREQUENCY_BIMANNUALLY  = 'Semestrielle';
	const FREQUENCY_ANNUALLY  = 'Annuelle';
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * @var Campain
	 *
	 * @ORM\ManyToOne(targetEntity="Campain\Entity\Campain", inversedBy="indicators")
	 */
	protected $campain;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $entity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="`primary`")
     */
    protected $primary;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
    protected $targetDate;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $frequency;
    
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $frequencyValue;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User\Entity\User")
     */
    protected $users;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Indicator\Entity\Measure", mappedBy="campainIndicator", cascade={"remove"} )
     */
    protected $measures;

        /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Campain\Entity\Measure", mappedBy="indicator", cascade={"all"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $campainMeasures;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->measures = new ArrayCollection();
        $this->campainMeasures = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * @return Campain
     */
    public function getCampain()
    {
        return $this->campain;
    }

    /**
     * @param Campain $campain
     */
    public function setCampain($campain)
    {
        $this->campain = $campain;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @param int $primary
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
    }

    /**
     * @return \DateTime
     */
    public function getTargetDate()
    {
        return $this->targetDate;
    }

    /**
     * @param \DateTime $targetDate
     */
    public function setTargetDate($targetDate)
    {
        $this->targetDate = $targetDate;
    }

    /**
     * @return string
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param string $frequency
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    }
    
    /**
     * @return int
     */
    public function getFrequencyValue()
    {
        return $this->frequencyValue;
    }

    /**
     * @param int $frequencyValue
     */
    public function setFrequencyValue($frequencyValue)
    {
        $this->frequencyValue = $frequencyValue;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    public function addUsers($users)
    {
        foreach ($users as $user) {
            $this->users->add($user);
        }
    }

    public function removeUsers($users)
    {
        foreach ($users as $user) {
            $this->users->removeElement($user);
        }
    }

    /**
     * @return Measure[]|ArrayCollection
     */
    public function getMeasures()
    {
        return $this->measures;
    }

    /**
     * @param Measure[]|ArrayCollection $measures
     */
    public function setMeasures($measures)
    {
        $this->measures = $measures;
    }

    /**
     * @param ArrayCollection|Measure[] $measures
     */
    public function addMeasures(ArrayCollection $measures)
    {
        foreach ($measures as $measure) {
            $this->addMeasure($measure);
        }
    }

        /**
     * @param Measure $measure
     */
    public function addMeasure($measure)
    {
        $this->getMeasures()->add($measure);
    }

    /**
     * @param ArrayCollection|Measure[] $measures
     */
    public function removeMeasures(ArrayCollection $measures)
    {
        foreach ($measures as $measure) {
            $this->getMeasures()->removeElement($measure);
        }
    }


    /**
     * @return Measure[]|ArrayCollection
     */
    public function getCampainMeasures()
    {
        return $this->campainMeasures;
    }

    /**
     * @param Measure[]|ArrayCollection $campainMeasures
     */
    public function setCampainMeasures($campainMeasures)
    {
        $this->campainMeasures = $campainMeasures;
    }

    /**
     * @param ArrayCollection|Measure[] $campainMeasures
     */
    public function addCampainMeasures(ArrayCollection $campainMeasures)
    {
        foreach ($campainMeasures as $campainMeasure) {
            $this->getCampainMeasures()->add($campainMeasure);
        }
    }

    /**
     * @param ArrayCollection|Measure[] $campainMeasures
     */
    public function removeCamapinMeasures(ArrayCollection $campainMeasures)
    {
        foreach ($campainMeasures as $campainMeasure) {
            $this->getMeasures()->removeElement($campainMeasure);
        }
    }

	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilterFactory = new Factory();
		$inputFilter = $inputFilterFactory->createInputFilter([
			[
                'name'     => 'entity',
                'required' => true,
            ],
			[
                'name'     => 'primary',
                'required' => true,
            ],
            [
                'name'     => 'campain',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => array(
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $campaign = $entityManager->getRepository('Campain\Entity\Campain')->find($value);

                                return $campaign !== null;
                            },
                            'message' => 'query_field_campain_error_non_exists'
                        ),
                    ]
                ],
            ],
            [
                'name'     => 'frequency',
                'required' => false,
                'validators' =>  [[
                    'name'    => 'Callback',
                    'options' => [
                        'callback' => function ($value, $context) {
                            if(!isset($context['frequencyValue'])){
                                return false;
                            }
                            return true;
                        },
                        'message'  => 'indicator_field_frequency_shouldbe_with_frequencyValue_error',
                    ],
                ]]
            ],
			[
                'name'     => 'frequencyValue',
                'required' => false,
                'validators' =>  [[
                    'name'    => 'Callback',
                    'options' => [
                        'callback' => function ($value, $context) {
                            if(!isset($context['frequency'])){
                                return false;
                            }
                            return true;
                        },
                        'message'  => 'indicator_field_frequencyValue_shouldbe_with_frequency_error',
                    ],
                ]]
            ],
			[
                'name'     => 'targetDate',
                'required' => true,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ]
            ],
			[
                'name'     => 'users',
                'required' => true
            ]
		]);

		return $inputFilter;
	}

	/**
	 * @return array
	 */
	public static function getAllFrequencies()
	{
		return [
			self::FREQUENCY_WEEKLY,
			self::FREQUENCY_MONTHLY,
			self::FREQUENCY_TRIMONTHLY,
			self::FREQUENCY_BIMANNUALLY,
			self::FREQUENCY_ANNUALLY
		];
	}
}
