<?php

/**
 * Retrieve the APP_ENV value.
 * Defined in public/.htaccess file.
 */
$env = getenv('APP_ENV') ?: 'development';

/**
 * Retrieve Commands configuration that need be injected on Global Scope.
 */
$globalConfigurationModules = (require __DIR__ . '/../globals.config.php');
$factories                  = $globalConfigurationModules['factories'] ?? [];
$commands                   = $globalConfigurationModules['commands'] ?? [];

return [
    'service_manager' => [
        'factories' => [
            'Translator'                         => 'Laminas\Mvc\I18n\TranslatorFactory',
            'translator'                         => 'Laminas\Mvc\I18n\TranslatorFactory',
            ...$factories,
        ],
    ],
    'translator'      => [
        /**
         * The default locale.
         */
        'locale' => 'fr_FR'
    ],
    'view_manager'    => [
        'display_not_found_reason' => ($env === 'development'),
        'display_exceptions'       => ($env === 'development'),
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'strategies'               => [
            'ViewJsonStrategy'
        ]
    ],
    'laminas-cli'     => [
        'commands' => $commands,
    ],
];
