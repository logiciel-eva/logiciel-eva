<?php

namespace Time;

use Core\Module\AbstractModule;
use Time\Service\TimeService;
use Time\View\Helper\TimesheetFilters;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'timesheetFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    return new TimesheetFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                },
            ]
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'TimeService' => function (ServiceManager $serviceManager) {
                    $environment = $serviceManager->get('Environment');
                    return new TimeService(
                        $environment->getEntityManager(),
                        $serviceManager
                    );
                },
            ]
        ];
    }
}
