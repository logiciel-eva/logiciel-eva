(function(app) {

    app.factory('alertService', [
        '$http', '$q', 'restFactoryService',
        function alertService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/alert'));
        }
    ])

})(window.EVA.app);
