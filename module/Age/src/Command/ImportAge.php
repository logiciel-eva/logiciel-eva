<?php

namespace Age\Command;

use Age\Import\Importer;
use Bootstrap\Entity\Client;
use Core\Command\AbstractCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportAge extends AbstractCommand
{
    /**
     * Call : php vendor/bin/laminas alert:send-alerts
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Doctrine\ORM\Exception\NotSupported
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[START] Importing Age data...');

        $em                   = $this->getEmDefault();
        $entityManagerFactory = $this->getEmFactory();
        $clients = $em->getRepository('Bootstrap\Entity\Client')->findAll();

        /** @var Client $client */
        foreach ($clients as $client) {
            $modules = $client->getModules();
            if (isset($modules['age']) && $modules['age']['active']) {
                $output->writeln('[ ' . $client->getName() . ' ]');
                $clientEm = $entityManagerFactory->getEntityManager($client);

                $importer = new Importer($modules['age']['configuration'], $clientEm);
                $importer->run();
            }
        }

        $output->writeln('[END] Importing Age data...');

        return Command::SUCCESS;
    }
}
