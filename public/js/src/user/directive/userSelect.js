(function(app) {

    app.directive('userSelect', [
        'userService', 'selectFactoryService',
        function userSelect(userService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: userService,
                    apiParams: {
                        col: ['id', 'name', 'avatar']
                    }
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name'],
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' +
                                    '<img class="avatar" src="' + escape(data.avatar) + '" />' +
                                    '<span class="name">' + escape(data.name) + '</span>' +
                                '</div>';
                        },
                        item: function (data, escape) {
                            return '<div class="option">' +
                                    '<img class="avatar" src="' + escape(data.avatar) + '" />' +
                                    '<span class="name">' + escape(data.name) + '</span>' +
                                '</div>';
                        }
                    }
                }
            });
        }
    ])

})(window.EVA.app);
