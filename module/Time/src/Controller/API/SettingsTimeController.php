<?php
namespace Time\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

/**
 * Class SettingsTimeController
 * @package Time\Controller\API
 */
class SettingsTimeController extends BasicRestController
{
    protected $entityClass = 'Time\Entity\SettingsTime';
    protected $repository  = 'Time\Entity\SettingsTime';
    protected $entityChain = 'time:e:absence-type';

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
