<?php

namespace Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use User\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name = "project_age_synchronisation")
 */
class AgeSynchronisation implements JsonSerializable
{

    const AGE_OP_BUDGET_SEIZABLE = "budgetSeizable";        // opération saisissable au budget
    const AGE_OP_BUDGET_PRESENTED = "budgetPresented";        // opération présentée au budget
    const AGE_OP_EXEC_SEIZABLE = "executionSeizable";        // opération saisissable en exécution

    const AGE_SYNCHRO_STATUS_PREVENT = "prevent";           // prevent synchronization in AGE
    const AGE_SYNCHRO_STATUS_PENDING = "pending";           // not in AGE, waiting for synchronization
    const AGE_SYNCHRO_STATUS_SYNCHRONISED = "synchronised"; // synchronized in AGE


    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    protected $status;

    /**
     * @var boolean
     * 
     * @ORM\Column(type="boolean", options={"default":"0"}, nullable=false)
     */
    protected $budgetSeizable = false;

    /**
     * @var boolean
     * 
     * @ORM\Column(type="boolean", options={"default":"0"}, nullable=false)
     */
    protected $budgetPresented = false;

    /**
     * @var boolean
     * 
     * @ORM\Column(type="boolean", options={"default":"0"}, nullable=false)
     */
    protected $executionSeizable = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $synchronisedAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $synchronisedBy;

    /**
     * @var string
     * 
     * @ORM\Column(type="text", nullable=true)
     */
    protected $serviceRequest;

    /**
     * @var string
     * 
     * @ORM\Column(type="text", nullable=true)
     */
    protected $serviceResponse;

    /**
     * @var Project
     * 
     * @ORM\OneToOne(targetEntity="Project", inversedBy="ageSynchronisation")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     */
    protected $project;


    public function __construct()
    {
        $this->status = self::AGE_SYNCHRO_STATUS_PENDING;
    }



    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of status
     *
     * @return  string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @param  string  $status
     *
     * @return  self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of budgetSeizable
     *
     * @return  boolean
     */
    public function getBudgetSeizable()
    {
        return $this->budgetSeizable;
    }

    /**
     * Set the value of budgetSeizable
     *
     * @param  boolean  $budgetSeizable
     *
     * @return  self
     */
    public function setBudgetSeizable($budgetSeizable)
    {
        $this->budgetSeizable = $budgetSeizable;

        return $this;
    }

    /**
     * Get the value of budgetPresented
     *
     * @return  boolean
     */
    public function getBudgetPresented()
    {
        return $this->budgetPresented;
    }

    /**
     * Set the value of budgetPresented
     *
     * @param  boolean  $budgetPresented
     *
     * @return  self
     */
    public function setBudgetPresented($budgetPresented)
    {
        $this->budgetPresented = $budgetPresented;

        return $this;
    }

    /**
     * Get the value of executionSeizable
     *
     * @return  boolean
     */
    public function getExecutionSeizable()
    {
        return $this->executionSeizable;
    }

    /**
     * Set the value of executionSeizable
     *
     * @param  boolean  $executionSeizable
     *
     * @return  self
     */
    public function setExecutionSeizable($executionSeizable)
    {
        $this->executionSeizable = $executionSeizable;

        return $this;
    }

    /**
     * Get the value of synchronisedAt
     *
     * @return  \DateTime
     */
    public function getSynchronisedAt()
    {
        return $this->synchronisedAt;
    }

    /**
     * Set the value of synchronisedAt
     *
     * @param  \DateTime  $synchronisedAt
     *
     * @return  self
     */
    public function setSynchronisedAt($synchronisedAt)
    {
        $this->synchronisedAt = $synchronisedAt;

        return $this;
    }

    /**
     * Get the value of synchronisedBy
     *
     * @return  User
     */
    public function getSynchronisedBy()
    {
        return $this->synchronisedBy;
    }

    /**
     * Set the value of synchronisedBy
     *
     * @param  User  $synchronisedBy
     *
     * @return  self
     */
    public function setSynchronisedBy($synchronisedBy)
    {
        $this->synchronisedBy = $synchronisedBy;

        return $this;
    }

    /**
     * Get the value of project
     *
     * @return  Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set the value of project
     *
     * @param  Project  $project
     *
     * @return  self
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get the value of serviceResponse
     *
     * @return  string
     */
    public function getServiceResponse()
    {
        return $this->serviceResponse;
    }

    /**
     * Set the value of serviceResponse
     *
     * @param  string  $serviceResponse
     *
     * @return  self
     */
    public function setServiceResponse($serviceResponse)
    {
        $this->serviceResponse = $serviceResponse;

        return $this;
    }


    /**
     * @return boolean
     */
    public function estSaisissableAuBudget()
    {
        return $this->getBudgetSeizable();
    }

    /**
     * @return boolean
     */
    public function estPresenteeAuBudget()
    {
        return $this->getBudgetPresented();
    }

    /**
     * @return boolean
     */
    public function estSaisissableEnExecution()
    {
        return $this->getExecutionSeizable();
    }

    /**
     * @return boolean
     */
    public function isPendingStatus()
    {
        return self::AGE_SYNCHRO_STATUS_PENDING === $this->status;
    }

    /**
     * @return boolean
     */
    public function isSynchronizedStatus()
    {
        return self::AGE_SYNCHRO_STATUS_SYNCHRONISED === $this->status;
    }


    public static function getOperationFlags()
    {
        return [
            self::AGE_OP_BUDGET_SEIZABLE,
            self::AGE_OP_BUDGET_PRESENTED,
            self::AGE_OP_EXEC_SEIZABLE
        ];
    }


    /**
     * Get the value of serviceRequest
     *
     * @return  string
     */
    public function getServiceRequest()
    {
        return $this->serviceRequest;
    }

    /**
     * Set the value of serviceRequest
     *
     * @param  string  $serviceRequest
     *
     * @return  self
     */
    public function setServiceRequest($serviceRequest)
    {
        $this->serviceRequest = $serviceRequest;

        return $this;
    }

    public function jsonSerialize() : mixed
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'budgetSeizable' => $this->getBudgetSeizable(),
            'budgetPresented' => $this->getBudgetPresented(),
            'executionSeizable' => $this->getExecutionSeizable()
        ];
    }

    public function reset()
    {
        $this->status = self::AGE_SYNCHRO_STATUS_PENDING;
        $this->budgetPresented = false;
        $this->budgetSeizable = false;
        $this->executionSeizable = false;
        $this->serviceRequest = null;
        $this->serviceResponse = null;
        $this->synchronisedAt = null;
        $this->synchronisedBy = null;
    }

	/**
	 * @return array
	 */
    public static function getAgeSynchronisationStatutes()
	{
		return [
			self::AGE_SYNCHRO_STATUS_PREVENT,
			self::AGE_SYNCHRO_STATUS_PENDING,
			self::AGE_SYNCHRO_STATUS_SYNCHRONISED,
		];
	}
}
