(function(app) {

    app.controller('measureFormController', [
        'measureService', 'indicatorService', 'flashMessenger', 'translator', '$scope',
        function measureFormController(measureService, indicatorService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal = angular.element('#measureFormModal');
            var apiParams = {
                col: [
                    'id',
                    'period',
                    'type',
                    'start',
                    'date',
                    'value',
                    'campainMeasure.id',
                    'campainMeasure.supposedDate',
                    'fixedValue',
                    'territories',
                    'project.id',
                    'project.name',
                    'campain.id',
                    'campain.name',
                    'comment',
                    'source',
                    'transverse',
                    'client',
                    'master',
                    'slave',
                    'createdAt',
                    'updatedAt',
                    'indicator.name'
                ]
            };

            self.measure = {};
            self.from = {
                type: null,
                id: null
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id = id;

                if (self.from.type == 'project') {
                    apiParams.col.push('indicator.id');
                    apiParams.col.push('indicator.name');
                    apiParams.col.push('indicator.type');
                    apiParams.col.push('indicator.values');
                    apiParams.col.push('indicator.description');
                    apiParams.col.push('indicator.method');
                    apiParams.col.push('indicator.definition');
                }
            };

            self.saveMeasure = function saveMeasure() {

                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.measure.id;
                if(!isCreation){
                    $scope.$broadcast('save-attachment');
                }
                measureService.save(self.measure, apiParams).then(function(res) {
                    if (isCreation) {
                        $scope.$broadcast('save-attachment', res.object.id);
                        $scope.$parent.self.measures.push(res.object);
                    } else {
                        for (var i in $scope.$parent.self.measures) {
                            if ($scope.$parent.self.measures[i].id == res.object.id) {
                                $scope.$parent.self.measures[i] = res.object;
                                break;
                            }
                        }
                    }

                    
                    flashMessenger.success(translator('measure_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function(err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                        if (field == 'indicator'){
                            self.errors.fields['client'] = {"callbackValue" : translator('measure_field_indicator_error_non_exists_client')};
                        }
                    }
                    self.loading.save = false;
                })
            };

            $scope.$on('create-measure', function() {
                self.measure = {
                    _rights: {
                        update: true
                    }
                };

                if(self.from.type == 'project'){
                    self.measure.project = $scope.$parent.$parent.$parent.self[self.from.type].id;
                } else if(self.from.type == 'indicator'){
                    self.measure.indicator = $scope.$parent.$parent.self[self.from.type];
                }
                self.panelTitle = translator('measure_nav_form');
                $scope.$broadcast('edit-attachement');
                $modal.modal('show');
            });

            $scope.$on('edit-measure', function(event, measure) {
                self.editedMeasure = measure;
                self.measure = angular.copy(measure);
                self.panelTitle = self.measure.indicator.name + ' - ' + moment(self.measure.date, 'DD/MM/YYYY HH:mm').format('DD/MM/YYYY');
                $scope.$broadcast('edit-attachement', measure.id);
                $modal.modal('show');
            });
        }
    ]);

})(window.EVA.app);