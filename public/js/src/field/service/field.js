(function(app) {

    app.factory('fieldService', [
        'restFactoryService',
        function fieldService(restFactoryService) {
            return restFactoryService.create('/api/field');
        }
    ])

})(window.EVA.app);
