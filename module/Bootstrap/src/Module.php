<?php

namespace Bootstrap;

use Bootstrap\Environment\Helper\EnvironmentViewHelper;
use Bootstrap\Environment\Plugin\EntityManagerPlugin;
use Bootstrap\Environment\Plugin\EnvironmentPlugin;
use Bootstrap\ORM\DynamicEntityManager;
use Bootstrap\ORM\EntityManagerFactory;
use Core\Event\LogSubscriber;
use Core\Module\AbstractModule;
use Core\Utils\RequestUtils;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;

/**
 * Class Module
 *
 * The Bootstrap module is responsible to load and prepare the environment according to the current hostname.
 *
 * @package Bootstrap
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class Module extends AbstractModule
{
    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        $request     = $application->getRequest();

        if (!RequestUtils::isCommandRequest($request)) {
            $serviceManager = $application->getServiceManager();

            $environment = $serviceManager->get('Environment'); 
            $environment->bootstrap();

            $sharedManager = $application->getEventManager()->getSharedManager();
            $sharedManager->attach('Laminas\Mvc\Controller\AbstractActionController', 'dispatch', [$this, 'setTemplate']);
            $sharedManager->attach('Laminas\Mvc\Application', 'dispatch.error', [$this, 'setTemplate']);

            $environment->getEntityManager()->getEventManager()->addEventsubscriber(new LogSubscriber($serviceManager));

            \Laminas\Validator\AbstractValidator::setDefaultTranslator($serviceManager->get('translator'));
        }
    }

    /**
     * @param MvcEvent $event
     */
    public function setTemplate(MvcEvent $event)
    {
        $application    = $event->getApplication();
        $serviceManager = $application->getServiceManager();
        $environment    = $serviceManager->get('Environment');
        $viewModel      = $event->getViewModel();

        $viewModel->setTemplate($environment->getTemplate());
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return [
            'aliases' => [
                'DefaultEntityManager' => 'Doctrine\ORM\EntityManager'
            ],
            'factories' => [
                'Environment' => function (ServiceManager $serviceManager) {
                    $router               = $serviceManager->get('Router');
                    $request              = $serviceManager->get('Request');

                    $config               = $serviceManager->get('Config');
                    $entityManagerFactory = $serviceManager->get('EntityManagerFactory');

                    $moduleManager        = $serviceManager->get('ModuleManager');
                    $translator           = $serviceManager->get('Translator');

                    $env = 'Bootstrap\Environment\Client\ClientEnvironment';

                    return new $env($config, $request, $router, $entityManagerFactory, $moduleManager, $translator);
                },
                'EntityManagerFactory' => function (ServiceManager $serviceManager) {
                    return new EntityManagerFactory($serviceManager);
                },
                'DynamicEntityManager' => function (ServiceManager $serviceManager) {
                    return new DynamicEntityManager($serviceManager);
                }
            ]
        ];
    }

    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'entityManager' => function ($serviceManager) {
                    $environment = $serviceManager->get('Environment');
                    return new EntityManagerPlugin($environment);
                },
                'environment' => function ($serviceManager) {
                    $environment = $serviceManager->get('Environment');
                    return new EnvironmentViewHelper($environment);
                },
            ]
        ];
    }

    /**
     * @return array
     */
    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'entityManager' => function (ServiceManager $serviceManager) {
                    $environment = $serviceManager->get('Environment');
                    return new EntityManagerPlugin($environment);
                },
                'environment' => function (ServiceManager $serviceManager) {
                    $environment = $serviceManager->get('Environment');
                    return new EnvironmentPlugin($environment);
                },
            ]
        ];
    }
}
