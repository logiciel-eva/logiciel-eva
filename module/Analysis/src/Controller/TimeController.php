<?php

namespace Analysis\Controller;

use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use Core\Utils\Tree;
use Time\Entity\Timesheet;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Stdlib\Parameters;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class TimeController extends AbstractActionSLController
{
    public function onDispatch(MvcEvent $e)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $exports = $this->entityManager()->getRepository('Time\Entity\Export')->findAll();

        return new ViewModel([
            'exports' => $exports,
        ]);
    }

    public function timesheetsAction()
    {
        $isMaster          = $this->environment()->getClient()->isMaster();
        $filters           = $this->params()->fromQuery('filters', []);
        $export            = $this->params()->fromQuery('export', false);
        $projectsAggregate = $this->params()->fromQuery('projectAggregate', false) === 'true';

        $totals = [
            Timesheet::TYPE_DONE   => 0,
            Timesheet::TYPE_TARGET => 0,
            'left'   => 0,
        ];


        $res    = [
            'clients' => [],
        ];
 
        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];
        
            $totalsByClient =  $this->serviceLocator->get('timeAnalysisService')->getTotalsByClient($client, $filters);
            $totals[Timesheet::TYPE_DONE] += $totalsByClient[Timesheet::TYPE_DONE];  
            $totals[Timesheet::TYPE_TARGET] += $totalsByClient[Timesheet::TYPE_TARGET];               
        }

        $totals['left'] = $totals[Timesheet::TYPE_TARGET] - $totals[Timesheet::TYPE_DONE];


        if ($export != false) {
            $parent = $this->params()->fromQuery('parent', null);
            $group  = $this->params()->fromQuery('group', null);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster);
        }

        return new JsonModel([
            'res'    => $res,
            'totals' => $totals,
        ]);
    }

    public function projectsAction()
    {
        $isMaster          = $this->environment()->getClient()->isMaster();
        $filters           = $this->params()->fromQuery('filters', []);
        $export            = $this->params()->fromQuery('export', false);
        $projectsAggregate = $this->params()->fromQuery('projectAggregate', false) === 'true';
        $res               = [
            'clients'  => [],
            'projects' => [],
        ];

        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];     

            $res['projects'][$client->getId()] = $this->serviceLocator->get('timeAnalysisService')->getByProjects($client, $filters);
        }

        if ($export != false) {
            $parent = $this->params()->fromQuery('parent', null);
            $group  = $this->params()->fromQuery('group', null);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster);
        }

        return new JsonModel([
            'res' => $res,
        ]);
    }

    public function projectKeywordsAction()
    {
        $em                  = $this->entityManager();
        $controllerTimesheet = $this->serviceLocator->get('ControllerManager')->get('Time\Controller\API\Timesheet');
        $isMaster            = $this->environment()->getClient()->isMaster();
        $filters             = $this->params()->fromQuery('filters', []);
        $export              = $this->params()->fromQuery('export', false);
        $projectsAggregate   = $this->params()->fromQuery('projectAggregate', false) === 'true';
        $divideTimes         = $this->params()->fromQuery('divideTimes', false) === 'true';

        $res                 = [
            'clients'         => [],
            'projectKeywords' => [],
        ];

        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

    
        $keywordsMap = [];
        $groups = $this->allowedKeywordGroups('project', false);
        foreach ($groups as $group) {
            if ($group->getKeywords()->count() > 0) {
                foreach ($group->getKeywords() as $keyword) {
                    $keywordsMap[$keyword->getId()] = [
                        'id'      => $keyword->getId(),
                        'name'    => $keyword->getName(),
                        'parents' => []
                    ];

                    foreach ($keyword->getParents() as $parent) {
                        $keywordsMap[$keyword->getId()]['parents'][] = [
                            'id' => $parent->getId(),
                        ];
                    }
                }
            }
        }

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];



            $res['projectKeywords'] = $this->serviceLocator->get('timeAnalysisService')->getByProjectsKeywords($client, $filters, $divideTimes);
        }

        

        // Agrégation des mots clefs projets
        foreach ($res['projectKeywords'] as $group => $keywords) {
            foreach ($keywords as $keyword) {
                $this->recursiveAggregateMulti($res['projectKeywords'][$group], $keyword, $keyword['parents'], $keywordsMap);
            }
        }

        foreach ($res['projectKeywords'] as $i => $group) {
            foreach ($group as $j => $keyword) {
                $res['projectKeywords'][$i][$j]['done'] = round($keyword['done'], 1);
                $res['projectKeywords'][$i][$j]['target'] = round($keyword['target'], 1);
            }
        }

        if ($export != false) {
            $parent     = $this->params()->fromQuery('parent', null);
            $group      = $this->params()->fromQuery('group', null);
            $all        = $this->params()->fromQuery('all', false);
            $group_type = $this->params()->fromQuery('group_type', null);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster, $all, $group_type);
        }

        return new JsonModel([
            'res' => $res,
        ]);
    }


    public function usersAction()
    {
        $isMaster          = $this->environment()->getClient()->isMaster();
        $filters           = $this->params()->fromQuery('filters', []);
        $export            = $this->params()->fromQuery('export', false);
        $projectsAggregate = $this->params()->fromQuery('projectAggregate', false) === 'true';
        $res               = [
            'clients' => [],
            'users'   => [],
        ];

        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];

            $res['users'][$client->getId()] = $this->serviceLocator->get('timeAnalysisService')->getByUser($client, $filters);
        }

        if ($export != false) {
            $parent = $this->params()->fromQuery('parent', null);
            $group  = $this->params()->fromQuery('group', null);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster);
        }

        return new JsonModel([
            'res' => $res,
        ]);
    }

    public function monthsAction()
    {
        $isMaster          = $this->environment()->getClient()->isMaster();
        $filters           = $this->params()->fromQuery('filters', []);
        $export            = $this->params()->fromQuery('export', false);
        $projectsAggregate = $this->params()->fromQuery('projectAggregate', false) === 'true';
        $res               = [
            'clients' => [],
            'months'  => [],
        ];

        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];

            $res['months'][$client->getId()] = $this->serviceLocator->get('timeAnalysisService')->getDoneByMonth($client, $filters);
        }

        if ($export != false) {
            $parent = $this->params()->fromQuery('parent', null);
            $group  = $this->params()->fromQuery('group', null);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster);
        }

        return new JsonModel([
            'res' => $res,
        ]);
    }

    public function userMonthsAction()
    {
        $isMaster          = $this->environment()->getClient()->isMaster();
        $filters           = $this->params()->fromQuery('filters', []);
        $export            = $this->params()->fromQuery('export', false);
        $projectsAggregate = $this->params()->fromQuery('projectAggregate', false) === 'true';
        $res               = [
            'clients' => [],
            'months'  => [],
        ];

        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];

            $bypassTimesheet = false;
            $months          = [];
            $timesheetFilter = isset($filters['timesheet']) ? $filters['timesheet'] : [];

            // Filtres sur les Projects
            $tempProjects = $this->getProjectsFiltered($client);

            if ($tempProjects !== false) {
                if (count($tempProjects) == 0) {
                    $bypassTimesheet = true;
                } else {
                    $timesheetFilter['project'] = [
                        'op'  => 'eq',
                        'val' => [],
                    ];

                    foreach ($tempProjects as $i => $project) {
                        $timesheetFilter['project']['val'][] = $project['id'];
                    }
                }
            }

            // Filtres sur les Users
            $tempUsers = $this->getUsersFiltered($client);

            if ($tempUsers !== false) {
                if (count($tempUsers) == 0) {
                    $bypassTimesheet = true;
                } else {
                    $timesheetFilter['user.id'] = [
                        'op'  => 'eq',
                        'val' => [],
                    ];
                    foreach ($tempUsers as $i => $user) {
                        $timesheetFilter['user.id']['val'][] = $user['id'];
                    }
                }
            }


            // Filtres sur les Timesheets
            $timesheets = $this->getTimesheetsFiltered($timesheetFilter, $bypassTimesheet, $client);

            $allMonths = [];
            if ($timesheets) {
                foreach ($timesheets as $timesheet) {
                    if ($timesheet['type'] == Timesheet::TYPE_DONE) {
                        $start = \DateTime::createFromFormat('d/m/Y H:i', $timesheet['start']);

                        if ($start) {
                            if (!isset($usersMonths[$timesheet['user']['name']]['color'])) {
                                $usersMonths[$timesheet['user']['name']]['color'] = $timesheet['user']['color'];
                            }

                            if (!isset($usersMonths[$timesheet['user']['name']]['values'])) {
                                $usersMonths[$timesheet['user']['name']]['values'] = [];
                            }

                            if (!isset($usersMonths[$timesheet['user']['name']]['values'][$start->format('m/Y')])) {
                                $usersMonths[$timesheet['user']['name']]['values'][$start->format('m/Y')] = 0;
                            }

                            $usersMonths[$timesheet['user']['name']]['values'][$start->format('m/Y')] += $timesheet['hours'];
                        }

                        $allMonths[] = $start->format('m/Y');
                    }
                }

                foreach($usersMonths as &$userMonth) {
                  // Trie le tableau par date
                  foreach($allMonths as $month){
                    if (!isset($userMonth['values'][$month])) {
                        $userMonth['values'][$month] = 0;
                    }
                  }
                  uksort($userMonth['values'], function ($a, $b) {
                    return \DateTime::createFromFormat('m/Y', $a) > \DateTime::createFromFormat('m/Y', $b) ? 1 : -1;
                   });
                }
            }

            $res['usersMonths'][$client->getId()] = $usersMonths;
        }

        if ($export != false) {
            $parent = $this->params()->fromQuery('parent', null);
            $group  = $this->params()->fromQuery('group', null);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster);
        }

        return new JsonModel([
            'res' => $res,
        ]);
    }

    public function keywordsAction()
    {
        $controllerTimesheet = $this->serviceLocator->get('ControllerManager')->get('Time\Controller\API\Timesheet');
        $em                  = $this->entityManager();
        $isMaster            = $this->environment()->getClient()->isMaster();
        $filters             = $this->params()->fromQuery('filters', []);
        $export              = $this->params()->fromQuery('export', false);
        $projectsAggregate   = $this->params()->fromQuery('projectAggregate', false) === 'true';
        $res                 = [
            'clients'  => [],
            'keywords' => [],
        ];


        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        // Répartition par keywords
        $groups = $this->allowedKeywordGroups('timesheet', false);
    
        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];

            $res['keywords'] = $this->serviceLocator->get('timeAnalysisService')->getByKeywords($client, $filters);
        
            // Aggrégation des mots clés
            foreach ($res['keywords'] as $group => $keywords) {
                foreach ($keywords as $keyword) {
                    $this->recursiveAggregateMulti($res['keywords'][$group], $keyword, $keyword['parents']);
                }
            }    
        }

        if ($export != false) {
            $parent = $this->params()->fromQuery('parent', null);
            $group  = $this->params()->fromQuery('group', null);
            $all    = $this->params()->fromQuery('all', false);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster, $all);
        }

        return new JsonModel([
            'res' => $res,
        ]);
    }

    public function territoriesAction()
    {
        $em                = $this->entityManager();
        $isMaster          = $this->environment()->getClient()->isMaster();
        $filters           = $this->params()->fromQuery('filters', []);
        $export            = $this->params()->fromQuery('export', false);
        $projectsAggregate = $this->params()->fromQuery('projectAggregate', false) === 'true';
        $res               = [
            'clients'     => [],
            'territories' => [],
        ];

        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }

            $res['clients'][$client->getId()] = [
                'id'     => $client->getId(),
                'name'   => $client->getName(),
                'master' => $client->isMaster(),
            ];

       
         

    
            $territories = $em->getRepository('Map\Entity\Territory')->findAll();
            $territoriesMap = [];
            foreach ($territories as $territory) {
                $territoriesMap[$territory->getId()] = [
                    'id'      => $territory->getId(),
                    'name'    => $territory->getName(),
                    'done'    => 0,
                    'target'  => 0,
                    'parents' => [],
                ];

                foreach ($territory->getParents() as $parent) {
                    $territoriesMap[$territory->getId()]['parents'][] = [
                        'id' => $parent->getId(),
                    ];
                }
            }

            $res['territories'] = $this->serviceLocator->get('timeAnalysisService')->getByTerritories($client, $filters);

            // Agrégation des territoires
            foreach ($res['territories'] as $territory) {
                $this->recursiveAggregateMulti($res['territories'], $territory, $territory['parents'], $territoriesMap);
            }
        }

        if ($export != false) {
            $parent = $this->params()->fromQuery('parent', null);
            $group  = $this->params()->fromQuery('group', null);
            $this->export($res, $export, $projectsAggregate, $parent, $group, $isMaster);
        }

        return new JsonModel([
            'res' => $res,
        ]);
    }

    protected function getProjectsFiltered($client)
    {
        $isProjectActive = $this->serviceLocator->get('MyModuleManager')->isActive('project');
        $isMaster        = $this->environment()->getClient()->isMaster();

        $filters           = $this->params()->fromQuery('filters', []);
        $controllerProject = $this->serviceLocator->get('ControllerManager')->get('Project\Controller\API\Project');

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);

        if ($isProjectActive || $isMaster) {
            $routeMatch = new RouteMatch([]);
            $e          = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $controllerProject->setEvent($e);
            $controllerProject->setClient($client);

            if ($isMaster) {
                if (!isset($filters['project'])) {
                    $filters['project'] = [];
                }

                $filters['project']['networkAccessible'] = [
                    'op'  => 'eq',
                    'val' => 1,
                ];
            }

            if (isset($filters['project'])) {
                $request->setQuery(new Parameters([
                    'col'    => [
                        'id', 'code', 'name',
                        'parent.id',
                        'hierarchySup.id', 'hierarchySup.name', 'hierarchySup.code', 'hierarchySup.parent.id',
                        'keywords',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'id',
                            'order'   => 'asc',
                            'filters' => $filters['project'],
                        ],
                    ],
                ]));

                $tempProjects = $controllerProject->dispatch($request)->getVariable('rows');

                return $tempProjects;
            }

            return false;
        }
    }

    protected function getUsersFiltered($client)
    {
        $filters        = $this->params()->fromQuery('filters', []);
        $controllerUser = $this->serviceLocator->get('ControllerManager')->get('User\Controller\API\User');

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);

        $routeMatch = new RouteMatch([]);
        $e          = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $controllerUser->setEvent($e);
        $controllerUser->setClient($client);

        if (isset($filters['user'])) {
            $request->setQuery(new Parameters([
                'col'    => [
                    'id', 'name',
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'id',
                        'order'   => 'asc',
                        'filters' => $filters['user'],
                    ],
                ],
            ]));

            $tempUsers = $controllerUser->dispatch($request)->getVariable('rows');

            return $tempUsers;
        }

        return false;
    }

    protected function getTimesheetsFiltered($timesheetFilter, $bypassTimesheet, $client)
    {
        $controllerTimesheet = $this->serviceLocator->get('ControllerManager')->get('Time\Controller\API\Timesheet');

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);

        $routeMatch = new RouteMatch([]);
        $e          = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $controllerTimesheet->setEvent($e);
        $controllerTimesheet->setClient($client);

        /**
         * Check si un filtre des feuilles de temps a été appliqué
         */
        $parametersTimesheet = new Parameters([
            'col'    => [
                'id', 'name', 'start', 'end', 'type', 'keywords', 'territories',
                'hours', 'createdAt', 'updatedAt',
                'project.id', 'project.name', 'project.code',
                // IF aggregé
                'project.parent.id',
                'project.hierarchySup.id', 'project.hierarchySup.name', 'project.hierarchySup.parent.id',
                'project.networkAccessible', 'project.keywords',
                // ENDIF
                'user.id', 'user.name', 'user.color'
            ],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort'    => 'id',
                    'order'   => 'asc',
                    'filters' => $timesheetFilter,
                ],
            ],
        ]);


        $request->setQuery($parametersTimesheet);

        if (!$bypassTimesheet) {
            $timesheets = $controllerTimesheet->dispatch($request)->getVariable('rows');

            return $timesheets;
        }

        return false;
    }

    protected function recursiveAggregateMulti(&$objects, $object, $parents, $keywordsMap = null)
    {
        foreach ($parents as $i => $parent) {
            if (!isset($objects[$parent['id']]) ) {
                if (!isset($keywordsMap[$parent['id']])) {
                    continue;
                }

                $objects[$parent['id']] =   [
                    'id'      => $parent['id'],
                    'name'    => $keywordsMap[$parent['id']]['name'],
                    'done'    => 0,
                    'target'  => 0,
                    'parents' => [],
                ];
            }

            $objects[$parent['id']]['done']   += $object['done'];
            $objects[$parent['id']]['target'] += $object['target'];
          
            $this->recursiveAggregateMulti($objects, $object, $objects[$parent['id']]['parents']);
        }
    }

    protected function recursiveAggregate(&$objects, $object, $parent)
    {
        if ($parent && isset($objects[$parent['id']])) {
            $objects[$parent['id']]['done']   += $object['done'];
            $objects[$parent['id']]['target'] += $object['target'];
            $this->recursiveAggregate($objects, $object, $objects[$parent['id']]['parent']);
        }
    }

    protected function export($res, $type, $projectsAggregate, $parent, $group, $isMaster = false, $all = false, $group_type = null)
    {
        $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
        $translator          = $helperPluginManager->get('translate');

        if ($all) {
            switch ($type) {
                case 'keywords':
                    $datas = [];

                    foreach ($res[$type] as $i => $keywords) {
                        $res[$type][$i] = Tree::toNNTree($keywords);
                    }

                    $request = $this->apiRequest('Keyword\Controller\API\Group', Request::METHOD_GET, [
                        'col'    => ['id', 'name', 'type'],
                        'search' => [
                            'type' => 'list',
                            'data' => [
                                'sort'    => 'name',
                                'order'   => 'asc',
                                'filters' => [
                                    'entities' => [
                                        'op'  => 'sa_cnt',
                                        'val' => 'timesheet',
                                    ],
                                    'id'       => [
                                        'op'  => 'eq',
                                        'val' => $group,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $groups  = $request->dispatch()->getVariable('rows');

                    foreach ($groups as $groupe) {
                        $datas[]     = [
                            'id'       => $groupe['id'],
                            'name'     => $groupe['name'],
                            'keywords' => [],
                        ];
                        $index_group = count($datas) - 1;

                        $keywords = $res[$type][$datas[$index_group]['id']];
                        $keywords = Tree::toArrayWithLevels($keywords);

                        foreach ($keywords as $keyword) {
                            $datas[$index_group]['keywords'][] = [
                                'id'     => $keyword['id'],
                                'name'   => $keyword['name'],
                                'level'  => $keyword['level'],
                                'done'   => $keyword['done'],
                                'target' => $keyword['target'],
                            ];
                        }
                    }

                    $header = [];

                    // TODO: Implémenter la vue Master
                    /*if ($isMaster) {
                        $header[] = ucfirst($translator->__invoke('client'));
                    }*/

                    $header[] = ucfirst($translator->__invoke('keyword_entity'));
                    $header[] = ucfirst($translator->__invoke('timesheet_type_done'));
                    $header[] = ucfirst($translator->__invoke('timesheet_type_target'));
                    $header[] = ucfirst($translator->__invoke('timesheet_type_left'));

                    $rows = [];
                    foreach ($datas as $data) {
                        foreach ($data['keywords'] as $keyword) {
                            $row = [];
                            // TODO: Implémenter la vue Master
                            /*if ($isMaster) {
                            }*/
                            $row[] = str_repeat(' ', $keyword['level'] * 4) . $keyword['name'];
                            $row[] = $keyword['done'];
                            $row[] = $keyword['target'];
                            $row[] = $keyword['target'] - $keyword['done'];

                            $rows[] = $row;
                        }
                    }

                    $array = array_merge([$header], $rows);
                    ExcelExporter::download($array);
                    break;
                case 'projectKeywords':
                    $datas = [];

                    foreach ($res[$type] as $i => $projectKeywords) {
                        $res[$type][$i] = Tree::toNNTree($projectKeywords);
                    }

                    $request = $this->apiRequest('Keyword\Controller\API\Group', Request::METHOD_GET, [
                        'col'    => ['id', 'name', 'type'],
                        'search' => [
                            'type' => 'list',
                            'data' => [
                                'sort'    => 'name',
                                'order'   => 'asc',
                                'filters' => [
                                    'entities' => [
                                        'op'  => 'sa_cnt',
                                        'val' => 'project',
                                    ],
                                    'id'       => [
                                        'op'  => 'eq',
                                        'val' => $group,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $groups  = $request->dispatch()->getVariable('rows');

                    foreach ($groups as $groupe) {
                        $keepGroup = true;
                        if ($group_type) {
                            if ($group_type !== $groupe['type']) {
                                $keepGroup = false;
                            }
                        }

                        if ($keepGroup) {
                            $datas[]     = [
                                'id'              => $groupe['id'],
                                'name'            => $groupe['name'],
                                'projectKeywords' => [],
                            ];
                            $index_group = count($datas) - 1;

                            $projectKeywords = $res[$type][$datas[$index_group]['id']];
                            $projectKeywords = Tree::toArrayWithLevels($projectKeywords);

                            foreach ($projectKeywords as $projectKeyword) {
                                $datas[$index_group]['projectKeywords'][] = [
                                    'id'     => $projectKeyword['id'],
                                    'name'   => $projectKeyword['name'],
                                    'level'  => $projectKeyword['level'],
                                    'done'   => $projectKeyword['done'],
                                    'target' => $projectKeyword['target'],
                                ];
                            }
                        }
                    }

                    $header = [];

                    // TODO: Implémenter la vue Master
                    /*if ($isMaster) {
                        $header[] = ucfirst($translator->__invoke('client'));
                    }*/

                    $header[] = ucfirst($translator->__invoke('keyword_entity'));
                    $header[] = ucfirst($translator->__invoke('timesheet_type_done'));
                    $header[] = ucfirst($translator->__invoke('timesheet_type_target'));
                    $header[] = ucfirst($translator->__invoke('timesheet_type_left'));
                    $header[] = '%';

                    $total = 0;
                    foreach ($datas as $data) {
                        foreach ($data['projectKeywords'] as $projectKeyword) {
                            if ($projectKeyword['level'] == 0) {
                                $total += $projectKeyword['done'];
                            }
                        }
                    }

                    $rows = [];
                    foreach ($datas as $data) {
                        foreach ($data['projectKeywords'] as $projectKeyword) {
                            $row = [];
                            // TODO: Implémenter la vue Master
                            /*if ($isMaster) {
                            }*/
                            $row[] = str_repeat(' ', $projectKeyword['level'] * 4) . $projectKeyword['name'];
                            $row[] = $projectKeyword['done'];
                            $row[] = $projectKeyword['target'];
                            $row[] = $projectKeyword['target'] - $projectKeyword['done'];
                            $row[] = round(($projectKeyword['done'] / $total) * 100, 1);

                            $rows[] = $row;
                        }
                    }

                    $array = array_merge([$header], $rows);
                    ExcelExporter::download($array);
                    break;
            }
        }

        if ($type == 'months') {
            if ($isMaster) {
                $header[] = ucfirst($translator->__invoke('client'));
            }

            $header[] = ucfirst($translator->__invoke('month'));
            $header[] = $translator->__invoke('timesheet_type_done') . ' (' . $translator->__invoke('hours') . ')';

            $array  = [$header];
            $months = [];

            foreach ($res[$type] as $idClient => $rows) {
                if ($isMaster) {
                    $array[] = [$res['clients'][$idClient]['name']];
                }

                $months = $rows;
            }

            foreach ($months as $month => $time) {
                $objectArray = [];

                if ($isMaster) {
                    $objectArray[] = '';
                }

                $objectArray[] = $month;
                $objectArray[] = floatval($time);

                $array[] = $objectArray;
            }

            ExcelExporter::download($array);
        } else if (isset($res[$type])) {
            $typeName = $type === 'projects' ? 'project' : (($type === 'projectKeywords' || $type === 'keywords') ? 'keyword' : (($type === 'users') ? 'user' : 'territory'));

            $header = [];

            if ($isMaster && $type !== 'projectKeywords' && $type !== 'territories') {
                $header[] = ucfirst($translator->__invoke('client'));
            }

            if ($type === 'projects') {
                $header[] = ucfirst($translator->__invoke('project_field_code'));
            }

            $header[] = ucfirst($translator->__invoke($typeName . '_entity'));
            $header[] = $translator->__invoke('timesheet_type_done');
            $header[] = $translator->__invoke('timesheet_type_target');
            $header[] = $translator->__invoke('timesheet_type_left');

            $array = [$header];

            if ($type === 'territories') {
                $res[$type] = [$res[$type]];
            }

            foreach ($res[$type] as $idClient => $rows) {
                if ($isMaster && $type !== 'projectKeywords' && $type !== 'territories') {
                    $array[] = [$res['clients'][$idClient]['name']];
                }

                $objects = [];
                if ($type == 'projects' && $projectsAggregate) {
                    foreach ($rows as $row) {
                        if ($row['parent'] == null && !$parent) {
                            $objects[] = $row;
                        } else if ($row['parent']['id'] == $parent) {
                            $objects[] = $row;
                        }
                    }
                } elseif ($type == 'projectKeywords' || $type == 'keywords') {
                    if ($idClient == $group) {
                        $objects = [];
                        foreach ($rows as $row) {
                            if (($row['parents'] == null || sizeOf($row['parents']) == 0) && !$parent) {
                                $objects[] = $row;
                            } else {
                                foreach ($row['parents'] as $_parent) {
                                    if ($_parent['id'] == $parent) {
                                        $objects[] = $row;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $objects = $rows;
                }

                foreach ($objects as $object) {
                    $objectArray = [];

                    if ($isMaster && $type !== 'projectKeywords' && $type !== 'territories') {
                        $objectArray[] = '';
                    }

                    if ($type === 'projects') {
                        $objectArray[] = $object['code'];
                    }
                    if (isset($object['hierarchySup'])) {
                        $objectArray[] = str_repeat('-', count($object['hierarchySup'])) . $object['name'];
                    } else {
                        $objectArray[] = $object['name'];
                    }

                    $objectArray[] = floatval($object[Timesheet::TYPE_DONE]);
                    $objectArray[] = floatval($object[Timesheet::TYPE_TARGET]);
                    $objectArray[] = floatval($object[Timesheet::TYPE_TARGET] - $object[Timesheet::TYPE_DONE]);

                    $array[] = $objectArray;
                }
            }

            ExcelExporter::download($array);
        }
        die();
    }
}
