<?php

namespace Core\Controller;

abstract class BasicRestSyncController extends BasicRestController
{
    public function create($data)
    {
        $response = parent::create($data);

        $currentClient = $this->environment()->getClient();
        if ($currentClient->hasNetwork() && $currentClient->isMaster() && $response->getVariable('success') === true) {
            $data['master'] = $response->getVariable('object')['id'];
            foreach ($currentClient->getNetwork()->getClients() as $client) {
                if (!$client->isMaster()) {
                    $this->setClient($client);
                    parent::create($data);
                }
            }
            $this->setClient($currentClient);
        }

        return $response;
    }

    public function update($id, $data)
    {
        $response = parent::update($id, $data);
        $currentClient = $this->environment()->getClient();

        if ($currentClient->hasNetwork() && $currentClient->isMaster() && $response->getVariable('success') === true) {
            $data['master'] = $id;
            foreach ($currentClient->getNetwork()->getClients() as $client) {
                if (!$client->isMaster()) {
                    $this->setClient($client);
                    $masterEntity = $this->getEntityManager()->getRepository($this->repository)->findOneByMaster($id);
                    if ($masterEntity === null) {
                        parent::create($data);
                    } else {
                        if (isset($data['id'])) {
                            $data['id'] = $masterEntity->getId();
                        }

                        parent::update($masterEntity->getId(), $data);
                    }
                }
            }
            $this->setClient($currentClient);
        }

        return $response;
    }

    public function delete($id)
    {
        $response = parent::delete($id);

        $currentClient = $this->environment()->getClient();
        if ($currentClient->hasNetwork() && $currentClient->isMaster() && $response->getVariable('success') === true) {
            foreach ($currentClient->getNetwork()->getClients() as $client) {
                if (!$client->isMaster()) {
                    $this->setClient($client);

                    $masterEntity = $this->getEntityManager()->getRepository($this->repository)->findOneByMaster($id);
                    if ($masterEntity !== null) {
                        parent::delete($masterEntity->getId());
                    }
                }
            }
            $this->setClient($currentClient);
        }

        return $response;
    }
}
