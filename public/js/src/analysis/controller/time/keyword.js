(function (app) {

    app.controller('keywordController', [
        '$timeout', '$scope', '$http', 'treeBuilderService', 'groupService',
        function keywordController($timeout, $scope, $http, treeBuilderService, groupService) {
            var self         = this;
            var first        = true;
            var shouldReload = false;

            self.datas = [];

            self.sunburst = {
                api    : {},
                options: {
                    chart: {
                        groupColorByParent: false,
                        type    : 'sunburstChart',
                        height  : 400,
                        color   : d3.scale.category20(),
                        duration: 500,
                        mode    : 'size',
                        tooltip : {
                            contentGenerator: function (obj) {
                                return '' +
                                    '<table>' +
                                    '    <tr>' +
                                    '        <td class="legend-color-guide">' +
                                    '            <div style="background-color:' + obj.color + '"></div>' +
                                    '        </td>' +
                                    '        <td class="key">' + obj.data.name + '</td>' +
                                    '        <td class="value">' + obj.data.realSize + '</td>' +
                                    '    </tr>' +
                                    '</table>';
                            }
                        },
                        key     : function (d) {
                            return d.name + d.i;
                        }
                    }
                }
            };

            $scope.$on('switchTab', function (event, data) {
                if (data.active === 'keyword' && shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                }
            });

            $scope.$on('applyFilters', function (event, data) {
                if (data.active === 'keyword' && data.shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                } else if (data.shouldReload) {
                    shouldReload = true;
                }
            });

            self.applyFilters = function (data) {
                self.loadingOn();
                self.i = 0;
                $http({
                    url   : '/analysis/time/keywords?' + $.param({filters: data.filters}),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;

                    for (var i in self.datas.res.keywords) {
                        self.datas.res.keywords[i] = treeBuilderService.toNNTree(self.datas.res.keywords[i]);
                    }

                    if (first) {
                        groupService.findAll({
                            col   : ['id', 'name', 'type'],
                            search: {
                                type: 'list',
                                data: {
                                    sort   : 'name',
                                    order  : 'asc',
                                    filters: {
                                        entities: {
                                            op : 'sa_cnt',
                                            val: 'timesheet'
                                        }
                                    }
                                }
                            }
                        }).then(function (res) {
                            self.keywords = res.rows;
                            if (self.keywords.length > 0) {
                                self.loadAll();
                                self.loadSunburstChart(res.rows[0]);
                            }
                            self.loadingOff();
                        }, function () {});

                        first = false;
                    } else {
                        self.loadAll();
                        self.loadSunburstChart(self.group);
                        self.loadingOff();
                    }
                }, function () {});
            };

            self.loadAll = function () {
                self.loadingOn();
                self.all = [];
                for (var i in self.keywords) {
                    var group = self.keywords[i];
                    self.all.push({
                        id      : group.id,
                        name    : group.name,
                        totalDone: 0,
                        totalTarget: 0,
                        keywords: []
                    });
                    var index_group = self.all.length - 1;

                    var keywords = self.datas.res.keywords[group.id];
                    keywords     = treeBuilderService.toArrayWithlevels(keywords);

                    for (var j in keywords) {
                        var keyword = keywords[j];
                        self.all[index_group].keywords.push({
                            id    : keyword.id,
                            name  : keyword.name,
                            level : keyword.level,
                            done  : keyword.done,
                            target: keyword.target
                        });
                        self.all[index_group].totalDone += keyword.done;
                        self.all[index_group].totalTarget += keyword.target;
                    }
                }
                for (var i in self.all) {
                    for(var j in self.all[i].keywords){
                        self.all[i].keywords[j].percentDone = self.all[i].keywords[j].done / self.all[i].totalDone * 100 || 0;
                        self.all[i].keywords[j].percentTarget = self.all[i].keywords[j].target / self.all[i].totalTarget * 100 || 0;
                    }
                }
                self.loadingOff();
            };

            self.loadSunburstChart = function (group) {
                if (group) {
                    self.loadingOn();
                    self.group         = group;
                    self.sunburst.data = [{
                        name    : group.name,
                        group_id: group.id,
                        realSize: 0,
                        children: [],
                        i       : self.i
                    }];

                    self.i++;

                    for (var i in self.datas.res.keywords[group.id]) {
                        var keyword = self.datas.res.keywords[group.id][i];

                        self.sunburst.data[0].children.push(loadSunburstData(keyword, [])[0]);
                        self.sunburst.data[0].realSize += keyword.done;
                    }

                    self.sunburst.data[0].children = removeKeywordSize0(self.sunburst.data[0].children);
                }
                self.loadingOff();
            };

            function loadSunburstData(keyword, res) {
                res.push({
                    name    : keyword.name,
                    size    : keyword.done,
                    children: [],
                    realSize: keyword.done,
                    i       : self.i
                });

                self.i++;
                var last_id = res.length - 1;

                if (keyword.children.length > 0) {
                    for (var i in keyword.children) {
                        loadSunburstData(keyword.children[i], res[last_id].children);
                    }
                }

                return res;
            }

            function removeKeywordSize0(keywords) {
                var i = keywords.length;

                while (i--) {
                    if (keywords[i].realSize === 0) {
                        keywords.splice(keywords.indexOf(keywords[i]), 1);
                    } else if (keywords[i].children.length > 0) {
                        keywords[i].children = removeKeywordSize0(keywords[i].children);
                    }
                }

                return keywords;
            }

            self.groupFilter = function (item) {
                return item.id === self.sunburst.data[0].group_id;
            };

            self.export = function (type, parent, group, all) {
                $scope.$emit('export', {type: type, parent: parent, group: group, all: all});
            };

            self.loadingOn = function () {
                self.loading = true;
                $scope.$emit('loadingOn');
            };

            self.loadingOff = function () {
                self.loading = false;
                $scope.$emit('loadingOff');
            };
        }
    ]);

})(window.EVA.app);

