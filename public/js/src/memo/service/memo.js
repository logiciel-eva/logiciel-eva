(function(app) {

    app.factory('memoService', [
        'restFactoryService',
        function noteService(restFactoryService) {
            return restFactoryService.create('/api/memo');
        }
    ])

})(window.EVA.app);
