<?php

return [
	'module' => [
		'user' => [
			'environments' => ['client'],

			'active' => true,
			'required' => true,

			'acl' => [
				'entities' => [
					[
						'name' => 'user',
						'class' => 'User\Entity\User',
						'keywords' => true,
						'owner' => ['own'],
						'properties' => ['role'],
					],
					[
						'name' => 'role',
						'class' => 'User\Entity\Role',
						'owner' => [],
					],
				],
			],

			'configuration' => [
				
			],
		],
	],
	'view_manager' => [
		'template_path_stack' => [__DIR__ . '/../view'],
	],
	'translator' => [
		'translation_file_patterns' => [
			[
				'type' => 'phparray',
				'base_dir' => __DIR__ . '/../language',
				'pattern' => '%s.php',
			],
		],
	],
];
