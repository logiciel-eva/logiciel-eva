<?php

namespace Map;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'map' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/map',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'territory' => [
                        'type' => 'Literal',
                        'options' => [
                            'route'    => '/territory',
                            'defaults' => [
                                'controller' => 'Map\Controller\Territory',
                                'action'     => 'index'
                            ]
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/form[/:id]',
                                    'defaults' => [
                                        'controller' => 'Map\Controller\Territory',
                                        'action'     => 'form',
                                    ],
                                ],
                            ],
                            'import' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/import',
                                    'defaults' => [
                                        'controller' => 'Map\Controller\Territory',
                                        'action'     => 'import',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
            ],
            'api' => [
                'child_routes'  => [
                    'map' => [
                        'type' => 'Literal',
                        'options' => [
                            'route'      => '/map',
                        ],
                        'may_terminate' => false,
                        'child_routes'  => [
                            'territory' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/territory[/:id]',
                                    'defaults' => [
                                        'controller' => 'Map\Controller\API\Territory'
                                    ]
                                ],
                                'may_terminate' => true,
                                'child_routes'  => [
                                    'form' => [
                                        'type'    => 'Segment',
                                        'options' => [
                                            'route'    => '/fusion',
                                            'defaults' => [
                                                'controller' => 'Map\Controller\API\Territory',
                                                'action'     => 'fusion',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            'association' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/association[/:id]',
                                    'defaults' => [
                                        'controller' => 'Map\Controller\API\Association'
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\TerritoryController::class       => ServiceLocatorFactory::class,
            Controller\API\TerritoryController::class   => ServiceLocatorFactory::class,
            Controller\API\AssociationController::class => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Map\Controller\Territory'       => Controller\TerritoryController::class,
            'Map\Controller\API\Territory'   => Controller\API\TerritoryController::class,
            'Map\Controller\API\Association' => Controller\API\AssociationController::class
        ]
    ]
];
