<?php

namespace Project;

use Age\Service\AgeSynchronisationService;
use Bootstrap\Environment\Client\ClientEnvironment;
use Budget\View\Helper\ExpenseTypes;
use Core\Module\AbstractModule;
use Core\Utils\RequestUtils;
use Project\Event\LevelSubscriber;
use Project\Event\ParentSubscriber;
use Project\Event\RateSubscriber;
use Project\Service\BudgetCustomExportService;
use Project\Service\ProjectUpdateService;
use Project\Service\ProjectFieldDescriptorService;
use Project\View\Helper\ProjectFilters;
use Laminas\I18n\Translator\TextDomain;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
	/**
	 * @param MvcEvent $event
	 */
	public function onBootstrap(MvcEvent $event)
	{
		$application = $event->getApplication();
		$request = $application->getRequest();

		if (!RequestUtils::isCommandRequest($request)) {
			$serviceManager = $application->getServiceManager();
			$environment = $serviceManager->get('Environment');
			$environment
				->getEntityManager()
				->getEventManager()
				->addEventsubscriber(new ParentSubscriber($serviceManager));
			$environment
				->getEntityManager()
				->getEventManager()
				->addEventsubscriber(new LevelSubscriber($serviceManager));
			$environment
				->getEntityManager()
				->getEventManager()
				->addEventsubscriber(new RateSubscriber($serviceManager));

			// We add level translations dynamically
			if ($environment instanceof ClientEnvironment) {
				$configuration = $serviceManager->get('ClientPrivateConfiguration');
				$modules = $configuration->getModules();
				if (isset($modules) && array_key_exists('project', $modules) && array_key_exists('levels', $modules['project'])) {
					$translator = $serviceManager->get('Translator');
					$translations = [];
					foreach ($configuration->getModules()['project']['levels'] as $level => $name) {
						$translations['project_level_' . $level] = $name['name'];
					}

					$textDomain = new TextDomain($translations);
					$translator->getAllMessages()->merge($textDomain);
				}
			}
		}
	}

	/**
     * @return array
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'AgeSynchronisationService' => function (ServiceManager $serviceManager) {
									$environment = $serviceManager->get('Environment');
														return new AgeSynchronisationService(
										$environment->getClient(),
										$environment->getEntityManager(),
										$serviceManager->get('AuthStorage')
									);
								},
								'ProjectUpdateService' => function (ServiceManager $serviceManager) {
									$environment = $serviceManager->get('Environment');
									$em = $environment->getEntityManager();
									$projectUpdateRepository = $em->getRepository('Project\Entity\ProjectUpdate');
									$authStorage = $serviceManager->get('AuthStorage');

									return new ProjectUpdateService(
										$em,
										$projectUpdateRepository,
										$authStorage
									);
								},
								'ProjectFieldDescriptorService' => function (ServiceManager $serviceManager) {
									$environment = $serviceManager->get('Environment');
									$em = $environment->getEntityManager();
									$authStorage = $serviceManager->get('AuthStorage');

									return new ProjectFieldDescriptorService(
										$em,
										$authStorage
									);
								},
								'BudgetCustomExportService' => function(ServiceManager $serviceManager) {
									$environment = $serviceManager->get('Environment');
									$em = $environment->getEntityManager();
									return new BudgetCustomExportService($em);
								}
				
            ]
        ];
    }

	/**
	 * @return array
	 */
	public function getViewHelperConfig()
	{
		return [
			'factories' => [
				'projectFilters' => function (ServiceManager $sm) {
					$hpm = $sm->get('ViewHelperManager');
					$moduleManager = $sm->get('MyModuleManager');
					$configuration = $sm->get('ClientPrivateConfiguration');
					$client = $sm->get('Environment')->getClient();

					$translateViewHelper = $hpm->get('translate');
					$entityManager = $hpm->get('entityManager')->__invoke();

					$expenseTypes = (new ExpenseTypes($sm->get('expenseTypes')))->__invoke();
					$incomeTypes = (new ExpenseTypes($sm->get('incomeTypes')))->__invoke();

					$allowedKeywordGroupsViewHelper = $hpm->get('allowedKeywordGroups');
					$allowedFieldGroupsViewHelper = $hpm->get('allowedFieldGroups');

					return new ProjectFilters(
						$translateViewHelper,
						$moduleManager,
						$entityManager,
						$configuration,
						$client,
						$expenseTypes,
						$incomeTypes,
						$allowedKeywordGroupsViewHelper,
						$allowedFieldGroupsViewHelper
					);
				},
			],
		];
	}
}
