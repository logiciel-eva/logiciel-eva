<?php

namespace Advancement\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Project;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Attachment
 *
 * @package Attachment\Attachment
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="advancement_advancement")
 */
class Advancement extends BasicRestEntityAbstract
{
    const TYPE_DONE   = 'done';
    const TYPE_TARGET = 'target';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project\Entity\Project")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $project;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function isYear($year = null)
    {
        if ($year !== null) {
            return $this->getDate()->format('Y') == $year;
        }

        return true;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'type',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getTypes()
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'date',
                'required' => true,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'     => 'project',
                'required' => false,
                'filters'  => [
                    ['name' => 'Core\Filter\IdFilter'],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $project = $entityManager->getRepository('Project\Entity\Project')->find($value);

                                return $project !== null;
                            },
                            'message' => 'advancement_field_project_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'value',
                'required' => true,
                'filters' => [
                    ['name' => 'NumberParse'],
                ],
            ],
            [
                'name'     => 'name',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'description',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_DONE,
            self::TYPE_TARGET
        ];
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        if ($this->getProject()) {
            return $this->getProject()->ownerControl($user, $crudAction, $owner, $entityManager);
        }

        return false;
    }
}
