<?php
namespace Time\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ExportController
 * @package Time\Controller\API
 */
class ExportController extends BasicRestController
{
    protected $entityClass = 'Time\Entity\Export';
    protected $repository  = 'Time\Entity\Export';
    protected $entityChain = 'time:e:export';

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
