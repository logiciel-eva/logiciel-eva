<?php

use Core\Command\CleanCache;
use Core\Command\CreateModule;
use Core\Command\GenerateClassmap;
use Core\Command\GenerateLanguage;
use Core\Command\GenerateLanguageFactory;
use Core\Command\GenerateProxies;
use Core\Command\GenerateProxiesFactory;
use Core\Command\RunCommand;
use Core\Command\RunCommandFactory;
use Core\Command\UpdateDatabases;
use Core\Command\UpdateDatabasesFactory;

/**
 * Inject configuations on global scope.
 */
return [
    'service_manager' => [
        'factories' => [
            UpdateDatabases::class  => UpdateDatabasesFactory::class,
            GenerateProxies::class  => GenerateProxiesFactory::class,
            GenerateLanguage::class => GenerateLanguageFactory::class,
            RunCommand::class       => RunCommandFactory::class,
        ],
    ],
    'laminas-cli'     => [
        'commands' => [

            // CONTEXTE : RUN ACTION
            'core:clean-cache'       => CleanCache::class,
            'core:update-dbs'        => UpdateDatabases::class,
            'core:generate-proxies'  => GenerateProxies::class,
            'core:generate-language' => GenerateLanguage::class,
            'core:generate-classmap' => GenerateClassmap::class,

            // CONTEXTE : BUILD ACTION (for dev.)
            'core:create-module'     => CreateModule::class,

            // Generic Command Helper
            'core:run-command'       => RunCommand::class,

        ],
    ],
];
