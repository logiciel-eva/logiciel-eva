<?php

namespace Time\Controller;

use Core\Controller\AbstractActionSLController;
use Core\Model\CurlAuthentication;
use Core\Utils\CurlUtils;
use Doctrine\ORM\EntityManager;
use Field\Entity\Field;
use Project\Entity\Project;
use Time\Entity\Synchronisation;
use Time\Service\TimeService;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class SynchronisationController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('time:e:synchronisation:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('time:e:synchronisation', $id)) {
            return $this->notFoundAction();
        }

        $synchronisation = $this->entityManager()->getRepository('Time\Entity\Synchronisation')->find($id);

        if (
            $synchronisation &&
            !$this->acl()->ownerControl('time:e:synchronisation:u', $synchronisation) &&
            !$this->acl()->ownerControl('time:e:synchronisation:r', $synchronisation)
        ) {
            return $this->notFoundAction();
        }

        $customs = [];
        $groups  = $this->entityManager()->getRepository('Field\Entity\Group')->findBy([
            'entity' => Project::class,
        ]);

        foreach ($groups as $group) {
            foreach ($group->getFields() as $field) {
                if ($field->getType() == Field::TYPE_TEXT) {
                    $customs[] = $field;
                }
            }
        }
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        return new ViewModel([
            'syncTypes'       => Synchronisation::getTypes(),
            'synchronisation' => $synchronisation,
            'customs'         => $customs,
            'configs'         => Synchronisation::getConfigOptions(),
            'timeZones'       => $tzlist
        ]);
    }

    public function importAction()
    {
        $rows  = $this->params()->fromQuery('rows', false);
        $start = $this->params()->fromQuery('start', false);
        $end   = $this->params()->fromQuery('end', false);
        $userId = $this->params()->fromQuery('user', null);
        $ics   = $this->params()->fromQuery('ics', false);
        $hoursPerDay   = $this->params()->fromQuery('hoursPerDay', 24);

        $customs = [];
        $groups  = $this->entityManager()->getRepository('Field\Entity\Group')->findBy([
            'entity' => Project::class,
        ]);
        $user = isset($userId)
            ? $this->entityManager()->getRepository('User\Entity\User')->findOneBy(['id' => $userId])
            : $this->authStorage()->getUser();

        foreach ($groups as $group) {
            foreach ($group->getFields() as $field) {
                if ($field->getType() == Field::TYPE_TEXT) {
                    $customs[] = $field;
                }
            }
        }

        return new ViewModel([
            'syncTypes' => Synchronisation::getTypes(),
            'customs'   => $customs,
            'configs'   => Synchronisation::getConfigOptions(),
            'rows'      => $rows,
            'start'     => $start,
            'end'       => $end,
            'user'      => $user,
            'ics'       => $ics,
            'hoursPerDay' => $hoursPerDay,
        ]);
    }

    public function doImportAction()
    {
        $config = [];
        $content = null;

        /** @var EntityManager $em */
        $em = $this->entityManager();

        $synchronisationIds = $this->params()->fromQuery('rows', false);
        $start = $this->params()->fromQuery('start', false);
        $end = $this->params()->fromQuery('end', false);
        $ics = $this->params()->fromQuery('ics', false);
        $userIdFromRequest = $this->params()->fromQuery('user', false);
        $user = $userIdFromRequest ? $em->getRepository('User\Entity\User')->find(intval($userIdFromRequest)) : $this->authStorage()->getUser();
        $hoursPerDay = $this->params()->fromQuery('hoursPerDay', false);

        if ($ics) {
            $config = [
                'join'    => $this->params()->fromQuery('join', false),
                'closing' => $this->params()->fromQuery('closing', false),
                'field'   => $this->params()->fromQuery('field', false),
                'custom'  => $this->params()->fromQuery('custom', false),
            ];
            $content = $this->getRequest()->getContent();
        }

        /** @var TimeService $service */
        $service = $this->serviceLocator->get('TimeService');
        $response = $service->parseIcsAndLink($synchronisationIds, [$user], $start, $end, $ics, $config, $content);

        return new JsonModel($response);
    }

    private function isDataMapped($data, $id)
    {
        foreach ($data as $datum) {
            if ($datum['id'] === $id) {
                return true;
            }
        }
        return false;
    }

    protected function zimbraToCalendar($sync)
    {
        // TODO : manage recurrences
        $events = [];

        $config = $sync->getConfig();
        if (isset($config['username']) && isset($config['password'])) {
            $username = $config['username'];
            $password = $config['password'];
            $curlAuthentication = new CurlAuthentication($username, $password);

            $url = rtrim($sync->getLink(), '/') . '/home/' . $username . '/calendar?auth=ba&fmt=json';

            $content = CurlUtils::getContent($url, $curlAuthentication);

            $data = json_decode($content);
            foreach ($data->appt as $appt) {

                if (!isset($appt->inv[0]->comp[0]->s) || !isset($appt->inv[0]->comp[0]->e)) {
                    continue;
                }

                $_start = $appt->inv[0]->comp[0]->s[0];
                $_end   = $appt->inv[0]->comp[0]->e[0];


                $tzStart = null;
                $tzEnd = null;
                if (isset($_start->tz)) {
                    $tzStart = new \DateTimeZone($_start->tz);
                }
                if (isset($_end->tz)) {
                    $tzEnd = new \DateTimeZone($_end->tz);
                }

                $start = new \DateTime($_start->d, $tzStart);
                $end = new \DateTime($_end->d, $tzEnd);

                $event = [
                    'UID'         => $appt->uid,
                    'SUMMARY'     => $appt->inv[0]->comp[0]->name,
                    'DESCRIPTION' => isset($appt->inv[0]->comp[0]->desc[0]->_content) ? $appt->inv[0]->comp[0]->desc[0]->_content : '',
                    'DTSTART'     => $start,
                    'DTEND'       => $end,
                    'TAGS'        => isset($appt->tn) ? explode(',', $appt->tn) : [],
                ];

                $events[] = $event;
            }
        }

        return $events;
    }
}
