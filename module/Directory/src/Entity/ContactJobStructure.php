<?php
namespace Directory\Entity;

use Doctrine\ORM\EntityManager;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * This Class manages the job of a contact in a structure
 *
 * @category Directory
 * @package EVA
 * @since Version 4
 * @version 4
 * @author Curtis Pelissier <curtis.pelissier@lignusdev.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="directory_contact_job_structure")
 */
class ContactJobStructure extends BasicRestEntityAbstract
{
    /**
     * Unique ID
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Job Contact
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Directory\Entity\Contact", inversedBy="links")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;

    /**
     * Job Structure
     * @var Job
     * @ORM\ManyToOne(targetEntity="Directory\Entity\Job", inversedBy="links")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    protected $job;

    /**
     * Job Structure
     * @var Structure
     * @ORM\ManyToOne(targetEntity="Directory\Entity\Structure", inversedBy="links")
     * @ORM\JoinColumn(name="structure_id", referencedColumnName="id")
     */
    protected $structure;

    /**
     * ContactJobStructure constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Job $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }

    /**
     * @return Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param Structure $structure
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'job',
                'required' => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_numeric($value)) {
                                    $job = $entityManager->getRepository('Directory\Entity\Job')->find($value);
                                } else {
                                    $job = $entityManager->getRepository('Directory\Entity\Job')->findOneBy(['name' => $value]);
                                }
                                return $job !== null;
                            },
                            'message'  => 'job_field_error_non_exists',
                        ],
                    ],
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_numeric($value)) {
                                    $job = $entityManager->getRepository('Directory\Entity\Job')->find($value);
                                } else {
                                    $job = $entityManager->getRepository('Directory\Entity\Job')->findOneBy(['name' => $value]);
                                }
                                if (is_numeric($context['structure'])) {
                                    $structure = $entityManager->getRepository('Directory\Entity\Structure')->find($context['structure']);
                                } else {
                                    $structure = $entityManager->getRepository('Directory\Entity\Structure')->findOneBy(['name' => $context['structure']]);
                                }
                                if (is_numeric($context['contact'])) {
                                    $contact = $entityManager->getRepository('Directory\Entity\Contact')->find($context['contact']);
                                } else {
                                    $contact = $entityManager->getRepository('Directory\Entity\Contact')->findOneBy(['name' => $context['contact']]);
                                }
                                $entity = $entityManager->getRepository('Directory\Entity\ContactJobStructure')->findOneBy(['contact' => $contact, 'structure' => $structure, 'job'=> $job]);
                                return $entity === null;
                            },
                            'message'  => 'job_structure_contact_field_error_already_exists',
                        ],
                    ],
                ],
            ], [
                'name'     => 'structure',
                'required' => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_numeric($value)) {
                                    $structure = $entityManager->getRepository('Directory\Entity\Structure')->find($value);
                                } else {
                                    $structure = $entityManager->getRepository('Directory\Entity\Structure')->findOneBy(['name' => $value]);
                                }
                                return $structure !== null;
                            },
                            'message'  => 'structure_field_error_non_exists',
                        ],
                    ],
                ],
            ], [
                'name'     => 'contact',
                'required' => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_numeric($value)) {
                                    $contact = $entityManager->getRepository('Directory\Entity\Contact')->find($value);
                                } else {
                                    $contact = $entityManager->getRepository('Directory\Entity\Contact')->findOneBy(['name' => $value]);
                                }
                                return $contact !== null;
                            },
                            'message'  => 'contact_field_error_non_exists',
                        ],
                    ],
                ],
            ],
        ]);
        /*$inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'required' => true,
        ], 'job');
        $inputFilter->add([
            'required' => true,
        ], 'structure');
        $inputFilter->add([
            'required' => true,
        ], 'contact');*/

        return $inputFilter;
    }
}
