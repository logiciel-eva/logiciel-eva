(function(app) {

    app.controller('syntheseTabController', [
        'projectService', '$scope', 'treeBuilderService', 'orderByFilter',
        function syntheseTabController(projectService, $scope, treeBuilderService, orderBy) {
            var self = this;

            self.timesheetFilters = {};
            self.children = true;
            self.arrayUserTimeArbo = [];
            self.arrayUserTime = [];
            self.reverse = true;

            self.resetFilters = function () {
                self.timesheetFilters = {};
            };


            self.sortBy = function(propertyName) {
                self.reverse = (propertyName !== null && self.propertyName === propertyName)
                    ? !self.reverse : false;
                self.propertyName = propertyName;
                self.arrayUserTimeArbo = orderBy(self.arrayUserTimeArbo, self.propertyName, self.reverse);
                self.arrayUserTime = orderBy(self.arrayUserTime, self.propertyName, self.reverse);
            };
            self.getData = function () {
                projectService.find($scope.$parent.$parent.self.project.id, {
                    col: [
                        'level',
                        'globalTimeTargetArbo',
                        'timeSpentArbo',
                        'timeTargetArbo',
                        'userTimeRepartitionArbo',
                        'globalTimeTarget',
                        'timeSpent',
                        'timeTarget',
                        'userTimeRepartition',
                        'globalTimeTarget',
                        'timeSpent',
                        'timeTarget',
                        'userTimeRepartition',
                        'hierarchySub.id',
                        'hierarchySub.code',
                        'hierarchySub.name',
                        'hierarchySub.parent.id',
                        'hierarchySub.globalTimeTargetArbo',
                        'hierarchySub.timeSpentArbo',
                        'hierarchySub.timeTargetArbo',
                        'hierarchySub.level'
                    ],
                    search: {
                        data: {
                            filters: {
                                project: {
                                    op: 'eq',
                                    val: $scope.$parent.$parent.self.project.id
                                }
                            }
                        }
                    },
                    timesheetFilters: self.timesheetFilters
                }).then(function(res) {
                    self.data = res.object;
                    self.arrayUserTimeArbo = [];
                    self.arrayUserTime = [];
                    self.data.costSpentArbo = null;
                    self.data.costTargetArbo = null;
                    self.data.costSpent = null;
                    self.data.costTarget = null;

                    let timesArbo = self.data.userTimeRepartitionArbo;
                    let times = self.data.userTimeRepartition;
                    // if(times.lenght > 0) {
                        angular.forEach(timesArbo, function(time) {
                            self.data.costSpentArbo += time.done_cost;
                            self.data.costTargetArbo += time.target_cost;
                            self.arrayUserTimeArbo.push(time);
                        });
                        angular.forEach(times, function(time) {
                            self.data.costSpent += time.done_cost;
                            self.data.costTarget += time.target_cost;
                            self.arrayUserTime.push(time);
                        });
                    // }
                    /*self.data.hierarchySub.push({id: $scope.$parent.$parent.self.project.id, parent: null});
                    self.data.projectRepartition = treeBuilderService.toArray(treeBuilderService.toTree(self.data.hierarchySub));*/
                });
            };

            self.getData();
        }
    ])

})(window.EVA.app);
