<?php

$contact = [
    'icon' => 'contact',
    'title' => 'contact_module_title',
    'href' => 'directory/contact',
    'right' => 'directory:e:contact:r',
    'priority' => 3,
];

$structure = [
    'icon' => 'structure',
    'title' => 'structure_module_title',
    'href' => 'directory/structure',
    'right' => 'directory:e:structure:r',
    'priority' => 2,
];

$jobs = [
    'icon' => 'job',
    'title' => 'job_module_title',
    'right' => 'directory:e:job:r',
    'href' => 'directory/job',
    'priority' => 2,
];

return [
    'menu' => [
        'client-menu' => [
            'directory' => [
                'icon' => 'directory',
                'title' => 'directory_module_title',
                'priority' => 2000,
                'children' => [
                    'contact' => $contact,
                    'structure' => $structure,
                ],
            ],
            'administration' => [
                'children' => [
                    'jobs' => $jobs,
                ],
            ],
        ],
        'client-menu-parc' => [
            'data' => [
                'children' => [
                    'directory' => [
                        'icon' => 'directory-parc',
                        'title' => 'directory_module_title',
                        'priority' => 2000,
                        'children' => [
                            'contact' => array_replace($contact, ['icon' => 'contact-parc']),
                            'structure' => $structure,
                            'jobs' => $jobs,
                        ],
                    ],
                ],
            ],
        ],
    ],
];
