<?php

return [
    'icons' => [
        'directory' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-university',
        ],
        'contact'   => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-users',
        ],
        'directory-parc' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-address-book',
        ],
        'contact-parc'   => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-address-card',
        ],
        'structure' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-building-o',
        ],
        'address' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-map-marker',
        ],
        'job' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-university',
        ],
    ],
];
