<?php

namespace Bootstrap\Environment;

/**
 * Class Host
 *
 * This class is a wrapper for a hostname.
 *
 * @package Bootstrap\Environment
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class Host
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function toAlnum()
    {
        return preg_replace('/[^A-Za-z0-9_]/', '_', $this->name);
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->name;
    }
}
