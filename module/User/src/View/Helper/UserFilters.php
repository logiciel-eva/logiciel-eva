<?php

namespace User\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class UserFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'id' => [
                'label' => ucfirst($this->translator->__invoke('user_entity')),
                'type' => 'user-select',
            ],
            'login' => [
                'label' => $this->translator->__invoke('user_field_login'),
                'type' => 'string',
            ],
            'name' => [
                'label' => $this->translator->__invoke('user_field_name'),
                'type' => 'string',
            ],
            'email' => [
                'label' => $this->translator->__invoke('user_field_email'),
                'type' => 'string',
            ],
            'gender' => [
                'label' => $this->translator->__invoke('user_field_gender'),
                'type' => 'manytoone',
                'options' => [
                    [
                        'value' => 'm',
                        'text' => $this->translator->__invoke('user_gender_m'),
                    ],
                    [
                        'value' => 'mme',
                        'text' => $this->translator->__invoke('user_gender_mme'),
                    ],
                    [
                        'value' => 'neutral',
                        'text' => $this->translator->__invoke('user_gender_neutral'),
                    ],
                ],
            ],
            'role' => [
                'label' => $this->translator->__invoke('user_field_role'),
                'type' => 'role-select',
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('user_field_createdAt'),
                'type' => 'date',
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('user_field_updatedAt'),
                'type' => 'date',
            ],
        ];

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('user', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type' => 'keyword-select',
                    'group' => $group->getId(),
                ];
            }
        }

        $this->filters = $filters;
    }
}
