<?php

namespace Indicator\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class IndicatorFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $typeOptions = [];
        foreach (\Indicator\Entity\Indicator::getTypes() as $type) {
            $typeOptions[] = [
                'value' => $type,
                'text'  => $this->translator->__invoke('indicator_type_' . $type)
            ];
        }

        $filters = [
            'id' => [
                'label' => $this->translator->__invoke('indicator_field_id'),
                'type'  => 'string'
            ],
            'name' => [
                'label' => $this->translator->__invoke('indicator_field_name'),
                'type'  => 'string'
            ],
            'uom' => [
                'label' => $this->translator->__invoke('indicator_field_uom'),
                'type'  => 'string'
            ],
            'archived' => [
                'label' => $this->translator->__invoke('indicator_field_archived'),
                'type'  => 'bool'
            ],
            'type' => [
                'label' => $this->translator->__invoke('indicator_field_type'),
                'type'    => 'manytoone',
                'options' => $typeOptions
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('indicator_field_createdAt'),
                'type'  => 'date'
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('indicator_field_updatedAt'),
                'type'  => 'date'
            ],
        ];

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('indicator', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type'  => 'keyword-select',
                    'group' => $group->getId()
                ];
            }
        }

        if ($this->moduleManager->getClientModuleConfiguration('user', 'service')) {
            $filters['services'] = [
                'label' => $this->translator->__invoke('indicator_field_services'),
                'type'  => 'service-select',
            ];
        }

        $this->filters = $filters;
    }
}
