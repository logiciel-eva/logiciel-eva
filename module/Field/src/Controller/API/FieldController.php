<?php

namespace Field\Controller\API;

use Core\Controller\BasicRestController;

class FieldController extends BasicRestController
{
    protected $entityClass = 'Field\Entity\Field';
    protected $repository  = 'Field\Entity\Field';
    protected $entityChain = 'field:e:field';

    protected function searchList($queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->join('object.group', 'group');

        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
