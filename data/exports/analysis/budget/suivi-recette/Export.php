<?php

use Bootstrap\Entity\Client;
use Core\Controller\Plugin\ApiRequestPlugin;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Export
{
	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var Translate
	 */
	protected $translator;

	/**
	 * @var ApiRequestPlugin
	 */
	protected $apiRequest;

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * Export constructor.
	 *
	 * @param EntityManager    $em
	 * @param Translate        $translator
	 * @param ApiRequestPlugin $apiRequest
	 * @param Client           $client
	 */
	public function __construct(
		EntityManager $em,
		Translate $translator,
		ApiRequestPlugin $apiRequest,
		Client $client
	) {
		$this->em = $em;
		$this->translator = $translator;
		$this->apiRequest = $apiRequest;
		$this->client = $client;
	}

	public function doExport($params, $data)
	{
		ini_set('memory_limit', -1);
		ini_set('max_execution_time', -1);
		setlocale(LC_TIME, 'fr_FR', 'French_France', 'French');

		$postes = [];
		foreach ($data as $project) {
			if (!$this->projectHasIncomes($project)) {
				continue;
			}

			foreach ($project['posteIncomes'] as $poste) {
				if (!$poste['envelope']) {
					continue;
				}

				if (!isset($postes[$poste['id']])) {
					$postes[$poste['id']] = $poste;
				}
			}
		}

		$years = $this->calculateAmountByYears($postes, 'requested');

		$lines = [];

		$headerLine = [
			'Financeur / Service instructeur',
			'Enveloppe',
			'Nom',
			'Année prog.',
			'Code opération SIREP@NET',
			'Libellé Opération',
			'Date d\'envoi dossier',
			'Date réception dossier',
			'Date délib.CP / Com.Prog.',
			'Date arrêté / convention',
			'N° arrêté / convention',
			'Montant subvention',
			'Montant total dépenses subventionables HT',
			'Montant total dépenses subventionables TTC',
			'N° titre',
			'Montants cumulés sollicités',
			'Détail des sollicitations',
			'Montants cumulés encaissés',
			'Détail des versements',
			'Reste à encaisser/sollicité',
			'Reste à encaisser/notifié',
			'Date limite début opération',
			'Délai réalisation',
			'Date limite de réception doc justificatifs',
		];

		foreach ($years as $year) {
			$headerLine[] = 'Recette prévisionnelle ' . $year;
		}

		$lines[] = $headerLine;

		foreach ($postes as $posteId => $poste) {
			$posteLine = [
				$poste['envelope']['financer']['name'] . ' ',
				$poste['envelope']['name'] . ' ',
				$poste['name'] . ' ',
				$this->getProgrammingDate($poste),
				$poste['project']['code'] . ' ',
				$poste['project']['name'] . ' ',
				str_replace('00:00', '', $poste['folderSendingDate']),
				str_replace('00:00', '', $poste['folderReceiptDate']),
				str_replace('00:00', '', $poste['deliberationDate']),
				str_replace('00:00', '', $poste['arrDate']),
				$poste['arrNumber'] . ' ',
				$poste['amount'],
				$poste['subventionAmountHT'],
				$poste['subventionAmount'],
				$this->getIncomesConventionTitle($poste),
				$this->getIncomesAmount($poste, 'requested'),
				$this->getIncomesDetail($poste, 'requested'),
				$this->getIncomesAmount($poste, 'perceived'),
				$this->getIncomesDetail($poste, 'perceived'),
				$this->getBalanceToBePerceived($poste, 'requested'),
				$this->getBalanceToBePerceived($poste, 'notified'),
				str_replace('00:00', '', $poste['caduciteStart']),
				str_replace('00:00', '', $poste['caduciteEnd']),
				str_replace('00:00', '', $poste['caduciteSendProof']),
			];

			foreach ($years as $year) {
				$posteLine[] = isset($poste['_years'][$year]) ? $poste['_years'][$year] : '';
			}

			$lines[] = $posteLine;
		}

		$this->download($lines);
	}

	protected function projectHasIncomes($project)
	{
		if (!$project['posteIncomes']) {
			return false;
		}

		foreach ($project['posteIncomes'] as $poste) {
			if (!$poste['incomes']) {
				continue;
			}

			foreach ($poste['incomes'] as $income) {
				if ($income['amount'] !== 0) {
					return true;
				}
			}
		}

		return false;
	}

	protected function calculateAmountByYears(&$postes, $type)
	{
		$years = [];

		foreach ($postes as &$poste) {
			$poste['_years'] = [];

			if (!$poste['incomes']) {
				continue;
			}

			foreach ($poste['incomes'] as $income) {
				if (!$income['date'] || $income['amount'] === 0 || $income['type'] !== $type) {
					continue;
				}

				$matches = [];
				preg_match('/[0-9]{4}/', $income['date'], $matches);

				if (!isset($matches[0])) {
					continue;
				}

				$year = $matches[0];

				if (!in_array($year, $years)) {
					$years[] = $year;
				}

				if (!isset($poste['_years'][$year])) {
					$poste['_years'][$year] = 0;
				}

				$poste['_years'][$year] += $income['amount'];
			}
		}

		sort($years);

		return $years;
	}

	protected function getProgrammingDate($poste)
	{
		if (!$poste['project']['programmingDate']) {
			return '';
		}

		$matches = [];
		preg_match('/[0-9]{4}/', $poste['project']['programmingDate'], $matches);

		return isset($matches[0]) ? $matches[0] : '';
	}

	protected function getIncomesConventionTitle($poste)
	{
		if (!$poste['incomes']) {
			return '';
		}

		$conventionTitles = [];
		foreach ($poste['incomes'] as $income) {
			if (!$income['conventionTitle'] || $income['conventionTitle'] === '') {
				continue;
			}

			$conventionTitles[] = 'Titre n° ' . $income['conventionTitle'];
		}

		if (count($conventionTitles) === 0) {
			return '';
		}

		return implode("\n", $conventionTitles);
	}

	protected function getIncomesDetail($poste, $type)
	{
		if (!$poste['incomes']) {
			return '';
		}

		$posteData = [];

		foreach ($poste['incomes'] as $income) {
			if ($income['type'] !== $type) {
				continue;
			}

			$incomeData = '';

			if ($income['date']) {
				$incomeData .= str_replace('00:00', '', $income['date']) . ' : ';
			}

			if ($income['name']) {
				$incomeData .= $income['name'] . ' : ';
			}

			if ($income['amount']) {
				$incomeData .= $income['amount'];
			}

			if (!$incomeData) {
				continue;
			}

			$posteData[] = $incomeData;
		}

		if (count($posteData) === 0) {
			return '';
		}

		return implode("\n", $posteData);
	}

	protected function getIncomesAmount($poste, $type = null)
	{
		$total = 0;

		if (!$poste['incomes']) {
			return $total;
		}

		foreach ($poste['incomes'] as $income) {
			if (!$type || $income['type'] === $type) {
				$total += $income['amount'];
			}
		}

		return $total;
	}

	protected function getBalanceToBePerceived($poste, $type)
	{
		return $this->getIncomesAmount($poste, $type) -
			$this->getIncomesAmount($poste, 'perceived');
	}

	protected function download($array)
	{
		try {
			foreach ($array as $i => $line) {
				foreach ($line as $j => $value) {
					if (is_numeric($value) && $value == 0) {
						$array[$i][$j] = '0';
					}
				}
			}

			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();

			$sheet->fromArray($array);

			foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
				$sheet->getColumnDimension($col)->setAutoSize(true);
			}

			$headerLine = $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . '1');
			$headerLine->getFont()->setBold(true);
			$headerLine
				->getAlignment()
				->setHorizontal(Alignment::HORIZONTAL_CENTER)
				->setVertical(Alignment::VERTICAL_CENTER);

			$sheet
				->getStyle('A1:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())
				->getAlignment()
				->setWrapText(true);

			$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

			header(
				'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8'
			);
			header('Content-Disposition: attachment; filename="export.xlsx"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Cache-Control: private', false);
			ob_end_clean();
			$writer->save('php://output');
			exit();
		} catch (Exception $exception) {
			var_dump($exception->getMessage());
		}
	}
}
