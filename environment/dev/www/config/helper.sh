#!/bin/bash

#
# @script : helper script for development.
# @since : 01/2023
# @author : gael@sigogneau.fr
#
WORKDIR="/var/www"

#
# Install dependencies, permissions, etc.
#
install() {
    echo "[START] Installation APP..."
    cd ${WORKDIR} || exit 1

    echo "[INFO] Install configuration"
    cp -f ${WORKDIR}/environment/dev/www/config/app/module.config.php module/Bootstrap/config/module.config.php
    cp -f ${WORKDIR}/environment/dev/www/config/app/doctrine.local.php config/autoload/doctrine.local.php

    echo "[INFO] Build dependencies" \
    && php composer.phar install \
    && bower install --allow-root \
    && echo "[INFO] Update database" \
    && chmod +x bin/update && bin/update

    echo "[INFO] Set unix permissions"
    chown -R www-data:www-data public data
    chmod --reference=config data
    chmod --reference=config data/DoctrineORMModule
    chmod --reference=config data/DoctrineORMModule/Proxy
    chmod -R 775 data
    # tips, check unix permission as apache with :  su -s /bin/bash -c 'ls -la ./data' www-data
    # chown www-data. data -Rf
    # chmod 660 data/ -Rf

    echo "[END] Installation APP..."
}

############## Create your custom helper here ##############
# After add your custom helper you need to rebuild : docker-compose -f qpilot-env-dev.yml down && docker-compose -f qpilot-env-dev.yml up --build -d

############## // Create your custom helper here ##############

echo "[START] Running function $@ on helper..."
"$@"
