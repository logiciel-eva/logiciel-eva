<?php

return [
    'module' => [
        'note' => [
            'environments' => [
                'client'
            ],
            'active'   => true,
            'required' => true,
            'acl' => [
                'entities' => [
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
