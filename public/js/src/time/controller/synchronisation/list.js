(function (app) {

    app.controller('synchronisationListController', [
        'synchronisationService', '$scope', 'userService', 'flashMessenger', 'translator', '$http',
        function synchronisationListController(synchronisationService, $scope, userService, flashMessenger, translator, $http) {
            var self = this;

            self.view = 'table';

            self.synchronisations = [];
            self.options = {
                name: 'synchronisation-list',
                col: [
                    'id',
                    'name',
                    'link',
                    'type',
                    'user.id',
                    'user.name',
                    'cronSchedule',
                    'cronLastSync',
                    'users.id',
                    'users.name',
                    'createdAt',
                    'updatedAt',
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    synchronisationService.findAll(params, abort.promise).then(function (res) {
                        self.synchronisations = res.rows;
                        callback(res);                      
                    })
                }
            };

            self.user = null;

            self.init = function init(user) {
                self.user = user;
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function (event) {
                if (event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.time = {
                'start': null,
                'end': null
            };
         

            self.import = function () {
                var selected = [];
                for (var i = 0; i < self.synchronisations.length; ++i) {
                    var sync = self.synchronisations[i];
                    if (sync.selected) {
                        selected.push(sync.id);
                    }
                }

                var doContinue = true,
                    params = { rows: selected };

                if (self.time.start == null &&
                    self.time.end == null
                ) {
                    doContinue = confirm(translator('timesheet_import_confirm_no_time_range'));
                } else {
                    if (self.time.start != null) {
                        params.start = self.time.start;
                    }

                    if (self.time.end != null) {
                        params.end = self.time.end;
                    }
                }
                params.user = self.user ? self.user.id : null;
                if (doContinue) {
                    window.location.href = '/timesheet/synchronisation/import?' + $.param(params);
                }
            };

            /**
             * Delete the synchronisation.
             * @param sync
             */
            self.deleteSynchronisation = function deleteSynchronisation(sync) {
                if (confirm(translator('synchronisation_confirm_delete'))) {
                    synchronisationService.remove(sync).then(function (res) {
                        var index = 0;
                        for (var i = 0; i < self.synchronisations.length; ++i) {
                            if (self.synchronisations[i].id == sync.id) {
                                index = i;
                                break;
                            }
                        }
                        self.synchronisations.splice(index, 1);
                        flashMessenger.success(translator('synchronisation_message_deleted'));
                    });
                }
            };

            /**
             * Edited line
             */
            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(sync) {
                sync.managers = [];

                self.editedLineErrors = {};
                self.editedLine = sync;
                self.editedLineFields = angular.copy(sync);
            };

            self.saveEditedLine = function saveEditedLine(sync) {
                self.editedLineErrors = {};
                syncToSave = angular.extend({}, sync, self.editedLineFields);
                delete syncToSave.cronLastSync;
                synchronisationService.save(syncToSave, {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        sync = angular.extend(sync, res.object);
                        self.editedLine = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('synchronisation_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };
        }
    ])

})(window.EVA.app);
