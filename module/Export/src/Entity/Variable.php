<?php

namespace Export\Entity;

class Variable
{
    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var array
     */
    protected $params;

    /**
     * Variable constructor.
     * @param string $str
     */
    public function __construct($str)
    {
        $this->parse($str);
    }

    /**
     * @param string $str
     */
    protected function parse($str)
    {
        $matches = [];
        preg_match('/^(.*)(\((.*)\)?)?$/U', $str, $matches);
        @list($string, $identifier, $_paramsStr, $paramsStr) = $matches;

        $this->identifier = $identifier;
        if ($paramsStr) {
            $params = $this->exploder($paramsStr, ',', '\[', '\]');
            foreach ($params as $i => $param) {
                if (preg_match('/\[(.*)\]/U', $param)) {
                    $params[$i] = explode(',', preg_replace('/\[(.*)\]/U', '$1', $param));
                    if ($params[$i] === ['']) {
                        unset($params[$i]);
                    }
                }
            }

            $this->params = $params;
        }
    }

    protected function exploder($input, $delimiter = ',', $open_tag = '\(', $close_tag = '\)')
    {
        // this will match any text inside parenthesis
        // including parenthesis itself and without nested parenthesis
        $regexp = '/'.$open_tag.'[^'.$open_tag.$close_tag.']*'.$close_tag.'/';

        // put in placeholders like {{\d}}. They can be nested.
        $r = array();
        while (preg_match_all($regexp, $input, $matches)) {
            if ($matches[0]) {
                foreach ($matches[0] as $match) {
                    $r[] = $match;
                    $input = str_replace($match, '{{'.count($r).'}}', $input);
                }
            } else {
                break;
            }
        }
        $output = array_map('trim', explode($delimiter, $input));

        // put everything back
        foreach ($output as &$a) {
            while (preg_match('/{{(\d+)}}/', $a, $matches)) {
                $a = str_replace($matches[0], $r[$matches[1] - 1], $a);
            }
        }

        return $output;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }
}