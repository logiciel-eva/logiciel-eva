<?php

namespace Campain\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Campain
 *
 * @package Campain\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="campain_campain")
 */
class Campain extends BasicRestEntityAbstract
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	protected $name;

    /**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $description;
    

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date")
	 */
    protected $startDate;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date")
	 */
    protected $endDate;

	/**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="User\Entity\User")
     * @ORM\JoinTable(
     *  name="campain_managers",
     *  joinColumns={
     *      @ORM\JoinColumn(name="campain_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *  }
     * )
     */
    protected $evaluationManagers;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Campain\Entity\Indicator", mappedBy="campain", cascade={"remove"} )
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $indicators;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Indicator\Entity\Measure", mappedBy="campain", cascade={"remove"})
     */
    protected $measures;

    /**
     * Constructor of the class
     *
     * @since Version 4
     * @version 4
     */
    public function __construct()
    {
         $this->indicators = new ArrayCollection();
         $this->measures = new ArrayCollection();
         $this->evaluationManagers = new ArrayCollection();
    }

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return ArrayCollection
     */
    public function getEvaluationManagers()
    {
        return $this->evaluationManagers;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setEvaluationManagers($evaluationManagers)
    {
        $this->evaluationManagers = $evaluationManagers;
    }

 /**
     * @param ArrayCollection $evaluationManagers
     */
    public function addEvaluationManagers(ArrayCollection $evaluationManagers)
    {
        foreach ($evaluationManagers as $manager) {
            $this->getEvaluationManagers()->add($manager);
        }
    }

    /**
     * @param ArrayCollection $evaluationManagers
     */
    public function removeEvaluationManagers(ArrayCollection $evaluationManagers)
    {
        foreach ($evaluationManagers as $manager) {
            $this->getEvaluationManagers()->removeElement($manager);
        }
    }

    /**
     * @return Indicator[]|ArrayCollection
     */
    public function getIndicators()
    {
        return $this->indicators;
    }

    /**
     * @param Indicator[]|ArrayCollection $indicators
     */
    public function setIndicators($indicators)
    {
        $this->indicators = $indicators;
    }

    /**
     * @param ArrayCollection|Indicator[] $indicators
     */
    public function addIndicators(ArrayCollection $indicators)
    {
        foreach ($indicators as $indicator) {
            $this->getIndicators()->add($indicator);
        }
    }

    /**
     * @param ArrayCollection|Indicator[] $indicators
     */
    public function removeIndicators(ArrayCollection $indicators)
    {
        foreach ($indicators as $indicator) {
            $this->getIndicators()->removeElement($indicator);
        }
    }

    /**
     * @return Measure[]|ArrayCollection
     */
    public function getMeasures()
    {
        return $this->measures;
    }

    /**
     * @param Measure[]|ArrayCollection $measures
     */
    public function setMeasures($measures)
    {
        $this->measures = $measures;
    }

    /**
     * @param ArrayCollection|Measure[] $measures
     */
    public function addMeasures(ArrayCollection $measures)
    {
        foreach ($measures as $measure) {
            $this->getMeasures()->add($measure);
        }
    }

    /**
     * @param ArrayCollection|Measure[] $measures
     */
    public function removeMeasures(ArrayCollection $measures)
    {
        foreach ($measures as $measure) {
            $this->getMeasures()->removeElement($measure);
        }
    }

	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilterFactory = new Factory();
		$inputFilter = $inputFilterFactory->createInputFilter([
			[
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'description',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'startDate',
                'required' => true,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
			[
                'name'     => 'endDate',
                'required' => true,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                $startDate = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $context['startDate'])
                                    ? \DateTime::createFromFormat('d/m/Y H:i', $context['startDate'])
                                    : \DateTime::createFromFormat('d/m/Y', $context['startDate']);
                                $endDate   = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $context['endDate'])
                                    ? \DateTime::createFromFormat('d/m/Y H:i', $context['endDate'])
                                    : \DateTime::createFromFormat('d/m/Y', $context['endDate']);

                                return $startDate <= $endDate;
                            },
                            'message'  => 'campain_field_enddate_before_startdate_error',
                        ],
                    ],
                ]
            ],
			[
                'name'     => 'evaluationManagers',
                'required' => true
            ]
		]);

		return $inputFilter;
	}
}
