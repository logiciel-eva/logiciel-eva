<?php

namespace Campain\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use BadMethodCallException;
use Bootstrap\Entity\Client;
use Indicator\Entity\Measure;
use Campain\Entity\Indicator;
use Campain\Entity\Campain;
use Campain\Entity\Measure as CampainMeasure;
use Indicator\Entity\Indicator as EntityIndicator;

class IndicatorController extends BasicRestController
{
    protected $entityClass = 'Campain\Entity\Indicator';
    protected $repository  = 'Campain\Entity\Indicator';
    protected $entityChain = 'campain:e:campain';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }
       
        $queryBuilder->addOrderBy($sort, $order);
        $queryBuilder->join('object.campain', 'campain');
        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'campain.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    protected function extract($object)
    {
        $array = parent::extract($object);
        $objectAsArray = [];
        if(isset($array['entity']) && isset($array['primary'])){
            $entityObject = $this->entityManager()
            ->getRepository($array['entity'])
            ->find($array['primary']);
            $array['entityDetails'] = parent::extractInternal($entityObject, 'name', $objectAsArray);
            if($array['entity'] == 'Indicator\Entity\Indicator'){
                $array['type'] = 'indicator';
                $array['indicatorPrimary']['id'] = $array['primary'];
                $array['indicatorPrimary']['name'] = $array['entityDetails']['name'];
            }else if($array['entity'] == 'Indicator\Entity\GroupIndicator'){
                $array['type'] = 'indicator-group';
                $array['groupIndicatorPrimary']['id'] = $array['primary'];
                $array['groupIndicatorPrimary']['name'] = $array['entityDetails']['name'];
            }
        }
        return $array;
    }

    public function delete($id)
    {
        // prepare for master client deletion (if necessary)
        $measuresToDelete = $this->getMeasuresToDelete($id);
        
        $response = parent::delete($id);
        if($response->getVariable("success") === true) {
            $this->deleteMeasuresInMaster($measuresToDelete);
        }

        return $response;
    }

    /**
     * @return array
     */
    private function getMeasuresToDelete($indicatorId)
    {
        $measuresToDelete = [];
        $campaignMeasures = $this->entityManager()->getRepository('Campain\Entity\Measure')->findBy(['indicator' => $indicatorId]);
        foreach($campaignMeasures as $campaignMeasure)
        {
            $measuresToDelete[] = $campaignMeasure->getTargetMeasure()->getId();
            $measuresToDelete[] = $campaignMeasure->getRealiseMeasure()->getId();
        }
        return $measuresToDelete;
    }

    private function deleteMeasuresInMaster(array $clientCampaignMeasures)
    {
        $currentClient = $this->environment()->getClient();
        if(null === $currentClient->getNetwork())
        {
            return;
        }
        $masterClient = $currentClient->getNetwork()->getMaster();
        $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
        $masterEm = $entityManagerFactory->getEntityManager($masterClient);

        $measureRepository = $masterEm->getRepository('Indicator\Entity\Measure');

        foreach($clientCampaignMeasures as $clientCampaignMeasure) {
            // todo: query client as well
            $masterMeasure = $measureRepository->findOneBy(['slave' => $clientCampaignMeasure]);
            if(null !== $masterMeasure) {
                $masterEm->remove($masterMeasure);
            }
        }
        $masterEm->flush();
    }

    public function create($data)
	{
        if($data['type'] == 'indicator' && isset($data['indicatorPrimary'])){
            $data['primary'] = $data['indicatorPrimary']['id'];
            $data['entity'] = 'Indicator\Entity\Indicator';
        }else if($data['type'] == 'indicator-group' && isset($data['groupIndicatorPrimary'])){
            $data['primary'] = $data['groupIndicatorPrimary']['id'];
            $data['entity'] = 'Indicator\Entity\GroupIndicator';
        }
		$response = parent::create($data);

        $this->createMeasuresInMaster($response->getVariable("object"));

        return $response;
    }

    private function createMeasuresInMaster(array $rawCampaignIndicator)
    {
        $campaignMeasures = $this->entityManager()->getRepository('Campain\Entity\Measure')->findBy(['indicator' => $rawCampaignIndicator['id']]);
        foreach($campaignMeasures as $campaignMeasure) {
            $campaignIndicator = $campaignMeasure->getIndicator();
            $indicator = $this->entityManager()->getRepository($campaignIndicator->getEntity())->findOneBy(['id' => $campaignIndicator->getPrimary()]);
            if(null === $indicator || null === $indicator->getMaster()) {
                continue;
            }
            $this->createMeasureInMaster($campaignMeasure->getRealiseMeasure(), $indicator);
            $this->createMeasureInMaster($campaignMeasure->getTargetMeasure(), $indicator);
        }
    }

    private function createMeasureInMaster(Measure $measure, EntityIndicator $indicator)
    {
        $currentClient = $this->environment()->getClient();
        if(null === $currentClient->getNetwork()) {
            return;
        }
        $masterClient = $currentClient->getNetwork()->getMaster();
        $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
        $masterEm = $entityManagerFactory->getEntityManager($masterClient);

        $masterMeasure = new Measure();
        $masterMeasure->setPeriod($measure->getPeriod());
        $masterMeasure->setType($measure->getType());
        $masterMeasure->setDate($measure->getDate());
        $masterMeasure->setStart($measure->getStart());
        $masterMeasure->setValue($measure->getValue());
        $masterMeasure->setComment($measure->getComment());
        $masterMeasure->setClient(array('id' => $currentClient->getId(), 'name' => $currentClient->getName(), 'selected' => true));
        $masterMeasure->setSlave($measure->getId());
        $masterMeasure->setSource($measure->getSource());
        $masterMeasure->setTransverse($measure->getTransverse());

        $masterIndicator = $masterEm->getRepository('Indicator\Entity\Indicator')->findOneBy(['id' => $indicator->getMaster()]);
        $masterMeasure->setIndicator($masterIndicator);

        $masterEm->persist($masterMeasure);
        $masterEm->flush();
    }

/**
	 * Hydrate a record from a data array.
	 * This method can be overridden to use another logic than the Doctrine Hydrator.
	 *
	 * @param array $data
	 * @param mixed $object
	 */
	protected function hydrate($data, $object)
	{
		parent::hydrate($data, $object);
        $this->createMeasures($object);
	}

     /**
     * @param Campain\Entity\Indicator $campainIndicator
     */
    private function createMeasures($campainIndicator){
        $entityObject = $this->entityManager()
        ->getRepository($campainIndicator->getEntity())
        ->find($campainIndicator->getPrimary());

        switch($campainIndicator->getEntity()){
            case 'Indicator\Entity\Indicator':
                $this->createMeasuresByIndicator($entityObject, null, $campainIndicator);
                break;
            case 'Indicator\Entity\GroupIndicator':
                $this->createMeasuresByIndicatorGroup($entityObject, $campainIndicator);
                break;
            default: throw new BadMethodCallException(
                'Invalid campain indicator entity'
            );
            break;
        }
    }
    
    /**
     * @param Indicator\Entity\Indicator $indicator
     * @param Indicator\Entity\GroupIndicator $indicatorGroup
     * @param Campain\Entity\Indicator $campainIndicator
     */
    private function createMeasuresByIndicator($indicator, $indicatorGroup, $campainIndicator){
        if(is_null($indicatorGroup)){
                   // On supprime les mesures non modifiées
            $em = $this->getEntityManager();
            $em ->createQuery('DELETE Indicator\Entity\Measure m WHERE m.indicator = :indicator_id AND m.campainIndicator = :campain_indicator_id AND m.updatedAt is NULL')
            ->setParameter('indicator_id', $indicator->getId())
            ->setParameter("campain_indicator_id", $campainIndicator->getId())
            ->execute();
        }
        $campainObject = $this->entityManager()
        ->getRepository('Campain\Entity\Campain')
        ->find($campainIndicator->getCampain()->getId());
        
        if($campainIndicator->getFrequency() && $campainIndicator->getFrequencyValue()>0){
            for($measureDate = $campainIndicator->getTargetDate(); 
                $measureDate < $campainObject->getEndDate(); 
                $measureDate = $this->getNextDateByFrequency(clone $measureDate, $campainIndicator->getFrequency(), $campainIndicator->getFrequencyValue())){
                    $realizeMeasure = $this->createMeasureObject($measureDate, Measure::TYPE_DONE, $campainObject, $campainIndicator, $indicator, $indicatorGroup);
                    $targetMeasure = $this->createMeasureObject($measureDate, Measure::TYPE_TARGET, $campainObject, $campainIndicator, $indicator, $indicatorGroup);
                    
                    $campainIndicator->getCampainMeasures()->add(
                        $this->createCampainMeasureObject(
                            $measureDate,
                            $targetMeasure,
                            $realizeMeasure,
                            $campainIndicator
                        )
                    );
            }
        } else if($campainIndicator->getTargetDate()){
            $targetMeasure = $this->createMeasureObject($campainIndicator->getTargetDate(), Measure::TYPE_TARGET, $campainObject, $campainIndicator, $indicator, null);
            $realizeMeasure = $this->createMeasureObject($campainIndicator->getTargetDate(), Measure::TYPE_DONE, $campainObject, $campainIndicator, $indicator, null);
            $campainIndicator->getCampainMeasures()->add(
                $this->createCampainMeasureObject(
                    $campainIndicator->getTargetDate(),
                    $targetMeasure,
                    $realizeMeasure,
                    $campainIndicator
                )
            );
        }  
    }

    private function createCampainMeasureObject($date, $targetMeasure, $realizedMeasure, $campainIndicator){
        $campainMeasureObject = new CampainMeasure();
        $campainMeasureObject->setSupposedDate($date);
        $campainMeasureObject->setTargetMeasure($targetMeasure);
        $campainMeasureObject->setRealiseMeasure($realizedMeasure);
        $campainMeasureObject->setIndicator($campainIndicator);
        $campainMeasureObject->setCampain($campainIndicator->getCampain());
        $targetMeasure->setCampainMeasure($campainMeasureObject);
        $realizedMeasure->setCampainMeasure($campainMeasureObject);
        return $campainMeasureObject;
    }

    private function createMeasureObjectByEntityManager($date, $type, $campainObject, $campainIndicator, $indicator, $indicatorGroup){
        $controllerManager   = $this->getServiceLocator()->get('ControllerManager');
        $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');
        $controllerMeasure->setEntityManager($this->getEntityManager());
        $data['indicator'] = [];
        $data['indicator']['id'] = $indicator->getId();
        $data['territories'] = [];
        $data['type'] = $type;
        $data['date'] = $date;
        $data['_rights'] = [];
        $data['_rights']['update'] = true;
        $measureObject = null;
        //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . "Build data - ". json_encode($data) . PHP_EOL, FILE_APPEND);
        
        $controllerMeasure->setColumns(["id"]);
        $jsonObject = $controllerMeasure->create($data);
        $measureData = $jsonObject->getVariable("object");
        //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . "Create measure - ". json_encode($measureData) . '- success: \''. $jsonObject->getVariable("success") . '\' - error: \''. $jsonObject->getVariable("error"). '\'' . PHP_EOL, FILE_APPEND);
        if(isset($measureData['id'])){
            $measureObject = $this->getEntityManager()
            ->getRepository('Indicator\Entity\Measure')
            ->findOneBy(['id' => $measureData['id']]);
            //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . "Retrieved measure - Id:". $measureObject->getId() . PHP_EOL, FILE_APPEND);
            
            $measureObject->setCampain($campainObject);
            $measureObject->setCampainIndicator($campainIndicator);
            $measureObject->setGroupIndicator($indicatorGroup);
        }
        return $measureObject;
    }


    private function createMeasureObject($date, $type, $campainObject, $campainIndicator, $indicator, $indicatorGroup){
        if($this->environment()->getClient()->isMaster()){
            return $this->createMeasureObjectByEntityManager($date, $type, $campainObject, $campainIndicator, $indicator, $indicatorGroup);
        } 

        $measureObject = new Measure();
        $measureObject->setType($type);
        $client = $this->environment()->getClient(); 
        $measureObject->setClient($client);
        $measureObject->setCampain($campainObject);
        $measureObject->setIndicator($indicator);
        $measureObject->setCampainIndicator($campainIndicator);
        $measureObject->setGroupIndicator($indicatorGroup);        
        return $measureObject;
    }

    private function getNextDateByFrequency($date, $frequency, $value){
        switch($frequency){
            case Indicator::FREQUENCY_WEEKLY:
                return $date->modify('+'.$value.' week');
            case Indicator::FREQUENCY_MONTHLY:
                return $date->modify('+'.$value.' month');
            case Indicator::FREQUENCY_TRIMONTHLY:
                return $date->modify('+'.(3*$value).' month');
            case Indicator::FREQUENCY_BIMANNUALLY:
                return $date->modify('+'.(6*$value).' month');
            case Indicator::FREQUENCY_ANNUALLY:
                return $date->modify('+'.$value.' year');
            default: throw new BadMethodCallException('Invalid frequency');
            break;
        }
    }

    /**
     * @param Indicator\Entity\GroupIndicator $indicatorGroup
     */
    private function createMeasuresByIndicatorGroup($indicatorGroup, $campainIndicator){
        // On supprime les mesures non modifiées
        $em = $this->getEntityManager();
        $em ->createQuery('DELETE Indicator\Entity\Measure m WHERE m.groupIndicator = :group_id AND m.campainIndicator = :campain_indicator_id AND m.updatedAt is NULL')
        ->setParameter('group_id', $indicatorGroup->getId())
        ->setParameter("campain_indicator_id", $campainIndicator->getId())
        ->execute();

        foreach($indicatorGroup->getIndicators() as $indicator){
            $this->createMeasuresByIndicator($indicator, $indicatorGroup, $campainIndicator);
        }
    }

}
