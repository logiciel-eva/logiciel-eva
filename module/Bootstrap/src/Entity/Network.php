<?php

namespace Bootstrap\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Network
 *
 * A "Network" represents is a group of clients.
 *
 * @package Bootstrap\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="eva_network")
 */
class Network extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Bootstrap\Entity\Client", mappedBy="network")
     */
    protected $clients;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="Bootstrap\Entity\Client")
     */
    protected $master;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @param ArrayCollection $clients
     */
    public function setClients($clients)
    {
        $this->clients = $clients;
    }

    public function addClients($clients)
    {
        foreach ($clients as $client) {
            $client->setNetwork($this);
            $this->clients->add($client);
        }
    }

    public function removeClients($clients)
    {
        foreach ($clients as $client) {
            $client->setNetwork(null);
            $this->clients->removeElement($client);
        }
    }

    /**
     * @return Client
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * @param Client $master
     */
    public function setMaster($master)
    {
        $this->master = $master;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'clients',
                'required' => false,
            ],
            [
                'name'     => 'master',
                'required' => false,
            ],
        ]);

        return $inputFilter;
    }
}
