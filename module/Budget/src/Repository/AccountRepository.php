<?php

namespace Budget\Repository;

use Doctrine\ORM\EntityRepository; 

class AccountRepository extends EntityRepository {


    public function resetDefault($type)
    {
        $this->createQueryBuilder('a')
            ->update()
            ->set('a.default', 'false')
            ->where('a.type = ?1')
            ->setParameter(1, $type)
            ->getQuery()
            ->execute();
    }

}