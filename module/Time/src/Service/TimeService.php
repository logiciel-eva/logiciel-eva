<?php

namespace Time\Service;

use Budget\Entity\BudgetCode;
use Core\Model\CurlAuthentication;
use Core\Utils\CurlUtils;
use Doctrine\ORM\EntityManager;
use Doctrine\Laminas\Hydrator\DoctrineObject as DoctrineHydrator;
use Exception;
use Core\Utils\ICalParser\ICalParser;
use Project\Entity\Project;
use Time\Entity\Synchronisation;
use Time\Entity\Timesheet;
use User\Entity\User;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Keyword\Entity\Association;
use Field\Entity\Value;

class TimeService
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected $translator;

    protected $modulesConfiguration;
    /**
     * @var DoctrineHydrator
     */
    private $hydrator;

    public function __construct(
        $entityManager,
        $serviceLocator
    )
    {
        $this->entityManager = $entityManager;
        $this->serviceLocator = $serviceLocator;
        $this->translator = $serviceLocator->get('ViewHelperManager')->get('translate');
    }

    /**
     * Import all calendars automatically.
     * This is an COMPLET import (without dates interval).
     *
     * @return array
     * @throws Exception
     */
    public function importCalendars()
    {
        $countTotal = 0;
        $countSuccess = 0;
        $countCreated = 0;
        $countUpdated = 0;
        $errors = [];

        // get all sync. configurations
        $syncs = $this->entityManager->getRepository('Time\Entity\Synchronisation')->createQueryBuilder('o')
             ->join('o.users', 'users')
            // andWhere : prevent user's mistake. database dont have necessaries constraints!
            ->where('o.link LIKE \'http%\'')
            ->andWhere('o.type IS NOT NULL')
            ->andWhere('o.config IS NOT NULL')
            ->andWhere('o.cronSchedule IS NOT NULL')
            // we groupBy link and conrUser for prevent multiple sync on same user and calendar
            ->groupBy('o.link')
            ->addGroupBy('users')
            ->getQuery()
            ->getResult();

        if (!empty($syncs)) {

            // keep only synchronisation to exec from scheduler rules
            /** @var Synchronisation[] $syncs */
            $syncs = array_filter($syncs, function ($sync) {
                return $sync->canSyncOnCronByScheduleRules();
            });

            if (!empty($syncs)) {
                // extract ids and update sync date on entities
                $syncIds = [];
                foreach ($syncs as $sync) {
                    $syncIds[] = $sync->getId();
                    $sync->setCronLastSync(new \DateTime());
                }
                $this->entityManager->flush();

                $parseds = $this->parseIcsAndLink($syncIds);

                if (!empty($parseds) && array_key_exists('timesheets', $parseds) && !empty($parseds['timesheets'])) {
                    // keep only events with project relationship
                    $parseds['timesheets'] = array_filter($parseds['timesheets'], function ($time) {
                        return isset($time['project']) && isset($time['project']['id']);
                    });

                    foreach ($parseds['timesheets'] as $timesheet) {
                        $countTotal++;
                        try {
                            list($timesheet, $countUpdated, $countCreated, $countSuccess) = $this->persistTimesheet($timesheet, $countUpdated, $countCreated, $countSuccess);
                        } catch (Exception $err) {
                            $errors[] = 'Synchronisation id=' . $timesheet['synchronisation']['id'] . ' : Fail to persist = ' . $err->getMessage();
                        }
                    }
                }
            }

        }

        return [
            'countTotal' => $countTotal,
            'countSuccess' => $countSuccess,
            'countCreated' => $countCreated,
            'countUpdated' => $countUpdated,
            'errors' => [
                'process' => isset($parseds['error']) ? (is_array($parseds['error']) ? $parseds['error'] : [$parseds['error']]) : [],
                'persist' => $errors,
            ]
        ];
    }

    /**
     * Parse Api (ics) or ics file from synchronisation's config.
     *
     * @param array $synchronisationIds (mandatory) synchronisation's ids template
     * @param User[] $users (optional) user's to attach calendar events. this parameter override synchronisation user's attributions (see cronUsers)
     * @param string $start (optional) date on format 'd/m/Y'
     * @param string $end (optional) date on format 'd/m/Y'
     * @param false $ics (optional) is ics file or not
     * @param array $config (optional) ics file configuration
     * @param null $fileContent (optional) ics file content
     * @return array|string[]
     * @throws Exception
     */
    public function parseIcsAndLink($synchronisationIds, $users = [], $start = false, $end = false, $ics = false, $config = [], $fileContent = null)
    {
        $isEmptyUsersOnInit = empty($users);

        if ($ics) {
            $file = base64_decode(preg_replace('#(.*base64,)#', '', $fileContent));
            $file = mb_convert_encoding($file, 'UTF-8', mb_detect_encoding($file, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
            $synchronisationIds = ['file'];

            if (!$file) {
                return ['error' => 'Fichier ICS manquant'];
            }
        }

        if (null === $users) {
            return ['error' => 'Utilisateur manquant'];
        }

        if (!$synchronisationIds) {
            return ['error' => 'Config de la synchronisation manquante'];
        }

        if ($start) {
            $start = \DateTime::createFromFormat('d/m/Y', $start);
            $start->setTime(0, 0, 0);
        }
        if ($end) {
            $end = \DateTime::createFromFormat('d/m/Y', $end);
            $end->setTime(23, 59, 59);
        }

        $timesheets = [];
        $syncErrors = [];

        foreach ($synchronisationIds as $id) {
            try {
                $cal = new ICalParser();

                $events = [];
                $sync = null;

                if ($ics) {
                    if (empty($users)) {
                        return ['error' => 'Utilisateur(s) manquant(s)'];
                    }
                    try {
                        $cal->parseString($file);
                    } catch (Exception $e) {
                        preg_match('/\([^()].*\)/', $e->getMessage(), $error);
                        return ['error' => $this->translator('timezone_error') . $error[0]];
                    }
                    $events = $cal->getSortedEvents();
                } else {
                    /** @var Synchronisation $sync */
                    $sync = $this->entityManager->getRepository('Time\Entity\Synchronisation')->find($id);

                    if (!is_null($sync->getTimezone())) {
                        $cal->fallbackTimeZone = $sync->getTimezone();
                    }

                    if ($sync->getType() === Synchronisation::SYNC_TYPE_ZIMBRA_REST) {
                        $events = $this->zimbraToCalendar($sync, $start, $end);
                    } else {
                        $content = CurlUtils::getContent($sync->getLink());
                        $cal->parseString($content);
                        $events = $cal->getSortedEvents();
                    }

                    if ($isEmptyUsersOnInit) {
                        $users = [];
                        // use cron's users as destination
                        foreach ($sync->getUsers() as $cronUser) {
                            $user = $this->entityManager->getRepository('User\Entity\User')->find($cronUser->getId());
                            if (!empty($user)) {
                                $users[] = $user;
                            }
                        }
                        // impl dateStart and dateEnd for cron job
                        $start = $sync->getSyncDateStartOnCron();
                    }
                }

                if (isset($sync) && !is_null($sync->getTimezone())) {
                    $timezone = $sync->getTimezone();
                } else {
                    $timezone = isset($cal->data['X-WR-TIMEZONE']) ? $cal->data['X-WR-TIMEZONE'] : null;
                }

                if ($timezone) {
                    try {
                        $timezone = new \DateTimeZone($timezone);
                    } catch (\Exception $e) {
                    }
                }
                if (!$timezone instanceof \DateTimeZone) {
                    $timezone = null;
                }

                foreach ($users as $user) {
                    $timesheets = $this->parseIcsEvents($events, $start, $end, $user, $ics, $sync, $timezone, $config, $timesheets);
                }
            } catch (Exception $err) {
                $syncErrors[] = 'Synchronisation id=' . $id . ' : ' . $err->getMessage();
            }
        }

        $response = [
            'timesheets' => $timesheets,
            'rows' => $synchronisationIds,
        ];

        if (count($syncErrors) > 0) {
            $response['error'] = $syncErrors;
        }
        return $response;
    }

    private function clean($string) {
        $string = str_replace(array("\r", "\n"), ' ', $string);
        $string = preg_replace('/[^A-Za-z0-9\-2éèàùêô.?=+&_ :\/]+/', '', $string); // Removes special chars.
        return $string;
     }
    /**
     * Parse one event.
     *
     * @param array $events
     * @param \DateTime $start
     * @param \DateTime $end
     * @param $user
     * @param bool $ics
     * @param Synchronisation $sync
     * @param \DateTimeZone|null $timezone
     * @param array $config
     * @param array $timesheets
     * @return array
     */
    protected function parseIcsEvents($events, $start, $end, $user, $ics, $sync, $timezone, $config, $timesheets)
    {
        foreach ($events as $r) {
            if (
                !isset($r['DTSTART']) ||
                !isset($r['DTEND']) ||
                !isset($r['UID'])
            ) {
                continue;
            }

            if ($start instanceof \DateTime && $start > $r['DTSTART']) {
                continue;
            }

            if ($end instanceof \DateTime && $end < $r['DTEND']) {
                continue;
            }
            $requestArguments = [
                'syncHash' => $r['UID'],
                'user' => $user->getId(),
            ];
            if(isset($r['RECURRING'])){
                $requestArguments= [
                    'syncHash' => $r['UID'],
                    'start' => $r['DTSTART'],
                    'end' => $r['DTEND'],
                    'user' => $user->getId(),
                ];
            }
            $timesheet = $this->entityManager->getRepository('Time\Entity\Timesheet')->findOneBy($requestArguments);

            if (!$timesheet) {
                $timesheet = new Timesheet();
                $timesheet->setSyncHash($r['UID']);
                $timesheet->setType(Timesheet::TYPE_DONE);
            }

            if (isset($r['SUMMARY'])) {
                $timesheet->setName($r['SUMMARY']);
            }

            if (isset($r['DESCRIPTION'])) {
                $description = $r['DESCRIPTION'];
                $description =$this->clean($description);
                $decriptionMaxLength= -1;
                if($sync){
                    $decriptionMaxLength=$sync->getDescriptionMaxLength();
                }
                if(isset($description) && isset($decriptionMaxLength) && $decriptionMaxLength>0){
                    $description = substr($description, 0, $decriptionMaxLength);
                    $description = htmlspecialchars(json_encode($description), ENT_QUOTES, 'UTF-8');
                }
                
                if(isset($decriptionMaxLength) && $decriptionMaxLength>0){
                    $timesheet->setDescription($description);
                }
            }

            $timesheet->setStart($r['DTSTART']);
            $timesheet->setEnd($r['DTEND']);
            if (!$ics) {
                $hours = $sync->getSettingsTime() ? $sync->getSettingsTime()->getHours() : null;
                $timesheet->calcHours($hours);
            } else {
                $timesheet->calcHours();
            }
            if ($timezone) {
                if ($timesheet->getStart()->getTimezone()->getName() === 'Z') {
                    $timesheet->getStart()->setTimezone($timezone);
                }
                if ($timesheet->getEnd()->getTimezone()->getName() === 'Z') {
                    $timesheet->getEnd()->setTimezone($timezone);
                }
            }

            $arr = [
                'id' => $timesheet->getId(),
                'name' => $timesheet->getName(),
                'start' => $timesheet->getStart()->format('d/m/Y H:i'),
                'end' => $timesheet->getEnd()->format('d/m/Y H:i'),
                'hours' => $timesheet->getHours(),
                'type' => $timesheet->getType(),
                'absenceType' => $timesheet->getAbsenceType()
                    ? [
                        'id' => $timesheet->getAbsenceType()->getId(),
                        'name' => $timesheet->getAbsenceType()->getName(),
                    ]
                    : null,
                'description' =>  $timesheet->getDescription() ? 
                                    $timesheet->getDescription() :
                                    null,
                'keywords' => new \stdClass(),
                'territories' => [],
                'syncHash' => $timesheet->getSyncHash(),
                'user' => [
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                ],
            ];

            if (!$ics) {
                $timesheet->setSynchronisation($sync);
                $arr += [
                    'synchronisation' => [
                        'id' => $sync->getId(),
                        'name' => $sync->getName(),
                    ],
                ];
            }

            /**
             * Gère la récupération du projet dans les infos
             * de la timesheet envoyée via l'ICS.
             */
            if ($this->moduleIsActive('project')) {

                if (!$ics) {
                    $config = $sync->getConfig();
                    if (!$config) {
                        throw new Exception('Synchronisation config is NULL');
                    }
                }

                if (isset($config['field']) && isset($config['join'])) {
                    if ($config['field'] != '0' && $config['join'] != '0') {
                        $field = strtoupper($config['field']);

                        if (isset($r[$field])) {
                            $elements = $r[$field];
                            if (!in_array($config['field'], [Synchronisation::SYNC_CONFIG_FIELD_TAGS])) {
                                $elements = [$r[$field]];
                            }

                            foreach ($elements as $element) {
                                $projects = [];

                                // To not take into account Hangout text, now when there is a line break,
                                // we only take the first line
                                if (strpos($element, "\n") !== false) {
                                    $element = strstr($element, "\n", true);
                                }

                                /**
                                 * On cherche à savoir si on récupère tout
                                 * ou seulement ce qu'il y a entre crochets [...]
                                 */
                                if (isset($config['closing'])) {
                                    $matches = [];

                                    // [MY_CLOSING]
                                    if ($config['closing'] == Synchronisation::SYNC_CONFIG_CLOSING_BRACKETS) {
                                        preg_match('/\[(.*)\]/U', $element, $matches);
                                        if ($matches) {
                                            $element = $matches[1];
                                        } else {
                                            break;
                                        }
                                    }
                                    // #MY_CLOSING
                                    else if ($config['closing'] == Synchronisation::SYNC_CONFIG_CLOSING_HASHTAGS) {
                                        if (0 === preg_match('/#(.*) /', $element, $matches)) {
                                            preg_match('/#(.*)/', $element, $matches);
                                        }
                                        if ($matches) {
                                            $element = explode(' ', $matches[1])[0];
                                        } else {
                                            break;
                                        }
                                    }
                                }

                                if($config['join'] == Synchronisation::SYNC_CONFIG_JOIN_CODE) {
                                    $project = $this->entityManager->getRepository(Project::class)->findOneBy(['code' => $element]);
                                    if(!isset($project)) {
                                        // try to find by budget code
                                        $queryBuilder = $this->entityManager->createQueryBuilder();
                                        $queryBuilder
											->select('o')
											->from(BudgetCode::class, 'o')
											->where('o.name = :code')
											->setParameters([
												'code' => $element,
											]);

										try {
											$entity = $queryBuilder->getQuery()->getOneOrNullResult();
                                            if(isset($entity)) {
                                                $project = $entity->getProject();
                                            }
										} catch (\Exception $e) {
											// do nothing
										}
                                    }

                                    if(isset($project)) {
                                        $projects[] = $project;
                                    }
                                } elseif ($config['join'] == Synchronisation::SYNC_CONFIG_JOIN_NAME) {
                                    $projects = $this->entityManager->getRepository(Project::class)->findBy([$config['join'] => $element]);
                                } elseif ($config['join'] == Synchronisation::SYNC_CONFIG_JOIN_CUSTOM) {
                                    $fieldObj = $this->entityManager->getRepository('Field\Entity\Field')->find($config['custom']);
                                    $values = $this->entityManager->getRepository('Field\Entity\Value')->findBy([
                                        'entity' => Project::class,
                                        'value' => $element,
                                        'field' => $fieldObj,
                                    ]);
                                    if (count($values) == 1) {
                                        $projects[] = $this->entityManager->getRepository($values[0]->getEntity())->find($values[0]->getPrimary());
                                    }
                                }

                                if (count($projects) == 1) {
                                    $timesheet->setProject($projects[0]);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if ($timesheet->getProject()) {
                $arr['project'] = [
                    'id' => $timesheet->getProject()->getId(),
                    'code' => $timesheet->getProject()->getCode(),
                    'name' => $timesheet->getProject()->getName(),
                ];
            }


            $keywords = [];
            $tmpKeywords = [];
            if (isset($r['DESCRIPTION'])) {
                $descriptionKeywords = preg_split('(,|;|\\s+(?![^\[]*\]))', $r['DESCRIPTION']);
                foreach ($descriptionKeywords as &$dk) {
                    $dk = str_replace('[', '', str_replace(']', '', $dk));
                }

                $timesheetKeywords = $this->entityManager->getRepository('Keyword\Entity\Keyword')->findByGroupEntity('timesheet');
                foreach ($timesheetKeywords as $timesheetKeyword) {
                    if (preg_grep("/" . $timesheetKeyword->getName() . "/i", $descriptionKeywords)
                        && !preg_grep("/" . $timesheetKeyword->getId() . "/i", $tmpKeywords)) {
                        $tmpKeywords[] = $timesheetKeyword->getId();
                        $keywords[$timesheetKeyword->getGroup()->getId()][] = [
                            'id' => $timesheetKeyword->getId(),
                            'name' => $timesheetKeyword->getName(),
                        ];
                    }
                }
            }

            if (isset($sync) && $sync->getParseTitle() && isset($r['SUMMARY'])) {
                $summaryWords = preg_split('(,|;|\\s+(?![^\[]*\]))', $r['SUMMARY']);
                foreach ($summaryWords as &$sw) {
                    $sw = str_replace('[', '', str_replace(']', '', $sw));
                }

                $timesheetKeywords = $this->entityManager->getRepository('Keyword\Entity\Keyword')->findByGroupEntity('timesheet');
                foreach ($timesheetKeywords as $timesheetKeyword) {
                    if (preg_grep("/" . $timesheetKeyword->getName() . "/i", $summaryWords)
                        && !preg_grep("/" . $timesheetKeyword->getId() . "/i", $tmpKeywords)) {
                        $tmpKeywords[] = $timesheetKeyword->getId();
                        $keywords[$timesheetKeyword->getGroup()->getId()][] = [
                            'id' => $timesheetKeyword->getId(),
                            'name' => $timesheetKeyword->getName(),
                        ];
                    }
                }

                $absenceTypes = $this->entityManager->getRepository('Time\Entity\AbsenceType')->findAll();
                foreach ($absenceTypes as $absenceType) {
                    if (preg_grep("/" . $absenceType->getName() . "/i", $summaryWords)) {
                        $arr['type'] = Timesheet::TYPE_ABSENCE;
                        $arr['absenceType'] = [
                            'id' => $absenceType->getId(),
                            'name' => $absenceType->getInitialLetters(),
                        ];
                    }
                }
            }

            $territories = [];
            if (isset($r['LOCATION']) && !(isset($field) && 'LOCATION' === $field)) {
                $matchingTerritories = $this->entityManager->getRepository('Map\Entity\Territory')->findBy([
                    'name' => $r['LOCATION']
                ]);
                if (count($matchingTerritories) > 0) {
                    $territories[] = [
                        'id' => $matchingTerritories[0]->getId(),
                        'name' => $matchingTerritories[0]->getName(),
                    ];
                }
            }


            if ($timesheet->getId()) {
                // Mots clefs
                $associations = $this->entityManager->getRepository('Keyword\Entity\Association')->findBy([
                    'entity' => Timesheet::class,
                    'primary' => $timesheet->getId(),
                ]);

                foreach ($associations as $association) {
                    $keyword = $association->getKeyword();
                    $group = $keyword->getGroup();

                    if (!array_key_exists($group->getId(), $keywords)) {
                        $keywords[$group->getId()] = [];
                    }

                    if ($this->isDataMapped($keywords[$group->getId()], $keyword->getId())) {
                        continue;
                    }

                    $keywords[$group->getId()][] = [
                        'id' => $keyword->getId(),
                        'name' => $keyword->getName(),
                    ];
                }


                // Territoires
                $tAssociations = $this->entityManager->getRepository('Map\Entity\Association')->findBy([
                    'entity' => Timesheet::class,
                    'primary' => $timesheet->getId(),
                ]);

                foreach ($tAssociations as $association) {
                    $territory = $association->getTerritory();
                    if ($this->isDataMapped($territories, $territory->getId())) {
                        continue;
                    }
                    $territories[] = [
                        'id' => $territory->getId(),
                        'name' => $territory->getName(),
                    ];
                }
            }

            $arr['keywords'] = count($keywords) > 0 ? $keywords : new \stdClass();
            $arr['territories'] = count($territories) > 0 ? $territories : [];
            $timesheets[] = $arr;
        }
        return $timesheets;
    }

    protected function isDataMapped($data, $id)
    {
        foreach ($data as $datum) {
            if ($datum['id'] === $id) {
                return true;
            }
        }
        return false;
    }

    protected function zimbraToCalendar($sync, $start = false, $end = false)
    {
        $events = [];

        $config = $sync->getConfig();
        if (isset($config['username']) && isset($config['password'])) {
            $username = $config['username'];
            $password = $config['password'];
            $curlAuthentication = new CurlAuthentication($username, $password);

            $url = rtrim($sync->getLink(), '/') . '/home/' . $username . '/calendar?auth=ba&fmt=json';
            if($start){
                $url = $url . '&start=' . $start->format('Y-m-d');
            }

            if($end){
                $url = $url . '&end=' . $end->format('Y-m-d');
            }

            $content = CurlUtils::getContent($url, $curlAuthentication);

            $data = json_decode($content);
            if(isset($data) && isset($data->appt)){
                foreach ($data->appt as $appt) {

                    if (!isset($appt->inv[0]->comp[0]->s) || !isset($appt->inv[0]->comp[0]->e)) {
                        continue;
                    }

                    $_start = $appt->inv[0]->comp[0]->s[0];
                    $_end = $appt->inv[0]->comp[0]->e[0];

                    $start = new \DateTime($_start->d);
                    $end = new \DateTime($_end->d);

                    $event = [
                        'UID' => $appt->uid,
                        'SUMMARY' => isset($appt->inv[0]->comp[0]->name) ? $appt->inv[0]->comp[0]->name : '',
                        'DESCRIPTION' => isset($appt->inv[0]->comp[0]->desc[0]->_content) ? $appt->inv[0]->comp[0]->desc[0]->_content : '',
                        'DTSTART' => $start,
                        'DTEND' => $end,
                        'TAGS' => isset($appt->tn) ? explode(',', $appt->tn) : [],
                    ];

                    $events[] = $event;
                }
            }
        }

        return $events;
    }

    protected function getTimesheetHydrator()
    {
        if ($this->hydrator === null) {
            $this->hydrator = new DoctrineHydrator($this->entityManager);
        }

        return $this->hydrator;
    }

    /**
     * @param $moduleName
     * @return bool
     */
    public function moduleIsActive($moduleName)
    {
        if (empty($this->modulesConfiguration)) {
            return $this->serviceLocator->get('MyModuleManager')->isActive($moduleName);
        }
        return $this->modulesConfiguration[$moduleName]['active'];
    }

    public function setModulesConfiguration($modulesConfiguration)
    {
        $this->modulesConfiguration = $modulesConfiguration;
    }

    /**
     * @param $timesheet
     * @param int $countUpdated
     * @param int $countCreated
     * @param int $countSuccess
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persistTimesheet($timesheet, int $countUpdated, int $countCreated, int $countSuccess): array
    {
        $object = $timesheet['id'] ?
            $this->entityManager->getRepository('Time\Entity\Timesheet')->find($timesheet['id']) :
            new Timesheet();
        if (null === $object) {
            throw new Exception('Timesheet id=' . $timesheet['id'] . ' not found');
        }
        $inputFilter = $object->getInputFilter($this->entityManager);
        if ($timesheet['id']) {
            $timesheet = array_merge($this->getTimesheetHydrator()->extract($object), $timesheet);
        }
        $inputFilter->setData($timesheet);

        if ($inputFilter->isValid()) {
            $this->getTimesheetHydrator()->hydrate($inputFilter->getValues(), $object);
            if ($timesheet['id']) {
                $object->setUpdatedAt(new \DateTime());
            } else {
                $object->setCreatedAt(new \DateTime());
            }
            // persist and flush every time of non blocked action for other user/calendar
            $this->entityManager->persist($object);
            $this->entityManager->flush();
            
            $this->saveKeywords($object, $timesheet, 'Time\Entity\Timesheet');
            $this->saveTerritories($object, $timesheet, 'Time\Entity\Timesheet');
            
            if ($timesheet['id']) {
                $countUpdated++;
            } else {
                $countCreated++;
            }
            $countSuccess++;
        } else {
            throw new Exception(implode(' / ', $inputFilter->getMessages()));
        }
        return array($timesheet, $countUpdated, $countCreated, $countSuccess);
    }
  
	/**
	 * Saving keywords linked to the record.
	 *
	 * Externalized in a specific function because they are no direct relation between the record and the keywords,
	 * so we can"t use the create/update function as they are.
	 *
	 * @param mixed $object
	 * @param array $data
	 * @param? string $entityClass
	 */
	public function saveKeywords($object, $data, $entityClass = null)
	{
		$repositoryKeyword =$this->entityManager->getRepository('Keyword\Entity\Keyword');
		$repositoryGroup = $this->entityManager->getRepository('Keyword\Entity\Group');
        if (isset($data['master'])){
            $tmpKeywords = [];
			foreach ($data['keywords'] as $key => $group) {
				$tmpGroup = $repositoryGroup->findOneBy(['id'=>$key]);
				if ($tmpGroup) {
					$groupId = $tmpGroup->getId();
					$tmpKeywords[$groupId] = [];
                    if(!is_array($group) || isset($group['id'])) {
                        $group = [$group];
                    }

					foreach ($group as $keyword) {
						$tmpKeyword = $repositoryKeyword->findOneBy(['id'=>$keyword['id']]);
						$keyword['id'] = $tmpKeyword->getId();
						$tmpKeywords[$groupId][] = $keyword;
					}
				}
			}
			$data['keywords'] = $tmpKeywords;
		}
        
		if (isset($data['keywords'])/* && $moduleManager->hasKeywords($entityClass)*/) {
			$removeQuery =$this->entityManager->createQuery(
				'DELETE FROM Keyword\Entity\Association a WHERE a.entity = :entity AND a.primary = :primary'
			);
			$removeQuery->setParameters([
				'entity' => $entityClass,
				'primary' => $object->getId(),
			]);
			$removeQuery->execute();

			foreach ($data['keywords'] as $group => $keywords) {
				$groupEntity = $repositoryGroup->find($group);

				if (!$groupEntity->isMultiple()) {
					if (isset($keywords['id'])) {
						$keywords = [$keywords];
					} elseif (sizeOf($keywords) > 1) {
						$keywords = [$keywords[0]];
					}
				}
				if ($keywords) {
					foreach ($keywords as $keyword) {
						$association = new Association();
						$association->setEntity($entityClass);
						$association->setPrimary($object->getId());
						$association->setKeyword($repositoryKeyword->find($keyword['id']));

						$this->entityManager->persist($association);
					}
				}
			}

			$this->entityManager->flush();
		}
	}
    
    /**
	 * Saving territories linked to the record.
	 *
	 * Externalized in a specific function because they are no direct relation between the record and the territories,
	 * so we can"t use the create/update function as they are.
	 *
	 * @param mixed $object
	 * @param array $data
	 */
	public function saveTerritories($object, $data, $entityClass = null)
	{
		if (isset($data['territories'])) {
			$repositoryTerritory = $this->entityManager->getRepository('Map\Entity\Territory');

			$removeQuery = $this->entityManager->createQuery(
				'DELETE FROM Map\Entity\Association a WHERE a.entity = :entity AND a.primary = :primary'
			);
			$removeQuery->setParameters([
				'entity' => $entityClass,
				'primary' => $object->getId(),
			]);
			$removeQuery->execute();

			foreach ($data['territories'] as $territory) {
				$association = new \Map\Entity\Association();
				$association->setEntity($entityClass);
				$association->setPrimary($object->getId());
				$association->setTerritory($repositoryTerritory->find($territory['id']));

				$this->entityManager->persist($association);
			}

			$this->entityManager->flush();
		}
	}
}
