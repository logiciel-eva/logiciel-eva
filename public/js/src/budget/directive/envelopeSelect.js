(function(app) {

    app.directive('envelopeSelect', [
        'envelopeService', 'selectFactoryService',
        function envelopeSelect(envelopeService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: envelopeService,
                    apiParams: {
                        col: ['id', 'code', 'name']
                    },
                },
                selectize: {
                    searchField: ['name', 'code'],
                    valueField: 'id',
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' + (data.code ? escape(data.code) + ' - ' : '') + escape(data.name) + '</div>';
                        },
                        item: function (data, escape) {
                            return '<div class="option">' + (data.code ? escape(data.code) + ' - ' : '') + escape(data.name) + '</div>';
                        }
                    }
                }
            });
        }
    ])

})(window.EVA.app);
