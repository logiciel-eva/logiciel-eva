(function(app) {

    app.factory('clientService', [
        'restFactoryService', '$http', '$q',
        function clientService(restFactoryService, $http, $q) {
            return angular.extend({
            }, restFactoryService.create('/analysis/indicator/get-clients'));
        }
    ])

})(window.EVA.app);
