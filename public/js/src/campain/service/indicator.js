(function(app) {

    app.factory('campain-indicatorService', [
        'restFactoryService',
        function campainindicatorService(restFactoryService) {
            return restFactoryService.create('/api/campain-indicator');
        }
    ])

})(window.EVA.app);
