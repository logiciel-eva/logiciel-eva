<?php

namespace Indicator;

return [
    'doctrine' => [
        'driver' => [
            'campain_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Campain\Entity' => 'campain_entities'
                ]
            ]
        ],
    ]
];
