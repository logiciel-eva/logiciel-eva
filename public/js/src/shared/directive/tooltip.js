(function(app) {

    app.directive('tooltip', [
        function tooltip() {
            return {
                restrict: 'A',
                replace: false,
                compile: function () {
                    angular.element('body').on('click', function () {
                        angular.element('.tooltip').remove();
                    });
                    return function (scope, element, attributes, ngModelCtrl) {
                        if (typeof attributes.hideIfEmpty !== 'undefined' && new RegExp("(project_help_)").test(attributes.tooltip.trim())) {
                            element.hide();
                        }
                        element.tooltip({
                            title: attributes.tooltip,
                            delay: {
                                show: 500,
                                hide: 0
                            },
                            container: 'body'
                        }).on('show.bs.tooltip', function () {
                            angular.element('.tooltip').remove();
                        });
                    }
                }
            }
        }
    ])

})(window.EVA.app);
