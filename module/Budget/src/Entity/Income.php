<?php

namespace Budget\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Project;

/**
 * Class Income
 *
 * @package Budget\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="budget_income")
 */
class Income extends Transaction
{
    const TYPE_REQUESTED = 'requested';
    const TYPE_NOTIFIED  = 'notified';
    const TYPE_ASSIGNED  = 'assigned';
    const TYPE_PERCEIVED = 'perceived';
    const TYPE_DV        = 'dv';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $convention;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $conventionTitle;

    /**
     * @var PosteIncome
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\PosteIncome", inversedBy="incomes")
     */
    protected $poste;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $serie;

    /**
     * @var
     */
    protected $subventionAmount;

    /**
     * The link to the project when a budget transfer applies (evolution is ticked)
     *
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project\Entity\Project")
     */
    protected $transfer;

    /**
     * @return string
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param string $serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return string
     */
    public function getConvention()
    {
        return $this->convention;
    }

    /**
     * @param string $convention
     */
    public function setConvention($convention)
    {
        $this->convention = $convention;
    }

    /**
     * @return string
     */
    public function getConventionTitle()
    {
        return $this->conventionTitle;
    }

    /**
     * @param string $conventionTitle
     */
    public function setConventionTitle($conventionTitle)
    {
        $this->conventionTitle = $conventionTitle;
    }

    public function getNature()
    {
        return $this->getPoste()->getNature();
    }

    public function getOrganization()
    {
        return $this->getPoste()->getOrganization();
    }

    public function getDestination()
    {
        return $this->getPoste()->getDestination();
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name'     => 'type',
            'required' => true,
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => self::getTypes()
                    ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'serie',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
        ]);

        $inputFilter->add([
            'name'     => 'convention',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function ($value) use ($entityManager) {
                            if (is_array($value)) {
                                $value = $value['id'];
                            }
                            $account = $entityManager->getRepository('Convention\Entity\Convention')->find($value);


                            return $account !== null;
                        },
                        'message' => 'convention_field_account_error_non_exists'
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'conventionTitle',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
        ]);

        $inputFilter->add([
			'name' => 'transfer',
			'required' => false,
		]);

        $inputFilter->add([
            'name'     => 'amountHT',
            'required' => false,
            'filters' => [
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function ($value) {
                            // Si la valeur de amountHT est null, la convertir en 0
                            if ($value === null) {
                                return 0;
                            }
                            // Sinon, retourner la valeur originale
                            return $value;
                        },
                    ],
                ],
            ],
        ]);
        
        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_REQUESTED,
            self::TYPE_NOTIFIED,
            self::TYPE_ASSIGNED,
            self::TYPE_PERCEIVED,
            self::TYPE_DV
        ];
    }

    /**
     * Get the link to the project when a budget transfer applies (evolution is ticked)
     *
     * @return  Project
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * Set the link to the project when a budget transfer applies (evolution is ticked)
     *
     * @param  Project  $transfer  The link to the project when a budget transfer applies (evolution is ticked)
     *
     * @return  self
     */
    public function setTransfer(Project $transfer)
    {
        $this->transfer = $transfer;

        return $this;
    }
}
