(function(app) {
    app.controller('filterController', [  
		'$scope',
		'$timeout',
		 function ($scope, $timeout) {
			var self = this;
	
			self.init = function () {
					$timeout(function () {
						if (typeof $scope.__filters === 'undefined') {
							$scope.__filters = {};
						}
					});
			};
		}
		]);
    app.controller('conventionFormController', [
        'conventionService', 'conventionLineService', 'flashMessenger', 'translator', '$scope', '$timeout',
        function conventionFormController(conventionService, conventionLineService, flashMessenger, translator, $scope, $timeout) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'number',
                    'numberArr',
                    'contractor.id',
                    'contractor.name',
                    'progress',
                    'start',
                    'end',
                    'decisionDate',
                    'notificationDate',
                    'returnDate',
                    'amount',
                    'amountSubv',
                    'description',
                    'members.id',
                    'members.name',
                    'members.avatar',
                    'financers.id',
                    'financers.name',
                    'lines.id',
                    'lines.percentage',
                    'lines.project.id',
                    'lines.project.code',
                    'lines.project.name',
                    'lines.project.amountIncomeArboRequested',
                    'lines.project.financerRepartitionIncomeArboRequested',
                    'lines.project.timeSpent',
                    'lines.project.amountIncomeAutofinancementArboRequested',
                    'keywords',
                    'territories',
                    'networkAccessible'
                ]
            };

            var lineCols = [
                'id',
                'percentage',
                'project.id',
                'project.code',
                'project.name',
                'project.amountIncomeArboRequested',
                'project.financerRepartitionIncomeArboRequested',
                'project.timeSpent',
                'project.amountIncomeAutofinancementArboRequested',
            ];

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.convention = {
                financers: [],
                lines: [],
                _rights: {
                    update: true
                }
            };

            self.init = function init(id, expenseTypes) {
                for (var i in expenseTypes) {
                    apiParams.col.push('lines.project.amountExpenseArbo' + expenseTypes[i].ucfirst());
                    apiParams.col.push('lines.project.amountHTExpenseArbo' + expenseTypes[i].ucfirst());
                    lineCols.push('project.amountExpenseArbo' + expenseTypes[i].ucfirst());
                    lineCols.push('project.amountHTExpenseArbo' + expenseTypes[i].ucfirst());
                }

                if (id) {
                    conventionService.find(id, apiParams).then(function (res) {
                        self.convention   = res.object;
                        self.panelTitle = self.convention.name;
                    });
                }
            };

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(line) {
                self.editedLineErrors = {};
                self.editedLine       = line;
                self.editedLineFields = angular.copy(line);
            };

            self.saveEditedLine = function saveEditedLine(line) {

                self.editedLineErrors = {};
                conventionLineService.save(angular.extend({}, line, self.editedLineFields), {
                    col: lineCols
                }).then(function (res) {
                    if (res.success) {
                        line = angular.extend(line, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('convention_line_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteLine = function deleteLine(line) {
                if (confirm(translator('convention_line_question_delete').replace('%1', line.project.name))) {
                    conventionLineService.remove(line).then(function (res) {
                        if (res.success) {
                            self.convention.lines.splice(self.convention.lines.indexOf(line), 1);
                            flashMessenger.success(translator('convention_line_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.saveConvention = function saveConvention() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.convention.id;

                conventionService.save(self.convention, apiParams).then(function (res) {
                    self.convention     = res.object;
                    self.panelTitle   = self.convention.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.convention.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('convention_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteConvention = function deleteConvention() {
                if (confirm(translator('convention_question_delete').replace('%1', self.convention.name))) {
                    conventionService.remove(self.convention).then(function (res) {
                        if (res.success) {
                            window.location = '/convention';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.getTotalAmountIncomeRequested = function getTotalAmountIncomeRequested() {
                var amount = 0;
                for (var i in self.convention.lines) {
                    amount += self.convention.lines[i].project.amountIncomeArboRequested;
                }
                return amount;
            };

            self.getAmountFinancerProject = function getAmountFinancerProject(percentage, total, part) {
                var lineAmount = (total * percentage) / 100;
                return (lineAmount * part) / 100;
            };

            self.getAmountFinancerTotal = function getAmountFinancerTotal(financer) {
                var amount = 0;
                for (var i in self.convention.lines) {
                    var line = self.convention.lines[i];
                    if (typeof line.project.financerRepartitionIncomeArboRequested[financer.id] !== 'undefined') {
                        amount += self.getAmountFinancerProject(line.percentage, line.project.amountIncomeArboRequested, line.project.financerRepartitionIncomeArboRequested[financer.id].percentage);
                    }
                }
                return amount;
            };

            self.getPartTotalAmountIncomeRequested = function () {
                var amount = 0;
                for (var i in self.convention.lines) {
                    amount += (self.convention.lines[i].percentage * self.convention.lines[i].project.amountIncomeArboRequested) / 100;
                }
                return amount;
            };

            self.getTotalTimeSpent = function getAmountFinancerTotal() {
                var amount = 0;
                for (var i in self.convention.lines) {
                    var line = self.convention.lines[i];
                        amount += (line.project.timeSpent * line.percentage) / 100;
                }
                return amount;
            };

            self.getTotalAmountExpense = function getTotalAmountExpense(type, ht) {
                ht = ht ? 'HT' : '';

                var amount = 0;
                for (var i in self.convention.lines) {
                    var line = self.convention.lines[i];
                    amount += (line.project['amount' + ht + 'ExpenseArbo' + type.ucfirst()] * line.percentage) / 100;
                }
                return amount;
            };

            self.getTotalAmountIncomeAutofinancementRequested = function getTotalAmountIncomeAutofinancementRequested() {
                var amount = 0;
                for (var i in self.convention.lines) {
                    var line = self.convention.lines[i];
                    amount += (line.project.amountIncomeAutofinancementArboRequested * line.percentage) / 100;
                }
                return amount;
            };


            $scope.$watch(function () { return self.convention.lines }, function () {
                self.fixedColumn();
            }, true);

            angular.element('.convention-form-nav a').click(function (e) {
                self.fixedColumn();
            });

            self.fixedColumn = function fixedColumn () {
                $timeout(function() {
                    $('.fixed-column').remove();
                    var $table = $('#convention-line-table');
                    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');

                    $fixedColumn.find('th:not(:first-child),td:not(:first-child)').remove();

                    $fixedColumn.find('tr').each(function (i, elem) {
                        $(elem).height($table.find('tr:eq(' + i + ')').height());
                    });
                });
            };

            self.createConventionLine = function () {
                $scope.$broadcast('create-convention-line', {
                    convention: self.convention
                });
            };
        }
    ]);

})(window.EVA.app);
