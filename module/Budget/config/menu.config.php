<?php

$envelope = [
    'icon' => 'envelope',
    'title' => 'envelope_module_title',
    'href' => 'budget/envelope',
    'right' => 'budget:e:envelope:r',
    'priority' => 8,
];

$account = [
    'icon' => 'account',
    'title' => 'account_module_title',
    'href' => 'budget/account',
    'right' => 'budget:e:account:r',
    'priority' => 7,
];

$nature = [
    'icon' => 'nature',
    'title' => 'nature_module_title',
    'href' => 'budget/nature',
    'right' => 'budget:e:nature:u',
    'control' => [ 
                  'gbcp' => true
                ],
    'priority' => 6,
];

$organization = [
    'icon' => 'organization',
    'title' => 'organization_module_title',
    'href' => 'budget/organization',
    'right' => 'budget:e:organization:u',
    'control' => [ 
                  'gbcp' => true
                ],
    'priority' => 5,
];

$managementUnit = [
    'icon' => 'managementUnit',
    'title' => 'management_unit_module_title',
    'href' => 'budget/management-unit',
    'right' => 'budget:e:managementUnit:u',
    'priority' => 4,
];

$destination = [
    'icon' => 'destination',
    'title' => 'destination_module_title',
    'href' => 'budget/destination',
    'right' => 'budget:e:destination:u',
    'control' => [ 
                  'gbcp' => true
                ],
    'priority' => 3,
];

$budgetStatus = [
    'icon' => 'budgetStatus',
    'title' => 'budgetStatus_module_title',
    'href' => 'budget/budget-status',
    'right' => 'budget:e:budgetStatus:u',
    'module' => 'project',
    'priority' => 2,
];

$import = [
    'icon' => 'import',
    'title' => 'budget_import_module_title',
    'href' => 'budget/import',
    'module' => 'project',
    'priority' => 1,
];

$autoImport = [
    'icon' => 'refresh',
    'right' => 'budget:e:importConfiguration:r',
    'title' => 'budget_automated_import_module_title',
    'href' => 'budget/import-configuration',
    'module' => 'project',
    'priority' => 0,
];


return [
    'menu' => [
        'client-menu' => [
            'budget' => [
                'icon' => 'budget',
                'title' => 'budget_module_title',
                'priority' => 990,
                'children' => [
                    'envelope' => $envelope,
                    'account' => $account,
                    'nature' => $nature,
                    'organization' => $organization,
                    'managementUnit' => $managementUnit,
                    'destination' => $destination,
                    'budget-status' => $budgetStatus,
                    'import' => $import,
                    'import-configuration' => $autoImport,
                ],
            ],
        ],
        'client-menu-parc' => [
            'data' => [
                'priority' => 9997,
                'children' => [
                    'budget' => [
                        'icon' => 'budget',
                        'title' => 'budget_module_title',
                        'priority' => 990,
                        'children' => [
                            'envelope' => $envelope,
                            'account' => array_replace($account, ['icon' => 'account-parc']),
                            'nature' => $nature,
                            'organization' => $organization,
                            'managementUnit' => $managementUnit,
                            'destination' => $destination,
                            'budget-status' => array_replace($budgetStatus, ['icon' => 'budgetStatus-parc']),
                            'import' => $import,
                            'import-configuration' => $autoImport,
                        ],
                    ],
                ],
            ],
        ],
    ],
];
