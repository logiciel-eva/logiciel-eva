<?php

namespace Budget\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Directory\Entity\Structure;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Envelope
 *
 * @package Budget\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="budget_envelope")
 */
class Envelope extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var Structure
     *
     * @ORM\ManyToOne(targetEntity="Directory\Entity\Structure")
     */
    protected $financer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $end;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $amount;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Structure
     */
    public function getFinancer()
    {
        return $this->financer;
    }

    /**
     * @param Structure $financer
     */
    public function setFinancer($financer)
    {
        $this->financer = $financer;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'code',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'financer',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {

                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $financer = $entityManager->getRepository('Directory\Entity\Structure')->findBy([
                                    'id'       => $value,
                                    'financer' => true
                                ]);

                                return (!empty($financer));
                            },
                            'message' => 'envelope_field_financer_error_non_exists'
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                return (is_numeric($value) ||
                                    (is_array($value) && isset($value['id']) && is_int($value['id'])) ||
                                    (is_object($value) && method_exists($value, 'getId') && is_numeric($value->getId()))) ;
                            },
                            'message' => 'project_field_id_expected',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'start',
                'required' => false,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'     => 'end',
                'required' => false,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'     => 'amount',
                'required' => false,
                'filters'  => [
                    ['name' => 'NumberParse']
                ]
            ],
        ]);

        return $inputFilter;
    }
}
