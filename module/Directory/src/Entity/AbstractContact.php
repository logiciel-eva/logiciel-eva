<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 06/05/16
 * Time: 13:32
 */

namespace Directory\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractContact
 * @package Directory\Entity
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractContact extends \Core\Entity\Contact\AbstractContact
{
    const PERSON_GENDER_M   = 'm';
    const PERSON_GENDER_MME = 'mme';
    const PERSON_GENDER_NEUTRAL = 'neutral';

    /**
     * Email
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * Phone Number
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * Phone Portable Number
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $portable;

    /**
     * Zipcode
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $zipcode;

    /**
     * Address Line 1
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $addressLine1;

    /**
     * Address Line 2
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $addressLine2;

    /**
     * City
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $city;

    /**
     * Country
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $country;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPortable()
    {
        return $this->portable;
    }

    /**
     * @param string $mobile
     */
    public function setPortable($mobile)
    {
        $this->portable = $mobile;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param string $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param string $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name' => 'email',
            'required' => false,
            'validators' => [
                ['name' => 'EmailAddress'],
            ],
        ], 'email');

        $inputFilter->add([
            'name'     => 'gender',
            'required' => false,
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => self::getGenders()
                    ],
                ]
            ],
        ], 'gender');

        $inputFilter->add([
            'name' => 'phone',
            'required' => false,
        ], 'phone');

        $inputFilter->add([
            'name' => 'portable',
            'required' => false,
        ], 'portable');

        $inputFilter->add([
            'name' => 'zipcode',
            'required' => false,
            'validators' => [
                ['name' => 'Alnum'],
            ],
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
        ], 'zipcode');

        $inputFilter->add([
            'name' => 'addressLine1',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
        ], 'addressLine1');

        $inputFilter->add([
            'name' => 'addressLine2',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
        ], '');

        $inputFilter->add([
            'name' => 'city',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
        ], 'city');

        $inputFilter->add([
            'name' => 'country',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
        ], 'country');

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getGenders()
    {
        return [
            self::PERSON_GENDER_M,
            self::PERSON_GENDER_MME,
            self::PERSON_GENDER_NEUTRAL
        ];
    }
}
