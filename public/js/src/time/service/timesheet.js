(function(app) {

    app.factory('timesheetService', [
        'restFactoryService',
        function timesheetService(restFactoryService) {
            return restFactoryService.create('/api/timesheet');
        }
    ])

})(window.EVA.app);
