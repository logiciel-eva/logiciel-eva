(function(app) {

    app.controller('recoverController', [
        'controlService',
        function recoverController(controlService) {
            controlService.desactivate();
        }
    ])

})(window.EVA.app);
