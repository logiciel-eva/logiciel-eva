<?php

namespace User\Controller;

use Core\Controller\AbstractActionSLController;
use Project\Entity\Project;
use Laminas\View\Model\ViewModel;

class RoleController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('user:e:role:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel([]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('user:e:role', $id)) {
            return $this->notFoundAction();
        }

        $role = $this->entityManager()->getRepository('User\Entity\Role')->find($id);

        if ($role && !$this->acl()->ownerControl('user:e:role:u', $role) && !$this->acl()->ownerControl('user:e:role:r', $role)) {
            return $this->notFoundAction();
        }

        $modules = $this->moduleManager()->getActiveModules();

        return new ViewModel([
            'role'    => $role,
            'modules' => $modules,
            'projectStatuses' => Project::getPublicationStatutes()
        ]);
    }
}
