<?php

namespace Export\Entity;

use Bootstrap\Entity\Client;

class Custom
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $reference;

    /**
     * @var array
     */
    protected $params;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    public static function getExports($client, $entity)
    {
        $exports = [];
        $folder  = getcwd() . '/public/clients/' . $client . '/exports/' . $entity;
        foreach (glob($folder . '/*/export.json') as $configFile) {
            $export = new Custom();
            $config = json_decode(file_get_contents($configFile), true);

            preg_match('/\/([a-zA-Z-]+)\/export.json/', $configFile, $matches);
            $export->setKey($entity . '_' . $matches[1]);

            foreach ($config as $property => $value) {
                $export->{'set' . ucfirst($property)}($value);
            }
            $exports[] = $export;
        }

        return $exports;
    }

    public static function doExport(Client $client, $key, $params, $data, $em, $translator, $apiRequest)
    {
        $key = explode('_', $key);
        $folder  = getcwd() . '/public/clients/' . $client->getKey() . '/exports/' . $key[0] . '/' . $key[1];

        require_once($folder . '/Export.php');

        $export = new \Export($em, $translator, $apiRequest, $client);
        $export->doExport($params, $data);
    }
}