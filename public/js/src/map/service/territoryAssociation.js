(function(app) {

    app.factory('territoryAssociationService', [
        'restFactoryService',
        function territoryAssociationService(restFactoryService) {
            return restFactoryService.create('/api/map/association');
        }
    ])

})(window.EVA.app);
