(function(app) {

    app.factory('userService', [
        'restFactoryService',
        function userService(restFactoryService) {
            return restFactoryService.create('/api/user');
        }
    ])

})(window.EVA.app);
