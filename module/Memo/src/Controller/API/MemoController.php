<?php

namespace Memo\Controller\API;

use Core\Controller\BasicRestController;
use Memo\Entity\Memo;

class MemoController extends BasicRestController
{
    protected $entityClass = 'Memo\Entity\Memo';
    protected $repository  = 'Memo\Entity\Memo';
    protected $entityChain = 'force:e:force';

    public function getList($callbacks = [], $master = false)
    {
        $memo = $this->getEntityManager()->getRepository($this->repository)->findOneBy([
            'createdBy' => $this->authStorage()->getUser()->getId()
        ]);
        if (!$memo) {
            $memo = new Memo();
            $this->getEntityManager()->persist($memo);
            $this->getEntityManager()->flush();
        }

        return parent::get($memo->getId());
    }


    protected function searchList($queryBuilder, &$data)
    {
        $sort  = isset($data['sort'])  ? $data['sort']  : 'object.createdAt';
        $order = isset($data['order']) ? $data['order'] : 'DESC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->orderBy($sort, $order);
    }

    public function get($id, $callbacks = [])
    {
        $memo = $this->getEntityManager()->getRepository($this->repository)->findOneBy([
            'createdBy' => $this->authStorage()->getUser()->getId()
        ]);
        if (!$memo) {
            $memo = new Memo();
            $this->getEntityManager()->persist($memo);
            $this->getEntityManager()->flush();
        }

        return parent::get($memo->getId());
    }


    public function create($data)
    {
        $memo = $this->getEntityManager()->getRepository($this->repository)->findOneBy([
            'createdBy' => $this->authStorage()->getUser()->getId()
        ]);
        if (!$memo) {
            $memo = new Memo();
            $this->getEntityManager()->persist($memo);
            $this->getEntityManager()->flush();
        }

        return parent::update($memo->getId(), $data);
    }

    public function update($id, $data)
    {
        $memo = $this->getEntityManager()->getRepository($this->repository)->findOneBy([
            'createdBy' => $this->authStorage()->getUser()->getId()
        ]);
        if (!$memo) {
            $memo = new Memo();
            $this->getEntityManager()->persist($memo);
            $this->getEntityManager()->flush();
        }
        $data['id'] = $memo->getId();
        return parent::update($memo->getId(), $data);
    }
}
