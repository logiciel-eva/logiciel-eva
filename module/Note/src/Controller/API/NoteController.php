<?php

namespace Note\Controller\API;

use Core\Controller\BasicRestController;
use Laminas\Mvc\MvcEvent;

class NoteController extends BasicRestController
{
    protected $entityClass = 'Note\Entity\Note';
    protected $repository  = 'Note\Entity\Note';
    protected $entityChain = '';

    public function onDispatch(MvcEvent $event)
    {
        $this->entityChain = $this->params()->fromQuery('right', null);
        if ($this->entityChain === null) {
            $request = $event->getRequest();
            $data = $this->processBodyContent($request);
            if (!isset($data['right'])) {
                return $this->notFoundAction();
            }
            $this->entityChain = $data['right'];
        }

        return parent::onDispatch($event);
    }

    public function getList($callbacks = [], $master = false){
        $res = parent::getList();

        $res->setVariable('currentUser', $this->authStorage()->getUser()->getId());

        return $res;
    }

    protected function searchList($queryBuilder, &$data)
    {
        $sort  = isset($data['sort'])  ? $data['sort']  : 'object.createdAt';
        $order = isset($data['order']) ? $data['order'] : 'DESC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        //$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
        $queryBuilder->orderBy($sort, $order);
    }

    public function create($data)
    {
        $response = parent::create($data);

        if ($response->getVariable('success') === true) {
            if (isset($data['attachment']['file'])) {
                $noteArray = $response->getVariable('object');
                $note = $this->entityManager()->getRepository($this->repository)->find($noteArray['id']);

                $data['attachment']['primary'] = $note->getId();
                $data['attachment']['name']    = 'Note #' . $note->getId() . ' attachment';

                $attachmentController = $this->serviceLocator->get('ControllerManager')->get('Attachment\Controller\API\Attachment');
                $attachmentController->setEntityChain($this->entityChain);
                $attachmentController->setColumns(['id', 'name']);
                $attachmentResponse = $attachmentController->create($data['attachment']);
                if ($attachmentResponse->getVariable('success') === true) {
                    $attachmentArray = $attachmentResponse->getVariable('object');
                    $attachment      = $this->entityManager()->getRepository($attachmentController->getRepository())->find($attachmentArray['id']);

                    $note->setAttachment($attachment);
                    $this->entityManager()->flush();

                    /*$this->entityManager()->createQueryBuilder('n')
                                          ->update($this->repository, 'n')
                                          ->set('n.attachment', $attachment['id'])
                                          ->where('n.id = :noteId')
                                          ->setParameter('noteId', $note['id'])
                                          ->getQuery()
                                          ->execute(); */

                    $response->setVariable('object', $this->extract($note));
                }
            }
        }

        return $response;
    }
}
