<?php

namespace Map\Entity;

use Budget\Entity\Expense;
use Budget\Entity\Income;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use GeoJson\GeoJson;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Indicator\Entity\Measure;
use Time\Entity\Timesheet;
use Laminas\InputFilter\Factory;
use Laminas\InputFilter\InputFilter;

/**
 * Class Territory
 * @package Map\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="map_territory")
 */
class Territory extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code;

    /**
     * @var GeoJson
     *
     * @ORM\Column(type="json", nullable=true)
     */
    protected $json;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Map\Entity\Territory", mappedBy="children")
     */
    protected $parents;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Map\Entity\Territory", inversedBy="parents")
     * @ORM\JoinTable(name="map_territory_children",
     *      joinColumns={@ORM\JoinColumn(name="territory_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $children;

    /**
     * @var ArrayCollection
     */
    protected $projects;

    /**
     * @var ArrayCollection
     */
    protected $timesheets;
    /**
     * @var ArrayCollection
     */
    protected $measures;

    public function __construct()
    {
        $this->parents  = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return GeoJson
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @param GeoJson $json
     */
    public function setJson($json)
    {
        $this->json = $json;
    }

    /**
     * @return ArrayCollection
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * @param ArrayCollection $parents
     */
    public function setParents($parents)
    {
        $this->parents = $parents;
    }

    /**
     * @param ArrayCollection $parents
     */
    public function addParents($parents)
    {
        foreach ($parents as $parent) {
            $parent->getChildren()->add($this);
            $this->getParents()->add($parent);
        }
    }

    /**
     * @param ArrayCollection $parents
     */
    public function removeParents($parents)
    {
        foreach ($parents as $parent) {
            $parent->getChildren()->removeElement($this);
            $this->getParents()->removeElement($parent);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function addChildren($children)
    {
        foreach ($children as $child) {
            $child->getParents()->add($this);
            $this->getChildren()->add($child);
        }
    }

    /**
     * @param ArrayCollection $children
     */
    public function removeChildren($children)
    {
        foreach ($children as $child) {
            $child->getParents()->removeElement($this);
            $this->getChildren()->removeElement($child);
        }
    }

    /**
     * @param bool $inclusive
     * @return array
     */
    public function getFlatTree($inclusive = false)
    {
        $tree = [];
        if ($inclusive) {
            $tree[] = $this->getId();
        }

        foreach ($this->getChildren() as $child) {
            $tree[] = $child->getId();
            $tree = array_merge($tree, $child->getFlatTree(false));
        }

        return $tree;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'code',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'json',
                'required' => false,
            ],
            [
                'name'     => 'children',
                'required' => false,
            ],
        ]);

        return $inputFilter;
    }

    public function __call($name, $arguments)
    {
        foreach (Expense::getTypes() as $type) {
            if ($name === 'getAmountExpense' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountExpense'], $arguments);
            } else if ($name === 'getAmountHTExpense' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountHTExpense'], $arguments);
            }
        }

        foreach (Income::getTypes() as $type) {
            if ($name === 'getAmountIncome' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountIncome'], $arguments);
            } else if ($name === 'getAmountHTIncome' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountHTIncome'], $arguments);
            }
        }

        if (preg_match('/getIndicatorDone([0-9]+)/', $name)) {
            array_unshift($arguments, str_replace('getIndicatorDone', '', $name));
            return call_user_func_array([$this, 'getIndicatorDone'], $arguments);
        }

        if (preg_match('/getIndicatorTarget([0-9]+)/', $name)) {
            array_unshift($arguments, str_replace('getIndicatorTarget', '', $name));
            return call_user_func_array([$this, 'getIndicatorTarget'], $arguments);
        }

        if (preg_match('/getIndicatorDoneDirect([0-9]+)/', $name)) {
            array_unshift($arguments, str_replace('getIndicatorDoneDirect', '', $name));
            return call_user_func_array([$this, 'getIndicatorDoneDirect'], $arguments);
        }

        if (preg_match('/getIndicatorTargetDirect([0-9]+)/', $name)) {
            array_unshift($arguments, str_replace('getIndicatorTargetDirect', '', $name));
            return call_user_func_array([$this, 'getIndicatorTargetDirect'], $arguments);
        }

        return null;
    }

    /**
     * Returns the sum of expenses for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    protected function getAmountExpense($type, $year, $em)
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountExpenseArbo' . ucfirst($type)}($year);
        }

        return $amount;
    }

    /**
     * Returns the sum of expenses ht for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    protected function getAmountHTExpense($type, $year, $em)
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountHTExpenseArbo' . ucfirst($type)}($year);
        }

        return $amount;
    }

    /**
     * Returns the sum of incomes for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    protected function getAmountIncome($type, $year, $em)
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountIncomeArbo' . ucfirst($type)}($year);
        }

        return $amount;
    }

    /**
     * Returns the sum of incomes for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    protected function getAmountHTIncome($type, $year, $em)
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountHTIncomeArbo' . ucfirst($type)}($year);
        }

        return $amount;
    }

    public function getTimeTarget($year, $em)
    {
        $timeTarget = 0;
        foreach ($this->getProjects($em) as $project) {
            $timeTarget += $project->getTimeTargetArbo($year);
        }

        return $timeTarget;
    }

    public function getTimeTargetDirect($year, $em)
    {
        $timeTarget = 0;

        foreach ($this->getTimesheets($em) as $timesheet) {
            if ($timesheet->isYear($year) && $timesheet->getType() === Timesheet::TYPE_TARGET) {
                $timeTarget += $timesheet->getHours();
            }
        }

        return $timeTarget;
    }

    public function getTimeSpent($year, $em)
    {
        $timeSpent = 0;
        foreach ($this->getProjects($em) as $project) {
            $timeSpent += $project->getTimeSpentArbo($year);
        }

        return $timeSpent;
    }

    public function getTimeSpentDirect($year, $em)
    {
        $timeSpent = 0;
        foreach ($this->getTimesheets($em) as $timesheet) {
            if ($timesheet->isYear($year) && $timesheet->getType() === Timesheet::TYPE_DONE) {
                $timeSpent += $timesheet->getHours();
            }
        }

        return $timeSpent;
    }

    public function getTimeLeft($year, $em)
    {
        return $this->getTimeTarget($year, $em) - $this->getTimeSpent($year, $em);
    }

    public function getTimeLeftDirect($year, $em)
    {
        return $this->getTimeTargetDirect($year, $em) - $this->getTimeSpentDirect($year, $em);
    }

    public function getIndicatorDone($id, $year, $em)
    {
        $measures = [];
        foreach ($this->getProjects($em) as $project) {
            $measures = array_merge($measures, $project->getMeasures($id, $year)->toArray());
        }
        $measures = new ArrayCollection($measures);

        $indicator = $em->getRepository('Indicator\Entity\Indicator')->find($id);
        $value = $indicator->getValue($measures->filter(function ($measure) {
            return $measure->getType() == Measure::TYPE_DONE;
        }));

        return $value;
    }

    public function getIndicatorDoneDirect($id, $year, $em)
    {
        $measures = $this->getMeasures($em);

        $indicator = $em->getRepository('Indicator\Entity\Indicator')->find($id);
        $value = $indicator->getValue($measures->filter(function ($measure) use ($indicator, $year) {
            return $measure->getType() == Measure::TYPE_DONE && $measure->isYear($year) && $measure->getIndicator() === $indicator;
        }));

        return $value;
    }

    public function getIndicatorTarget($id, $year, $em)
    {
        $measures = [];
        foreach ($this->getProjects($em) as $project) {
            $measures = array_merge($measures, $project->getMeasures($id, $year)->toArray());
        }
        $measures = new ArrayCollection($measures);

        $indicator = $em->getRepository('Indicator\Entity\Indicator')->find($id);
        $value = $indicator->getValue($measures->filter(function ($measure) {
            return $measure->getType() == Measure::TYPE_TARGET;
        }));

        return $value;
    }

    public function getIndicatorTargetDirect($id, $year, $em)
    {
        $measures = $this->getMeasures($em);

        $indicator = $em->getRepository('Indicator\Entity\Indicator')->find($id);
        $value = $indicator->getValue($measures->filter(function ($measure) use ($indicator, $year) {
            return $measure->getType() == Measure::TYPE_TARGET && $measure->isYear($year) && $measure->getIndicator() === $indicator;
        }));

        return $value;
    }

    protected function getProjects($em)
    {
        if ($this->projects === null) {
            $this->projects = new ArrayCollection();

            $associations = $em->getRepository('Map\Entity\Association')->findBy([
                'territory' => $this,
                'entity'    => 'Project\Entity\Project'
            ]);

            foreach ($associations as $association) {
                $project = $em->getRepository('Project\Entity\Project')->find($association->getPrimary());
                $this->projects->add($project);
            }
        }

        return $this->projects;
    }

    public function setProjects($projects)
    {
        $this->projects = $projects;
    }

    protected function getTimesheets($em)
    {
        if ($this->timesheets === null) {
            $this->timesheets = new ArrayCollection();

            $associations = $em->getRepository('Map\Entity\Association')->findBy([
                'territory' => $this,
                'entity'    => 'Time\Entity\Timesheet'
            ]);

            foreach ($associations as $association) {
                $timesheet = $em->getRepository('Time\Entity\Timesheet')->find($association->getPrimary());
                if ($timesheet) {
                    $this->timesheets->add($timesheet);
                }
            }
        }

        return $this->timesheets;
    }

    protected function getMeasures($em)
    {
        if ($this->measures === null) {
            $this->measures = new ArrayCollection();

            $associations = $em->getRepository('Map\Entity\Association')->findBy([
                'territory' => $this,
                'entity'    => 'Indicator\Entity\Measure'
            ]);
            foreach ($associations as $association) {
                $measure = $em->getRepository('Indicator\Entity\Measure')->find($association->getPrimary());
                if ($measure) {
                    $this->measures->add($measure);
                }
            }
        }

        return $this->measures;
    }

    public function getYearsBounds($key, $em)
    {
        $bounds = ['min' => null, 'max' => null];

        if (preg_match('/^amountExpense/', $key)) {
            $projects = $this->getProjects($em);
            foreach ($projects as $project) {
                foreach ($project->getPosteExpensesArbo() as $poste) {
                    foreach ($poste->getExpensesArbo() as $expense) {
                        $year = $expense->getDate()->format('Y');
                        if ($bounds['min'] === null || $year < $bounds['min']) {
                            $bounds['min'] = $year;
                        }
                        if ($bounds['max'] === null || $year > $bounds['max']) {
                            $bounds['max'] = $year;
                        }
                    }
                }
            }
        } else if (preg_match('/^amountHTExpense/', $key)) {
            $projects = $this->getProjects($em);
            foreach ($projects as $project) {
                foreach ($project->getPosteExpensesArbo() as $poste) {
                    foreach ($poste->getExpensesArbo() as $expense) {
                        $year = $expense->getDate()->format('Y');
                        if ($bounds['min'] === null || $year < $bounds['min']) {
                            $bounds['min'] = $year;
                        }
                        if ($bounds['max'] === null || $year > $bounds['max']) {
                            $bounds['max'] = $year;
                        }
                    }
                }
            }

        } else if (preg_match('/^amountIncome/', $key)) {
            $projects = $this->getProjects($em);
            foreach ($projects as $project) {
                foreach ($project->getPosteIncomesArbo() as $poste) {
                    foreach ($poste->getIncomesArbo() as $income) {
                        $year = $income->getDate()->format('Y');
                        if ($bounds['min'] === null || $year < $bounds['min']) {
                            $bounds['min'] = $year;
                        }
                        if ($bounds['max'] === null || $year > $bounds['max']) {
                            $bounds['max'] = $year;
                        }
                    }
                }
            }
        } else if (preg_match('/^amountHTIncome/', $key)) {
            $projects = $this->getProjects($em);
            foreach ($projects as $project) {
                foreach ($project->getPosteIncomesArbo() as $poste) {
                    foreach ($poste->getIncomesArbo() as $income) {
                        $year = $income->getDate()->format('Y');
                        if ($bounds['min'] === null || $year < $bounds['min']) {
                            $bounds['min'] = $year;
                        }
                        if ($bounds['max'] === null || $year > $bounds['max']) {
                            $bounds['max'] = $year;
                        }
                    }
                }
            }
        } else if (preg_match('/^time(.+)Direct$/', $key)) {
            foreach ($this->getTimesheets($em) as $timesheet) {
                $year = $timesheet->getStart()->format('Y');
                if ($bounds['min'] === null || $year < $bounds['min']) {
                    $bounds['min'] = $year;
                }
                if ($bounds['max'] === null || $year > $bounds['max']) {
                    $bounds['max'] = $year;
                }
            }
        } else if (preg_match('/^time/', $key)) {
            $projects = $this->getProjects($em);
            foreach ($projects as $project) {
                $timesheets = $project->getTimesheetsArbo();
                foreach ($timesheets as $timesheet) {
                    $year = $timesheet->getStart()->format('Y');
                    if ($bounds['min'] === null || $year < $bounds['min']) {
                        $bounds['min'] = $year;
                    }
                    if ($bounds['max'] === null || $year > $bounds['max']) {
                        $bounds['max'] = $year;
                    }
                }
            }

        } else if (preg_match('/^indicator(.+)Direct[0-9]+$/', $key)) {
            $id = preg_replace('/[a-zA-Z]/', '', $key);
            foreach ($this->getMeasures($em) as $measure) {
                if ($measure->getIndicator()->getId() == $id) {
                    $year = $measure->getDate()->format('Y');
                    if ($bounds['min'] === null || $year < $bounds['min']) {
                        $bounds['min'] = $year;
                    }
                    if ($bounds['max'] === null || $year > $bounds['max']) {
                        $bounds['max'] = $year;
                    }
                }
            }
        }  else if (preg_match('/^indicator/', $key)) {
            $id = preg_replace('/[a-zA-Z]/', '', $key);
            $projects = $this->getProjects($em);
            foreach ($projects as $project) {
                $measures = $project->getMeasures($id);
                foreach ($measures as $measure) {
                    $year = $measure->getDate()->format('Y');
                    if ($bounds['min'] === null || $year < $bounds['min']) {
                        $bounds['min'] = $year;
                    }
                    if ($bounds['max'] === null || $year > $bounds['max']) {
                        $bounds['max'] = $year;
                    }
                }
            }

        }

        return $bounds;
    }

    public function reccursiveBounds()
    {

    }
}
