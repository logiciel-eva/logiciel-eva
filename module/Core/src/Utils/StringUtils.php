<?php

namespace Core\Utils;

class StringUtils
{

    public static function strStartsWith($haystack, $needle)
    {
        $length = strlen( $needle );
        return substr( $haystack, 0, $length ) === $needle;
    }

    public static function strEndsWith($haystack, $needle)
    {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

}