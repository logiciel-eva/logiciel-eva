(function(app) {

    app.factory('advancementService', [
        'restFactoryService',
        function advancementService(restFactoryService) {
            return restFactoryService.create('/api/advancement');
        }
    ])

})(window.EVA.app);
