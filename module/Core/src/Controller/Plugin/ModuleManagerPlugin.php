<?php

namespace Core\Controller\Plugin;

use Core\Module\MyModuleManager;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

class ModuleManagerPlugin extends AbstractPlugin
{
    /**
     * @var MyModuleManager
     */
    protected $moduleManager;

    public function __construct(MyModuleManager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }

    public function __invoke()
    {
        return $this->moduleManager;
    }
}

