<?php

namespace Project\Event;

use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Project\Entity\Project;
use Laminas\ServiceManager\ServiceLocatorInterface;

class ParentSubscriber implements EventSubscriber
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * RateSubscriber constructor.
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::onFlush,
        ];
    }

    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Project) {
                $changes = $uow->getEntityChangeSet($entity);

                if (isset($changes['parent'])) {
                    if ($changes['parent'][0]) {
                        /** @var PosteExpense $poste */
                        foreach ($entity->getPosteExpenses() as $poste) {
                            if ($poste->getParent()) {
                                $poste->setParent(null);

                                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($poste)), $poste);
                            }
                        }

                        /** @var PosteIncome $poste */
                        foreach ($entity->getPosteIncomes() as $poste) {
                            if ($poste->getParent()) {
                                $poste->setParent(null);

                                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($poste)), $poste);
                            }
                        }
                    }
                }

                continue;
            }
        }
    }
}
