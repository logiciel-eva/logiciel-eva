<?php

namespace Bootstrap\Environment;

use Bootstrap\Environment\Exception\EnvironmentException;
use Core\Utils\ArrayUtils;
use Doctrine\ORM\EntityManager;
use Laminas\Http\Request as HttpRequest;
use Laminas\Router\Http\TreeRouteStack;

/**
 * Class Environment
 *
 * Application environment is responsible to configure and start the application based on the current
 * hostname.
 * It will start an env-based session, load some routes, use a specific template and own a specific Doctrine Entity Manager.
 *
 * Routes should be in the application config in the key 'myEnvName-routes'.
 * By default, the current hostname and the environment name will be stored in session in the key corresponding
 * to the cleaned hostname (hostname with only alphanumerics).
 *
 * @package Bootstrap\Environment
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
abstract class Environment implements EnvironmentInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var Host
     */
    protected $host;

    /**
     * @var HttpRequest
     */
    protected $request;

    /**
     * @var TreeRouteStack
     */
    protected $router;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $template;

    /**
     * @param string $name
     * @param array $config
     * @param HttpRequest $request
     * @param TreeRouteStack $router
     * @param Session $session
     * @param EntityManager $entityManager
     * @param string $template
     */
    public function __construct($name, array $config, HttpRequest $request, TreeRouteStack $router, Session $session, EntityManager $entityManager, $template)
    {
        $this->name          = $name;
        $this->config        = $config;
        $this->request       = $request;
        $this->router        = $router;
        $this->session       = $session;
        $this->entityManager = $entityManager;
        $this->template      = $template;

        $this->host = new Host($this->request->getUri()->getHost());
    }

    /**
     * @throws EnvironmentException
     */
    public function bootstrap()
    {
        $this->session->start($this->host->toAlnum());

        $this->session->store('host', $this->host->toString());
        $this->session->store('environment', $this->name);

        $routes = $this->config['router'][$this->name . '-routes'];
        if ($routes === null) {
            throw new EnvironmentException('There are no routes for the  "' . $this->name . '" environment, set routes with "' . $this->name . '-routes" key in the router configuration.', 1);
        }

        $this->router->setRoutes(ArrayUtils::arrayMergeRecursiveDistinct($this->config['router']['routes'], $routes));
        //$this->router->addRoutes($routes);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }
}
