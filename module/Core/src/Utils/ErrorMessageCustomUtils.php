<?php

namespace Core\Utils;

/**
 * Class ErrorMessageCustomUtils
 */
class ErrorMessageCustomUtils
{
    /**
     * @var string Message Foreign Key Constraint Violation
     */
    private $custom_message = 'Cette suppression n\'est pas autorisée car un lien existe vers cette information. L\'élément concerné est : "%s". Pour plus de précision consulter l\'assistance interne ou en ligne  avec la référence "%s"';

    /**
     * Custom Error Message Foreign Key Constraint Violation
     *
     * @param $message
     * @return string
     */
    public function customForeignKeyConstraintViolation($message): string
    {
        $matches = [];
        preg_match('/FOREIGN KEY \(`(.*?)`\) REFERENCES `(.*?)`/', $message, $matches, PREG_OFFSET_CAPTURE);

        return sprintf(
            $this->custom_message,
            $matches[1][0],
            $matches[2][0]
        );
    }
}
