<?php

namespace Budget\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Project\Entity\Project;
use Laminas\InputFilter\Factory;

/**
 * @ORM\Entity
 * @ORM\Table(name="budget_code")
 */
class BudgetCode extends BasicRestEntityAbstract
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", unique=true)
	 */
	protected $name;

	/**
	 * @var Project
	 *
	 * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="budgetCodes")
	 */
	protected $project;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return Project
	 */
	public function getProject()
	{
		return $this->project;
	}

	/**
	 * @param Project $project
	 */
	public function setProject($project)
	{
		$this->project = $project;
	}

	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilterFactory = new Factory();

		$inputFilter = $inputFilterFactory->createInputFilter([
			[
				'name' => 'name',
				'required' => true,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value, $context) use ($entityManager) {
								$queryBuilder = $entityManager->createQueryBuilder();

								$queryBuilder
									->select('o')
									->from(BudgetCode::class, 'o')
									->where('o.name = :name')
									->andWhere('o.id != :id')
									->setParameters([
										'id' => $context['id'] ?? '',
										'name' => $value,
									]);

								try {
									$entity = $queryBuilder->getQuery()->getOneOrNullResult();
								} catch (Exception $e) {
									$entity = null;
								}

								return $entity === null;
							},
							'message' => 'budgetCode_field_name_error_unique',
						],
					],
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value, $context) use ($entityManager) {
								$queryBuilder = $entityManager->createQueryBuilder();

								$queryBuilder
									->select('o')
									->from(Project::class, 'o')
									->where('o.code = :name')
									->setParameters([
										'name' => $value,
									]);

								try {
									$entity = $queryBuilder->getQuery()->getOneOrNullResult();
								} catch (Exception $e) {
									$entity = null;
								}

								return $entity === null;
							},
							'message' => 'budgetCode_field_name_error_used_by_a_project',
						],
					],
				],
			],
			[
				'name' => 'project',
				'required' => true,
				'filters' => [['name' => 'Core\Filter\IdFilter']],
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value) use ($entityManager) {
								return $entityManager->getRepository(Project::class)->find($value) !== null;
							},
							'message' => 'budgetCode_field_project_error_non_exists',
						],
					],
				],
			],
		]);

		return $inputFilter;
	}
}
