<?php

namespace Project\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Project\Entity\Project;

class TemplateController extends BasicRestController
{
    protected $entityClass = 'Project\Entity\Template';
    protected $repository  = 'Project\Entity\Template';
    protected $entityChain = 'project:e:template';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }


	public function update($id, $data)
	{
        $data = $this->defaultHiddenValue($data);
        $vm = parent::update($id, $data);

		return $vm;
    }

	public function create($data)
	{
        $data = $this->defaultHiddenValue($data);
        $vm = parent::create($data);

		return $vm;
    }

    // Set all non-assigned values to false
    private function defaultHiddenValue($data) {
        $fields = [];
        $project = new Project();
        foreach ($project->getInputFilter($this->entityManager(), $this->authStorage()->getUser())->getInputs() as $field) {
            if (!$field->isRequired() && $field->getName() !== 'template' && $field->getName() !== 'members') {
                $fields[] = $field->getName();
            }
        }
        foreach($fields as $field) {
            if ($data['config']['fields'][$field]['show'] != "true"){
                $data['config']['fields'][$field] = array('show' => "false");
            }
        }
        
        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('project', false);
        }
        foreach($groups as $group) {
            if ($data['config']['keywords'][$group->getId()]['show'] != "true"){
                $data['config']['keywords'][$group->getId()] = array('show' => "false");
            }
        }

        $customs = [];
        if ($this->moduleManager()->isActive('field')) {
            $customs = $this->allowedFieldGroups('Project\Entity\Project');
        }
        foreach($customs as $custom) {
            if ($data['config']['custom'][$custom->getId()]['show'] != "true"){
                $data['config']['custom'][$custom->getId()] = array('show' => "false");
            }
        }
        return $data;
    }
}
