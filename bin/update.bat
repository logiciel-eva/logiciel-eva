@ECHO OFF

.\vendor\bin\laminas core:clean-cache && ^
.\vendor\bin\laminas core:update-dbs && ^
.\vendor\bin\laminas core:generate-proxies && ^
.\vendor\bin\laminas core:generate-language
