<?php

namespace Home\Controller\API;

use Core\Controller\BasicRestController;

class HomeController extends BasicRestController
{
    protected $entityClass = 'Home\Entity\Home';
    protected $repository  = 'Home\Entity\Home';
    protected $entityChain = 'home:e:home';
}
