<?php

$indicator = [
    'icon' => 'indicator',
    'right' => 'indicator:e:indicator:r',
    'title' => 'indicator_module_title',
    'priority' => 999,
    'children' => [
        'indicator' => [
            'icon' => 'indicator',
            'right' => 'indicator:e:indicator:r',
            'title' => 'indicator_module_title',
            'href' => 'indicator',
            'priority' => 999,
        ],
        'group-indicator' => [
            'icon'     => 'groupindicator',
            'right'    => 'indicator:e:group-indicator:r',
            'title'    => 'group-indicator_module_title',
            'href'     => 'group-indicator',
            'priority' => 998,
        ]
    ]
];

return [
    'menu' => [
        'client-menu' => [
                'indicator' => $indicator,
        ],
        'client-menu-parc' => [
            'data' => [
                'children' => [
                    'indicator' => $indicator
                ]
            ]
        ]
    ]
];
