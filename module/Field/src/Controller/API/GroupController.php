<?php

namespace Field\Controller\API;

use Core\Controller\BasicRestController;

class GroupController extends BasicRestController
{
    protected $entityClass = 'Field\Entity\Group';
    protected $repository  = 'Field\Entity\Group';
    protected $entityChain = 'field:e:fieldGroup';

    protected function searchList($queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
