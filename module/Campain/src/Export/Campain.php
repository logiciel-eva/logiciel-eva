<?php

namespace Campain\Export;

use Core\Export\IExporter;

abstract class Campain implements IExporter
{
    public static function getConfig()
    {
        return [
            'evaluationManagers' => [
                'property' => 'evaluationManagers.name'
            ]
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
