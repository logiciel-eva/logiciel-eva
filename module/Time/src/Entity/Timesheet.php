<?php

namespace Time\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Project\Entity\Project;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Timesheet
 *
 * @package Time\Entity
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="time_timesheet")
 */
class Timesheet extends BasicRestEntityAbstract
{
    const TYPE_DONE    = 'done';
    const TYPE_TARGET  = 'target';
    const TYPE_ABSENCE = 'absence';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="timesheets", cascade={})
     */
    protected $project;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $end;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $hours;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * Synchronisation
     * @var Synchronisation
     * @ORM\ManyToOne(targetEntity="Time\Entity\Synchronisation")
     */
    protected $synchronisation;

    /**
     * Synchronisation Hash Id
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $syncHash;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type = self::TYPE_DONE;

    /**
     * @var AbsenceType
     * @ORM\ManyToOne(targetEntity="Time\Entity\AbsenceType")
     */
    protected $absenceType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        if (!$this->name) {
            return 'Feuille de temps du ' . $this->start->format('d/m/Y') . ' au ' . $this->end->format('d/m/Y');
        }
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return float
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @param float $hours
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Synchronisation
     */
    public function getSynchronisation()
    {
        return $this->synchronisation;
    }

    /**
     * @param Synchronisation $synchronisation
     */
    public function setSynchronisation($synchronisation)
    {
        $this->synchronisation = $synchronisation;
    }

    /**
     * @return string
     */
    public function getSyncHash()
    {
        return $this->syncHash;
    }

    /**
     * @param string $syncHash
     */
    public function setSyncHash($syncHash)
    {
        $this->syncHash = $syncHash;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return AbsenceType
     */
    public function getAbsenceType()
    {
        return $this->absenceType;
    }

    /**
     * @param AbsenceType $absenceType
     */
    public function setAbsenceType($absenceType)
    {
        $this->absenceType = $absenceType;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'project',
                'required'   => false,
                'filters'    => [
                    ['name' => 'Core\Filter\IdFilter'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_numeric($value)) {
                                    $project = $entityManager->getRepository('Project\Entity\Project')->find($value);
                                } else {
                                    $project = $entityManager->getRepository('Project\Entity\Project')->find(['name' => $value]);
                                }
                                return $project !== null;
                            },
                            'message'  => 'timesheet_field_project_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'user',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $user = $entityManager->getRepository('User\Entity\User')->find($value);

                                return $user !== null;
                            },
                            'message'  => 'timesheet_field_user_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'start',
                'required' => true,
                'filters'  => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'       => 'end',
                'required'   => true,
                'filters'    => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                return \DateTime::createFromFormat('d/m/Y H:i', $context['start']) <= \DateTime::createFromFormat('d/m/Y H:i', $context['end']);
                            },
                            'message'  => 'timesheet_field_end_before_start_error',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'hours',
                'required' => true,
                'filters' => [['name' => 'NumberParse']],
            ],
            [
                'name'     => 'description',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'syncHash',
                'required' => false,
            ],
            [
                'name'     => 'synchronisation',
                'required' => false,
            ],
            [
                'name'       => 'type',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => self::getTypes(),
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'absenceType',
                'required'   => false,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $absenceType = $entityManager->getRepository('Time\Entity\AbsenceType')->find($value);

                                return $absenceType !== null;
                            },
                            'message'  => 'timesheet_field_absenceType_error_non_exist',
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }

    public function calcHours($hoursPerDay = 24)
    {
        if ($this->start instanceof \DateTime && $this->end instanceof \DateTime) {
            $interval    = $this->start->diff($this->end);
            $this->hours = intval($interval->format('%a')) * $hoursPerDay;
            $this->hours += intval($interval->format('%h'));
            $this->hours += intval($interval->format('%i')) / 60;
            $this->hours = round($this->hours, 2);
        }


    }

    public function isYear($year = null)
    {
        if ($year !== null) {
            return $this->getStart()->format('Y') == $year;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_DONE,
            self::TYPE_TARGET,
            self::TYPE_ABSENCE,
        ];
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return $this->user === $user;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $user
     * @param string $owner
     */
    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        $ownCondition = 'object.user = :ownerControl';
        if ($owner === 'own') {
            $queryBuilder->andWhere($ownCondition);
            $queryBuilder->setParameter('ownerControl', $user);
        }
    }
}
