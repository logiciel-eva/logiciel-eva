<?php

return [
    'export_module_title' => '[[ export_entities | ucfirst ]]',

    'export_nav_list'      => 'Liste des {{ __tb.total }} [[ export_entities ]]',
    'export_nav_form'      => 'Créer un [[ export_entity ]]',
    'exportWord_nav_form'  => 'Créer un [[ export_entity ]] Word',
    'exportExcel_nav_form' => 'Créer un [[ export_entity ]] Excel',
    'exportPublipostage_nav_form' => 'Créer un [[ export_entity ]] Publipostage',
    

    'export_entity'        => 'export',
    'export_entities'      => 'exports',
    'exportWord_entities'  => 'exports Word',
    'exportExcel_entities' => 'exports Excel',
    'exportPublipostages_entities' => 'exports Publipostages',

    'template_entity'   => 'template',
    'template_entities' => 'templates',

    'export_field_name'                                 => 'Nom',
    'export_field_createdAt'                            => '[[ field_created_at_m ]]',
    'export_field_updatedAt'                            => '[[ field_updated_at_m ]]',
    'export_field_templates'                            => '[[ template_entities | ucfirst ]]',
    'export_field_reference'                            => 'Arborescence',
    'export_field_year'                                 => 'Filtre par année',
    'export_field_showArbo'                             => 'Voir l\'arborescence',
    'export_field_dataArbo'                             => 'Données à utiliser pour l\'arborescence',
    'export_field_groupByKeywords'                      => 'Grouper par mots-clés',
    'export_field_groupByKeywordsTitles_keywords'       => 'Titre du [[ keyword_entity ]]',
    'export_field_groupByKeywordsTitles_parentProjects' => 'Titre du [[ project_field_parent | lcfirst ]] d\'une [[ project_entity ]]',
    'export_hide_zero_amount'                           => 'Cacher les postes dont le montant est égal à 0',
    'export_show_only_total'                            => 'Afficher seulement les totaux',
    'export_group_by_year'                              => 'Grouper par année',
    'export_prev_and_current_years'                     => 'Colonnes année(s) précédente(s) et année en cours',   
    'export_show_only_poste'                            => 'Afficher uniquement le total des postes', 

    'export_special_field_order'       => 'ordre',
    'export_special_field_field'       => 'champ',
    'export_special_field_translation' => 'intitulé',
    'export_special_field_params'      => 'paramètres',

    'export_showArbo_no'      => 'Non',
    'export_showArbo_indent'  => 'Indenter les valeurs dans les colonnes',
    'export_showArbo_columns' => 'Ajouter de nouvelles colonnes',

    'export_dataArbo_code'      => '[[ project_field_code ]]',
    'export_dataArbo_name'      => '[[ project_field_name ]]',
    'export_dataArbo_code-name' => '[[ project_field_code]] - [[ project_field_name ]]',

    'export_field_year_invalid' => 'Année invalide',

    'export_reference_initial' => '[[ project_initial_tree ]]',
    'export_reference_keyword' => '[[ keyword_entity | ucfirst ]] référentiel',

    'export_action_add_groupByKeywordTitle_keyword' => 'Ajouter un titre pour un [[ keyword_entity ]] de niveau supérieur',

		'export_userTimeRepartition_byYear' => 'Afficher une colonne par année',
		'export_userTimeRepartition_yearMin' => 'Année minimale à afficher',
		'export_userTimeRepartition_yearMax' => 'Année maximale à afficher',
		'export_userTimeRepartition_convertToDays' => 'Convertir en jours',
		'export_userTimeRepartition_hoursByDay' => 'Nombre d\'heures par jour',

    'template_field_content'   => '',
    'template_field_type'      => 'Type',
    'template_field_pagebreak' => 'Nouvelle page',

    'template_pagebreak_none'   => 'Jamais',
    'template_pagebreak_before' => 'Avant',
    'template_pagebreak_after'  => 'Après',
    'template_pagebreak_both'   => 'Avant et après',

    'template_type_keyword'        => '[[ keyword_entity | ucfirst ]]',
    'template_type_parent'         => 'Parent',
    'template_type_parent_project' => '[[ project_entity | ucfirst ]] rattachée à un [[ template_type_parent | lower ]]',
    'template_type_node'           => 'Noeud',
    'template_type_node_project'   => '[[ project_entity | ucfirst ]] rattachée à un [[ template_type_node | lower ]]',
    'template_type_leaf'           => 'Feuille',
    'template_type_leaf_project'   => '[[ project_entity | ucfirst ]] rattachée à un [[ template_type_leaf | lower ]]',

    'export_message_saved'             => '[[ export_entity | ucfirst ]] enregistré',
    'export_message_deleted'           => '[[ export_entity | ucfirst ]] supprimé',
    'export_question_delete'           => 'Voulez-vous réellement supprimer l\'[[ export_entity ]] %1 ?',
    'export_question_change_reference' => 'Attention, la modification de l\'arborescence effacera les templates déjà créés. Continuer ?',

    'export' => 'Exporter',
    'word'   => 'Word',
    'excel'  => 'Excel',

    'export_variables'    => 'Variables',
    'export_add_variable' => 'Insérer une variable',

    'pictures_module_title'   => 'Images',
    'pictures_module_project' => 'Fiches',
    'pictures_module_global'  => 'Global',

    'picture_message_saved' => 'Image enregistrée',
    'picture_message_error' => 'Une erreur a été rencontrée durant l\'enregistrement de l\'image',
    'picture_add_title'     => 'Ajouter une image',

    'export_custom_entity' => 'Export personnalisé',

    'by_project' => 'Oui et par [[ project_entities ]]',
];
