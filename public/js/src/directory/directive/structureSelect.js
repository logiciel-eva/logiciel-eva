(function(app) {

    app.directive('structureSelect', [
        'structureService', 'selectFactoryService',
        function structureSelect(structureService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: structureService,
                    apiParams: {
                        col: ['id', 'name', 'sigle']
                    },
                    full: true
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['sigle', 'name'],
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' +
                                (data.sigle ? '<span class="sigle">' + escape(data.sigle) + '</span> - ' : '') + '<span class="name">' + escape(data.id) + ' - ' + escape(data.name) + '</span>' +
                            '</div>';
                        },
                        item: function (data, escape) {
                            return '<div class="option">' +
                                (data.sigle ? '<span class="sigle">' + escape(data.sigle) + '</span> - ' : '') + '<span class="name">' + escape(data.id) + ' - ' + escape(data.name) + '</span>' +
                            '</div>';
                        }
                    }
                }
            });
        }
    ]);

})(window.EVA.app);
