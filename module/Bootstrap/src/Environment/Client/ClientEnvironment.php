<?php

namespace Bootstrap\Environment\Client;

use Bootstrap\Entity\Client;
use Bootstrap\Environment\Environment;
use Bootstrap\Environment\Exception\EnvironmentException;
use Bootstrap\Environment\Session;
use Bootstrap\ORM\EntityManagerFactory;
use Core\Module\AbstractModule;
use Core\Utils\ArrayUtils;
use Doctrine\ORM\EntityManager;
use Laminas\Http\Request as HttpRequest;
use Laminas\I18n\Translator\TextDomain;
use Laminas\Mvc\I18n\Translator;
use Laminas\ModuleManager\ModuleManager;
use Laminas\Router\Http\TreeRouteStack;

/**
 * Class ClientEnvironment
 *
 * The client environment has to load the client corresponding to the current hostname.
 * We need to use the default Doctrine Entity Manager AND the client Doctrine Entity Manager.
 * Plus, the client identifier is stored in session.
 *
 * @package Bootstrap\Environment\Client
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class ClientEnvironment extends Environment
{
    /**
     * @var EntityManagerFactory
     */
    protected $entityManagerFactory;

    /**
     * @var EntityManager
     */
    protected $clientEntityManager;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ModuleManager
     */
    protected $moduleManager;

    /**
     * @var \Laminas\I18n\Translator\Translator
     */
    protected $translator;

    /**
     * @param array $config
     * @param HttpRequest $request
     * @param TreeRouteStack  $router
     * @param EntityManagerFactory $entityManagerFactory
     * @param ModuleManager $moduleManager
     * @param Translator $translator
     */
    public function __construct(array $config, HttpRequest $request, TreeRouteStack  $router, EntityManagerFactory $entityManagerFactory, ModuleManager $moduleManager, Translator $translator)
    {
        $entityManager = $entityManagerFactory->getEntityManager();
        parent::__construct('client', $config, $request, $router, new Session(), $entityManager, 'layout/layout');

        $this->entityManagerFactory = $entityManagerFactory;
        $this->moduleManager        = $moduleManager;
        $this->translator           = $translator->getTranslator();
    }

    /**
     * @throws EnvironmentException
     */
    public function bootstrap()
    {
        parent::bootstrap();

        $client = $this->entityManager->getRepository('Bootstrap\Entity\Client')->findOneByHost($this->host->toString());
        if (!$client) {
            throw new EnvironmentException('Client corresponding to "' . $this->host->toString() . '" host doesn\'t exists');
        }

        $this->session->store('client', $client->getId());

        $mergedConfiguration = $this->moduleManager->getEvent()->getConfigListener()->getMergedConfig(false);

        $clientPrivateConfig = $this->getEntityManager()->getRepository('Application\Entity\Configuration')->find(1);

        /*** Icons ***/
        $clientIcons = $client->getIcons();
        $mergedConfiguration['icons'] = ArrayUtils::arrayMergeRecursiveDistinct($mergedConfiguration['icons'], $clientIcons);

        $this->moduleManager->getEvent()->getConfigListener()->setMergedConfig($mergedConfiguration);

        /*** Translations ***/
        if ($this->translator instanceof \Laminas\I18n\Translator\Translator) {
            $translations = $client->getTranslations();
            $textDomain = new TextDomain($translations);
            $this->translator->getAllMessages()->merge($textDomain);

            $clientCustomTranslations = $clientPrivateConfig ? $clientPrivateConfig->getTranslations() : [];
            if ($clientCustomTranslations !== null) {
                foreach ($clientCustomTranslations as $key => $value) {
                    $clientCustomTranslations['base_' . $key] = $this->translator->translate($key);
                }
                $textDomain = new TextDomain($clientCustomTranslations);
                $this->translator->getAllMessages()->merge($textDomain);
            }
        }

        /*** Modules ***/
        $clientModules       = $client->getModules();
        $clientCustomModules = $clientPrivateConfig ? $clientPrivateConfig->getModules() : null;

        foreach ($this->moduleManager->getLoadedModules() as $module) {
            if ($module instanceof AbstractModule) {
                $key = $module->getKey();
                $moduleConfig = $module->getConfig();

                if (isset($moduleConfig['module'][$key])) {
                    if (!isset($moduleConfig['module'][$key]['activable'])) {
                        $moduleConfig['module'][$key]['activable'] = !$moduleConfig['module'][$key]['required'];
                    }

                    if (isset($clientModules[$key])) {
                        $moduleConfig['module'][$key] = ArrayUtils::arrayMergeRecursiveDistinct($moduleConfig['module'][$key], $clientModules[$key]);
                    }

                    if (isset($clientCustomModules[$key])) {
                        $moduleConfig['module'][$key] = ArrayUtils::arrayMergeRecursiveDistinct($moduleConfig['module'][$key], $clientCustomModules[$key]);

                        if (isset($clientModules[$key])) {
                            if (!$clientModules[$key]['activable']) {
                                $moduleConfig['module'][$key]['active'] = $clientModules[$key]['active'];
                            }
                        }
                    }

                    // Retrait des entités selon la configuration du module
                    if (isset($moduleConfig['module'][$key]['acl']['entities'])) {
                        $entities = [];
                        foreach ($moduleConfig['module'][$key]['acl']['entities'] as $entity) {
                            $control = true;
                            if (isset($entity['control'])) {
                                foreach ($entity['control'] as $conditionKey => $conditionValue) {
                                    $control &= $moduleConfig['module'][$key]['configuration'][$conditionKey] === $conditionValue;
                                }
                            }

                            if ($control) {
                                $entities[] = $entity;
                            }
                        }

                        $moduleConfig['module'][$key]['acl']['entities'] = $entities;
                    }

                    $module->setConfig($moduleConfig);
                }
            }
        }
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        if ($this->client === null) {
            $this->client = $this->entityManager->getRepository('Bootstrap\Entity\Client')->find($this->session->get('client'));
        }

        return $this->client;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        if ($this->clientEntityManager === null) {
            $this->clientEntityManager = $this->entityManagerFactory->getEntityManager($this->getClient());
        }

        return $this->clientEntityManager;
    }
}
