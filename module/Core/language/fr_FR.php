<?php

return [
    'exporter_error_value' => 'Valeur illisible',
    'exporter_empty_header' => 'En-tête vide',
];
