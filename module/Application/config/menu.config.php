<?php

$configuration = [
    'icon' => 'configuration',
    'href' => 'configuration',
    'right' => 'application:e:configuration:r',
    'title' => 'configuration_module_title',
    'priority' => 1,
];

return [
    'menu' => [
        'client-menu' => [
            'index' => [
                'icon' => 'home',
                'href' => 'root',
                'title' => 'menu_accueil_title',
                'priority' => 9999,
            ],
            'administration' => [
                'icon' => 'configuration',
                'title' => 'administration_module_title',
                'children' => [
                    'configuration' => $configuration,
                ],
            ],
        ],
        'client-menu-parc' => [
            'administration' => [
                'icon' => 'configuration',
                'title' => 'administration_module_title',
                'priority' => 1,
                'children' => [
                    'configuration' => array_replace($configuration, ['icon' => 'configuration2']),
                ],
            ],
        ],
    ],
];
