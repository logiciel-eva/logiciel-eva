<?php

namespace User\Auth;

use Bootstrap\Environment\Session;
use Doctrine\ORM\EntityManager;
use User\Entity\User;

/**
 * Class AuthStorage
 *
 * Will manage the environment session to store the current user.
 *
 * @package User\Auth
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AuthStorage
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var User
     */
    protected $user;

    /**
     * @param Session $session
     */
    public function __construct(EntityManager $entityManager, Session $session)
    {
        $this->entityManager = $entityManager;
        $this->session       = $session;
    }

    /**
     * @param User $user
     */
    public function store(User $user)
    {
        $this->session->store('user', $user->getId());
    }

    public function clear()
    {
        $this->session->clear('user');
        $this->session->clear('activity');
    }

    public function updateActivity()
    {
        $this->session->store('activity', time());
    }

    public function getActivity()
    {
        return $this->session->get('activity');
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        if ($this->user === null) {
            if ($this->session->get('user')) {
                $this->user = $this->entityManager->getRepository('User\Entity\User')->find($this->session->get('user'));

                if ($this->user->isDisabled()) {
                    $this->clear();
                    $this->user = null;
                }
            }

            if ($this->user === null) {
                $this->clear();
            }
        }

        return $this->user;
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return $this->session->get('user') !== null;
    }

    /**
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public function isAllowed($controller, $action)
    {
        if ($controller === 'Export\Controller\Index' && ($action === 'map' || $action === 'advancement')) {
            return true;
        }

        if ($controller === 'Analysis\Controller\Map' && ($action === 'index' || $action == 'territories' || $action == 'data')) {
            return true;
        }

        if ($controller === 'User\Controller\Auth' && $action === 'control') {
            return true;
        }

        if ($controller === 'User\Controller\Auth' && $action === 'login') {
            return !$this->isAuthenticated();
        }

        if ($controller === 'User\Controller\Auth' && $action === 'recover') {
            return !$this->isAuthenticated();
        }

        if ($controller === 'Event\Controller\Invitation' && $action === 'index') {
            return true;
        }

        if ($controller === 'Directory\Controller\Contact' && $action === 'unsubscribe') {
            return true;
        }

        return $this->isAuthenticated();
    }
}
