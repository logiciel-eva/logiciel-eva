<?php

namespace Core\Export;

use Core\Extractor\ObjectExtractor;
use Core\View\Helper\Translate;

interface IExporter
{
    /**
     * Get the config of the object to export.
     *
     * @return array
     */
    public static function getConfig();

    /**
     * Get aliases for the config of the object to export.
     *
     * @return array
     */
    public static function getAliases();

    /**
     * Get more rows within a row.
     *
     * @param  ObjectExtractor $extractor
     * @param  array           $objects
     * @param  array           $rows
     * @param  array           $cols
     * @param  array           $itemsRemoved
     * @param  array           $itemsSelected
     * @param  Translate       $translator
     * @return array
     */
    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator);
}
