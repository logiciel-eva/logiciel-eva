<?php

namespace Convention\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class ConventionController extends AbstractActionSLController
{
	public function indexAction()
	{
		if (!$this->acl()->isAllowed('convention:e:convention:r')) {
			return $this->notFoundAction();
		}

		$groups = [];
		if ($this->moduleManager()->isActive('keyword')) {
			$groups = $this->allowedKeywordGroups('convention', false);
		}

		return new ViewModel([
			'groups' => $groups,
		]);
	}

	public function formAction()
	{
		$id = $this->params()->fromRoute('id', false);
		if (!$this->acl()->basicFormAccess('convention:e:convention', $id)) {
			return $this->notFoundAction();
		}

		$convention = $this->entityManager()
			->getRepository('Convention\Entity\Convention')
			->find($id);

		if (
			$convention &&
			!$this->acl()->ownerControl('convention:e:convention:u', $convention) &&
			!$this->acl()->ownerControl('convention:e:convention:r', $convention)
		) {
			return $this->notFoundAction();
		}

		return new ViewModel([
			'convention' => $convention
		]);
	}
}
