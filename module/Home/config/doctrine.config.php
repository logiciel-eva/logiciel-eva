<?php
namespace Home;

return [
    'doctrine' => [
        'driver' => [
            'home_entities'   => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],
            'orm_environment' => [
                'drivers' => [
                    'Home\Entity' => 'home_entities',
                ],
            ],
        ],
    ],
];
