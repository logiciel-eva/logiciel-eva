<?php

namespace Time\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\Laminas\Hydrator\DoctrineObject as DoctrineHydrator;
use Laminas\ServiceManager\ServiceLocatorInterface;

class UsersMigrationService
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected $translator;

    protected $modulesConfiguration;
    /**
     * @var DoctrineHydrator
     */
    private $hydrator;

    public function __construct(
        $entityManager,
        $serviceLocator
    )
    {
        $this->entityManager = $entityManager;
        $this->serviceLocator = $serviceLocator;
        $this->translator = $serviceLocator->get('ViewHelperManager')->get('translate');
    }


    public function run()
    {
       $syncs = $this->getSyncs();
        foreach ($syncs as $sync) {
            $migratedUsersIds = [];
            foreach($sync->getUsers() as $usr) {
                $migratedUsersIds[] = $usr->getId();
            }
    
            foreach ($sync->getCronUsers() as $cronUser) {
                $user = $this->entityManager->getRepository('User\Entity\User')->find($cronUser['id']);
                if (!empty($user) && !in_array($user->getId(), $migratedUsersIds)) {
                   $sync->addUser($user);
                }
            }

            $this->entityManager->persist($sync);
            $this->entityManager->flush();
        }
   
    }
    private function getSyncs() {
        return $this->entityManager->getRepository('Time\Entity\Synchronisation')->createQueryBuilder('o')
            ->leftJoin('o.users', 'users')
            ->where('o.cronUsers IS NOT NULL')
            ->andWhere('o.cronUsers != \'[]\'')
            ->getQuery()
            ->getResult();
   }

}
