<?php

namespace Task\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Project\Entity\Project;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Task
 *
 * @package Task\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="task_task")
 */
class Task extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="tasks")
     */
    protected $project;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="User\Entity\User")
     * @ORM\JoinTable(
     *  name="task_users",
     *  joinColumns={
     *      @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *  }
     * )
     */
    protected $users;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $end;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $progress = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function addUsers(ArrayCollection $users)
    {
        foreach ($users as $user) {
            $this->getUsers()->add($user);
        }
    }

    /**
     * @param ArrayCollection $users
     */
    public function removeUsers(ArrayCollection $users)
    {
        foreach ($users as $user) {
            $this->getUsers()->removeElement($user);
        }
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param int $progress
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'project',
                'required'   => false,
                'filters'  => [
                    ['name' => 'Core\Filter\IdFilter'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $project = $entityManager->getRepository('Project\Entity\Project')
                                                         ->find($value);

                                return $project !== null;
                            },
                            'message'  => 'task_field_project_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'users',
                'required'   => true
            ],
            [
                'name'     => 'start',
                'required' => true,
                'filters'  => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'       => 'end',
                'required'   => true,
                'filters'    => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                $start = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $context['start']) ? \DateTime::createFromFormat('d/m/Y H:i', $context['start']) : \DateTime::createFromFormat('d/m/Y', $context['start']);
                                $end = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $context['end']) ? \DateTime::createFromFormat('d/m/Y H:i', $context['end']) : \DateTime::createFromFormat('d/m/Y', $context['end']);

                                return  $start <= $end;
                            },
                            'message' => 'task_field_end_before_start_error',
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'progress',
                'required'    => false,
                'allow_empty' => true,
                'filters'    => [
                    ['name' => 'NumberParse'],
                ],
                'validators' => [
                    [
                        'name'    => 'Between',
                        'options' => [
                            'min' => 0,
                            'max' => 100,
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'description',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return $this->users->contains($user);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $user
     * @param string $owner
     */
    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        $ownCondition = ':ownerControl MEMBER OF object.users';
        if ($owner === 'own') {
            $queryBuilder->andWhere($ownCondition);
            $queryBuilder->setParameter('ownerControl', $user);
        }
    }
}
