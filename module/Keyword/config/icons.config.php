<?php

return [
    'icons' => [
        'keyword' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-tags'
        ]
    ]
];
