<?php

return [
    'import'   => 'importer',
    'Import'   => 'Importer',
    'validate' => 'valider',
    'actions'  => 'Actions',

    'import_module_title' => 'Import',
    'import_form_title'   => 'Importer des éléments',
    'import_delimiter'    => 'Délimiteur',
    'import_enclosure'    => 'Entourage des colonnes',
    'import_escape'       => 'Caractère d\'échappement',
    'import_file'         => 'Ficher .CSV à importer',
    'import_first_line_contains_header' => 'La première ligne contient les en-têtes des colonnes ?',

    'import_message_file_not_valid' => 'Une erreur s\'est produite lors de la validation du fichier, veuillez réessayer',

    'import_message_info' => '<b>Envoyer</b> : lit le fichier .csv et l’affiche dans un tableau
    <b>Valider</b> : vérifie si le tableau est correct (indiqué en vert ou rouge)
    <b>Importer</b> : importe toutes les lignes vertes dans EVA',

    'import_message_success' => 'Les données ont bien été importées',
    'import_message_error'   => 'Les données n\'ont pas pu être importées. Une erreur s\'est produite : %1',
    'import_question_delete_line' => 'Voulez-vous réellement supprimer cette ligne ?',
    'import_question_lets_save'  => 'Vous allez tenter d\'importer %1 ligne(s), les lignes comportant des erreurs ne seront pas enregistrées, continuer ?',
    'import_message_lines_saved' => '%2 ligne(s) sur %1 ont bien été enregistrées et ont été retirées du tableau.',
    'import_message_keywords_saved' => '%2 ligne(s) sur %1 ont bien été enregistrées.',
    'import_message_keywords_invalid' => 'Mot clés invalide, identifiant ou nom attendu.',
    'import_add_line'   => 'Ajouter une ligne',
    'import_add_column' => 'Ajouter une colonne',
    'import_message_info_keyword' => '<b>Envoyer</b> : lit le fichier .csv et l’affiche dans un tableau
    <b>Importer</b> : importe toutes les lignes vertes dans EVA',

    'import_process_tooltips' => 'Aide',
    'import_process_modal' => '<u><b>Procédure</b></u> : <br>
        1 - Créer un tableur avec l’outil de tableur privilégié (Excel,Numbers, Google Sheet, Calc...) et remplir les données avec les en-têtes de colonnes correspondantes.<br>
        2 - Sauvegarder au format csv<br>
        3 - Importer dans EVA<br>
        <br>
        <br>
        <u><b>Conseils</b></u> : <br>
        - Chaque ligne correspond à une entrée EVA<br>
        - L’ordre des colonnes n’a pas d’importance, certaines cases peuvent être vides si elles ne sont pas dans les colonnes de champs obligatoires<br>
        - Dans certaines colonnes, on peut indiquer un texte librement, pour d’autres seulement certains mots sont acceptés ou seulement des chiffres, les majuscules/minuscules sont importantes et attention aux caractères spécifiques qui ne sont pas acceptés.<br>
        - Pour éviter les doublons et les mauvaises associations, dans cet import il est PRÉFÉRABLE D’UTILISER LES ID des contacts et structures accessibles dans leur tableau et par l’export'
];
