<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 16/08/16
 * Time: 09:34
 */

namespace Budget\Export;

use Core\Export\IExporter;

abstract class Account implements IExporter
{
    public static function getConfig()
    {
        return [
            'type' => [
                'property' => 'type',
                'format' => function ($value) {
                    if ('expense' != $value) {
                        return 'Recette';
                    }
                    return 'Dépense';
                }
            ],
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}