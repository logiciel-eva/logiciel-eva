<?php

namespace Core\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateClassmap extends AbstractCommand
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[START] Generate Classmap...');
        exec('php ' . getcwd() . '/composer.phar dump-autoload --optimize');
        $output->writeln('[END] Generate Classmap...');
        return Command::SUCCESS;
    }
}
