<?php

return [
    'icons' => [
        'time'     => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-clock-o',
        ],
        'calendar' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-calendar',
        ],
        'trash'    => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-trash',
        ],
        'bookmark'    => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-bookmark',
        ]
    ],
];
