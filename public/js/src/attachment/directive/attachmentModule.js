(function(app) {
    app.directive('attachmentModule', [
        'attachmentService', 'flashMessenger', 'translator', 'FileUploader',
        function attachmentModule(attachmentService, flashMessenger, translator, FileUploader) {
            return {
                restrict    : 'EA',
                require     : '^?attachment-module',
                replace     : false,
                transclude  : false,
                scope: {
                    entity: '@entity',
                    primary: '=primary',
                    right: '@right'
                },
                templateUrl : '/js/src/attachment/template/attachment.html',
                compile: function ($element, attrs) {
                    return function link ($scope, $element, attrs, $controller) {
                        $scope.attachment  = {
                            primary: $scope.primary,
                            entity: $scope.entity,
                            right: $scope.right,
                            type: 'file',
                            file: null
                        };
                        $scope.attachments = [];
                        $scope.errors = {
                            fields: {}
                        };
                        $scope.loading = {
                            save: false
                        };
                        $scope.currentUser = null;

                        
                        $scope.$on('edit-attachement', function(event, primary){
                            $scope.clearFile();
                            $scope.primary = primary;
                            $scope.attachment.primary = primary;
                            
                            attachmentService.findAll({
                                right: $scope.right,
                                col: [
                                    'id',
                                    'name',
                                    'url',
                                    'createdBy.name',
                                    'createdAt'
                                ],
                                search: {
                                    type: 'list',
                                    data: {
                                        filters: {
                                            entity: {
                                                val: $scope.entity,
                                                op: 'eq'
                                            },
                                            primary: {
                                                val: $scope.primary,
                                                op: 'eq'
                                            }
                                        }
                                    }
                                }
                            }).then(function (res) {
                                $scope.attachments = res.rows;
                            });
                        });
                        
            
                        $scope.$on('save-attachment', function (event, primary) {
                            if(!$scope.attachment.primary && primary !== null){
                                $scope.primary = primary;
                                $scope.attachment.primary = primary;
                            }
                            if($scope.attachment.file !== null){
                                $scope.saveAttachment();
                            }
                            $scope.loading.save = false;
                            $scope.showAttachmentForm = false;
                            $scope.attachment  = {
                                primary: $scope.primary,
                                entity: $scope.entity,
                                right: $scope.right,
                                type: 'file',
                                file: null
                            };
                        });

                        $scope.saveAttachment = function saveAttachment() {
                            $scope.loading.save = true;
                            attachmentService.save($scope.attachment, {
                                right: $scope.right,
                                col: [ 
                                    'id',
                                    'name',
                                    'url',
                                    'createdBy.name',
                                    'createdAt'
                                ]
                            }).then(function (res) {
                                $scope.attachments.unshift(res.object);
                                flashMessenger.success(translator('attachment_message_saved'));
                                $scope.loading.save = false;
                                $scope.showAttachmentForm = false;
                                $scope.attachment  = {
                                    primary: $scope.primary,
                                    entity: $scope.entity,
                                    right: $scope.right,
                                    type: 'file',
                                    file: null
                                };
                            }, function (err) {
                                for (var field in err.fields) {
                                    $scope.errors.fields[field] = err.fields[field];
                                }
                                $scope.loading.save = false;
                            });
                            $scope.clearFile();
                        };

                        $scope.deleteAttachment = function deleteAttachment(attachment){
                            if(confirm(translator('attachment_question_delete').replace('%1', attachment.name))){
                                attachmentService.remove(attachment, {
                                    right: $scope.right,
                                    col: [
                                        'id',
                                        'name',
                                        'url',
                                        'createdBy.name',
                                        'createdAt'
                                    ]
                                }).then(function(res){
                                    var index = 0;
                                    for(var i = 0; i < $scope.attachments.length; ++i){
                                        if($scope.attachments[i].id == attachment.id){
                                            index = i;
                                            break;
                                        }
                                    }

                                    $scope.attachments.splice(index, 1);
                                    flashMessenger.success(translator('attachment_message_deleted'));
                                });
                            }
                        };

                        $scope.uploader = new FileUploader();
                        $scope.reader   = new FileReader();
                        $scope.reader.onloadend = function(e) {
                            $scope.$apply(function() {
                                $scope.attachment.file = e.target.result;
                            });
                        };
                        $scope.clearFile =  function clearFile(){
                            if( angular.element('.attach_form_file')){
                                angular.element('.attach_form_file').val(""); 
                            }
                        };

                        $scope.uploader.onAfterAddingFile = function(fileItem) {
                            $scope.reader.readAsDataURL(fileItem._file);
                            $scope.attachment.filename = fileItem.file.name;
                            $scope.attachment.size = fileItem.file.size;
                            $scope.errors.fields['attachment'] = null;
                        };
                    }
                }
            }
        }
    ]);

})(window.EVA.app);
