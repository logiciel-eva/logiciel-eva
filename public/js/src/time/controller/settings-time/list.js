(function(app) {

    app.controller('settingsTimeController', [
        'settingsTimeService', 'flashMessenger', 'translator',
        function settingsTimeController(settingsTimeService, flashMessenger, translator) {
            var self = this;

            self.settingsTimes = [];
            self.settingsTime = {};
            self.formTitle = null;

            self.errors = {
                fields: {}
            };

            self.loading = {
                save: false
            };

            var params = {
                col: [
                    'id',
                    'name',
                    'hours'
                ],
                search: {
                    type: 'list',
                    data: {
                        sort: 'name',
                        order: 'asc'
                    }
                }
            };

            self.init = function () {
                settingsTimeService.findAll(params).then(function (res) {
                    self.settingsTimes = res.rows;
                });
            };

            self.create = function () {
                self.settingsTime = {};
                self.formTitle = translator('time_settings_time_nav_form');
                angular.element('#formModal').modal('show');
            };

            self.edit = function (settingsTime) {
                self.settingsTime = angular.copy(settingsTime);
                self.formTitle = settingsTime.name;
                angular.element('#formModal').modal('show');
            };

            self.save = function () {
                self.loading.save = true;
                self.errors = { fields: {} };
                console.log(self.settingsTime);
                settingsTimeService.save(self.settingsTime, params).then(function (res) {
                    if (res.success) {
                        if (typeof self.settingsTime.id === 'undefined') {
                            self.settingsTimes.push(res.object);
                        } else {
                            for (var i in self.settingsTimes) {
                                if (self.settingsTimes[i].id === res.object.id) {
                                    self.settingsTimes[i] = res.object;
                                    break;
                                }
                            }
                        }
                        flashMessenger.success(translator('time_settings_time_message_saved'));
                        angular.element('#formModal').modal('hide');
                    }
                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                });
            };

            self.delete = function (settingsTime) {
                if (confirm(translator('time_settings_time_question_delete').replace('%1', settingsTime.name))) {
                    settingsTimeService.remove(settingsTime).then(function (res) {
                        if (res.success) {
                            self.settingsTimes.splice(self.settingsTimes.indexOf(settingsTime), 1);
                            flashMessenger.success(translator('time_settings_time_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);