<?php

namespace Directory;

return [
    'doctrine' => [
        'driver' => [
            'directory_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],
            'orm_environment'    => [
                'drivers' => [
                    'Directory\Entity' => 'directory_entities',
                ],
            ],
        ],
    ],
];
