<?php

namespace Analysis\Controller;

use Core\Controller\AbstractActionSLController;
use Core\PhantomJS\PhantomJS;
use Core\Utils\ColorUtils;
use Core\Utils\DistributionUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class MapController extends AbstractActionSLController
{
    public function onDispatch(MvcEvent $e)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
        $translator = $helperPluginManager->get('translate');
        $budgetHTEnabled = $this->serviceLocator->get('MyModuleManager')->getClientModuleConfiguration('budget', 'ht');

        $data = [];

        if ($this->serviceLocator->get('MyModuleManager')->isActive('budget')) {
            $data['budget_module_title'] = [];
            foreach ($this->serviceLocator->get('expenseTypes') as $type) {
                $data['budget_module_title']['amountExpense' . ucfirst($type)] = [
                    'text' => ucfirst($translator->__invoke('expense_entity')) . ' - ' . $translator->__invoke('expense_type_' . $type),
                ];

                if ($budgetHTEnabled) {
                    $data['budget_module_title']['amountHTExpense' . ucfirst($type)] = [
                        'text' => ucfirst($translator->__invoke('expense_entity')) . ' - ' . $translator->__invoke('expense_type_' . $type . '_ht'),
                    ];
                }
            }

            foreach ($this->serviceLocator->get('incomeTypes') as $type) {
                $data['budget_module_title']['amountIncome' . ucfirst($type)] = [
                    'text' => ucfirst($translator->__invoke('income_entity')) . ' - ' . $translator->__invoke('income_type_' . $type),
                ];

                if ($budgetHTEnabled) {
                    $data['budget_module_title']['amountHTIncome' . ucfirst($type)] = [
                        'text' => ucfirst($translator->__invoke('income_entity')) . ' - ' . $translator->__invoke('income_type_' . $type . '_ht'),
                    ];
                }
            }
        }

        if ($this->serviceLocator->get('MyModuleManager')->isActive('time')) {
            $data['time_module_title'] = [
                'timeSpent' => [
                    'text' => $translator->__invoke('timesheet_type_done') . ' (Via ' . $translator->__invoke('project_entity') . ')'
                ],
                'timeSpentDirect' => [
                    'text' => $translator->__invoke('timesheet_type_done') . ' (Via ' . $translator->__invoke('territory_entity') . ')'
                ],
                'timeTarget' => [
                    'text' => $translator->__invoke('timesheet_type_target') . ' (Via ' . $translator->__invoke('project_entity') . ')'
                ],
                'timeTargetDirect' => [
                    'text' => $translator->__invoke('timesheet_type_target') . ' (Via ' . $translator->__invoke('territory_entity') . ')'
                ],
                'timeLeft' => [
                    'text' => $translator->__invoke('timesheet_type_left') . ' (Via ' . $translator->__invoke('project_entity') . ')'
                ],
                'timeLeftDirect' => [
                    'text' => $translator->__invoke('timesheet_type_left') . ' (Via ' . $translator->__invoke('territory_entity') . ')'
                ]
            ];
        }

        if ($this->serviceLocator->get('MyModuleManager')->isActive('indicator')) {
            $data['indicator_module_title'] = [];

            $indicators = $this->entityManager()
                    ->getRepository('Indicator\Entity\Indicator')
                    ->findAll();

            foreach ($indicators as $indicator) {
                $data['indicator_module_title']['indicatorDone' . $indicator->getId()] = [
                    'text' => $indicator->getName() . ' - ' . $translator->__invoke('measure_type_done') . ' (Via ' . $translator->__invoke('project_entity') . ')'
                ];
                $data['indicator_module_title']['indicatorDoneDirect' . $indicator->getId()] = [
                    'text' => $indicator->getName() . ' - ' . $translator->__invoke('measure_type_done') . ' (Via ' . $translator->__invoke('territory_entity') . ')'
                ];
                $data['indicator_module_title']['indicatorTarget' . $indicator->getId()] = [
                    'text' => $indicator->getName() . ' - ' . $translator->__invoke('measure_type_target') . ' (Via ' . $translator->__invoke('project_entity') . ')'
                ];
                $data['indicator_module_title']['indicatorTargetDirect' . $indicator->getId()] = [
                    'text' => $indicator->getName() . ' - ' . $translator->__invoke('measure_type_target') . ' (Via ' . $translator->__invoke('territory_entity') . ')'
                ];
            }
        }

        $preConfiguration = $this->params()->fromQuery();

        // To fix a bug happening in production where the json object is sent as string.
        foreach ($preConfiguration as $key => $config) {
            $preConfiguration[$key] = is_string($config) ? urldecode($config) : $config;
        }

        return new ViewModel([
            'data'             => $data,
            'preConfiguration' => $preConfiguration,
            'export'           => $this->params()->fromQuery('export', false)
        ]);
    }

    /**
     * @deprecated by Leaflet plugins
     */
    public function exportAction()
    {
        $host = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : 'http://' . $_SERVER['HTTP_HOST'];

        $authCookie = $this->getRequest()->getCookie()->getFieldValue();
        $image = getcwd() . '/data/phpdocx/temp/export_map_analysis_' . time() . '.png';
        $requestQuery = $host . '/analysis/map?export=true&' . $_SERVER['QUERY_STRING'];

        PhantomJS::captureWaitFor($requestQuery, $image, '800px*910px', $authCookie);

        header('Cache-Control: private');
        header('Content-Type: application/stream');
        header('Content-Length: ' . filesize($image));
        header('Content-Disposition: attachment; filename=export.png');

        readfile($image);
        unlink($image);
        exit();
    }

    public function territoriesAction()
    {
        $filters  = $this->params()->fromQuery('filters', []);
        $isMaster = $this->environment()->getClient()->isMaster();

        $territoryFilters = isset($filters['territory']) ? $filters['territory'] : [];

        if (isset($filters['project'])) {
            $request = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
                'col'    => ['territories'],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'name',
                        'order'   => 'asc',
                        'filters' => $filters['project']
                    ],
                ],
                'master' => $isMaster
            ]);
            $request->getController()->setFromConsole(true);
            $projects = $request->dispatch()->getVariable('rows');

            foreach ($projects as $project) {
                if ($project['territories']) {
                    foreach($project['territories'] as $territory) {
                        if (!isset($territoryFilters['id'])) {
                            $territoryFilters['id'] = [
                                'val' => [],
                                'op' => 'eq'
                            ];
                        }
                        if (!in_array($territory['id'], $territoryFilters['id']['val'])) {
                            $territoryFilters['id']['val'][] = $territory['id'];
                        }
                    }
                }
            }
        }

        $request = $this->apiRequest('Map\Controller\API\Territory', Request::METHOD_GET, [
            'col'    => ['id', 'name', 'code', 'json'],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort'    => 'name',
                    'order'   => 'asc',
                    'filters' => $territoryFilters
                ],
            ],
            'master' => $isMaster,
        ]);
        $request->getController()->setFromConsole(true);
        $territories = $request->dispatch()->getVariable('rows');

        return new JsonModel($territories);
    }

    public function dataAction()
    {
        $configuration = $this->params()->fromQuery('configuration', []);
        if ($configuration['type'] == 'details') {
            $territories = [['id' => $configuration['territory']]];
        } else {
            $territories = $this->territoriesAction()->getVariables();
        }

        if (count($territories) === 0) {
            return new JsonModel([]);
        }

        $em  = $this->entityManager();
        $res = [
            'data' => []
        ];

        $filters  = $this->params()->fromQuery('filters', []);
        $projectsFilters = isset($filters['project']) ? $filters['project'] : [];

        $min = null;
        $max = null;

        $year = isset($configuration['year']) ? $configuration['year'] : null;
        if ($year === '') {
            $year = null;
        }

        foreach ($territories as $_territory) {
            $territory = $em->getRepository('Map\Entity\Territory')->find($_territory['id']);

            $_projectsFilters = $projectsFilters;
            $_projectsFilters['territory'] = [
                'op'  => 'eq',
                'val' =>  $territory->getId()
            ];

            $request =  $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
                'col'    => ['id'],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'name',
                        'order'   => 'asc',
                        'filters' => $_projectsFilters
                    ],
                ]
            ]);
            $request->getController()->setFromConsole(true);
            $projects = $request->dispatch()->getVariable('rows');

            $_projects = new ArrayCollection();
            foreach ($projects as $project) {
                $_projects->add($em->getRepository('Project\Entity\Project')->find($project['id']));
            }

            if ($configuration['type'] === 'details') {

                $years = [];
                $keys = $this->indexAction()->getVariables()['data'];

                foreach ($configuration['data'] as $i => $key) {
                    if ($key) {
                        $bounds = $territory->getYearsBounds($key, $em);
                        if ($bounds['min'] && $bounds['max']) {
                            $text = $key;
                            foreach ($keys as $module => $moduleKeys) {
                                foreach ($moduleKeys as $_key => $_text) {
                                    if ($_key === $key) {
                                        $text = $_text['text'];
                                    }
                                }
                            }
                            $data = [
                                'key'    => $text,
                                'direct' => preg_match('/Direct[0-9]*$/', $key),
                                'years'  => []
                            ];

                            for ($year = $bounds['min']; $year <= $bounds['max']; $year++) {
                                $years[] = $year;
                                $data['years'][$year] = [
                                    'value' => null
                                ];
                                if ($data['direct']) {
                                    $value = $territory->{'get' . ucfirst($key)}($year, $em);
                                    $data['years'][$year]['value'] = $value;
                                } else {
                                    $territory->setProjects($_projects);
                                    $data['years'][$year]['value']  = $territory->{'get' . ucfirst($key)}($year, $em);;
                                    $data['years'][$year]['stacks'] = [];
                                    foreach ($_projects as $project) {
                                        $territory->setProjects(new ArrayCollection([$project]));
                                        $data['years'][$year]['stacks'][] = [
                                            'k' => $project->getName(),
                                            'v' => $territory->{'get' . ucfirst($key)}($year, $em)
                                        ];
                                    }
                                }
                            }

                            $res['data'][] = $data;
                        }
                    }
                }

                $years = array_unique($years);
                sort($years);

                $res['years'] = $years;

            } elseif($configuration['type'] === 'color') {
                $territory->setProjects($_projects);
                $total = 0;
                foreach ($configuration['data'] as $data) {
                    $total += $territory->{'get' . ucfirst($data)}($year, $em);
                }
                //$data = $territory->{'get' . ucfirst($configuration['data'])}($year, $em);
                $res['data'][] = [
                    'id'   => $territory->getId(),
                    'data' => $total
                ];
            } elseif ($configuration['type'] === 'proportional') {

                $territory->setProjects($_projects);
                $total = $territory->{'get' . ucfirst($configuration['data'])}($year, $em);
                $data  = [];
                if (preg_match('/Direct[0-9]*$/', $configuration['data'])) {
                    $data[] = [
                        'project' => [
                            'id'   => null,
                            'name' => $territory->getName()
                        ],
                        'data' => $total
                    ];
                } else {
                    foreach ($_projects as $project) {
                        $territory->setProjects(new ArrayCollection([$project]));
                        $data[] = [
                            'project' => [
                                'id'   => $project->getId(),
                                'name' => $project->getName()
                            ],
                            'data' => $territory->{'get' . ucfirst($configuration['data'])}($year, $em)
                        ];
                    }
                }

                $res['data'][] = [
                    'id'    => $territory->getId(),
                    'total' => $total,
                    'data'  => $data
                ];

                $min = ($min == null || $total < $min ? $total : $min);
                $max = ($max == null || $total > $max ? $total : $max);
            }
        }

        if ($configuration['type'] === 'color') {
            $res['colors'] = ColorUtils::gradient($configuration['color']['min'], $configuration['color']['max'], $configuration['classes']);
            $res['distribution'] = DistributionUtils::distribute(array_map(function($r) {
                return $r['data'];
            }, $res['data']), $configuration['distribution'], $configuration['classes']);
        }

        if ($configuration['type'] === 'proportional') {
            $res['min'] = $min;
            $res['max'] = $max;
        }

        return new JsonModel($res);
    }
}
