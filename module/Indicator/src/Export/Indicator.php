<?php

namespace Indicator\Export;

use Core\Export\IExporter;

abstract class Indicator implements IExporter
{
    public static function getConfig()
    {
        return [
            'groupIndicators' => [
                'property' => 'groupIndicators.name'
            ],
            'type' => [
                'format' => function ($value, $obj, $translator) {
                    return $translator('indicator_type_' . $value);
                }
            ],
            'operator' => [
                'format' => function ($value, $obj, $translator) {
                    return $translator('indicator_operator_' . $value);
                }
            ]
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
