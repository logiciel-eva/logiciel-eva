<?php

namespace User\Controller\API;

use Core\Controller\AbstractActionSLController;
use User\Entity\Configuration;
use Laminas\View\Model\JsonModel;

class ConfigurationController extends AbstractActionSLController
{
    public function getTableAction()
    {
        $name = $this->params()->fromQuery('name', null);

        if (preg_match('/^container-admin-\d+$/', $name)) {
            $containerId = str_replace('container-admin-', '', $name);
            $container = $this->entityManager()->getRepository('Home\Entity\ContainerList')->find($containerId);

            return new JsonModel($container->getColumns());
        } else {
            $configuration = $this->getConfiguration();
            return new JsonModel($configuration->getTable($name));
        }
    }

    public function saveTableAction()
    {
        $em = $this->entityManager();
        $name = $this->params()->fromPost('name', null);
        $cols = $this->params()->fromPost('cols', []);

        if (preg_match('/^container-admin-\d+$/', $name)) {
            $containerId = str_replace('container-admin-', '', $name);
            $container = $this->entityManager()->getRepository('Home\Entity\ContainerList')->find($containerId);
            $container->setColumns($cols);

            $em->persist($container);
            $em->flush();

            return new JsonModel([
                'success' => true
            ]);
        } else {
            $configuration = $this->getConfiguration();

            $configuration->setTable($name, $cols);
            $em->flush($configuration);

            return new JsonModel([
                'success' => true
            ]);
        }
    }

    public function create()
    {
        $em   = $this->entityManager();
        $user = $this->authStorage()->getUser();

        $configuration = new Configuration();
        $configuration->setUser($user);

        $em->persist($configuration);
        $em->flush();

        return $configuration;
    }

    public function getConfiguration()
    {
        $em   = $this->entityManager();
        $user = $this->authStorage()->getUser();

        $configuration = $em->getRepository('User\Entity\Configuration')->findOneByUser($user);
        if ($configuration === null) {
            $configuration = $this->create();
        }

        return $configuration;
    }

    public function getConfigurationWithTableAction()
    {
        $name = $this->params()->fromQuery('name', null);
        $configuration = $this->getConfiguration();

        return new JsonModel(
            array('table' => $configuration->getTable($name), 'resizableTable' => $configuration->getResizableTable())
        );
    }

    public function saveCustomisationAction()
    {
        $em = $this->entityManager();
        $customisation = $this->params()->fromPost('customisation', null);
        $configuration = $this->getConfiguration();
        if(is_array($customisation)) {
            $configuration->setResizableTable($customisation['resizableTable'] === 'true' ? 1 : 0);
        }
        $em->flush($configuration);

        return new JsonModel([
            'success' => true,

        ]);
    }
}
