(function(app) {

    app.controller('envelopeListController', [
        'envelopeService', '$scope', 'flashMessenger', 'translator',
        function envelopeListController(envelopeService, $scope, flashMessenger, translator) {
            var self = this;

            self.envelopes = [];
            self.options = {
                name: 'envelopes-list',
                col: [
                    'id',
                    'code',
                    'name',
                    'financer.id',
                    'financer.name',
                    'start',
                    'end',
                    'amount',
                    'createdAt',
                    'updatedAt',
                    'keywords',
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: false,
                getItems: function getData(params, callback, abort) {
                    envelopeService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    });
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(envelope) {
                if (angular.isArray(envelope.keywords) || envelope.keywords == null) {
                    envelope.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = envelope;
                self.editedLineFields = angular.copy(envelope);
            };

            self.saveEditedLine = function saveEditedLine(envelope) {
                self.editedLineErrors = {};
                envelopeService.save(angular.extend({}, envelope, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        envelope = angular.extend(envelope, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('envelope_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteEnvelope = function deleteEnvelope(envelope) {
                if (confirm(translator('envelope_question_delete').replace('%1', envelope.name))) {
                    envelopeService.remove(envelope).then(function (res) {
                        if (res.success) {
                            self.envelopes.splice(self.envelopes.indexOf(envelope), 1);
                            flashMessenger.success(translator('envelope_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
