(function(app) {

    app.directive('groupIndicatorSelect', [
        'groupIndicatorService', 'selectFactoryService',
        function groupIndicatorSelect(groupIndicatorService, selectFactoryService) {
            var optgroups = [
                {label: 'A', value: 'fixed'},
                {label: 'B', value: 'free'}
            ];

            return selectFactoryService.create({
                service : {
                    object: groupIndicatorService,
                    apiParams: {
                        col: ['id', 'name']
                    }
                },
                selectize: {
                    optgroups: optgroups,
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name'],
                    onLoad: function(scope, data) {
                        // ??
                        var t =  {id: 55555, name: 'intest'};
                        if (data) {
                            data.push(t);
                        }                        
                    },
                    optgroupField: 'type'
                }
            });
        }
    ])

})(window.EVA.app);
