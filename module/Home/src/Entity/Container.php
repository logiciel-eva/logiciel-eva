<?php
namespace Home\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Container
 *
 * @package Home\Entity
 *
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     Container::TYPE_TEXT     = "ContainerText",
 *     Container::TYPE_LIST     = "ContainerList",
 *     Container::TYPE_CALENDAR = "ContainerCalendar",
 *     Container::TYPE_CHAT     = "ContainerChat",
 *     Container::TYPE_GRAPHIC  = "ContainerGraphic"
 * })
 * @ORM\Table(name="home_container")
 */
abstract class Container extends BasicRestEntityAbstract
{
    const TYPE_TEXT     = 'text';
    const TYPE_LIST     = 'list';
    const TYPE_CALENDAR = 'calendar';
    const TYPE_CHAT     = 'chat';
    const TYPE_GRAPHIC  = 'graphic';

    const WIDTH_ONE_QUARTER    = '1/4';
    const WIDTH_ONE_THIRD      = '1/3';
    const WIDTH_ONE_HALF       = '1/2';
    const WIDTH_TWO_THIRDS     = '2/3';
    const WIDTH_THREE_QUARTERS = '3/4';
    const WIDTH_ONE            = '1';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="`order`")
     */
    protected $order;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="width")
     */
    protected $width;

    /**
     * @ORM\ManyToOne(targetEntity="Home", inversedBy="containers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $home;

    /**
     * @ORM\ManyToOne(targetEntity="HomeAdmin", inversedBy="containers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $homeAdmin;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param string $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return Home
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param Home $home
     */
    public function setHome($home)
    {
        $this->home = $home;
    }

    /**
     * @return HomeAdmin
     */
    public function getHomeAdmin()
    {
        return $this->homeAdmin;
    }

    /**
     * @param HomeAdmin $homeAdmin
     */
    public function setHomeAdmin($homeAdmin)
    {
        $this->homeAdmin = $homeAdmin;
    }

    public function getContent() { }

    public function setContent($content) { }

    public function getEntity() { }

    public function setEntity($entity) { }

    public function getQuery() { }

    public function setQuery($query) { }

    public function getGraphicType() { }

    public function setGraphicType($graphicType) {}

    public function getColumns() {}

    public function setColumns($columns) {}

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'title',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'order',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse'],
                ],
            ],
            [
                'name'      => 'type',
                'required'  => true,
                'validator' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => self::getTypes(),
                        ],
                    ],
                ],
            ],
            [
                'name'      => 'width',
                'required'  => true,
                'validator' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => self::getWidths(),
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'home',
                'required'   => false,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                $home = $entityManager->getRepository('Home\Entity\Home')->find($value);

                                return $home !== null;
                            },
                            'message'  => 'container_field_home_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'homeAdmin',
                'required'   => false,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                $home = $entityManager->getRepository('Home\Entity\HomeAdmin')->find($value);

                                return $home !== null;
                            },
                            'message'  => 'container_field_homeAdmin_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'      => 'columns',
                'required'  => false,
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_TEXT,
            self::TYPE_LIST,
            self::TYPE_CALENDAR,
            self::TYPE_CHAT,
            self::TYPE_GRAPHIC,
        ];
    }

    /**
     * @return array
     */
    public static function getWidths()
    {
        return [
            self::WIDTH_ONE_QUARTER,
            self::WIDTH_ONE_THIRD,
            self::WIDTH_ONE_HALF,
            self::WIDTH_TWO_THIRDS,
            self::WIDTH_THREE_QUARTERS,
            self::WIDTH_ONE,
        ];
    }
}
