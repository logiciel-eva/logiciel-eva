<?php

namespace Budget\Helper;

final class ImportBudgetHelper
{
    private const IMPORT_ROOT_DIR = 'data/imports';
    private const DONE_FOLDERS_NAME = 'done';

    private string $path;
    public function __construct(private string $csvFolder)
    {
        $this->path = getcwd() . DIRECTORY_SEPARATOR . self::IMPORT_ROOT_DIR;
    }

    public function hasFiles(): bool
    {
        return $this->getFiles() !== [];
    }
    public function getFiles()
    {
        return glob($this->getCsvFolderRootPath() . '/*.csv');
    }

    public function getCsvFolderRootPath()
    {
        return $this->path . DIRECTORY_SEPARATOR . $this->csvFolder;
    }

    public function getCsvFolderDonePath()
    {
        return $this->getCsvFolderRootPath() . DIRECTORY_SEPARATOR . self::DONE_FOLDERS_NAME;
    }


}