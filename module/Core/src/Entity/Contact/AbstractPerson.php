<?php

namespace Core\Entity\Contact;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractPerson
 * @package Core\Entity\Contact
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractPerson extends AbstractContact
{
    const PERSON_GENDER_M   = 'm';
    const PERSON_GENDER_MME = 'mme';
    const PERSON_GENDER_NEUTRAL = 'neutral';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $gender = self::PERSON_GENDER_M;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $lastName;

    /**
     * @return string
     */
    public function getGender()
    {
        if (!$this->gender) {
            $this->gender = self::PERSON_GENDER_M;
        }
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        if (!$gender) {
            $gender = self::PERSON_GENDER_M;
        }
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        $this->name = $this->getName();
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        $this->name = $this->getName();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->remove('name');
        //$inputFilter->get('name')->setRequired(false);

        $inputFilter->add([
            'name'     => 'gender',
            'required' => false,
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => self::getGenders()
                    ],
                ]
            ],
        ], 'gender');

        $inputFilter->add([
            'name'     => 'firstName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
        ], 'firstName');

        $inputFilter->add([
            'name'     => 'lastName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
        ], 'lastName');

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getGenders()
    {
        return [
            self::PERSON_GENDER_M,
            self::PERSON_GENDER_MME,
            self::PERSON_GENDER_NEUTRAL
        ];
    }
}
