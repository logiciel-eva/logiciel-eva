<?php

return [
    'keyword'  => '[[ keyword_entity | ucfirst ]]',
    'keywords' => '[[ keyword_entities | ucfirst ]]',

    'keywords_keyword'   => '[[ keywords ]]',
    'keyword_reference'  => '[[ group_type_reference ]]',
    'keywords_reference' => '[[ group_type_reference ]]s',
    'reference_entity'   => 'Réferentiel',


    'keyword_module_title' => '[[ keyword_entities | ucfirst ]]',
    'keyword_module_title2' => 'Indexage',
    'keyword_fusion_title' => 'Fusionner des [[ keyword_entities ]]',

    'keyword_nav_list' => 'Liste des {{ __tb.total }} [[ keyword_entities ]]',
    'keyword_nav_form' => 'Créer un [[ keyword_entity ]]',

    'group_entity'     => 'groupe',
    'keyword_entity'   => 'mot-clé',
    'keyword_entities' => 'mots-clés',

    'group_nav_form' => 'Créer un [[ group_entity ]]',

    'group_field_name'     => 'Nom',
    'group_field_multiple' => 'Multiple',
    'group_field_entities' => 'Objets',
    'group_field_services' => '[[ service_entities | ucfirst ]]',
    'group_type_reference' => 'Référentiel',
    'group_type_keyword'   => '[[ keyword_entity | ucfirst ]]',

    'group_question_delete'    => 'Voulez-vous réellement supprimer le [[ group_entity ]] de [[ keyword_entities ]] %1 ?',
    'group_message_deleted'    => '[[ group_entity | ucfirst ]] supprimé',
    'group_message_saved'      => '[[ group_entity | ucfirst ]] enregistré',
    'group_message_archived'   => '[[ group_entity | ucfirst ]] archivé',
    'group_message_unarchived' => '[[ group_entity | ucfirst ]] désarchivé',

    'keyword_field_name'        => 'Nom',
    'keyword_field_description' => 'Description',
    'keyword_field_parents'     => 'Parents',
    'keyword_field_order'       => 'Ordre',
    'keyword_field_createdAt'   => '[[ field_created_at_m ]]',
    'keyword_field_updatedAt'   => '[[ field_updated_at_m ]]',

    'keyword_question_delete' => 'Voulez-vous réellement supprimer le [[ keyword_entity ]] %1 ?',
    'keyword_message_deleted' => '[[ keyword_entity | ucfirst ]] supprimé',

    'keyword_field_group_error_non_exists' => 'Ce [[ group_entity ]] n\'existe pas ou plus',

    'keyword_message_saved'      => '[[ keyword_entity | ucfirst ]] enregistré',
    'keyword_message_archived'   => '[[ keyword_entity | ucfirst ]] archivé',
    'keyword_message_unarchived' => '[[ keyword_entity | ucfirst ]] désarchivé',
    'keyword_nav_import' => 'Importer des mots clés',
    'referential_nav_import' => 'Importer des référentiels',

];
