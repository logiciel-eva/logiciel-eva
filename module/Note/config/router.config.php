<?php

namespace Note;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'note' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/note',
                    'defaults' => [
                        'controller' => 'Note\Controller\Index',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
            ],
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'note' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/note[/:id]',
                            'defaults' => [
                                'controller' => 'Note\Controller\API\Note'
                            ]
                        ]
                    ],
                ]
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class    => ServiceLocatorFactory::class,
            Controller\API\NoteController::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Note\Controller\Index'    => Controller\IndexController::class,
            'Note\Controller\API\Note' => Controller\API\NoteController::class
        ]
    ]
];
