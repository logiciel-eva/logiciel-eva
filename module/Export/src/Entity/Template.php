<?php

namespace Export\Entity;

use Budget\Entity\PosteIncome;
use Budget\Helper\IBudgetComputable;
use Core\Extractor\ObjectExtractor;
use Core\PhantomJS\PhantomJS;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Indicator\Entity\Indicator;
use Indicator\Entity\Measure;
use Keyword\Entity\Association;
use Keyword\Entity\Keyword;
use Project\Entity\Project;

/**
 * Class Template
 *
 * @package Export\Entity
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 * @ORM\Entity
 * @ORM\Table(name="export_template")
 */
class Template
{
	const PAGEBREAK_NONE = 'none';
	const PAGEBREAK_BEFORE = 'before';
	const PAGEBREAK_AFTER = 'after';
	const PAGEBREAK_BOTH = 'both';

	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var Export
	 * @ORM\ManyToOne(targetEntity="Export\Entity\Export", inversedBy="templates")
	 */
	protected $export;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $content;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $type;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $pageBreak;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return Export
	 */
	public function getExport()
	{
		return $this->export;
	}

	/**
	 * @param Export $export
	 */
	public function setExport($export)
	{
		$this->export = $export;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getPageBreak()
	{
		return $this->pageBreak;
	}

	/**
	 * @param string $pageBreak
	 */
	public function setPageBreak($pageBreak)
	{
		$this->pageBreak = $pageBreak;
	}

	public static function getPageBreaks()
	{
		return [
			self::PAGEBREAK_NONE,
			self::PAGEBREAK_AFTER,
			self::PAGEBREAK_BEFORE,
			self::PAGEBREAK_BOTH,
		];
	}

	/**
	 * @param Project|Keyword 			$object
	 * @param EntityManager   			$em
	 * @param Translate       			$translator
	 * @param array           			$globalVariables
	 * @param string          			$exportIdentifier
	 * @param bool            			$excel
	 *
	 * @return string|array
	 */
	public function parseContent(
		$object,
		$em,
		$translator,
		&$globalVariables,
		$exportIdentifier,
		$excel = false
	) {
		$class = str_replace('DoctrineORMModule\Proxy\__CG__\\', '', get_class($object));
		$isProject = $class == 'Project\Entity\Project';
		$isKeyword = $class == 'Keyword\Entity\Keyword';

		$year = strlen($this->export->getYear()) == 4 ? $this->export->getYear() : null;

		$extractor = new ObjectExtractor();
		$extractor->setExporter(str_replace('Entity', 'Export', $class));
		$extractor->setEntityManager($em);
		$extractor->setObject($object);
		$extractor->setTranslator($translator);

		$index = uniqid();

		$content = $this->getContent();

		$variables = [];
		preg_match_all('/\$(.*)\$/U', $content, $variables);

		if ($excel) {
			foreach ($variables[1] as $i => $variable) {
				$variables[1][$i] = explode('|||', $variable)[1];
			}
		}

		if ($variables) {
			$replacements = [];
			foreach ($variables[1] as $variableStr) {
				$variable = new Variable($variableStr);

				$identifier = $variable->getIdentifier();
				$params = $variable->getParams();

				if ($isKeyword) {
					if ($identifier === 'name') {
						$replacements[] = $extractor->extract($identifier, null, ' ; ', $variableStr);
						continue;
					}
					if($excel) {
						$replacements[] = null;
						continue;
					}
				}

				$replacement = '$' . $variableStr . $index . '$';
				$globalVariable = null;

				// Budget Amounts
				if (preg_match('/^amount[Expense|Income]/', $identifier)) {
					$isArborescent = $params[0] === 'true';

					if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
						$isArborescent = false;
					}

					$evolution = $params[1] ?: null;

					$fn = 'get' . ucfirst($identifier);
					if ($isArborescent) {
						$fn = str_replace(['Expense', 'Income'], ['ExpenseArbo', 'IncomeArbo'], $fn);
					}

					$replacement = number_format($object->{$fn}($year, $em, $evolution), 2, '.', ' ');
					$identifier = 'null';
				} elseif (preg_match('/^amountHT[Expense|Income]/', $identifier)) {
					$isArborescent = $params[0] === 'true';

					if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
						$isArborescent = false;
					}

					$evolution = $params[1] ?: null;

					$fn = 'get' . ucfirst($identifier);
					if ($isArborescent) {
						$fn = str_replace(['Expense', 'Income'], ['ExpenseArbo', 'IncomeArbo'], $fn);
					}

					$replacement = number_format($object->{$fn}($year, $em, $evolution), 2, '.', ' ');
					$identifier = 'null';
				}

				switch ($identifier) {
					case 'null':
						break;

						/***********************
						 *        PROJECT      *
						 ***********************/
					case 'parent':
						if ($isProject) {
							$arbo = $params[0] === 'true';
							$replacement = '';

							if (!$arbo) {
								/** @var Project $projectOriginal */
								$projectOriginal = $em
									->getRepository('Project\Entity\Project')
									->find($object->getId());
								$replacement = $projectOriginal->getParent()
									? $projectOriginal->getParent()->getName()
									: '';
							} else {
								/** @var Project $projectOriginal */
								$projectOriginal = $em
									->getRepository('Project\Entity\Project')
									->find($object->getId());
								$parents_sup = $projectOriginal->getHierarchySup();
								$parents_sup = $parents_sup->toArray();

								$flatArray = [];

								foreach ($parents_sup as $parent_sup) {
									if (!isset($flatArray[$parent_sup->getId()])) {
										$flatArray[$parent_sup->getId()] = [];
									}
								}

								foreach ($flatArray as $project_id => $values) {
									/** @var Project $project */
									$project = $em->getRepository('Project\Entity\Project')->find($project_id);
									if ($project->getParent()) {
										$flatArray[$project_id]['parent'] = $project->getParent()->getId();
									}
									$flatArray[$project_id]['id'] = $project_id;
									$flatArray[$project_id]['name'] = $project->getName();
								}

								$tree = [];
								$n = sizeOf($flatArray);

								do {
									$relaunch = false;
									foreach ($flatArray as $i => $item) {
										$item['children'] = [];
										$item['level'] = 0;
										if (!isset($item['parent']) || $item['parent'] == null) {
											$tree[$i] = $item;
											$n--;
											$relaunch = true;
											unset($flatArray[$i]);
										} else {
											if ($this->insertInTree($item, $tree, 1)) {
												unset($flatArray[$i]);
												$n--;
												$relaunch = true;
											}
										}
									}
								} while ($n > 0 && $relaunch);

								foreach ($tree as $parent) {
									$replacement .= $this->getNameArboRecursive(
										$parent,
										$projectOriginal->getName(),
										$excel
									);
								}

								if (!$excel) {
									$replacement .= '</li></ul>';
								}
							}
						}
						break;
					case 'children':
						if ($isProject) {
							$properties =
								is_array($params[0]) && !array_diff($params[0], ['code', 'name'])
								? $params[0]
								: null;
							$depth = $params[1] === 'direct' || $params[1] === 'all' ? $params[1] : null;
							$format = $params[2] === 'comma' || $params[2] === 'list' ? $params[2] : null;

							if (!isset($properties) || !isset($depth) || !isset($format)) {
								$replacement = 'La variable $children$ est mal configurée.';
							} else {
								$replacement = '';
								/** @var Project $projectOriginal */
								$projectOriginal = $em
									->getRepository('Project\Entity\Project')
									->find($object->getId());

								$children = [];
								switch ($depth) {
									case 'direct':
										$children = $projectOriginal->getChildren()->toArray();
										break;
									case 'all':
										$children = $projectOriginal->getHierarchySub()->toArray();
								}

								if (count($children) > 0) {
									switch ($format) {
										case 'comma':
											foreach ($children as $child) {
												foreach ($properties as $property) {
													$replacement .= $child->{'get' . ucfirst($property)}() . ' ';
												}
												$replacement .= '; ';
											}
											$replacement = rtrim($replacement, ' ; ');
											break;
										case 'list':
											$start = $excel ? '' : '<ul><li>';
											$tab = $excel ? '    ' : '';
											$endLine = $excel ? "\n" : '';
											$end = $excel ? '' : '</li></ul>';
											$newItem = $excel ? '' : '</li><li>';

											$lastLevel = $children[0]->getLevel();
											$replacement .= $start;

											foreach ($children as $i => $child) {
												if ($i !== 0) {
													if ($child->getLevel() > $lastLevel) {
														$replacement .= $start;
													} elseif ($child->getLevel() < $lastLevel) {
														$replacement .=
															str_repeat($end, $lastLevel - $child->getLevel()) . $newItem;
													} else {
														$replacement .= $newItem;
													}
												}

												$replacement .= str_repeat(
													$tab,
													$child->getLevel() - $children[0]->getLevel()
												);
												foreach ($properties as $property) {
													$replacement .= $child->{'get' . ucfirst($property)}() . ' ';
												}

												$replacement .= $endLine;
												$lastLevel = $child->getLevel();
											}
											if ($excel) {
												$replacement = rtrim($replacement, "\n");
											}
											$replacement .= str_repeat($end, $lastLevel - $children[0]->getLevel() + 1);
											break;
									}
								}
							}
						}
						break;

						/***********************
						 *         TIME        *
						 ***********************/
					case 'timeSpent':
					case 'timeTarget':
					case 'timeLeft':
						$isArborescent = $params[0] === 'true';
						if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
							$isArborescent = false;
						}
						$replacement = $object->{'get' . ucfirst($identifier) . ($isArborescent ? 'Arbo' : '')}(
							$year,
							$em
						);
						break;
					case 'userTimeRepartition':
						$isArborescent = $params[0] === 'true';
						$columns = is_array($params[1]) ? $params[1] : [];
						$byYear = $params[2] === 'true';
						$yearMin = $params[3];
						$yearMax = $params[4];
						$convertToDays = $params[5] === 'true';
						$hoursByDay = $params[6];

						$byDays = $convertToDays && $hoursByDay;

						if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
							$isArborescent = false;
						}

						$repartition = $object->{'get' .
							ucfirst($identifier) . ($byYear ? 'ByYear' : '') . ($isArborescent ? 'Arbo' : '')}($year, $em);

						$getWorkHoursPerDay = function ($user) use ($hoursByDay) {
							$user_hours = $user['hour_per_day'];
							$profile_hours = $user['profile_hour_per_day'];
							$param_hours = $hoursByDay;


							if (!empty($user_hours)) {
								return $user_hours;
							}

							if (!empty($profile_hours)) {
								return $profile_hours;
							}

							return $param_hours;
						};

						if ($byDays) {
							foreach ($repartition as $i => $user) {
								if (isset($user['years'])) {
									foreach ($user['years'] as $j => $y) {

										$hoursByDayUser = $getWorkHoursPerDay($user['user']);

										$repartition[$i]['years'][$j]['done'] = round($y['done'] / $hoursByDayUser, 2);
										$repartition[$i]['years'][$j]['target'] = round($y['target'] / $hoursByDayUser, 2);
										$repartition[$i]['years'][$j]['left'] = round($y['left'] / $hoursByDayUser, 2);
									}

									continue;
								}

								$hoursByDayUser = $getWorkHoursPerDay($user['user']);

								$repartition[$i]['done'] = round($user['done'] / $hoursByDayUser, 2);
								$repartition[$i]['target'] = round($user['target'] / $hoursByDayUser, 2);
								$repartition[$i]['left'] = round($user['left'] / $hoursByDayUser, 2);
							}
						}

						if ($byYear) {
							$minYear = null;
							$maxYear = null;

							foreach ($repartition as $user) {
								foreach ($user['years'] as $yearKey => $values) {
									if (!$minYear) {
										$minYear = $yearKey;
										$maxYear = $yearKey;
										continue;
									}

									if ($yearKey < $minYear) {
										$minYear = $yearKey;
									}

									if ($yearKey > $maxYear) {
										$maxYear = $yearKey;
									}
								}
							}

							$years = [];

							for ($i = $minYear; $i <= $maxYear; $i++) {
								if ($yearMin && $i < $yearMin) {
									continue;
								}

								if ($yearMax && $i > $yearMax) {
									continue;
								}

								$years[] = $i;
							}
						}

						$replacement = '<table class="table">';
						$replacement .= '<thead>';

						if ($byYear) {
							$replacement .= '<tr>';
							$replacement .= '<th colspan="1"></th>';

							$columnsLength = count($columns);

							foreach ($years as $y) {
								$replacement .= '<th colspan="' . $columnsLength . '">' . $y . '</th>';
							}

							$replacement .= '</tr>';
						}



						$replacement .= '<tr>';
						$replacement .= '<th>' . $translator->__invoke('timesheet_field_user') . '</th>';
						$replacement .= in_array('target', $columns)
							? '<th>' . $translator->__invoke('timesheet_type_profile') . '</th>'
							: '';

						$replacement .= in_array('profile_cost', $columns)
							? '<th>' . $translator->__invoke('timesheet_type_profile_cost') . '</th>'
							: '';

						if (!$byYear) {
							$replacement .= in_array('target', $columns)
								? '<th>' . $translator->__invoke('timesheet_type_target') . '</th>'
								: '';
							$replacement .= in_array('target_cost', $columns)
								? '<th>' . $translator->__invoke('timesheet_type_target_cost') . '</th>'
								: '';

							$replacement .= in_array('done', $columns)
								? '<th>' . $translator->__invoke('timesheet_type_done') . '</th>'
								: '';

							$replacement .= in_array('done_cost', $columns)
								? '<th>' . $translator->__invoke('timesheet_type_done_cost') . '</th>'
								: '';

							$replacement .= in_array('left', $columns)
								? '<th>' . $translator->__invoke('timesheet_type_left') . '</th>'
								: '';
						} else {
							foreach ($years as $y) {
								$replacement .= in_array('target', $columns)
									? '<th>' . $translator->__invoke('timesheet_type_target') . '</th>'
									: '';
								$replacement .= in_array('target_cost', $columns)
									? '<th>' . $translator->__invoke('timesheet_type_target_cost') . '</th>'
									: '';
								$replacement .= in_array('done', $columns)
									? '<th>' . $translator->__invoke('timesheet_type_done') . '</th>'
									: '';
								$replacement .= in_array('done_cost', $columns)
									? '<th>' . $translator->__invoke('timesheet_type_done_cost') . '</th>'
									: '';
								$replacement .= in_array('left', $columns)
									? '<th>' . $translator->__invoke('timesheet_type_left') . '</th>'
									: '';
							}
						}

						$replacement .= '</tr>';
						$replacement .= '</thead>';
						$replacement .= '<tbody>';

						foreach ($repartition as $user) {
							$replacement .= '<tr>';
							$replacement .= '<td>' . ($user['user'] ? $user['user']['name'] : 'N/A') . '</td>';

							$replacement .= in_array('profile', $columns)
								? '<td style="text-align: center">' .
								$user['user']['profile'] .
								'</td>'
								: '';

							$replacement .= in_array('profile_cost', $columns)
								? '<td style="text-align: center">' .
								($byDays ? $user['user']['price_per_day'] : $user['user']['price_per_hour']) .
								($byDays ? ' €/j' : ' €/h') . ' </td>'
								: '';

							if (!$byYear) {


								$replacement .= in_array('target', $columns)
									? '<td style="text-align: right">' .
									$user['target'] . ($byDays ? ' j.' : ' h.') .
									'</td>'
									: '';
								$replacement .= in_array('target_cost', $columns)
									? '<td style="text-align: right">' .
									$user['target_cost'] . ' €' .
									'</td>'
									: '';

								$replacement .= in_array('done', $columns)
									? '<td style="text-align: right">' .
									$user['done'] . ($byDays ? ' j.' : ' h.') .
									'</td>'
									: '';

								$replacement .= in_array('done_cost', $columns)
									? '<td style="text-align: right">' .
									$user['done_cost'] . ' €' .
									'</td>'
									: '';

								$replacement .= in_array('left', $columns)
									? '<td style="text-align: right">' .
									$user['left'] . ($byDays ? ' j.' : ' h.') .
									'</td>'
									: '';
							} else {
								foreach ($years as $y) {
									if (!isset($user['years'][$y])) {
										$replacement .= '<td></td>';
										$replacement .= '<td></td>';
										$replacement .= '<td></td>';

										if (in_array('done_cost', $columns)) {
											$replacement .= '<td></td>';
										}
										if (in_array('target_cost', $columns)) {
											$replacement .= '<td></td>';
										}

										continue;
									}

									$replacement .= in_array('target', $columns)
										? '<td style="text-align: right">' .
										$user['years'][$y]['target'] . ($byDays ? ' j.' : ' h.') .
										'</td>'
										: '';
									$replacement .= in_array('target_cost', $columns)
										? '<td style="text-align: right">' .
										$user['years'][$y]['target_cost'] . ' €' .
										'</td>'
										: '';

									$replacement .= in_array('done', $columns)
										? '<td style="text-align: right">' .
										$user['years'][$y]['done'] . ($byDays ? ' j.' : ' h.') .
										'</td>'
										: '';

									$replacement .= in_array('done_cost', $columns)
										? '<td style="text-align: right">' .
										$user['years'][$y]['done_cost'] . ' €' .
										'</td>'
										: '';

									$replacement .= in_array('left', $columns)
										? '<td style="text-align: right">' .
										$user['years'][$y]['left'] . ($byDays ? ' j.' : ' h.') .
										'</td>'
										: '';
								}
							}

							$replacement .= '</tr>';
						}
						$replacement .= '</tbody>';
						$replacement .= '</table>';

						break;
					case 'userTimeRepartitionChart':
						$isArborescent = $params[0] === 'true';
						if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
							$isArborescent = false;
						}
						$type = $params[1];
						$showPercent = $params[2] === 'true';
						$showValue = $params[3] === 'true';

						$repartition = $object->{'getUserTimeRepartition' . ($isArborescent ? 'Arbo' : '')}(
							$year,
							$em
						);

						$chart = [];
						foreach ($repartition as $user) {
							if ($user['user']) {
								$chart[$user['user']['name']] = [$user[$type]];
							} else {
								$chart['N/A'] = [$user[$type]];
							}
						}

						$chartIndex = $variableStr . $index . uniqid();
						$globalVariable = [
							'name' => $chartIndex,
							'type' => 'chart',
							'data' => [
								'type' => 'pie',
								'data' => $chart,
								'chartAlign' => 'center',
								'showPercent' => $showPercent,
								'showValue' => $showValue,
							],
							'options' => [
								'type' => 'block',
							],
						];
						$replacement = '$' . $chartIndex . '$';
						break;

						/***********************
						 *       BUDGET        *
						 ***********************/					
					case 'expensesArray':
						$isArborescent = $params[0] === 'true';

						if (
							($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) ||
							$isKeyword
						) {
							$isArborescent = false;
						}

						$expensesTypes = is_array($params[1]) ? $params[1] : [];
						$expensesColumns = is_array($params[2]) ? $params[2] : [];
						$total = $params[3];
						$groupByPoste = $params[4] === 'true';
						$postesColumns = is_array($params[5]) ? $params[5] : [];
						$hidePostesAmount0 = $params[6] === 'true';
						$showOnlyTotal = $params[7] === 'true';
						$groupByYear = array_key_exists(8, $params) ? $params[8] === 'true' : false;
						$showOnlyPreviousAndCurrentYears = array_key_exists(9, $params) ? $params[9] === 'true' : false;
						$showOnlyPoste = array_key_exists(10, $params) ? $params[10] === 'true' : false;

						if ($groupByPoste) {
							if (in_array('poste', $expensesColumns)) {
								$expensesColumns = array_diff($expensesColumns, ['poste']);
							}

							if (in_array('name', $expensesColumns) && in_array('name', $postesColumns)) {
								$expensesColumns = array_diff($expensesColumns, ['name']);
							}
						}

						$postes = $object->{'getPosteExpenses' . ($isArborescent ? 'Arbo' : '')}($em);

						// added for years grouping
						$expensesYears = [];		// to loop on years when building the table
						// fill only when grouping years and with a column per year
						if ($groupByYear && !$showOnlyPreviousAndCurrentYears) {
							$expensesYears = Template::getTransactionsYears($postes, 'expense');
						}

						$totals = [
							'Total' => [
								'amount' => 0,
								'amountHT' => 0,
								'amountArbo' => 0,
								'amountHTArbo' => 0
							],
						];

						$projects = [];

						$replacement = '<table class="table">';
						$replacement .= '<thead>';
						$replacement .= '<tr>';

						if (
							$total !== 'false' && ($groupByPoste &&
								!in_array('account', $postesColumns) && ($groupByPoste && !in_array('name', $postesColumns)) &&
								!in_array('poste', $expensesColumns) &&
								!in_array('name', $expensesColumns) &&
								!in_array('project', $expensesColumns))
						) {
							$replacement .= '<th></th>';
						}

						if ($groupByPoste) {
							$replacement .= in_array('account', $postesColumns)
								? '<th>' . $translator->__invoke('poste_expense_field_account') . '</th>'
								: '';
							$replacement .= in_array('name', $postesColumns)
								? '<th>' . $translator->__invoke('poste_expense_field_name') . '</th>'
								: '';
						}

						$replacement .= in_array('poste', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_poste') . '</th>'
							: '';
						$replacement .= in_array('name', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_name') . '</th>'
							: '';

						$replacement .= in_array('project', $expensesColumns)
							? '<th>' . ucfirst($translator->__invoke('project_entity')) . '</th>'
							: '';

						if ($groupByPoste) {
							$replacement .= in_array('amount', $postesColumns)
								? '<th>' . $translator->__invoke('poste_expense_field_amount') . '</th>'
								: '';
							$replacement .= in_array('amountHT', $postesColumns)
								? '<th>' . $translator->__invoke('poste_expense_field_amountHT') . '</th>'
								: '';
							$replacement .= in_array('amountArbo', $postesColumns)
								? '<th>' . $translator->__invoke('poste_expense_field_amountArbo') . '</th>'
								: '';
							$replacement .= in_array('amountHTArbo', $postesColumns)
								? '<th>' . $translator->__invoke('poste_expense_field_amountHTArbo') . '</th>'
								: '';
						}

						foreach ($expensesTypes as $type) {
							if ($groupByYear) {
								if ($showOnlyPreviousAndCurrentYears) {		// add the previous and the current colmuns
									$replacement .= '<th>' . $translator->__invoke('expense_type_' . $type . '_previous') . '</th>';
									$replacement .= '<th>' . $translator->__invoke('expense_type_' . $type . '_current') . '</th>';
								} else {									// loop on available years to add the necessary columns
									foreach ($expensesYears as $y) {
										$replacement .= '<th>' . $translator->__invoke('expense_type_' . $type) . ' ' . $y . '</th>';
									}
								}
							} else {										// add only the type column
								$replacement .= '<th>' . $translator->__invoke('expense_type_' . $type) . '</th>';
							}
						}

						$replacement .= in_array('date', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_date') . '</th>'
							: '';
						$replacement .= in_array('tiers', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_tiers') . '</th>'
							: '';
						$replacement .= in_array('anneeExercice', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_anneeExercice') . '</th>'
							: '';
						$replacement .= in_array('bordereau', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_bordereau') . '</th>'
							: '';
						$replacement .= in_array('mandat', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_mandat') . '</th>'
							: '';
						$replacement .= in_array('engagement', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_engagement') . '</th>'
							: '';
						$replacement .= in_array('complement', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_complement') . '</th>'
							: '';
						$replacement .= in_array('dateFacture', $expensesColumns)
							? '<th>' . $translator->__invoke('expense_field_dateFacture') . '</th>'
							: '';

						$replacement .= '</tr>';
						$replacement .= '</thead>';
						$replacement .= '<tbody>';


						foreach ($postes as $poste) {
							if (
								!$hidePostesAmount0 ||
								$poste->{'getAmount' . ($isArborescent ? 'Arbo' : '')}() != 0
							) {
								$expenses = $poste->{'getExpenses' . ($isArborescent ? 'Arbo' : '')}();

								if ($groupByPoste) {
									if (!$showOnlyTotal) {
										$replacement .= '<tr class="group">';

										if (
											$total !== 'false' && ($groupByPoste &&
												!in_array('account', $postesColumns) && ($groupByPoste && !in_array('name', $postesColumns)) &&
												!in_array('poste', $expensesColumns) &&
												!in_array('name', $expensesColumns) &&
												!in_array('project', $expensesColumns))
										) {
											$replacement .= '<td></td>';
										}

										$replacement .= in_array('account', $postesColumns)
											? '<td> ' . ($poste->getAccount() ? $poste->getAccount()->getCode() : '') .
											'</td>'
											: '';
										$replacement .= in_array('name', $postesColumns)
											? '<td>' . $poste->getName() . '</td>'
											: '';

										$replacement .= in_array('project', $expensesColumns)
											? '<td>' . ($poste->getProject() ? $poste->getProject()->getName() : '') .
											'</td>'
											: '';

										$replacement .= in_array('amount', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmount(), 2, '.', ' ') .
											'</td>'
											: '';
										$replacement .= in_array('amountHT', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmountHT(), 2, '.', ' ') .
											'</td>'
											: '';
										$replacement .= in_array('amountArbo', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmountArbo(), 2, '.', ' ') .
											'</td>'
											: '';
										$replacement .= in_array('amountHTArbo', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmountHTArbo(), 2, '.', ' ') .
											'</td>'
											: '';

										foreach ($expensesTypes as $type) {
											if ($groupByYear) {
												if ($showOnlyPreviousAndCurrentYears) {
													$replacement .=
														'<td style="text-align: right;">' .
														number_format(
															$poste->{'getPreviousYearsAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
																ucfirst(str_replace('_ht', '', $type))}(),
															2,
															'.',
															' '
														) . '</td>';
													$replacement .=
														'<td style="text-align: right;">' .
														number_format(
															$poste->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
																ucfirst(str_replace('_ht', '', $type))}(date('Y')),
															2,
															'.',
															' '
														) . '</td>';
												} else {
													foreach ($expensesYears as $y) {
														$replacement .= '<td>' .
															number_format(
																$poste->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
																	ucfirst(str_replace('_ht', '', $type))}($y),
																2,
																'.',
																' '
															) .
															'</td>';
													}
												}
											} else {
												$replacement .=
													'<td style="text-align: right">' .
													number_format(
														$poste->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
															ucfirst(str_replace('_ht', '', $type))}($year),
														2,
														'.',
														' '
													) .
													'</td>';
											}
										}

										$replacement .= in_array('date', $expensesColumns) ? '<td></td>' : '';
										$replacement .= in_array('tiers', $expensesColumns) ? '<td></td>' : '';
										$replacement .= in_array('anneeExercice', $expensesColumns) ? '<td></td>' : '';
										$replacement .= in_array('bordereau', $expensesColumns) ? '<td></td>' : '';
										$replacement .= in_array('mandat', $expensesColumns) ? '<td></td>' : '';
										$replacement .= in_array('engagement', $expensesColumns) ? '<td></td>' : '';
										$replacement .= in_array('complement', $expensesColumns) ? '<td></td>' : '';
										$replacement .= in_array('dateFacture', $expensesColumns) ? '<td></td>' : '';

										$replacement .= '</tr>';
									}

									$projectId = $poste->getProject()->getId();

									if (!isset($projects[$projectId])) {
										$projects[$projectId] = $poste->getProject()->getName();
									}

									if (!isset($totals[$projectId])) {
										$totals[$projectId] = [
											'amount' => 0,
											'amountHT' => 0,
											'amountArbo' => 0,
											'amountHTArbo' => 0
										];
									}

									$totals[$projectId]['amount'] += $poste->getAmount();
									$totals[$projectId]['amountHT'] += $poste->getAmountHT();
									$totals[$projectId]['amountArbo'] += $poste->getAmountArbo();
									$totals[$projectId]['amountHTArbo'] += $poste->getAmountHTArbo();
									$totals['Total']['amount'] += $poste->getAmount();
									$totals['Total']['amountHT'] += $poste->getAmountHT();
									$totals['Total']['amountArbo'] += $poste->getAmountArbo();
									$totals['Total']['amountHTArbo'] += $poste->getAmountHTArbo();
								}

								foreach ($expenses as $expense) {
									if (in_array($expense->getType(), $expensesTypes) && $expense->isYear($year)) {
										if (!$showOnlyTotal && !$showOnlyPoste) {
											$replacement .= '<tr>';

											if (
												$total !== 'false' && ($groupByPoste &&
													!in_array('account', $postesColumns) && ($groupByPoste && !in_array('name', $postesColumns)) &&
													!in_array('poste', $expensesColumns) &&
													!in_array('name', $expensesColumns) &&
													!in_array('project', $expensesColumns))
											) {
												$replacement .= '<td></td>';
											}

											if ($groupByPoste) {
												$replacement .= in_array('account', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('name', $postesColumns)
													? '<td>' . $expense->getName() . '</td>'
													: '';
											}

											$replacement .= in_array('poste', $expensesColumns)
												? '<td>' . $expense->getPoste()->getName() . '</td>'
												: '';
											$replacement .= in_array('name', $expensesColumns)
												? '<td>' . $expense->getName() . '</td>'
												: '';

											$replacement .= in_array('project', $expensesColumns)
												? '<td>' .
												$expense
												->getPoste()
												->getProject()
												->getName() .
												'</td>'
												: '';

											if ($groupByPoste) {
												$replacement .= in_array('amount', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('amountHT', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('amountArbo', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('amountHTArbo', $postesColumns) ? '<td></td>' : '';
											}

											foreach ($expensesTypes as $type) {
												if ($groupByYear) {
													if ($showOnlyPreviousAndCurrentYears) {
														$replacement .=
															'<td style="text-align: right">' . ($expense->getType() == str_replace('_ht', '', $type) && $expense->belongsToPreviousYears()
																? number_format(
																	$expense->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')}(),
																	2,
																	'.',
																	' '
																)
																: '') .
															'</td>';
														$replacement .=
															'<td style="text-align: right">' . ($expense->getType() == str_replace('_ht', '', $type) && $expense->isYear(date('Y'))
																? number_format(
																	$expense->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')}(),
																	2,
																	'.',
																	' '
																)
																: '') .
															'</td>';
													} else {
														foreach ($expensesYears as $y) {
															$replacement .= '<td>' . ($expense->getType() == str_replace('_ht', '', $type) && $expense->isYear($y)
																? number_format(
																	$expense->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')}(),
																	2,
																	'.',
																	' '
																)
																: '') .
																'</td>';
														}
													}
												} else {
													$replacement .=
														'<td style="text-align: right">' . ($expense->getType() == str_replace('_ht', '', $type)
															? number_format(
																$expense->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')}(),
																2,
																'.',
																' '
															)
															: '') .
														'</td>';
												}
											}

											$replacement .= in_array('date', $expensesColumns)
												? '<td>' . ($expense->getDate() ? $expense->getDate()->format('d/m/Y') : '') .
												'</td>'
												: '';
											$replacement .= in_array('tiers', $expensesColumns)
												? '<td>' . $expense->getTiers() . '</td>'
												: '';
											$replacement .= in_array('anneeExercice', $expensesColumns)
												? '<td>' . $expense->getAnneeExercice() . '</td>'
												: '';
											$replacement .= in_array('bordereau', $expensesColumns)
												? '<td>' . $expense->getBordereau() . '</td>'
												: '';
											$replacement .= in_array('mandat', $expensesColumns)
												? '<td>' . $expense->getMandat() . '</td>'
												: '';
											$replacement .= in_array('engagement', $expensesColumns)
												? '<td>' . $expense->getEngagement() . '</td>'
												: '';
											$replacement .= in_array('complement', $expensesColumns)
												? '<td>' . $expense->getComplement() . '</td>'
												: '';
											$replacement .= in_array('dateFacture', $expensesColumns)
												? '<td>' . ($expense->getDateFacture()
													? $expense->getDateFacture()->format('d/m/Y')
													: '') .
												'</td>'
												: '';

											$replacement .= '</tr>';
										}

										$projectId = $expense
											->getPoste()
											->getProject()
											->getId();

										if (!isset($projects[$projectId])) {
											$projects[$projectId] = $expense
												->getPoste()
												->getProject()
												->getName();
										}

										if (!isset($totals[$projectId])) {
											$totals[$projectId] = [];
										}

										if (!isset($totals[$projectId][$expense->getType()])) {
											$totals[$projectId][$expense->getType()]['all'] = 0;
											$totals[$projectId][$expense->getType() . '_ht']['all'] = 0;
											if (!isset($totals[$projectId][$expense->getType()]['years'])) {				// added for year grouping
												$totals[$projectId][$expense->getType()]['years'] = [];
											}
											if (!isset($totals[$projectId][$expense->getType() . '_ht']['years'])) {		// added for year grouping
												$totals[$projectId][$expense->getType() . '_ht']['years'] = [];
											}
										}

										if (!isset($totals['Total'][$expense->getType()])) {
											$totals['Total'][$expense->getType()]['all'] = 0;
											$totals['Total'][$expense->getType() . '_ht']['all'] = 0;
											if (!isset($totals['Total'][$expense->getType()]['years'])) {				// added for year grouping
												$totals['Total'][$expense->getType()]['years'] = [];
											}
											if (!isset($totals['Total'][$expense->getType() . '_ht']['years'])) {		// added for year grouping
												$totals['Total'][$expense->getType() . '_ht']['years'] = [];
											}
										}

										$totals[$projectId][$expense->getType()]['all'] += $expense->getAmount();
										// added for year grouping
										if (!isset($totals[$projectId][$expense->getType()]['years'][$expense->getYear()])) {
											$totals[$projectId][$expense->getType()]['years'][$expense->getYear()] = 0;
										}
										$totals[$projectId][$expense->getType()]['years'][$expense->getYear()] += $expense->getAmount();

										$totals[$projectId][$expense->getType() . '_ht']['all'] += $expense->getAmountHT();
										// added for year grouping
										if (!isset($totals[$projectId][$expense->getType() . '_ht']['years'][$expense->getYear()])) {
											$totals[$projectId][$expense->getType() . '_ht']['years'][$expense->getYear()] = 0;
										}
										$totals[$projectId][$expense->getType() . '_ht']['years'][$expense->getYear()] += $expense->getAmountHT();

										$totals['Total'][$expense->getType()]['all'] += $expense->getAmount();
										// added for year grouping
										if (!isset($totals['Total'][$expense->getType()]['years'][$expense->getYear()])) {
											$totals['Total'][$expense->getType()]['years'][$expense->getYear()] = 0;
										}
										$totals['Total'][$expense->getType()]['years'][$expense->getYear()] += $expense->getAmount();

										$totals['Total'][$expense->getType() . '_ht']['all'] += $expense->getAmountHT();
										// added for year grouping
										if (!isset($totals['Total'][$expense->getType() . '_ht']['years'][$expense->getYear()])) {
											$totals['Total'][$expense->getType() . '_ht']['years'][$expense->getYear()] = 0;
										}
										$totals['Total'][$expense->getType() . '_ht']['years'][$expense->getYear()] += $expense->getAmountHT();
									}
								}
							}
						}

						if ($total !== 'false') {
							if ($total !== 'byProject' || !$isArborescent) {
								$totals = ['Total' => $totals['Total']];
							}

							foreach ($totals as $totalKey => $totalValues) {
								if ($totalKey !== 'Total') {
									$totalTitle = $projects[$totalKey];
								} else {
									$totalTitle = 'Total (' . $object->getName() . ')';
								}

								$replacement .= '<tr class="group">';

								$totalTitlePlaced = false;

								if (
									$total !== 'false' && ($groupByPoste &&
										!in_array('account', $postesColumns) && ($groupByPoste && !in_array('name', $postesColumns)) &&
										!in_array('poste', $expensesColumns) &&
										!in_array('name', $expensesColumns) &&
										!in_array('project', $expensesColumns))
								) {
									$replacement .= '<td><b>' . $totalTitle . '</b></td>';
									$totalTitlePlaced = true;
								}

								if ($groupByPoste) {
									if (in_array('account', $postesColumns)) {
										$replacement .=
											'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
										$totalTitlePlaced = true;
									} else {
										$replacement .= '';
									}

									if (in_array('name', $postesColumns)) {
										$replacement .=
											'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
										$totalTitlePlaced = true;
									} else {
										$replacement .= '';
									}
								}

								if (in_array('poste', $expensesColumns)) {
									$replacement .=
										'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
									$totalTitlePlaced = true;
								} else {
									$replacement .= '';
								}

								if (in_array('name', $expensesColumns)) {
									$replacement .=
										'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
									$totalTitlePlaced = true;
								} else {
									$replacement .= '';
								}

								if (in_array('project', $expensesColumns)) {
									$replacement .=
										'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
									$totalTitlePlaced = true;
								} else {
									$replacement .= '';
								}

								if ($groupByPoste) {
									$replacement .= in_array('amount', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amount'], 2, '.', ' ') .
										'</td>'
										: '';
									$replacement .= in_array('amountHT', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amountHT'], 2, '.', ' ') .
										'</td>'
										: ''; 
									$replacement .= in_array('amountArbo', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amountArbo'], 2, '.', ' ') .
										'</td>'
										: '';
									$replacement .= in_array('amountHTArbo', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amountHTArbo'], 2, '.', ' ') .
										'</td>'
										: '';
								}

								foreach ($expensesTypes as $type) {
									if ($groupByYear) {
										if ($showOnlyPreviousAndCurrentYears) {
											$replacement .=
												'<td style="text-align: right;">' .
												number_format(Template::getTotalAmountForPreviousYears($totalValues[$type]['years']), 2, '.', ' ') .
												'</td>';
											$replacement .=
												'<td style="text-align: right;">' .
												number_format(Template::getTotalAmountByYear(date('Y'), $totalValues[$type]['years']), 2, '.', ' ') .
												'</td>';
										} else {
											foreach ($expensesYears as $y) {
												$replacement .=
													'<td>' .
													number_format(Template::getTotalAmountByYear($y, $totalValues[$type]['years']), 2, '.', ' ') .
													'</td>';
											}
										}
									} else {
										$replacement .=
											'<td style="text-align: right">' .
											number_format($totalValues[$type]['all'], 2, '.', ' ') .
											'</td>';
									}
								}

								$replacement .= in_array('date', $expensesColumns) ? '<td></td>' : '';
								$replacement .= in_array('tiers', $expensesColumns) ? '<td></td>' : '';
								$replacement .= in_array('anneeExercice', $expensesColumns) ? '<td></td>' : '';
								$replacement .= in_array('bordereau', $expensesColumns) ? '<td></td>' : '';
								$replacement .= in_array('mandat', $expensesColumns) ? '<td></td>' : '';
								$replacement .= in_array('engagement', $expensesColumns) ? '<td></td>' : '';
								$replacement .= in_array('complement', $expensesColumns) ? '<td></td>' : '';
								$replacement .= in_array('dateFacture', $expensesColumns) ? '<td></td>' : '';

								$replacement .= '</tr>';
							}
						}

						$replacement .= '</tbody>';
						$replacement .= '</table>';

						break;
					case 'incomesArray':

						$isArborescent = $params[0] === 'true';

						if (
							($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) ||
							$isKeyword
						) {
							$isArborescent = false;
						}

						$incomesTypes = is_array($params[1]) ? $params[1] : [];
						$incomesColumns = is_array($params[2]) ? $params[2] : [];
						$total = $params[3];
						$groupByPoste = $params[4] === 'true';
						$postesColumns = is_array($params[5]) ? $params[5] : [];
						$hidePostesAmount0 = $params[6] === 'true';
						$showOnlyTotal = $params[7] === 'true';
						$showTotalEnvelopes = $params[8] === 'true';
						$groupByYear = array_key_exists(9, $params) ? $params[9] === 'true' : false;
						$showOnlyPreviousAndCurrentYears = array_key_exists(10, $params) ? $params[10] === 'true' : false;
						$showOnlyPoste = array_key_exists(11, $params) ? $params[11] === 'true' : false;

						if ($groupByPoste) {
							if (in_array('poste', $incomesColumns)) {
								$incomesColumns = array_diff($incomesColumns, ['poste']);
							}

							if (in_array('name', $incomesColumns) && in_array('name', $postesColumns)) {
								$incomesColumns = array_diff($incomesColumns, ['name']);
							}

							if (in_array('financer', $incomesColumns) && in_array('financer', $postesColumns)) {
								$incomesColumns = array_diff($incomesColumns, ['financer']);
							}
						}

						/** @var PosteIncome[] $postes */
						$postes = $object->{'getPosteIncomes' . ($isArborescent ? 'Arbo' : '')}($em);

						// added for years grouping
						$incomesYears = [];		// to loop on years when building the table
						// fill only when grouping years and with a column per year
						if ($groupByYear && !$showOnlyPreviousAndCurrentYears) {
							$incomesYears = Template::getTransactionsYears($postes, 'income');
						}

						$totals = [
							'Total' => [
								'amount' => 0,
								'amountArbo' => 0,
							],
						];

						$projects = [];

						$replacement = '<table class="table">';

						$replacement .= '<thead>';
						$replacement .= '<tr>';

						if (
							$total !== 'false' && ($groupByPoste &&
								!in_array('account', $postesColumns) && ($groupByPoste && !in_array('envelope', $postesColumns)) && ($groupByPoste && !in_array('financer', $postesColumns)) && ($groupByPoste && !in_array('name', $postesColumns)) &&
								!in_array('financer', $incomesColumns) &&
								!in_array('poste', $incomesColumns) &&
								!in_array('name', $incomesColumns) &&
								!in_array('project', $incomesColumns))
						) {
							$replacement .= '<th></th>';
						}

						if ($groupByPoste) {
							$replacement .= in_array('account', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_account') . '</th>'
								: '';
							$replacement .= in_array('envelope', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_envelope') . '</th>'
								: '';
							$replacement .= in_array('financer', $postesColumns)
								? '<th>' . $translator->__invoke('envelope_field_financer') . '</th>'
								: '';
							$replacement .= in_array('name', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_name') . '</th>'
								: '';
						}

						$replacement .= in_array('financer', $incomesColumns)
							? '<th>' . $translator->__invoke('envelope_field_financer') . '</th>'
							: '';
						$replacement .= in_array('poste', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_poste') . '</th>'
							: '';
						$replacement .= in_array('name', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_name') . '</th>'
							: '';

						$replacement .= in_array('project', $incomesColumns)
							? '<th>' . ucfirst($translator->__invoke('project_entity')) . '</th>'
							: '';

						if ($groupByPoste) {
							$replacement .= in_array('percentage', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_percentage') . '</th>'
								: '';
							$replacement .= in_array('amount', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_amount') . '</th>'
								: '';
							$replacement .= in_array('amountHT', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_amountHT') . '</th>'
								: '';
							$replacement .= in_array('amountArbo', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_amountArbo') . '</th>'
								: '';
							$replacement .= in_array('amountHTArbo', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_amountHTArbo') . '</th>'
								: '';
						}

						foreach ($incomesTypes as $type) {
							if ($groupByYear) {
								if ($showOnlyPreviousAndCurrentYears) {		// add the previous and the current colmuns
									$replacement .= '<th>' . $translator->__invoke('income_type_' . $type . '_previous') . '</th>';
									$replacement .= '<th>' . $translator->__invoke('income_type_' . $type . '_current') . '</th>';
								} else {									// loop on available years to add the necessary columns
									foreach ($incomesYears as $y) {
										$replacement .= '<th>' . $translator->__invoke('income_type_' . $type) . ' ' . $y . '</th>';
									}
								}
							} else {
								$replacement .= '<th>' . $translator->__invoke('income_type_' . $type) . '</th>';
							}
						}

						$replacement .= in_array('date', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_date') . '</th>'
							: '';
						$replacement .= in_array('tiers', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_tiers') . '</th>'
							: '';
						$replacement .= in_array('anneeExercice', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_anneeExercice') . '</th>'
							: '';
						$replacement .= in_array('bordereau', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_bordereau') . '</th>'
							: '';
						$replacement .= in_array('mandat', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_mandat') . '</th>'
							: '';
						$replacement .= in_array('engagement', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_engagement') . '</th>'
							: '';
						$replacement .= in_array('complement', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_complement') . '</th>'
							: '';
						$replacement .= in_array('serie', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_serie') . '</th>'
							: '';
						$replacement .= in_array('evolution', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_evolution') . '</th>'
							: '';
						$replacement .= in_array('transfer.name', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_transfer_name') . '</th>'
							: '';
						$replacement .= in_array('transfer.code', $incomesColumns)
							? '<th>' . $translator->__invoke('income_field_transfer_code') . '</th>'
							: '';

						if ($groupByPoste) {
							$replacement .= in_array('autofinancement', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_autofinancement') . '</th>'
								: '';
							$replacement .= in_array('caduciteStart', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_caduciteStart') . '</th>'
								: '';
							$replacement .= in_array('caduciteEnd', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_caduciteEnd') . '</th>'
								: '';
							$replacement .= in_array('caduciteSendProof', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_caduciteSendProof') . '</th>'
								: '';
							$replacement .= in_array('folderSendingDate', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_folderSendingDate') . '</th>'
								: '';
							$replacement .= in_array('folderReceiptDate', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_folderReceiptDate') . '</th>'
								: '';
							$replacement .= in_array('arrDate', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_arrDate') . '</th>'
								: '';
							$replacement .= in_array('arrNumber', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_arrNumber') . '</th>'
								: '';
							$replacement .= in_array('deliberationDate', $postesColumns)
								? '<th>' . $translator->__invoke('poste_income_field_deliberationDate') . '</th>'
								: '';
						}

						$replacement .= '</tr>';
						$replacement .= '</thead>';

						if (!$showOnlyTotal) {
							$replacement .= '<tbody>';
						}

						$amount = 1;
						if (in_array('percentage', $postesColumns)) {
							$amount = $object->{'getAmountPosteIncome' . ($isArborescent ? 'Arbo' : '')}($em);
						}


						foreach ($postes as $poste) {
							if (
								!$hidePostesAmount0 ||
								$poste->{'getAmount' . ($isArborescent ? 'Arbo' : '')}() != 0
							) {
								$percentage = 0;
								if (in_array('percentage', $postesColumns) && $amount > 0) {
									$percentage = ($poste->{'getAmount' . ($isArborescent ? 'Arbo' : '')}() * 100) / $amount;
								}

								$incomes = $poste->{'getIncomes' . ($isArborescent ? 'Arbo' : '')}();

								if ($groupByPoste) {
									if (!$showOnlyTotal) {
										$replacement .= '<tr class="group">';

										if (
											$total !== 'false' && ($groupByPoste &&
												!in_array('account', $postesColumns) && ($groupByPoste && !in_array('envelope', $postesColumns)) && ($groupByPoste && !in_array('financer', $postesColumns)) && ($groupByPoste && !in_array('name', $postesColumns)) &&
												!in_array('financer', $incomesColumns) &&
												!in_array('poste', $incomesColumns) &&
												!in_array('name', $incomesColumns) &&
												!in_array('project', $incomesColumns))
										) {
											$replacement .= '<td></td>';
										}

										$replacement .= in_array('account', $postesColumns)
											? '<td>' . ($poste->getAccount() ? $poste->getAccount()->getCode() : '') .
											'</td>'
											: '';
										$replacement .= in_array('envelope', $postesColumns)
											? '<td>' . ($poste->getEnvelope() ? $poste->getEnvelope()->getName() : '') .
											'</td>'
											: '';
										$replacement .= in_array('financer', $postesColumns)
											? '<td>' . ($poste->getEnvelope()
												? $poste
												->getEnvelope()
												->getFinancer()
												->getName()
												: '') .
											'</td>'
											: '';
										$replacement .= in_array('name', $postesColumns)
											? '<td>' . $poste->getName() . '</td>'
											: '';

										$replacement .= in_array('project', $incomesColumns)
											? '<td>' . $poste->getProject()->getName() . '</td>'
											: '';

										$replacement .= in_array('percentage', $postesColumns)
											? '<td>' . number_format($percentage, 2, '.', ' ') . '</td>'
											: '';
										$replacement .= in_array('amount', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmount(), 2, '.', ' ') .
											'</td>'
											: '';
										$replacement .= in_array('amountHT', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmountHT(), 2, '.', ' ') .
											'</td>'
											: '';
										$replacement .= in_array('amountArbo', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmountArbo(), 2, '.', ' ') .
											'</td>'
											: '';
										$replacement .= in_array('amountHTArbo', $postesColumns)
											? '<td style="text-align: right">' .
											number_format($poste->getAmountHTArbo(), 2, '.', ' ') .
											'</td>'
											: '';

										foreach ($incomesTypes as $type) {
											if ($groupByYear) {
												if ($showOnlyPreviousAndCurrentYears) {
													$replacement .=
														'<td style="text-align: right;">' .
														number_format(
															$poste->{'getPreviousYearsAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
																ucfirst(str_replace('_ht', '', $type))}(),
															2,
															'.',
															' '
														) . '</td>';
													$replacement .=
														'<td style="text-align: right;">' .
														number_format(
															$poste->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
																ucfirst(str_replace('_ht', '', $type))}(date('Y')),
															2,
															'.',
															' '
														) . '</td>';
												} else {
													foreach ($incomesYears as $y) {
														$replacement .= '<td>' .
															number_format(
																$poste->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
																	ucfirst(str_replace('_ht', '', $type))}($y),
																2,
																'.',
																' '
															) .
															'</td>';
													}
												}
											} else {
												$replacement .=
													'<td style="text-align: right">' .
													number_format(
														$poste->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '') . ($isArborescent ? 'Arbo' : '') .
															ucfirst(str_replace('_ht', '', $type))}($year),
														2,
														'.',
														' '
													) .
													'</td>';
											}
										}

										$replacement .= in_array('date', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('tiers', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('anneeExercice', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('bordereau', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('mandat', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('engagement', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('complement', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('serie', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('evolution', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('transfer.name', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('transfer.code', $incomesColumns) ? '<td></td>' : '';

										$replacement .= in_array('autofinancement', $postesColumns)
											? '<td>' . ($poste->getAutofinancement() ? 'Oui' : 'Non') . '</td>'
											: '';
										$replacement .= in_array('caduciteStart', $postesColumns)
											? '<td>' . ($poste->getCaduciteStart()
												? $poste->getCaduciteStart()->format('d/m/Y')
												: '') .
											'</td>'
											: '';
										$replacement .= in_array('caduciteEnd', $postesColumns)
											? '<td>' . ($poste->getCaduciteEnd()
												? $poste->getCaduciteEnd()->format('d/m/Y')
												: '') .
											'</td>'
											: '';
										$replacement .= in_array('caduciteSendProof', $postesColumns)
											? '<td>' . ($poste->getCaduciteSendProof()
												? $poste->getCaduciteSendProof()->format('d/m/Y')
												: '') .
											'</td>'
											: '';
										$replacement .= in_array('folderSendingDate', $postesColumns)
											? '<td>' . ($poste->getFolderSendingDate()
												? $poste->getFolderSendingDate()->format('d/m/Y')
												: '') .
											'</td>'
											: '';
										$replacement .= in_array('folderReceiptDate', $postesColumns)
											? '<td>' . ($poste->getFolderReceiptDate()
												? $poste->getFolderReceiptDate()->format('d/m/Y')
												: '') .
											'</td>'
											: '';
										$replacement .= in_array('arrDate', $postesColumns)
											? '<td>' . ($poste->getArrDate() ? $poste->getArrDate()->format('d/m/Y') : '') .
											'</td>'
											: '';
										$replacement .= in_array('arrNumber', $postesColumns)
											? '<td>' . $poste->getArrNumber() . '</td>'
											: '';
										$replacement .= in_array('deliberationDate', $postesColumns)
											? '<td>' . ($poste->getDeliberationDate()
												? $poste->getDeliberationDate()->format('d/m/Y')
												: '') .
											'</td>'
											: '';

										$replacement .= '</tr>';
									}

									$projectId = $poste->getProject()->getId();

									if (!isset($projects[$projectId])) {
										$projects[$projectId] = $poste->getProject()->getName();
									}

									if (!isset($totals[$projectId])) {
										$totals[$projectId] = [
											'amount' => 0,
											'amountHT' => 0,
											'amountArbo' => 0,
											'amountHTArbo' => 0,
										];
									}

									$totals[$projectId]['amount'] += $poste->getAmount();
									$totals[$projectId]['amountHT'] += $poste->getAmountHT();
									$totals[$projectId]['amountArbo'] += $poste->getAmountArbo();
									$totals[$projectId]['amountHTArbo'] += $poste->getAmountHTArbo();
									$totals['Total']['amount'] += $poste->getAmount();
									$totals['Total']['amountHT'] += $poste->getAmountHT();
									$totals['Total']['amountArbo'] += $poste->getAmountArbo();
									$totals['Total']['amountHTArbo'] += $poste->getAmountHTArbo();
								}

								foreach ($incomes as $income) {
									if (in_array($income->getType(), $incomesTypes) && $income->isYear($year)) {
										if (!$showOnlyTotal && !$showOnlyPoste) {
											$replacement .= '<tr>';

											if (
												$total !== 'false' && ($groupByPoste &&
													!in_array('account', $postesColumns) && ($groupByPoste && !in_array('envelope', $postesColumns)) && ($groupByPoste && !in_array('financer', $postesColumns)) && ($groupByPoste && !in_array('name', $postesColumns)) &&
													!in_array('financer', $incomesColumns) &&
													!in_array('poste', $incomesColumns) &&
													!in_array('name', $incomesColumns) &&
													!in_array('project', $incomesColumns))
											) {
												$replacement .= '<td></td>';
											}

											if ($groupByPoste) {
												$replacement .= in_array('account', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('envelope', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('financer', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('name', $postesColumns)
													? '<td>' . $income->getName() . '</td>'
													: '';
											}

											$replacement .= in_array('financer', $incomesColumns)
												? '<td>' . ($income->getPoste()->getEnvelope()
													? $income
													->getPoste()
													->getEnvelope()
													->getFinancer()
													->getName()
													: '') .
												'</td>'
												: '';
											$replacement .= in_array('poste', $incomesColumns)
												? '<td>' . $income->getPoste()->getName() . '</td>'
												: '';
											$replacement .= in_array('name', $incomesColumns)
												? '<td>' . $income->getName() . '</td>'
												: '';

											$replacement .= in_array('project', $incomesColumns)
												? '<td>' .
												$income
												->getPoste()
												->getProject()
												->getName() .
												'</td>'
												: '';

											if ($groupByPoste) {
												$replacement .= in_array('percentage', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('amount', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('amountHT', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('amountArbo', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('amountHTArbo', $postesColumns) ? '<td></td>' : '';
											}

											foreach ($incomesTypes as $type) {
												if ($groupByYear) {
													if ($showOnlyPreviousAndCurrentYears) {
														$replacement .=
															'<td style="text-align: right">' . ($income->getType() == str_replace('_ht', '', $type) && $income->belongsToPreviousYears()
																? number_format(
																	$income->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')}(),
																	2,
																	'.',
																	' '
																)
																: '') .
															'</td>';
														$replacement .=
															'<td style="text-align: right">' . ($income->getType() == str_replace('_ht', '', $type) && $income->isYear(date('Y'))
																? number_format(
																	$income->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')}(),
																	2,
																	'.',
																	' '
																)
																: '') .
															'</td>';
													} else {
														foreach ($incomesYears as $y) {
															$replacement .= '<td>' . ($income->getType() == str_replace('_ht', '', $type) && $income->isYear($y)
																? number_format(
																	$income->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')}(),
																	2,
																	'.',
																	' '
																)
																: '') .
																'</td>';
														}
													}
												} else {
													$replacement .=
														'<td style="text-align: right">' . ($income->getType() == str_replace('_ht', '', $type)
															? number_format(
																$income->{'getAmount' . (strpos($type, '_ht') ? 'HT' : '')},
																2,
																'.',
																' '
															)
															: '') .
														'</td>';
												}
											}

											$replacement .= in_array('date', $incomesColumns)
												? '<td>' . ($income->getDate() ? $income->getDate()->format('d/m/Y') : '') .
												'</td>'
												: '';
											$replacement .= in_array('tiers', $incomesColumns)
												? '<td>' . $income->getTiers() . '</td>'
												: '';
											$replacement .= in_array('anneeExercice', $incomesColumns)
												? '<td>' . $income->getAnneeExercice() . '</td>'
												: '';
											$replacement .= in_array('bordereau', $incomesColumns)
												? '<td>' . $income->getBordereau() . '</td>'
												: '';
											$replacement .= in_array('mandat', $incomesColumns)
												? '<td>' . $income->getMandat() . '</td>'
												: '';
											$replacement .= in_array('engagement', $incomesColumns)
												? '<td>' . $income->getEngagement() . '</td>'
												: '';
											$replacement .= in_array('complement', $incomesColumns)
												? '<td>' . $income->getComplement() . '</td>'
												: '';
											$replacement .= in_array('serie', $incomesColumns)
												? '<td>' . $income->getSerie() . '</td>'
												: '';
											$replacement .= in_array('evolution', $incomesColumns)
												? '<td>' . ($income->getEvolution() ? 'Oui' : 'Non') . '</td>'
												: '';
											$replacement .= in_array('transfer.name', $incomesColumns)
												? '<td>' . (null !== $income->getTransfer() ? $income->getTransfer()->getName() : '') . '</td>'
												: '';
											$replacement .= in_array('transfer.code', $incomesColumns)
												? '<td>' . (null !== $income->getTransfer() ? $income->getTransfer()->getCode() : '') . '</td>'
												: '';

											if ($groupByPoste) {
												$replacement .= in_array('autofinancement', $postesColumns)
													? '<td></td>'
													: '';
												$replacement .= in_array('caduciteStart', $postesColumns)
													? '<td></td>'
													: '';
												$replacement .= in_array('caduciteEnd', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('caduciteSendProof', $postesColumns)
													? '<td></td>'
													: '';
												$replacement .= in_array('folderSendingDate', $postesColumns)
													? '<td></td>'
													: '';
												$replacement .= in_array('folderReceiptDate', $postesColumns)
													? '<td></td>'
													: '';
												$replacement .= in_array('arrDate', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('arrNumber', $postesColumns) ? '<td></td>' : '';
												$replacement .= in_array('deliberationDate', $postesColumns)
													? '<td></td>'
													: '';
											}

											$replacement .= '</tr>';
										}

										$projectId = $income
											->getPoste()
											->getProject()
											->getId();

										if (!isset($projects[$projectId])) {
											$projects[$projectId] = $income
												->getPoste()
												->getProject()
												->getName();
										}

										if (!isset($totals[$projectId])) {
											$totals[$projectId] = [];
										}

										if (!isset($totals[$projectId][$income->getType()])) {
											$totals[$projectId][$income->getType()]['all'] = 0;
											$totals[$projectId][$income->getType() . '_ht']['all'] = 0;
											if (!isset($totals[$projectId][$income->getType()]['years'])) {				// added for year grouping
												$totals[$projectId][$income->getType()]['years'] = [];
											}
											if (!isset($totals[$projectId][$income->getType() . '_ht']['years'])) {		// added for year grouping
												$totals[$projectId][$income->getType() . '_ht']['years'] = [];
											}
										}

										if (!isset($totals['Total'][$income->getType()])) {
											$totals['Total'][$income->getType()]['all'] = 0;
											$totals['Total'][$income->getType() . '_ht']['all'] = 0;
											if (!isset($totals['Total'][$income->getType()]['years'])) {				// added for year grouping
												$totals['Total'][$income->getType()]['years'] = [];
											}
											if (!isset($totals['Total'][$income->getType() . '_ht']['years'])) {		// added for year grouping
												$totals['Total'][$income->getType() . '_ht']['years'] = [];
											}
										}

										$totals[$projectId][$income->getType()]['all'] += $income->getAmount();
										// added for year grouping
										if (!isset($totals[$projectId][$income->getType()]['years'][$income->getYear()])) {
											$totals[$projectId][$income->getType()]['years'][$income->getYear()] = 0;
										}
										$totals[$projectId][$income->getType()]['years'][$income->getYear()] += $income->getAmount();

										$totals[$projectId][$income->getType() . '_ht']['all'] += $income->getAmountHT();
										// added for year grouping
										if (!isset($totals[$projectId][$income->getType() . '_ht']['years'][$income->getYear()])) {
											$totals[$projectId][$income->getType() . '_ht']['years'][$income->getYear()] = 0;
										}
										$totals[$projectId][$income->getType() . '_ht']['years'][$income->getYear()] += $income->getAmountHT();

										$totals['Total'][$income->getType()]['all'] += $income->getAmount();
										// added for year grouping
										if (!isset($totals['Total'][$income->getType()]['years'][$income->getYear()])) {
											$totals['Total'][$income->getType()]['years'][$income->getYear()] = 0;
										}
										$totals['Total'][$income->getType()]['years'][$income->getYear()] += $income->getAmount();

										$totals['Total'][$income->getType() . '_ht']['all'] += $income->getAmountHT();
										// added for year grouping
										if (!isset($totals['Total'][$income->getType() . '_ht']['years'][$income->getYear()])) {
											$totals['Total'][$income->getType() . '_ht']['years'][$income->getYear()] = 0;
										}
										$totals['Total'][$income->getType() . '_ht']['years'][$income->getYear()] += $income->getAmountHT();
									}
								}
							}

							if ($showTotalEnvelopes) {
								$envelope = $poste->getEnvelope();

								if ($envelope) {
									$project_id = $poste->getProject()->getId();

									if (!isset($totals[$project_id]['envelopes'])) {
										$totals[$project_id]['envelopes'] = [];
									}

									if (!isset($totals['Total']['envelopes'])) {
										$totals['Total']['envelopes'] = [];
									}

									if (!isset($totals[$project_id]['envelopes'][$envelope->getId()])) {
										$totals[$project_id]['envelopes'][$envelope->getId()] = [
											'name' => $envelope->getName(),
											'amount' => 0,
											'amountArbo' => 0,
										];
									}

									if (!isset($totals['Total']['envelopes'][$envelope->getId()])) {
										$totals['Total']['envelopes'][$envelope->getId()] = [
											'name' => $envelope->getName(),
											'amount' => 0,
											'amountArbo' => 0,
										];
									}

									$totals[$project_id]['envelopes'][$envelope->getId()]['amount'] += $poste->getAmount();
									$totals[$project_id]['envelopes'][$envelope->getId()]['amountArbo'] += $poste->getAmountArbo();

									$totals['Total']['envelopes'][$envelope->getId()]['amount'] += $poste->getAmount();
									$totals['Total']['envelopes'][$envelope->getId()]['amountArbo'] += $poste->getAmountArbo();
								}
							}
						}

						if ($total !== 'false') {
							if ($total !== 'byProject' || !$isArborescent) {
								$totals = ['Total' => $totals['Total']];
							}

							foreach ($totals as $totalKey => $totalValues) {
								if ($totalKey !== 'Total') {
									$totalTitle = $projects[$totalKey];
								} else {
									$totalTitle = 'Total (' . $object->getName() . ')';
								}

								$replacement .= '<tr class="group">';

								$totalTitlePlaced = false;

								if (
									$total !== 'false' && ($groupByPoste &&
										!in_array('account', $postesColumns) && ($groupByPoste && !in_array('envelope', $postesColumns)) && ($groupByPoste && !in_array('financer', $postesColumns)) && ($groupByPoste && !in_array('name', $postesColumns)) &&
										!in_array('financer', $incomesColumns) &&
										!in_array('poste', $incomesColumns) &&
										!in_array('name', $incomesColumns) &&
										!in_array('project', $incomesColumns))
								) {
									$replacement .= '<td><b>' . $totalTitle . '</b></td>';
									$totalTitlePlaced = true;
								}

								if ($groupByPoste) {
									if (in_array('account', $postesColumns)) {
										$replacement .=
											'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
										$totalTitlePlaced = true;
									} else {
										$replacement .= '';
									}

									if (in_array('envelope', $postesColumns)) {
										$replacement .=
											'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
										$totalTitlePlaced = true;
									} else {
										$replacement .= '';
									}

									if (in_array('financer', $postesColumns)) {
										$replacement .=
											'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
										$totalTitlePlaced = true;
									} else {
										$replacement .= '';
									}

									if (in_array('name', $postesColumns)) {
										$replacement .=
											'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
										$totalTitlePlaced = true;
									} else {
										$replacement .= '';
									}
								}

								if (in_array('financer', $incomesColumns)) {
									$replacement .=
										'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
									$totalTitlePlaced = true;
								} else {
									$replacement .= '';
								}

								if (in_array('poste', $incomesColumns)) {
									$replacement .=
										'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
									$totalTitlePlaced = true;
								} else {
									$replacement .= '';
								}

								if (in_array('name', $incomesColumns)) {
									$replacement .=
										'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
									$totalTitlePlaced = true;
								} else {
									$replacement .= '';
								}

								if (in_array('project', $incomesColumns)) {
									$replacement .=
										'<td>' . (!$totalTitlePlaced ? '<b>' . $totalTitle . '</b>' : '') . '</td>';
									$totalTitlePlaced = true;
								} else {
									$replacement .= '';
								}

								if ($groupByPoste) {
									$replacement .= in_array('percentage', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('amount', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amount'], 2, '.', ' ') .
										'</td>'
										: '';
									$replacement .= in_array('amountHT', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amountHT'], 2, '.', ' ') .
										'</td>'
										: '';
									$replacement .= in_array('amountArbo', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amountArbo'], 2, '.', ' ') .
										'</td>'
										: '';
									$replacement .= in_array('amountHTArbo', $postesColumns)
										? '<td style="text-align: right">' .
										number_format($totalValues['amountHTArbo'], 2, '.', ' ') .
										'</td>'
										: '';
								}

								foreach ($incomesTypes as $type) {
									if ($groupByYear) {
										if ($showOnlyPreviousAndCurrentYears) {
											$replacement .=
												'<td style="text-align: right;">' .
												number_format(Template::getTotalAmountForPreviousYears($totalValues[$type]['years']), 2, '.', ' ') .
												'</td>';
											$replacement .=
												'<td style="text-align: right;">' .
												number_format(Template::getTotalAmountByYear(date('Y'), $totalValues[$type]['years']), 2, '.', ' ') .
												'</td>';
										} else {
											foreach ($incomesYears as $y) {
												$replacement .=
													'<td>' .
													number_format(Template::getTotalAmountByYear($y, $totalValues[$type]['years']), 2, '.', ' ') .
													'</td>';
											}
										}
									} else {
										$replacement .=
											'<td style="text-align: right">' .
											number_format($totalValues[$type]['all'], 2, '.', ' ') .
											'</td>';
									}
								}

								$replacement .= in_array('date', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('tiers', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('anneeExercice', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('bordereau', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('mandat', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('engagement', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('complement', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('serie', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('evolution', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('transfer.name', $incomesColumns) ? '<td></td>' : '';
								$replacement .= in_array('transfer.code', $incomesColumns) ? '<td></td>' : '';

								if ($groupByPoste) {
									$replacement .= in_array('autofinancement', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('caduciteStart', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('caduciteEnd', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('caduciteSendProof', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('folderSendingDate', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('folderReceiptDate', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('arrDate', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('arrNumber', $postesColumns) ? '<td></td>' : '';
									$replacement .= in_array('deliberationDate', $postesColumns) ? '<td></td>' : '';
								}

								$replacement .= '</tr>';

								if (isset($totalValues['envelopes']) && is_array($totalValues['envelopes'])) {
									foreach ($totalValues['envelopes'] as $envelope) {
										$replacement .= '<tr>';

										$envelopeNamePlaced = false;

										if (
											$total !== 'false' && ($groupByPoste &&
												!in_array('account', $postesColumns) && ($groupByPoste && !in_array('envelope', $postesColumns)) && ($groupByPoste && !in_array('financer', $postesColumns)) && ($groupByPoste && !in_array('name', $postesColumns)) &&
												!in_array('financer', $incomesColumns) &&
												!in_array('poste', $incomesColumns) &&
												!in_array('name', $incomesColumns) &&
												!in_array('project', $incomesColumns))
										) {
											$replacement .= '<td><b>' . $envelope['name'] . '</b></td>';
											$envelopeNamePlaced = true;
										}

										if ($groupByPoste) {
											if (in_array('account', $postesColumns)) {
												$replacement .=
													'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
													'</td>';
												$envelopeNamePlaced = true;
											} else {
												$replacement .= '';
											}

											if (in_array('envelope', $postesColumns)) {
												$replacement .=
													'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
													'</td>';
												$envelopeNamePlaced = true;
											} else {
												$replacement .= '';
											}

											if (in_array('financer', $postesColumns)) {
												$replacement .=
													'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
													'</td>';
												$envelopeNamePlaced = true;
											} else {
												$replacement .= '';
											}

											if (in_array('name', $postesColumns)) {
												$replacement .=
													'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
													'</td>';
												$envelopeNamePlaced = true;
											} else {
												$replacement .= '';
											}
										}

										if (in_array('financer', $incomesColumns)) {
											$replacement .=
												'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
												'</td>';
											$envelopeNamePlaced = true;
										} else {
											$replacement .= '';
										}

										if (in_array('poste', $incomesColumns)) {
											$replacement .=
												'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
												'</td>';
											$envelopeNamePlaced = true;
										} else {
											$replacement .= '';
										}

										if (in_array('name', $incomesColumns)) {
											$replacement .=
												'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
												'</td>';
											$envelopeNamePlaced = true;
										} else {
											$replacement .= '';
										}

										if (in_array('project', $incomesColumns)) {
											$replacement .=
												'<td>' . (!$envelopeNamePlaced ? '<b>' . $envelope['name'] . '</b>' : '') .
												'</td>';
											$envelopeNamePlaced = true;
										} else {
											$replacement .= '';
										}

										if ($groupByPoste) {
											$replacement .= in_array('percentage', $postesColumns) ? '<td></td>' : '';
											$replacement .= in_array('amount', $postesColumns)
												? '<td style="text-align: right">' .
												number_format($envelope['amount'], 2, '.', ' ') .
												'</td>'
												: '';
											$replacement .= in_array('amountHT', $postesColumns)
												? '<td style="text-align: right">' .
												number_format($envelope['amountHT'], 2, '.', ' ') .
												'</td>'
												: '';
											$replacement .= in_array('amountArbo', $postesColumns)
												? '<td style="text-align: right">' .
												number_format($envelope['amountArbo'], 2, '.', ' ') .
												'</td>'
												: '';
											$replacement .= in_array('amountHTArbo', $postesColumns)
												? '<td style="text-align: right">' .
												number_format($envelope['amountHTArbo'], 2, '.', ' ') .
												'</td>'
												: '';
										}

										foreach ($incomesTypes as $type) {
											if ($groupByYear) {
												if ($showOnlyPreviousAndCurrentYears) {
													$replacement .= '<td></td><td></td>';	// two columns (previous years and current one)
												} else {
													foreach ($incomesYears as $y) {
														$replacement .= '<td></td>';		// one column per year
													}
												}
											} else {
												$replacement .= '<td></td>';				// only one column
											}
										}

										$replacement .= in_array('date', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('tiers', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('anneeExercice', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('bordereau', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('mandat', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('engagement', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('complement', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('serie', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('evolution', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('transfer.name', $incomesColumns) ? '<td></td>' : '';
										$replacement .= in_array('transfer.code', $incomesColumns) ? '<td></td>' : '';

										if ($groupByPoste) {
											$replacement .= in_array('autofinancement', $postesColumns)
												? '<td></td>'
												: '';
											$replacement .= in_array('caduciteStart', $postesColumns) ? '<td></td>' : '';
											$replacement .= in_array('caduciteEnd', $postesColumns) ? '<td></td>' : '';
											$replacement .= in_array('caduciteSendProof', $postesColumns)
												? '<td></td>'
												: '';
											$replacement .= in_array('folderSendingDate', $postesColumns)
												? '<td></td>'
												: '';
											$replacement .= in_array('folderReceiptDate', $postesColumns)
												? '<td></td>'
												: '';
											$replacement .= in_array('arrDate', $postesColumns) ? '<td></td>' : '';
											$replacement .= in_array('arrNumber', $postesColumns) ? '<td></td>' : '';
											$replacement .= in_array('deliberationDate', $postesColumns)
												? '<td></td>'
												: '';
										}

										$replacement .= '</tr>';
									}
								}
							}
						}

						$replacement .= '</tbody>';
						$replacement .= '</table>';

						break;
					case 'financer':
					case 'financerBudgetRepartitionChart':
						$isArborescent = $params[0] === 'true';
						if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
							$isArborescent = false;
						}
						$type = $params[1];
						$showPercent = $params[2] === 'true';
						$showValue = $params[3] === 'true';

						if ($type === 'poste') {
							$repartition = $object->{'getFinancerRepartitionPosteIncome' . ($isArborescent ? 'Arbo' : '')}($em);
						} else {
							$repartition = $object->{'getFinancerRepartitionIncome' . ($isArborescent ? 'Arbo' : '')}($type, $year);
						}

						if ($repartition) {
							switch ($identifier) {
								case 'financer':
									$replacement = '';
									foreach ($repartition as $i => $financer) {
										if ($i !== current(array_keys($repartition))) {
											$replacement .= ', ';
										}

										$replacement .= $financer['name'];
										if ($showValue && $showPercent) {
											$replacement .=
												' (' . $financer['amount'] . '€, ' . round($financer['percentage']) . '%)';
										} else {
											if ($showValue) {
												$replacement .= ' (' . $financer['amount'] . '€)';
											} elseif ($showPercent) {
												$replacement .= ' (' . round($financer['percentage']) . '%)';
											}
										}
									}
									break;
								case 'financerBudgetRepartitionChart':
									$chart = [];
									foreach ($repartition as $financer) {
										if ($financer['amount'] > 0) {
											if ($financer['name']) {
												$chart[$financer['name']] = [$financer['amount']];
											} else {
												$chart['N/A'] = [$financer['amount']];
											}
										}
									}

									$chartIndex = $variableStr . $index . uniqid();
									$globalVariable = [
										'name' => $chartIndex,
										'type' => 'chart',
										'data' => [
											'type' => 'pie',
											'data' => $chart,
											'chartAlign' => 'center',
											'showPercent' => $showPercent,
											'showValue' => $showValue,
										],
										'options' => [
											'type' => 'block',
										],
									];
									$replacement = '$' . $chartIndex . '$';
									break;
							}
						} else {
							$replacement = '';
						}
						break;
					case 'amountPosteExpenseArbo':
					case 'amountPosteExpense':
					case 'amountPosteIncomeArbo':
					case 'amountPosteIncome':
					case 'amountHTPosteExpenseArbo':
					case 'amountHTPosteExpense':
					case 'amountHTPosteIncomeArbo':
					case 'amountHTPosteIncome':		
						if(!($object instanceof IBudgetComputable)) {
							throw new \Exception("To be able to use $identifier in export, please implement IBudgetComputable interface on source object");
						}			
						$replacement = $object->{'get' . ucfirst($identifier)}($em);
						break;

						/***********************
						 *     CONVENTIONS     *
						 ***********************/
					case 'conventionArray':
						$columns = is_array($params[0]) ? $params[0] : [];

						$replacement = '<table class="table">';
						$replacement .= '<thead>';
						$replacement .= '<tr>';
						$replacement .= in_array('name', $columns)
							? '<th>' . $translator->__invoke('convention_field_name') . '</th>'
							: '';
						$replacement .= in_array('contractor', $columns)
							? '<th>' . $translator->__invoke('convention_field_contractor') . '</th>'
							: '';
						$replacement .= in_array('percentage', $columns)
							? '<th>' . $translator->__invoke('convention_line_field_percentage') . '</th>'
							: '';
						$replacement .= in_array('number', $columns)
							? '<th>' . $translator->__invoke('convention_field_number') . '</th>'
							: '';
						$replacement .= in_array('numberArr', $columns)
							? '<th>' . $translator->__invoke('convention_field_numberArr') . '</th>'
							: '';
						$replacement .= in_array('start', $columns)
							? '<th>' . $translator->__invoke('convention_field_start') . '</th>'
							: '';
						$replacement .= in_array('end', $columns)
							? '<th>' . $translator->__invoke('convention_field_end') . '</th>'
							: '';
						$replacement .= in_array('decisionDate', $columns)
							? '<th>' . $translator->__invoke('convention_field_decisionDate') . '</th>'
							: '';
						$replacement .= in_array('notificationDate', $columns)
							? '<th>' . $translator->__invoke('convention_field_notificationDate') . '</th>'
							: '';
						$replacement .= in_array('returnDate', $columns)
							? '<th>' . $translator->__invoke('convention_field_returnDate') . '</th>'
							: '';
						$replacement .= in_array('amount', $columns)
							? '<th>' . $translator->__invoke('convention_field_amount') . '</th>'
							: '';
						$replacement .= in_array('amountSubv', $columns)
							? '<th>' . $translator->__invoke('convention_field_amountSubv') . '</th>'
							: '';
						$replacement .= '</tr>';
						$replacement .= '</thead>';
						$replacement .= '<tbody>';
						foreach ($object->getConventions() as $line) {
							$convention = $line->getConvention();
							$replacement .= '<tr>';
							$replacement .= in_array('name', $columns)
								? '<td>' . $convention->getName() . '</td>'
								: '';
							$replacement .= in_array('contractor', $columns)
								? '<td>' . ($convention->getContractor() ? $convention->getContractor()->getName() : '') .
								'</td>'
								: '';
							$replacement .= in_array('percentage', $columns)
								? '<td style="text-align:right">' . $line->getPercentage() . ' %</td>'
								: '';
							$replacement .= in_array('number', $columns)
								? '<td>' . $convention->getNumber() . '</td>'
								: '';
							$replacement .= in_array('numberArr', $columns)
								? '<td>' . $convention->getNumberArr() . '</td>'
								: '';
							$replacement .= in_array('start', $columns)
								? '<td>' . ($convention->getStart() ? $convention->getStart()->format('d/m/Y') : '') .
								'</td>'
								: '';
							$replacement .= in_array('end', $columns)
								? '<td>' . ($convention->getEnd() ? $convention->getEnd()->format('d/m/Y') : '') .
								'</td>'
								: '';
							$replacement .= in_array('decisionDate', $columns)
								? '<td>' . ($convention->getDecisionDate()
									? $convention->getDecisionDate()->format('d/m/Y')
									: '') .
								'</td>'
								: '';
							$replacement .= in_array('notificationDate', $columns)
								? '<td>' . ($convention->getNotificationDate()
									? $convention->getNotificationDate()->format('d/m/Y')
									: '') .
								'</td>'
								: '';
							$replacement .= in_array('returnDate', $columns)
								? '<td>' . ($convention->getReturnDate()
									? $convention->getReturnDate()->format('d/m/Y')
									: '') .
								'</td>'
								: '';
							$replacement .= in_array('amount', $columns)
								? '<td style="text-align:right">' .
								number_format($convention->getAmount(), 2, '.', ' ') .
								' €</td>'
								: '';
							$replacement .= in_array('amountSubv', $columns)
								? '<td style="text-align:right">' .
								number_format($convention->getAmountSubv(), 2, '.', ' ') .
								' €</td>'
								: '';
							$replacement .= '</tr>';
						}
						$replacement .= '</tbody>';
						$replacement .= '</table>';

						break;

						/***********************
						 *       FIELDS        *
						 ***********************/
					case 'customField':
						$id = $params[0];
						$value = $em->getRepository('Field\Entity\Value')->findOneBy([
							'primary' => $object->getId(),
							'entity' => $class,
							'field' => str_replace('customField', '', $id),
						]);
						$replacement = '';
						if ($value) {
							$replacement = is_array($value->getValue())
								? implode(' ; ', $value->getValue())
								: $value->getValue();
						}
						break;



						/***********************
						 *       KEYWORDS      *
						 ***********************/
					case 'keyword':
                        $id = $params[0];
                        $arbo = false;
                        if (isset($params[1])) {
                            $arbo = $params[1] === 'true';
                        }

                        $query = $em->createQuery(
                            'SELECT a FROM Keyword\Entity\Association a WHERE a.entity = :entity AND a.primary = :primary AND a.keyword IN (SELECT k FROM Keyword\Entity\Keyword k WHERE k.group = :group AND k.archived = :archived)'
                        );
                        $query->setParameters([
                            'primary' => $object->getId(),
                            'entity' => $class,
                            'group' => $id,
                            'archived' => false,
                        ]);

                        /** @var Association[] $associations */
                        $associations = $query->getResult();
                        $replacement = '';
                        $keywordArray = [];
                        $tree = [];
                        if ($associations) {
                            $separator = '';
                            foreach ($associations as $association) {
                                if (!$arbo) {
                                    $replacement .= $separator . $association->getKeyword()->getName();
                                    $separator = ' ; ';
                                } else {
                                    /** @var Keyword $keywordOriginal */
                                    $keywordOriginal = $association->getKeyword();
                                    /** @var Keyword[] $parents */
                                    $parents_sup = $keywordOriginal->getHierarchySup();
                                    $parents_sup = $parents_sup->toArray();
                                    if (isset($parents_sup[count($parents_sup) - 1])) {
                                        $keywordArray[$parents_sup[count($parents_sup) - 1]->getId()][] = $keywordOriginal;
                                    }
                                    $flatArray = [];
                                    foreach ($parents_sup as $parent_sup) {
                                        if (!isset($flatArray[$parent_sup->getId()])) {
                                            $flatArray[$parent_sup->getId()] = [];
                                        }
                                    }

                                    foreach ($flatArray as $keyword_id => $values) {
                                        $keyword = $em->getRepository('Keyword\Entity\Keyword')->find($keyword_id);
                                        if ($keyword->getParents()) {
                                            foreach ($keyword->getParents() as $parent) {
                                                $flatArray[$keyword_id]['parents'][] = $parent->getId();
                                            }
                                        }
                                        $flatArray[$keyword_id]['id'] = $keyword_id;
                                        $flatArray[$keyword_id]['name'] = $keyword->getName();
                                        $flatArray[$keyword_id]['group'] = $keyword->getGroup()->getName();
                                    }

                                    $n = sizeOf($flatArray);

                                    do {
                                        $relaunch = false;
                                        foreach ($flatArray as $i => $item) {
                                            $item['level'] = 0;
                                            if (!isset($item['parents']) || $item['parents'] == null) {
                                                if (is_null($tree[$i])) {
                                                    $tree[$i] = $item;
                                                }
                                                $n--;
                                                $relaunch = true;
                                                unset($flatArray[$i]);
                                            } else {
                                                if ($this->insertInTreeWithParents($item, $tree, 1)) {
                                                    unset($flatArray[$i]);
                                                    $n--;
                                                    $relaunch = true;
                                                }
                                            }
                                        }
                                    } while ($n > 0 && $relaunch);
                                    $first = true;

                                }
                            }
                            foreach ($tree as $parent) {
                                $childs = $parent['children'];
								if(isset($childs) && is_array($childs)) {
                                	array_multisort($childs);
								}
                                $parent['children'] = $childs;
                                if ($first && !$excel) {
                                    $replacement .= '<ul><li>' . $parent['group'];
                                    $first = false;
                                }
                                $replacement .= $this->getNameArboRecursiveTree(
                                    $parent,
                                    $keywordArray,
                                    $excel
                                );
                            }

                            if (!$excel) {
                                $replacement .= '</li></ul>';
                            }
                        }
                        break;

                    /***********************
                     *         MAP         *
                     ***********************/
                    case 'territories':
                        $isArborescent = $params[0] === 'true';
                        $withParents = array_key_exists(1, $params) ? ($params[1] === 'true') : false;
                        if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
                            $isArborescent = false;
                        }
                        $territories = $object->{'getTerritories' . ($withParents ? 'Parent' : '') . ($isArborescent ? 'Arbo' : '')}($em);

                        $replacement = '';
                        $separator = '';
                        foreach ($territories as $territory) {
                            $replacement .= $separator . $territory->getName();
                            $separator = ' ; ';
                        }
                        break;
                    case 'territoriesArray':
                        $isArborescent = $params[0] === 'true';
                        $withParents = array_key_exists(2, $params) ? ($params[2] === 'true') : false;
                        if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
                            $isArborescent = false;
                        }
                        $columns = is_array($params[1]) ? $params[1] : [];

                        $territories = $object->{'getTerritories' . ($withParents ? 'Parent' : '') . ($isArborescent ? 'Arbo' : '')}($em);

                        $replacement = '<table class="table">';
                        $replacement .= '<thead>';
                        $replacement .= '<tr>';
                        $replacement .= in_array('code', $columns)
                            ? '<th>' . $translator->__invoke('territory_field_code') . '</th>'
                            : '';
                        $replacement .= in_array('name', $columns)
                            ? '<th>' . $translator->__invoke('territory_field_name') . '</th>'
                            : '';
                        $replacement .= '</tr>';
                        $replacement .= '</thead>';
                        $replacement .= '<tbody>';
                        foreach ($territories as $territory) {
                            $replacement .= '<tr>';
                            $replacement .= in_array('code', $columns)
                                ? '<td>' . $territory->getCode() . '</td>'
                                : '';
                            $replacement .= in_array('name', $columns)
                                ? '<td>' . $territory->getName() . '</td>'
                                : '';
                            $replacement .= '</tr>';
                        }
                        $replacement .= '</tbody>';
                        $replacement .= '</table>';
                        break;
                    case 'territoriesMap':
                        $isArborescent = $params[0] === 'true';
                        $isArborescent = $params[0] === 'true';
                        $sizeW = $params[1] ?: '200px';
                        $sizeH = $params[2] ? '*' . $params[2] : '';
                        $size = $sizeW . $sizeH;

                        if ($isProject && $this->export->getReference() == Export::EXPORT_REFERENCE_KEYWORD) {
                            $isArborescent = false;
                        }
                        $territories = $object->{'getTerritories' . ($isArborescent ? 'Arbo' : '')}($em);

                        $replacement = '';
                        if (sizeof($territories) > 0) {
                            $image =
                                getcwd() .
                                '/data/phpdocx/temp/' .
                                $exportIdentifier .
                                '_img_map_' .
                                uniqid() .
                                '.png';
                            $host = isset($_SERVER['HTTP_ORIGIN'])
                                ? $_SERVER['HTTP_ORIGIN']
                                : 'http://' . $_SERVER['HTTP_HOST'];
                            PhantomJS::capture(
                                $host .
                                '/export/map?mode=' . ($isArborescent ? 'arbo' : '') .
                                '&class=' .
                                $class .
                                '&id=' .
                                $object->getId(),
                                $image,
                                $size
                            );
                            $replacement = '<img src="file://' . $image . '" />';
                        }
                        break;

						/***********************
						 *     INDICATORS      *
						 ***********************/
					case 'indicatorDone':
					case 'indicatorTarget':
						$replacement = '';
						$id = $params[0];

						$measures = $object->getMeasures($id, $year);
						if ($measures->count() > 0) {
							$indicator = $measures->first()->getIndicator();
							$replacement = $indicator->getValue(
								$measures->filter(function ($measure) use ($identifier) {
									return $measure->getType() == ($identifier === 'indicatorTarget'
										? Measure::TYPE_TARGET
										: Measure::TYPE_DONE) /*&& $measure->getPeriod() == Measure::PERIOD_INTERMEDIATE*/;
								})
							);

							if ($indicator->getType() === Indicator::TYPE_FIXED) {
								$replacement = $indicator->getFixedValue($replacement)['text'];
							}
						}
						break;
					case 'indicatorRatio':
						$replacement = '';
						$id = $params[0];

						$measures = $object->getMeasures($id, $year);
						if ($measures->count() > 0) {
							$indicator = $measures->first()->getIndicator();
							$done = $indicator->getValue(
								$measures->filter(function ($measure) {
									return $measure->getType() ==
										Measure::TYPE_DONE /*&& $measure->getPeriod() == Measure::PERIOD_INTERMEDIATE*/;
								})
							);
							$target = $indicator->getValue(
								$measures->filter(function ($measure) {
									return $measure->getType() ==
										Measure::TYPE_TARGET /*&& $measure->getPeriod() == Measure::PERIOD_INTERMEDIATE*/;
								})
							);

							$replacement =
								$target > 0 ? round(($done * 100) / $target) : ($done > 0 ? 100 : null);
						}
						break;
					case 'indicatorArray':
						$ids = is_array($params[0]) ? $params[0] : [];
						foreach ($ids as $id) {
							if ($id == 'dynamic') {
								$ids = $object->getIndicatorsIds($year);
								break;
							}
						}
						$columns = is_array($params[1]) ? $params[1] : [];

						$replacement = '<table class="table">';
						$replacement .= '<thead>';
						$replacement .= '<tr>';
                        $replacement .= in_array('indicator', $columns)
							? '<th>' . $translator->__invoke('measure_field_indicator') . '</th>'
							: '';
						$replacement .= in_array('period', $columns)
							? '<th>' . $translator->__invoke('measure_field_period') . '</th>'
							: '';
						$replacement .= in_array('type', $columns)
							? '<th>' . $translator->__invoke('measure_field_type') . '</th>'
							: '';
						$replacement .= in_array('start', $columns)
							? '<th>' . $translator->__invoke('measure_field_start') . '</th>'
							: '';
						$replacement .= in_array('date', $columns)
							? '<th>' . $translator->__invoke('measure_field_date') . '</th>'
							: '';
						$replacement .= in_array('comment', $columns)
							? '<th>' . $translator->__invoke('measure_field_comment') . '</th>'
							: '';
						$replacement .= in_array('source', $columns)
							? '<th>' . $translator->__invoke('measure_field_source') . '</th>'
							: '';
						$replacement .= in_array('value', $columns)
							? '<th>' . $translator->__invoke('measure_field_value') . '</th>'
							: '';
						$replacement .= in_array('territories', $columns)
							? '<th>' . $translator->__invoke('measure_field_territories') . '</th>'
							: '';
						$replacement .= '</tr>';
						$replacement .= '</thead>';
						$replacement .= '<tbody>';
						foreach ($ids as $id) {
							$measures = $object->getMeasures(
								$id,
								$year
							) /*->filter(function ($measure) {
                                    return $measure->getPeriod() == Measure::PERIOD_INTERMEDIATE;
                                })*/;
							if ($measures->count() > 0) {
								$indicator = $measures->first()->getIndicator();

								$replacement .= '<tr>';
								$replacement .=
									'<td colspan="' .
									sizeOf($columns) .
									'"><b>' .
									$indicator->getName() .
									'</b></td>';
								$replacement .= '</tr>';

								foreach ($measures as $measure) {
									$associations = $em->getRepository('Map\Entity\Association')->findBy([
										'primary' => $measure->getId(),
										'entity'    => 'Indicator\Entity\Measure'
									]);
									$territories = [];
									foreach($associations as $association) {
										$territories[] = $association->getTerritory()->getName();
									}
									$replacement .= '<tr>';
                                    $replacement .= in_array('indicator', $columns)
										? '<td>' . $indicator->getName().
										'</td>'
										: '';
									$replacement .= in_array('period', $columns)
										? '<td>' . $translator->__invoke('measure_period_' . $measure->getPeriod()) .
										'</td>'
										: '';
									$replacement .= in_array('type', $columns)
										? '<td>' .
										$translator->__invoke('measure_type_' . $measure->getType()) .
										'</td>'
										: '';
									$replacement .= in_array('start', $columns)
										? '<td>' . ($measure->getStart() ? $measure->getStart()->format('d/m/Y') : '') .
										'</td>'
										: '';
									$replacement .= in_array('date', $columns)
										? '<td>' . ($measure->getDate() ? $measure->getDate()->format('d/m/Y') : '') .
										'</td>'
										: '';
									$replacement .= in_array('comment', $columns)
										? '<td>' .
										($measure->getComment() ? $measure->getComment() : '') .
										'</td>'
										: '';
									$replacement .= in_array('source', $columns)
										? '<td>' .
										($measure->getSource() ? $measure->getSource() : '') .
										'</td>'
										: '';
									if (in_array('value', $columns)) {
										if ($indicator->getType() === Indicator::TYPE_FIXED) {
											$value = $measure->getFixedValue();
											$replacement .=
												'<td style="background-color: ' .
												$value['color'] .
												'">' .
												$value['text'] .
												'</td>';
										} else {
											$replacement .= '<td>' . $measure->getValue() . '</td>';
										}
									}
									$replacement .= in_array('territories', $columns)
										? '<td>' .
										implode('; ', $territories) .
										'</td>'
										: '';
									$replacement .= '</tr>';
								}
							}
						}
						$replacement .= '</tbody>';
						$replacement .= '</table>';
						break;
					case 'indicatorChart':
						$ids = is_array($params[0]) ? $params[0] : [];
						foreach ($ids as $id) {
							if ($id == 'dynamic') {
								$ids = $object->getIndicatorsIds($year);
								break;
							}
						}

						$chart = [
							'legend' => [
								$translator->__invoke('measure_type_target'),
								$translator->__invoke('measure_type_done'),
							],
						];
						foreach ($ids as $id) {
							$measures = $object->getMeasures(
								$id,
								$year
							);
							if ($measures->count() > 0) {
								$indicator = $measures->first()->getIndicator();

								$target = $indicator->getValue(
									$measures->filter(function ($measure) use ($identifier) {
										return $measure->getType() == Measure::TYPE_TARGET;
									})
								);

								$done = $indicator->getValue(
									$measures->filter(function ($measure) use ($identifier) {
										return $measure->getType() == Measure::TYPE_DONE;
									})
								);

								$chart[$indicator->getName()] = [(float) $target, (float) $done];
							}
						}

						$chartIndex = $variableStr . $index . uniqid();
						$globalVariable = [
							'name' => $chartIndex,
							'type' => 'chart',
							'data' => [
								'data' => $chart,
								'type' => 'bar',
								'chartAlign' => 'center',
								'sizeX' => 15,
							],
							'options' => [
								'type' => 'block',
							],
						];

						$replacement = '$' . $chartIndex . '$';

						break;
					case 'indicatorRecap':
						$ids = is_array($params[0]) ? $params[0] : [];
						foreach ($ids as $id) {
							if ($id == 'dynamic') {
								$ids = $object->getIndicatorsIds($year);
								break;
							}
						}

						$data = [];
						$years = [];
						foreach ($ids as $id) {
							$measures = $object->getMeasures($id, $year);
							if ($measures->count() > 0) {
								foreach ($measures as $measure) {
									$indicator = $measure->getIndicator();

									if (!isset($data[$indicator->getId()])) {
										$data[$indicator->getId()] = [
											'indicator' => $indicator,
											'measures' => [],
										];
									}

									$yearMeasure = $measure->getDate()->format('Y');
									if (!isset($data[$indicator->getId()]['measures'][$yearMeasure])) {
										$data[$indicator->getId()]['measures'][$yearMeasure] = [
											Measure::TYPE_DONE => [],
											Measure::TYPE_TARGET => [],
										];
									}

									$data[$indicator->getId()]['measures'][$yearMeasure][$measure->getType()][] = $measure;

									if (!in_array($yearMeasure, $years)) {
										$years[] = $yearMeasure;
									}
								}
							}
						}

						sort($years);

						$replacement = '<table class="table">';
						$replacement .= '<thead>';
						$replacement .= '<tr>';
						$replacement .= '<th></th>';
						foreach ($years as $yearReplacement) {
							$replacement .= '<th colspan="3">' . $yearReplacement . '</th>';
						}
						$replacement .= '</tr>';
						$replacement .= '<tr>';
						$replacement .= '<th>' . ucfirst($translator->__invoke('indicator_entity')) . '</th>';
						foreach ($years as $yearReplacement) {
							$replacement .= '<th>' . $translator->__invoke('measure_type_target') . '</th>';
							$replacement .= '<th>' . $translator->__invoke('measure_type_done') . '</th>';
							$replacement .= '<th>%</th>';
						}
						$replacement .= '</tr>';
						$replacement .= '</thead>';
						$replacement .= '<tbody>';
						foreach ($data as $_indicator) {
							$indicator = $_indicator['indicator'];
							$replacement .= '<tr>';
							$replacement .= '<td><b>' . $indicator->getName() . '</b></td>';
							foreach ($years as $yearReplacement) {
								$_measures = isset($_indicator['measures'][$yearReplacement])
									? $_indicator['measures'][$yearReplacement]
									: [];

								$done = $indicator->getValue($_measures[Measure::TYPE_DONE]);
								$target = $indicator->getValue($_measures[Measure::TYPE_TARGET]);
								$ratio = $target > 0 ? round(($done * 100) / $target) : ($done > 0 ? 100 : null);

								if ($indicator->getType() === Indicator::TYPE_FIXED) {
									$done = $indicator->getFixedValue($done)['text'];
									$target = $indicator->getFixedValue($target)['text'];
								}
								$replacement .= '<td>' . $target . '</td>';
								$replacement .= '<td>' . $done . '</td>';
								$replacement .=
									'<td style="text-align: right; background-color:#' . ($ratio > 0 ? ($ratio >= 100 ? '88D888' : 'FFDD68') : 'F56E6E') .
									'">' .
									$ratio .
									' %</td>';
							}
							$replacement .= '</tr>';
						}

						$replacement .= '</tbody>';
						$replacement .= '</table>';

						break;

						/***********************
						 *     ADVANCEMENT     *
						 ***********************/
					case 'advancementDone':
					case 'advancementTarget':
						$type = strpos($identifier, 'Done') !== false ? 'done' : 'target';
						$method = ucfirst($params[0]);
						if ($method == 'Last') {
							$method = 'LastAdd';
						}

						$replacement = '';

						$advancement = $object->{'get' . $method . 'Advancement'}($type, $year);
						if ($advancement) {
							$replacement = $advancement->getValue();
						}
						break;
					case 'advancementDoneName':
					case 'advancementTargetName':
						$type = strpos($identifier, 'Done') !== false ? 'done' : 'target';
						$method = ucfirst($params[0]);
						if ($method == 'Last') {
							$method = 'LastAdd';
						}

						$replacement = '';

						$advancement = $object->{'get' . $method . 'Advancement'}($type, $year);
						if ($advancement) {
							$replacement = $advancement->getName();
						}
						break;
					case 'advancementDoneDescription':
					case 'advancementTargetDescription':
						$type = strpos($identifier, 'Done') !== false ? 'done' : 'target';
						$method = ucfirst($params[0]);
						if ($method == 'Last') {
							$method = 'LastAdd';
						}

						$replacement = '';

						$advancement = $object->{'get' . $method . 'Advancement'}($type, $year);
						if ($advancement) {
							$replacement = $advancement->getDescription();
						}
						break;
					case 'advancementDoneDate':
					case 'advancementTargetDate':
						$type = strpos($identifier, 'Done') !== false ? 'done' : 'target';
						$method = ucfirst($params[0]);
						if ($method == 'Last') {
							$method = 'LastAdd';
						}

						$replacement = '';

						$advancement = $object->{'get' . $method . 'Advancement'}($type, $year);
						if ($advancement) {
							$replacement = $advancement->getDate()->format('d/m/Y');
						}
						break;
					case 'advancementArray':
						$columns = is_array($params[0]) ? $params[0] : [];

						$replacement = '<table class="table">';
						$replacement .= '<thead>';
						$replacement .= '<tr>';
						$replacement .= in_array('date', $columns)
							? '<th>' . $translator->__invoke('advancement_field_date') . '</th>'
							: '';
						$replacement .= in_array('type', $columns)
							? '<th>' . $translator->__invoke('advancement_field_type') . '</th>'
							: '';
						$replacement .= in_array('value', $columns)
							? '<th>' . $translator->__invoke('advancement_field_value') . '</th>'
							: '';
						$replacement .= in_array('name', $columns)
							? '<th>' . $translator->__invoke('advancement_field_name') . '</th>'
							: '';
						$replacement .= in_array('description', $columns)
							? '<th>' . $translator->__invoke('advancement_field_description') . '</th>'
							: '';
						$replacement .= '</tr>';
						$replacement .= '</thead>';
						$replacement .= '<tbody>';
						foreach ($object->getAdvancements() as $advancement) {
							$replacement .= '<tr>';
							$replacement .= in_array('date', $columns)
								? '<td>' . ($advancement->getDate() ? $advancement->getDate()->format('d/m/Y') : '') .
								'</td>'
								: '';
							$replacement .= in_array('type', $columns)
								? '<td>' .
								$translator->__invoke('advancement_type_' . $advancement->getType()) .
								'</td>'
								: '';
							$replacement .= in_array('value', $columns)
								? '<td style="text-align: right">' . $advancement->getValue() . '%</td>'
								: '';
							$replacement .= in_array('name', $columns)
								? '<td>' . $advancement->getName() . '</td>'
								: '';
							$replacement .= in_array('description', $columns)
								? '<td>' . $advancement->getDescription() . '</td>'
								: '';
							$replacement .= '</tr>';
						}
						$replacement .= '</tbody>';
						$replacement .= '</table>';
						break;
					case 'advancementChart':
                        $sizeW = $params[0] ?: '200px';
                        $sizeH = $params[1] ? '*' . $params[1] : '';
                        $size = $sizeW . $sizeH;

                        $image =
							getcwd() .
							'/data/phpdocx/temp/' .
							$exportIdentifier .
							'_img_advancement_' .
							uniqid() .
							'.png';
						$host = isset($_SERVER['HTTP_ORIGIN'])
							? $_SERVER['HTTP_ORIGIN']
							: 'http://' . $_SERVER['HTTP_HOST'];
						PhantomJS::capture(
							$host . '/export/advancement?class=' . $class . '&id=' . $object->getId(),
							$image,
                            $size
						);
						$replacement = '<img src="file://' . $image . '" />';
						break;

						/***********************
						 *        ACTORS       *
						 ***********************/
					case preg_match('/^actorsKeywords_\d+$/', $identifier) ? true : false:
						$replacement = '';
						$group_id = str_replace('actorsKeywords_', '', $identifier);
						$group = $em->getRepository('Keyword\Entity\Group')->find($group_id);
						$separatorActor = '';
						foreach ($object->getActors() as $actor) {
							$query = $em->createQuery(
								'SELECT a FROM Keyword\Entity\Association a WHERE a.entity = :entity AND a.primary = :primary AND a.keyword IN (SELECT k FROM Keyword\Entity\Keyword k WHERE k.group = :group AND k.archived = :archived)'
							);
							$query->setParameters([
								'primary' => $actor->getId(),
								'entity' => 'Project\Entity\Actor',
								'group' => $group->getId(),
								'archived' => false,
							]);
							$associations = $query->getResult();

							$replacement .= $separatorActor . $actor->getStructure()->getName();

							if (count($associations) > 0) {
								$replacement .= ' : ';
								$separatorAssociation = '';
								foreach ($associations as $association) {
									$replacement .= $separatorAssociation . $association->getKeyword()->getName();
									$separatorAssociation = ' ; ';
								}
							}

							$separatorActor = ' ';
						}
						break;

					case 'actorsArrayKeywords':
						$groups = $em->getRepository('Keyword\Entity\Group')->findByEntity('actor', false);

						$replacement = '<table class="table">';
						$replacement .= '<thead>';
						$replacement .= '<tr>';
						$replacement .=
							'<th>' . $translator->__invoke('[[ actor_entity | ucfirst ]]') . '</th>';
						foreach ($groups as $group) {
							$replacement .= '<th>' . $group->getName() . '</th>';
						}
						$replacement .= '</tr>';
						$replacement .= '</thead>';
						$replacement .= '<tbody>';
						foreach ($object->getActors() as $actor) {
							$replacement .= '<tr>';
							$replacement .= '<td>' . $actor->getStructure()->getName() . '</td>';
							foreach ($groups as $group) {
								$query = $em->createQuery(
									'SELECT a FROM Keyword\Entity\Association a WHERE a.entity = :entity AND a.primary = :primary AND a.keyword IN (SELECT k FROM Keyword\Entity\Keyword k WHERE k.group = :group AND k.archived = :archived)'
								);
								$query->setParameters([
									'primary' => $actor->getId(),
									'entity' => 'Project\Entity\Actor',
									'group' => $group->getId(),
									'archived' => false,
								]);
								$associations = $query->getResult();

								$replacement .= '<td>';
								$separator = '';
								foreach ($associations as $association) {
									$replacement .= $separator . $association->getKeyword()->getName();
									$separator = ' ; ';
								}
								$replacement .= '</td>';
							}
							$replacement .= '</tr>';
						}
						$replacement .= '</tbody>';
						$replacement .= '</table>';

						break;

						/***********************
						 *        PICTURES     *
						 ***********************/
					case 'picture':
						$key = $params[0];
                        $sizeW = $params[1] ? 'width="'.$params[1].'"' : '';
                        $sizeH = $params[2] ? 'height="'.$params[2].'"' : '';
                        $size = str_replace('px', '', $sizeW . ' ' . $sizeH);

						$attachment = $em->getRepository('Attachment\Entity\Attachment')->findOneBy([
							'primary' => $object->getId(),
							'entity' => 'Project\Entity\Project',
							'key' => $key,
						]);

						// Gere le cas ou la cle est null ou vide
						if (!$key && !$attachment) {
							$attachment = $em->getRepository('Attachment\Entity\Attachment')->findOneBy([
								'primary' => $object->getId(),
								'entity' => 'Project\Entity\Project',
								'key' => '',
							]);
						}

						$replacement = '';
						if ($attachment) {
							$host = isset($_SERVER['HTTP_ORIGIN'])
								? $_SERVER['HTTP_ORIGIN']
								: 'http://' . $_SERVER['HTTP_HOST'];
                            $replacement = '<img src="' . $host . $attachment->getUrl() . '" ' . $size . ' />';
						}
						break;
					case 'pictureGlobal':
						$id = $params[0];
                        $sizeW = $params[1] ? 'width="'.$params[1].'"' : '';
                        $sizeH = $params[2] ? 'height="'.$params[2].'"' : '';
                        $size = str_replace('px', '', $sizeW . ' ' . $sizeH);

						$attachment = $em->getRepository('Attachment\Entity\Attachment')->findOneBy([
							'entity' => 'Bootstrap\Entity\Client',
							'id' => $id,
						]);

						$replacement = '';
						if ($attachment) {
							$host = isset($_SERVER['HTTP_ORIGIN'])
								? $_SERVER['HTTP_ORIGIN']
								: 'http://' . $_SERVER['HTTP_HOST'];
                            $replacement = '<img src="' . $host . $attachment->getUrl() . '" ' . $size . ' />';
						}
						break;

					default:
						$replacement = $extractor->extract($identifier, null, ' ; ', $variableStr);
				}

				if ($globalVariable) {
					$globalVariables[] = $globalVariable;
				}
				$replacements[] = $replacement;
			}

			if ($excel) {
				return $replacements;
			}

			$content = preg_replace(
				array_map(function ($variable) {
					return '/\$' .
						str_replace(['(', ')', '[', ']'], ['\(', '\)', '\[', '\]'], $variable) .
						'\$/';
				}, $variables[1]),
				$replacements,
				$content,
				1
			);
		}

		// Ici saut de page automatique après chaque "objet"

		$pageBreak = '<div style="page-break-after:always;"></div>';

		return ($this->getPageBreak() == self::PAGEBREAK_BEFORE ||
			$this->getPageBreak() == self::PAGEBREAK_BOTH
			? $pageBreak
			: '') .
			$content . ($this->getPageBreak() == self::PAGEBREAK_AFTER ||
				$this->getPageBreak() == self::PAGEBREAK_BOTH
				? $pageBreak
				: '');
	}

	/**
	 * Retrieve the years of the postes' transactions
	 *
	 * @param array		$postes
	 * @param string	$type (expense|income)
	 *
	 * @return array of unique years sorted (ASC)
	 */
	private static function getTransactionsYears($postes, $type)
	{
		$years = [date('Y')];	// default with current year to avoid the type columns disappearance (because of the loop)
		if (!isset($postes)) {
			return $years;
		}
		foreach ($postes as $poste) {
			$transactions = $poste->{'get' . ucfirst($type) . 's'}();
			if (null === $transactions) {
				continue;
			}
			foreach ($transactions as $transaction) {
				array_push($years, $transaction->getYear());
			}
		}
		$uniqueYears = array_unique($years);	// only one column per year
		asort($uniqueYears);					// sort ascending for table presentation
		return $uniqueYears;
	}

	/**
	 * Retrieve the total amount for the given year
	 *
	 * @param int		$year
	 * @param array 	$input yearly based transaction total amounts
	 *
	 * @return float total amount
	 */
	private static function getTotalAmountByYear($year, $input)
	{
		if (!isset($input)) {
			return 0;
		}
		$totalAmount = 0;
		foreach ($input as $y => $total) {
			if ($year == $y) {
				$totalAmount += $total;
			}
		}
		return $totalAmount;
	}

	/**
	 * Retrieve the total amount for the years before the current one
	 *
	 * @param array 	$input yearly based transaction total amounts
	 *
	 * @return float total amount
	 */
	private static function getTotalAmountForPreviousYears($input)
	{
		if (!isset($input)) {
			return 0;
		}
		$currentYear = date('Y');
		$totalAmount = 0;
		foreach ($input as $year => $total) {
			if ($year < $currentYear) {
				$totalAmount += $total;
			}
		}
		return $totalAmount;
	}

	public function insertInTree(&$_item, &$tree, $level)
	{
		foreach ($tree as $id => $item) {
			if ($item['id'] == $_item['parent']) {
				unset($_item['parent']);
				$_item['level'] = $level;
				$tree[$id]['children'][$_item['id']] = $_item;

				return true;
			}

			if ($this->insertInTree($_item, $tree[$id]['children'], $level + 1)) {
				return true;
			}
		}

		return false;
	}

	public function insertInTreeWithParents(&$_item, &$tree, $level)
	{
		foreach ($tree as $id => $item) {
			foreach ($_item['parents'] as $parent) {
				if ($item['id'] == $parent) {
					unset($parent);
					$_item['level'] = $level;
					$tree[$id]['children'][$_item['id']] = $_item;

					return true;
				}

				if ($this->insertInTreeWithParents($_item, $tree[$id]['children'], $level + 1)) {
					return true;
				}
			}
		}

		return false;
	}

	public function getNameArboRecursive($parent, $keyword_name, $excel)
	{
		$start = $excel ? '' : '<ul><li>';
		$tab = $excel ? '    ' : '';
		$endLine = $excel ? "\n" : '';
		$end = $excel ? '' : '</li></ul>';

		$res = '';
		$res .= $start . str_repeat($tab, $parent['level']) . $parent['name'] . $endLine;

		if ($parent['children'] != []) {
			foreach ($parent['children'] as $child) {
				$res .= $this->getNameArboRecursive($child, $keyword_name, $excel);
			}
		} else {
			$res .= $start . str_repeat($tab, $parent['level'] + 1) . $keyword_name . $end;
		}

		$res .= $end;

		return $res;
	}

    /**
     * @param $parent
     * @param $keywordArray
     * @param $excel
     * @return string
     * Method to add keywords from Array with parentId
     */
    public function getNameArboRecursiveTree($parent, $keywordArray, $excel)
    {
        $start = $excel ? '' : '<ul><li>';
        $tab = $excel ? '    ' : '';
        $endLine = $excel ? "\n" : '';
        $end = $excel ? '' : '</li></ul>';

        $res = '';
        $res .= $start . str_repeat($tab, $parent['level']) . $parent['name'] . $endLine;

        if ($parent['children'] != []) {
            foreach ($parent['children'] as $child) {
                $res .= $this->getNameArboRecursiveTree($child, $keywordArray, $excel);
            }
        } else {
            if (key_exists($parent['id'], $keywordArray)) {
                $keywords = $keywordArray[$parent['id']];
                usort($keywords, function($a, $b) {return strcmp($a->getName(), $b->getName());});
                foreach ($keywords as $word) {
                    $res .= $start . str_repeat($tab, $parent['level'] + 1) . $word->getName() . $end;
                }
            }
        }

        $res .= $end;
        return $res;
    }
}
