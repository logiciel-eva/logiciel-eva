(function(app) {

    app.controller('transverseListController', [
        '$scope', 'deploymentService', 'measureService', 'flashMessenger', 'translator', '$rootScope',
        function deploymentController($scope, deploymentService, measureService, flashMessenger, translator, $rootScope) {
            var self = this;

            self.project = $scope.$parent.self.project;
            var module = !!self.project;

            self.indicators = [];

            self.getIndicators = function () {
                deploymentService.findAll({
                    col: [
                        'indicator.id',
                        'indicator.name',
                        'indicator.type',
                        'indicator.values'
                    ],
                    search: {
                        type: 'list',
                        data: {
                            filters: {
                                id: {
                                    op : 'project' ,
                                    val: module.id
                                },
                            }
                        }
                    },
                }).then(function (res) {
                    self.indicators = res.rows;
                    for (var i in self.indicators) {
                        self.indicators[i].measure = {
                            indicator: self.indicators[i].indicator,
                            type: 'done',
                            period: 'intermediate',
                            date: moment().format('DD/MM/YYYY'),
                            transverse: true,
                            project: module.id,
                            value: null,
                            _rights: {
                                update: true
                            }
                        };
                    }

                    measureService.findAll({
                        col: [
                            'id',
                            'period',
                            'type',
                            'start',
                            'date',
                            'value',
                            'fixedValue',
                            'comment',
                            'createdAt',
                            'updatedAt',
                            'indicator.id'
                        ],
                        search: {
                            type: 'list',
                            data: {
                                sort: 'date',
                                order: 'asc',
                                filters: {
                                    project: {
                                        op : 'eq',
                                        val: module.id
                                    },
                                    transverse: {
                                        op: 'eq',
                                        val: '1'
                                    }
                                }
                            }
                        }
                    }).then(function(res) {
                        for (var i in res.rows) {
                            for (var j in self.indicators) {
                                if (self.indicators[j].indicator.id == res.rows[i].indicator.id) {
                                    self.indicators[j].measure = res.rows[i];
                                    self.indicators[j].measure.indicator = self.indicators[j].indicator;
                                    break;
                                }
                            }
                        }
                    })
                })
            };

            self.saveMeasure = function saveMeasure(measure, forceCreate) {
                measure.indicator.loading = true;
                if (forceCreate === true) {
                    measure.id = null;
                }

                measureService.save(measure, {
                    col: [
                        'id',
                        'period',
                        'type',
                        'start',
                        'date',
                        'value',
                        'fixedValue',
                        'transverse',
                        'comment',
                        'createdAt',
                        'updatedAt',
                        'indicator.id',
                        'project.id'
                    ]
                }).then(function(res) {
                    for (var j in self.indicators) {
                        if (self.indicators[j].indicator.id == res.object.indicator.id) {
                            self.indicators[j].measure = res.object;
                            self.indicators[j].measure.indicator = self.indicators[j].indicator;
                            break;
                        }
                    }

                    flashMessenger.success(translator('measure_message_saved'));
                    measure.indicator.loading = false;
                }, function (err) {
                    measure.indicator.loading = false;
                })
            };

            self.deleteMeasure = function deleteMeasure(measure) {
                if (confirm(translator('measure_question_delete'))) {
                    measureService.remove(measure).then(function (res) {
                        if (res.success) {
                            for (var i in self.indicators) {
                                if (measure.indicator.id == self.indicators[i].indicator.id) {
                                    self.indicators[i].measure = {
                                        indicator: self.indicators[i].indicator,
                                        type: 'done',
                                        period: 'intermediate',
                                        date: moment().format('DD/MM/YYYY'),
                                        transverse: true,
                                        project: module.id,
                                        value: null,
                                        _rights: {
                                            update: true
                                        }
                                    };
                                }
                            }
                            flashMessenger.success(translator('measure_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.getSliderOptions = function(measure) {

                var indicator = measure.indicator;

                if (!indicator.sliderOptions) {
                    indicator.sliderOptions = {
                        showTicksValues: true,
                        translate: function(value) {
                            return '';
                        },
                        stepsArray: indicator.values.map(function(value) {
                            return {
                                value: +value.value,
                                legend: {
                                    icon: value.icon,
                                    text: value.text,
                                    color: value.color,
                                }
                            }
                        }),
                        getPointerColor: function(value) {
                            for (var i in  indicator.values) {
                                if (indicator.values[i].value == value) {
                                    return  indicator.values[i].color;
                                }
                            }
                        },
                        readOnly: !measure._rights.update,
                        enforceStep: false,
                        enforceRange: false
                    };
                }

                return indicator.sliderOptions;
            };

            self.goTo = function (indicator) {
                $rootScope.$broadcast('filter-transverse-measure', {id: indicator.id});
                $('#indicators a[href="#measures"]').tab('show');
            };

            self.getIndicators();
        }
    ])

})(window.EVA.app);
