<?php

return [
    'icons' => [
        'campain' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-compass'
        ],
        'generate' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-refresh'
        ]
    ]
];
