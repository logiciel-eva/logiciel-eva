<?php

namespace Core\View\Helper;

use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class FabMenu
 *
 * @package Core\View\Helper
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class FabMenu extends AbstractHelper
{
    public function __invoke($links = [])
    {
        $fabLinks = '';

        foreach ($links as $link) {
            // pas fou
            $href    = isset($link['href'])     ? 'href="' . $link['href'] . '"' : '';
            $ngClick = isset($link['ng-click']) ? 'ng-click="' . $link['ng-click'] . '"' : '';

            $fabLinks .= '
                <li><a '. $href . ' ' . $ngClick . '>' . $link['text'] . '</a></li>
            ';
        }

        $fabMenu = '
        <div class="fab-menu">
            <div class="fab-links-container" ng-class="{
                    open: showFab
                }">
                <ul class="fab-links">
                    ' . $fabLinks . '
                </ul>
                <div class="fab-caret"></div>
            </div>
            <div class="fab-button" ng-click="showFab = !showFab">
                <i class="fa" ng-class="{
                    \'fa-bars\' : !showFab,
                    \'fa-times\' : showFab
                }"></i>
            </div>
        </div>
        <div class="fab-overlay ng-hide" ng-click="showFab = false" ng-show="showFab"></div>
        ';



        return $fabLinks ? $fabMenu : '';
    }
}
