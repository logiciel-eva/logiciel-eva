<?php

namespace Application\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        return new ViewModel();
    }
}
