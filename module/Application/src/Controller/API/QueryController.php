<?php

namespace Application\Controller\API;

use Application\Entity\Query;
use Core\Controller\BasicRestController;
use Laminas\View\Model\JsonModel;

class QueryController extends BasicRestController
{
    protected $entityClass = 'Application\Entity\Query';
    protected $repository  = 'Application\Entity\Query';
    protected $entityChain = 'application:e:query';

    public function create($data)
    {
        $data['owner'] = $this->authStorage()->getUser()->getId();

        return parent::create($data);
    }

    public function getList($callbacks = [], $master = false)
    {
        $response = parent::getList($callbacks, $master);
        
        return $response;
    }


    public function get($id, $callbacks = [])
    {
        $success       = false;
        $objectAsArray = [];

        /** @var Query $object */
        $object = $this->getEntityManager()->getRepository($this->repository)->find($id);

        if (
            $object &&
            $this->acl()->ownerControl($this->entityChain . ':r', $object) ||
            $this->acl()->onlyOwned('application:e:query:r') && $object->getShared() ||
            $this->params()->fromQuery('force', false)
        ) {
            if ($callbacks) {
                foreach ($callbacks as $callback) {
                    $callback($object);
                }
            }
            $objectAsArray = $this->extract($object);

            $success = true;
        }

        return new JsonModel([
            'object'  => $objectAsArray,
            'success' => $success,
        ]);
    }
}
