(function(app) {
    app.controller('natureFormController', [
        'natureService',
        'flashMessenger',
        'translator',
        function (natureService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: ['id', 'code', 'name', 'parent.id', 'parent.code', 'parent.name'],
            };

            self.loading = {
                save: false,
            };

            self.errors = {
                fields: {},
            };

            self.nature = {
                _rights: {
                    update: true,
                },
            };

            self.init = function (id) {
                if (id) {
                    natureService.find(id, apiParams).then(function(res) {
                        self.nature = res.object;
                        self.panelTitle = self.nature.name;
                    });
                }
            };

            self.saveNature = function () {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.nature.id;

                natureService.save(self.nature, apiParams).then(
                    function(res) {
                        self.nature = res.object;
                        self.panelTitle = self.nature.name;
                        self.loading.save = false;

                        if (isCreation) {
                            var url = window.location.pathname + '/' + self.nature.id;
                            history.replaceState('', '', url);
                        }

                        flashMessenger.success(translator('nature_message_saved'));
                    },
                    function(err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }

                        self.loading.save = false;
                    },
                );
            };

            self.deleteNature = function () {
                if (confirm(translator('nature_question_delete').replace('%1', self.nature.name))) {
                    natureService.remove(self.nature).then(function(res) {
                        if (res.success) {
                            window.location = '/budget/nature';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        },
    ]);
})(window.EVA.app);
