<?php

namespace Project\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ProjectRepository
 *
 * @package Project\Repository
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class ProjectRepository extends EntityRepository
{
    public function findMaxLevel()
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $queryBuilder->select('MAX(p.level)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
