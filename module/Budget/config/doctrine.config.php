<?php

namespace Budget;

return [
    'doctrine' => [
        'driver' => [
            'budget_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Budget\Entity' => 'budget_entities'
                ]
            ]
        ],
    ]
];
