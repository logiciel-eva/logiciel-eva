(function(app) {

    app.controller('templateFormController', [
        'templateService', 'flashMessenger', 'translator',
        function templateFormController(templateService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'config',
                    'default'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.template = {
                config: {
                    fields: {},
                    keywords: {},
                    custom: {},
                    workflows: {}
                },
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    templateService.find(id, apiParams).then(function (res) {
                        self.template = res.object;
                        self.panelTitle = self.template.name;

                        if (angular.isArray(self.template.config.fields)) {
                            self.template.config.fields = {};
                        }
                        if (angular.isArray(self.template.config.keywords)) {
                            self.template.config.keywords = {};
                        }
                        if (angular.isArray(self.template.config.custom)) {
                            self.template.config.custom = {};
                        }
                        if (angular.isArray(self.template.config.workflows)) {
                            self.template.config.workflows = {};
                        }
                    });
                }
            };

            self.defaultTemplate = function () {
                templateService.save(self.template, apiParams).then(function (res) {
                    if (res.success) {
                        self.template = angular.extend(self.template, res.object);

                        if (angular.isArray(self.template.config.fields)) {
                            self.template.config.fields = {};
                        }
                        if (angular.isArray(self.template.config.keywords)) {
                            self.template.config.keywords = {};
                        }
                        if (angular.isArray(self.template.config.custom)) {
                            self.template.config.custom = {};
                        }
                        if (angular.isArray(self.template.config.workflows)) {
                            self.template.config.workflows = {};
                        }

                        if (self.template.default === true) {
                            flashMessenger.success(translator('template_message_default'));
                        } else {
                            flashMessenger.success(translator('template_message_notDefault'));
                        }
                    }
                });
            };

            self.saveTemplate = function saveTemplate() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.template.id;

                templateService.save(self.template, apiParams).then(function (res) {
                    self.template = res.object;
                    self.panelTitle = self.template.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.template.id;
                        history.replaceState('', '', url);
                    }

                    if (angular.isArray(self.template.config.fields)) {
                        self.template.config.fields = {};
                    }
                    if (angular.isArray(self.template.config.keywords)) {
                        self.template.config.keywords = {};
                    }
                    if (angular.isArray(self.template.config.custom)) {
                        self.template.config.custom = {};
                    }
                    if (angular.isArray(self.template.config.workflows)) {
                        self.template.config.workflows = {};
                    }

                    flashMessenger.success(translator('template_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteTemplate = function deleteTemplate() {
                if (confirm(translator('template_question_delete').replace('%1', self.template.name))) {
                    templateService.remove(self.template).then(function (res) {
                        if (res.success) {
                            window.location = '/project/template';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ]);

})(window.EVA.app);
