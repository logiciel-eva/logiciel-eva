<?php

namespace Time\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class TimesheetController extends BasicRestController
{
    protected $entityClass  = 'Time\Entity\Timesheet';
    protected $repository   = 'Time\Entity\Timesheet';
    protected $entityChain  = 'time:e:timesheet';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        $queryBuilder->join('object.user', 'user');
        $queryBuilder->leftJoin('object.absenceType', 'absenceType');

        if ($this->serviceLocator->get('MyModuleManager')->isActive('project')) {
            $queryBuilder->leftJoin('object.project', 'project');
            $queryBuilder->leftJoin('project.parent', 'project_parent');
        }

        if ($this->serviceLocator->get('MyModuleManager')->isActive('profile')) {
            $queryBuilder->leftJoin('user.profile', 'profile');
        }

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        //$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    public function create($data)
	{
        if(isset($data["description"])){
            $decodedjson = json_decode($data["description"]);
            if(isset($decodedjson)){
                $data["description"] = $decodedjson;
            }
        }
        return parent::create($data);
    }

    public function update($id, $data){
        if(isset($data["description"])){
            $decodedjson = json_decode($data["description"]);
            if(isset($decodedjson)){
                $data["description"] = $decodedjson;
            }
        }
        return parent::update($id, $data);
    }
}
