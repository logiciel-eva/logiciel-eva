<?php

namespace Note;

return [
    'doctrine' => [
        'driver' => [
            'note_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Note\Entity' => 'note_entities'
                ]
            ]
        ]
    ]
];
