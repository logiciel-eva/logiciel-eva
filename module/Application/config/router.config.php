<?php

namespace Application;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'root' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
            ],
            'configuration' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/configuration',
                    'defaults' => [
                        'controller' => 'Application\Controller\Configuration',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
            ],
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'configuration' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/configuration[/:id]',
                            'defaults' => [
                                'controller' => 'Application\Controller\API\Configuration'
                            ]
                        ]
                    ],
                    'query' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/query[/:id]',
                            'defaults' => [
                                'controller' => 'Application\Controller\API\Query'
                            ]
                        ]
                    ],
                    'command' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/command/run/:name',
                            'defaults' => [
                                'controller' => 'Application\Controller\Command',
                                'action'     => 'run'
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class             => ServiceLocatorFactory::class,
            Controller\ConfigurationController::class     => ServiceLocatorFactory::class,
            Controller\CommandController::class           => ServiceLocatorFactory::class,
            Controller\API\ConfigurationController::class => ServiceLocatorFactory::class,
            Controller\API\QueryController::class         => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Application\Controller\Index'             => Controller\IndexController::class,
            'Application\Controller\Configuration'     => Controller\ConfigurationController::class,
            'Application\Controller\Command'           => Controller\CommandController::class,
            'Application\Controller\API\Configuration' => Controller\API\ConfigurationController::class,
            'Application\Controller\API\Query'         => Controller\API\QueryController::class
        ],
    ],
];
