<?php

/**
 * Retrieve the APP_ENV value.
 * Defined in public/.htaccess file.
 */
$env = getenv('APP_ENV') ? : 'development';

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => \Doctrine\DBAL\Driver\PDO\MySQL\Driver::class,
                'params' => [
                    'host'     => 'eva-database',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => 'toor',
                    'dbname'   => 'admin',
                    'driverOptions' => [
                        1002 => 'SET NAMES utf8'
                    ],
                ],
            ]
        ],
        'entitymanager' => [
            'orm_default' => [
                'connection'    => 'orm_default',
                'configuration' => 'orm_default',
            ]
        ],
        'configuration' => [
            'orm_default' => [
                'metadata_cache'    => ($env === 'development' ? 'array' : 'apcu'),
                'query_cache'       => ($env === 'development' ? 'array' : 'apcu'),
                'result_cache'      => ($env === 'development' ? 'array' : 'apcu'),
                'driver'            => 'orm_default',
                'generate_proxies'  => ($env === 'development'),
                'proxy_dir'         => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace'   => 'DoctrineORMModule\Proxy',
                'filters'           => []
            ]
        ],
        'driver' => [
            'orm_default' => [
                'class'   => \Doctrine\Persistence\Mapping\Driver\MappingDriverChain::class,
            ]
        ]
    ],
];

