<?php

namespace Project\Service;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class BudgetCustomExportService
{

    private $_orange;
    private $_green;
    private $_purple;
    private $_blue;

    private $_entityManager;

    private $_posteExpenseRepository;
    private $_posteIncomeRepository;


    public function __construct($entityManager)
    {
        $this->_entityManager = $entityManager;

        $this->_posteExpenseRepository = $this->_entityManager->getRepository('Budget\Entity\PosteExpense');
        $this->_posteIncomeRepository = $this->_entityManager->getRepository('Budget\Entity\PosteIncome');

        $this->_orange = new Color('00F8CBAD');
        $this->_green = new Color('00D8E4BC');
        $this->_purple = new Color('00CCC0DA');
        $this->_blue = new Color('00B8CCE4');
    }


    public function export($projects, $keywordsFamily, $expenseType, $incomeType, $financialYear = null, $nature = null)
    {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();

        // will give all the sub tables in export
        $keywords = $this->getKeywords($keywordsFamily['id']);
        $keywordsTable = [];
        $index = 1;
        foreach ($keywords as $keywordId => $keyword) {
            $keywordTable = [
                'title'     => $this->processTableTitle($keyword, $financialYear, $nature),
                'position'  => $index,
                'total'     => 0.0,     // total (incomes - expenses) of the whole table
                'expenses'  => [
                    'total'     => 0.0,     // total of the expenses of all projects
                    'accounts'  => []
                ],    // columns indexed by account id
                'incomes'   => [
                    'total'     => 0.0,     // total of the incomes of all projects
                    'envelopes' => []
                ],    // columns indexed by envelope id or autoFinancing
                'projects'  => []    // lines
            ];

            foreach ($projects as $project) {
                $projectKeywords = $this->extractKeywords($project['keywords']);
                // filter the projects according to keyword
                if (!array_key_exists($keywordId, $projectKeywords)) {
                    continue;
                }
                $projectLine = [
                    'total' => 0.0,         // total of the line (incomes - expenses)
                    'title' => $project['name'],
                    'expenses' => [
                        'total'     => 0.0, // total of the project expenses
                        'accounts'  => []
                    ],
                    'incomes' => [
                        'total'     => 0.0, // total of the project incomes
                        'envelopes' => []
                    ]
                ];

                // expenses
                $postesExpense = isset($nature)
                    ? $this->_posteExpenseRepository->findBy(['project' => $project['id'], 'nature' => $nature['id']])
                    : $this->_posteExpenseRepository->findBy(['project' => $project['id']]);
                foreach ($postesExpense as $posteExpense) {
                    if (null == $posteExpense->getAccount()) {
                        continue;
                    }
                    $amount = $this->getPosteAmount($posteExpense->getExpenses(), $expenseType, $financialYear);

                    $account = $posteExpense->getAccount();
                    // check if the account already exist in table expenses
                    if (!array_key_exists($account->getId(), $keywordTable['expenses']['accounts'])) {
                        $keywordTable['expenses']['accounts'][$account->getId()] = [
                            'code'  => $account->getCode(),
                            'title' => $account->getName(),
                            'total' => 0.0  // total of one account column
                        ];
                    }
                    $projectLine['expenses']['accounts'][$account->getId()] = $amount;
                    // compute total amounts
                    $projectLine['total'] -= $amount;   // subtract expenses
                    $projectLine['expenses']['total'] += $amount;
                    $keywordTable['expenses']['accounts'][$account->getId()]['total'] += $amount;
                    $keywordTable['expenses']['total'] += $amount;
                    $keywordTable['total'] -= $amount;
                }

                // incomes
                $postesIncome = isset($nature)
                    ? $this->_posteIncomeRepository->findBy(['project' => $project['id'], 'nature' => $nature['id']])
                    : $this->_posteIncomeRepository->findBy(['project' => $project['id']]);
                foreach ($postesIncome as $posteIncome) {
                    if (!$posteIncome->isAutofinancement() && (null === $posteIncome->getEnvelope() || null === $posteIncome->getEnvelope()->getFinancer())) {
                        continue;
                    }
                    $amount = $this->getPosteAmount($posteIncome->getIncomes(), $incomeType, $financialYear);

                    $envelopeId = null;
                    $title = null;
                    if($posteIncome->isAutofinancement()) {
                        $envelopeId = 'autoFinancing';
                        $title = 'autofinancement';
                    } else {
                        $envelopeId = $posteIncome->getEnvelope()->getId();
                        $title = $posteIncome->getEnvelope()->getFinancer()->getName();

                    }
                    if (!array_key_exists($envelopeId, $keywordTable['incomes']['envelopes'])) {
                        $keywordTable['incomes']['envelopes'][$envelopeId] = [
                            'title' => $title,
                            'total' => 0.0  // total of one envelope column
                        ];
                    }
                    $projectLine['incomes']['envelopes'][$envelopeId] = $amount;
                    // compute total amounts
                    $projectLine['total'] += $amount;   // add incomes
                    $projectLine['incomes']['total'] += $amount;
                    $keywordTable['incomes']['envelopes'][$envelopeId]['total'] += $amount;
                    $keywordTable['incomes']['total'] += $amount;
                    $keywordTable['total'] += $amount;
                }
                $keywordTable['projects'][] = $projectLine;
            }

            $keywordsTable[] = $keywordTable;
            $index++;
        }

        // turn keywords table into data to be exported (flatten)
        $metadata = [];
        $data = $this->flatten($keywordsTable, $metadata);
        $sheet->fromArray($data);

        // styling
        
        $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())->getFont()->setSize(10.0);
        $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        for ($col = 2; $col <= Coordinate::columnIndexFromString($sheet->getHighestDataColumn()); $col++) {
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($col))->setWidth(12.0);
        }
        $sheet->getStyle('A1:A' . $sheet->getHighestDataRow())->getFont()->setBold(true);
        $sheet->getStyle('A1:A' . $sheet->getHighestDataRow())->getAlignment()->setWrapText(true);
        $sheet->getColumnDimension('A')->setWidth(30.0);
        foreach ($metadata as $metadatum) {

            $expenseHeaderRange = 'B' . $metadatum['startRow'] . ':' . Coordinate::stringFromColumnIndex($metadatum['incomesStartCol'] - 1) . $metadatum['startRow'];
            $sheet->mergeCells($expenseHeaderRange);

            $incomeHeaderRange = Coordinate::stringFromColumnIndex($metadatum['incomesStartCol']) . $metadatum['startRow'] . ':' . Coordinate::stringFromColumnIndex($metadatum['endCol'] - 1) . $metadatum['startRow'];
            $sheet->mergeCells($incomeHeaderRange);

            $sheet->getStyle('B' . ($metadatum['startRow'] + 1) . ':B' . ($metadatum['startRow'] + 1))->getFont()->setBold(true);
            $sheet->getStyle(Coordinate::stringFromColumnIndex($metadatum['incomesStartCol']) . ($metadatum['startRow'] + 1) . ':' . Coordinate::stringFromColumnIndex($metadatum['incomesStartCol']) . ($metadatum['startRow'] + 1))->getFont()->setBold(true);
            if($metadatum['hasAutoFinancingCol']) {
                $sheet->getStyle(Coordinate::stringFromColumnIndex($metadatum['autoFinancingStartCol']) . ($metadatum['startRow'] + 1) . ':' . Coordinate::stringFromColumnIndex($metadatum['autoFinancingStartCol']) . ($metadatum['startRow'] + 1))->getFont()->setBold(true);
            }

            $subHeaderRange = 'A' . ($metadatum['startRow'] + 1) . ':' . Coordinate::stringFromColumnIndex($metadatum['endCol']) . ($metadatum['startRow'] + 1);
            $sheet->getStyle($subHeaderRange)->getFont()->setItalic(true);
            $sheet->getStyle($subHeaderRange)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $range = Coordinate::stringFromColumnIndex($metadatum['startCol']) . $metadatum['startRow'] . ':' . Coordinate::stringFromColumnIndex($metadatum['endCol']) . $metadatum['endRow'];
            $sheet->getStyle($range)->getBorders()->getVertical()->setBorderStyle(Border::BORDER_THIN);
            $sheet->getStyle($range)->getBorders()->getHorizontal()->setBorderStyle(Border::BORDER_THIN);
            $sheet->getStyle($range)->getBorders()->getRight()->setBorderStyle(Border::BORDER_MEDIUM);
            $sheet->getStyle($range)->getBorders()->getLeft()->setBorderStyle(Border::BORDER_MEDIUM);
            $sheet->getStyle($range)->getBorders()->getBottom()->setBorderStyle(Border::BORDER_MEDIUM);
            $sheet->getStyle($range)->getBorders()->getTop()->setBorderStyle(Border::BORDER_MEDIUM);

            $wrapRange = 'A' . $metadatum['startRow'] . ':' . $sheet->getHighestDataColumn() . ($metadatum['startRow'] + 1);
            $sheet->getStyle($wrapRange)->getAlignment()->setWrapText(true);
            

            $formatingRange = 'B' . ($metadatum['startRow'] + 2) . ':' . $sheet->getHighestDataColumn() . $metadatum['endRow'];
            $sheet->getStyle($formatingRange)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            $sheet->getStyle($formatingRange)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $sheet->getStyle($formatingRange)->getFont()->setItalic(true);

            $headerWrap = 'B' . $metadatum['startRow'] . ':' . Coordinate::stringFromColumnIndex($metadatum['endCol']) . $metadatum['startRow'];
            $sheet->getStyle($headerWrap)->getFont()->setBold(true);
            $sheet->getStyle($headerWrap)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);


            $blueRange = Coordinate::stringFromColumnIndex($metadatum['expensesStartCol']) . ($metadatum['startRow'] + 1) . ':' . Coordinate::stringFromColumnIndex($metadatum['expensesStartCol']) . $metadatum['endRow'];
            $sheet->getStyle($blueRange)->getFill()->setFillType(Fill::FILL_SOLID)->setStartColor($this->_blue)->setEndColor($this->_blue);

            $purpleRange = Coordinate::stringFromColumnIndex($metadatum['incomesStartCol']) . ($metadatum['startRow'] + 1) . ':' . Coordinate::stringFromColumnIndex($metadatum['incomesStartCol']) . $metadatum['endRow'];
            $sheet->getStyle($purpleRange)->getFill()->setFillType(Fill::FILL_SOLID)->setStartColor($this->_purple)->setEndColor($this->_purple);

            if($metadatum['hasAutoFinancingCol']) {
                $greenRange = Coordinate::stringFromColumnIndex($metadatum['autoFinancingStartCol']) . ($metadatum['startRow'] + 1) . ':' . Coordinate::stringFromColumnIndex($metadatum['autoFinancingStartCol']) . $metadatum['endRow'];
                $sheet->getStyle($greenRange)->getFill()->setFillType(Fill::FILL_SOLID)->setStartColor($this->_green)->setEndColor($this->_green);
            }
            
            $orangeRange = Coordinate::stringFromColumnIndex($metadatum['endCol']) . ($metadatum['startRow'] + 1) . ':' . Coordinate::stringFromColumnIndex($metadatum['endCol']) . $metadatum['endRow'];
            $sheet->getStyle($orangeRange)->getFill()->setFillType(Fill::FILL_SOLID)->setStartColor($this->_orange)->setEndColor($this->_orange);
        }



        /**
         * Generates the Excel from the datas
         */
        $objWriter = IOFactory::createWriter($spreadsheet, 'Xlsx');

        /**
         * Return the file in the output
         */
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
        header('Content-Disposition: attachment; filename="export_budget_detaille.xlsx"');
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        ob_end_clean();
        $objWriter->save('php://output');
        exit();
    }



    private function flatten($keywordsTable, &$metadata)
    {
        if (empty($keywordsTable)) {
            return [];
        }
        $data = [];
        foreach ($keywordsTable as $keywordTable) {
            $data = array_merge($data, $this->flattenTable($keywordTable, $metadata));
            // line breaks between tables
            $data[] = [];
            $data[] = [];
        }
        return $data;
    }

    private function flattenTable($keywordTable, &$metadata)
    {
        // sort incomes, so that auto financing column is the first one
        uksort($keywordTable['incomes']['envelopes'], function($id1, $id2) {
            if($id1 === 'autoFinancing') {
                return -1;
            } else if($id2 === 'autoFinancing') {
                return 1;
            }
            return ($id1 > $id2) ? 1 : -1;
        });

        $data = [];

        // HEADER line
        $header = [$keywordTable['title'], 'DEPENSES'];
        for ($i = 0; $i < count($keywordTable['expenses']['accounts']); $i++) {
            $header[] = null;
        }
        $header[] = 'RECETTES';
        for ($i = 0; $i < count($keywordTable['incomes']['envelopes']); $i++) {
            $header[] = null;
        }
        $header[] = 'solde à' . "\n" . 'financer';
        $data[] = $header;

        // SUB HEADER line
        $subHeader = [null, 'Total' . "\n" . 'dépenses'];
        foreach ($keywordTable['expenses']['accounts'] as $account) {
            $subHeader[] = $account['code'] . "\n" . $account['title'];
        }
        $subHeader[] = 'Total' . "\n" . 'recettes';
        foreach ($keywordTable['incomes']['envelopes'] as $envelope) {
            $subHeader[] = $envelope['title'];
        }
        $subHeader[] = null;
        $data[] = $subHeader;

        // PROJECTS lines
        foreach ($keywordTable['projects'] as $project) {
            $line = [$project['title'], $this->getExcelAmount($project['expenses']['total'])];
            foreach ($keywordTable['expenses']['accounts'] as $accountId => $account) {
                if(array_key_exists($accountId, $project['expenses']['accounts'])) {
                    $line[] = $this->getExcelAmount($project['expenses']['accounts'][$accountId]);
                } else {
                    $line[] = null;
                }
            }
            $line[] = $this->getExcelAmount($project['incomes']['total']);
            foreach ($keywordTable['incomes']['envelopes'] as $envelopeId => $envelope) {
                if(array_key_exists($envelopeId, $project['incomes']['envelopes'])) {
                    $line[] = $this->getExcelAmount($project['incomes']['envelopes'][$envelopeId]);
                } else {
                    $line[] = null;
                }
            }
            $line[] = $this->getExcelAmount($project['total']);
            $data[] = $line;
        }


        // TOTAL line
        $total = ['TOTAL', $this->getExcelAmount($keywordTable['expenses']['total'])];
        foreach ($keywordTable['expenses']['accounts'] as $account) {
            $total[] = $this->getExcelAmount($account['total']);
        }
        $total[] = $this->getExcelAmount($keywordTable['incomes']['total']);
        foreach ($keywordTable['incomes']['envelopes'] as $envelope) {
            $total[] = $this->getExcelAmount($envelope['total']);
        }
        $total[] = $this->getExcelAmount($keywordTable['total']);
        $data[] = $total;


        $latestTableMetadata = end($metadata);
        if ($latestTableMetadata) {
            $startRow = ($latestTableMetadata['endRow'] + 3);
        } else {
            $startRow = 1;
        }
        reset($metadata);
        $metadata[] = [
            'startRow' => $startRow,
            'endRow' => $startRow + count($keywordTable['projects']) + 2,
            'startCol' => 1,
            'expensesStartCol' => 2,
            'incomesStartCol' => 2 + count($keywordTable['expenses']['accounts']) + 1,
            'hasAutoFinancingCol' => array_key_exists('autoFinancing', $keywordTable['incomes']['envelopes']),
            'autoFinancingStartCol' => 2 + count($keywordTable['expenses']['accounts']) + 2,
            'endCol' => 1 + 1 + count($keywordTable['expenses']['accounts']) + 1 + count($keywordTable['incomes']['envelopes']) + 1
        ];

        return $data;
    }

    private function getExcelAmount($amount)
    {
        return number_format($amount, 2, '.', ',') . ' €';
    }


    private function getKeywords($groupId)
    {
        $group = $this->_entityManager->getRepository('Keyword\Entity\Group')->findOneBy(['id' => $groupId]);
        if (!isset($group)) {
            return [];
        }
        $keywordsName = [];
        foreach ($group->getKeywords() as $keyword) {
            $keywordsName[$keyword->getId()] = $keyword->getName();
        }
        return $keywordsName;
    }

    private function extractKeywords($keywords)
    {
        if (empty($keywords)) {
            return [];
        }
        $keywordsName = [];
        foreach ($keywords as $keywordArray) {
            foreach ($keywordArray as $keyword) {
                $keywordsName[$keyword['id']] = $keyword['name'];
            }
        }
        return $keywordsName;
    }

    private function processTableTitle($keyword, $financialYear, $nature)
    {
        $tableTitle = $keyword;
        if (isset($financialYear)) {
            $tableTitle .= "\n" . 'Exercice ' . $financialYear;
        }
        if (isset($nature)) {
            $tableTitle .= ' - ' . $nature['name'];
        }
        return $tableTitle;
    }

    private function getPosteAmount($transactions, $type, $financialYear = null)
    {
        if(empty($transactions)) {
            return 0.0;
        }
        $filteredTransactions = $transactions->filter(function ($transaction) use ($type, $financialYear) {            
            return $transaction->getType() === $type && $transaction->isFromFinancialYear($financialYear);
        });

        $totalTransactionsAmount = 0.0;
        foreach($filteredTransactions as $transaction) {
            $totalTransactionsAmount += $transaction->getAmount();
        }
        return $totalTransactionsAmount;
    }

}
