(function(app) {

    app.controller('mapController', [
        '$scope', '$http', 'leafletData', '$timeout', '$q', 'queryService', '$rootScope', 'controlService',
        function mapController($scope, $http, leafletData, $timeout, $q, queryService, $rootScope, controlService) {
            controlService.desactivate();

            var self = this;
            self.isExport = false;
            self.isFistLoad = true;
            self.nbFilters = 1;


            self.loading = false;
            self.loadings = {
                color: false,
                details: false,
                proportional: false
            };
            self.filters = typeof filters !== 'undefined' ? filters : {
                project: {},
                territory: {},
                showAllTerritories: false
            };
            self.configuration = typeof configuration !== 'undefined' ? configuration : {
                colored: {
                    type: 'color',
                    data: [],
                    color: {
                        min: '#FF0000',
                        max: '#00FF00'
                    },
                    distribution: 'constant_amplitude',
                    classes: '5',
                    year: null
                },
                details: {
                    type: 'details',
                    data: null,
                    territory: null
                },
                proportional: {
                    type: 'proportional',
                    data: null,
                    year: null
                }
            };

            self.detailsChart1 = {
                api: null
            };
            self.detailsChart2 = {
                options: {
                    chart: {
                        type         : 'multiBarChart',
                        showControls : false,
                        showValues   : true,
                        stacked      : true,
                        height       : 400,
                        labelsOutside: true,
                    }
                },
                data: [],
                api: null
            };

            /*
            self.detailsChart = {
                options: {
                    chart: {
                        type: 'multiChart',
                        height: 400,
                        yAxis1: {
                            tickFormat: function(d){
                                return d3.format(',.1f')(d);
                            }
                        },
                        yAxis2: {
                            tickFormat: function(d){
                                return d3.format(',.1f')(d);
                            }
                        },
                    }
                },
                data: [
                    {
                        type: 'bar',
                        key: 'test3',
                        yAxis: 1,
                        values: [{x: 0, y: 10}, {x: 1, y: 23}, {x: 2, y: 48}]
                    },
                    {
                        type: 'bar',
                        key: 'test',
                        yAxis: 1,
                        values: [{x: 0, y: 10}, {x: 1, y: 23}, {x: 2, y: 48}]
                    },
                    {
                        type: 'line',
                        key: 'test2',
                        yAxis: 2,
                        values: [{x: 0, y: 10}, {x: 1, y: 23}, {x: 2, y: 48}]
                    }
                ],
                api: null
            };
             */

            self.proportionalConfig = typeof proportionalConfig !== 'undefined' ? proportionalConfig : {
                minRadius: 10,
                maxRadius: 30,
                fillColor: '#FF0000',
                color: '#252525'
            };

            self.showPanelLeft = true;
            self.showPanelRight = false;

            self.allTerritories = [];

            self.applyFilters = function applyFilters() {
                self.loading = true;
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.territory).length;

                $http({
                    url: '/analysis/map/territories?' + $.param({filters: !self.filters ? {} : self.filters}),
                    method: 'GET'
                }).then(function (res) {
                    var territories = res.data;

                    if (self.isFistLoad) {
                        self.allTerritories = angular.copy(res.data);
                    }

                    leafletData.getMap('map').then(function(map) {
                        self.removeTerritories();

                        if (self.filters.showAllTerritories) {
                            for (var i in self.allTerritories) {
                                self.addTerritory(self.allTerritories[i]);
                            }
                        }

                        for (i in territories) {
                            self.addTerritory(territories[i]);
                        }

                        self.fitTerritories();

                        if (!isFirst) {
                            var a = self.applyColoredData();
                            self.applyDetailsData();
                            var b = self.applyProportionalData();

                            $q.all([a, b]).then(function() {
                                $timeout(function() {
                                    $('#phantom').text('ok');
                                })
                            });
                        }

                        if (self.isFistLoad && self.isExport) {
                            self.applyFilters();
                        }
                    });


                    self.loading = false;
                    self.isFistLoad = false;
                });
            };

            self.resetFilters = function() {
                self.filters.project = {};
                self.filters.territory = {};
                self.applyFilters();
            };

            self.init = function init(isMaster, isExport) {
                if (!isExport) {
                    self.initQueries();
                }
                self.createDetailsChart();

                self.isExport = isExport;
                $scope.isMaster = isMaster;
            };

            self.togglePanelLeft = function togglePanelLeft() {
                leafletData.getMap('map').then(function(map) {
                    var center = map.getCenter();
                    self.showPanelLeft = !self.showPanelLeft;

                    $timeout(function() {
                        map.invalidateSize();
                        map.setView(center);
                        self.detailsChart1.api.refresh();
                        self.detailsChart2.api.refresh();
                    });
                });
            };

            self.togglePanelRight = function togglePanelRight(how) {
                leafletData.getMap('map').then(function(map) {
                    var center = map.getCenter();
                    if (how === true) {
                        self.showPanelRight = true;
                    } else if (how === false) {
                        self.showPanelRight = false;
                    } else {
                        self.showPanelRight = !self.showPanelRight;
                    }

                    $timeout(function() {
                        map.invalidateSize();
                        map.setView(center);
                        self.detailsChart1.api.refresh();
                        self.detailsChart2.api.refresh();
                    });
                });
            };

            self.applyColoredData = function applyColoredData() {
                var deferred = $q.defer();

                if (self.configuration.colored.data !== null && self.configuration.colored.data.length > 0) {
                    self.loadings.color = true;
                    $http({
                        url: '/analysis/map/data?' + $.param({filters: self.filters, configuration: self.configuration.colored}),
                        method: 'GET'
                    }).then(function (res) {
                        self.dict.colorLegend = [];
                        for (var i = 0; i < res.data.distribution.length - 1; i++) {
                            self.dict.colorLegend.push({
                                min: Math.round(res.data.distribution[i]),
                                max: Math.round(res.data.distribution[i + 1]),
                                color: '#' + res.data.colors[i]
                            });
                        }

                        for (var j in res.data.data) {
                            var row = res.data.data[j];
                            self.dict.territories[row.id].options.data.colored.data = row.data;
                            self.setColorFromColoredData(self.dict.territories[row.id]);
                        }

                        self.loadings.color = false;
                        $scope.colorLegend = true;

                        self.applyDetailsData();

                        deferred.resolve();
                    });
                } else {
                    deferred.resolve();
                }

                return deferred.promise;
            };

            self.applyDetailsData = function applyDetailsData() {
                if (self.configuration.details.territory) {
                    self.loading.details = true;

                    self.configuration.details.data = [self.configuration.colored.data[0], self.configuration.proportional.data];

                    $http({
                        url: '/analysis/map/data?' + $.param({filters: self.filters, configuration: self.configuration.details}),
                        method: 'GET'
                    }).then(function (res) {

                        var charts = [{key: null, values: [], direct: false}, {key: null, values: [], direct: false}];
                        for (var i in res.data.data) {
                            charts[i].key    = res.data.data[i].key;
                            charts[i].direct = res.data.data[i].direct;
                            charts[i].base   = res.data.data[i].years;
                            for (var j in res.data.years) {
                                var year = res.data.years[j];
                                charts[i].values.push({
                                    x: year,
                                    y: (typeof res.data.data[i].years[year] !== 'undefined' ? res.data.data[i].years[year].value : null),
                                });
                            }
                        }

                        self.createDetailsChart(charts[0], charts[1]);
                    });
                }
            };

            self.createDetailsChart = function (c1, c2) {
                self.detailsChart1 = {
                    options: {
                        chart: {
                            type: 'multiChart',
                            showControls : false,
                            showValues   : true,
                            height       : 400,
                            labelsOutside: true,
                            legend: {
                                rightAlign: false, align: false
                            }
                        }
                    },
                    data: [],
                    api: self.detailsChart1.api
                };

                if (c1) {
                    if (c1.values.length > 0) {
                        self.detailsChart1.data.push({
                            color: '#7488e2',
                            type: 'line',
                            key: c1.key,
                            yAxis: 1,
                            values: c1.values,
                            direct: c1.direct,
                            base: c1.base
                        })
                    }
                }

                self.detailsChart2.data = [];
                if (c2) {
                    if (c2.values.length > 0) {
                        self.detailsChart1.data.push({
                            color: '#f7a636',
                            type: 'bar',
                            key: c2.key,
                            yAxis: 2,
                            values: c2.values,
                            direct: c2.direct,
                            base: c2.base
                        });

                        if (!c2.direct) {
                            var series = {};
                            for (var year in c2.base) {
                                for (var i in c2.base[year].stacks) {
                                    var project = c2.base[year].stacks[i].k;
                                    var value   = c2.base[year].stacks[i].v;

                                    if (typeof series[project] === 'undefined') {
                                        series[project] = {
                                            key: project,
                                            values: []
                                        }
                                    }

                                    series[project].values.push({
                                        x: year,
                                        y: value
                                    })
                                }
                            }
                            self.detailsChart2.key  = c2.key;
                            self.detailsChart2.data = Object.values(series);
                        }
                    }
                }
                $timeout(function () {
                    self.detailsChart1.api.refresh();
                    self.detailsChart1.api.refresh();
                    self.detailsChart2.api.refresh();
                });
            };

            self.switchDetailsCharts = function () {
                var kBar = null;
                var kLine = null;
                var vBar = [];
                var vLine = [];
                var dLine = false;
                var dBar  = false;
                var bBar = null;
                var bLine = null;

                for (var i in self.detailsChart1.data) {
                    switch(self.detailsChart1.data[i].type) {
                        case 'bar':
                            kBar = angular.copy(self.detailsChart1.data[i].originalKey);
                            vBar = angular.copy(self.detailsChart1.data[i].values);
                            dBar = angular.copy(self.detailsChart1.data[i].direct);
                            bBar = angular.copy(self.detailsChart1.data[i].base);
                            break;
                        case 'line':
                            kLine = angular.copy(self.detailsChart1.data[i].originalKey);
                            vLine = angular.copy(self.detailsChart1.data[i].values);
                            dLine = angular.copy(self.detailsChart1.data[i].direct);
                            bLine = angular.copy(self.detailsChart1.data[i].base);
                            break;
                    }
                }

                self.createDetailsChart({key: kBar, values: vBar, direct: dBar, base: bBar}, {key: kLine, values: vLine, direct: dLine, base: bLine});
            };

            self.applyProportionalData = function applyProportionalData() {
                var deferred = $q.defer();

                if (self.configuration.proportional.data !== null) {
                    self.loadings.proportional = true;
                    $http({
                        url: '/analysis/map/data?' + $.param({filters: self.filters, configuration: self.configuration.proportional}),
                        method: 'GET'
                    }).then(function (res) {
                        self.removePies();

                        if (res.data.min == res.data.max || res.data.min == 0) {
                            res.data.min = 1;
                        }

                        self.dict.propLegend = {
                            min: Math.round(res.data.min * 100) / 100,
                            max: Math.round(res.data.max * 100) / 100
                        };

                        leafletData.getMap('map').then(function(map) {
                            for (var i in res.data.data) {
                                var row = res.data.data[i];
                                var territory = self.dict.territories[row.id];

                                var options = {
                                    data: {},
                                    chartOptions: {},
                                    radius: self.scaleBetween(row.total, self.proportionalConfig.minRadius, self.proportionalConfig.maxRadius, res.data.min, res.data.max),
                                    fillOpacity: 0.6
                                };

                                if (row.data.length > 0) {
                                    for (var j in row.data) {
                                        var data = row.data[j];
                                        if (data.data !== null && data.data !== 0) {
                                            options.data[data.project.name] = data.data;
                                            options.chartOptions[data.project.name] = {
                                                color: self.proportionalConfig.color,
                                                fillColor: self.proportionalConfig.fillColor
                                            };
                                        }
                                    }

                                    var bounds = territory.getBounds();
                                    if (bounds.isValid()) {
                                        self.addPie(bounds.getCenter(), options);
                                    }
                                }
                            }

                            self.loadings.proportional = false;
                            $scope.propLegend = true;

                            deferred.resolve();
                            self.applyDetailsData();
                        });
                    });
                } else {
                    deferred.resolve();
                }

                return deferred.promise;
            };

            self.scaleBetween = function scaleBetween(unscaledNum, minAllowed, maxAllowed, min, max) {
                return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
            };

            $scope.$watch(function() { return self.proportionalConfig.fillColor }, function () {
                self.changeProportionalColor(self.proportionalConfig.fillColor);
            });

            self.changeProportionalColor = function changeProportionalColor(color) {
                for (var i in self.layers.pies._layers) {
                    var pie = self.layers.pies._layers[i];
                    pie.options.fillColor = color;
                    for (var l in pie.options.chartOptions) {
                        pie.options.chartOptions[l].fillColor = color;
                    }

                    pie.redraw();
                }
            };

            // QUERIES
            self.queries          = [];
            self.query            = {
                list: 'analysis-map'
            };
            self.errors           = {
                fields: {}
            };
            self.loadingQueries   = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries        = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters    = self.filters;

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };

            /***************** MANAGE MAP ****************/

            self.options = {
                tileLayer: 'https://api.mapbox.com/styles/v1/jb-siter/cixrdm33m001b2smw1w3ad4pg/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiamItc2l0ZXIiLCJhIjoiSVNyaTkwOCJ9.aZ-ShCPXp8mlUusn8flEDQ'
            };
            self.currentFit    = null;
            self.selectedLayer = null;
            self.styles = {
                default: {
                    fillColor: '#939393',
                    color: '#6d6d6d'
                },
                highlight: {
                    fillColor: '#f4ff84',
                    color: '#cdd66d'
                },
                selected: {
                    fillColor: '#f4ff84',
                    color: '#cdd66d'
                }
            };
            self.layers = {
                territories: new L.featureGroup(),
                pies: new L.featureGroup()
            };
            self.dict = {
                colorLegend: [],
                propLegend: {},
                territories: {}
            };

            leafletData.getMap('map').then(function(map) {
                self.layers.territories.addTo(map);
                self.layers.pies.addTo(map);

                // @see https://github.com/rowanwins/leaflet-easyPrint
                self.printPlugin = L.easyPrint({
                    title: 'Cartographie',
                    position: 'topright',
                    exportOnly: true, // false = pdf
                    sizeModes: ['A4Portrait', 'A4Landscape']
                }).addTo(map);
            });

            self.addTerritory = function addTerritory(data) {
                var layer = L.geoJson(data.json, {
                    weight: 1,
                    fillOpacity: 0.5,
                    data: {
                        id: data.id,
                        name: data.name,
                        code: data.code,
                        colored: {
                            data: null,
                            color: null
                        }
                    },
                    coordinates: [],
                    onEachFeature: function (feature, layer) {
                        //this.coordinates.push(feature.geometry.coordinates);
                    }
                });

                layer.setStyle(self.styles.default);

                layer.on({
                    mouseover: function (e) {
                        var layer = e.target;
                        layer.setStyle(self.styles.highlight);
                    },
                    mouseout: function (e) {
                        var layer = e.target;
                        if (self.selectedLayer !== layer) {
                            layer.setStyle(self.styles.default);
                            if (layer.options.data.colored.data !== null) {
                                layer.setStyle({
                                    fillColor: layer.options.data.colored.color
                                });
                            }
                        }
                    },
                    click: function (e) {
                        if (self.selectedLayer !== null) {
                            self.selectedLayer.setStyle(self.styles.default);
                            if (self.selectedLayer.options.data.colored.data !== null) {
                                self.selectedLayer.setStyle({
                                    fillColor: self.selectedLayer.options.data.colored.color
                                });
                            }
                        }

                        if (self.selectedLayer !== e.target) {
                            self.selectedLayer = e.target;
                            self.selectedLayer.setStyle(self.styles.selected);
                            self.togglePanelRight(true);
                            /*leafletData.getMap('map').then(function(map) {
                                map.fitBounds(self.selectedLayer.getBounds());
                            });*/

                            self.configuration.details.territory = self.selectedLayer.options.data.id;
                            self.applyDetailsData();
                        } else {
                            self.selectedLayer = null;
                        }
                    }
                });

                self.layers.territories.addLayer(layer);
                self.dict.territories[data.id] = layer;
            };

            self.addPie = function addPie(center, options) {
                var layer = new L.PieChartMarker(center, options);
                self.layers.pies.addLayer(layer);
            };

            self.removeTerritories = function removeTerritories() {
                for (var i in self.layers.territories._layers) {
                    var layer = self.layers.territories._layers[i];
                    self.removeTerritory(layer);
                }
            };

            self.removePies = function removePies() {
                for (var i in self.layers.pies._layers) {
                    var layer = self.layers.pies._layers[i];
                    self.removePie(layer);
                }
            };

            self.removeTerritory = function removeTerritory(layer) {
                self.layers.territories.removeLayer(layer);
                delete self.dict.territories[layer.options.data.id];
            };

            self.removePie = function removePie(layer) {
                self.layers.pies.removeLayer(layer);
            };

            self.fitTerritories = function fitTerritories() {
                leafletData.getMap('map').then(function(map) {
                    map.fitBounds(self.layers.territories.getBounds());
                });

                self.currentFit = 'territories';
            };

            self.setColorFromColoredData = function setColorFromData(layer) {
                var data = layer.options.data.colored.data;
                var color = null;

                if (data !== null) {
                    for (var i in self.dict.colorLegend) {
                        var step = self.dict.colorLegend[i];
                        if (data >= step.min && data < step.max) {
                            color = step.color;
                            break;
                        }

                        color = step.color;
                    }

                    layer.options.data.colored.color = color;

                    layer.setStyle({
                        fillColor: color
                    });
                } else {
                    layer.setStyle({
                        fillColor: self.styles.default.color
                    });
                }
            };
        }
    ]);

    app.controller('filterController', [
        '$scope',
        function ($scope) {
            var self = this;

            self.filtersKey = null;

            self.init = function (filtersKey) {
                self.filtersKey = filtersKey;
            };

            var events = [
                'analysis-map-query-loaded'
            ];

            events.forEach(function (event) {
                $scope.$on(event, function (e, filters) {
                    showFilters(filters[self.filtersKey]);
                });
            });

            function showFilters(filters) {
                $scope.__filters = {};

                for (var i in filters) {
                    if (
                        filters[i] !== null &&
                        (
                            typeof filters[i].val !== 'undefined' ||
                            filters[i].op === 'isNull'
                        )
                    ) {
                        $scope.__filters[i] = true;
                    } else {
                        $scope.__filters[i] = false;
                    }
                }
            }
        }
    ]);
})(window.EVA.app);
