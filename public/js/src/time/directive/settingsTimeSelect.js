(function(app) {

    app.directive('settingsTimeSelect', [
        'settingsTimeService', 'selectFactoryService',
        function settingsTimeSelect(settingsTimeService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: settingsTimeService,
                    apiParams: {
                        col: ['id', 'name', 'hours'],
                        search : {
                            data : {
                                sort: 'name',
                                order: 'asc'
                            }
                        }
                    },
                    tree: false,
                    nnTree: false
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name', 'hours']
                }
            });
        }
    ])

})(window.EVA.app);
