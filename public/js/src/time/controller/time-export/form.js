(function (app) {

    app.controller('timeExportFormController', [
        'timeExportService', '$scope', 'flashMessenger', 'translator',
        function timeExportFormController(timeExportService, $scope, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'orientation',
                    'hoursByDay',
                    'cost',
                    'totalUnit',
                    'groupByUnit',
                    'sortByProperty',
                    'addMissingDays',
                    'completeMonths',
                    'allYear'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.timeExport = {
                _rights  : {
                    update: true
                }
            };

            self.init = function (id) {
                if (id) {
                    timeExportService.find(id, apiParams).then(function (res) {
                        self.timeExport = res.object;
                        self.panelTitle = self.timeExport.name;
                    });
                }
            };

            self.saveTimeExport = function () {
                self.errors.fields = {};
                self.loading.save  = true;

                var isCreation = !self.timeExport.id;

                timeExportService.save(self.timeExport, apiParams).then(function (res) {
                    self.timeExport   = res.object;
                    self.panelTitle   = self.timeExport.name;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.timeExport.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('time_export_message_saved'));
                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                })
            };

            self.deleteTimeExport = function () {
                if (confirm(translator('time_export_question_delete').replace('%1', self.timeExport.name))) {
                    timeExportService.remove(self.timeExport).then(function (res) {
                        if (res.success) {
                            window.location = '/time/export';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
