<?php

namespace Directory\Controller;

use Core\Controller\AbstractActionSLController;
use Directory\Entity\Structure;
use Doctrine\ORM\EntityManager;
use Keyword\Entity\Association;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class StructureController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('directory:e:structure:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups =$this->allowedKeywordGroups('structure', false);
        }

        return new ViewModel([
            'groups' => $groups,
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);
        if (!$this->acl()->basicFormAccess('directory:e:structure', $id)) {
            return $this->notFoundAction();
        }

        $structure = $this->entityManager()->getRepository('Directory\Entity\Structure')->find($id);

        if ($structure &&
            !$this->acl()->ownerControl('directory:e:structure:u', $structure) &&
            !$this->acl()->ownerControl('directory:e:structure:r', $structure)
        ) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('contactJobStructure', false);
        }

        return new ViewModel([
            'structure' => $structure,
            'groups' => $groups
        ]);
    }

    /**
     * @return JsonModel
     */
    public function duplicateAction()
    {
        $success = true;
        $errors  = [];
        $newId   = null;

        try {
            $id = $this->params()->fromRoute('id', false);
            /* @var EntityManager $em */
            $em = $this->entityManager();

            /* @var Structure $structure  */
            $structure = $em->getRepository('Directory\Entity\Structure')->find($id);

            $newStructure = clone $structure;
            $newStructure->setCreatedAt(new \DateTime());
            $newStructure->setCreatedBy($this->authStorage()->getUser());

            $em->persist($newStructure);
            $em->flush();

            /**
             * Get all keywords of the structure
             * @var Association[] $associations
             */
            $associations = $em->getRepository('Keyword\Entity\Association')->findBy([
                'entity'  => 'Directory\Entity\Structure',
                'primary' => $structure->getId(),
            ]);

            foreach ($associations as $association) {
                $newAssociation = clone $association;
                $newAssociation->setPrimary($newStructure->getId());
                $em->persist($newAssociation);
            }

            $em->flush();

            $newId = $newStructure->getId();
        } catch (\Exception $e) {
            $success  = false;
            $errors[] =  $e->getMessage();
        } finally {
            return new JsonModel([
                'success' => $success,
                'errors'  => $errors,
                'structure' => [
                    'id' => $newId,
                ],
            ]);
        }
    }
}
