(app => {
	app.controller('expenseController', [
		'queryService',
		'$rootScope',
		'$scope',
		'translator',
		'$http',
		'flashMessenger',
		app.expenseControllerFactory('analysis-budget-expense', 'exportModalExpense', false),
	]);
})(window.EVA.app);
