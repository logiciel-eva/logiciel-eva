<?php

namespace Age;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'controllers' => [
        'factories' => [
            Command\ImportAge::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
        ],
    ],
];
