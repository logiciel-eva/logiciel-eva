<?php

namespace Directory\Controller\API;

use Core\Controller\BasicRestController;
use Directory\Entity\Contact;
use Directory\Entity\ContactJobStructure;
use Doctrine\ORM\QueryBuilder;
use Keyword\Entity\Association;
use Laminas\View\Model\JsonModel;

class ContactController extends BasicRestController
{
    protected $entityClass = 'Directory\Entity\Contact';
    protected $repository  = 'Directory\Entity\Contact';
    protected $entityChain = 'directory:e:contact';

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.lastName';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->leftJoin('object.contactEmails', 'contactEmails');

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                    'object.firstName LIKE :f_full',
                    'object.lastName LIKE :f_full',
                    'object.email LIKE :f_full',
                    'object.phone LIKE :f_full',
                    'object.portable LIKE :f_full',
                    'object.zipcode LIKE :f_full',
                    'object.country LIKE :f_full',
                    'object.city LIKE :f_full',
                    'object.addressLine1 LIKE :f_full',
                    'object.addressLine2 LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }

        if (isset($data['filters'])) {
            foreach ($data['filters'] as $property => $filter) {
                if ($property == 'structure' && isset($filter['op'])) {
                    $operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
                    $queryBuilder->andWhere('
                        object.id ' . $operator . ' (SELECT c1.id FROM Directory\Entity\ContactJobStructure cjs1 JOIN cjs1.structure s1 JOIN cjs1.contact c1 WHERE s1.id IN (:structure) )
                    ');
                    if (is_array($filter['val'])) {
                        $queryBuilder->setParameter('structure', $filter['val'], 'simple_array');
                    } else {
                        $queryBuilder->setParameter('structure', $filter['val']);
                    }
                    unset($data['filters'][$property]);
                    continue;
                }
                if ($property == 'job' && isset($filter['op'])) {
                    $operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
                    $queryBuilder->andWhere('
                        object.id ' . $operator . ' (SELECT c2.id FROM Directory\Entity\ContactJobStructure cjs2 JOIN cjs2.job j2 JOIN cjs2.contact c2 WHERE j2.id IN (:job) )
                    ');
                    if (is_array($filter['val'])) {
                        $queryBuilder->setParameter('job', $filter['val'], 'simple_array');
                    } else {
                        $queryBuilder->setParameter('job', $filter['val']);
                    }
                    unset($data['filters'][$property]);
                    continue;
                }                
                if ($property == 'contactEmails' && isset($filter['op'])) {
                    $operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
                    $queryBuilder->andWhere('
                        object.id ' . $operator . ' (
                            SELECT c4.id
                            FROM Directory\Entity\ContactEmail ce4
                            JOIN ce4.contact c4
                            WHERE ce4.id IN (:contactEmails)
                        )
                    ');
                    if (is_array($filter['val'])) {
                        $queryBuilder->setParameter('contactEmails', $filter['val'], 'simple_array');
                    } else {
                        $queryBuilder->setParameter('contactEmails', $filter['val']);
                    }
                    unset($data['filters'][$property]);
                    continue;
                }
            }
        }
    }

    public function create($data)
    {
        $this->uploadAvatar($data);
        return parent::create($data);
    }

    public function update($id, $data)
    {
        if ($this->acl()->getUser()) {
            /** @var Contact $contact */
            $contact = $this
                ->getEntityManager()
                ->getRepository('Directory\Entity\Contact')
                ->findOneBy(['uuid' => $id]);

            if (!$contact) {
                $contact = $this
                    ->getEntityManager()
                    ->getRepository('Directory\Entity\Contact')
                    ->find($id);
            }

            if (!$contact) {
                return parent::update($id, $data);
            }

            $this->uploadAvatar($data);
            return parent::update($contact->getId(), $data);
        }

        $objectAsArray = [];
        $success       = false;
        $errors        = [
            'fields'     => [],
            'exceptions' => [],
        ];

        try {
            $object = $this
                ->getEntityManager()
                ->getRepository($this->repository)
                ->findOneBy(['uuid' => $id]);

            if ($object) {
                $this->uploadAvatar($data);
                $inputFilter = $object->getInputFilterPublic($this->getEntityManager());
                $data        = array_merge($this->hydratorExtract($object), $data);

                $inputFilter->setData($data);

                if ($inputFilter->isValid()) {
                    $this->hydrate($inputFilter->getValues(), $object);
                    $this->getEntityManager()->flush();

                    $objectAsArray = [];
                    $cache         = [];
                    if ($this->columns) {
                        foreach ($this->columns as $column) {
                            $this->extractInternal($object, $column, $objectAsArray, $cache);
                        }
                    }

                    $success = true;
                }

                $errors['fields'] = $inputFilter->getMessages();
            }
        } catch (\Exception $e) {
            $errors['exceptions'][] = $e->getMessage();
        } finally {
            return new JsonModel([
                'errors'  => $errors,
                'success' => $success,
                'object'  => $objectAsArray,
            ]);
        }
    }

    protected function uploadAvatar(&$data)
    {
        if (isset($data['avatar'])) {
            $avatar = $data['avatar'];
            if (strpos($avatar, 'data:image') !== false) {
                $filePath = '/clients/' . $this->environment()->getClient()->getKey(). '/avatar/' . uniqid() . '.png';
                $file   = fopen(getcwd() . '/public' . $filePath, 'wb');
                $base64 = explode(',', $avatar);
                fwrite($file, base64_decode($base64[1]));
                fclose($file);

                $data['avatar'] = $filePath;
            }
        }
    }

    public function fusionAction()
    {
        $contactToKeep_id   = $this->params()->fromRoute('id');
        $contactToFusion_id = $this->params()->fromPost('contactToFusion_id', null);
        $type               = $this->params()->fromPost('type', null);

        if (!$contactToFusion_id ||
            $contactToKeep_id == $contactToFusion_id ||
            !$this->acl()->isAllowed('directory:e:contact:d') ||
            !in_array($type, ['duplicate', 'transfer'])
        ) {
            echo $contactToKeep_id;
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Contact $contactToKeep */
        $contactToKeep = $this->getEntityManager()
            ->getRepository('Directory\Entity\Contact')
            ->find($contactToKeep_id);

        /** @var Contact $contactToFusion */
        $contactToFusion = $this->getEntityManager()
            ->getRepository('Directory\Entity\Contact')
            ->find($contactToFusion_id);

        if (!$contactToKeep) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Association[] $associationsToKeep */
        $associationsToKeep = $this->getEntityManager()
            ->getRepository('Keyword\Entity\Association')
            ->findBy(['primary' => $contactToKeep_id]);

        /** @var Association[] $associationsToChange */
        $associationsToChange = $this->getEntityManager()
            ->getRepository('Keyword\Entity\Association')
            ->findBy(['primary' => $contactToFusion_id]);

        foreach ($associationsToChange as $association) {
            foreach ($associationsToKeep as $associationToKeep) {
                if (
                    $associationToKeep->getEntity() === $association->getEntity() &&
                    $associationToKeep->getPrimary() === $association->getPrimary()
                ) {
                    if ($type === 'transfer') {
                        $this->getEntityManager()->remove($association);
                    }
                    continue 2;
                }
            }

            if ($type === 'transfer') {
                /** @var Association $isAssociationExist */
                $isAssociationExist = $this->getEntityManager()
                    ->getRepository('Keyword\Entity\Association')
                    ->findOneBy(['primary' => $contactToKeep_id, 'entity' => $association->getEntity(), 'keyword' => $association->getKeyword()]);

                if (!isset($isAssociationExist)) {
                    $association->setPrimary($contactToKeep_id);
                    $this->getEntityManager()->persist($association);
                }
            } else {
                $isAssociationExist = $this->getEntityManager()
                    ->getRepository('Keyword\Entity\Association')
                    ->findOneBy(['primary' => $contactToFusion_id, 'entity' => $association->getEntity(), 'keyword' => $association->getKeyword()]);

                if (!isset($isAssociationExist)) {
                    $newAssociation = new Association();
                    $newAssociation->setEntity($association->getEntity());
                    $newAssociation->setPrimary($contactToFusion_id);
                    $newAssociation->setKeyword($association->getKeyword());
                    $this->getEntityManager()->persist($newAssociation);
                }
            }
        }

        /** @var ContactJobStructure[] $associationsToKeep */
        $jobAssociationsToKeep = $this->getEntityManager()
            ->getRepository('Directory\Entity\ContactJobStructure')
            ->findBy(['contact' => $contactToKeep_id]);

        /** @var ContactJobStructure[] $associationsToChange */
        $jobAssociationsToChange = $this->getEntityManager()
            ->getRepository('Directory\Entity\ContactJobStructure')
            ->findBy(['contact' => $contactToFusion_id]);

        foreach ($jobAssociationsToChange as $jobAssociation) {
            foreach ($jobAssociationsToKeep as $jobAssociationToKeep) {
                if (
                    $jobAssociationToKeep->getStructure()->getId() === $jobAssociation->getStructure()->getId() &&
                    $jobAssociationToKeep->getContact()->getId() === $jobAssociation->getContact()->getId()
                ) {
                    if ($type === 'transfer') {
                        $this->getEntityManager()->remove($jobAssociation);
                    }
                    continue 2;
                }
            }

            if ($type === 'transfer') {
                $jobAssociation->setContact($contactToKeep);
                $this->getEntityManager()->persist($jobAssociation);
                $this->getEntityManager()->flush();
            } else {
                $newJobAssociation = new ContactJobStructure();
                $newJobAssociation->setContact($contactToKeep);
                $newJobAssociation->setJob($jobAssociation->getJob());
                $newJobAssociation->setStructure($jobAssociation->getStructure());
                $this->getEntityManager()->persist($newJobAssociation);
            }
        }

        if ($type === 'transfer') {
            $this->getEntityManager()->remove($contactToFusion);
            $this->getEntityManager()->flush();
        }

        $this->getEntityManager()->flush();

        return new JsonModel([
            'success' => true,
        ]);
    }
}
