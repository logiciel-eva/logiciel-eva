(function(app) {

    app.controller('configurationController', [
        'configurationService', 'commandService', 'flashMessenger', 'translator',
        function configurationController(configurationService, commandService, flashMessenger, translator) {
            var self = this;

            self.init = false;

            var apiParams = {
                col: [
                    'id',
                    'modules',
                    'translations',
                    'code',
                    'timeGraph',
                    'budgetAutomatedImport'
                ]
            };

            self.configuration = {
                _rights: {
                    update: true
                }            
            };
            self.modules       = {};

            self.loading = {
                save: false,
                command: {}
            };

            configurationService.find(1, apiParams).then(function(res) {
                self.configuration = res.object;
                if (Object.keys(self.configuration).length == 0) {
                    self.configuration = {};
                }

                if (Object.keys(self.configuration.translations).length == 0) {
                    self.configuration.translations = {};
                }

                if (Object.keys(self.configuration.timeGraph).length == 0) {
                    self.configuration.timeGraph = {};
                }

                if (Object.keys(self.configuration.budgetAutomatedImport).length == 0) {
                    self.configuration.budgetAutomatedImport = {};
                }

                self.configuration.modules = angular.merge({}, self.modules, self.configuration.modules);
                self.init = true;
            });

            self.saveConfiguration = function saveConfiguration () {
                self.loading.save = true;
                configurationService.save(self.configuration, apiParams).then(function(res) {
                    self.configuration = res.object;
                    self.loading.save  = false;

                    flashMessenger.success(translator('configuration_message_saved'));

                }, function (err) {
                    self.loading.save = false;
                })
            };

            self.runCommand = function (name) {
                if (confirm(translator('command_run_confirm'))) {
                    self.loading.command[name] = true;
                    commandService.run(name).then(function(res) {
                        if (res.success === true) {
                            flashMessenger.success(translator('command_run_success'));
                        } else {
                            flashMessenger.error(translator('command_run_error'));
                        }
                        self.loading.command[name] = false;
                    }, function () {
                        flashMessenger.error(translator('command_run_error'));
                        self.loading.command[name] = false;
                    })
                }
            }
        }
    ])

})(window.EVA.app);
