(function(app) {

    app.controller('absenceTypeController', [
        'absenceTypeService', 'flashMessenger', 'translator',
        function absenceTypeController(absenceTypeService, flashMessenger, translator) {
            var self = this;

            self.absenceTypes = [];
            self.absenceType = {};
            self.formTitle = null;

            self.errors = {
                fields: {}
            };

            self.loading = {
                save: false
            };

            var params = {
                col: [
                    'id',
                    'name',
                    'initialLetters'
                ],
                search: {
                    type: 'list',
                    data: {
                        sort: 'name',
                        order: 'asc'
                    }
                }
            };

            self.init = function () {
                absenceTypeService.findAll(params).then(function (res) {
                    self.absenceTypes = res.rows;
                });
            };

            self.create = function () {
                self.absenceType = {};
                self.formTitle = translator('time_absence_type_nav_form');
                angular.element('#formModal').modal('show');
            };

            self.edit = function (absenceType) {
                self.absenceType = angular.copy(absenceType);
                self.formTitle = absenceType.name;
                angular.element('#formModal').modal('show');
            };

            self.save = function () {
                self.loading.save = true;
                self.errors = { fields: {} };

                absenceTypeService.save(self.absenceType, params).then(function (res) {
                    if (res.success) {
                        if (typeof self.absenceType.id === 'undefined') {
                            self.absenceTypes.push(res.object);
                        } else {
                            for (var i in self.absenceTypes) {
                                if (self.absenceTypes[i].id === res.object.id) {
                                    self.absenceTypes[i] = res.object;
                                    break;
                                }
                            }
                        }
                        flashMessenger.success(translator('time_absence_type_message_saved'));
                        angular.element('#formModal').modal('hide');
                    }
                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                });
            };

            self.delete = function (absenceType) {
                if (confirm(translator('time_absence_type_question_delete').replace('%1', absenceType.name))) {
                    absenceTypeService.remove(absenceType).then(function (res) {
                        if (res.success) {
                            self.absenceTypes.splice(self.absenceTypes.indexOf(absenceType), 1);
                            flashMessenger.success(translator('time_absence_type_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);