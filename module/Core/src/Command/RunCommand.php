<?php

namespace Core\Command;

use Admin\Command\Runner;
use Bootstrap\Entity\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument as InputArgumentAlias;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Exec a command on all Clients/Tenants.
 * @see \Admin\Command\Runner::run() options
 */
class RunCommand extends AbstractCommand
{

    protected function arguments(): void
    {
        $this->addArgument('CommandName', InputArgumentAlias::REQUIRED, 'This command will be exec on all Clients/Tenants. Command name to exec (See options on \Admin\Command\Runner::run()) ?');
    }

    /**
     * Call :
     * <pre>php vendor/bin/laminas core:run-command <CommandName></pre>
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('max_execution_time', -1);
        ini_set('memory_limit', -1);

        $name = $input->getArgument('CommandName');
        $output->writeln('[START] Launching command ' . $name);

        $em                   = $this->getEmDefault();
        $entityManagerFactory = $this->getEmFactory();
        $clients              = $em->getRepository('Bootstrap\Entity\Client')->findAll();
        $exitCode             = Command::SUCCESS;

        /** @var Client $client */
        foreach ($clients as $client) {
            $output->writeln("[INFO] Launching $name on Client=" . $client->getName());
            $clientEm = $entityManagerFactory->getEntityManager($client);

            $runner  = new Runner($clientEm, $client, $this->serviceLocator);
            $success = $runner->run($name);
            $output->writeln("[INFO] Launching $name on Client=" . $client->getName() . ' State=' . ($success ? 'SUCCESS' : 'FAILED'));
            if (!$success) {
                $exitCode = Command::FAILURE;
            }
        }

        $output->writeln('[END] Launching command ' . $name);
        return $exitCode;
    }
}
