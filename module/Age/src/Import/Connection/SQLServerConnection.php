<?php

namespace Age\Import\Connection;

use Age\Import\AbstractConnection;

/**
 * Create a pdo SQL Server instance to the AGE database.
 *
 * @package Age\Importer\Connection
 */
class SQLServerConnection extends AbstractConnection
{
  public function __construct($configuration)
  {
    parent::__construct(
      $configuration,
      "sqlsrv:Server={$configuration['dbHost']}, {$configuration['dbPort']};Database={$configuration['dbName']};Encrypt=true;TrustServerCertificate=true",
      $configuration['dbUser'],
      $configuration['dbPassword']
    );
  }


  /**
   * Retrieve structure id from libelle.
   * Used to filters rows.
   *
   * @param $libelle
   *
   * @return bool
   */
  protected function etablissement($libelle)
  {
    $statement = $this->prepare('
      SELECT 
        ID
      FROM
        ref_etablissements
      WHERE
        LIBELLE = :libelle;
    ');

    $statement->execute([
      ':libelle' => $libelle,
    ]);

    if ($row = $statement->fetch()) {
      return $row['ID'];
    }

    return false;
  }

  /**
   * Retrieve budget accounts.
   *
   * @return array
   */
  public function accounts()
  {
    $statement = $this->prepare('
      SELECT 
        CODE, LIBELLE, EST_DEPENSE
      FROM 
        ref_natures
      WHERE 
        ETABLISSEMENT_ID = :identifier
        AND EST_SAISISSABLE_AU_BUDGET <> 0;
    ');

    $statement->execute([
      ':identifier' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function expenseDv()
  {
    $query = '
      SELECT
        SUM(TL.VAL_COMPTA_MONTANT) AS "amount",
        TP.DATE_COMPTABLE AS "date",
        TV.EXERCICE AS "exercice",
        RO.CODE AS "project_code",
        RN.CODE AS "account_code",
        TP.NUMERO_PIECE AS "engagement",
        TP.OBJET AS "name",
        TP.OBJET AS "complement",
        RT.LIBELLE AS "tiers",
        OB.CODE AS "managementUnit"
      FROM 
        TRESO_DV_VENTILATIONS TV
        INNER JOIN REF_OPERATIONS RO ON RO.ID = TV.OPERATION_ID
        INNER JOIN TRESO_DV_LIGNES TL ON TL.ID = TV.LIGNE_ID 
        INNER JOIN TRESO_DV_PIECES TP ON TP.ID = TL.PIECE_ID 
        LEFT JOIN REF_NATURES RN ON RN.ID = TV.NATURE_ID 
        INNER JOIN REF_TIERS RT ON RT.ID = TL.TIERS_ID
        LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = TV.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = TV.ETABLISSEMENT_ID
      WHERE 
        TV.ETABLISSEMENT_ID = :identifier1
        AND RO.TYPE_OPERATION = 1
        AND TP.ETAPE_WF_CODE = \'REGLE\'
        AND TP.NATURE = 4';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND TV.EXERCICE IN(' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY TP.DATE_COMPTABLE, TV.EXERCICE, RO.CODE, RN.CODE, TP.NUMERO_PIECE, TP.OBJET, RT.LIBELLE, OB.CODE;
    ';

    if ($this->debug) {
      $f = fopen('expense_dv.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);
    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_AE_COMMITTED expenses.
   *
   * @return array
   */
  public function expenseAeCommitted()
  {
    $query = '
      SELECT 
        SUM(CB.MONTANT_COMPTABILISATION) AS "amount", 
        CB.DATE_ECRITURE AS "date",
        CB.EXERCICE as "exercice",
        RO.' . $this->codeColumn . ' AS "project_code",
        RN.CODE AS "account_code",
        CASE
          WHEN EP.NATURE_DOCUMENT = \'TRANCHE\' THEN CONCAT(MM.NUMERO_PIECE,\'/\',CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \')
          ELSE CONCAT(CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \')
        END AS "engagement",
        CASE
		      WHEN CB.PIECE_NATURE_DOCUMENT = \'DP\' THEN DP.OBJET
		      WHEN CB.PIECE_NATURE_DOCUMENT = \'OD\' THEN OD.OBJET
		      ELSE EP.OBJET 
		    END AS "name",
        CASE
          WHEN EP.NATURE_DOCUMENT = \'TRANCHE\' THEN RT2.LIBELLE
          WHEN CB.PIECE_NATURE_DOCUMENT = \'DP\' THEN RT3.LIBELLE
          WHEN CB.PIECE_NATURE_DOCUMENT = \'REJ\' THEN RT4.LIBELLE
          WHEN CB.PIECE_NATURE_DOCUMENT = \'OD\' THEN RT5.LIBELLE
          ELSE RT.LIBELLE
        END AS "tiers",
        OB.CODE AS "managementUnit"
      FROM 
        comptabilites_budgetaires CB
        INNER JOIN ' . $this->codeTable . ' RO ON ' . $this->codeJoin . '
        INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
        LEFT JOIN ej_pieces EP ON EP.ID = CB.PIECE_ID
        LEFT JOIN ref_tiers RT ON RT.ID = EP.TIERS_ID 
        LEFT JOIN marches_marches MM ON MM.ID = EP.PIECE_MARCHE_ID
        LEFT JOIN ref_tiers RT2 ON RT2.ID = MM.TIERS_ID
        LEFT JOIN dp_pieces DP ON DP.ID = CB.PIECE_ID
        LEFT JOIN ref_tiers RT3 ON RT3.ID = DP.TIERS_ID
        LEFT JOIN ej_pieces EP2 ON EP2.ID = EP.PIECE_REFERENCE_ID
        LEFT JOIN ref_tiers RT4 ON RT4.ID = EP2.TIERS_ID
        LEFT JOIN od_pieces OD ON OD.ID = CB.PIECE_ID
        LEFT JOIN od_pieces_liees ODP ON ODP.PIECE_OD_ID = OD.ID AND ODP.NATURE_PIECE_LIEE = \'EJ\'
        LEFT JOIN ej_pieces EP3 ON EP3.ID = ODP.PIECE_LIEE_ID
        LEFT JOIN ref_tiers RT5 ON RT5.ID = EP3.TIERS_ID 
        LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = CB.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
      WHERE 
        CB.TYPE_COMPTA = \'AE\'
        AND CB.EST_CONSOMMATION = 1
        AND CB.EST_BUDGET = 0
        AND CB.ETABLISSEMENT_ID = :identifier1';

    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY CB.DATE_ECRITURE, CB.EXERCICE, RO.' . $this->codeColumn . ', RN.CODE, MM.NUMERO_PIECE, CB.PIECE_NUMERO_PIECE, CB.PIECE_NATURE_DOCUMENT, DP.OBJET, OD.OBJET, EP.OBJET, EP.NATURE_DOCUMENT, RT2.LIBELLE, RT3.LIBELLE, RT4.LIBELLE, EP3.ID, RT5.LIBELLE, RT.LIBELLE, OB.CODE;
    ';

    if ($this->debug) {
      $f = fopen('expense_ae_committed.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function expenseAe()
  {
    $query = '
      SELECT
          RO.' . $this->codeColumn . ' AS "project_code",
          CB.PIECE_OBJET AS "name",
          CB.EXERCICE AS "exercice",
          CB.DATE_ECRITURE AS "date",
          SUM(CB.MONTANT_COMPTABILISATION) AS "amount",
          CASE
            WHEN EP.NATURE_DOCUMENT = \'TRANCHE\' THEN CONCAT(MM.NUMERO_PIECE,\'/\',CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \')
            ELSE CONCAT(CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \')
          END AS "engagement",
          RN.CODE AS "account_code",
          OB.CODE AS "managementUnit"
      FROM
          comptabilites_budgetaires CB
          INNER JOIN ' . $this->codeTable . ' RO ON ' . $this->codeJoin . '
          INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
          LEFT JOIN ej_pieces EP ON EP.ID = CB.PIECE_ID
          LEFT JOIN marches_marches MM ON MM.ID = EP.PIECE_MARCHE_ID
          LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = CB.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
      WHERE
          CB.EST_BUDGET = 1 
          AND CB.ETABLISSEMENT_ID = :identifier1
          AND CB.TYPE_COMPTA = \'AE\'';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY RO.' . $this->codeColumn . ', CB.PIECE_OBJET, CB.EXERCICE, CB.DATE_ECRITURE, EP.NATURE_DOCUMENT, MM.NUMERO_PIECE, CB.PIECE_NUMERO_PIECE, CB.PIECE_NATURE_DOCUMENT, RN.CODE, OB.CODE;
    ';

    if ($this->debug) {
      $f = fopen('expense_ae.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function expenseCp()
  {
    $query = '
      SELECT
          RO.' . $this->codeColumn . ' AS "project_code",
          CB.PIECE_OBJET AS "name",
          CB.EXERCICE AS "exercice",
          CB.DATE_ECRITURE AS "date",
          SUM(CB.MONTANT_COMPTABILISATION) AS "amount",
          CONCAT(CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \') AS "engagement",
          RN.CODE AS "account_code",
          OB.CODE AS "managementUnit"
      FROM
          comptabilites_budgetaires CB
          INNER JOIN ' . $this->codeTable . ' RO ON ' . $this->codeJoin . '
          INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
          LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = CB.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
      WHERE
          CB.EST_BUDGET = 1 
          AND CB.ETABLISSEMENT_ID = :identifier1
          AND CB.TYPE_COMPTA = \'CP\'';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY RO.' . $this->codeColumn . ', CB.PIECE_OBJET, CB.EXERCICE, CB.DATE_ECRITURE, CB.PIECE_NUMERO_PIECE, CB.PIECE_NATURE_DOCUMENT, RN.CODE, OB.CODE;
    ';

    if ($this->debug) {
      $f = fopen('expense_cp.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_CP_ESTIMATED expenses.
   *
   * @return array
   */
  public function expenseCpEstimated()
  {
    $query = '
      SELECT
        SUM(CB.VAL_COMPTA_CHARGE_BUD)-SUM(isnull(cp.montantCP,0)) AS "amount",
        RO.CODE AS "project_code",
        RN.CODE AS "account_code",
        DP.EXERCICE as "exercice",
        DP.DATE_RECEPTION AS "date",
        FF.DATE_FACTURE AS "date_facture",
        DP.REFERENCE_FACTURE AS "mandat",
        DP.OBJET AS "name",
        DP.OBJET AS "complement",
        RT.LIBELLE AS "tiers",
        OB.CODE AS "managementUnit"
      FROM
        dp_ventilations CB
      INNER JOIN ref_operations RO ON RO.ID = CB.OPERATION_ID
      INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND CB.ETABLISSEMENT_ID = RN.ETABLISSEMENT_ID
      INNER JOIN dp_pieces DP ON DP.ID = CB.PIECE_ID
      INNER JOIN ref_tiers RT ON RT.ID = DP.TIERS_ID
      LEFT JOIN ff_pieces FF ON FF.ID = DP.PIECE_FF_ID
      LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = CB.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
      LEFT JOIN (select cbud.VENTILATION_ID, sum(CBUD.MONTANT_COMPTABILISATION) montantCP
          FROM comptabilites_budgetaires CBUD
          WHERE CBUD.TYPE_COMPTA = \'CP\' AND CBUD.EST_CONSOMMATION = 1 AND CBUD.EST_BUDGET = 0 AND CBUD.ETABLISSEMENT_ID = :identifier1';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
          AND CBUD.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .='
          GROUP BY cbud.VENTILATION_ID) cp on cp.VENTILATION_ID=cb.ID
      WHERE
        CB.EST_ANNULE = 0
        AND CB.ETABLISSEMENT_ID = :identifier2';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
          AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY RO.CODE, DP.EXERCICE, DP.DATE_RECEPTION, FF.DATE_FACTURE, DP.REFERENCE_FACTURE, DP.OBJET, RN.CODE, RT.LIBELLE, OB.CODE
    ';

    if ($this->debug) {
      $f = fopen('expense_cp_estimated.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_CP_COMMITTED expenses.
   *
   * @return array
   */
  public function expenseCpCommitted()
  {
    $query = '
      SELECT 
        SUM(CB.MONTANT_COMPTABILISATION) AS "amount", 
        CB.DATE_ECRITURE AS "date",
        CB.EXERCICE as "exercice",
        RO.' . $this->codeColumn . ' AS "project_code",
        RN.CODE AS "account_code",
        CASE CB.PIECE_NATURE_DOCUMENT
        		WHEN \'OD\' 
				    THEN RT2.LIBELLE
        		ELSE RT.LIBELLE
		    END AS "tiers",
        CASE CB.PIECE_NATURE_DOCUMENT
          WHEN \'OD\'
          THEN OD.NUMERO_PIECE
          ELSE DP.NUMERO_PIECE
        END AS "bordereau",
        CASE CB.PIECE_NATURE_DOCUMENT
          WHEN \'OD\'
          THEN OD.OBJET
          ELSE DP.OBJET
        END AS "complement",
        CASE CB.PIECE_NATURE_DOCUMENT
          WHEN \'OD\'
          THEN OD.OBJET
          ELSE DP.OBJET
        END AS "name",
        OB.CODE AS "managementUnit"
      FROM 
        comptabilites_budgetaires CB
        INNER JOIN ' . $this->codeTable . ' RO ON ' . $this->codeJoin . '
        INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
        LEFT JOIN dp_ventilations DV ON DV.ID = CB.VENTILATION_ID
        LEFT JOIN dp_pieces DP ON DP.ID = DV.PIECE_ID
        LEFT JOIN ref_tiers RT ON RT.ID = DP.TIERS_ID
        LEFT JOIN od_pieces OD ON OD.ID = CB.PIECE_ID
        LEFT JOIN OD_PIECES_LIEES ODL ON ODL.PIECE_OD_ID = OD.ID AND ODL.NATURE_PIECE_LIEE = \'DP\'
        LEFT JOIN DP_PIECES DP2 ON DP2.ID = ODL.PIECE_LIEE_ID
        LEFT JOIN REF_TIERS RT2 ON RT2.ID = DP2.TIERS_ID
        LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = CB.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
      WHERE 
        CB.TYPE_COMPTA = \'CP\'
        AND CB.EST_CONSOMMATION = 1
        AND CB.EST_BUDGET = 0
        AND CB.ETABLISSEMENT_ID = :identifier1';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY CB.DATE_ECRITURE, CB.EXERCICE, RO.' . $this->codeColumn . ', RN.CODE, CASE CB.PIECE_NATURE_DOCUMENT WHEN \'OD\' THEN RT2.LIBELLE ELSE RT.LIBELLE END, CASE CB.PIECE_NATURE_DOCUMENT WHEN \'OD\' THEN OD.NUMERO_PIECE ELSE DP.NUMERO_PIECE END, CASE CB.PIECE_NATURE_DOCUMENT WHEN \'OD\' THEN OD.OBJET ELSE DP.OBJET END, OB.CODE;';

    if ($this->debug) {
      $f = fopen('expense_cp_committed.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }



  public function incomeDv()
  {
    $query = '
      SELECT
        SUM(TL.VAL_COMPTA_MONTANT) AS "amount",
        TP.DATE_COMPTABLE AS "date",
        TV.EXERCICE AS "exercice",
        RO.CODE AS "project_code",
        RN.CODE AS "account_code",
        TP.NUMERO_PIECE AS "convention_title",
        TP.OBJET AS "name",
        TP.OBJET AS "engagement",
        RT.LIBELLE AS "tiers",
        OB.CODE AS "managementUnit"
      FROM 
        TRESO_DV_VENTILATIONS TV
        INNER JOIN REF_OPERATIONS RO ON RO.ID = TV.OPERATION_ID
        INNER JOIN TRESO_DV_LIGNES TL ON TL.ID = TV.LIGNE_ID 
        INNER JOIN TRESO_DV_PIECES TP ON TP.ID = TL.PIECE_ID 
        LEFT JOIN REF_NATURES RN ON RN.ID = TV.NATURE_ID AND RN.ETABLISSEMENT_ID = TV.ETABLISSEMENT_ID
        INNER JOIN REF_TIERS RT ON RT.ID = TL.TIERS_ID
        LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = TV.ORGANISATION_BUDGETAIRE_ID AND TV.ETABLISSEMENT_ID = TV.ETABLISSEMENT_ID
      WHERE 
        TV.ETABLISSEMENT_ID = :identifier1
        AND RO.TYPE_OPERATION = 1
        AND TP.ETAPE_WF_CODE = \'REGLE\'
        AND TP.NATURE = 3';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND TV.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY TP.DATE_COMPTABLE, TV.EXERCICE, RO.CODE, RN.CODE, TP.NUMERO_PIECE, TP.OBJET, RT.LIBELLE, OB.CODE;';

    if ($this->debug) {
      $f = fopen('income_dv.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);
    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_REQUESTED incomes.
   *
   * @return array
   */
  public function incomeRequested()
  {
    $query = '
      SELECT
          SUM(TL.VAL_COMPTA_MONTANT_TTC_A_ENCAISSER) AS "amount",
          TP.DATE_COMPTABLE AS "date",
          TV.EXERCICE as "exercice",
          RO.' . ($this->switchCode ? 'CODE_ANALYTIQUE_CODE' : 'CODE') . ' AS "project_code",
          RN.CODE AS "account_code",
          TP.NUMERO_PIECE AS "convention_title",
          TP.OBJET AS "name",
          TP.OBJET AS "engagement",
          RT.LIBELLE AS "tiers",
          OB.CODE AS "managementUnit"
      FROM
          titres_ventilations TV
          INNER JOIN titres_lignes TL ON TL.ID = TV.LIGNE_ID
          INNER JOIN titres_pieces TP ON TP.ID = TL.PIECE_ID
          INNER JOIN ref_tiers RT ON RT.ID = TP.TIERS_ID
          INNER JOIN ' . ($this->switchCode ? 'ref_analytique' : 'ref_operations') . '  RO ON RO.ID = TV.' . ($this->switchCode ? 'ANALYTIQUE_ID' : 'OPERATION_ID') . '
          INNER JOIN ref_natures RN ON RN.ID = TV.NATURE_ID AND RN.ETABLISSEMENT_ID = TV.ETABLISSEMENT_ID
          LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = TV.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = TV.ETABLISSEMENT_ID
      WHERE
          TL.VAL_COMPTA_MONTANT_TTC_A_ENCAISSER <> 0
          AND TV.ETABLISSEMENT_ID = :identifier1';
    if ($this->switchCode) {
      $query .= '
        AND RO.NIVEAU_ANALYTIQUE_ORDRE_DE_PRESENTATION = ' . $this->level;
    }
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND TV.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY TP.DATE_COMPTABLE, TV.EXERCICE, RO.' . ($this->switchCode ? 'CODE_ANALYTIQUE_CODE' : 'CODE') . ', RN.CODE, TP.NUMERO_PIECE, TP.OBJET, RT.LIBELLE, OB.CODE;';

    if ($this->debug) {
      $f = fopen('income_requested.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_PERCEIVED incomes.
   *
   * @return array
   */
  public function incomePerceived()
  {
    $query = '
      SELECT 
        SUM(CB.MONTANT_COMPTABILISATION) AS "amount", 
        CB.DATE_ECRITURE AS "date",
        CB.EXERCICE as "exercice",
        RO.' . ($this->switchCode ? 'CODE_ANALYTIQUE_CODE' : 'CODE') . ' AS "project_code",
        RN.CODE AS "account_code",
        TP.NUMERO_PIECE AS "convention_title",
        TP.OBJET AS "name",
        TP.OBJET AS "engagement",
        RT.LIBELLE AS "tiers",
        OB.CODE AS "managementUnit"
      FROM 
        comptabilites_budgetaires CB
        INNER JOIN ' . ($this->switchCode ? 'ref_analytique' : 'ref_operations') . '  RO ON RO.ID = CB.' . ($this->switchCode ? 'ANALYTIQUE_ID' : 'OPERATION_ID') . '
        INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
        INNER JOIN titres_ventilations TV ON TV.ID = CB.VENTILATION_ID
        INNER JOIN titres_pieces TP ON TP.ID = TV.PIECE_ID
        INNER JOIN ref_tiers RT ON RT.ID = TP.TIERS_ID
        LEFT JOIN REF_ORGANISATIONS_BUDGETAIRES OB ON OB.ID = CB.ORGANISATION_BUDGETAIRE_ID AND OB.ETABLISSEMENT_ID = CB.ETABLISSEMENT_ID
      WHERE 
        CB.TYPE_COMPTA = \'RECETTE\'
        AND CB.EST_CONSOMMATION = 1
        AND CB.EST_BUDGET = 0
        AND CB.ETABLISSEMENT_ID = :identifier1';
    if ($this->switchCode) {
      $query .= '
        AND RO.NIVEAU_ANALYTIQUE_ORDRE_DE_PRESENTATION = ' . $this->level;
    }
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY CB.DATE_ECRITURE, CB.EXERCICE, RO.' . ($this->switchCode ? 'CODE_ANALYTIQUE_CODE' : 'CODE') . ', RN.CODE, TP.NUMERO_PIECE, TP.OBJET, RT.LIBELLE, OB.CODE;';

    if ($this->debug) {
      $f = fopen('income_perceived.txt', 'w+');
      fwrite($f, $query);
      fclose($f);
    }

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }
}
