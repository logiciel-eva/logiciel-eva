(function (app) {

    app.controller('recapController', [
        '$http', '$timeout', '$scope', 'queryService', '$rootScope','translator','userConfigurationService',
        function recapController($http, $timeout, $scope, queryService, $rootScope, translator, userConfigurationService) {
            var self = this;

            self.view = 'table';
            self.details = true;
    
            self.loading = false;
            self.filters = {
                project: {},
                indicator: {},
                measure: {},
                group: 'indicator'
            };
            self.nbFilters = 1;
            self.group = null;
            self.indicators = {};
            self.years = [];
            self.periods = [];
            self.aggregateBy = 1;
            self.yearsAggregates;

            self.setAggregateBy = function(value) {
                this.aggregateBy = value;
                self.resetPeriods();
            }

            self.setPeriodicity = function(value) {
                self.periodicity = value;
                self.resetPeriods();
            }
            
            self.resetPeriods = function() {
                switch(self.periodicity) {
                    case 'MONTH' : self.subPeriods = 12; self.aggregateBy = 1; break;
                    case 'QUARTER' : self.subPeriods = 4;self.aggregateBy = 1; break;
                    case 'YEAR' : self.subPeriods = 1; break;
                }

                self.periods = [];
                self.yearsAggregates = [];
                if(self.periodicity === 'YEAR' && self.aggregateBy > 1) {
                    for (let i = 0; i < self.years.length; i += self.aggregateBy) {
                        const aggregate = self.years.slice(i, i + self.aggregateBy);
                        self.yearsAggregates.push(aggregate.join('-'));
                    }
                } else {
                    self.yearsAggregates = self.years;
                }
                
                 for (var y in self.yearsAggregates) {
                    for(var s = 1; s <= self.subPeriods; s++) {
                       self.periods.push({
                            alias: self.yearsAggregates[y],
                            subPeriod: s
                     }); 
                   }
                }
            }
  
            self.setPeriodicity('YEAR');

            self.hasSubPeriods = function () {
                return self.periodicity !== 'YEAR';
            }
            self.getTargetValue = function(indicatorData, period)  {
                var data = self.resolveData(indicatorData, period);
                return data && data.target ? data.target.value : null;
            }

            self.getDoneValue = function(indicatorData, period)  {
                var data = self.resolveData(indicatorData, period);
                return data && data.done ? data.done.value : null;
            }

            self.getTargetText = function(indicatorData, period) {
                var data = self.resolveData(indicatorData, period);
                return data && data.target && data.target.content? data.target.content.text : null;
            }

            self.getDoneText = function(indicatorData, period) {
                 var data = self.resolveData(indicatorData, period);
                 return data && data.done && data.done.content? data.done.content.text : null;
            }

            self.getTargetColor = function(indicatorData, period) {
                var data = self.resolveData(indicatorData, period);
                return data && data.target && data.target.content? data.target.content.color : null;
            }

            self.getDoneColor = function(indicatorData, period) {
                var data = self.resolveData(indicatorData, period);
                return data && data.done && data.done.content? data.done.content.color : null;
            }

            self.getRatio = function(indicatorData, period) {
                var data = self.resolveData(indicatorData, period);
                return   data ? data.ratio : null;
            }

            self.getRatioText = function(indicatorData, period) {
                var ratioValue = self.getRatio(indicatorData, period);
                var ratioText = ratioValue  || ratioValue === 0 ? ratioValue : '';
                if(ratioValue !== 'NaN' && (ratioValue || ratioValue === 0)) {
                    ratioText += ' %';
                }
               return ratioText;
            }

            self.resolveData = function(indicatorData, period) {
                if(!!!indicatorData) return null;

                if(period.alias === 'Total' || (self.periodicity === 'YEAR' && self.aggregateBy === 1)) {
                   return  indicatorData[period.alias];
                }

                if(self.periodicity === 'YEAR' && self.aggregateBy > 1) {
                   return self.aggregateData(indicatorData, period.alias.split('-'));
                }

                var yearData = indicatorData[period.alias];
                if(!!!yearData) return null;
                const subPeriodKey = self.periodicity === 'MONTH' ? 'months' : 'quarters';
                return yearData[subPeriodKey] ? yearData[subPeriodKey][period.subPeriod] : null;
            }

            self.aggregateData = function(indicatorData, years) {
               const aggregatedData = {
                  done: {value: null},
                  target:{value: null},
                  ratio: {}
               }

               for(var year of years) {
                    if(indicatorData[year] && indicatorData[year].done && indicatorData[year].done.value) {
                        if(null === aggregatedData.done.value) {
                             aggregatedData.done.value = 0;
                        }
                        aggregatedData.done.value+= indicatorData[year].done.value;
                    }

                    if(indicatorData[year] && indicatorData[year].target && indicatorData[year].target.value) {
                        if(null === aggregatedData.target.value) {
                             aggregatedData.target.value = 0;
                        }
                        aggregatedData.target.value+= target = indicatorData[year].target.value;;
                    }
                   
               }

                aggregatedData.ratio = self.getAggregateRatio(indicatorData.type, aggregatedData.done.value, aggregatedData.target.value );

                return aggregatedData;  
            }

            self.getAggregateRatio = function(indicatorType, done, target) {
                if (indicatorType === 'fixed') {
                    return 'NaN'
                }
                if ((null === target || undefined === target) && (null === done || undefined === done)) {
                    return null;
                } else if ((null === target || undefined === target) || (null === done || undefined === done)) {
                    return 'NaN';
                } else if (target > 0) {
                    return Math.round(done * 100 / target);
                } else {
                    return done > 0 ? 100 : null;
                }
            }

            self.applyFilters = function applyFilters() {
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.indicator).length + Object.keys(self.filters.measure).length;
                self.loading = true;
                var group = self.filters.group;

                $http({
                    url: '/analysis/indicator/recap?' + $.param({filters: self.filters, aggregateBy: self.aggregateBy }),
                    method: 'GET',
                }).then(function (res) {
                    self.group = group;
                    self.groupIndicators = res.data.groupIndicators;
                    self.indicators = res.data.indicators;
                    self.years = res.data.years;
                    self.resetPeriods();
                    self.loading = false;

                    self.loadBubbleChart();
                    self.loadLineChart();
                    self.loadBarChart();
                    //self.loadPieChart();
                });
            };

            self.resetFilters = function () {
                self.filters.project = {};
                self.filters.indicator = {};
                self.filters.measure = {};
                self.applyFilters();
            };

            self.hasIndicators = function(groupIndicator) {
                if(!!!groupIndicator.indicators) return false;

                if(angular.isArray(groupIndicator.indicators)) return groupIndicator.indicators.length;

                return Object.values(groupIndicator.indicators).length;                    
            };

            self.getIndicators = function(indicatorIds){
                result = [];
                for (var i in self.indicators) {
                    var indicator = self.indicators[i];
                    if(!!indicatorIds) {
                        if(!angular.isArray(indicatorIds)) {
                           indicatorIds = Object.values(indicatorIds);
                        }
                        indicatorIds.forEach(id => {
                            if(id == indicator.id){
                               result.push(indicator);
                            }
                        });
                    }

                }
                return result;
            }
            self.export = function () {
                var columns = [];
                for (var i in self.columns) {
                    if(self.columns[i].visible) {
                       columns = [...columns, self.columns[i].name];
                    }
				}
    
                window.open('/analysis/indicator/recap?' + $.param({
                    filters: self.filters, 
                    export: true, columns, 
                    periodicity: self.periodicity,
                    aggregateBy: self.aggregateBy
                }));
            };

            self.chart = {
                bubble: {
                    options: {
                        chart: {
                            type: 'scatterChart',
                            height: 600,
                            showDistX: true,
                            showDistY: true,
                            xAxis: {
                                tickFormat: function (d) {
                                    if (d == parseInt(self.years[0] - 1)) {
                                        return 'Initiale'
                                    } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                        return 'Finale';
                                    }
                                    return d;
                                },
                                tickValues: []
                            },
                        }
                    },
                    data: [],
                    api: null
                },
                line: {
                    options: {
                        chart: {
                            type: 'lineChart',
                            height: 600,
                            showDistX: true,
                            showDistY: true,
                            xAxis: {
                                tickFormat: function (d) {
                                    if (d == parseInt(self.years[0] - 1)) {
                                        return 'Initiale'
                                    } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                        return 'Finale';
                                    }
                                    return d;
                                },
                                tickValues: []
                            },
                        }
                    },
                    data: [],
                    api: null
                },
                bar: {
                    options: {
                        chart: {
                            type: 'multiBarChart',
                            height: 600,
                            showDistX: true,
                            showDistY: true,
                            xAxis: {
                                tickFormat: function (d) {
                                    if (d == parseInt(self.years[0] - 1)) {
                                        return 'Initiale'
                                    } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                        return 'Finale';
                                    }
                                    return d;
                                },
                                tickValues: []
                            },
                        }
                    },
                    data: [],
                    api: null
                },
                pie: []
            };

            self.loadBubbleChart = function () {
                self.chart.bubble.data = [];
                self.chart.bubble.options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];
                for (var y in self.years) {
                    self.chart.bubble.options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                }
                self.chart.bubble.options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);

                for (var i in self.indicators) {
                    var data = {
                        key: self.indicators[i].name,
                        values: []
                    };

                    
        
                    for (var y in self.years) {
                        var year = self.years[y];
                        const indicatorData = self.indicators[i].data[year]; 
                        if (indicatorData && indicatorData.ratio && indicatorData.ratio !== 'NaN') {
                            data.values.push({
                                y: indicatorData.ratio,
                                x: parseInt(year)
                            });
                        }
                    }

                    self.chart.bubble.data.push(data);
                }
            };

            self.loadLineChart = function () {
                self.chart.line.data = [];
                self.chart.line.options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];
                for (var y in self.years) {
                    self.chart.line.options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                }
                self.chart.line.options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);

                for (var i in self.indicators) {
                    var dataT = {
                        key: self.indicators[i].name + ' - Cible',
                        values: []
                    }, dataD = {
                        key: self.indicators[i].name + ' - Réalisé',
                        values: []
                    };

                    for (var y in self.years) {
                        var year = self.years[y];
                         const indicatorData = self.indicators[i].data[year]; 
                        if (indicatorData && indicatorData.target && indicatorData.target.value) {
                            dataT.values.push({
                                y: indicatorData.target.value,
                                x: parseInt(year),
                            });
                        }

                        if (indicatorData && indicatorData.done && indicatorData.done.value) {
                            dataD.values.push({
                                y: indicatorData.done.value,
                                x: parseInt(year),
                            });
                        }
                    }




                    self.chart.line.data.push(dataT);
                    self.chart.line.data.push(dataD);
                }
            };

            self.loadBarChart = function () {
                self.chart.bar.data = [];
                self.chart.bar.options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];

                for (var y in self.years) {
                    self.chart.bar.options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                }
                self.chart.bar.options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);

                for (var i in self.indicators) {
                    var dataT = {
                        key: self.indicators[i].name + ' - Cible',
                        values: []
                    }, dataD = {
                        key: self.indicators[i].name + ' - Réalisé',
                        values: []
                    };


                    for (var y in self.years) {
                        var year = self.years[y];
                         const indicatorData = self.indicators[i].data[year]; 
                        if (indicatorData && indicatorData.target && indicatorData.target.value) {
                            dataT.values.push({
                                y: indicatorData.target.value,
                                x: parseInt(year),
                            });
                        }

                        if (indicatorData && indicatorData.done && indicatorData.done.value) {
                            dataD.values.push({
                                y:  indicatorData.done.value,
                                x: parseInt(year),
                            });
                        }
                    }

                
                    if (dataT.values.length > 0){
                        self.chart.bar.data.push(dataT);
                    }
                    if (dataD.values.length > 0){
                        self.chart.bar.data.push(dataD);
                    }
                }
            };

            /*self.loadPieChart = function () {
                var data = [];
                for (var y in self.years) {
                    data[self.years[y]] = [];
                }

                for (var i in self.indicators) {

                    for (var y in self.years) {
                        var year = self.years[y];
                        const indicatorData = self.indicators[i].data[year];
                        if (indicatorData && indicatorData.done && indicatorData.done.value) {
                            data[year].push({
                                value: indicatorData.done.value,
                                label: self.indicators[i].name,
                            });
                        }
                    }
                }


                self.chart.pie = [];

    
                for (var y in self.years) {
                    self.chart.pie.push({
                        api: {},
                        name: self.years[y],
                        options: {
                            chart: {
                                type: 'pieChart',
                                showControls: false,
                                showValues: true,
                                stacked: true,
                                height: 400,
                                useInteractiveGuideline: true,
                                labelsOutside: true,
                                x: function (d) {
                                    return d.label;
                                },
                                y: function (d) {
                                    return d.value;
                                },
                            }
                        },
                        data: data[self.years]
                    });
                }
            };*/

            $scope.$watch(function () {
                return self.view
            }, function () {
                $timeout(function () {
                    self.chart.bubble.api.refresh();
                    self.chart.line.api.refresh();
                    self.chart.bar.api.refresh();
                    //self.chart.pie.map(pie => pie.api.refresh());
                });
            });

            // QUERIES
            self.queries = [];
            self.query = {
                list: 'analysis-indicator-recap'
            };
            self.errors = {
                fields: {}
            };
            self.loadingQueries = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.init = function(keywordsColumns) {
                self.getTableConfiguration();
                self.initColumns(keywordsColumns); 
                self.initQueries();
            }
            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };

        
            self.cols = ['description', 'definition', 'method', 'interpretation','uom', 'type', 'operator','createdAt','updatedAt'];
            self.columns = {};
            self.initColumns = function(keywordGroups) {
                for (var col of self.cols) {
                    self.columns[col] = {
                        name: col,
                        label: translator('indicator_field_' + col)
                    };
                }
              	for (var group in keywordGroups) {
                   const key = 'keyword' +  keywordGroups[group].id;
					self.columns[key]=  {
                        name: key,
                        label: keywordGroups[group].name
                    }
				} 
            }
 
			self.getTableConfiguration = function () {
				userConfigurationService.getTable('indicator-analysis-recap-list').then(function (res) {
					for (var i in res) {
                        self.columns[i].visible = res[i] == 'true';
                    }
				});
			};

			self.saveTableConfiguration = function () {
				var cols = {};
				for (var i in self.columns) {
					var column = self.columns[i];
					cols[column.name] = column.visible;
				}

				userConfigurationService.saveTable('indicator-analysis-recap-list', cols).then(function () { });
			};

        }
    ]);
})(window.EVA.app);
