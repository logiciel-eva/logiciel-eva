(function(app) {

    app.directive('projectSelect', [
        'projectService', 'selectFactoryService',
        function projectSelect(projectService, selectFactoryService) {
            return selectFactoryService.create({
                service: {
                    object: projectService,
                    apiParams: {
                        col: ['id', 'code', 'name', 'parent.id'],
                        search: {
                            type: 'list',
                            data: {
                                sort  : 'code',
                                order : 'asc',
                                filters: {
                                    publicationStatus: {
                                        op : 'neq',
                                        val: ['archived']
                                    }
                                }
                            }
                        }
                    },
                    full: true,
                    tree: true,
                    treeOrder: 'code',
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['code', 'name'],
                    render: {
                        option: (data, escape) => `
                            <div class="option">
                                ${data.code ? `${escape(data.code)} - ` : '' }${escape(data.name)}
                            </div>
                        `,
                        item: (data, escape) => `
                            <div class="option">
                                ${data.code ? `${escape(data.code)} - ` : '' }${escape(data.name)}
                            </div>
                        `,
                    },
                },
            });
        }
    ])

})(window.EVA.app);
