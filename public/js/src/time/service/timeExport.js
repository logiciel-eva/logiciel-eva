(function(app) {

    app.factory('timeExportService', [
        'restFactoryService',
        function timeExportService(restFactoryService) {
            return restFactoryService.create('/api/timesheet/export');
        }
    ])

})(window.EVA.app);
