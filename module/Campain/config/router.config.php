<?php

namespace Campain;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'campain' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/campain',
                    'defaults' => [
                        'controller' => 'Campain\Controller\Index',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'Campain\Controller\Index',
                                'action'     => 'form'
                            ]
                        ]
                    ]
                ]
            ],
            'api' => [
                'child_routes'  => [
                    'campain' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/campain[/:id]',
                            'defaults' => [
                                'controller' => 'Campain\Controller\API\Campain'
                            ]
                        ]
                    ],
                    'campain-indicator' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/campain-indicator[/:id]',
                            'defaults' => [
                                'controller' => 'Campain\Controller\API\Indicator'
                            ]
                        ]
                    ],
                    'campain-measure' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/campain-measure[/:id]',
                            'defaults' => [
                                'controller' => 'Campain\Controller\API\Measure'
                            ]
                        ]
                    ]
                    
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class          => ServiceLocatorFactory::class,
            Controller\API\CampainController::class    => ServiceLocatorFactory::class,
            Controller\API\IndicatorController::class  => ServiceLocatorFactory::class,
            Controller\API\MeasureController::class  => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Campain\Controller\Index'               => Controller\IndexController::class,
            'Campain\Controller\API\Campain'         => Controller\API\CampainController::class,
            'Campain\Controller\API\Indicator'       => Controller\API\IndicatorController::class,
            'Campain\Controller\API\Measure'       => Controller\API\MeasureController::class
        ],
    ],
];
