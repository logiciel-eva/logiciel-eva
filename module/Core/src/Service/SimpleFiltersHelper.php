<?php

namespace Core\Service;

use DateTime;
use Doctrine\ORM\QueryBuilder;


class SimpleFiltersHelper
{

	private $_entityManager;

	public function __construct($entityManager)
    {
        $this->_entityManager = $entityManager;
    }

	
	 /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->_entityManager = $entityManager;
    }

	public function apply(QueryBuilder $queryBuilder, $entityClass,$master, $filters = [], $mode = 'and')
	{
		$whereMode = $mode === 'and' ? 'andWhere' : 'orWhere';
		$havingMode = $mode === 'and' ? 'andHaving' : 'orHaving';

		$index = 0;
		$keywords = [];
		$territories = null;

		foreach ($filters as $property => $filter) {
			if (
				isset($filter['val']) ||
				(isset($filter['op']) && $filter['op'] == 'isNull') ||
				(isset($filter['op']) && $filter['op'] == 'isNotNull')
			) {
				if (strpos($property, 'keyword.') !== false && isset($filter['op'])) {
					$keywords[$property] = $filters[$property];
					continue;
				}

				if (strpos($property, 'customField.') !== false && isset($filter['op'])) {
					$prop = str_replace('.', '_', $property);
					$queryBuilder->leftJoin(
						'Field\Entity\Value',
						$prop,
						'WITH',
						$prop .
							'.primary = object.id AND ' .
							$prop .
							'.entity = :custom_field_entity AND ' .
							$prop .
							'.field = ' .
							str_replace('customField_', '', $prop)
					);
					$queryBuilder->setParameter('custom_field_entity', $entityClass);
					$property = 'virtual.' . $prop . '.value';
				}

				if ($property == 'territory' && isset($filter['op'])) {
					$territories = $filters[$property];
					continue;
				}

				$continue = [];
				$anotherBindParam = null;
				$op = null;
				$values = null;
				$bindParamKey = 'param_' . $index;

				if (
					(isset($filter['op']) && $filter['op'] == 'isNull') ||
					(isset($filter['op']) && $filter['op'] == 'isNotNull')
				) {
					$value = null;
				} else {
					$value = $filter['val'];
				}
				if (isset($value['min']) || isset($value['max'])) {
					$op = 'number';
				} else {
					if (!isset($filter['op'])) {
						continue;
					}
					$op = $filter['op'];
				}

				if (strpos($property, '.') === false) {
					$property = 'object.' . $property;
				}
				$isVirtual = strpos($property, 'virtual.') !== false;
				$property = str_replace('virtual.', '', $property);

				$matches = [];
				if ($value && !is_array($value) && preg_match_all('/{([^{}]+)}+/', $value, $matches)) {
					$values = $matches[1];
				}

				if (!isset($values)) {
					$values = [$value];
				}

				$operator = $op;
				foreach ($values as $key => $value) {
					switch ($op) {
						case 'cnt':
							$operator = 'like';
							$values[$key] = '%' . $value . '%';
							break;
						case 'ncnt':
							$operator = 'notLike';
							$values[$key] = '%' . $value . '%';
							break;
						case 'scnt':
							$operator = 'like';
							$values[$key] = $value . '%';
							break;
						case 'sncnt':
							$operator = 'notLike';
							$values[$key] = $value . '%';
							break;
						case 'ecnt':
							$operator = 'like';
							$values[$key] = '%' . $value;
							break;
						case 'encnt':
							$operator = 'notLike';
							$values[$key] = '%' . $value;
							break;
						case 'eq':
							$operator = 'eq';
							if (is_array($value)) {
								$operator = 'in';
							}
							break;
						case 'neq':
							$operator = 'neq';
							if (is_array($value)) {
								$operator = 'notIn';
							}
							break;
						case 'isNull':
						case 'isnull':
							$operator = 'isNull';
							break;
						case 'isNotNull':
							$operator = 'isNotNull';
							break;
						case 'isEmpty':
						case 'isempty':
							$continue[] = $key;
							$queryBuilder->$whereMode($property . ' IS EMPTY');
							break;
						case 'member':
							$continue[] = $key;
							$operator = 'isMemberOf';
							$queryBuilder->expr()->isMemberOf($value, $property);
							break;
						case 'period':
							$start = null;
							$end = null;

							if (isset($value['start'])) {
								$start = DateTime::createFromFormat('d/m/Y H:i', $value['start']);
								if (!$start) {
									$start = DateTime::createFromFormat('d/m/Y', $value['start']);
									if ($start) {
										$start->setTime(00, 00, 00);
									}
								}
							}

							if (isset($value['end'])) {
								$end = DateTime::createFromFormat('d/m/Y H:i', $value['end']);
								if (!$end) {
									$end = DateTime::createFromFormat('d/m/Y', $value['end']);
									if ($end) {
										$end->setTime(23, 59, 59);
									}
								}
							}

							if ($start && $end) {
								$anotherBindParam = ':param_end_' . uniqid();
								$operator = 'between';
								$values[$key] = $start;

								$queryBuilder->setParameter($anotherBindParam, $end);
							} elseif ($start) {
								$operator = 'gte';
								$values[$key] = $start;
							} elseif ($end) {
								$operator = 'lte';
								$values[$key] = $end;
							} else {
								continue 3;
							}

							break;
						case 'calendar':
							$start = null;
							$end = null;
							$col = null;

							if (isset($value['start'])) {
								$start = DateTime::createFromFormat('d/m/Y H:i', $value['start']);
								if (!$start) {
									$start = DateTime::createFromFormat('d/m/Y', $value['start']);
									if ($start) {
										$start->setTime(00, 00, 00);
									}
								}
							}

							if (isset($value['end'])) {
								$end = DateTime::createFromFormat('d/m/Y H:i', $value['end']);
								if (!$end) {
									$end = DateTime::createFromFormat('d/m/Y', $value['end']);
									if ($end) {
										$end->setTime(23, 59, 59);
									}
								}
							}

							if (isset($value['col'])) {
								$col = 'object.' . $value['col'];
							}

							if ($start && $end && $col) {
								$queryBuilder->$whereMode(
									'
                                (' .
										$property .
										' >= :start_' .
										$bindParamKey .
										' AND ' .
										$property .
										' <= :end_' .
										$bindParamKey .
										')
                                OR
                                (' .
										$property .
										' <= :start_' .
										$bindParamKey .
										' AND ' .
										$col .
										' >= :start_' .
										$bindParamKey .
										')
                            '
								);

								$queryBuilder->setParameter('start_' . $bindParamKey, $start);
								$queryBuilder->setParameter('end_' . $bindParamKey, $end);

								$continue[] = $key;
							} else {
								continue 3;
							}
							break;
						case 'before':
							$property = 'DATEDIFF(' . $property .', CURRENT_DATE())';
							$operator = 'gte';
							$values[$key] = $value['days'];
							break;

						case 'lessThan':
							$property = 'DATEDIFF(CURRENT_DATE(), ' . $property . ')';
							$operator = 'between';
							$anotherBindParam = ':param_max_' . uniqid();
							$queryBuilder->setParameter($anotherBindParam, 0);
							$values[$key] = $value['days'] * -1;
							break;

						case 'beforeStrict':
							$property = 'DATEDIFF(CURRENT_DATE(), ' . $property . ')';
							$operator = 'eq';
							$values[$key] = $value['days'] * -1;

							break;
						case 'afterStrict':
							$property = 'DATEDIFF(CURRENT_DATE(), ' . $property . ')';
							$operator = 'eq';
							$values[$key] = $value['days'];
							break;
						case 'after':
							$property = 'DATEDIFF(' . $property . ', CURRENT_DATE())';
							$operator = 'lte';
							$values[$key] = $value['days'] * -1;
							break;
						case 'number':
							$min = null;
							$max = null;

							if (isset($value['min'])) {
								$min = $value['min'];
							}

							if (isset($value['max'])) {
								$max = $value['max'];
							}

							if ($min && $max) {
								$anotherBindParam = ':param_max_' . uniqid();
								$operator = 'between';
								$values[$key] = $min;

								$queryBuilder->setParameter($anotherBindParam, $max);
							} elseif ($min) {
								$operator = 'gte';
								$values[$key] = $min;
							} elseif ($max) {
								$operator = 'lte';
								$values[$key] = $max;
							} else {
								$continue[] = $key;
							}
							break;
						case 'sa_cnt':
							$queryBuilder->$whereMode(
								'
                                (' .
									$property .
									' LIKE :start_' .
									$bindParamKey .
									'
                                OR
                                ' .
									$property .
									' LIKE :end_' .
									$bindParamKey .
									'
                                OR
                                ' .
									$property .
									' LIKE :middle_' .
									$bindParamKey .
									'
                                OR
                                ' .
									$property .
									' = :alone_' .
									$bindParamKey .
									')
                            '
							);

							$queryBuilder->setParameter('start_' . $bindParamKey, $value . ',%');
							$queryBuilder->setParameter('middle_' . $bindParamKey, '%,' . $value . ',%');
							$queryBuilder->setParameter('end_' . $bindParamKey, '%,' . $value);
							$queryBuilder->setParameter('alone_' . $bindParamKey, $value);

							$continue[] = $key;
							break;
						case 'inArray':
							$q = [];
							$p = [];
							foreach ($value as $i => $_value) {
								$q[] = $property . ' LIKE :' . $bindParamKey . '_' . $i;
								$p[$bindParamKey . '_' . $i] = '%' . $_value . '%';
							}
							if ($q) {
								$queryBuilder->$whereMode(implode(' AND ', $q));
								$queryBuilder->setParameters($p);
							}
							$continue[] = $key;
							break;
						case 'notInArray':
							$q = [];
							$p = [];
							foreach ($value as $i => $_value) {
								$q[] = $property . ' NOT LIKE :' . $bindParamKey . '_' . $i;
								$p[$bindParamKey . '_' . $i] = '%' . $_value . '%';
							}
							if ($q) {
								$queryBuilder->$whereMode(implode(' AND ', $q));
								$queryBuilder->setParameters($p);
							}
							$continue[] = $key;
							break;
						case 'group':
							$continue[] = $key;
							$this->keywordFilters(
								$entityClass,
								$property,
								$value['op'],
								$value['group'],
								$value['tree'] === 'true',
								$value['val'],
								$queryBuilder
							);
							break;
						case 'service':
							$continue[] = $key;
							$this->serviceFilters($entityClass, $property, $value['op'], $value['val'], $queryBuilder);
							break;
					}
				}

				$expressions = [];
				foreach ($values as $key => $value) {
					if (!in_array($key, $continue)) {
						$expressions[$key] = [
							'value' => $value,
							'bindParamKey' => $bindParamKey . '_' . $key,
							'anotherBindParam' => $anotherBindParam ? $anotherBindParam : null,
						];
					}
				}

				if (count($expressions) === 1) {
					if (isset($filter['having']) && $filter['having'] === true) {
						$queryBuilder->$havingMode(
							$queryBuilder
								->expr()
								->$operator(
									$property,
									':' . $expressions[0]['bindParamKey'],
									$expressions[0]['anotherBindParam']
								)
						);
					} else {
						$queryBuilder->$whereMode(
							$queryBuilder
								->expr()
								->$operator(
									$property,
									':' . $expressions[0]['bindParamKey'],
									$expressions[0]['anotherBindParam']
								)
						);
					}

					if ($operator !== 'isNull' && $operator !== 'isNotNull') {
						$queryBuilder->setParameter($expressions[0]['bindParamKey'], $expressions[0]['value']);
					}
				} else {
					$methodExpressions = [];
					foreach ($expressions as $expression) {
						$methodExpressions[] = $queryBuilder
							->expr()
							->$operator(
								$property,
								':' . $expression['bindParamKey'],
								$expression['anotherBindParam']
							);
					}

					if (isset($filter['having']) && $filter['having'] === true) {
						$queryBuilder->$havingMode($queryBuilder->expr()->orX(...$methodExpressions));
					} else {
						$queryBuilder->$whereMode($queryBuilder->expr()->orX(...$methodExpressions));
					}

					if ($operator !== 'isNull' && $operator !== 'isNotNull') {
						foreach ($expressions as $expression) {
							$queryBuilder->setParameter($expression['bindParamKey'], $expression['value']);
						}
					}
				}

				$index++;
			}
		}

		if ($keywords) {
			foreach ($keywords as $property => $filter) {
				$group = str_replace('keyword.', '', $property);
				$shouldTree = isset($filter['tree']) ? $filter['tree'] == 'true' : false;

				$this->keywordFilters(
					$entityClass,
					'object.id',
					$filter['op'],
					$group,
					$shouldTree,
					isset($filter['val']) ? $filter['val'] : null,
					$queryBuilder,
					null,
					$master
				);
			}
		}

		/*if ($customFields) {
            foreach ($customFields as $property => $filter) {
                $prop = str_replace('virtual.', '', $property);
                $queryBuilder->addSelect('(SELECT v.value FROM Field\Entity\Value v WHERE v.primary = object.id AND v.entity = :custom_field_entity AND v.field = :' . $prop . ') AS HIDDEN ' . $prop);
                $queryBuilder->setParameter('custom_field_entity', str_replace('\\', '\\\\', $this->entityClass));
                $queryBuilder->setParameter($prop, str_replace('customField_', '', $prop));
            }
        }*/

		if ($territories) {
			if ($territories['op'] === 'isNull') {
				$queryBuilder->andWhere('
                    object.id NOT IN (SELECT terr.primary FROM Map\Entity\Association terr WHERE terr.entity = :terr_entity AND terr.primary = object.id)
                ');
			} elseif ($territories['op'] === 'isNotNull') {
				$queryBuilder->andWhere('
                    object.id IN (SELECT terr.primary FROM Map\Entity\Association terr WHERE terr.entity = :terr_entity AND terr.primary = object.id)
                ');
			} else {
				$territoryRepository = $this->_entityManager->getRepository('Map\Entity\Territory');
				$territoriesValues = $territories['val'];
				$shouldTree = isset($territories['tree']) ? $territories['tree'] == 'true' : false;
				if ($shouldTree) {
					$territoriesValues = [];
					if (is_array($territories['val'])) {
						foreach ($territories['val'] as $id) {
							$territory = $territoryRepository->find($id);
							if ($territory) {
								$territoriesValues = array_merge($territoriesValues, $territory->getFlatTree(true));
							}
						}
					} else {
						$territory = $territoryRepository->find($territories['val']);
						if ($territory) {
							$territoriesValues = array_merge($territoriesValues, $territory->getFlatTree(true));
						}
					}
				}
				$queryBuilder->andWhere(
					'
                    object.id ' .
						($territories['op'] === 'neq' || $territories['op'] === 'isNull' ? 'NOT' : '') .
						' IN (SELECT terr.primary FROM Map\Entity\Association terr WHERE terr.entity = :terr_entity AND terr.primary = object.id AND terr.territory IN (:terr_values))
                '
				);

				//$queryBuilder->innerJoin('Map\Entity\Association', 'terr', 'WITH', 'terr.entity = :terr_entity AND terr.primary = object.id AND terr.territory ' . ( $territories['op'] === 'neq' ? 'NOT': '' ) . ' IN (:terr_values)');
				$queryBuilder->setParameter('terr_values', $territoriesValues);
			}
			$queryBuilder->setParameter('terr_entity', $entityClass);
		}
	}

	/**
	 * Add keywords filters for any property.
	 *
	 * @param string $property
	 * @param string $op
	 * @param string|int $group
	 * @param bool $shouldTree
	 * @param array $values
	 * @param QueryBuilder|null $queryBuilder
	 * @param string|null $entity
	 * @param bool $master
	 *
	 * @return array|void
	 */
	public function keywordFilters(
		$entityClass,
		$property,
		$op,
		$group,
		$shouldTree,
		$values,
		$queryBuilder = null,
		$entity = null,
		$master = false
	) {
		if (!$op || !$group) {
			return;
		}

		if (!$entity) {
			switch ($property) {
				case 'object.id':
				case 'object.parent':
					$entity = $entityClass;
					break;
				case 'object.user':
				case 'users.id':
				case 'members.id':
					$entity = 'User\Entity\User';
					break;
				case 'object.project':
					$entity = 'Project\Entity\Project';
					break;
				default:
					return;
			}
		}

		$ret = [
			'restrictions' => [],
			'parameters' => [],
		];

		$prefix = $property === 'object.id' ? '' : str_replace('.', '_', $property . '_');

		if (in_array($op, ['isNull', 'isNotNull'])) {
			$groupProperty = $master ? 'master' : 'id';			
			$restriction =
				$property .
				($op == 'isNull' ? ' NOT ' : '') .
				' IN (
                SELECT kkw' .
				$group .
				'.primary
                FROM Keyword\Entity\Association kkw' .
				$group .
				'
                JOIN kkw' .
				$group .
				'.keyword kkwk' .
				$group .
				'
                JOIN kkwk' .
				$group .
				'.group kkwg' .
				$group .
				'
                WHERE kkw' .
				$group .
				'.entity = :' .
				$prefix .
				'kw_entity
                AND kkw' .
				$group .
				'.primary = ' .
				$property .
				'
                AND kkwg' .
				$group .
				'.' . $groupProperty . ' = ' .
				$group .
				'
            )';
			if ($queryBuilder) {
				$queryBuilder->andWhere($restriction);
				$queryBuilder->setParameter($prefix . 'kw_entity', $entity);
				return;
			} else {
				$ret['restrictions'][] = $restriction;
				$ret['parameters'][$prefix . 'kw_entity'] = $entity;
				return $ret;
			}
		}

		$keywordsValues = [
			'in' => [],
			'notIn' => [],
			'groups' => [],
		];

		foreach ($values as $id) {
			$keyword = $this->_entityManager
				->getRepository('Keyword\Entity\Keyword')
				->findOneBy([
					$master ? 'master' : 'id' => $id,
				]);

			if ($keyword) {
				$tree = $shouldTree ? $keyword->getFlatTree(true) : [$keyword->getId()];

				if ($op === 'eq') {
					$keywordsValues['in'] = array_merge($keywordsValues['in'], $tree);
				} else {
					$keywordsValues['notIn'] = array_merge($keywordsValues['notIn'], $tree);
				}

				$keywordsValues['groups'][] = $keyword->getGroup()->getId();
			}
		}

		if ($keywordsValues['in']) {
			$restriction =
				$property .
				' IN (
                SELECT kw' .
				$group .
				'.primary
                FROM Keyword\Entity\Association kw' .
				$group .
				'
                WHERE kw' .
				$group .
				'.entity = :' .
				$prefix .
				'kw_entity
                AND kw' .
				$group .
				'.primary = ' .
				$property .
				'
                AND kw' .
				$group .
				'.keyword IN (:' .
				$prefix .
				'kw_values_' .
				$group .
				')
            )';

			if ($queryBuilder) {
				$queryBuilder->andWhere($restriction);
				$queryBuilder->setParameter($prefix . 'kw_entity', $entity);
				$queryBuilder->setParameter($prefix . 'kw_values_' . $group, $keywordsValues['in']);
			} else {
				$ret['restrictions'][] = $restriction;
				$ret['parameters'][$prefix . 'kw_entity'] = $entity;
				$ret['parameters'][$prefix . 'kw_values_' . $group] = $keywordsValues['in'];
			}
		}

		if ($keywordsValues['notIn']) {
			$restriction =
				$property .
				' NOT IN (
                SELECT nkw' .
				$group .
				'.primary
                FROM Keyword\Entity\Association nkw' .
				$group .
				'
                WHERE nkw' .
				$group .
				'.entity = :' .
				$prefix .
				'kw_entity
                AND nkw' .
				$group .
				'.primary = ' .
				$property .
				'
                AND nkw' .
				$group .
				'.keyword IN (:' .
				$prefix .
				'kw_nvalues_' .
				$group .
				')
            )';

			if ($queryBuilder) {
				$queryBuilder->andWhere($restriction);
				$queryBuilder->setParameter($prefix . 'kw_entity', $entity);
				$queryBuilder->setParameter($prefix . 'kw_nvalues_' . $group, $keywordsValues['notIn']);
			} else {
				$ret['restrictions'][] = $restriction;
				$ret['parameters'][$prefix . 'kw_entity'] = $entity;
				$ret['parameters'][$prefix . 'kw_nvalues_' . $group] = $keywordsValues['notIn'];
			}
		}

		if ($op !== 'neqOrIsNull' && ($keywordsValues['in'] || $keywordsValues['notIn'])) {
			$restriction =
				$property .
				' IN (
                SELECT kkw' .
				$group .
				'.primary
                FROM Keyword\Entity\Association kkw' .
				$group .
				'
                JOIN kkw' .
				$group .
				'.keyword kkwk' .
				$group .
				'
                JOIN kkwk' .
				$group .
				'.group kkwg' .
				$group .
				'
                WHERE kkw' .
				$group .
				'.entity = :' .
				$prefix .
				'kw_entity
                AND kkw' .
				$group .
				'.primary = ' .
				$property .
				'
                AND kkwg' .
				$group .
				' IN (:' .
				$prefix .
				'kw_groups_' .
				$group .
				')
            )';

			if ($queryBuilder) {
				$queryBuilder->andWhere($restriction);
				$queryBuilder->setParameter($prefix . 'kw_groups_' . $group, $keywordsValues['groups']);
			} else {
				$ret['restrictions'][] = $restriction;
				$ret['parameters'][$prefix . 'kw_groups_' . $group] = $keywordsValues['groups'];
			}
		}

		if ($queryBuilder) {
			return;
		} else {
			return $ret;
		}
	}

	protected function serviceFilters( $entityClass,$property, $op, $values, $queryBuilder = null, $entity = null)
	{
		if (!$entity) {
			switch ($property) {
				case 'object.id':
				case 'object.parent':
					$entity = $entityClass;
					break;
				case 'object.user':
				case 'users.id':
				case 'members.id':
					$entity = 'User\Entity\User';
					break;
				default:
					return;
			}
		}

		$ret = [
			'restrictions' => [],
			'parameters' => [],
		];

		$prefix = $property === 'object.id' ? '' : str_replace('.', '_', $property . '_');

		if ($op === 'isNull') {
			$restriction =
				$property .
				' NOT IN (SELECT serv.primary FROM User\Entity\Association serv WHERE serv.entity = :' .
				$prefix .
				'serv_entity AND serv.primary = ' .
				$property .
				')';
		} elseif ($op === 'isNotNull') {
			$restriction =
				$property .
				' IN (SELECT serv.primary FROM User\Entity\Association serv WHERE serv.entity = :' .
				$prefix .
				'serv_entity AND serv.primary = ' .
				$property .
				')';
		} elseif ($op === 'service') {
			$this->serviceFilters($entityClass, $property, $values['op'], $values['val'], $queryBuilder, $entity);
		} else {
			$restriction =
				$property .
				' ' .
				($op === 'neq' || $op === 'isNull' ? 'NOT' : '') .
				' IN (SELECT serv.primary FROM User\Entity\Association serv WHERE serv.entity = :' .
				$prefix .
				'serv_entity AND serv.primary = ' .
				$property .
				' AND serv.service IN (:' .
				$prefix .
				'serv_values))';
			$ret['parameters'][$prefix . 'serv_values'] = $values;
		}
		$ret['parameters'][$prefix . 'serv_entity'] = $entity;
		$ret['restrictions'][] = $restriction;

		if ($queryBuilder) {
			foreach ($ret['restrictions'] as $restriction) {
				$queryBuilder->andWhere($restriction);
			}
			foreach ($ret['parameters'] as $key => $param) {
				$queryBuilder->setParameter($key, $param);
			}
			return;
		} else {
			return $ret;
		}
	}

}
