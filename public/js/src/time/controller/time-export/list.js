(function (app) {

    app.controller('timeExportListController', [
        'timeExportService', '$scope', 'flashMessenger', 'translator',
        function timeExportListController(timeExportService, $scope, flashMessenger, translator) {
            var self = this;

            self.timeExports = [];
            self.options     = {
                name      : 'timeExports-list',
                col       : [
                    'id',
                    'name',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort      : 'name',
                order     : 'asc',
                pager     : true,
                getItems  : function getData(params, callback, abort) {
                    timeExportService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function (event) {
                if (event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};

            self.editLine         = function editLine(timeExport) {
                self.editedLineErrors = {};
                self.editedLine       = timeExport;
                self.editedLineFields = angular.copy(timeExport);
            };

            self.saveEditedLine = function saveEditedLine(timeExport) {
                self.editedLineErrors = {};
                timeExportService.save(angular.extend({}, timeExport, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        timeExport            = angular.extend(timeExport, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;
                        flashMessenger.success(translator('time_export_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteTimeExport = function deleteTimeExport(timeExport) {
                if (confirm(translator('time_export_question_delete').replace('%1', timeExport.name))) {
                    timeExportService.remove(timeExport).then(function (res) {
                        if (res.success) {
                            self.timeExports.splice(self.timeExports.indexOf(timeExport), 1);
                            flashMessenger.success(translator('time_export_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
