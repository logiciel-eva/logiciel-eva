<?php

namespace Keyword\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Core\Utils\ArrayUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use User\Entity\User;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Group
 * @package Keyword\Entity
 *
 * @ORM\Entity(repositoryClass="Keyword\Repository\GroupRepository")
 * @ORM\Table(name="keyword_group")
 */
class Group extends BasicRestEntityAbstract
{
    const GROUP_TYPE_KEYWORD   = 'keyword';
    const GROUP_TYPE_REFERENCE = 'reference';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * Type of the group
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type = self::GROUP_TYPE_KEYWORD;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Keyword\Entity\Keyword", mappedBy="group", orphanRemoval=true, cascade={"all"})
     */
    protected $keywords;

    /**
     * @var array
     *
     * @ORM\Column(type="simple_array", nullable=true)
     */
    protected $entities;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $multiple;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $master;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $archived = false;

    public function __construct()
    {
        $this->keywords = new ArrayCollection();
        $this->entities = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return ArrayCollection
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param ArrayCollection $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @param ArrayCollection $keywords
     */
    public function addKeywords($keywords)
    {
        foreach ($keywords as $keyword) {
            $keyword->setGroup($this);
            $this->getKeywords()->add($keyword);
        }
    }

    /**
     * @param ArrayCollection $keywords
     */
    public function removeKeywords($keywords)
    {
        foreach ($keywords as $keyword) {
            $keyword->setGroup(null);
            $this->getKeywords()->removeElement($keyword);
        }
    }

    /**
     * @return array
     */
    public function getEntities()
    {
        return $this->entities;
    }

    /**
     * @param array $entities
     */
    public function setEntities($entities)
    {
        $this->entities = $entities;
    }

    /**
     * @return boolean
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * @return boolean
     */
    public function isMultiple()
    {
        return $this->getMultiple();
    }

    /**
     * @param boolean $multiple
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    }

    /**
     * @return int
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * @param int $master
     */
    public function setMaster($master)
    {
        $this->master = $master;
    }

    /**
     * @return bool
     */
    public function isArchived()
    {
        return $this->archived;
    }

    /**
     * @return bool
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param bool $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

    public function getMaxLevel()
    {
        $level = 0;
        foreach ($this->keywords as $keyword) {
            $keywordLevel = $keyword->getMaxLevel();
            $level        = $keywordLevel > $level ? $keywordLevel : $level;
        }

        return $level;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'type',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'        => 'multiple',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'     => 'entities',
                'required' => false,
            ],
            [
                'name'     => 'master',
                'required' => false,
            ],
            [
                'name'        => 'archived',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
        ]);

        return $inputFilter;
    }

    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return true;
    }

    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::GROUP_TYPE_KEYWORD,
            self::GROUP_TYPE_REFERENCE,
        ];
    }
}
