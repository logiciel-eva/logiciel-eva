(app => {
    app.controller('gbcpController', [
        'queryService',
        '$rootScope',
        'translator',
        '$http',
        '$scope',
        function(queryService, $rootScope, translator, $http, $scope) {
            const self = this;

            self.rows = [];

            self.filters = {
                project: {},
                posteExpense: {},
                posteIncome: {},
                expense: {},
                income: {},
            };

            self.loading = false;

            self.options = {
                name: 'budget-analysis-gbcp-list',
                autoload: false,
                getItems: () => {},
            };

            self.applyFilters = () => {
                self.loading = true;

                $http({
                    url: '/analysis/budget/gbcp?' + $.param({ filters: self.filters }),
                    method: 'GET',
                }).then(res => {
                    self.rows = res.data;
                    self.loading = false;
                });
            };

            self.getTotal = type => {
                let total = 0;

                for (const row of self.rows) {
                    total += row['total'][type] || 0;
                }

                return total;
            };

            self.resetFilters = () => {
                self.filters.project = {};
                self.filters.posteExpense = {};
                self.filters.posteIncome = {};
                self.filters.expense = {};
                self.filters.income = {};

                self.applyFilters();
            };

            self.export = () => {
                let columns = [];

                for (const key in $scope.__tb.columns) {
                    if (
                        $scope.__tb.columns[key].visible &&
                        !['selection', 'actions', 'tree'].includes(key)
                    ) {
                        columns = [...columns, key];
                    }
                }

                window.open(
                    '/analysis/budget/gbcp?' +
                        $.param({ filters: self.filters, export: true, columns }),
                );
            };

            // QUERIES
            self.queries = [];
            self.query = {
                list: 'budget-analysis-gbcp-list',
            };
            self.errors = {
                fields: {},
            };
            self.loadingQueries = false;
            self.loadingSaveQuery = false;

            const queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = () => {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list,
                                    },
                                },
                            },
                        },
                    })
                    .then(
                        res => {
                            self.queries = res.rows;
                            self.loadingQueries = false;
                        },
                        () => {
                            self.loadingQueries = false;
                        },
                    );
            };

            self.loadQuery = query => {
                self.filters = angular.copy(query.filters);

                if (angular.isArray(self.filters.project)) {
                    self.filters.project = {};
                }
                if (angular.isArray(self.filters.posteExpense)) {
                    self.filters.posteExpense = {};
                }
                if (angular.isArray(self.filters.posteIncome)) {
                    self.filters.posteIncome = {};
                }
                if (angular.isArray(self.filters.expense)) {
                    self.filters.expense = {};
                }
                if (angular.isArray(self.filters.income)) {
                    self.filters.income = {};
                }

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = () => {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService.save(self.query, { col: queryApiCol }).then(
                    function(res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    },
                    function(err) {
                        for (const field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    },
                );
            };

            self.deleteQuery = query => {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(
                        function() {
                            self.queries.splice(self.queries.indexOf(query), 1);
                            self.loadingSaveQuery = false;
                        },
                        function() {
                            self.loadingSaveQuery = false;
                        },
                    );
                }
            };
        },
    ]);
})(window.EVA.app);
