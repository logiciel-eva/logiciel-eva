(function (app) {

    app.factory('contactService', [
        'restFactoryService', '$http', '$q',
        function contactService(restFactoryService, $http, $q) {
            var url = '/api/directory/contact';

            var methods = restFactoryService.create(url);

            methods.fusion = function (contactToKeep_id, contactToFusion_id, type) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: url + '/' + contactToKeep_id + '/fusion',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ contactToFusion_id : contactToFusion_id, type: type })
                }).then(function(res) {
                    deferred.resolve(res.data)
                }, function(err) {
                    deferred.reject(err)
                });

                return deferred.promise;
            };

            return methods;
        }
    ])
})(window.EVA.app);
