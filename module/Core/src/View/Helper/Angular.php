<?php

namespace Core\View\Helper;

use Laminas\Http\Response;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\BasePath;

/**
 * Class Angular
 *
 * @package Core\View\Helper
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class Angular extends AbstractHelper
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var array
     */
    protected $files = [];

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @param string|null $file
     * @param string $mode
     * @return $this
     */
    public function __invoke($file = null, $mode = 'append')
    {
        if ($file) {
            if (!in_array($file, $this->files)) {
                $this->{$mode.'File'}($file);
            }
        }
        return $this;
    }

    /**
     * @param string $file
     */
    protected function appendFile($file)
    {
        $this->files[] = $file;
    }

    /**
     * @param string $file
     */
    protected function prependFile($file)
    {
        array_unshift($this->files, $file);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $env      = getenv('APP_ENV') ? : 'development';
        $scripts  = '';
        $basepath = $this->getView()->plugin('basepath');

        $files = $this->files;
        if ($env === 'production') {
            $mvcEvent = $this->serviceLocator->get('Application')->getMvcEvent();
            if ($mvcEvent->getResponse()->getStatusCode() == Response::STATUS_CODE_200) {
                $files = [$this->minifyFile($basepath, $mvcEvent)];
            }
        }

        if($env === 'production') {
            $version = $this->version();
        } else {
            $version = rand(0, 10000);
        }

        foreach ($files as $script) {
            $scripts .= '<script type="text/javascript" src="'.$basepath($script).'?v=' . $version . '"></script>';
        }

        return $scripts;
    }

    protected function version()
    {
        $rev = exec('git rev-parse --short HEAD');
        return $rev;
    }

    /**
     * @param BasePath $basepath
     * @return string
     */
    protected function minifyFile(BasePath $basepath, MvcEvent $mvcEvent)
    {
        $routeMatch = $mvcEvent->getRouteMatch();
        if ($routeMatch !== null) {
            $fileName   = str_replace('/', '_', $routeMatch->getMatchedRouteName()) . '.js';
            $filePath   = getcwd() . '/public/js/min/' . $fileName;

            if (!file_exists($filePath)) {
                $minifiedScripts = '';
                foreach ($this->files as $script) {
                    $minifiedScripts .= file_get_contents(getcwd() . '/public' . $basepath($script));
                }
                $minifiedScripts = $this->minify($minifiedScripts);

                $file = fopen($filePath, 'w');
                fwrite($file, $minifiedScripts);
                fclose($file);
            }

            return 'js/min/' . $fileName;
        }

        return null;
    }

    /**
     * @param string $input
     * @return mixed
     */
    protected function minify($input)
    {
        if (trim($input) === '') {
            return $input;
        }

        $regexps = [
            // Remove comment(s)
            '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
            // Remove white-space(s) outside the string and regex
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
            // Remove the last semicolon
            '#;+\}#',
            // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
            '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
            // --ibid. From `foo['bar']` to `foo.bar`
            '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
        ];

        $replacements = [
            '$1',
            '$1$2',
            '}',
            '$1$3',
            '$1.$3'
        ];

        return preg_replace($regexps, $replacements, $input);
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
