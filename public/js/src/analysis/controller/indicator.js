(function(app) {

    app.controller('indicatorController', [
        '$scope',
        function ($scope) {
            var self = this;

            self.init = function (isMaster) {
                $scope.isMaster = isMaster;
            };
        }
    ]);

    app.controller('filterController', [
        '$scope',
        function ($scope) {
            var self = this;

            self.filtersKey = null;

            self.init = function (filtersKey) {
                self.filtersKey = filtersKey;
            };

            var events = [
                'analysis-indicator-recap-query-loaded',
                'analysis-indicator-recap-client-query-loaded',
                'analysis-indicator-recap-with-measures-query-loaded'
            ];

            events.forEach(function (event) {
                $scope.$on(event, function (e, filters) {
                    showFilters(filters[self.filtersKey]);
                });
            });

            function showFilters(filters) {
                $scope.__filters = {};

                for (var i in filters) {
                    if (
                        filters[i] !== null &&
                        (
                            typeof filters[i].val !== 'undefined' ||
                            filters[i].op === 'isNull'
                        )
                    ) {
                        $scope.__filters[i] = true;
                    } else {
                        $scope.__filters[i] = false;
                    }
                }
            }
        }
    ]);
})(window.EVA.app);
