<?php

namespace Age\Service;

use Age\Import\AbstractConnection;
use Age\Import\Connection\MysqlConnection;
use Age\Import\Connection\SQLServerConnection;
use Age\Model\AgeCheckRequest;
use Age\Model\AgeIdentifier;
use Age\Model\AgeOperation;
use Age\Model\AgeRequest;
use Bootstrap\Entity\Client;
use Exception;
use GuzzleHttp\Client as GuzzleHttpClient;
use Project\Entity\AgeSynchronisation;
use Project\Entity\Project;
use Project\Repository\ProjectRepository;
use User\Auth\AuthStorage;

class AgeSynchronisationService implements AgeSynchronisationServiceInterface
{

    private $_ageModule = null;

    /**
     * @var AbstractConnection
     */
    private $_connection = null;

    /**
     * @var GuzzleHttpClient
     */
    private $_httpClient = null;

    /**
     * @var
     */
    private $_entityManager;

    /**
     * @var AuthStorage
     */
    private $_authStorage;

    /**
     * @param Client client
     * @param ProjectRepository projectRepository
     */
    public function __construct(Client $client, $entityManager, AuthStorage $authStorage)
    {
        $this->_entityManager = $entityManager;
        $this->_authStorage = $authStorage;
        $modules = $client->getModules();
        $this->_ageModule = isset($modules['age']) ? $modules['age'] : null;
        $this->_httpClient = new GuzzleHttpClient(['verify' => false]);
    }

    public function isSynchronisationActive()
    {
        return $this->canSynchronize();
    }

    private function initDatabaseConnection()
    {
        if (null !== $this->_connection) {
            return;
        }
        $configuration = $this->_ageModule['configuration'];
        if (!isset($configuration['dbType'])) {
            $configuration['dbType'] = 'mysql';
        }
        switch ($configuration['dbType']) {
            case 'mysql':
                $this->_connection = new MysqlConnection($configuration);
                break;
            case 'sql-server':
                $this->_connection = new SQLServerConnection($configuration);
                break;
            default:
                throw new \Exception('This DB type is not handled');
        }
    }


    public function initForProject($project)
    {
        if (!isset($project) || !$this->canSynchronize()) {
            return;
        }
        $this->initAgeSynchronisationForProject($project);
        $this->checkSynchronisationWithAge($project);
    }

    private function checkSynchronisationWithAge($project)
    {
        $code = $project->getAccountingCode();
        if (null === $code) {
            return;
        }

        // only a pending synchronization in QPilote is checked in AGE
        $projectSynchronisation = $project->getAgeSynchronisation();
        $synchronizedInAge = $this->isProjectSynchronizedInAge($code);
        if ($synchronizedInAge) {
            $projectSynchronisation->setStatus(AgeSynchronisation::AGE_SYNCHRO_STATUS_SYNCHRONISED);
        } elseif (!$synchronizedInAge && $projectSynchronisation->isSynchronizedStatus()) {
            $projectSynchronisation->reset();
        }
        $this->saveAgeSynchronisation($projectSynchronisation);
    }

    private function initAgeSynchronisationForProject($project)
    {
        if (!isset($project) || null !== $project->getAgeSynchronisation()) {
            return;
        }
        $ageSynchronisation = new AgeSynchronisation();
        $ageSynchronisation->setProject($project);
        $project->setAgeSynchronisation($ageSynchronisation);
        $this->saveAgeSynchronisation($ageSynchronisation);
    }

    private function saveAgeSynchronisation(AgeSynchronisation $ageSynchronisation)
    {
        $this->_entityManager->persist($ageSynchronisation);
        $this->_entityManager->flush();
    }


    /**
     * Check in client configuration that Age is activated and that required configuration is setup
     * 
     * @return boolean
     */
    private function canSynchronize()
    {
        return null !== $this->_ageModule && $this->_ageModule['active'] && isset($this->_ageModule['configuration']['apiUri']) && '' !== $this->_ageModule['configuration']['apiUri'];
    }

    /**
     * Check whether project is synchronized in AGE through account code
     * 
     * @param string account code
     * @return bool
     */
    private function isProjectSynchronizedInAge(string $code)
    {
        if (null === $code) {
            return false;
        }
        $checkRequest = new AgeCheckRequest();
        $checkRequest->setReference("referenceRequete");
        $checkRequest->setEtablissement($this->_ageModule['configuration']['identifier']);
        $identifier = new AgeIdentifier();
        $identifier->setId("");
        $identifier->setCode(strtoupper($code));
        $identifier->setReferenceExterne("");
        $checkRequest->addIdentifiant($identifier);

        $jsonRequest = json_encode($checkRequest);

        $authToken = $this->_ageModule['configuration']['apiAuthToken'];
        $url = $this->_ageModule['configuration']['apiUri'] . '/etape-workflow';
        try {
            $response = json_decode($this->_httpClient->post($url, [
                'headers' => ['Content-Type' => 'application/json', 'elap-authentication-token' => $authToken],
                'body' => $jsonRequest
            ])->getBody()->read(1024)); 
            return filter_var($response->isSuccess, FILTER_VALIDATE_BOOLEAN);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param Project project
     * 
     * @return an array of validation errors
     */
    public function createOperation(Project $project)
    {
        $errors = [];
        if (!$this->isValidForSynchronization($project, $errors)) {
            return $errors;
        }
        $this->initDatabaseConnection();

        $ageSynchronisation = $project->getAgeSynchronisation();

        $request = new AgeRequest();
        $request->setEtablissement($this->_ageModule['configuration']['identifier']);
        $request->setExercice(self::prepareDate($project->getProgrammingDate(), 'Y'));
        $request->setImportPartiel(false);
        $request->setWfEtapeInitiale("BROUILLON");
        $request->setWfTransition("");      // todo: put it in configuration
        $request->setNatureImport("OPE");
        $request->setReferenceExterne(strval($project->getId()));

        $operation = new AgeOperation();
        $operation->setReferenceExterne(strval($project->getId()));
        $operation->setCodeOperation(strtoupper($project->getCode()));
        if (null !== $project->getParent()) {
            //$operation->setReferenceExterneOperationParent(strval($project->getParent()->getId()));
            //$operation->setCodeOperationParent($project->getParent()->getCode());
        }
        $operation->setLibelle(strip_tags(html_entity_decode($project->getName())));
        $operation->setTypeOperation(0);    // operation type = 'operation'
        $operation->setSaisissableBudget($ageSynchronisation->estSaisissableAuBudget());
        $operation->setPresenteeBudget($ageSynchronisation->estPresenteeAuBudget());
        $operation->setSaisissableExecution($ageSynchronisation->estSaisissableEnExecution());
        $operation->setDateDebut(self::prepareDate($project->getPlannedDateStart(), 'Ymd'));
        $operation->setDateFin(self::prepareDate($project->getPlannedDateEnd(), 'Ymd'));
        $operation->setLimitativite(0);     // niveau de contrôle : sans contrôle
        $operation->setMontantGlobal(0);
        $operation->setDescription(strip_tags(html_entity_decode($project->getDescription())));

        $request->addOperation($operation);

        $jsonRequest = json_encode($request);
        $ageSynchronisation->setServiceRequest($jsonRequest);

        $authToken = $this->_ageModule['configuration']['apiAuthToken'];
        $url = $this->_ageModule['configuration']['apiUri'] . '/import';

        $response = $this->_httpClient->post($url, [
            'headers' => ['Content-Type' => 'application/json', 'elap-authentication-token' => $authToken],
            'body' => $jsonRequest
        ]);

        $ageSynchronisation->setServiceResponse($response->getBody()->read(1024));

        // when API call succeeds, update synchronisation data
        if (200 === $response->getStatusCode()) {
            $ageSynchronisation->setSynchronisedBy($this->_authStorage->getUser());
            $ageSynchronisation->setSynchronisedAt(new \DateTime());
            $ageSynchronisation->setStatus(AgeSynchronisation::AGE_SYNCHRO_STATUS_SYNCHRONISED);
        }

        $this->saveAgeSynchronisation($ageSynchronisation);

        if (200 !== $response->getStatusCode()) {
            throw new Exception("Synchronisation failed");
        }
        return $errors;
    }

    /**
     * @param Project project
     * @param Array errors
     */
    private function isValidForSynchronization($project, &$errors)
    {
        $canSynchronise = $this->canSynchronize()
            && null !== $project
            && null !== $project->getAgeSynchronisation() && $project->getAgeSynchronisation()->isPendingStatus();

        if (null === $project->getName()) {
            $errors[] = 'project_field_name';
        }
        if (null === $project->getAccountingCode()) {
            $errors[] = 'project_field_code';
        } else if (strlen($project->getAccountingCode()) > 20) {
            $errors[] = 'Le code est trop long (maximum 20 caractères)';
        }
        if (null === $project->getPlannedDateStart()) {
            $errors[] = 'project_field_plannedDateStart';
        }
        if (null === $project->getPlannedDateEnd()) {
            $errors[] = 'project_field_plannedDateEnd';
        }
        if (
            !$project->getAgeSynchronisation()->getBudgetSeizable()
            && !$project->getAgeSynchronisation()->getBudgetPresented()
            && !$project->getAgeSynchronisation()->getExecutionSeizable()
        ) {
            $errors[] = 'age_synchro_flag_label';
        }
        return $canSynchronise && 0 === count($errors);
    }

    private static function prepareDate($date, $format)
    {
        if (null === $date) {
            return null;
        }
        return intval($date->format($format));
    }
}
