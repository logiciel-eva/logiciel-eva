<?php

return [
    'module' => [
        'convention' => [
            'environments' => [
                'client'
            ],
            'active'   => true,
            'required' => false,
            'acl' => [
                'entities' => [
                    [
                        'name'     => 'convention',
                        'class'    => 'Convention\Entity\Convention',
                        'owner' => [],
                        'keywords' => true
                    ],
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
