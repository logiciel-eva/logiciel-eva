(function(app) {

    app.controller('loginController', [
        'controlService', '$http',
        function loginController(controlService, $http) {
            controlService.desactivate();

            var self = this;

            self.error   = false;
            self.loading = false;

            self.submitPartial = function (loginForm) {
                self.error = false;
                self.loading = true;
                $http({
                    method: 'POST',
                    url: '/user/auth/login?partial=true',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({
                        login: loginForm.login.$modelValue,
                        password: loginForm.password.$modelValue
                    })
                }).then(function(response) {
                    self.loading = false;
                    self.error   = response.data.error;
                    if (!response.data.error) {
                        $('.partial-login').remove();
                        controlService.start();
                    }
                })
            }
        }
    ])

})(window.EVA.app);
