(function (app) {

    app.controller('alertFormController', [
        'alertService', 'flashMessenger', 'translator', '$http',
        function alertFormController(alertService, flashMessenger, translator, $http) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'category',
                    'query.id',
                    'query.name',
                    'indicators.id',
                    'indicators.name',
                    'groupIndicators.id',
                    'groupIndicators.name',
                    'recipients.id',
                    'recipients.name',
                    'recipients.avatar',
                    'toManagers',
                    'toValidators',
                    'toMembers',
                    'subject',
                    'content',
                    'enabled',
                    'periodicity',
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.alert = {
                category: 'project',
                _rights: {
                    update: true
                }
            };

            self.init = function init(id, category) {
                self.alert.category = category;
                if (id) {
                    alertService.find(id, apiParams).then(function (res) {
                        self.alert = res.object;
                        if(self.alert.category === null || 
                            self.alert.category === ''){
                            self.alert.category = 'project';
                        }
                        self.panelTitle = self.alert.name;
                    });
                }
            };

            self.saveAlert = function saveAlert() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.alert.id;
                alertService.save(self.alert, apiParams).then(function (res) {
                    self.alert = res.object;
                    self.panelTitle = self.alert.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.alert.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('alert_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                });
            };

            self.deleteAlert = function deleteAlert() {
                if (confirm(translator('alert_question_delete').replace('%1', self.alert.name))) {
                    alertService.remove(self.alert).then(function (res) {
                        if (res.success) {
                            window.location = '/alert';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.enableAlert = function () {
                self.editedGroupErrors = {};

                alertService.save(self.alert, {
                    col: apiParams.col
                }).then(function (res) {
                    if (res.success) {
                        self.alert = angular.extend(self.alert, res.object);

                        if (self.alert.enabled == true) {
                            flashMessenger.success(translator('alert_message_enabled'));
                        } else {
                            flashMessenger.success(translator('alert_message_disabled'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }
                });
            };

            self.sendAlert = function (id) {
                $http.get('/alert/send/' + id).then(function (res) {
                    if (res.data.success) {
                        flashMessenger.success(translator('alert_sent'));
                    } else {
                        flashMessenger.error(res.data.message);
                    }
                });
            };

            self.insertVariable = function insertVariable(variable) {
                if (self.alert._rights.update) {
                    var editor = self.getEditor();
                    editor.insertContent('$' + variable + '$');
                }
            };

            self.getEditor = function () {
                var id = $('textarea[ui-tinymce]').attr('id');
                return tinyMCE.get(id);
            };
        }
    ]);

})(window.EVA.app);
