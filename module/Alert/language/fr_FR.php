<?php

return [
    'alert_module_title' => '[[ alert_entities | ucfirst ]]',
    'alert_campain_module_title' => '[[ alert_module_title ]] de campagne',

    'daily'   => 'journalier',
    'weekly'  => 'hebdomadaire',
    'monthly' => 'mensuel',

    'alert_entity'   => 'alerte',
    'alert_entities' => 'alertes',

    'alert_field_name'         => 'Nom',
    'alert_field_query'        => '[[ query_entity | ucfirst ]]',
    'alert_field_createdAt'    => '[[ field_created_at_f ]]',
    'alert_field_updatedAt'    => '[[ field_updated_at_f ]]',
    'alert_field_recipients'   => 'Destinataires',
    'alert_field_toManagers'   => '[[project_field_managers]]',
    'alert_field_toValidators' => '[[project_field_validators]]',
    'alert_field_toMembers'    => '[[project_field_members]]',
    'alert_field_subject'      => 'Objet',
    'alert_field_content'      => 'Corps',
    'alert_field_enabled'      => 'Activé ?',
    'alert_field_periodicity'  => 'Période d\'envoi',
    'alert_field_entity'       => 'Entité',
    'alert_field_category'       => 'Type d\'alerte',

    'alert_nav_list' => 'Liste des {{ __tb.total }} [[ alert_entities ]]',
    'alert_nav_form' => 'Créer une [[ alert_entity ]] de fiche',
    'alert_nav_form_campain' => 'Créer une [[ alert_entity ]] de campagne',

    'alert_message_saved'         => '[[ alert_entity | ucfirst ]] enregistrée',
    'alert_message_deleted'       => '[[ alert_entity | ucfirst ]] supprimée',
    'alert_message_enabled'       => '[[ alert_entity | ucfirst ]] activée',
    'alert_message_disabled'      => '[[ alert_entity | ucfirst ]] désactivée',
    'alert_question_delete'       => 'Voulez-vous réellement supprimer l\'[[ alert_entity ]] %1 ?',
    'alert_toManagers_enabled'    => '[[project_field_managers | ucfirst]] ajouté(s) dans la liste des Destinataires',
    'alert_toManagers_disabled'   => '[[project_field_managers | ucfirst]] retiré(s) dans la liste des Destinataires',
    'alert_toValidators_enabled'  => '[[project_field_validators | ucfirst]] ajouté(s) dans la liste des Destinataires',
    'alert_toValidators_disabled' => '[[project_field_validators | ucfirst]] retiré(s) dans la liste des Destinataires',
    'alert_toMembers_enabled'     => '[[project_field_members | ucfirst]] ajoutée dans la liste des Destinataires',
    'alert_toMembers_disabled'    => '[[project_field_members | ucfirst]] retirée dans la liste des Destinataires',

    'alert_field_query_error_non_exists' => 'Cette [[ query_entity ]] n\'existe pas ou plus',

    'alert_sent'               => '[[ alert_entity | ucfirst ]] envoyée',
    'alert_variables'          => 'Variables disponibles',
    'alert_variable_projects'  => 'Liste des [[ project_entities ]]',
    'alert_variable_recipient' => 'Prénom et nom du destinataire',
    'alert_variable_measures' => 'Liste des mesures',
    'alert_variable_indicators' => 'Liste des indicateurs',
    'alert_variable_campainName' => 'Nom de la campagne'
];
