<?php

namespace Budget\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class NatureController extends BasicRestController
{
    protected $entityClass = 'Budget\Entity\Nature';
    protected $repository = 'Budget\Entity\Nature';
    protected $entityChain = 'budget:e:nature';

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder
                    ->expr()
                    ->orX(...['object.name LIKE :f_full', 'object.code LIKE :f_full'])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
