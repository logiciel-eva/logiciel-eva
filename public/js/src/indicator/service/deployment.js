(function(app) {

    app.factory('deploymentService', [
        'restFactoryService',
        function deploymentService(restFactoryService) {
            return restFactoryService.create('/api/deployment');
        }
    ])

})(window.EVA.app);
