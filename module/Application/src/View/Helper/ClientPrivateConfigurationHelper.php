<?php

namespace Application\View\Helper;

use Application\Entity\Configuration;
use Laminas\View\Helper\AbstractHelper;

class ClientPrivateConfigurationHelper extends AbstractHelper
{
    /**
     * @var Configuration
     */
    protected $configuration;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function __invoke()
    {
        return $this->configuration;
    }
}
