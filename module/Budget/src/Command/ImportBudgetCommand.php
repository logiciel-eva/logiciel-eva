<?php

namespace Budget\Command;

use Bootstrap\Entity\Client;
use Bootstrap\Environment\Client\ClientEnvironment;
use Bootstrap\Environment\Environment;
use Budget\Controller\ImportController;
use Budget\Entity\ImportConfiguration;
use Budget\Helper\ImportBudgetHelper;
use Budget\Service\ImportBudgetService;
use Core\Command\AbstractCommand;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\Http\TreeRouteStack;
use Laminas\Router\RouteMatch;
use Laminas\View\Model\JsonModel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportBudgetCommand extends AbstractCommand
{
    private Environment $environment;
    private ImportController $importController;

    private function getEnvironment($client, $serviceManager)
    {
        if(PHP_SAPI !== 'cli') {
            return $serviceManager->get('Environment');
        }

        if ($serviceManager->has('Environment')) {
            return $serviceManager->get('Environment');
        }

        $request = new Request();
        $router = new TreeRouteStack();

        $config               = $serviceManager->get('Config');
        $entityManagerFactory = $serviceManager->get('EntityManagerFactory');

        $moduleManager        = $serviceManager->get('ModuleManager');
        $translator           = $serviceManager->get('Translator');

        $environment = new ClientEnvironment($config, $request, $router, $entityManagerFactory, $moduleManager, $translator);

        $serviceManager->setService('Environment', $environment);

        return $environment;
    }

    /**
     * Call : php vendor/bin/laminas budget:imports
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $this->importController = $controllerManager->get('Budget\Controller\ImportController');

        $serviceManager = $this->serviceLocator->get('ServiceManager');

        $translator = $this->serviceLocator->get('Translator');

        $em = $this->getEmDefault();
        $clients = $em->getRepository('Bootstrap\Entity\Client')->findAll();

        $output->writeln($translator->translate('budget_automated_import_cron_start'));

        /** @var Client $client */
        foreach ($clients as $client) {
            /** @var Environment $environment */
            $this->environment = $this->getEnvironment($client, $serviceManager);

            $entityManagerFactory = $this->getEmFactory();
            $output->writeln('[ ' . $client->getName() . ' ] '.$translator->translate('budget_automated_import_cron_client_process'));

            $clientEm = $entityManagerFactory->getEntityManager($client);

            $this->environment->getSession()->start('budget_import_' . $client->getId());
            $this->environment->getSession()->store('client', $client->getId());

            /** @var array<ImportConfiguration> $importConfigurations */
            $importConfigurations = $clientEm->getRepository('Budget\Entity\ImportConfiguration')->findAll();
            foreach($importConfigurations as $importConfiguration) {
               if($importConfiguration->shouldRun() === false){
                    $output->writeln(
                        sprintf(
                            $translator->translate('budget_automated_import_cron_skipping_info'),
                            $importConfiguration->getId(),
                            $translator->translate('import_configuration_frequency_'.strtolower($importConfiguration->getFrequencyLabel())),
                            $importConfiguration->getLastImportAt()->format('Y-m-d H:i:s')
                        )
                    );
                    continue;
                }

                $importBudgetHelper = new ImportBudgetHelper(
                    $importConfiguration->getPath()
                );

                $output->writeln($translator->translate('budget_automated_import_cron_start').' #'.$importConfiguration->getId().' : '.$importBudgetHelper->getCsvFolderRootPath().'...');

                if($importBudgetHelper->hasFiles() === false){
                    $output->writeln($translator->translate('budget_automated_import_cron_no_files_to_import'));

                    $importConfiguration->setLastImportAt(new \DateTime());
                    $clientEm->persist($importConfiguration);
                    continue;
                }

                $this->setupImportRequest($importConfiguration);
                $output->writeln('request setup done');

                foreach($importBudgetHelper->getFiles() as $importFile){
                    $output->writeln('preparing request for file '. $importFile);
                    $importService = new ImportBudgetService(
                        $importFile,
                        $importBudgetHelper->getCsvFolderDonePath(),
                        $translator,
                    );

                    $content = [
                        'options' => [
                            'type' => $importConfiguration->getBudgetType(),
                            'mode' => $importConfiguration->getImportType(),
                            'year' => $importConfiguration->getYear(),
                            'inPosteAccountParent' => $importConfiguration->getInPosteAccountParent(),
                        ],
                        'properties' => $importConfiguration->getQuery()->getFilters(),
                        'lines' => [],
                    ];

                    $content['lines'] = iterator_to_array(
                        $importService->getCsvData(
                            $importConfiguration->getDelimiter()
                        )
                    );

                    $request = new Request();
                    $request->setMethod(Request::METHOD_POST);
                    $request->setContent(
                        json_encode($content)
                    );

                    $output->writeln('done preparing request for file '. $importFile);

                    $response = null;
                    $clientPrivateConfig = $clientEm->getRepository('Application\Entity\Configuration')->find(1);

                    try {
                        $output->writeln('import request for file '. $importFile);
                        $response = $this->importController->dispatch($request);
                        $output->writeln('done import request for file '. $importFile);
                    }catch (\Throwable $t){
                        $importService->logger->error($t->getMessage());
                        $importService->logger->error(sprintf($translator->translate('budget_automated_import_cron_file_error'), $importFile));
                        $importService->sendEmail($client, $clientPrivateConfig);  
                        $importService->moveFileToDonePath();
                    }

                    if(!$response instanceof JsonModel){
                        $output->writeln(sprintf($translator->translate('budget_automated_import_cron_file_error'), $importFile));
                        continue;
                    }

                    $importService->writeLogs($response);
                    $importService->sendEmail($client, $clientPrivateConfig);
                    $importService->moveFileToDonePath();
                }

                $importConfiguration->setLastImportAt(new \DateTime());
                $clientEm->persist($importConfiguration);
            }

           $clientEm->flush();
           $output->writeln('[ ' . $client->getName() . ' ] '.$translator->translate('budget_automated_import_cron_client_process_done'));
        }

       

        $output->writeln($translator->translate('budget_automated_import_cron_stop'));

        return Command::SUCCESS;
    }

    private function setupImportRequest(
        ImportConfiguration $importConfiguration,
    )
    {
        $this->environment->getSession()->store('user', $importConfiguration->getCreatedBy());

        $routeMatch = new RouteMatch([
            'controller' => $this->importController,
            'action' => 'save',
            'entity' => $importConfiguration->getBudgetEntity()
        ]);
        $e = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $this->importController->setEvent($e);
    }
}