<?php

namespace Age\Model;

use JsonSerializable;

class AgeIdentifier implements JsonSerializable
{

    private $Id;
    private $Code;
    private $ReferenceExterne;


    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->Id = $id;

        return $this;
    }

    /**
     * Get the value of code
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */
    public function setCode($code)
    {
        $this->Code = $code;

        return $this;
    }

    /**
     * Get the value of referenceExterne
     */
    public function getReferenceExterne()
    {
        return $this->ReferenceExterne;
    }

    /**
     * Set the value of referenceExterne
     *
     * @return  self
     */
    public function setReferenceExterne($referenceExterne)
    {
        $this->ReferenceExterne = $referenceExterne;

        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
