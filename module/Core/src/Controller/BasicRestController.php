<?php

namespace Core\Controller;

use Bootstrap\Entity\Client;
use Core\Utils\ErrorMessageCustomUtils;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Laminas\Hydrator\DoctrineObject as DoctrineHydrator;
use Field\Entity\Value;
use Keyword\Entity\Association;
use Keyword\Entity\Keyword;
use Laminas\Filter\StripTags;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Model\JsonModel;

/**
 * Class BasicRestController
 *
 * The abstract class BasicRestController is an automated controller that manages a REST resource.
 * It's one of the pillar of the application because it's linked with 99% of the client AJAX request (lists, filters,
 * autocomplete, forms etc...)
 * The majority of the application entities can be managed here but they should respect some contracts. They must
 * inherits the BasicRestEntityAbstract class or at least implements the BasicRestEntityInterface. They must also be
 * managed in the ACL module configuration.
 *
 * @package Core\Controller
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 */
abstract class BasicRestController extends AbstractRestfulSLController
{
	/**
	 * @var string The full entity class name
	 */
	protected $entityClass;

	/**
	 * @var string The rights chain for this entity (should be moduleName:e:entityName)
	 */
	protected $entityChain;

	/**
	 * @var string The module name.
	 */
	protected $entityModule = '';

	/**
	 * @var string The entity name.
	 */
	protected $entityName = '';

	/**
	 * @var string The repository name (should be the same as the $entityClass in 90% of times)
	 */
	protected $repository;

	/**
	 * @var DoctrineHydrator A Doctrine Hydrator used to hydrate array data to record object
	 */
	protected $hydrator;

	/**
	 * @var array The columns/properties that must be fetched by the extract process
	 */
	protected $columns;

	/**
	 * @var EntityManager
	 */
	protected $entityManager;

	/**
	 * @var Client A way to get current client (should be owner of entity manager)
	 */
	protected $client;

	/**
	 * @var bool
	 */
	protected $isMasterRequest = false;

	protected $fromConsole = false;

	protected $isGeneric = false;
    /**
     * @var bool set it to true if a duplicate search is needed on the list
     */
    protected $withDuplicate = false;

	public function __construct()
	{
		if ($this->entityChain) {
			$array = explode(':', $this->entityChain);

			if (is_array($array) && count($array) === 3) {
				$this->entityModule = $array[0];
				$this->entityName = $array[2];
			}
		}
	}

	/**
	 * Assign the $columns property form the "col" query param.
	 * The "col" query params must be a simple array like : col[]=col1&col[]=col2&col[]=col3.
	 * It will be used to indicates which properties of the record(s) should be extracted in each response.
	 *
	 * @param MvcEvent $event
	 *
	 * @return mixed
	 */
	public function onDispatch(MvcEvent $event)
	{
		ini_set('memory_limit', -1);
		ini_set('max_execution_time', -1);

		$this->columns = $this->params()->fromQuery('col', []);

		if (!$this->fromConsole) {
			if ($this->environment()->getName() == 'client' && $this->client == null) {
				$client = $this->environment()->getClient();
				$this->client = $client;
				if ($this->params()->fromQuery('master', false) == 'true') {
					$client = $this->environment()->getClient();
					if ($client->hasNetwork() && $client->isMaster()) {
						$this->isMasterRequest = true;
					}
				}
			}
		}

		return parent::onDispatch($event);
	}

	/**
	 * Update and/or create a list of records.
	 *
	 * Method : PUT (without identifier)
	 * If the "id" key is missing in the array data, data will be passed to the create action, otherwise,
	 * to the update action.
	 * The response will contains an array of formatted json responses (one for each record) depending of the called
	 * action.
	 *
	 * @param array $data
	 *
	 * @return JsonModel
	 */
	public function replaceList($data)
	{
		$responses = [];
		foreach ($data as $objectAsArray) {
			if (!isset($objectAsArray['id']) || $objectAsArray['id'] === null) {
				$jsonModel = $this->create($objectAsArray);
			} else {
				$jsonModel = $this->update($objectAsArray['id'], $objectAsArray);
			}

			$responses[] = $jsonModel->getVariables();
		}

		return new JsonModel($responses);
	}

	public function deleteList($data)
	{
		$responses = [];
		foreach ($data as $objectAsArray) {
			if (isset($objectAsArray['id']) && $objectAsArray['id'] !== null) {
				$jsonModel = $this->delete($objectAsArray['id']);
				$responses[] = $jsonModel->getVariables();
			}
		}

		return new JsonModel($responses);
	}

	/**
	 * Create a record.
	 *
	 * Method : POST (without identifier)
	 * If data are validated in the entity input filter and the access control is confirmed then the record is hydrated,
	 * persisted and saved.
	 *
	 * This method should returns the formatted json response :
	 * [
	 *  'object'  => [],         // The record as array (data array)
	 *  'success' => true/false, // Does the record creation succeeded ?
	 *  'errors'  => [
	 *      'fields'     => [],  // Errors by field (messages from the input filter)
	 *      'exceptions' => []   // Exceptions thrown by the flush
	 *  ]
	 * ]
	 *
	 * @param array $data
	 *
	 * @return JsonModel
	 */
	public function create($data)
	{
        /* @TODO Error Zend : Losrque un validator attend un boolean et que false est envoyé,
         * zend considére la valeur obligatoire et la vois vide, fonctionne très bien lorsque
         * je transforme le false a 0. Comportement à vérifier.
         */
        foreach ($data as &$d) {
            if($d === false) $d=0;
            if($d === true) $d=1;
        }

		$object = new $this->entityClass();
		$objectAsArray = [];
		$success = false;
		$errors = [
			'fields' => [],
			'exceptions' => [],
		];

		try {
			$inputFilter = $object->getInputFilter(
				$this->getEntityManager(),
				$this->authStorage()->getUser()
			);
			$data = $this->removePropertiesAcl($data);
			$inputFilter->setData($data);

			if ($inputFilter->isValid()) {				
				$this->hydrate($inputFilter->getValues(), $object);
				if (
					$this->acl()->ownerControl($this->entityChain . ($this->isGeneric ? ':u' : ':c'), $object)
				) {
					$this->getEntityManager()->persist($object);
					$this->getEntityManager()->flush();

					$this->saveKeywords($object, $data);
					$this->saveTerritories($object, $data);

					$objectAsArray = $this->extract($object);
					$success = true;
				}
			}

			$errors['fields'] = $inputFilter->getMessages();
		} catch (\Exception $e) {
			$errors['exceptions'][] = $e->getMessage();
		} finally {
			return new JsonModel([
				'errors' => $errors,
				'success' => $success,
				'object' => $objectAsArray,
			]);
		}
	}

	/**
	 * Hydrate a record from a data array.
	 * This method can be overridden to use another logic than the Doctrine Hydrator.
	 *
	 * @param array $data
	 * @param mixed $object
	 */
	protected function hydrate($data, $object)
	{
		$this->getHydrator()->hydrate($data, $object);
	}

	protected function hydratorExtract($object)
	{
		return $this->getHydrator()->extract($object);
	}

	/**
	 * Doctrine Hydrator as a singleton.
	 *
	 * @return DoctrineHydrator
	 */
	protected function getHydrator()
	{
		if ($this->hydrator === null) {
			$this->hydrator = new DoctrineHydrator($this->getEntityManager());
		}

		return $this->hydrator;
	}

	/**
	 * Saving keywords linked to the record.
	 *
	 * Externalized in a specific function because they are no direct relation between the record and the keywords,
	 * so we can"t use the create/update function as they are.
	 *
	 * @param mixed $object
	 * @param array $data
	 * @param? string $entityClass
	 */
	public function saveKeywords($object, $data, $entityClass = null)
	{
		$moduleManager = $this->moduleManager();
		$repositoryKeyword = $this->getEntityManager()->getRepository('Keyword\Entity\Keyword');
		$repositoryGroup = $this->getEntityManager()->getRepository('Keyword\Entity\Group');
        if (isset($data['master']) && array_key_exists('keywords', $data)){
            $tmpKeywords = [];
			foreach ($data['keywords'] as $key => $group) {
				$tmpGroup = $repositoryGroup->findOneBy(['id'=>$key]);
				if ($tmpGroup) {
					$groupId = $tmpGroup->getId();
					$tmpKeywords[$groupId] = [];
                    if(!is_array($group) || isset($group['id'])) {
                        $group = [$group];
                    }

					foreach ($group as $keyword) {
						$tmpKeyword = $repositoryKeyword->findOneBy(['id'=>$keyword['id']]);
						$keyword['id'] = $tmpKeyword->getId();
						$tmpKeywords[$groupId][] = $keyword;
					}
				}
			}
			$data['keywords'] = $tmpKeywords;
		}
		$_entityClass = $entityClass ?: $this->entityClass;
		if (isset($data['keywords']) && $moduleManager->hasKeywords($_entityClass)) {
			$removeQuery = $this->getEntityManager()->createQuery(
				'DELETE FROM Keyword\Entity\Association a WHERE a.entity = :entity AND a.primary = :primary'
			);
			$removeQuery->setParameters([
				'entity' => $_entityClass,
				'primary' => $object->getId(),
			]);
			$removeQuery->execute();

			foreach ($data['keywords'] as $group => $keywords) {
				$groupEntity = $repositoryGroup->find($group);

				if (!$groupEntity->isMultiple()) {
					if (isset($keywords['id'])) {
						$keywords = [$keywords];
					} elseif (sizeOf($keywords) > 1) {
						$keywords = [$keywords[0]];
					}
				}
				if ($keywords) {
					foreach ($keywords as $keyword) {
						$association = new Association();
						$association->setEntity($_entityClass);
						$association->setPrimary($object->getId());
						$association->setKeyword($repositoryKeyword->find($keyword['id']));

						$this->getEntityManager()->persist($association);
					}
				}
			}

			$this->getEntityManager()->flush();
		}
	}

	/**
	 * Saving territories linked to the record.
	 *
	 * Externalized in a specific function because they are no direct relation between the record and the territories,
	 * so we can"t use the create/update function as they are.
	 *
	 * @param mixed $object
	 * @param array $data
	 */
	public function saveTerritories($object, $data, $entityClass = null)
	{
		$moduleManager = $this->moduleManager();

		if (isset($data['territories'])) {
			$_entityClass = $entityClass ?: $this->entityClass;
			$repositoryTerritory = $this->getEntityManager()->getRepository('Map\Entity\Territory');

			$removeQuery = $this->getEntityManager()->createQuery(
				'DELETE FROM Map\Entity\Association a WHERE a.entity = :entity AND a.primary = :primary'
			);
			$removeQuery->setParameters([
				'entity' => $_entityClass,
				'primary' => $object->getId(),
			]);
			$removeQuery->execute();

			foreach ($data['territories'] as $territory) {
				$association = new \Map\Entity\Association();
				$association->setEntity($_entityClass);
				$association->setPrimary($object->getId());
				$association->setTerritory($repositoryTerritory->find($territory['id']));

				$this->getEntityManager()->persist($association);
			}

			$this->getEntityManager()->flush();
		}
	}

	/**
	 * Saving custom fields linked to the record.
	 *
	 * Externalized in a specific function because they are no direct relation between the record and the custom fields,
	 * so we can"t use the create/update function as they are.
	 *
	 * @param mixed $object
	 * @param array $data
	 */
	public function saveCustomFields($object, $data)
	{
		if (isset($data['customFields'])) {
			$repositoryField = $this->getEntityManager()->getRepository('Field\Entity\Field');
			foreach ($data['customFields'] as $id => $field) {
				$removeQuery = $this->getEntityManager()->createQuery(
					'DELETE FROM Field\Entity\Value a WHERE a.entity = :entity AND a.primary = :primary AND a.field = :field'
				);
				$removeQuery->setParameters([
					'entity' => $this->entityClass,
					'primary' => $object->getId(),
					'field' => $repositoryField->find($id),
				]);

				if ($field === null) {
					$field = '';
				}

				$association = new Value();
				$dataArray = [
					'entity' => $this->entityClass,
					'primary' => $object->getId(),
					'field' => $id,
					'value' => $field,
				];

				$inputFilter = $association->getInputFilter(
					$this->getEntityManager(),
					$this->authStorage()->getUser()
				);
				$inputFilter->setData($dataArray);
				if ($inputFilter->isValid()) {
					$removeQuery->execute();
					$this->hydrate($inputFilter->getValues(), $association);
					$this->getEntityManager()->persist($association);
				}
			}

			$this->getEntityManager()->flush();
		}
	}

	
	/**
	 * Extract a record to a data array.
	 *
	 * Properties extracted comes from the "col" query param.
	 * Cascade properties are possibles with the dot separator.
	 * Many2one and One2many relations are also managed.
	 * In addition to the cols, the "_rights" array will contains each access control rule for the current record.
	 * The only exception is the "keywords" column because its processing is a little bit different and common with
	 * all "keyworded" entities.
	 *
	 * @param mixed $object
	 *
	 * @return array
	 */
	protected function extract($object)
	{
		//$array = $this->getHydrator()->extract($object);
		$array = [];

		if ($this->columns) {
			foreach ($this->columns as $column) {
				if ($column === 'territories') {
					$array[$column] = $this->getTerritories($object);
				} elseif ($column === 'keywords') {
					$array[$column] = $this->getKeywords($object);
				} elseif ($column === 'customFields') {
					$array[$column] = $this->getCustomFields($object);
				} else {
					$this->extractInternal($object, $column, $array);
				}
			}
		}

		if (!$this->fromConsole) {
			$array['_rights'] = [
				'read' => $this->acl()->ownerControl($this->entityChain . ':r', $object),
				'update' => $this->acl()->ownerControl($this->entityChain . ':u', $object),
				'delete' => $this->acl()->ownerControl($this->entityChain . ':d', $object),
			];
		}

		if ($this->isMasterRequest) {
			$array['_client'] = [
				'id' => $this->client->getId(),
				'name' => $this->client->getName(),
			];
		}

		return $array;
	}

	/**
	 * Get the array of keywords associated with a record grouped by Group.
	 *
	 * @param $objectOrId
	 *
	 * @return array
	 */
	public function getKeywords($objectOrId, $entityClass = null)
	{
		$keywords = [];
		$id = is_object($objectOrId) ? $objectOrId->getId() : $objectOrId;
        $master =  $this->isMasterRequest || $this->getEvent()->getParam('master', false);

		$associations = $this->getEntityManager()
			->getRepository('Keyword\Entity\Association')
			->findBy([
				'entity' => $entityClass ? $entityClass : $this->entityClass,
				'primary' => $id,
			]);

		foreach ($associations as $association) {
			$keyword = $association->getKeyword();
			$group = $keyword->getGroup();
            $groupId = $master ? $group->getMaster() : $group->getId();
			if (!array_key_exists($groupId, $keywords)) {
				$keywords[$groupId] = [];
			}

			$keywords[$groupId][] = [
				'id' => $keyword->getId(),
				'name' => $keyword->getName(),
			];
		}

		return $keywords;
	}

	/**
	 * Get the array of custom fields associated with a record
	 *
	 * @param mixed $object
	 *
	 * @return array
	 */
	public function getCustomFields($object, $entityClass = null)
	{
		$stripTags = new StripTags();
		$customFields = [];

		$values = $this->getEntityManager()
			->getRepository('Field\Entity\Value')
			->findBy([
				'entity' => $entityClass ? $entityClass : $this->entityClass,
				'primary' => $object->getId(),
			]);

		foreach ($values as $value) {
			$field = $value->getField();
			$val = $value->getValue();
			if (is_array($val)) {
				$val = implode(', ', $val);
			}

			if ($field->getType() === 'textarea') {
				$val = $stripTags->filter(html_entity_decode($val));
			}

			$customFields[$field->getId()] = $val;
		}

		return $customFields;
	}

	/**
	 * Get the array of territories associated with a record
	 *
	 * @param mixed $object
	 *
	 * @return array
	 */
	public function getTerritories($object, $entityClass = null)
	{
		$territories = [];

		$associations = $this->getEntityManager()
			->getRepository('Map\Entity\Association')
			->findBy([
				'entity' => $entityClass ? $entityClass : $this->entityClass,
				'primary' => $object->getId(),
			]);

		foreach ($associations as $association) {
			$territory = $association->getTerritory();
			if ($territory) {
				$territories[] = [
					'id' => $territory->getId(),
					'name' => $territory->getName(),
				];
			}
		}

		return $territories;
	}

	/**
	 * The internal extractor.
	 *
	 * @param mixed $object
	 * @param string $column
	 * @param array $array
	 *
	 * @return mixed
	 */
	protected function extractInternal($object, $column, &$array)
	{
		$em = $this->getEntityManager();

		$chain = explode('.', $column);

		$getter = 'get' . ucfirst($chain[0]);
		$property = $chain[0];

		if ($object !== null) {
			if ($object instanceof Collection) {
				$i = 0;
				$idGroup = null;
				foreach ($object as $item) {
					// For keywords hierarchySup
					if ($item instanceof Keyword && ($chain[0] == 'hierarchySup')){
						if ($idGroup != $item->getGroup()->getId()){
							$idGroup = $item->getGroup()->getId();
							$i = 0;
						}
						$hierarchies = [];
						foreach($item->getHierarchySup() as $key => $hierarchy){
							$hierarchies[$key] = ['id' => $hierarchy->getId(), 'name' => $hierarchy->getName()];
						}
						$array[$idGroup][$i][$property] = $hierarchies;
					} else {
						if (!isset($array[$i])) {
							$array[$i] = [];
						}
						$this->extractInternal($item, implode('.', $chain), $array[$i]);
					}
					$i++;
				}
				return $array;
			}
	
			if (sizeOf($chain) > 1) {
				// It's not finished, recall extractInternal with the value as $object
				array_shift($chain);
				$array[$property] = $this->extractInternal(
					$object->$getter($em),
					implode('.', $chain),
					$array[$property]
				);
			} else {
				if ($property === 'territories') {
					$array[$property] = $this->getTerritories(
						$object,
						str_replace('DoctrineORMModule\\Proxy\\__CG__\\', '', get_class($object))
					);
				} elseif ($property === 'keywords') {
					$array[$property] = $this->getKeywords(
						$object,
						str_replace('DoctrineORMModule\\Proxy\\__CG__\\', '', get_class($object))
					);
				} elseif ($property === 'customFields') {
					$array[$property] = $this->getCustomFields(
						$object,
						str_replace('DoctrineORMModule\\Proxy\\__CG__\\', '', get_class($object))
					);
				} else {
					$value = $object->$getter($em);
					$array[$property] = $value instanceof DateTime ? $value->format('d/m/Y H:i') : $value;
				}
			}
		}

		return $array;
	}

	/**
	 * Update a record.
	 *
	 * Method : PUT (with identifier)
	 * If data are validated in the entity input filter and the access control is confirmed then the record is hydrated
	 * and saved.
	 *
	 * This method should returns the formatted json response :
	 * [
	 *  'object'  => [],         // The record as array (data array)
	 *  'success' => true/false, // Does the update succeeded ?
	 *  'errors'  => [
	 *      'fields'     => [],  // Errors by field (messages from the input filter)
	 *      'exceptions' => []   // Exceptions thrown by the flush
	 *  ]
	 * ]
	 *
	 * @param mixed $id
	 * @param array $data
	 *
	 * @return JsonModel
	 */
	public function update($id, $data)
	{
        /* @TODO Error Zend : Losrque un validator attend un boolean et que false est envoyé,
         * zend considére la valeur obligatoire et la vois vide, fonctionne très bien lorsque
         * je transforme le false a 0. Comportement à vérifier.
         */
        foreach ($data as &$d) {
            if($d === false) $d=0;
            if($d === true) $d=1;
        }
        
		$objectAsArray = [];
		$success = false;
		$errors = [
			'fields' => [],
			'exceptions' => [],
		];
		try {
			$object = $this->getEntityManager()
				->getRepository($this->repository)
				->find($id);
			if (
				$object &&
				($this->acl()->ownerControl($this->entityChain . ':u', $object) ||
					$this->params()->fromQuery('force', false))
			) {
				$inputFilter = $object->getInputFilter(
					$this->getEntityManager(),
					$this->authStorage()->getUser()
				);
				$data = $this->removePropertiesAcl($data);
				$data = array_merge($this->hydratorExtract($object), $data);
				$inputFilter->setData($data);

				if ($inputFilter->isValid()) {
					$this->hydrate($inputFilter->getValues(), $object);
					$this->getEntityManager()->flush();

					$this->saveKeywords($object, $data);
					$this->saveTerritories($object, $data);
					$this->saveCustomFields($object, $data);

					$objectAsArray = $this->extract($object);

					$success = true;
				}

				$errors['fields'] = $inputFilter->getMessages();
			}
		} catch (\Exception $e) {
			$errors['exceptions'][] = $e->getMessage();
		} finally {
			return new JsonModel([
				'errors' => $errors,
				'success' => $success,
				'object' => $objectAsArray,
			]);
		}
	}

	/**
	 * Delete a record.
	 *
	 * Method : DELETE (with identifier)
	 * If the access control is confirmed then the record is removed.
	 * We're using the Doctrine remove() function.
	 *
	 * This method should returns the formatted json response :
	 * [
	 *  'object'  => [],         // The object as array (entity data array)
	 *  'success' => true/false, // Does the delete succeeded ?
	 *  'errors'  => [
	 *      'exceptions' => []   // Exceptions thrown by the flush
	 *  ]
	 * ]
	 *
	 * @param mixed $id
	 *
	 * @return JsonModel
	 */
	public function delete($id)
	{
		$objectAsArray = [];
		$success = false;
		$errors = [
			'exceptions' => [],
		];

		try {
			$object = $this->getEntityManager()
				->getRepository($this->repository)
				->find($id);

			if ($this->acl()->ownerControl($this->entityChain . ':d', $object)) {
				$this->getEntityManager()->remove($object);
				$this->getEntityManager()->flush();
				$success = true;

				$this->deleteAssociations($id);

				$objectAsArray = $this->extract($object);
			}
        } catch (ForeignKeyConstraintViolationException $e) {
            $errors['exceptions'][] = (new ErrorMessageCustomUtils())
                ->customForeignKeyConstraintViolation($e->getMessage());
        } catch (\Exception $e) {
            $errors['exceptions'][] = $e->getMessage();
		} finally {
			return new JsonModel([
				'errors' => $errors,
				'success' => $success,
				'object' => $objectAsArray,
			]);
		}
	}

	/**
	 * Read a record.
	 *
	 * Method : GET (with identifier)
	 * If the access control is confirmed then the record is retrieved.
	 *
	 * This method should returns the formatted json response :
	 * [
	 *  'object'  => [],         // The object as array (entity data array)
	 *  'success' => true/false, // Does the read succeeded ?
	 * ]
	 *
	 * @param mixed $id
	 *
	 * @return JsonModel
	 */
	public function get($id, $callbacks = [])
	{
		$success = false;
		$objectAsArray = [];

		$object = $this->getEntityManager()
			->getRepository($this->repository)
			->find($id);

		if (
			($object && $this->acl()->ownerControl($this->entityChain . ':r', $object)) ||
			$this->params()->fromQuery('force', false)
		) {
			if ($callbacks) {
				foreach ($callbacks as $callback) {
					$callback($object);
				}
			}
			$objectAsArray = $this->extract($object);

			$success = true;
		}

		return new JsonModel([
			'object' => $objectAsArray,
			'success' => $success,
		]);
	}

	/**
	 * Read a record.
	 *
	 * Method : GET (without identifier)
	 *
	 * This method should returns the formatted json response :
	 * [
	 *  'rows'    => [],         // The objects as arrays (entity data array)
	 *  'total'   => [],         // The total of rows
	 *  'success' => true/false, // Does the read succeeded ?
	 * ]
	 *
	 * @return JsonModel
	 */
	public function getList($callbacks = [], $master = false)
	{
		$success = true;
		$rows = [];

		// If its a "master" request, let's return all client rows
		if ($this->isMasterRequest && !$master) {
			foreach ($this->client->getNetwork()->getClients() as $client) {
				if (!$client->isMaster()) {
					$this->setClient($client);
					$response = $this->getList($callbacks, true);

					$rows = array_merge($rows, $response->getVariable('rows'));
					$success = $success && $response->getVariable('success');
				}
			}

			return new JsonModel([
				'rows' => $rows,
				'total' => sizeOf($rows),
				'success' => $success,
			]);
		}

		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder->select('object');
		$queryBuilder->from($this->repository, 'object');

		$search = $this->params()->fromQuery('search', []);
		$type = isset($search['type']) ? $search['type'] : null;
		$data = isset($search['data']) ? $search['data'] : [];

        if ($this->params()->fromQuery('duplicate')) {
            return $this->findDuplicateAction($data);
        }

        $data = $this->changeCurrentUserFilter($data);

		if ($type !== null) {
			$fn = 'search' . ucfirst($type);
			if (method_exists($this, $fn)) {
				$this->{$fn}($queryBuilder, $data);
			}
		}

		if (isset($data['filters']) && is_array($data['filters'])) {
			$master = $this->isMasterRequest || $this->getEvent()->getParam('master', false);
			$filtersHelper = $this->serviceLocator->get('simpleFiltersHelper');
			$filtersHelper->setEntityManager($this->getEntityManager());
			$filtersHelper->apply($queryBuilder, $this->entityClass, $master, $data['filters'], $data['mode'] ?? 'and');
		}
		if (!$this->fromConsole && ($this->acl()->onlyOwned($this->entityChain . ':r') || $this->acl()->getStatusRight($this->entityChain . ':r') !== false)) {
			$class = $this->entityClass;
			$class::ownerControlDQL(
				$queryBuilder,
				$this->authStorage()->getUser(),
				$this->acl()->getOwningRight($this->entityChain . ':r'),
				$this->acl()->getStatusRight($this->entityChain . ':r')
			);
		}

		if (isset($data['page']) && isset($data['limit'])) {
			$objects = new Paginator($queryBuilder);
			$total = count($objects);

			$limit = $data['limit'];
			$start = $limit * $data['page'];

			if ($start > $total) {
				$start = $total - $limit;
			}

			$query = $objects->getQuery();
			$query->setFirstResult($start > 0 ? $start : 0)->setMaxResults($limit);
		} else {
			$query = $queryBuilder->getQuery();
			$objects = $query->getResult();
			$total = sizeof($objects);
		}

		if ($callbacks) {
			foreach ($callbacks as $callback) {
				$callback($objects, $data, $this->getEntityManager());
			}
		}

		if ($this->params()->fromQuery('excel', false)) {
			ini_set('memory_limit', -1);
			ini_set('max_execution_time', -1);

			$exporter = $this->serviceLocator->get('ExcelExporter');
			$exporter->setName($this->params()->fromQuery('exportName', ''));
			$exporter->setEntityClass($this->entityClass);
			$exporter->setExporter(str_replace('Entity', 'Export', $this->entityClass));
			$exporter->setEntityManager($this->getEntityManager());
			$exporter->setRows($objects);
			$exporter->setCols($this->params()->fromQuery('colsExport', []));
			$exporter->setItemsRemoved($this->params()->fromQuery('itemsRemoved', []));
			$exporter->setItemsSelected($this->params()->fromQuery('itemsSelected', []));
			$exporter->render();
			die();
		}

		foreach ($objects as $object) {
			$rows[] = $this->extract($object);
		}

		return new JsonModel([
			'rows' => $rows,
			'total' => $total,
			'success' => $success,
		]);
	}

	/**
	 * @return string
	 */
	public function getEntityClass()
	{
		return $this->entityClass;
	}

	/**
	 * Add keywords filters for any property.
	 *
	 * @param string $property
	 * @param string $op
	 * @param string|int $group
	 * @param bool $shouldTree
	 * @param array $values
	 * @param QueryBuilder|null $queryBuilder
	 * @param string|null $entity
	 * @param bool $master
	 *
	 * @return array|void
	 */
	protected function keywordFilters(
		$property,
		$op,
		$group,
		$shouldTree,
		$values,
		$queryBuilder = null,
		$entity = null,
		$master = false
	) {
		$this->serviceLocator->get('simpleFiltersHelper')->keywordFilters(
			$property,
			$op,
			$group,
			$shouldTree,
			$values,
			$queryBuilder,
			$entity,
			$master
		);
	}

	
	/**
	 * @param string $entityClass
	 */
	public function setEntityClass($entityClass)
	{
		$this->entityClass = $entityClass;
	}

	/**
	 * @return string
	 */
	public function getEntityChain()
	{
		return $this->entityChain;
	}

	/**
	 * @param string $entityChain
	 */
	public function setEntityChain($entityChain)
	{
		$this->entityChain = $entityChain;
	}

	/**
	 * @return string
	 */
	public function getRepository()
	{
		return $this->repository;
	}

	/**
	 * @param string $repository
	 */
	public function setRepository($repository)
	{
		$this->repository = $repository;
	}

	/**
	 * @return array
	 */
	public function getColumns()
	{
		return $this->columns;
	}

	/**
	 * @param array $columns
	 */
	public function setColumns($columns)
	{
		$this->columns = $columns;
	}

	/**
	 * @return EntityManager
	 */
	public function getEntityManager()
	{
		if ($this->entityManager === null) {
			$this->entityManager = $this->entityManager();
		}
		return $this->entityManager;
	}

	/**
	 * @param EntityManager $entityManager
	 */
	public function setEntityManager($entityManager)
	{
		$this->entityManager = $entityManager;
		$this->hydrator = new DoctrineHydrator($this->getEntityManager());
	}

	/**
	 * @return Client
	 */
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * @param Client $client
	 */
	public function setClient($client)
	{
		$this->client = $client;
		$entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
		$this->setEntityManager($entityManagerFactory->getEntityManager($client, true));
	}

	/**
	 * @param boolean $isMasterRequest
	 */
	public function setIsMasterRequest($isMasterRequest)
	{
		$this->isMasterRequest = $isMasterRequest;
	}

	protected function deleteAssociations($id)
	{
		$em = $this->getEntityManager();

		// On supprime les keywords
		$em
			->createQuery(
				'DELETE Keyword\Entity\Association e WHERE e.entity = :entity AND e.primary = :primary'
			)
			->setParameter('entity', $this->repository)
			->setParameter('primary', $id)
			->execute();

		// On supprime les territoires
		$em
			->createQuery(
				'DELETE Map\Entity\Association e WHERE e.entity = :entity AND e.primary = :primary'
			)
			->setParameter('entity', $this->repository)
			->setParameter('primary', $id)
			->execute();

		// On supprime les field values
		$em
			->createQuery('DELETE Field\Entity\Value e WHERE e.entity = :entity AND e.primary = :primary')
			->setParameter('entity', $this->repository)
			->setParameter('primary', $id)
			->execute();

		// On supprime les notes
		$em
			->createQuery('DELETE Note\Entity\Note e WHERE e.entity = :entity AND e.primary = :primary')
			->setParameter('entity', $this->repository)
			->setParameter('primary', $id)
			->execute();

		// On supprime les attachments
		$em
			->createQuery(
				'DELETE Attachment\Entity\Attachment e WHERE e.entity = :entity AND e.primary = :primary'
			)
			->setParameter('entity', $this->repository)
			->setParameter('primary', $id)
			->execute();
	}

	/**
	 * @return boolean
	 */
	public function isFromConsole()
	{
		return $this->fromConsole;
	}

	/**
	 * @param boolean $fromConsole
	 */
	public function setFromConsole($fromConsole)
	{
		$this->fromConsole = $fromConsole;
	}

	/*protected function controlFieldCondition($object) {
        $modules = $this->moduleManager()->getActiveModules();
        foreach ($modules as $module) {
            if ($module->getKey() === $this->entityModule) {
                $aclConfiguration = $module->getAclconfiguration();
                if (!isset($aclConfiguration['entities']) || !$aclConfiguration['entities']) {
                    return true;
                }

                foreach ($aclConfiguration['entities'] as $entity) {
                    if ($entity['name'] !== $this->entityName) {
                        continue;
                    }

                    if (!isset($entity['conditions']) || !is_array($entity['conditions']) || count($entity['conditions']) === 0) {
                        return true;
                    }

                    foreach ($entity['conditions'] as $condition) {
                        $value = $object->{'get' . ucfirst($condition['property'])}();
                        var_dump($value);
                    }
                }
            }
        }

        return true;
    }*/

	protected function removePropertiesAcl($data)
	{
		$modules = $this->moduleManager()->getActiveModules();
		$moduleToKeep = null;

		foreach ($modules as $module) {
			if ($module->getKey() === $this->entityModule) {
				$moduleToKeep = $module;
			}
		}

		if (!$moduleToKeep) {
			return $data;
		}

		$aclConfiguration = $moduleToKeep->getAclconfiguration();

		if (!isset($aclConfiguration['entities']) || !$aclConfiguration['entities']) {
			return $data;
		}

		foreach ($aclConfiguration['entities'] as $entity) {
			if ($entity['name'] !== $this->entityName) {
				continue;
			}

			if (
				!isset($entity['properties']) ||
				!is_array($entity['properties']) ||
				count($entity['properties']) === 0
			) {
				return $data;
			}

			foreach ($entity['properties'] as $property) {
				if (
					!$this->acl()->isAllowed(
						$this->entityModule . ':p:' . $this->entityName . ':' . $property
					)
				) {
					if (!isset($data[$property])) {
						continue;
					}

					unset($data[$property]);
				}
			}
		}

		return $data;
	}

    protected function findDuplicateAction ($data): JsonModel
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $select = "";
        foreach ($this->params()->fromQuery('duplicate') as $col) {
            if (in_array($col, $this->columns)) {
                $select .= "s1." . $col . ", ";
            }
        }
        $queryBuilder->select('s1');
        $queryBuilder->from($this->repository, 's1');
        $queryBuilder->join($this->repository, 's2');
        $queryBuilder->where('s1.id <> s2.id');
        foreach ($this->params()->fromQuery('duplicate') as $col) {
            if (in_array($col, $this->columns)) {
                $queryBuilder->andWhere($this->getNonAccentedWord('s1.' . $col) . " = " . $this->getNonAccentedWord('s2.' . $col) . ' OR (s1.' . $col . ' IS NULL AND s2.' . $col . ' IS NULL)');
            }
        }
        foreach ($this->params()->fromQuery('duplicate') as $col) {
            if (in_array($col, $this->columns)) {
                $queryBuilder->addOrderBy('s1.' . $col);
            }
        }
        if (isset($data['page']) && isset($data['limit'])) {
            $objects = new Paginator($queryBuilder);
            $total = count($objects);

            $limit = $data['limit'];
            $start = $limit * $data['page'];

            if ($start > $total) {
                $start = $total - $limit;
            }

            $query = $objects->getQuery();
            $query->setFirstResult($start > 0 ? $start : 0)->setMaxResults($limit);
        } else {
            $query = $queryBuilder->getQuery();
            $objects = $query->getResult();
            $total = sizeof($objects);
        }

        foreach ($objects as $object) {
            $rows[] = $this->extract($object);
        }

        return new JsonModel([
            'rows' => isset($rows) > 0 ? $rows : [],
            'total' => $total,
            'success' => true,
        ]);
    }

    private function getNonAccentedWord ($word) {
        return "REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(
            REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(
                REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(
                    LOWER(". $word ."),
                    'ù','u'),'ú','u'),'û','u'),'ü','u'),'ý','y'),'ë','e'),'à','a'),'á','a'),'â','a'),'ã','a'),
                'ä','a'),'å','a'),'æ','a'),'ç','c'),'è','e'),'é','e'),'ê','e'),'ë','e'),'ì','i'),'í','i'),
            'î','i'),'ï','i'),'ð','o'),'ñ','n'),'ò','o'),'ó','o'),'ô','o'),'õ','o'),'ö','o'),'ø','o')";
    }


    protected function changeCurrentUserFilter($data)
	{
		if (!isset($data['filters']) || !is_array($data['filters'])) {
			return $data;
		}

		foreach ($data['filters'] as &$filter) {
			if (!isset($filter['val'])) {
				continue;
			}

			if ($filter['val'] === 'current-user') {
				$filter['val'] = $this->authStorage()
					->getUser()
					->getId();
				continue;
			}

			if (is_array($filter['val']) && in_array('current-user', $filter['val'])) {
				foreach ($filter['val'] as &$value) {
					if ($value === 'current-user') {
						$value = $this->authStorage()
							->getUser()
							->getId();
					}
				}
			}
		}

		return $data;
	}
}
