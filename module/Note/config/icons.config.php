<?php

return [
    'icons' => [
        'note' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-sticky-note-o',
        ]
    ]
];
