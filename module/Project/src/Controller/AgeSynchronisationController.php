<?php

namespace Project\Controller;

use Age\Service\AgeSynchronisationServiceInterface;
use Core\Controller\BasicRestController;
use Project\Entity\AgeSynchronisation;
use Project\Entity\Project;
use Laminas\View\Model\JsonModel;

class AgeSynchronisationController extends BasicRestController
{

	public function readAction()
	{
		$entityManager = $this->getEntityManager();
		$id = $this->params()->fromRoute('id', false);
		/** @var Project */
		$project = $entityManager->getRepository('Project\Entity\Project')->find($id);

		/** @var AgeSynchronisationServiceInterface */
		$ageSynchroService = $this->serviceLocator->get('AgeSynchronisationService');
		$ageSynchroService->initForProject($project);

		return new JsonModel(['synchro' => $project->getAgeSynchronisation()]);
	}

	public function synchroniseAction()
	{
		$entityManager = $this->getEntityManager();
		$id = $this->params()->fromRoute('id', false);
		$data = json_decode($this->getRequest()->getContent());
		/** @var Project */
		$project = $entityManager->getRepository('Project\Entity\Project')->find($id);

		$validationErrors = [];

		$ageSynchronisation = $project->getAgeSynchronisation();
		if (isset($ageSynchronisation)) {
			$ageSynchronisation->setBudgetSeizable($data->budgetSeizable);
			$ageSynchronisation->setBudgetPresented($data->budgetPresented);
			$ageSynchronisation->setExecutionSeizable($data->executionSeizable);
			/** @var AgeSynchronisationServiceInterface */
			$ageSynchroService = $this->serviceLocator->get('AgeSynchronisationService');
			$validationErrors = $ageSynchroService->createOperation($project);
		}
		return new JsonModel([
			'errors' => $validationErrors,
			'synchro' => [
				'id' => $ageSynchronisation->getId(),
				'budgetSeizable' => $ageSynchronisation->getBudgetSeizable(),
				'budgetPresented' => $ageSynchronisation->getBudgetPresented(),
				'executionSeizable' => $ageSynchronisation->getExecutionSeizable(),
				'status' => $ageSynchronisation->getStatus()
			]
		]);
	}


	public function preventAction()
	{
		$entityManager = $this->getEntityManager();
		$id = $this->params()->fromRoute('id', false);
		/** @var Project */
		$project = $entityManager->getRepository('Project\Entity\Project')->find($id);

		$ageSynchronisation = $project->getAgeSynchronisation();
		if (isset($ageSynchronisation)) {
			$ageSynchronisation->setStatus(AgeSynchronisation::AGE_SYNCHRO_STATUS_PREVENT);
			$entityManager->persist($ageSynchronisation);
			$entityManager->flush();
		}
		return new JsonModel([
			'id' => $ageSynchronisation->getId(),
			'budgetSeizable' => $ageSynchronisation->getBudgetSeizable(),
			'budgetPresented' => $ageSynchronisation->getBudgetPresented(),
			'executionSeizable' => $ageSynchronisation->getExecutionSeizable(),
			'status' => $ageSynchronisation->getStatus()
		]);
	}
}
