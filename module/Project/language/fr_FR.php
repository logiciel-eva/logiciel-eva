<?php

return [
    'menu_suivi_title' => 'Suivi de projet',

    'project_module_title' => '[[ project_entities | ucfirst ]]',

    'project_entity'   => 'fiche',
    'project_entities' => 'fiches',

    'project_nav_list'          => 'Liste des {{__tb.total }} [[ project_entities ]]',
    'project_nav_list_wo_count' => 'Liste des [[ project_entities ]]',
    'project_nav_form'          => 'Créer une [[ project_entity ]]',
    'project_nav_import'        => 'Importer des [[ project_entities ]]',

    'project_new_project' => 'Nouvelle [[ project_entity ]]',

    'project_multipleEdit_title'       => 'Modification multiple',
    'project_exportWord_module_title'  => 'Export Word des [[ project_entities ]]',
    'project_exportExcel_module_title' => 'Export Excel des [[ project_entities ]]',
    'project_field_id'                              => 'Identifiant',
    'project_field_id_short'                        => 'ID',
    'project_field_code'                            => 'Code',
    'project_field_code_error_unique'               => 'Ce code est déjà utilisé',
    'project_field_code_error_used_by_a_budgetCode' => 'Ce code est déjà utilisé par un [[ budgetCode_entity | ucfirst ]]',
    'project_field_code_length'                     => 'Le code est trop long (maximum 20 caractères)',
    'project_field_name'                            => 'Titre',
    'project_field_managers'                        => 'Chef(s) de projet',
    'project_field_projects'                        => 'Fiche',
    'project_field_validators'                      => 'Validateur(s)',    
    'project_field_team_members'                    => 'Membres de l\'équipe',
    'project_field_parent'                          => 'Rattachement',
    'project_field_parent_error_non_exists'         => 'Cette [[ project_entity ]] n\'existe pas ou plus',
    'project_field_parent_error_hierarchy'          => 'Cette [[ project_entity ]] ne peut pas être utilisée comme parent de cette [[ project_entity ]]',
    'project_field_createdAt'                       => '[[ field_created_at_m ]]',
    'project_field_updatedAt'                       => '[[ field_updated_at_m ]]',
    'project_field_result'                          => 'Bilan',
    'project_field_prevResult'                      => 'Bilan des années antérieures',
    'project_field_description'                     => 'Descriptif',
    'project_field_objectifs'                       => 'Objectifs',
    'project_field_context'                         => 'Contexte et motif',
    'project_field_members'                         => 'Équipe',
    'project_field_actors'                          => '[[ actor_entities | ucfirst ]]',
    'project_field_publicationStatus'               => 'Statut',
    'project_field_publicationStatus_error_right'   => 'Vous n\'avez pas le droit de mettre ce [[ project_field_publicationStatus | lcfirst ]]',
    'project_field_external'                        => 'Maîtrise d\'ouvrage externe',
    'project_field_ownership'                       => 'Maître d\'ouvrage',
    'project_field_ownership_error_non_exists'      => 'Ce [[ project_field_ownership ]] n\'existe pas ou plus',
    'project_field_members_error_non_exists'        => 'Un des membres ou roles n\'existe pas',
    'project_field_members_error_duplicate'         => 'Un des membres est présent plusieurs fois',
    'project_field_actors_error_non_exists'         => 'Une des structures ou roles n\'existe pas',
    'project_field_networkAccessible'               => 'Accessible au réseau',
    'project_field_caducite_start'                  => '[[ poste_income_field_caduciteStart ]]',
    'project_field_caducite_end'                    => '[[ poste_income_field_caduciteEnd ]]',
    'project_field_advancement'                     => '[[ advancement_entity | ucfirst ]]',
    'project_field_programmingDate'                 => 'Date de programmation',
    'project_field_plannedDateStart'                => 'Date de démarrage prévue',
    'project_field_plannedDateEnd'                  => 'Date de fin prévue',
    'project_field_realDateStart'                   => 'Date de démarrage effective',
    'project_field_realDateEnd'                     => 'Date de fin effective',
    'project_field_level'                           => 'Niveau',
    'project_field_budgetStatus'                    => '[[ budgetStatus_entity | ucfirst ]]',
    'project_field_budgetStatus_error_non_exists'   => 'Ce [[ budgetStatus_entity ]] n\'existe pas ou plus',
    'project_field_budgetStatus_error_type'         => 'Un identifiant est attendu',
    'project_field_advancement_date_error'          => 'La date doit être sous la forme yyyy-mm-dd',
    'project_field_advancement_percent_error'       => 'L\'avancement doit être sous la forme x%',
    'project_field_advancement_date_percent_error'  => 'La date doit être sous la forme aaaa-mm-jj,nombre%',
    'project_field_globalTimeTarget'                => 'Temps prévu',
    'project_field_template'                        => 'Modèle de [[ project_entity ]]',
    'project_field_rateExpenseRealized'             => 'Taux de [[ expense_entities ]] réalisées',
    'project_field_rateIncomeRealized'              => 'Taux de [[ income_entities ]] réalisées',
    'project_field_budgetCodes'                     => 'Codes financier',
    'project_field_services'                        => '[[ service_entities | ucfirst ]]',
    'project_field_ageSynchronisation'              => 'Synchronisation AGE',
    'project_field_plannedDateStart_year'           => 'Année de la [[ project_field_plannedDateStart ]]',
    'project_field_template.id'                     => 'ID Modèle de [[ project_entity ]]',
    'project_field_convention' => 'Convention',
    'project_field_members_role_error_non_exists' => 'Un des roles n\'existe pas',
    'project_field_members_error_syntax' => 'Erreur de syntaxe',
    'project_field_budgetStatus_error_type ' => 'Un identifiant est attendu',
    'project_field_template_error_non_exists' => 'Ce modèle de fiche n\'existe pas',
    'project_field_polluted' => 'Site pollué',
    'project_field_polluted_territories' => 'Territoires site pollué',
    'project_field_polluted_sites' => 'Site(s) pollué(s)',
    'project_field_id_expected' => 'ID attendue',
    'project_incorrect_date' => 'Date incorrect : DD/MM/YYYY HH:mm ou DD/MM/YYYY',
    'project_field_check_territories' => 'ID ou Titre attendu séparé par --',

    'project_field_user_no_exist'                   => 'Cette utilisateur n\'existe pas',

    'project_formule_rateExpenseRealized' => '= ([[ expense_entity ]] [[ expense_type_paid | lcfirst ]] / [[ poste_expense_entity ]] [[ poste_expense_field_amount | lcfirst ]])',
    'project_formule_rateIncomeRealized' => '= ([[ income_entity ]] [[ income_type_perceived | lcfirst ]] / [[ poste_income_entity ]] [[ poste_income_field_amount | lcfirst ]])',

    'project_help_entity'    => 'tooltip',
    'project_export_default' => 'Export par défaut',
    'project_export_budget'  => 'Export budget détaillé',
    'project_export_budget_keywords_family_choice' => 'Choisir un [[ group_entity ]] de [[ keyword_entities ]]',
    'project_export_budget_expense_type_choice' => 'Choisir un type de [[ expense_entity ]]',
    'project_export_budget_income_type_choice' => 'Choisir un type de [[ income_entity ]]',
    'project_export_budget_financial_year_choice' => 'Choisir une année d\'exercice (optionel)',
    'project_export_budget_nature_choice' => 'Choisir un(e) [[ nature_entity ]] (optionel)',
    'project_export_budget_no_keywords_family_error' => 'Il faut choisir un [[ group_entity ]] de [[ keyword_entities ]] pour réaliser cet export',
    'project_export_budget_no_expense_type_error' => 'Il faut choisir un type de [[ expense_entity ]] pour réaliser cet export',
    'project_export_budget_no_income_type_error' => 'Il faut choisir un type de [[ income_entity ]] pour réaliser cet export',

    'project_help_code'              => '',
    'project_help_name'              => '',
    'project_help_managers'          => '',
    'project_help_validators'        => '',
    'project_help_parent'            => '',
    'project_help_result'            => '',
    'project_help_prevResult'        => '',
    'project_help_description'       => '',
    'project_help_objectifs'         => '',
    'project_help_context'           => '',
    'project_help_members'           => '',
    'project_help_publicationStatus' => '',
    'project_help_external'          => '',
    'project_help_ownership'         => '',
    'project_help_networkAccessible' => '',
    'project_help_programmingDate'   => '',
    'project_help_plannedDateStart'  => '',
    'project_help_plannedDateEnd'    => '',
    'project_help_realDateStart'     => '',
    'project_help_realDateEnd'       => '',
    'project_help_globalTimeTarget'  => '',
    'project_help_budgetStatus'      => '',

    'project_placeholder_entity' => 'placeholder',

    'project_member_field_user' => '[[ member ]]',
    'project_member_field_role' => 'Rôle',
    'project_add_members'       => 'Ajouter des [[ members | lower ]]',

    'project_message_saved'              => '[[ project_entity | ucfirst ]] enregistrée',
    'project_message_deleted'            => '[[ project_entity | ucfirst ]] supprimée',
    'project_message_archived'           => '[[ project_entity | ucfirst ]] archivée',
    'project_message_unarchived'         => '[[ project_entity | ucfirst ]] désarchivée',
    'project_question_delete'            => 'Voulez-vous réellement supprimer cette [[ project_entity ]] %1 ?',
    'project_message_saved_at_least_one' => 'Au moins 1 [[ project_entity ]] a été enregistrée.<br>Les [[ project_entity | ucfirst ]] contenant au moins 1 erreur n\'ont pas été enregistrées',
    'project_message_saved_all_errors'   => 'Aucun(e) [[ project_entity ]] n\'a été enregistrée.<br>Veuillez regarder les erreurs.',

    'manager'   => 'Chef de projet',
    'validator' => 'Validateur',
    'member'    => 'Membre',
    'members'   => 'Membres',

    'member_entity' => 'Membre',

    'project_publicationStatus_draft'              => 'Brouillon',
    'project_publicationStatus_pending'            => 'En suspens',
    'project_publicationStatus_waiting_validation' => 'A valider',
    'project_publicationStatus_validated'          => 'Validée',
    'project_publicationStatus_published'          => 'Votée',
    'project_publicationStatus_rejected'           => 'Refusée',
    'project_publicationStatus_archived'           => 'Archivée',

    'project_initial_tree' => 'Arborescence initiale',

    'project_tree_parent' => 'Parent',
    'project_tree_child'  => 'Enfant',
    'project_hierarchy'   => 'Plan d\'action',

    'actor_entity'   => 'acteur',
    'actor_entities' => 'acteurs',

    'actor_field_structure' => '[[ structure_entity | ucfirst ]]',

    'actor_field_structure.name' => 'Structure',
    'actor_field_structure.sigle' =>  'Sigle',
    'project_field_territories' => 'Territoires',

    'actor_nav_form' => 'Ajouter un [[ actor_entity ]]',

    'actor_question_delete' => 'Voulez-vous réellement supprimer l\' [[ actor_entity ]] %1 ?',
    'actor_message_deleted' => '[[ actor_entity | ucfirst ]] supprimé',
    'actor_message_saved'   => '[[ actor_entity | ucfirst ]] enregistré',

    'actor_field_project_error_non_exists'    => 'Cette [[ project_entity ]] n\'existe pas ou plus',

    'tooltip_switch_view_project_tree' => 'Vue arborescente',
    'tooltip_switch_view_project_list' => 'Vue liste',

    'levels_entity' => 'Niveaux',
    'level_entity'  => 'Niveau',

    'project_arbo_filter_select_arbo'            => 'Sélectionner une arborescence',
    'project_arbo_filter_select_level_range'     => 'Sélectionner les [[ levels_entity | lower ]] concernés',
    'project_arbo_filter_select_reference_level' => 'Sélectionner le [[ level_entity | lower ]] de référence',
    'project_arbo_filter_level_filters'          => 'Filtres par [[ level_entity | lower ]]',
    'project_arbo_filter_from_level'             => 'A partir du [[ level_entity | lower ]]',
    'project_arbo_filter_to_level'               => 'Jusqu\'au [[ level_entity | lower ]]',
    'project_arbo_filter_clear_keywords'         => 'Retirer les [[ keyword_entities ]] sans [[ project_entities ]]',
    'project_arbo_filter_mode_all'               => 'Tout les [[ project_entities ]] %1',
    'project_arbo_filter_mode_parent'            => 'Uniquement les parents des [[ project_entities ]] %1 retenues',
    'project_arbo_filter_mode_leaves'            => 'Uniquement les parents des [[ project_entities ]] %1 retenues et inclure les feuilles %2',
    'project_arbo_filter_sort_order'             => 'Critère de tri',

    'project_nav_list_with_filters'          => 'Liste des {{__tb.total }} [[ project_entities ]] avec {{__tb.apiParams.search.data.nbFilters}} filtre{{__tb.apiParams.search.data.nbFilters > 1 ? "s" : ""}}',

    'template_module_title' => '[[ template_entities | ucfirst ]]',

    'template_entity'   => 'modèle de [[ project_entity ]]',
    'template_entities' => 'modèles de [[ project_entities ]]',

    'template_nav_list' => 'Liste des {{ __tb.total }} [[ template_entities ]]',
    'template_nav_form' => 'Créer un [[ template_entity ]]',

    'template_message_saved'   => '[[ template_entity | ucfirst ]] enregistré',
    'template_message_deleted' => '[[ template_entity | ucfirst ]] supprimé',
    'template_question_delete' => 'Voulez-vous réellement supprimer le [[ template_entity ]] %1 ?',

    'template_field_id'        => 'Id',
    'template_field_name'      => 'Nom',
    'template_field_default'   => 'Par défaut',
    'template_field_createdAt' => '[[ field_created_at_m ]]',
    'template_field_updatedAt' => '[[ field_updated_at_m ]]',
    'template_field_services'  => '[[ service_entities | ucfirst ]]',

    'template_message_default'    => 'Le template est par défaut',
    'template_message_notDefault' => 'Le template n\'est plus par défaut',

    'change_project_template' => 'Modifier le [[ template_entity ]]',

    'project_message_notification_managers'   => 'Voulez-vous qu\'un message soit envoyé aux [[ project_field_managers ]] après enregistrement ?',
    'project_message_notification_validators' => 'Voulez-vous qu\'un message soit envoyé aux [[ project_field_validators ]] après enregistrement ?',

    'project_gantt_view_configuration' => 'Vue Gantt',

    'project_updates_tooltip' => 'Modifications',
    'project_updates_title' => 'Dernières modifications',
    'project_updates_noentry' => 'Aucune modification enregistrée'
];
