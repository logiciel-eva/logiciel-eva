<?php

use Budget\Command\ImportBudgetCommand;
use Budget\Command\ImportBudgetCommandFactory;

/**
 * Inject configuations on global scope.
 */
return [
    'service_manager' => [
        'factories' => [
            ImportBudgetCommand::class => ImportBudgetCommandFactory::class,
        ],
    ],
    'laminas-cli'     => [
        'commands' => [
            'budget:imports' => ImportBudgetCommand::class,
        ],
    ],
];
