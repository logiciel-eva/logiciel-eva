<?php

namespace Bootstrap\Environment\Plugin;

use Bootstrap\Environment\Environment;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class EnvironmentPlugin
 *
 * Access environment from controller.
 *
 * @package Bootstrap\Environment\Plugin
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class EnvironmentPlugin extends AbstractPlugin
{
    /**
     * @var Environment
     */
    protected $environment;

    /**
     * @param Environment $environment
     */
    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @return Environment
     */
    public function __invoke()
    {
        return $this->environment;
    }
}
