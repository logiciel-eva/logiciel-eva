<?php

return [
    'module'       => [
        'time' => [
            'environments' => [
                'client',
            ],

            'active'   => true,
            'required' => false,

            'acl' => [
                'entities' => [
                    [
                        'name'     => 'timesheet',
                        'class'    => 'Time\Entity\Timesheet',
                        'keywords' => true,
                        'owner'    => ['own', 'service']
                    ],
                    [
                        'name'     => 'bookmark',
                        'class'    => 'Time\Entity\Bookmark',
                        'keywords' => false,
                        'owner'    => ['own']
                    ],
                    [
                        'name'     => 'synchronisation',
                        'class'    => 'Time\Entity\Synchronisation',
                        'keywords' => false,
                        'owner'    => ['own']
                    ],
                    [
                        'name'     => 'export',
                        'class'    => 'Time\Entity\Export',
                        'keywords' => false,
                        'owner'    => []
                    ],
                    [
                        'name'     => 'absence-type',
                        'class'    => 'Time\Entity\AbsenceType',
                        'keywords' => false,
                        'owner'    => []
                    ],
                    [
                        'name'     => 'settings-time',
                        'class'    => 'Time\Entity\SettingsTime',
                        'keywords' => false,
                        'owner'    => []
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator'   => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
