(function(app) {

    app.controller('indicatorListController', [
        'indicatorService', '$scope', 'flashMessenger', 'translator',
        function indicatorListController(indicatorService, $scope, flashMessenger, translator) {
            var self = this;
            self.view = 'list';
            self.indicators = [];
            self.options = {
                name: 'indicators-list',
                col: [
                    'id',
                    'name',
                    'uom',
                    'description',
                    'definition',
                    'groupIndicators.id',
                    'groupIndicators.name',
                    'method',
                    'interpretation',
                    'type',
                    'operator',
                    'values',
                    'keywords',
                    'archived',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    indicatorService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(indicator) {
                if (angular.isArray(indicator.keywords) || indicator.keywords == null) {
                    indicator.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = indicator;
                self.editedLineFields = angular.copy(indicator);
            };

            self.saveEditedLine = function saveEditedLine(indicator) {
                self.editedLineErrors = {};
                console.log(indicator, self.editedLineFields);
                indicatorService.save(angular.extend({}, indicator, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        indicator = angular.extend(indicator, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('indicator_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.archiveIndicator = function archiveIndicator(indicator) {
                self.editedLineErrors = {};
                indicator.archived = indicator.archived ? 0 : 1;
                indicatorService.save(indicator, {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        indicator = angular.extend(indicator, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;
                        if (indicator.archived) {
                            flashMessenger.success(translator('indicator_message_archived'));
                        } else {
                            flashMessenger.success(translator('indicator_message_unarchived'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteIndicator = function deleteIndicator(indicator) {
                if (confirm(translator('indicator_question_delete').replace('%1', indicator.name))) {
                    indicatorService.remove(indicator).then(function (res) {
                        if (res.success) {
                            self.indicators.splice(self.indicators.indexOf(indicator), 1);
                            flashMessenger.success(translator('indicator_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
