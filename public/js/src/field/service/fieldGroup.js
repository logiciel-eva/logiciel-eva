(function(app) {

    app.factory('fieldGroupService', [
        'restFactoryService',
        function fieldGroupService(restFactoryService) {
            return restFactoryService.create('/api/fieldGroup');
        }
    ])

})(window.EVA.app);
