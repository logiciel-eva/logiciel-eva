(function (app) {

    app.controller('recapWithMeasuresController', [
        '$http', 'queryService', '$rootScope', '$scope', '$timeout',
        function recapWithMeasuresController($http, queryService, $rootScope, $scope, $timeout) {
            var self = this;

            self.view = 'table';
            self.nbFilters = 1;

            self.loading  = false;
            self.filters  = {
                project  : {},
                indicator: {},
                measure  : {},
                clients: []
            };
            self.measures = {};

            self.applyFilters = function applyFilters() {
                self.loading = true;
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.indicator).length + Object.keys(self.filters.measure).length + self.filters.clients.length;

                $http({
                    url   : '/analysis/indicator/recap-with-measures?' + $.param({filters: self.filters}),
                    method: 'GET',
                }).then(function (res) {
                    self.measures = res.data.measures;
                    self.loading  = false;

                    self.loadBarChart();
                });
            };

            self.resetFilters = function () {
                self.filters.project   = {};
                self.filters.indicator = {};
                self.filters.measure   = {};
                self.applyFilters();
            };

            self.export = function () {
                window.open('/analysis/indicator/recap-with-measures?' + $.param({filters: self.filters, export: true}));
            };

            self.chart = {
                bar: []
            };

            $scope.$watch(function () {
                return self.view
            }, function () {
                $timeout(function () {
                    self.chart.bar.map(bar => bar.api.refresh());
                });
            });

            self.loadBarChart = function () {
                self.chart.bar = [];

                var charts = {};
                var values = {};
                for (var i in self.measures) {
                    var measure = self.measures[i];
                    var indicator = measure.indicator;

                    if (typeof charts[indicator.id] === 'undefined') {
                        charts[indicator.id] = {
                            name: indicator.name,
                            options: {
                                chart: {
                                    type: 'multiBarChart',
                                    height: 400,
                                    showDistX: true,
                                    showDistY: true,
                                    color : function(d){
                                        return d.color
                                    }
                                }
                            },
                            data: [{key: 'Nombre', values: []}],
                            api: null
                        };

                        values[indicator.id] = {};
                    }

                    if (typeof values[indicator.id][measure.fixedValue.text] === 'undefined') {
                        values[indicator.id][measure.fixedValue.text] = {
                            value: 0,
                            color: measure.fixedValue.color || '#525252'
                        };
                    }

                    values[indicator.id][measure.fixedValue.text].value++;
                }

                for (var i in charts) {
                    for (var key in values[i]) {
                        charts[i].data[0].values.push({
                            x: key,
                            y: values[i][key].value,
                            color: values[i][key].color
                        })
                    }
                }
                self.chart.bar = Object.values(charts);
            };

            // QUERIES
            self.queries          = [];
            self.query            = {
                list: 'analysis-indicator-recap-with-measures'
            };
            self.errors           = {
                fields: {}
            };
            self.loadingQueries   = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries        = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };
        }
    ]);
})(window.EVA.app);
