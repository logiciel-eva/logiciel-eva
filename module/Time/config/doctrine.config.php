<?php

namespace Time;

return [
    'doctrine' => [
        'driver' => [
            'time_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Time\Entity' => 'time_entities'
                ]
            ]
        ],
    ]
];
