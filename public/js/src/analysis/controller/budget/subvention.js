(app => {
	app.controller('subventionController', [
		'queryService',
		'$rootScope',
		'$scope',
		'translator',
		'$http',
		'flashMessenger',
		app.expenseControllerFactory('budget-analysis-subvention-list', 'exportModalSubvention', true),
	]);
})(window.EVA.app);
