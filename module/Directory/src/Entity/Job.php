<?php

namespace Directory\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Job manages the job of a contact in a structure
 *
 * @category Directory
 * @package EVA
 * @since Version 4
 * @version 4
 * @author Curtis Pelissier <curtis.pelissier@lignusdev.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="directory_job")
 */
class Job extends \Core\Entity\Contact\AbstractContact
{
    /**
     * Unique ID
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Jobs Contact
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Directory\Entity\ContactJobStructure", mappedBy="jobs")
     */
    protected $links;


    /**
     * PostParc ID
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $postParcId;

    /**
     * Constructor of the class
     *
     * @since Version 4
     * @version 4
     */
    public function __construct()
    {
        $this->links = new ArrayCollection();
    }

    /**
     * Gets the Unique ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Unique ID.
     *
     * @param int $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Contact
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param Contact $links
     */
    public function setLinks($links)
    {
        $this->links = $links;
    }

    /**
     * Add Jobs(s) to Job's ArrayCollection
     *
     * @param ArrayCollection $links The jobs arraycollection
     * @return self
     * @since Version 4
     * @version 4
     */
    public function addLinks(ArrayCollection $links)
    {
        foreach ($links as $link) {
            $link->setJob($this);
            $this->links->add($link);
        }
        return $this;
    }

    /**
     * Remove Jobs(s) to Job's ArrayCollection
     *
     * @param ArrayCollection $links The jobs arraycollection
     * @return self
     * @since Version 4
     * @version 4
     */
    public function removeLinks(ArrayCollection $links)
    {
        foreach ($links as $link) {
            $link->setJob(null);
            $this->links->removeElement($link);
        }
        return $this;
    }

    public function getPostParcId()
    {
        return $this->postParcId;
    }


    public function setPostParcId($id)
    {
        $this->postParcId = $id;

        return $this;
    }


    /**
     * @param EntityManager $entityManager
     * @return InputFilter
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        return parent::getInputFilter($entityManager);
    }
}
