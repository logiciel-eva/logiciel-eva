<?php

namespace Project\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;

/**
 * Class Member
 *
 * @package Project\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="project_member")
 */
class Member
{
    const ROLE_MANAGER   = 'manager';
    const ROLE_VALIDATOR = 'validator';
    const ROLE_MEMBER    = 'member';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="members")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $project;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * Clone function
     */
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project = null)
    {
        $this->project = $project;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }
}
