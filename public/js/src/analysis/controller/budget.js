(function (app) {

    app.controller('budgetController', [
        '$scope',
        function budgetController($scope) {
            var self = this;

            self.init = function init(isMaster) {
                $scope.isMaster = isMaster;
            };
        }
    ]);

    app.controller('filterController', [
        '$scope',
        function ($scope) {
            var self = this;

            self.filtersKey = null;

            self.init = function (filtersKey) {
                self.filtersKey = filtersKey;
            };

            var events = [
                'budget-analysis-subvention-list-query-loaded',
                'budget-analysis-gbcp-list-query-loaded',
                'analysis-budget-expense-query-loaded',
                'analysis-budget-income-query-loaded',
                'analysis-budget-financer-query-loaded',
                'analysis-budget-year-query-loaded'
            ];

            events.forEach(function (event) {
                $scope.$on(event, function (e, filters) {
                    showFilters(filters[self.filtersKey]);
                });
            });

            function showFilters(filters) {
                $scope.__filters = {};

                for (var i in filters) {
                    if (
                        filters[i] !== null &&
                        (
                            typeof filters[i].val !== 'undefined' ||
                            filters[i].op === 'isNull'
                        )
                    ) {
                        $scope.__filters[i] = true;
                    } else {
                        $scope.__filters[i] = false;
                    }
                }
            }
        }
    ]);

})(window.EVA.app);
