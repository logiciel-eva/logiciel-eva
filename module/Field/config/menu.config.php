<?php

$fields = [
    'icon' => 'field',
    'title' => '[[field_entities | ucfirst]]',
    'right' => 'field:e:fieldGroup:r',
    'href' => 'field',
    'priority' => 3,
];

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'fields' => $fields,
                ],
            ],
        ],
        'client-menu-parc' => [
            'administration' => [
                'children' => [
                    'fields' => array_replace($fields, ['priority' => 99]),
                ],
            ],
        ],
    ],
];
