<?php

namespace Memo\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Memo
 *
 * @package Memo\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="user_memo")
 */
class Memo extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return $user->getId() === $this->createdBy->getId();
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $user
     * @param string $owner
     */
    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        $queryBuilder->andWhere('object.createdBy = :ownerControl');
        $queryBuilder->setParameter('ownerControl', $user);
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'content',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ]
        ]);

        return $inputFilter;
    }
}
