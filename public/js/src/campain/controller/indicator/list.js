(function(app) {

    app.controller('indicatorController', [
        'campain-indicatorService', '$scope', 'flashMessenger', 'translator', '$rootScope',
        function indicatorController(indicatorService, $scope, flashMessenger, translator, $rootScope) {
            var self = this;

            self.from   = {
                id: null
            };
            self.object = null;

            const $modal = angular.element('#indicator-modal');
            self.indicatorType = 'indicator';
            self.indicatorGroupType = 'indicator-group';
            self.indicators = [];
            self.indicator = {
                targetDate: $scope.$parent.self.campain.startDate,
                measures: []
            };
            self.loading = {
                save: false
            };

            self.options = {
                name: 'campain-indicators-list',
                col: [
                    'id',
                    'campain.id',
                    'primary',
                    'entity',
                    'targetDate',
                    'frequency',
                    'frequencyValue',
                    'users.id',
                    'users.name',
                    'users.avatar'
                ],
                searchType: 'list',
                sort: 'primary',
                order: 'asc',
                pager: false,
                getItems: function getItems(params, callback, abort) {
                    
                    if (self.from.id) {
                        params.search.data.filters['campain.id'] = {
                            op: 'eq',
                            val: self.from.id
                        };

                        indicatorService.findAll(params, abort.promise).then(function (res) {
                            callback(res);
                        });
                    } else {
                        callback({rows: []});
                    }
                }
            };

            self.init = function init(id) {
                self.from.id = id;
            };

            self.editIndicator = function editIndicator(indicator) {
                self.editedIndicator = indicator;
                self.indicator = angular.copy(indicator);
                self.panelTitle = self.indicator.name;

                self.errors = {
                    fields: {}
                };
                $modal.modal('show');
            };

            self.createIndicator = function createIndicator(indicatortype) {
                if(self.from.id===null){
                    self.from.id = $scope.$parent.self.campain.id;
                }
                self.indicator = {
                    id: null,
                    targetDate: $scope.$parent.self.campain.startDate,
                    type : indicatortype,
                    campain:{
                        id: self.from.id
                    },
                    _rights: {
                        update: true
                    }
                };

                self.errors = {
                    fields: {}
                };
                
                $modal.modal('show');
            };
            self.frequencyLabel = function frequencyLabel(indicator){
                return translator('campain_recurrence_' + indicator.frequency);
            };
            self.deleteIndicator = function deleteIndicator(indicator) {
                if (confirm(translator('campain_indicator_question_delete').replace('%1', indicator.entityDetails.name))) {
                    indicatorService.remove(indicator).then(function (res) {
                        if (res.success) {
                            self.indicators.splice(self.indicators.indexOf(indicator), 1);
                            flashMessenger.success(translator('indicator_message_deleted'));
                            $rootScope.$broadcast('update-measure-campain');
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.saveIndicator = function saveIndicator() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.indicator.id;

                indicatorService.save(self.indicator, {col: self.options.col}).then(function(res) {
                    if (isCreation) {
                        self.indicators.push(res.object);
                    } else {
                        for(var i in self.indicators){
                            if(self.indicators[i].id == res.object.id){
                                self.indicators[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('indicator_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                    $rootScope.$broadcast('update-measure-campain');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };
            self.isIndicatorType = function isIndicatorType(intype){
                return intype === self.indicatorType;
            };
            self.isIndicatorGroupType = function isIndicatorGroupType(intype){
                return intype === self.indicatorGroupType;
            };
            self.openIndicatorModal = function openIndicatorModal(){
                $modal.modal('show');
            };
        }
    ])

})(window.EVA.app);

