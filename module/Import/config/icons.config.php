<?php

return [
    'icons' => [
        'import' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-download',
        ],
    ],
];
