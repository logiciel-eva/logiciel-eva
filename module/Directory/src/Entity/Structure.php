<?php

namespace Directory\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\Filter\Boolean;

/**
 * Class Structure manages structure of the directory
 *
 * @category Directory
 * @package EVA
 * @since Version 4
 * @version 4
 * @author Curtis Pelissier <curtis.pelissier@lignusdev.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="directory_structure")
 */
class Structure extends AbstractContact
{
    /**
     * Unique ID
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Sigle
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sigle;

    /**
     * Téléphone 2
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone2;

    /**
     * Structure Jobs
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Directory\Entity\ContactJobStructure", mappedBy="structure", orphanRemoval=true, cascade={"all"})
     */
    protected $links;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $financer = false;

    /**
     * SIRET
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $siret;

    /**
     * Site web
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $website;

    /**
     * PostParc ID
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $postParcId;

    /**
     * Constructor of the class
     *
     * @since Version 4
     * @version 4
     */
    public function __construct()
    {
        $this->links = new ArrayCollection();
    }

    /**
     * Gets the Unique ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Unique ID.
     *
     * @param int $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getSigle()
    {
        return $this->sigle;
    }

    /**
     * @param string $sigle
     */
    public function setSigle($sigle)
    {
        $this->sigle = $sigle;
    }

    /**
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * @param string $phone2
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;
    }

    /**
     * @return ArrayCollection
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param ArrayCollection $links
     */
    public function setLinks($links)
    {
        $this->links = $links;
    }

    /**
     * Add Jobs(s) to Job's ArrayCollection
     *
     * @param ArrayCollection $links The jobs arraycollection
     * @return self
     * @since Version 4
     * @version 4
     */
    public function addLinks(ArrayCollection $links)
    {
        foreach ($links as $link) {
            $link->setJob($this);
            $this->links->add($link);
        }
        return $this;
    }

    /**
     * Remove Jobs(s) to Job's ArrayCollection
     *
     * @param ArrayCollection $links The jobs arraycollection
     * @return self
     * @since Version 4
     * @version 4
     */
    public function removeLinks(ArrayCollection $links)
    {
        foreach ($links as $link) {
            $link->setJob(null);
            $this->links->removeElement($link);
        }
        return $this;
    }

    /**
     * @return boolean
     */
    public function isFinancer()
    {
        return $this->getFinancer();
    }

    /**
     * @return boolean
     */
    public function getFinancer()
    {
        return $this->financer;
    }

    /**
     * @param boolean $financer
     */
    public function setFinancer($financer)
    {
        $this->financer = $financer ?? false;
    }

    public function getPostParcId()
    {
        return $this->postParcId;
    }

    public function setPostParcId($id)
    {
        $this->postParcId = $id;

        return $this;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name' => 'phone2',
            'required' => false,
        ], 'phone2');

        $inputFilter->add([
            'name' => 'sigle',
            'required' => false,
        ], 'sigle');

        $inputFilter->add([
            'name' => 'email',
            'required' => false,
            'validators' => [
                ['name' => 'EmailAddress'],
            ],
        ], 'email');

        $inputFilter->add([
            'name' => 'financer',
            'required' => false,
            'allow_empty' => true,
            'filters' => [
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function ($value) {

                            if ((is_string($value) && (strtolower($value) == 'oui' || $value === '1' )) ||  $value === 1) {
                                return true;
                            }
                            if ((is_string($value) && (strtolower($value) == 'non' || $value === '0')) || $value === 0 ) {
                                return false;
                            }

                            return $value;
                        },
                    ],
                ],
            ],
            'validators' => [
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function ($value) {
                            return  is_bool($value);
                        },
                        'message' => 'structure_field_type_boolean'
                    ],
                ],
            ],
        ], 'financer');

        $inputFilter->add([
            'name' => 'siret',
            'required' => false,
        ], 'siret');

        $inputFilter->add([
            'name' => 'website',
            'required' => false,
        ], 'website');

        return $inputFilter;
    }

    /**
     * Get sIRET
     *
     * @return  string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set sIRET
     *
     * @param  string  $siret  SIRET
     *
     * @return  self
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get site web
     *
     * @return  string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set site web
     *
     * @param  string  $website  Site web
     *
     * @return  self
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }
}
