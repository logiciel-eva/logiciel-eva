(function(app) {

    app.factory('toGeoJson', [
        '$q',
        function toGeoJson($q) {
            var self = this;
            self.fromString = function (string, type) {
                var geojson = null;
                switch(type) {
                    case 'kml': {
                        geojson = toGeoJSON.kml(angular.element.parseXML(string));
                    } break;
                    case 'gpx': {
                        geojson = toGeoJSON.gpx(angular.element.parseXML(string));
                    } break;
                    case 'geojson':
                    case 'json' : {
                        geojson = JSON.parse(string);
                    } break;
                }

                return geojson;
            };

            self.fromFile = function (file) {
                var defer = $q.defer();

                var reader = new FileReader();

                var type = self.getFileExtension(file.name);

                reader.readAsText(file);
                reader.onloadend = function(e) {
                    var geojson = self.fromString(e.target.result, type);
                    defer.resolve(geojson);
                };

                return defer.promise;
            };

            self.getFileExtension = function getFileExtension(filename) {
                return filename.split('.').pop();
            };

            return {
                fromFile: self.fromFile
            }
        }
    ]);

})(window.EVA.app);
