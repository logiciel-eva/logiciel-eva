<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class ImportConfigurationController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:importConfiguration:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }
}
