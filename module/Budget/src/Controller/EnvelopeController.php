<?php

namespace Budget\Controller;

use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class EnvelopeController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:envelope:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->serviceLocator->get('MyModuleManager')->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('envelope', false);
        }

        return new ViewModel([
            'groups' => $groups,
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);
        if (!$this->acl()->basicFormAccess('budget:e:envelope', $id)) {
            return $this->notFoundAction();
        }

        $envelope = $this->entityManager()->getRepository('Budget\Entity\Envelope')->find($id);

        // get all poste incomes to retrieve projects linked to the envelope
        $postesIncome = isset($envelope) ? $this->entityManager()->getRepository('Budget\Entity\PosteIncome')->getAllByEnvelope($envelope) : [];
        $postesExpense = isset($envelope) ? $this->getPostesExpense($postesIncome, $envelope->getAmount()) : [];

        if (
            $envelope &&
            !$this->acl()->ownerControl('budget:e:envelope:u', $envelope) &&
            !$this->acl()->ownerControl('budget:e:envelope:r', $envelope)
        ) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'envelope'      => $envelope,
            'postesExpense' => $postesExpense,
            'totalsExpense' => $this->computeTotals($postesExpense, Expense::getTypes(), PosteExpense::getBalances()),
            'postesIncome'  => $postesIncome,
            'totalsIncome'  => $this->computeTotals($postesIncome, Income::getTypes(), PosteIncome::getBalances())
        ]);
    }


    private function getPostesExpense($postesIncome, $envelopeAmount)
    {
        $projects = [];
        foreach ($postesIncome as $posteIncome) {
            $projectId = $posteIncome->getProject()->getId();
            if (!array_key_exists($projectId, $projects)) {
                $projects[$projectId] = array('project' => $posteIncome->getProject(), 'amount' => 0.0);
            }
            $projects[$projectId]['amount'] += $posteIncome->getAmount();
        }

        // init types and balances array
        $newArray = [];
        foreach (Expense::getTypes() as $type) {
            $newArray[$type] = 0.0;
        }
        foreach (PosteExpense::getBalances() as $balance) {
            $newArray[$balance] = 0.0;
        }


        $postesExpense = [];
        foreach ($projects as $project) {
            $ratio = $project['amount'] < $envelopeAmount && 0.0 !== $envelopeAmount ? ((100.0 * $project['amount'] / $envelopeAmount) / 100) : 1.0;
            $posteExpense = array_merge(
                array(
                    'id' => $project['project']->getId(), 
                    'name' => $project['project']->getName(), 
                    'amount' => 0.0
                ),
                $newArray
            );
            foreach ($project['project']->getPosteExpenses() as $projectPosteExpense) {
                $posteExpense['amount'] += $projectPosteExpense->getAmount() * $ratio;
                foreach (Expense::getTypes() as $type) {
                    $method = "getAmount" . ucfirst($type);
                    $posteExpense[$type] += $projectPosteExpense->$method() * $ratio;
                }

                foreach (PosteExpense::getBalances() as $balance) {
                    $method = "getAmount" . ucfirst($balance);
                    $posteExpense[$balance] = $projectPosteExpense->$method() * $ratio;
                }
            }
            $postesExpense[] = $posteExpense;
        }
        return $postesExpense;
    }

    private function computeTotals($postes, $types, $balances)
    {
        $totals = [];

        // init
        $totals['amount'] = 0.0;
        foreach ($types as $type) {
            $totals[$type] = 0.0;
        }
        foreach ($balances as $balance) {
            $totals[$balance] = 0.0;
        }

        // compute
        foreach ($postes as $poste) {
            $totals['amount'] += is_object($poste) ? $poste->getAmount() : $poste['amount'];
            foreach ($types as $type) {
                $method = "getAmount" . ucfirst($type);
                $totals[$type] += is_object($poste) ? $poste->$method() : $poste[$type];
            }
            foreach ($balances as $balance) {
                $method = "getAmount" . ucfirst($balance);
                $totals[$balance] += is_object($poste) ? $poste->$method() : $poste[$balance];
            }
        }
        return $totals;
    }
}
