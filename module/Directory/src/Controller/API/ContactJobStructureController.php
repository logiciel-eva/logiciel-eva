<?php

namespace Directory\Controller\API;

use Core\Controller\BasicRestController;
use Directory\Entity\ContactJobStructure;
use Directory\Entity\Job;
use Laminas\View\Model\JsonModel;

class ContactJobStructureController extends BasicRestController
{
    protected $entityClass = 'Directory\Entity\ContactJobStructure';
    protected $repository  = 'Directory\Entity\ContactJobStructure';
    protected $entityChain = 'directory:e:job';

    public function create($data)
    {
        if (!$this->acl()->isAllowed('directory:e:job:c')) {
            return $this->notFoundAction();
        }

        return $this->alterLink($data);
    }

    private function getJob($job)
    {
        $id = 0;
        $name = null;

        if (is_array($job)) {
            $name = isset($job['name']) ? $job['name'] : null;
            $id = isset($job['id']) ? $job['id'] : 0;
        } else {
            throw new \Exception($this->translate('job_name_error_invalid'), 83001);
        }

        $em  = $this->entityManager();
        $job = $em->getRepository('Directory\Entity\Job')->findOneBy([
            'id' => $id,
        ]);

        if (!$job) {
            $job = $em->getRepository('Directory\Entity\Job')->findOneBy([
                'name' => $name,
            ]);
        }

        if (!$job) {
            $job = new Job();
            $job->setName($name);
            $em->persist($job);
        }

        return $job;
    }

    private function getStructure($structure)
    {
        return $this->entityManager()->getRepository('Directory\Entity\Structure')->find($structure['id']);
    }

    private function getContact($contact)
    {
        return $this->entityManager()->getRepository('Directory\Entity\Contact')->find($contact['id']);
    }

    private function toArrayLink($link)
    {
        return [
            'id'        => $link->getId(),
            'keywords'  => $this->getKeywords($link),
            'job'       => [
                'id'   => $link->getJob()->getId(),
                'name' => $link->getJob()->getName(),
            ],
            'contact'   => [
                'id'   => $link->getContact()->getId(),
                'name' => $link->getContact()->getName(),
            ],
            'structure' => [
                'id'   => $link->getStructure()->getId(),
                'name' => $link->getStructure()->getName(),
            ],
        ];
    }

    public function update($id, $data)
    {
        if (!$this->acl()->isAllowed('directory:e:job:u')) {
            return $this->notFoundAction();
        }

        return $this->alterLink($data, $id);
    }

    private function alterLink($data, $id = null)
    {
        $em = $this->entityManager();

        if ($id != null) {
            $link = $em->getRepository($this->entityClass)->find($id);
        } else {
            $link = new ContactJobStructure();
        }

        $errors = [];
        $success = true;
        $object = [];

        try {
            $link->setJob($this->getJob($data['job']));
            $link->setStructure($this->getStructure($data['structure']));
            $link->setContact($this->getContact($data['contact']));
            $this->saveKeywords($link, $data);
            $this->entityManager()->persist($link);
            $this->entityManager()->flush();

            $object = $this->toArrayLink($link);
        } catch (\Exception $e) {
            $success = false;
            $errors[] = $e->getMessage();
        }

        return new JsonModel([
            'errors'  => $errors,
            'success' => $success,
            'object'  => $object,
        ]);
    }

    public function delete($id)
    {
        if (!$this->acl()->isAllowed('directory:e:job:d')) {
            return $this->notFoundAction();
        }

        $em = $this->entityManager();
        $link = $em->getRepository($this->entityClass)->find($id);
        $success = false;

        if ($link) {
            $success = true;
            $em->remove($link);
            $em->flush();
        }

        return new JsonModel([
            'errors'  => [],
            'success' => $success,
            'object'  => $this->toArrayLink($link),
        ]);
    }
}
