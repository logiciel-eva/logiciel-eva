(function(app) {

    app.factory('flashMessenger', [
        '$timeout',
        function flashMessenger($timeout) {
            var self = this;

            var $messageContainer = angular.element('#flashMessage-container');

            self.addMessage = function (message, type) {
                self.showMessage({
                    message: message,
                    type: type
                })
            };

            self.showMessage = function (message) {
                var $message = angular.element('<div class="alert alert-' + message.type + '">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                    message.message +
                '</div>');

                $messageContainer.prepend($message);
                $timeout(function () {
                    $message.remove();
                }, 5000);
            };


            return {
                message : self.addMessage,
                success: function success(message) {
                    self.addMessage(message, 'success');
                },
                error: function success(message) {
                    self.addMessage(message, 'danger');
                },
                warning: function success(message) {
                    self.addMessage(message, 'warning');
                }
            };
        }
    ])

})(window.EVA.app);
