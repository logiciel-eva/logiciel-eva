<?php

namespace Field\Repository;

use Doctrine\ORM\EntityRepository;
use Field\Entity\Group;

/**
 * Class GroupRepository
 *
 * @package Field\Repository
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 */
class GroupRepository extends EntityRepository
{
    public function findByEntity($entity, $servicesUser = null)
    {
        $queryBuilder = $this->createQueryBuilder('object');
        $queryBuilder->where('object.entity = :entity');

        $queryBuilder->setParameter('entity', $entity);


        if ($servicesUser) {
            Group::ownerControlDQL($queryBuilder, $servicesUser, 'service');
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
