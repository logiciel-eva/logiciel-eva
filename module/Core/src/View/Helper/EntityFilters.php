<?php

namespace Core\View\Helper;


use Core\Module\MyModuleManager;
use Doctrine\ORM\EntityManager;
use Laminas\View\Helper\AbstractHelper;

class EntityFilters extends AbstractHelper
{
    /**
     * @var Translate
     */
    protected $translator;

    /**
     * @var MyModuleManager
     */
    protected $moduleManager;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $filters;

    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager)
    {
        $this->translator    = $translator;
        $this->moduleManager = $moduleManager;
        $this->entityManager = $entityManager;
        $this->filters       = [];
    }

    /**
     * @param $filter
     */
    public function addFilter($name, $filter)
    {
        $this->filters[$name] = $filter;
    }

    /**
     * @param $filters
     */
    public function addFilters($filters)
    {
        $this->filters += $filters;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    public function __invoke($toRemove = [])
    {
        $regexes = [];
        foreach ($toRemove as $i => $property) {
            // Check if first character === @
            if (mb_substr($property, 0, 1, 'utf-8') === '@') {
                $regexes[] = str_replace('@', '', $property);
                unset($toRemove[$i]);
            }
        }

        $filters = [];
        foreach ($this->getFilters() as $name => $filter) {
            if (in_array($name, $toRemove)) {
                continue;
            }

            foreach ($regexes as $regex) {
                if (preg_match('/^' . $regex . '-/', $name)) {
                    continue 2;
                }
            }

            $filters[$name] = $filter;
        }

        return $filters;
    }
}
