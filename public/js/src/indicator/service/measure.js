(function(app) {

    app.factory('measureService', [
        'restFactoryService',
        function measureService(restFactoryService) {
            return restFactoryService.create('/api/measure');
        }
    ])

})(window.EVA.app);
