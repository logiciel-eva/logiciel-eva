<?php

namespace User\Auth\Helper;

use User\Auth\Acl;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class AclHelper
 *
 * Access acl from view.
 *
 * @package User\Auth\Helper
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AclHelper extends AbstractHelper
{
    /**
     * @var Acl
     */
    protected $acl;

    /**
     * @param Acl $acl
     */
    public function __construct(Acl $acl)
    {
        $this->acl = $acl;
    }

    /**
     * @return Acl
     */
    public function __invoke()
    {
        return $this->acl;
    }
}
