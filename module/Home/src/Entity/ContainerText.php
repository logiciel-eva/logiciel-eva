<?php
namespace Home\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContainerText
 *
 * @package Home\Entity
 * @author  Enzo Cornand <e.cornand@siter.fr>
 *
 * @ORM\Entity
 */
class ContainerText extends Container
{
    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }


    /**
     * @return string
     */
    public function getType()
    {
        return $this::TYPE_TEXT;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name'     => 'content',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        return $inputFilter;
    }
}
