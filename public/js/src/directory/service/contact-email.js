(function (app) {

    app.factory('contactEmailService', [
        'restFactoryService',
        function (restFactoryService) {
            return restFactoryService.create('/api/directory/contact-email');
        }
    ]);
})(window.EVA.app);
