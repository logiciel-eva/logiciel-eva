<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class BudgetStatusController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:budgetStatus:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);
        if (!$this->acl()->basicFormAccess('budget:e:budgetStatus', $id)) {
            return $this->notFoundAction();
        }

        $budgetStatus = $this->entityManager()->getRepository('Budget\Entity\BudgetStatus')->find($id);

        if ($budgetStatus &&
            !$this->acl()->ownerControl('budget:e:budgetStatus:u', $budgetStatus) &&
            !$this->acl()->ownerControl('budget:e:budgetStatus:r', $budgetStatus)
        ) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'budgetStatus' => $budgetStatus,
        ]);
    }
}
