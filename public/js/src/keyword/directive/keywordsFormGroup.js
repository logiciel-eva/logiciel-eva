(function(app) {

    app.directive('keywordsFormGroup', [
        'groupService',
        function keywordsFormGroup(groupService) {
            return {
                restrict: 'E',
                scope: {
                    entity: '@entity',
                    ngModel: '=ngModel',
                    type: '@kwType'
                },
                replace: false,
                transclude: true,
                //todo directives template projet à conditionellement
                template: '<div class="row" ng-repeat="group in groups" show-tpl="ngModel.template" tpl-field-name="{{group.id}}" tpl-field-type="keywords"><div class="form-group col-xs-12"><label>{{group.name}}</label><keyword-select multiple="group.multiple" ng-model="ngModel.keywords[\'\' + group.id]" ng-disabled="!ngModel._rights.update || ngModel.locked" configuration="group.selectConfig"></keyword-select></div></div>',
                link: function (scope) {
                    var filters = {
                        entities: {
                            op: 'sa_cnt',
                            val: scope.entity
                        }
                    };

                    if(typeof scope.type !== 'undefined'){
                        filters.type = {
                            op: 'eq',
                            val: scope.type
                        };
                    }

                    scope.$watch('ngModel.keywords', function () {
                        if (scope.ngModel !== null && typeof scope.ngModel !== 'undefined') {
                            if (typeof scope.ngModel.keywords == 'undefined' || angular.isArray(scope.ngModel.keywords)) {
                                scope.ngModel.keywords = {};
                            }
                        }
                    });
                    groupService.findAll({
                        col: [
                            'id', 'name', 'multiple'
                        ],
                        search: {
                            type: 'list',
                            data: {
                                sort: 'name',
                                order: 'asc',
                                filters: filters
                            }
                        }
                    }).then(function (res) {
                        for (var i in res.rows) {
                            res.rows[i].selectConfig = {
                                service: {
                                    apiParams: {
                                        search : {
                                            data : {
                                                filters: {
                                                    group : {
                                                        op: 'eq',
                                                        val: res.rows[i].id
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            };
                        }
                        scope.groups = res.rows;
                    });
                }
            }
        }
    ])

})(window.EVA.app);
