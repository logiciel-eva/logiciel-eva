<?php

namespace Indicator\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('indicator:e:indicator:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('indicator', false);
        }

        return new ViewModel([
            'groups' => $groups,
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('indicator:e:indicator', $id)) {
            return $this->notFoundAction();
        }

        $indicator = $this->entityManager()->getRepository('Indicator\Entity\Indicator')->find($id);

        if ($indicator && !$this->acl()->ownerControl('indicator:e:indicator:u', $indicator) && !$this->acl()->ownerControl('indicator:e:indicator:r', $indicator)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
			'isMaster' => $this->environment()
				->getClient()
				->isMaster(),
            'indicator' => $indicator
        ]);
    }
}
