<?php

return [
    'icons' => [
        'post-parc-synchro' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-link'
        ]
    ]
];
