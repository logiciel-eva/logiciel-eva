<?php

namespace Directory\Export;

use Core\Export\IExporter;

abstract class Structure implements IExporter
{
    public static function getConfig()
    {
        return [
            'financer' => [
                'format' => function ($value, $obj, $translator) {
                    return $translator($value ? 'yes' : 'no');
                }
            ]
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
