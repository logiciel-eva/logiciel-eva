<?php


namespace Profile\Controller\API;


use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class ProfileController extends BasicRestController
{
    protected $entityClass = 'Profile\Entity\Profile';
    protected $repository  = 'Profile\Entity\Profile';
    protected $entityChain  = 'task:e:task';


    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {

        $sort  = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}