<?php

namespace Core\PhantomJS;

/**
 * Wrapper to use PhantomJs.
 *
 * Notice : due to env limitation, do not work with "localhost" domain.
 */
class PhantomJS
{
    const PHANTOM_DIR = DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'phantomjs' . DIRECTORY_SEPARATOR;
    const LOG_FILE = DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'phpdocx' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . 'phantomjs-$(date \'+%Y-%m-%d\').log';

    /** track PhantomJS logs */
    const DEBUG = false;

    /**
     * @var string  environnement variables for exec PhantomJS
     *
     * OPENSSL_CONF : was set to null for prevent ssl errors with PhantomJS 2.x
     */
    const UNIX_VARIABLES_CTX = 'OPENSSL_CONF=/dev/null';

    public static function capture($url, $file, $size = null, $authCookie = null)
    {
        self::exec('rasterize.js', $url, $file, $size, $authCookie);
    }

    public static function captureWaitFor($url, $file, $size = null, $authCookie = null)
    {
        self::exec('rasterizeWaitFor.js', $url, $file, $size, $authCookie);
    }

    private static function exec($raster, $url, $file, $size, $authCookie)
    {
        $urlWrap = $url ? '"' . $url . '"' : '';
        $fileWrap = $file ? '"' . $file . '"' : '';
        $sizeWrap = $size ? '"' . $size . '"' : '';
        $authWrap = $authCookie ? '"' . $authCookie . '"' : '';

        $cmd = getcwd() . self::PHANTOM_DIR . "$raster $urlWrap $fileWrap $sizeWrap $authWrap";
        self::execOnUnixCtx($cmd);
    }

    private static function execOnUnixCtx($cmd)
    {
        $debugMode = self::DEBUG ? '--remote-debugger-port=9000 --remote-debugger-autorun=true' : '';
        $params = self::UNIX_VARIABLES_CTX . ' ' . getcwd() . self::PHANTOM_DIR . "phantomjs $debugMode --ignore-ssl-errors=true";
        $logPath = getcwd() . self::LOG_FILE;
        $unixCmd = $params . ' ' . $cmd . " >> $logPath";
        $exitCode = 0;
        $result = [];

        exec($unixCmd, $result, $exitCode);

        if ($exitCode > 0) {
            throw new \Exception("Fail to generated output render : " . implode(' ; ', $result));
        }
    }
}
