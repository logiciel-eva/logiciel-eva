# EVA

voir la documentation technique dans le [WIKI](https://gitlab.com/logiciel-eva/logiciel-eva/-/wikis/home)

## Table of content
- [EVA](#eva)
  - [Table of content](#table-of-content)
  - [Specifics functionalities](#specifics-functionalities)
    - [Technologies](#technologies)
    - [Configurations allowed](#configurations-allowed)
    - [Clients and Network management](#clients-and-network-management)
    - [Rights](#rights)
  - [Project structure](#project-structure)
  - [More details](#more-details)
    - [Admin and clients management](#admin-and-clients-management)
    - [Clients, Networks and master](#clients-networks-and-master)
    - [Users rights](#users-rights)
    - [More configurations by module](#more-configurations-by-module)
    - [Custom Exports](#custom-exports)
  - [Install EVA with Docker](#install-eva-with-docker)
  - [Install EVA locally](#install-eva-locally)
    - [Clone the remote repository locally](#clone-the-remote-repository-locally)
    - [Get PHP dependencies](#get-php-dependencies)
    - [Get dependencies managers](#get-dependencies-managers)
    - [Install the dependencies](#install-the-dependencies)
    - [Configure folder ownership \& permissions](#configure-folder-ownership--permissions)
    - [Modify your hosts file](#modify-your-hosts-file)
    - [Create databases](#create-databases)
    - [Create Bootstrap module config file](#create-bootstrap-module-config-file)
    - [Create doctrine configuration file](#create-doctrine-configuration-file)
    - [Launch bin/update](#launch-binupdate)
    - [Create first role and user in admin DB](#create-first-role-and-user-in-admin-db)
    - [Launch PHP built-in web server](#launch-php-built-in-web-server)
    - [Create client(s)](#create-clients)
    - [Relaunch bin/update](#relaunch-binupdate)
    - [Connect to client(s)](#connect-to-clients)
    - [It's done !](#its-done-)
  - [Connection to SQL Server (AGE)](#connection-to-sql-server-age)
    
## Specifics functionalities

### Technologies

Laminas MVC

AngularJS

Note : this is not a typical structure where the back and the front are separated. Laminas is doing the
routing and in the views we are loading AngularJS. So AngularJS is used to make the pages more 
dynamic and is calling the REST API in Laminas. That's why in Laminas Controllers we have 2 types of
controllers, the typical ones used in MVC framework that returns HTML response and the API 
Controllers that return JSON response.

### Configurations allowed

This project is meant to be used by multiple clients while allowing them to personalize the 
appearance, functionalities and translations.

There is multiple levels of personalization :
- in the Admin database, we can define for each client what module is activated, what translation 
is used and what icons should be used.
- in the Client interface, he can personalize (if authorized via the Admin Interface) if a module 
is loaded and some translations.

### Clients and Network management

In the admin interface, we can create some networks. Then we can attach some clients to these 
networks. One of them will be the master of this network and he will be allow to analyze the datas 
of all the clients of his network.

### Rights

There is a right management personalizable by each client. They can create some roles and define 
for each roles the CRUD (Create, Read, Update, Delete) for each entities. There is also a owner 
notion in certain entity.

## Project structure

```
|-- bin/                                  --> Executable files
|-- config/                               --> Config files for Laminas
|-- data/                                 --> Some files of librairies
|-- module/                               --> All Laminas code, divide into modules
    |-- ...  
    |-- Project/                          --> Project module
        |-- config/                       --> Config files of this module 
            |-- doctrine.config.php       --> Config file for doctrine mapping 
            |-- icons.config.php          --> Config file to define some icon helpers 
            |-- menu.config.php           --> Config file to define what the menu should look like
                                              when the module is loaded 
            |-- module.config.php         --> Config file of this module  
            |-- router.config.php         --> Config file to define all the routes of this module, 
                                              shared between the normal routes and the API routes, 
                                              and defines what controller and function to call in it 
        |-- language/                     --> Folder containing all the translations 
        |-- src/                          --> Source files 
            |-- Controller/               --> Controllers 
                |-- API/                  --> API Controllers 
                    |-- ...      
                |-- IndexController.php   --> Controller returning HTML views 
                |-- ...  
            |-- Entity/                   --> Entity classes used by Doctrine
            |-- Event/                    --> EventListeners or Subscribers
            |-- Export/                   --> Config classes used when doing default excel exports
            |-- Repository/               --> Repository classes
            |-- View/                     --> View Helpers used in HTML views 
            |-- Module.php                --> Configure the module
        |-- view/                         --> HTML view files
    |-- ...       
|-- node_modules/                         --> Dependencies of package.json
|-- public/                               --> Files accessible by the front
    |-- clients/                          --> Client specific files
    |-- css/                              --> LESS/CSS files
    |-- img/                              --> Images
    |-- js/                               --> All JS files, including AngularJS files
        |-- ...         
        |-- src/                          --> All the AngularJS files, divide into modules 
            |-- ...              
            |-- project/                  --> Project module
                |-- controller/           --> Controller files
                |-- directive/            --> Directive files
                |-- service/              --> Service files
            |-- ...              
            |-- app.js                    --> Main AngularJS file       
        |-- ...       
    |-- theme/                            --> LESS/CSS files specifics to each themes
    |-- vendor/                           --> Dependencies of bower.json
    |-- .htaccess                         --> Specificis rules for Apache
    |-- index.php                         --> index file used by all the routes
|-- vendor/                               --> Dependencies of composer.json
    |-- ...                               --> Git config files and dependencies files
```

## More details

### Admin and clients management

There is 2 types of database in this project :
- one database for the admin client
- one database for each client

These 2 types of databases contains different tables.

The database for the admin will contain all the informations about each client and how to connect
to their databases. 

The configuration to connect to the admin DB is in `config/autoload/doctrine.local.php`. It's a 
git ignored file and you should have created it if you have followed the tutoriel on how to install 
EVA locally.

Laminas will know if you are connecting to the admin host by checking the admin host configured in 
`module/Bootstrap/config/module.config.php`. 

If the host is different to the admin host, we will check in the admin DB if a client with the same
host exists and if it's the case we will create a Doctrine entityManager with the client's DB 
configuration.

### Clients, Networks and master

In the admin interface, we can create networks. A network is usefull to analyze the datas of 
multiple clients at the same time. So a network contains multiple clients and one client is 
the master of this network.

The master can create some entities that will be accessible to all the clients of his network
but the clients cannot modify them. He can create some Indicators and some Keywords.

Note : there is a special case where a client can modify the keywords of his master. In fact, he 
can't really modify them, but if we modify the database to add some children keywords to the 
keywords created by the master, when the master will analyze the datas by keywords, if the checkbox 
"Arborescent" is checked, it will also check the children of the keywords created by the master. So
the datas analyzed are still good and that allow the clients to customize a bit to their need these
master keywords. You can also rename the keywords in the database of the client.

So the master can analyze the datas of his clients, but not all the datas. The clients must specify
the projects that are accessible to the master threw the networkAccessible property.

### Users rights

The rights are defined threw the terms ACL.

A user is linked to a Role and a Role have some rights by entity. 

The list of entities that are available in the Role form is fetched from each module activated for
the current client by checking in `config/module.config.php` in the key `acl`. They are also define 
by the module name defined in this same file.

The right chain is defined with CRUD (Create, Read, Update, Delete) action in mind.

The right chain string is like this `module_name:e:entity_name:c`.

This right chain will check if the user has the right to create the entity.

The possibilities other than `c` are `r`, `u` and `d`.

The `e` means `entity`. There is also another possibility, if you want to limit a certain property 
with the rights. The key is `p` and the chain looks like that 
`module_name:p:entity_name:c:property_name`.

There is also a owner notion. It is avalaible only to the entities where the function `ownerControl`
is defined. You can write your own custom logic in it and if for example the right update is set 
to owner, then this function is called to check if the current user has the correct right.

### More configurations by module

In the admin interface, we can toggle a module or let a client toggle the module himself. 

But there is more configuration possible, in `config/module.config.php`, you can define in 
`configuration` key some other functionalities configurable for each client.

For example, in the Budget module, we can check HT activation. It allows to have 2 amounts for the 
transactions instead of 1, one amount VAT and the other duty free. Not all clients want these 2 
amounts, so it's configurable like that.

To check if a configuration is active, we have some functions avalaible :

- in views : 

```php
<?php

$budgetHTEnabled = $this->module()->getClientModuleConfiguration('budget', 'ht');
```

- in controllers : 

```php
<?php

$budgetHTEnabled = $this->serviceLocator
  ->get('MyModuleManager')
  ->getClientModuleConfiguration('budget', 'ht');
```

- adding and creating a module :
You must add a line in `composer.json` to allow PSR-4 autoloading the needed class :

```json
  "autoload": {
    "psr-4": {
      ...
      "Namespace\\": "module/ModuleName/src/",
      ...
    },
```

### Custom Exports

In the application, the users can define their own exports, in Excel and in Word. But sometimes it's 
not enough and they want a custom export.

They are saved for git in `data/exports/` but to allow a client to use them, we have to copy paste
them in `public/clients/client_name/exports/`. 

In the controllers, we can get the list of custom exports avalaible for the current client by using
this function `Export\Entity\Custom::getExports()` and then do the export using this function
`Export\Entity\Custom::doExport()`.

## Install EVA with Docker
* Add Hostname resolution on your Host File :
```
127.0.0.1 client1.eva.local
```

* Build environnements :
```bash
docker-compose --project-name eva -f eva-dev.yml up --build -d
docker exec -ti eva-www helper install
```

* Destroy environnements
```bash
docker-compose --project-name eva -f eva-dev.yml down -v
```

* Endpoints : 
  * http://client1.eva.local
    * login : ```BALDON``` | pass : ```BALDON```
  * http://localhost:1080/#/

## Install EVA locally 

### Clone the remote repository locally

```shell script
git clone https://gitlab.com/logiciel-eva/logiciel-eva.git
```

### Get PHP dependencies

EVA runs without warnings on PHP 8.2.

You must install these PHP dependencies :
* apcu
* curl
* gd
* intl
* mbstring
* mysql
* tidy
* xml
* zip
* ssh2
* imagick

### Get dependencies managers

You must have composer and bower to get the dependencies of the project.

### Install the dependencies

```shell script
cd logiciel-eva
composer install
bower install
```

### Configure folder ownership & permissions

```shell script
cd logiciel-eva
chown www-data. data -Rf
chmod 660 data/ -Rf
```

### Modify your hosts file 

Hosts file path :
```
Linux : /etc/hosts
Windows : C:\Windows\System32\Drivers\etc\hosts
```

Add this line if you don't have it already :

```
127.0.0.1 localhost
```

And for each client, you must add a new line :

```
127.0.0.1 client1.eva.local
```

### Create databases

You must have MariaDB or another databases manager.

Create 1 MySQL database for the admin (example: eva_admin).

Create 1 MySQL database for each client (example: eva_client1).

### Create doctrine configuration file 

You must create the doctrine configuration file *config/autoload/doctrine.local.php*

Put the following content, replace `dbname` value by the name of your admin database name and modify the other params to allow doctrine to connect to your database :

```php
<?php

/**
 * Retrieve the APP_ENV value.
 * Defined in public/.htaccess file.
 */
$env = getenv('APP_ENV') ? : 'development';

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => \Doctrine\DBAL\Driver\PDO\MySQL\Driver::class,
                'params' => [
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => 'mysql',
                    'dbname'   => 'eva_admin',
                    'driverOptions' => [
                        1002 => 'SET NAMES utf8'
                    ],
                ],
            ]
        ],
        'entitymanager' => [
            'orm_default' => [
                'connection'    => 'orm_default',
                'configuration' => 'orm_default',
            ]
        ],
        'configuration' => [
            'orm_default' => [
                'metadata_cache'    => ($env === 'development' ? 'array' : 'apcu'),
                'query_cache'       => ($env === 'development' ? 'array' : 'apcu'),
                'result_cache'      => ($env === 'development' ? 'array' : 'apcu'),
                'driver'            => 'orm_default',
                'generate_proxies'  => ($env === 'development'),
                'proxy_dir'         => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace'   => 'DoctrineORMModule\Proxy',
                'filters'           => []
            ]
        ],
        'driver' => [
            'orm_default' => [
                'class'   => \Doctrine\Persistence\Mapping\Driver\MappingDriverChain::class,
            ]
        ]
    ],
];

```

### Launch bin/update

The command will execute many operations to update the project and the databases.

```shell script
php bin/update
```

### Create first role and user in admin DB

Copy and Paste this data in your admin DB in user_role table :

```sql
INSERT INTO eva_admin.user_role (id, name, rights, createdAt, updatedAt, createdBy_id, updatedBy_id) 
VALUES (1, 'Administrateur', '{"user:e:user:c":{"value":true},"user:e:role:c":{"value":true},"project:e:project:c":{"value":false},"user:e:user:r":{"value":true,"owner":"all"},"user:e:role:r":{"value":true,"owner":"all"},"project:e:project:r":{"value":false},"project:e:project:u":{"value":false},"project:e:project:d":{"value":false},"user:e:user:u":{"value":true,"owner":"all"},"user:e:role:u":{"value":true,"owner":"all"},"user:e:user:d":{"value":true,"owner":"all"},"user:e:role:d":{"value":true,"owner":"all"},"admin:e:client:c":{"value":true},"admin:e:client:r":{"value":true,"owner":"all"},"admin:e:client:u":{"value":true,"owner":"all"},"admin:e:client:d":{"value":true,"owner":"all"},"admin:e:network:c":{"value":true},"admin:e:network:r":{"value":true},"admin:e:network:u":{"value":true},"admin:e:network:d":{"value":true}}', null, null, null, null);
```

Copy and Paste this data in your admin DB in user_user table :

```sql
INSERT INTO eva_admin.user_user (id, role_id, login, password, avatar, email, color, disabled, gender, firstName, lastName, name, createdAt, updatedAt, createdBy_id, updatedBy_id) 
VALUES (1, 1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '', '', '', 0, '', '', '', '', null, null, null, null);
```

### Create client(s)

A client can be created in the admin database using a query like:

```sql
INSERT INTO eva_admin.eva_client (network_id,name,`key`,host,dbHost,dbPort,dbUser,dbPassword,dbName,theme,modules,translations,icons,email,createdAt,updatedAt,createdBy_id,updatedBy_id) VALUES
	 (1,'Client 1','client1','client1.eva.local','localhost','3306','root','mysql','eva_client1','parc','{"age": {"active": true, "activable": true, "configuration": {"allowedIncomeTypes": ["requested", "notified", "assigned", "perceived", "dv"], "allowedExpenseTypes": ["planned", "committed", "paid", "released", "committedPaid", "aeEstimated", "ae", "aeCommitted", "cpEstimated", "cp", "cpCommitted", "dv", "portfolio"]}}, "map": {"active": true, "activable": true}, "home": {"active": true, "activable": true}, "memo": {"active": false, "activable": false}, "task": {"active": true, "activable": true}, "time": {"active": true, "activable": true}, "alert": {"active": true, "activable": true}, "field": {"active": true, "activable": true}, "budget": {"active": true, "activable": true, "configuration": {"ht": true, "code": true, "gbcp": true, "evolution": false, "incomeTypes": ["requested", "notified", "assigned", "perceived"], "expenseTypes": ["aeEstimated", "ae", "aeCommitted", "cpEstimated", "cp", "cpCommitted"]}}, "campain": {"active": true, "activable": true}, "keyword": {"active": true, "activable": true}, "profile": {"active": false, "activable": false}, "project": {"active": true, "activable": true}, "postParc": {"active": false, "activable": true, "configuration": {}}, "indicator": {"active": true, "activable": true, "configuration": {"analysis_bar": true, "analysis_pie": true, "analysis_line": true, "analysis_bubble": true, "analysis_measures_bar": true}}, "convention": {"active": true, "activable": true}, "advancement": {"active": true, "activable": true}, "application": {"active": true, "activable": false}}','[]','[]',NULL,NULL,NULL,NULL,NULL);
```

### Relaunch bin/update

```shell script
php bin/update
```

### Connect to client(s)

You can now launch PHP buil-in web server to connect to your admin host :

```shell script
php -S client1.eva.local:8000 -t public/
```

And connect to your client host in a browser (example: http://client1.eva.local:8000)

```
username: admin
password: admin
```

### It's done !

Well play, now when you want to add a client, just add a new line in your hosts file, create a new database and create a new client in the admin interface.

## Connection to SQL Server (AGE)

If you want to be able to connect to a SQL Server (by using the link with AGE), you'll want to install the necessary PHP drivers.

You have to follow the documentation at https://docs.microsoft.com/en-us/sql/connect/php/microsoft-php-driver-for-sql-server?view=sql-server-2017
