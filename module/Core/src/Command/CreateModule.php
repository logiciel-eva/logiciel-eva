<?php

namespace Core\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument as InputArgumentAlias;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Create new module on project.
 */
class CreateModule extends AbstractCommand
{

    protected function arguments(): void
    {
        $this->addArgument('ModuleName', InputArgumentAlias::REQUIRED, 'Module\'s name (Exemple "MyModule") ?');
    }

    /**
     * Call :
     * <pre>php vendor/bin/laminas core:create-module <ModuleName></pre>
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('ModuleName');
        $output->writeln('Creating module ' . ucfirst($name));

        $moduleDirectory    = getcwd() . '/module/' . ucfirst($name);
        $configDirectory    = $moduleDirectory . '/config';
        $languageDirectory  = $moduleDirectory . '/language';
        $srcDirectory       = $moduleDirectory . '/src';
        $srcModuleDirectory = $moduleDirectory . '/src/' . ucfirst($name);
        $viewDirectory      = $moduleDirectory . '/view';

        $output->writeln('- Creating directory : ' . $moduleDirectory);
        mkdir($moduleDirectory, 0755);
        $output->writeln('- Creating file      : ' . $moduleDirectory . '/Module.php');
        file_put_contents(
            $moduleDirectory . '/Module.php',
            '<?php' . PHP_EOL .
            PHP_EOL .
            'namespace ' . ucfirst($name) . ';' . PHP_EOL .
            PHP_EOL .
            'use Core\Module\AbstractModule;' . PHP_EOL .
            PHP_EOL .
            'class Module extends AbstractModule' . PHP_EOL .
            '{' . PHP_EOL .
            PHP_EOL .
            '}' . PHP_EOL
        );


        $output->writeln('- Creating directory : ' . $configDirectory);
        mkdir($configDirectory, 0755);
        $output->writeln('- Creating file      : ' . $configDirectory . '/doctrine.config.php');
        file_put_contents(
            $configDirectory . '/doctrine.config.php',
            '<?php' . PHP_EOL .
            PHP_EOL .
            'namespace ' . ucfirst($name) . ';' . PHP_EOL .
            PHP_EOL .
            'return [' . PHP_EOL .
            '    \'doctrine\' => [' . PHP_EOL .
            '        \'driver\' => [' . PHP_EOL .
            '            \'' . strtolower($name) . '_entities\' => [' . PHP_EOL .
            '                \'class\' => \'Doctrine\ORM\Mapping\Driver\AnnotationDriver\',' . PHP_EOL .
            '                \'cache\' => \'array\',' . PHP_EOL .
            '                \'paths\' => [' . PHP_EOL .
            '                    __DIR__ . \'/../src/' . ucfirst($name) . '/Entity\'' . PHP_EOL .
            '                ]' . PHP_EOL .
            '            ],' . PHP_EOL .
            '            \'orm_environment\' => [' . PHP_EOL .
            '                \'drivers\' => [' . PHP_EOL .
            '                    \'' . ucfirst($name) . '\Entity\' => \'' . strtolower($name) . '_entities\'' . PHP_EOL .
            '                ]' . PHP_EOL .
            '            ]' . PHP_EOL .
            '        ]' . PHP_EOL .
            '    ]' . PHP_EOL .
            '];' . PHP_EOL
        );

        $output->writeln('- Creating file      : ' . $configDirectory . '/icons.config.php');
        file_put_contents(
            $configDirectory . '/icons.config.php',
            '<?php' . PHP_EOL .
            PHP_EOL .
            'return [' . PHP_EOL .
            '    \'icons\' => [' . PHP_EOL .
            PHP_EOL .
            '    ]' . PHP_EOL .
            '];' . PHP_EOL
        );

        $output->writeln('- Creating file      : ' . $configDirectory . '/menu.config.php');
        file_put_contents(
            $configDirectory . '/menu.config.php',
            '<?php' . PHP_EOL .
            PHP_EOL .
            'return [' . PHP_EOL .
            '    \'menu\' => [' . PHP_EOL .
            '        \'client-menu\' => [' . PHP_EOL .
            PHP_EOL .
            '        ]' . PHP_EOL .
            '    ]' . PHP_EOL .
            '];' . PHP_EOL
        );

        $output->writeln('- Creating file      : ' . $configDirectory . '/module.config.php');
        file_put_contents(
            $configDirectory . '/module.config.php',
            '<?php' . PHP_EOL .
            PHP_EOL .
            'return [' . PHP_EOL .
            '    \'module\' => [' . PHP_EOL .
            '        \'' . strtolower($name) . '\' => [' . PHP_EOL .
            '            \'environments\' => [' . PHP_EOL .
            PHP_EOL .
            '            ],' . PHP_EOL .
            '            \'active\'   => true,' . PHP_EOL .
            '            \'required\' => false,' . PHP_EOL .
            '            \'acl\' => [' . PHP_EOL .
            '                \'entities\' => [' . PHP_EOL .
            PHP_EOL .
            '                ]' . PHP_EOL .
            '            ]' . PHP_EOL .
            '        ]' . PHP_EOL .
            '    ],' . PHP_EOL .
            '    \'view_manager\' => [' . PHP_EOL .
            '        \'template_path_stack\' => [' . PHP_EOL .
            '            __DIR__ . \'/../view\'' . PHP_EOL .
            '        ],' . PHP_EOL .
            '    ],' . PHP_EOL .
            '    \'translator\' => [' . PHP_EOL .
            '        \'translation_file_patterns\' => [' . PHP_EOL .
            '            [' . PHP_EOL .
            '                \'type\'     => \'phparray\',' . PHP_EOL .
            '                \'base_dir\' => __DIR__ . \'/../language\',' . PHP_EOL .
            '                \'pattern\'  => \'%s.php\',' . PHP_EOL .
            '            ],' . PHP_EOL .
            '        ],' . PHP_EOL .
            '    ],' . PHP_EOL .
            '];' . PHP_EOL
        );

        $output->writeln('- Creating file      : ' . $configDirectory . '/router.config.php');
        file_put_contents(
            $configDirectory . '/router.config.php',
            '<?php' . PHP_EOL .
            PHP_EOL .
            'namespace ' . ucfirst($name) . ';' . PHP_EOL .
            PHP_EOL .
            'return [' . PHP_EOL .
            '    \'router\' => [' . PHP_EOL .
            '        \'client-routes\' => [' . PHP_EOL .
            PHP_EOL .
            '        ]' . PHP_EOL .
            '    ],' . PHP_EOL .
            '    \'controllers\' => [' . PHP_EOL .
            '        \'invokables\' => [' . PHP_EOL .
            PHP_EOL .
            '        ]' . PHP_EOL .
            '    ]' . PHP_EOL .
            '];' . PHP_EOL
        );


        $output->writeln('- Creating directory : ' . $languageDirectory);
        mkdir($languageDirectory, 0755);
        $output->writeln('- Creating file      : ' . $languageDirectory . '/fr_FR.php');
        file_put_contents(
            $languageDirectory . '/fr_FR.php',
            '<?php' . PHP_EOL .
            PHP_EOL .
            'return [' . PHP_EOL .
            PHP_EOL .
            '];' . PHP_EOL
        );


        $output->writeln('- Creating directory : ' . $srcDirectory);
        mkdir($srcDirectory, 0755);
        $output->writeln('- Creating directory : ' . $srcModuleDirectory);
        mkdir($srcModuleDirectory, 0755);
        $output->writeln('- Creating directory : ' . $srcModuleDirectory . '/Entity');
        mkdir($srcModuleDirectory . '/Entity', 0755);
        $output->writeln('- Creating directory : ' . $srcModuleDirectory . '/Controller');
        mkdir($srcModuleDirectory . '/Controller', 0755);
        $output->writeln('- Creating directory : ' . $srcModuleDirectory . '/Controller/API');
        mkdir($srcModuleDirectory . '/Controller/API', 0755);


        $output->writeln(PHP_EOL . '- Creating directory : ' . $viewDirectory);
        mkdir($viewDirectory, 0755);
        $output->writeln('- Creating directory : ' . $viewDirectory . '/' . strtolower($name));
        mkdir($viewDirectory . '/' . strtolower($name), 0755);

        $output->writeln('Done. Don\'t forget to load the ' . ucfirst($name) . ' module in ' . getcwd() . '/config/application.config.php');

        return Command::SUCCESS;
    }
}
