<?php

namespace Indicator\Controller\API;

use Bootstrap\Entity\Client;
use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Core\Filter\DateTimeFilter;
use DateTime;

class MeasureController extends BasicRestController
{
    protected $entityClass  = 'Indicator\Entity\Measure';
    protected $repository   = 'Indicator\Entity\Measure';
    protected $entityChain  = 'indicator:e:measure';
    private $dateFilter;

    public function __construct()
    {
        $this->dateFilter = new DateTimeFilter();
    }

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.date';
        $order = $data['order'] ? $data['order'] : 'ASC';

        $queryBuilder->join('object.indicator', 'indicator');
        $queryBuilder->leftJoin('object.project', 'project');
        $queryBuilder->leftJoin('object.campain', 'campain');
        $queryBuilder->leftJoin('object.groupIndicator', 'groupIndicator');
        $queryBuilder->leftJoin('object.campainMeasure', 'campainMeasure');

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.type LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
        if (isset($data['filters']) && isset($data['filters']['client'])) {
            $op = $data['filters']['client']['op'];
            if ($op == 'isNull'){
                $queryBuilder->orWhere(
                    'object.client IS NULL'
                );
            } else if($op == 'isNotNull'){
                $queryBuilder->orWhere(
                    'object.client IS NOT NULL'
                );
            } else {
                foreach($data['filters']['client']['val'] as $i => $id){
                    if ($op == 'eq'){
                        $queryBuilder->orWhere(
                           'JSON_EXTRACT(object.client ,\'$.id\') =:value'.$i
                        );
                        $queryBuilder->setParameter('value' . $i,   (int)$id );
                    }
                    else if ($op == 'neq'){
                        $queryBuilder->andWhere(
                          'NOT JSON_EXTRACT(object.client ,\'$.id\') =:value'.$i
                        );    
                       $queryBuilder->setParameter('value' . $i,   (int)$id );
                    }
                    
                }
            }
            unset($data['filters']['client']);
        }
        if(isset($data['filters'])&&isset($data['filters']['groupIndicator.id'])){
            $groupindicatorquery = $this->entityManager()->getrepository('Indicator\Entity\GroupIndicator')->createquerybuilder('gi');
            $indicatorIds = $groupindicatorquery->select('ind.id')
                ->join('gi.indicators', 'ind')
                ->where('gi in (:groupIndicatorId)')
                ->setParameter('groupIndicatorId', $data['filters']['groupIndicator.id'])
                ->getQuery()->getResult();

            $queryBuilder->Where('indicator.id in (:ids)')
            ->setParameter('ids', array_values($indicatorIds));
            
            unset($data['filters']['groupIndicator.id']);
        }
    }

    public function create($data)
    {
        if (!isset($data['client'])){
            $data['client'] = [];
        }
        //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . "Create new measure - ". json_encode($data) . PHP_EOL, FILE_APPEND);
        $currentClient = $this->environment()->getClient();

        $response = parent::create($data);
        //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . "Created measure - ". json_encode($response->getVariable("object")) . PHP_EOL, FILE_APPEND);
        $indicator = $this->getEntityManager()->getRepository('Indicator\Entity\Indicator')->findOneBy(['id' => $data['indicator']['id']]);
        // Save in the master
        if ($response->getVariable('success', false) === true && $indicator->getMaster()){
            $data['id'] = $response->getVariable('object')['id'];
            if(null !== $currentClient->getNetwork())
            {
                $this->createMeasureInMaster($data, $currentClient->getNetwork()->getMaster());
            }
        }

        // Save in the child
        if ($this->environment()->getClient()->isMaster() && null !== $this->environment()->getClient()->getNetwork())
        {
            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                if (isset($data['client']['id']) && $client->getId() == $data['client']['id']){
                    $data['id'] = $response->getVariable('object')['id'];
                    $this->createMeasureInSlave($data, $client);
                }
            }
        }


        return $response;
    }

    public function delete($id)
    {
        // for deletion in cascade
        $measure = $this->getEntityManager()->getRepository($this->repository)->find($id);
        $measureClient = $measure->getClient();
        $slaveMeasureId = $measure->getSlave();

        $response = parent::delete($id);

        if ($response->getVariable('success', false) === true) {
            $em = $this->entityManager();

            // On supprime les territoires
            $em ->createQuery('DELETE Map\Entity\Association e WHERE e.entity = :entity AND e.primary = :primary')
                ->setParameter('entity', $this->repository)
                ->setParameter('primary', $id)
                ->execute();
        }

        if ($this->environment()->getClient()->isMaster())
        {
            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client) {
                if(null === $measureClient || $client->getId() !== $measureClient['id']) {
                    continue;
                }
                $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
                $clientEm = $entityManagerFactory->getEntityManager($client);
                $clientMeasureRepository = $clientEm->getRepository($this->repository);

                $clientMeasure = null !== $slaveMeasureId
                    ? $clientMeasureRepository->find($slaveMeasureId)
                    : $clientMeasureRepository->findOneBy(['master' => $id]);

                if(null !== $clientMeasure) {
                    $clientEm->remove($clientMeasure);
                    $clientEm->flush();
                }
            }
        } else {
            $network = $this->environment()->getClient()->getNetwork();
            if(null !== $network) {
                $master = $network->getMaster();
                if(null !== $master) {
                    $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
                    $masterEm = $entityManagerFactory->getEntityManager($master);

                    $masterEm->createQuery('DELETE Indicator\Entity\Measure m WHERE m.slave = :slave_id AND JSON_EXTRACT(m.client ,\'$.id\') = :client_id')
                        ->setParameter('slave_id', $id)
                        ->setParameter('client_id', $this->environment()->getClient()->getId())
                        ->execute();
                }
                
            }
        }

        return $response;
    }

	public function update($id, $data)
    {
        if (!isset($data['client'])){
            $data['client'] = [];
        }

        $response = parent::update($id, $data);
        if($response->getVariable('success', false) === false)
        {
            return $response;
        }

        if(isset($data["campainMeasure"]) && isset($data["campainMeasure"]['id'])){
            $em = $this->getEntityManager();
            $campainMeasure = $em->getRepository('Campain\Entity\Measure')->find($data["campainMeasure"]['id']);
            if($campainMeasure !=null){
                $campainMeasure->setSupposedDate($this->dateFilter->filter($data["campainMeasure"]['supposedDate']));
                $em->persist($campainMeasure);
                $em->flush();
            }
        }
        if ($this->environment()->getClient()->isMaster() && null !== $this->environment()->getClient()->getNetwork())
        {
            $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');

            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client)
            {
                if ($client->getId() == $data['client']['id'])
                {
                    $clientEm = $entityManagerFactory->getEntityManager($client);
                    $measureClientRepository = $clientEm->getRepository('Indicator\Entity\Measure');

                    $clientMeasure = array_key_exists('slave', $data) && isset($data['slave'])
                        ? $measureClientRepository->findOneBy(['id' => $data['slave']])
                        : $measureClientRepository->findOneBy(['master' => $data['id']]);
                    
                    if(isset($clientMeasure))
                    {
                        $clientMeasure->setPeriod($data['period']);
                        $clientMeasure->setType($data['type']);
                        if(array_key_exists('date', $data) && null !== $data['date'])
                        {
                            $endDate = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $data['date'])
											? DateTime::createFromFormat('d/m/Y H:i', $data['date'])
											: DateTime::createFromFormat('d/m/Y', $data['date']);
                            $clientMeasure->setDate($endDate);
                        }
                        if(array_key_exists('start', $data) && null !== $data['start'])
                        {
                            $startDate = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $data['start'])
												? DateTime::createFromFormat('d/m/Y H:i', $data['start'])
												: DateTime::createFromFormat('d/m/Y', $data['start']);
                            $clientMeasure->setStart($startDate);
                        }
                        $clientMeasure->setValue($data['value']);
                        $clientMeasure->setSource($data['source']);
                        $clientMeasure->setComment($data['comment']);

                        $clientEm->persist($clientMeasure);
                        $clientEm->flush();
                    }
                }
            }
        } else {
            $network = $this->environment()->getClient()->getNetwork();
            if(null !== $network) {
                $master = $network->getMaster();
                if(null !== $master) {
                    // delete the line in the master client
                    $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
                    $masterEm = $entityManagerFactory->getEntityManager($master);
                    
                    $masterEm->createQuery('DELETE Indicator\Entity\Measure m WHERE m.slave = :slave_id AND JSON_EXTRACT(m.client ,\'$.id\') = :client_id')
                        ->setParameter('slave_id', $id)
                        ->setParameter('client_id', $this->environment()->getClient()->getId())
                        ->execute();

                    $data['id'] = $id;
                    $this->createMeasureInMaster($data, $master);
                }
                
            }
        }

        return $response;
    }

    private function applyDateFilter($data, $measurekey, $datekey ){
        if(isset($data[$measurekey][$datekey])){
            $data[$measurekey][$datekey] = $this->dateFilter->filter($data[$measurekey][$datekey]);
        }
        return $data;
    }

    /**
     * @param measure           array of data
     */
    private function createMeasureInMaster($measure, Client $masterClient)
    {
        $indicator = $this->getEntityManager()->getRepository('Indicator\Entity\Indicator')->findOneBy(['id' => $measure['indicator']['id']]);

        // setup controller with client (master) context
        $controllerManager   = $this->serviceLocator->get('ControllerManager');
        $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');
        $controllerMeasure->setClient($masterClient);
        
        $currentClient = $this->environment()->getClient();

        $clientData = $measure;
        $clientData['id'] = null;
        $clientData['project'] = null;
        $clientData['campain'] = null;
        $clientData['indicator']['id'] = $indicator->getMaster();
        $clientData['client'] = array('id' => $currentClient->getId(), 'name' => $currentClient->getName(), 'selected' => true); 
        $clientData['slave'] = $measure['id'];

        $response = parent::create($clientData);

        // reset controller client context to current client
        $controllerMeasure->setClient($currentClient);
    }

    private function createMeasureInSlave($measure, Client $slaveClient)
    {
        $currentClient = $this->environment()->getClient();

        // setup controller with client (slave) context
        $controllerManager   = $this->serviceLocator->get('ControllerManager');
        $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');
        $controllerMeasure->setClient($slaveClient);

        $indicator = $this->getEntityManager()->getRepository('Indicator\Entity\Indicator')->findOneBy(['master' => $measure['indicator']['id']]);
        $clientData = $measure;
        $clientData['id'] = null;
        $clientData['project'] = null;
        $clientData['campain'] = null;
        $clientData['indicator']['id'] = $indicator->getId();
        $clientData['indicator']['master'] = $indicator->getMaster();
        $clientData['client'] = null;
        $clientData['master'] = $measure['id'];
        parent::create($clientData);

        // reset controller client context to current client
        $controllerMeasure->setClient($currentClient);
    }

}
