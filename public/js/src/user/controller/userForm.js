(function(app) {

    app.controller('userFormController', [
        'userService', '$scope', 'FileUploader', 'flashMessenger', 'translator', 'userConfigurationService',
        function userFormController(userService, $scope, FileUploader, flashMessenger, translator, userConfigurationService) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'gender',
                    'firstName',
                    'lastName',
                    'login',
                    'password',
                    'role.id',
                    'role.name',
                    'profile.id',
                    'profile.name',
                    'worked_hours',
                    'avatar',
                    'email',
                    'keywords',
                    'color',
                    'disabled'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.user = {
                avatar: '/img/default-avatar.png',
                color: '#009688',
                _rights: {
                    update: true
                }
            };


            self.userConnectedId = null;

            self.uploader = new FileUploader();
            self.uploader.filters.push({
                name: 'imageFilter',
                fn: function(item, options) {
                    //var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    var type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase() + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            });
            self.reader   = new FileReader();
            self.reader.onloadend = function(e) {
                $scope.$apply(function() {
                    self.user.avatar = e.target.result;
                })
            };

            self.uploader.onAfterAddingFile = function(fileItem) {
                self.reader.readAsDataURL(fileItem._file);
                self.errors.fields['avatar'] = null;
            };
            self.uploader.onWhenAddingFileFailed = function(item , filter, options) {
                self.errors.fields['avatar'] = ['Veuillez sélectionner un fichier image (jpg, png, bmp, gif)'];
            };

            self.init = function init(id, userConnectedId) {
                self.userConnectedId = userConnectedId;
                if (id) {
                    userService.find(id, apiParams).then(function(res) {
                        self.user       = res.object;
                        self.panelTitle = self.user.login;
                    })
                    userConfigurationService.getConfigurationWithTable().then(function(res) {
                        self.configuration = res;
                    })
                }
            };

            self.saveUser = function saveUser() {
                self.errors.fields  = {};
                self.loading.save   = true;
                self.save();
                userConfigurationService.saveCustomisation(self.configuration).then(function(res){
                    self.loading.save   = false;
                });
            };

            self.saveCustomisation = function saveCustomisation() {
                self.errors.fields  = {};
                self.loading.save   = true;
                userConfigurationService.saveCustomisation(self.configuration).then(function(res){
                    self.loading.save   = false;

                });
            };

            self.save = function save() {
                var isCreation = !self.user.id;
                userService.save(self.user, apiParams).then(function(res) {
                    self.user         = res.object;
                    self.panelTitle   = self.user.login;
                    self.loading.save = false;
                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.user.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('user_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.saveUserDisabled = function () {
                var user = {
                    id: self.user.id,
                    disabled: self.user.disabled
                };

                userService.save(user, apiParams).then(function (res) {
                    self.user.disabled = res.object.disabled;

                    flashMessenger.success(translator('user_message_disabled_' + (res.object.disabled ? 'true' : 'false')));
                }, function () {
                    flashMessenger.success(translator('error_occured'));
                });
            };

            self.deleteUser = function deleteUser() {
                if (confirm(translator('user_question_delete').replace('%1', self.user.login))) {
                    userService.remove(self.user).then(function(res) {
                        if (res.success) {
                            window.location = '/user';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
