(function (angular, app) {

    app.controller('navbarController', [
        '$timeout',
        function navbarController($timeout) {
            var self = this;

            angular.element('.nav-container .nav li').on('click', function (event) {
                event.stopPropagation();
                if ($(this).hasClass('open')) {
                    $(this).removeClass('open');
                    $(this).find('.nav, .nav li').removeClass('open');
                } else {
                    $('.nav, .nav li').removeClass('open');
                    $(this).addClass('open');
                    $(this).find(' > .nav').addClass('open');

                    // For the sub-submenus, keep the parent open
                    $(this).parent('ul').addClass('open').parent('li').addClass('open');
                }
            });

            if ($('#__theme').text() !== 'parc') {
                angular.element('.nav a').each(function() {
                    var href = $(this).attr('href');
                    if (href == window.location.pathname) {
                        $(this).addClass('active');
                        $(this).parents('.nav').parents('li').find('> .nav').addClass('open');
                        $(this).parents('.nav').parents('li').addClass('open');
                    }
                });
            }


            var $navScroller = angular.element('.nav-scroller');
            var $nav = angular.element('.nav-container');
            var $contentScroller = angular.element('.content-scroller');
            var $container = angular.element('.container-fluid');
            var $contentContainer = angular.element('.content-container');
            var $body = angular.element('body');
            var $headerContainer = angular.element('.header-container');
            var $footer = angular.element('.footer');

            self.layoutScrollers = function layoutScrollers() {
                $timeout(function () {
                    //var height = $body.height() - $headerContainer.outerHeight();

                    /**
                     * Curtis: j'ai modifié la hauteur car cela foirait quand
                     * on avait un menu horizontal.
                     *
                     * Jules: <-- du coup la modif fait planter le menu vertical ><
                     */
                    var pageHeight = $body.height() - $headerContainer.outerHeight();
                    var height = $body.height() - $headerContainer.outerHeight() - $footer.outerHeight();

                    if ($nav.height() < height && $('#__theme').text() === 'parc') {
                        height -= $nav.height();
                    }


                    $navScroller.css('max-height', height);
                    $contentScroller.css('max-height', height);
                    $contentScroller.css('min-height', height);

                    /* if (angular.element(window).width() > 768) {
                     $container.css('width', angular.element(window).width() - $nav.outerWidth() + 30);
                     } else {
                     $container.css('width', 'auto');
                     }*/

                    //$navScroller.perfectScrollbar('update');
                    //$contentScroller.perfectScrollbar('update');
                })
            };

            angular.element(window).on('resize', function () {
                self.layoutScrollers();
            });

            /* $navScroller.perfectScrollbar();
             $contentScroller.perfectScrollbar({
             suppressScrollX: true
             });*/

            self.layoutScrollers();
        }
    ])

})(window.angular, window.EVA.app);
