(function (app) {

    app.controller('posteExpenseFormController', [
        'posteExpenseService', 'flashMessenger', 'translator', '$scope',
        function posteExpenseFormController(posteExpenseService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal    = angular.element('#posteExpenseFormModal');
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'account.id',
                    'account.name',
                    'account.code',
                    'nature.id',
                    'nature.name',
                    'nature.code',
                    'organization.id',
                    'organization.name',
                    'organization.code',
                    'destination.id',
                    'destination.name',
                    'destination.code',
                    'parent.id',
                    'parent.name',
                    'parent.account.id',
                    'parent.account.code',
                    'project.id',
                    'project.name',
                    'project.parent.id',
                    'envelope.id',
                    'envelope.name',
                    'amount',
                    'amountHT',
                    'amountArbo',
                    'amountHTArbo',
                    'amountAggreg',
                    'amountHTAggreg',
                    'createdAt',
                    'updatedAt',
                    'expensesArbo.id',
                    'expensesArbo.name',
                    'expensesArbo.amount',
                    'expensesArbo.amountHT',
                    'expensesArbo.date',
                    'expensesArbo.type',
                    'expensesArbo.poste.id',
                    'expensesArbo.keywords',
                    'expensesArbo.tiers',
                    'expensesArbo.anneeExercice',
                    'expensesArbo.bordereau',
                    'expensesArbo.mandat',
                    'expensesArbo.complement',
                    'expensesArbo.engagement',
                    'expensesArbo.dateFacture',
                    'expensesArbo.project.id',
                    'expensesArbo.project.name',
                    'expensesArbo.age',
                    'keywords',
                    'createdAt',
                    'updatedAt'
                ]
            };

            self.posteExpense = {};
            self.object       = null;
            self.from         = {
                type: null,
                id  : null
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(from, id, types, balances) {
                self.from.type = from;
                self.from.id   = id;
                if (self.from.type == 'project') {
                    self.object = $scope.$parent.$parent.$parent.self[self.from.type];
                }

                for (var i in types) {
                    apiParams.col.push('amountArbo' + types[i].ucfirst());
                    apiParams.col.push('amountHTArbo' + types[i].ucfirst());
                }

                for (var j in balances) {
                    apiParams.col.push('amountArbo' + balances[j].ucfirst());
                    apiParams.col.push('amountHTArbo' + balances[j].ucfirst());
                }
            };

            self.savePosteExpense = function savePosteExpense() {
                self.errors.fields = {};
                self.loading.save  = true;

                var isCreation = !self.posteExpense.id;

                posteExpenseService.save(self.posteExpense, apiParams).then(function (res) {
                    if (isCreation) {
                        $scope.$parent.self.posteExpenses.splice($scope.$parent.self.posteExpenses.length - 1, 0, res.object);
                    } else {
                        for (var i in $scope.$parent.self.posteExpenses) {
                            if ($scope.$parent.self.posteExpenses[i].id == res.object.id) {
                                $scope.$parent.self.posteExpenses[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('poste_expense_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            $scope.$on('create-poste-expense', function () {
                self.posteExpense = {
                    _rights: {
                        update: true
                    },
                    amount : 0,
                    amountHT : 0
                };

                self.errors = {
                    fields: {}
                };

                if (self.from.type == 'project') {
                    self.posteExpense.project = self.object.id;
                }

                //self.panelTitle = translator('poste_expense_nav_form');
                $modal.modal('show');

                // Pour reinitialiser le select account
                $modal.on('hidden.bs.modal', function () {
                    $scope.$apply(function () {
                        self.reset = false;
                    });
                });
                self.reset = true;
            });

            $scope.$on('edit-poste-expense', function (event, posteExpense) {
                self.editedPosteExpense = posteExpense;
                self.posteExpense       = angular.copy(posteExpense);
                self.panelTitle         = self.posteExpense.name;

                self.errors = {
                    fields: {}
                };

                $modal.modal('show');

                // Pour reinitialiser le select account
                $modal.on('hidden.bs.modal', function () {
                    $scope.$apply(function () {
                        self.reset = false;
                    });
                });
                self.reset = true;
            });
        }
    ]);

})(window.EVA.app);
