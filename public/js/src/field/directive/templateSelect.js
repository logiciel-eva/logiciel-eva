(function(app) {

    app.directive('templateSelect', [
        'templateService', 'selectFactoryService',
        function templateSelect(templateService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: templateService,
                    apiParams: {
                        col: ['id', 'name']
                    },
                    full: true
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['name'],
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' +
                                '<span class="name">' + escape(data.name) + '</span>' +
                            '</div>';
                        },
                        item: function (data, escape) {
                            return '<div class="option">' +
                                '<span class="name">' + escape(data.name) + '</span>' +
                            '</div>';
                        }
                    }
                }
            });
        }
    ]);

})(window.EVA.app);
