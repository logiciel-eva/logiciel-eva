<?php

namespace Project;

return [
    'doctrine' => [
        'driver' => [
            'project_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Project\Entity' => 'project_entities'
                ]
            ]
        ],
    ]
];
