<?php

$task = [
    'icon'     => 'task',
    'right'    => 'task:e:task:r',
    'title'    => 'task_module_title',
    'href'     => 'task',
    'priority' => 1000,
];

return [
    'menu' => [
        'client-menu' => [
            'task' => $task,
        ],
        'client-menu-parc' => [
            'suivi' => [
                'children' => [
                    'task' => $task,
                ],
            ],
        ],
    ],
];
