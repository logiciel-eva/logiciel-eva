<?php

namespace Budget\Controller\API;

use Budget\Entity\Income;
use Budget\Entity\PosteIncome;
use Budget\Helper\PosteSorter;
use Core\Controller\BasicRestController;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Laminas\Http\Request;
use Laminas\View\Model\JsonModel;

class PosteIncomeController extends BasicRestController
{
	protected $entityClass = 'Budget\Entity\PosteIncome';
	protected $repository = 'Budget\Entity\PosteIncome';
	protected $entityChain = 'budget:e:posteIncome';



	protected function extract($object)
	{
		$array = parent::extract($object);
		$done = false;
		if (!$this->fromConsole) {
			if ($this->columns) {
				foreach ($this->columns as $column) {
					if (strpos($column, 'incomesArbo') !== false && $done === false) {
						foreach ($object->getIncomesArbo() as $i => $income) {
							$array['incomesArbo'][$i]['_rights'] = [
								'read' => $this->acl()->ownerControl('budget:e:income:r', $income),
								'update' => $this->acl()->ownerControl('budget:e:income:u', $income),
								'delete' => $this->acl()->ownerControl('budget:e:income:d', $income),
							];
						}
						$done = true;
					}
				}
			}
		}

		return $array;
	}

	/**
	 * @param QueryBuilder $queryBuilder
	 * @param $data
	 */
	protected function searchList(QueryBuilder $queryBuilder, &$data)
	{
		$sort = $data['sort'] ? $data['sort'] : 'object.name';
		$order = $data['order'] ? $data['order'] : 'ASC';

		if (strpos($sort, '.') === false) {
			$sort = 'object.' . $sort;
		}

		$queryBuilder->join('object.project', 'project');
		$queryBuilder->leftJoin('object.account', 'account');
		$queryBuilder->leftJoin('object.envelope', 'envelope');
		$queryBuilder->leftJoin('envelope.financer', 'financer');
		$queryBuilder->leftJoin('object.incomes', 'incomes');
		$queryBuilder->groupBy('object.id');

		/*foreach (Income::getTypes() as $type) {
            $queryBuilder->addSelect('SUM(CASE WHEN incomes.type = \'' . $type . '\' THEN incomes.amount ELSE 0 END) AS HIDDEN amount' . ucfirst($type));
        }*/

		$sort = str_replace('virtual.', '', $sort);

		//$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
		if (!preg_match('/^computed\./', $sort)) {	// do not try to sort when the property belongs to incomes sub object
			$queryBuilder->addOrderBy($sort, $order);
		}
		$sort = str_replace('computed.', '', $sort);	// mandatory here: computed amounts must not be sorted (error)

		if (isset($data['full']) && $data['full']) {
			if (isset($data['filters']['project'])) {
				$data['filters']['name'] = [
					'name' => [
						'op' => 'cnt',
						'val' => $data['full'],
					],
				];
			} else {
				$queryBuilder->andWhere(
					$queryBuilder
						->expr()
						->orX(
							...[
								'object.name LIKE :f_full',
								'account.name LIKE :f_full',
								'envelope.name LIKE :f_full',
							]
						)
				);

				$queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
			}
		}

		if (isset($data['filters'])) {
			foreach ($data['filters'] as $property => $filter) {
				if ($property === '_client') {
                    $data['filterClient'] = $data['filters'][$property];
                    unset($data['filters'][$property]);
                    continue;
                }

				// We send virtual.amount{IncomeType}
				if (
					in_array(strtolower(str_replace('virtual.amount', '', $property)), Income::getTypes())
				) {
					$data['filters'][$property]['having'] = true;
					continue;
				}

				if ($property === 'incomes.date') {
					$data['filterTransactionsDate'] = $data['filters'][$property];
					unset($data['filters'][$property]);
					continue;
				}

				// Get sub postes
				if ($property === 'project.id' && $data['filters'][$property]['val']) {
					$project = $this->entityManager
						->getRepository('Project\Entity\Project')
						->find($data['filters'][$property]['val']);
					$hierarchy = $project->getHierarchySub();
					$data['filters'][$property]['val'] = [];
					foreach ($hierarchy as $child) {
						$data['filters'][$property]['val'][] = $child->getId();
					}
					$data['filters'][$property]['op'] = 'in';

					if ($hierarchy->count() > 0) {
						$queryBuilder->andWhere(
							$queryBuilder
								->expr()
								->orX(
									'project.id = ' . $project->getId(),
									'project.id IN (' .
										implode(',', $data['filters'][$property]['val']) .
										') AND object.parent IS NULL'
								)
						);
					} else {
						$queryBuilder->andWhere('project.id = ' . $project->getId());
					}

					unset($data['filters'][$property]);
				}
			}
		}
	}

	public function getList($callbacks = [], $master = false)
	{
		$callbacks[] = function (&$objects, $data) {
			if (isset($data['filterTransactionsDate'])) {
				if (
					isset($data['filterTransactionsDate']['op']) &&
					$data['filterTransactionsDate']['op'] == 'period'
				) {
					$start = isset($data['filterTransactionsDate']['val']['start'])
						? \DateTime::createFromFormat('d/m/Y', $data['filterTransactionsDate']['val']['start'])
						: false;
					$end = isset($data['filterTransactionsDate']['val']['end'])
						? \DateTime::createFromFormat('d/m/Y', $data['filterTransactionsDate']['val']['end'])
						: false;

					foreach ($objects as $object) {
						$transactions = $object->getIncomes();
						if ($transactions) {
							foreach ($transactions as $transaction) {
								$keep = true;
								if ($start && $transaction->getDate() < $start) {
									$keep = false;
								}

								if ($end && $transaction->getDate() > $end) {
									$keep = false;
								}

								if (!$keep) {
									$transactions->removeElement($transaction);
								}

								$object->setIncomes($transactions);
								$object->setFiltered(true);
							}
						}
					}
				}
			}
		};
		$callbacks[] = function (&$objects, $data) {
			if (isset($data['filterClient'])) {
				if (isset($data['filterClient']['op']) && isset($data['filterClient']['val'])) {
					$inArray = in_array($this->getClient()->getId(), $data['filterClient']['val']);
					if ($data['filterClient']['op'] === 'eq' && !$inArray) {
						$objects = [];
					} elseif ($data['filterClient']['op'] === 'neq' && $inArray) {
						$objects = [];
					}
				}
			}
		};

		$search = $this->params()->fromQuery('search', null);

		// clients filter on the postes request
		// to send on downstream requests
		$clientsFilter = null;

		if ($search && isset($search['data']) && isset($search['data']['filters'])) {
			$filters = $search['data']['filters'];

			// set client filter of the projects request
			if (array_key_exists('_client', $filters)) {
				$clientsFilter = $filters['_client'];
			}
		}

		$col = $this->params()->fromQuery('col');
		$incomeFilters = $this->params()->fromQuery('incomeFilters', null);

		if ($incomeFilters) {
			// if a client filter has been set on the postes request
			// add it to the descending budget request
			if(isset($clientsFilter)) {
				$incomeFilters['_client'] = $clientsFilter;
			}

			$callbacks[] = function (&$objects) use ($col, $incomeFilters) {
				$incomeCol = [];
				foreach ($col as $column) {
					if (preg_match('/^incomesArbo./', $column)) {
						$incomeCol[] = str_replace('incomesArbo.', '', $column);
					}
				}

				foreach ($objects as $object) {
					$incomes = $object->getIncomesArbo();

					$incomeFilters['id'] = [
						'op' => 'eq',
						'val' => [],
					];
					foreach ($incomes as $income) {
						$incomeFilters['id']['val'][] = $income->getId();
					}

					$request = $this->apiRequest('Budget\Controller\API\Income', Request::METHOD_GET, [
						'col' => $incomeCol,
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $incomeFilters,
							],
						],
					]);
					$incomes = $request->dispatch()->getVariable('rows');

					$incomesObjects = new ArrayCollection();
					foreach ($incomes as $income) {
						$incomesObjects->add(
							$this->entityManager()
								->getRepository('Budget\Entity\Income')
								->find($income['id'])
						);
					}

					$object->setIncomes($incomesObjects);
					$object->setFiltered(true);
				}
			};
		}

		$groupBy = $this->params()->fromQuery('groupBy', 'poste');

		if (
			$groupBy === 'date' || $groupBy === 'anneeExercice' ||
			$groupBy === 'mandat' || $groupBy === 'complement' || $groupBy === 'engagement' ||
			$groupBy === 'evolution' || $groupBy === 'bordereau'
		) {

			$callbacks[] = function (&$objects) use ($groupBy) {
				$grouped = [];

				foreach ($objects as $poste) {
					foreach ($poste->getIncomesArbo() as $income) {
						$key = '';

						switch ($groupBy) {
							case 'date':
								$key = $income->getDate()->format('Y');
								break;
							case 'anneeExercice':
								$key = $income->getAnneeExercice();
								break;
							case 'mandat':
								$key = $income->getMandat();
								break;
							case 'complement':
								$key = $income->getComplement();
								break;
							case 'engagement':
								$key = $income->getEngagement();
								break;
							case 'dateFacture':
								$key = $income->getDateFacture()->format('Y');;
								break;
							case 'evolution':
								$key = $income->getEvolution();
								break;
							case 'bordereau':
								$key = $income->getBordereau();
								break;
						}

						if (!isset($grouped[$key])) {
							$grouped[$key] = new PosteIncome();
							$grouped[$key]->setName($key);
							$grouped[$key]->setProject($poste->getProject());
							$grouped[$key]->setAmount(0);
							$grouped[$key]->setIncomesArbo(new ArrayCollection());
							$grouped[$key]->setFiltered(true);
						}

						$grouped[$key]->getIncomesArbo()->add($income);
					}
				}

				$objects = $grouped;
			};
		}

		if ($groupBy === 'gbcp') {
			$callbacks[] = function (&$objects) {
				$grouped = [];
				foreach ($objects as $poste) {
					if (!$poste->getNature() || !$poste->getOrganization() || !$poste->getDestination()) {
						continue;
					}

					$key =
						$poste->getProject()->getId() .
						'-' .
						$poste->getNature()->getId() .
						'-' .
						$poste->getOrganization()->getId() .
						'-' .
						$poste->getDestination()->getId();

					if (!isset($grouped[$key])) {
						$grouped[$key] = new PosteIncome();
						$grouped[$key]->setNature($poste->getNature());
						$grouped[$key]->setOrganization($poste->getOrganization());
						$grouped[$key]->setDestination($poste->getDestination());
						$grouped[$key]->setAmount(0);
						$grouped[$key]->setIncomesArbo(new ArrayCollection());
						$grouped[$key]->setFiltered(true);
					}

					foreach ($poste->getIncomesArbo() as $income) {
						$grouped[$key]->setAmount($grouped[$key]->getAmount() + $income->getAmount());
						$grouped[$key]->setAmountHT($grouped[$key]->getAmountHT() + $income->getAmountHT());
						$grouped[$key]->getIncomesArbo()->add($income);
					}
				}

				$objects = $grouped;
			};
		}

		$res = parent::getList($callbacks, $master);

		// incomes sorting
		$res->setVariable(
			'rows',
			PosteSorter::sortPostes(
				$res->getVariable('rows'),
				$this->params()->fromQuery('search'),
				'incomes'
			)
		);

		if ($groupBy === 'gbcp') {
			$rows = $res->getVariable('rows');
			foreach ($rows as $i => $row) {
				$rows[$i]['project']['name'] = '';
				$rows[$i]['_rights']['update'] = false;
				$rows[$i]['_rights']['delete'] = false;
				$rows[$i]['_rights']['read'] = false;
			}

			$res->setVariable('rows', $rows);
		}

		return $res;
	}
}
