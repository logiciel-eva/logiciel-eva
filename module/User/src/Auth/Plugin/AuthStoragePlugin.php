<?php

namespace User\Auth\Plugin;

use User\Auth\AuthStorage;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use Laminas\Mvc\MvcEvent;

/**
 * Class AuthStoragePlugin
 *
 * Access auth storage from controller.
 *
 * @package User\Auth\Plugin
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AuthStoragePlugin extends AbstractPlugin
{
    /**
     * @var AuthStorage
     */
    protected $authStorage;

    /**
     * @param AuthStorage $authStorage
     */
    public function __construct(AuthStorage $authStorage)
    {
        $this->authStorage = $authStorage;
    }

    /**
     * @return AuthStorage
     */
    public function __invoke()
    {
        return $this->authStorage;
    }

    /**
     * @param MvcEvent $event
     */
    public function preDispatch(MvcEvent $event)
    {
        $response   = $event->getResponse();

        $routeMatch = $event->getRouteMatch();
        $controller = $routeMatch->getParam('controller');
        $action     = $routeMatch->getParam('action');

        // Idle is permitted for 4 hours
        $lastActivity = $this->authStorage->getActivity();
        if ($lastActivity !== null && time() - $lastActivity >= (3600 * 4)) {
            $this->authStorage->clear();
        } else {
            $this->authStorage->updateActivity();
        }

        if (!$this->authStorage->isAllowed($controller, $action)) {
            $router = $event->getApplication()->getServiceManager()->get('Router');
            $url = $router->assemble([], [
                'name' => $this->authStorage->isAuthenticated() ? 'root' : 'user/auth/login'
            ]);

            $requestUri = $event->getRequest()->getRequestUri();
            if ($requestUri !== '/') {
                $url .= '?url=' . urlencode($requestUri);
            }

            $response->getHeaders()->addHeaders(['Location' => $url]);
            $response->setStatusCode(302);
            $response->sendHeaders();

            return $response;
        }
    }
}
