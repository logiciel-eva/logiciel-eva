<?php

namespace Budget\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class BudgetStatusController extends BasicRestController
{
    protected $entityClass = 'Budget\Entity\BudgetStatus';
    protected $repository  = 'Budget\Entity\BudgetStatus';
    protected $entityChain = 'budget:e:budgetStatus';

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
