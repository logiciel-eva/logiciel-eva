<?php

namespace Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;

/**
 * @ORM\Entity(repositoryClass="Project\Repository\ProjectUpdateRepository")
 * @ORM\Table(name="project_update")
 */
class ProjectUpdate
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="updates")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $project;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User\Entity\User", inversedBy="projectUpdates")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;


    /**
	 * Date
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $date;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     *
     * @return self;
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }


    /**
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param \DateTime $date
     *
     * @return self
	 */
	public function setDate($date)
	{
        $this->date = $date;

        return $this;
	}
}
