<?php

return [
    'icons' => [
        'convention' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-file-text-o',
        ],
        'subvention' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-paw',
        ],
    ]
];
