(function(app) {

    app.directive('campainSelect', [
        'campainService', 'selectFactoryService',
        function campainSelect(campainService, selectFactoryService) {
            var optgroups = [
                {label: 'A', value: 'fixed'},
                {label: 'B', value: 'free'}
            ];

            return selectFactoryService.create({
                service : {
                    object: campainService,
                    apiParams: {
                        col: ['id', 'name']
                    }
                },
                selectize: {
                    optgroups: optgroups,
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name'],
                    onLoad: function(scope, data) {
                        var t =  {id: 55555, name: 'intest'};
                        data.push(t);
                    },
                    optgroupField: 'type'
                }
            });
        }
    ])

})(window.EVA.app);
