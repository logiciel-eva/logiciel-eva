<?php

namespace Core\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateProxies extends AbstractCommand
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[START] Generating admin proxies...');

        exec(getcwd() . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'doctrine-module orm:generate:proxies');

        $output->writeln('Generating clients proxies...');

        $em                   = $this->getEmDefault();
        $entityManagerFactory = $this->getEmFactory();
        $exitCode = Command::SUCCESS;

        $clients = $em->getRepository('Bootstrap\Entity\Client')->findAll();
        foreach ($clients as $client) {
            $clientEm   = $entityManagerFactory->getEntityManager($client);

            try {
                $metadatas = $clientEm->getMetadataFactory()->getAllMetadata();
                $destPath  = $em->getConfiguration()->getProxyDir();

                $clientEm->getProxyFactory()->generateProxyClasses($metadatas, $destPath);
            } catch (\Exception $e) {
                echo $e->getMessage();
                $output->writeln('[ERROR] - ' . $e->getMessage());
                $exitCode = Command::FAILURE;
            }
        }

        $output->writeln('[END] Generating admin proxies...');
        return $exitCode;
    }
}
