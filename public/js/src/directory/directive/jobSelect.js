(function(app) {

    app.directive('jobSelect', [
        'jobService', 'selectFactoryService',
        function jobSelect(jobService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: jobService,
                    apiParams: {
                        col: ['id', 'name'],
                        search : {
                            data : {
                                sort: 'name',
                                order: 'asc'
                            }
                        }
                    },
                    tree: true,
                    nnTree: true
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);

