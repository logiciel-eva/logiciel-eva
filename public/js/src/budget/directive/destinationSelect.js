(function(app) {
    app.directive('destinationSelect', [
        'destinationService',
        'selectFactoryService',
        function(destinationService, selectFactoryService) {
            return selectFactoryService.create({
                service: {
                    object: destinationService,
                    apiParams: {
                        col: ['id', 'code', 'name', 'parent.id'],
                    },
                    full: true,
                    tree: true,
                    treeOrder: 'code',
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['code', 'name'],
                    render: {
                        option: (data, escape) => `
                            <div class="option">
                                ${escape(data.code)} - ${escape(data.name)}
                            </div>
                        `,
                        item: (data, escape) => `
                            <div class="option">
                                ${escape(data.code)} - ${escape(data.name)}
                            </div>
                        `,
                    },
                },
            });
        },
    ]);
})(window.EVA.app);
