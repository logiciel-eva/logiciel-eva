<?php

namespace Budget\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class ExpenseController extends BasicRestController
{
    protected $entityClass = 'Budget\Entity\Expense';
    protected $repository  = 'Budget\Entity\Expense';
    protected $entityChain = 'budget:e:expense';

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->join('object.poste', 'poste');

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }

        if (isset($data['filters'])) {
            if(array_key_exists('_client', $data['filters'])) {
                $data['filterClient'] = $data['filters']['_client'];
                unset($data['filters']['_client']);
            }
        }

    }

    public function getList($callbacks = [], $master = false)
	{
		$callbacks[] = function (&$objects, $data) {
			if (isset($data['filterClient'])) {
				if (isset($data['filterClient']['op']) && isset($data['filterClient']['val'])) {
					$inArray = in_array($this->getClient()->getId(), $data['filterClient']['val']);
					if ($data['filterClient']['op'] === 'eq' && !$inArray) {
						$objects = [];
					} elseif ($data['filterClient']['op'] === 'neq' && $inArray) {
						$objects = [];
					}
				}
			}
		};
        return parent::getList($callbacks, $master);
    }

}
