<?php

$users = [
    'icon' => 'users',
    'title' => 'user_module_title',
    'right' => 'user:e:user:u',
    'href' => 'user',
    'priority' => 4,
];

$role = [
    'icon' => 'role',
    'title' => 'role_module_title',
    'right' => 'user:e:role:r',
    'href' => 'role',
    'priority' => 3,
];

$service = [
    'icon' => 'service',
    'title' => 'service_module_title',
    'right' => 'user:e:service:u',
    'href' => 'service',
    'priority' => 3,
];

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'users' => $users,
                    'role' => $role,
                ],
            ],
        ],
        'client-menu-parc' => [
            'administration' => [
                'children' => [
                    'profils' => [
                        'icon' => 'profile',
                        'title' => 'menu_profils_title',
                        'priority' => 9999,
                        'children' => [
                            'users' => $users,
                            'role' => $role,
                        ],
                    ],
                ],
            ],
        ],
    ],
];
