<?php

use Bootstrap\Entity\Client;
use Core\Controller\Plugin\ApiRequestPlugin;
use Core\View\Helper\Translate;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Keyword\Entity\Group;
use Keyword\Entity\Keyword;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Project\Entity\Project;
use Time\Entity\AbsenceType;
use User\Entity\User;
use Laminas\Http\Request;

class Export
{
    const ABSENCE_WEEKEND  = 'WE';
    const ABSENCE_HOLIDAYS = 'F';

    const STYLE_TABLE        = [
        'borders' => [
            'outline' => [
                'borderStyle' => Style\Border::BORDER_MEDIUM,
                'color'       => ['rgb' => '000000'],
            ],
            'inside'  => [
                'borderStyle' => Style\Border::BORDER_THIN,
                'color'       => ['rgb' => '000000'],
            ],
        ],
    ];
    const STYLE_SIGNATURE    = [
        'borders' => [
            'outline' => [
                'borderStyle' => Style\Border::BORDER_MEDIUM,
                'color'       => ['rgb' => '000000'],
            ],
        ],
    ];
    const STYLE_ALIGN_LEFT   = [
        'alignment' => [
            'horizontal' => Style\Alignment::HORIZONTAL_LEFT,
        ],
    ];
    const STYLE_ALIGN_CENTER = [
        'alignment' => [
            'horizontal' => Style\Alignment::HORIZONTAL_CENTER,
            'vertical'   => Style\Alignment::VERTICAL_CENTER,
        ],
    ];
    const STYLE_ALIGN_RIGHT  = [
        'alignment' => [
            'horizontal' => Style\Alignment::HORIZONTAL_RIGHT,
        ],
    ];
    const STYLE_BOLD         = [
        'font' => [
            'bold' => true,
        ],
    ];

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translate
     */
    protected $translator;

    /**
     * @var ApiRequestPlugin
     */
    protected $apiRequest;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $reference;

    /**
     * @var string
     */
    protected $timeType;

    /**
     * @var float
     */
    protected $dayHours;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var DateTime
     */
    protected $start;

    /**
     * @var DateTime
     */
    protected $end;

    /**
     * @var array
     */
    protected $otherLifeHours;

    /**
     * @var Group
     */
    protected $group;

    /**
     * @var array
     */
    protected $projectIds;

    /**
     * @var DateTime
     */
    protected $currentStart;

    /**
     * @var DateTime
     */
    protected $currentEnd;

    /**
     * @var int
     */
    protected $currentNumberOfDays;

    /**
     * Export constructor.
     *
     * @param EntityManager    $em
     * @param Translate        $translator
     * @param ApiRequestPlugin $apiRequest
     * @param Client           $client
     */
    public function __construct(EntityManager $em, Translate $translator, ApiRequestPlugin $apiRequest, Client $client)
    {
        $this->em         = $em;
        $this->translator = $translator;
        $this->apiRequest = $apiRequest;
        $this->client     = $client;
    }

    public function doExport($params, $data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);
        setlocale(LC_TIME, 'fr_FR', 'French_France', 'French');

        $this->reference      = $params['reference'];
        $this->timeType       = $params['timeType'];
        $this->dayHours       = (float)$params['dayHours'];
        $this->user           = $this->em->getRepository('User\Entity\User')->find($params['user']['id']);
        $this->start          = DateTime::createFromFormat('d/m/Y', $params['start'])->setTime(0, 0);
        $this->end            = DateTime::createFromFormat('d/m/Y', $params['end'])->setTime(0, 0);
        $this->group          = $this->getGroup($data);
        $this->otherLifeHours = $this->getOtherLifeHours(isset($params['otherLifeGroups']) ? $params['otherLifeGroups'] : null);

        $sheets = [];

        $startYear = $this->start->format('Y');
        $endYear   = $this->end->format('Y');

        $yearReviewsData = [];
        for ($currentYear = $startYear; $currentYear <= $endYear; $currentYear++) {
            $startMonth = '1';
            $endMonth   = '12';
            if ($currentYear == $startYear) {
                $startMonth = $this->start->format('n');
            }
            if ($currentYear == $endYear) {
                $endMonth = $this->end->format('n');
            }

            $yearReviewsData[$currentYear] = [];
            for ($currentMonth = $startMonth; $currentMonth <= $endMonth; $currentMonth++) {
                $currentMonthYear = DateTime::createFromFormat(
                    'j/n/Y',
                    1 . '/' . $currentMonth . '/' . $currentYear
                )->setTime(0, 0);

                $this->currentStart = clone $currentMonthYear;
                if ($currentMonth == $startMonth && $currentYear == $startYear) {
                    $this->currentStart = $this->start;
                }
                if ($currentMonth == $endMonth && $currentYear == $endYear) {
                    $this->currentEnd = $this->end;
                } else {
                    $daysToAdd        = $currentMonthYear->format('t') - 1;
                    $this->currentEnd = $currentMonthYear->add(DateInterval::createFromDateString($daysToAdd . ' days'));
                }

                $this->projectIds          = [];
                $this->currentNumberOfDays = $this->currentStart->diff($this->currentEnd)->days + 1;

                $lines = [$this->getDateLine()];

                $this->getWorkHours($lines, $data);
                $this->getAbsences($lines);
                $this->getDaysOff($lines);

                $this->removeZeros($lines);

                $month     = utf8_encode(ucfirst(strftime('%B', $currentMonthYear->getTimestamp())));
                $year      = $currentYear;
                $sheetName = $month . ' ' . $year;

                $header = $this->getHeader($year, $month);
                $footer = $this->getFooter();

                $sheets[$sheetName] = [$header, $lines, $footer];

                $yearReviewsData[$currentYear][$currentMonth] = [];
                foreach ($lines as $key => $line) {
                    // Remove the string Total from the array
                    if ($key !== 0) {
                        $yearReviewsData[$currentYear][$currentMonth][] = $month . ' ' . $year;
                    }
                }
            }
        }

        $this->getYearReviews($sheets, $yearReviewsData);

        $this->download($sheets);
    }

    protected function getDateLine()
    {
        $line  = ['Date'];
        $start = clone $this->currentStart;

        for ($cpt = 0; $cpt < $this->currentNumberOfDays; $cpt++) {
            $line[] = $start->format('j');
            $start->add(DateInterval::createFromDateString('1 day'));
        }

        return $line;
    }

    protected function getGroup($tree)
    {
        $keywordId = reset($tree)['id'];

        /** @var Keyword $keyword */
        $keyword = $this->em->getRepository('Keyword\Entity\Keyword')->find($keywordId);

        return $keyword->getGroup();
    }

    protected function getWorkHours(&$lines, $tree)
    {
        foreach ($tree as $node) {
            $subLines = $this->getLinesRecursively($node, 0);

            $lines = array_merge($lines, $subLines);
        }
    }

    protected function getAbsences(&$lines)
    {
        $timesheets = $this->getTimesheetRequest(
            ['id', 'start', 'absenceType.initialLetters'],
            $this->projectIds,
            ['absence']
        )->dispatch()->getVariable('rows');

        foreach ($timesheets as $timesheet) {
            $start = DateTime::createFromFormat('d/m/Y H:i', $timesheet['start'])->setTime(0, 0);
            $diff  = (float)$this->currentStart->diff($start)->format('%d');

            for ($i = 1; $i < count($lines); $i++) {
                $lines[$i][$diff + 1] = $timesheet['absenceType']['initialLetters'];
            }
        }
    }

    protected function getDaysOff(&$lines)
    {
        $holidays = $this->getHolidays();

        foreach ($holidays as $holiday) {
            if (($holiday > $this->currentStart) && ($holiday < $this->currentEnd)) {
                $diff = (float)$this->currentStart->diff($holiday)->format('%d');

                for ($i = 1; $i < count($lines); $i++) {
                    $lines[$i][$diff + 1] = self::ABSENCE_HOLIDAYS;
                }
            }
        }

        $day = clone $this->currentStart;
        for ($cpt = 0; $cpt < $this->currentNumberOfDays; $cpt++) {
            if (in_array($day->format('l'), ['Saturday', 'Sunday'])) {
                $diff = (float)$this->currentStart->diff($day)->format('%d');

                for ($i = 1; $i < count($lines); $i++) {
                    $lines[$i][$diff + 1] = self::ABSENCE_WEEKEND;
                }
            }

            $day->add(DateInterval::createFromDateString('1 day'));
        }
    }

    protected function removeZeros(&$lines)
    {
        foreach ($lines as &$line) {
            foreach ($line as &$value) {
                if ($value === 0) {
                    $value = '';
                }
            }
        }
    }

    protected function getHeader($year, $month = null)
    {
        $header = [
            ['Référence du projet', $this->reference],
            ['Nom du bénéficiaire / partenaire', $this->client->getName()],
            ['Prénom, nom du salarié', $this->user->getFirstName() . ' ' . ucwords($this->user->getLastName())],
            ['Plein temps ou temps partiel ?', $this->timeType],
            ['Année', $year],
        ];

        if ($month) {
            $header[] = ['Mois', $month];
        }

        return $header;
    }

    protected function getFooter($review = false)
    {
        if (!$review) {
            $absences = [['Absences', '']];

            $absenceTypes = $this->getAbsenceTypes();
            foreach ($absenceTypes as $absenceTypeName => $absenceTypeInitialLetters) {
                $absences[] = [$absenceTypeName, $absenceTypeInitialLetters];
            }
        }

        $syntheses            = [['Synthèse ' . ($review ? 'annuelle' : 'mensuelle'), '']];
        $daysLife             = ['Jours ' . $this->group->getName(), ''];
        $daysOthersLife       = ['Jours autres LIFE', ''];
        $daysOthersActivities = ['Jours autres activités', ''];
        $daysTotal            = ['Total jours', ''];
        if (!$review) {
            for ($i = 0; $i < 7; $i++) {
                $syntheses[0][]         = '';
                $daysLife[]             = '';
                $daysOthersLife[]       = '';
                $daysOthersActivities[] = '';
                $daysTotal[]            = '';
            }
        }
        $syntheses[] = $daysLife;
        $syntheses[] = $daysOthersLife;
        $syntheses[] = $daysOthersActivities;
        $syntheses[] = $daysTotal;

        $footer = [];
        if (isset($absences)) {
            foreach ($absences as $absence) {
                $footer[] = $absence;
            }
            // Adjust the height of the array to the maximum.
            if (count($syntheses) > count($absences)) {
                foreach ($syntheses as $key => $synthesis) {
                    if (!isset($footer[$key])) {
                        $footer[$key] = ['', ''];
                    }
                }
            }
        }

        foreach ($syntheses as $i => $synthesis) {
            if (isset($footer[$i])) {
                $footer[$i] = array_merge($footer[$i], [''], $synthesis);
            } else {
                // Review case, there is no absences.
                $footer[$i] = $synthesis;
            }
        }

        $responsable = ['Date et signature du responsable hiérarchique'];
        $salarie     = ['Date et signature du salarié'];
        $blanksToAdd = $review ? 2 : 8;
        for ($i = 0; $i < $blanksToAdd; $i++) {
            $responsable[] = '';
            $salarie[]     = '';
        }

        $footer[0] = array_merge($footer[0], [''], $responsable);
        $footer[0] = array_merge($footer[0], [''], $salarie);

        return $footer;
    }

    protected function getYearReviews(&$sheets, $yearReviewsData)
    {
        $yearReviews = [];
        foreach ($yearReviewsData as $year => $months) {
            $yearReviews['Bilan ' . $year]   = [];
            $yearReviews['Bilan ' . $year][] = $this->getHeader($year);

            foreach (reset($sheets)[1] as $key => $values) {
                // Remove the string Date from the array.
                if ($key === 0) {
                    $yearReviews['Bilan ' . $year][1][] = ['Mois'];
                } else {
                    $yearReviews['Bilan ' . $year][1][] = [$values[0]];
                }
            }

            foreach ($months as $month => $totals) {
                $month = DateTime::createFromFormat(
                    'j/n/Y',
                    1 . '/' . $month . '/' . $year
                )->setTime(0, 0);
                $month = utf8_encode(ucfirst(strftime('%B', $month->getTimestamp())));

                $yearReviews['Bilan ' . $year][1][0][] = $month;

                foreach ($totals as $i => $total) {
                    $yearReviews['Bilan ' . $year][1][$i + 1][] = $total;
                }
            }
        }

        foreach ($yearReviews as $key => $yearReview) {
            $yearReviews[$key][] = $this->getFooter(true);
        }

        $sheets = array_merge($sheets, $yearReviews);
    }

    protected function getLinesRecursively($node, $level)
    {
        $line = array_merge([''], array_fill(0, $this->currentNumberOfDays, 0));

        if ($node['__type'] === 'keyword') {
            $line[0] = str_repeat('    ', $level) . $node['name'];
        } else {
            $this->getTimesheetsHours($line, $node['id']);
            $this->projectIds[] = $node['id'];
        }

        $lines = [];
        foreach ($node['children'] as $child) {
            $subLines = $this->getLinesRecursively($child, $level + 1);

            foreach ($subLines as $i => $subLine) {
                // Sum only the project lines.
                if ($subLine[0] === '') {
                    foreach ($subLine as $col => $value) {
                        if (!is_string($value) && $value !== 0) {
                            $line[$col] += $value;
                        }
                    }
                }

                // Remove project lines.
                if ($subLine[0] === '') {
                    unset($subLines[$i]);
                }
            }
            $lines = array_merge($lines, $subLines);
        }

        return array_merge([$line], $lines);
    }

    protected function getTimesheetsHours(&$line, $projectId)
    {
        $timesheets = $this->getTimesheetRequest(
            ['id', 'hours', 'start'],
            $projectId,
            ['done']
        )->dispatch()->getVariable('rows');

        foreach ($timesheets as $timesheet) {
            $start = \DateTime::createFromFormat('d/m/Y H:i', $timesheet['start'])->setTime(0, 0);
            $diff  = (float)$this->currentStart->diff($start)->format('%d');

            $line[$diff + 1] += $timesheet['hours'];
        }
    }

    protected function getTimesheetRequest($col, $project, $type)
    {
        return $this->apiRequest->__invoke(
            'Time\Controller\API\Timesheet',
            Request::METHOD_GET,
            [
                'col'    => $col,
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'name',
                        'order'   => 'asc',
                        'filters' => [
                            'user'    => [
                                'op'  => 'eq',
                                'val' => $this->user->getId(),
                            ],
                            'project' => [
                                'op'  => 'eq',
                                'val' => $project,
                            ],
                            'type'    => [
                                'op'  => 'eq',
                                'val' => $type,
                            ],
                            'start'   => [
                                'op'  => 'period',
                                'val' => [
                                    'start' => $this->currentStart->format('d/m/Y'),
                                    'end'   => $this->currentEnd->format('d/m/Y'),
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );
    }

    protected function download($array)
    {
        try {
            foreach ($array as $i => $tables) {
                foreach ($tables as $j => $lines) {
                    foreach ($lines as $k => $row) {
                        foreach ($row as $l => $value) {
                            if (is_numeric($value) && $value == 0) {
                                $array[$i][$j][$k][$l] = '0';
                            }
                        }
                    }
                }
            }

            $totalLinesColBySheet = [];
            $reviewSheetNames     = [];

            $spreadsheet = new Spreadsheet();

            foreach ($array as $worksheetName => $tables) {
                $worksheet = $spreadsheet->createSheet();
                $worksheet->setTitle($worksheetName);

                $isReview = preg_match('/^Bilan/', $worksheetName);
                if ($isReview) {
                    $reviewSheetNames[] = $worksheetName;
                }

                foreach ($tables as $key => $lines) {
                    switch ($key) {
                        // Header
                        case 0:
                            $worksheet->fromArray($lines, null, 'A1');

                            $logo1 = new Drawing();
                            $logo1->setName('Natura 2000');
                            $logo1->setPath(__DIR__ . '/img/natura2000.jpg');
                            $logo1->setWidthAndHeight(105, 105);

                            $logo2 = new Drawing();
                            $logo2->setName('LIFE');
                            $logo2->setPath(__DIR__ . '/img/life.jpg');
                            $logo2->setWidthAndHeight(105, 105);

                            if ($isReview) {
                                $worksheet
                                    ->getStyle('A1:E5')
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_ALIGN_LEFT
                                    ));
                                $worksheet
                                    ->getStyle('A1:E1')
                                    ->applyFromArray(self::STYLE_BOLD);
                                $worksheet->mergeCells('B1:E1');
                                $worksheet->mergeCells('B2:E2');
                                $worksheet->mergeCells('B3:E3');
                                $worksheet->mergeCells('B4:E4');
                                $worksheet->mergeCells('B5:E5');

                                $logo1->setCoordinates('G2');
                                $logo2->setCoordinates('I2');
                            } else {
                                $worksheet
                                    ->getStyle('A1:M6')
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_ALIGN_LEFT
                                    ));
                                $worksheet
                                    ->getStyle('A1:M1')
                                    ->applyFromArray(self::STYLE_BOLD);
                                $worksheet->mergeCells('B1:M1');
                                $worksheet->mergeCells('B2:M2');
                                $worksheet->mergeCells('B3:M3');
                                $worksheet->mergeCells('B4:M4');
                                $worksheet->mergeCells('B5:M5');
                                $worksheet->mergeCells('B6:M6');

                                $worksheet->setCellValue('Q1', 'Feuille de temps');
                                $worksheet->mergeCells('Q1:Y1');
                                $worksheet
                                    ->getStyle('Q1')
                                    ->applyFromArray(array_merge(
                                        self::STYLE_ALIGN_CENTER,
                                        self::STYLE_BOLD
                                    ));

                                $logo1->setCoordinates('Q3');
                                $logo2->setCoordinates('W3');
                            }

                            $logo1->setWorksheet($worksheet);
                            $logo2->setWorksheet($worksheet);
                            break;
                        // Body
                        case 1:
                            // Link reviews sheets to month values
                            if ($isReview) {
                                // Variables used to calculate the values of the total lines.
                                $linesCopy = $lines[1];
                                $numberProjects = count($lines);

                                for ($i = 1; $i < count($lines); $i++) {
                                    for ($j = 1; $j < count($lines[$i]); $j++) {
                                        $lines[$i][$j] = '=\'' . $lines[$i][$j] . '\'!' . $totalLinesColBySheet[$lines[$i][$j]] . ($i + 8);
                                    }
                                }
                            }

                            // Adding Total Cols
                            $rowToBegin = (int)$worksheet->getHighestDataRow() + 2;

                            $rowToSumBegin = $rowToBegin + 1;
                            $rowToSumEnd   = $rowToSumBegin + count($lines) - 1;
                            end($lines[0]);
                            $lastKey = key($lines[0]);
                            reset($lines[0]);
                            $colToEnd  = Coordinate::stringFromColumnIndex($lastKey + 1);
                            $totalCols = ['Total'];
                            for ($i = $rowToSumBegin; $i < $rowToSumEnd; $i++) {
                                $totalCols[] = '=SUM(B' . $i . ':' . $colToEnd . $i . ')';
                            }

                            foreach ($totalCols as $k => $totalCol) {
                                $lines[$k][] = '';
                                $lines[$k][] = $totalCol;
                            }

                            $worksheet->fromArray($lines, null, 'A' . $rowToBegin);

                            // Adding Total Lines
                            $rowToSumEnd    = (int)$worksheet->getHighestDataRow();
                            $colToEnd       = Coordinate::stringFromColumnIndex(count($lines[0]) - 2);
                            $columnIterator = $worksheet->getColumnIterator('B', $colToEnd);

                            $hoursLife             = ['Total heures ' . $this->group->getName()];
                            $hoursOthersLife       = ['Heures autres projets LIFE'];
                            $hoursOthersActivities = ['Heures autres activités'];
                            $hoursTotal            = ['Heures totales'];
                            $daysTotal             = ['Nombre de jours'];

                            foreach ($columnIterator as $k => $col) {
                                $index = Coordinate::columnIndexFromString($k) - 1;
                                if (is_string($lines[1][$index])
                                    && $lines[1][$index] !== ''
                                    && !preg_match('/^=/', $lines[1][$index])
                                ) {
                                    $hoursLife[]             = $lines[1][$index];
                                    $hoursOthersLife[]       = $lines[1][$index];
                                    $hoursOthersActivities[] = $lines[1][$index];
                                    $hoursTotal[]            = $lines[1][$index];
                                    $daysTotal[]             = $lines[1][$index];
                                } else if (!$isReview) {
                                    $date                    = $index . ' ' . $worksheetName;
                                    $hoursLife[]             = '=SUM(' . $k . $rowToSumBegin . ':' . $k . $rowToSumEnd . ')';
                                    $hoursOthersLife[]       = isset($this->otherLifeHours[$date])
                                        ? $this->otherLifeHours[$date]
                                        : '';
                                    $hoursOthersActivities[] = '';
                                    $hoursTotal[]            = '=SUM(' . $k . ($rowToSumEnd + 1) . ':' . $k . ($rowToSumEnd + 3) . ')';
                                    $daysTotal[]             = '=' . $k . ($rowToSumEnd + 4) . '/' . $this->dayHours;
                                } else {
                                    $hoursLife[]             = '=\'' . $linesCopy[$index] . '\'!' . $totalLinesColBySheet[$linesCopy[$index]] . ($numberProjects + 8);
                                    $hoursOthersLife[]       = '=\'' . $linesCopy[$index] . '\'!' . $totalLinesColBySheet[$linesCopy[$index]] . ($numberProjects + 9);
                                    $hoursOthersActivities[] = '=\'' . $linesCopy[$index] . '\'!' . $totalLinesColBySheet[$linesCopy[$index]] . ($numberProjects + 10);
                                    $hoursTotal[]            = '=\'' . $linesCopy[$index] . '\'!' . $totalLinesColBySheet[$linesCopy[$index]] . ($numberProjects + 11);
                                    $daysTotal[]             = '=\'' . $linesCopy[$index] . '\'!' . $totalLinesColBySheet[$linesCopy[$index]] . ($numberProjects + 12);
                                }
                            }

                            $hoursLife[]             = '';
                            $hoursOthersLife[]       = '';
                            $hoursOthersActivities[] = '';
                            $hoursTotal[]            = '';
                            $daysTotal[]             = '';

                            $hoursLife[]             = '=SUM(B' . ($rowToSumEnd + 1) . ':' . $colToEnd . ($rowToSumEnd + 1) . ')';
                            $hoursOthersLife[]       = '=SUM(B' . ($rowToSumEnd + 2) . ':' . $colToEnd . ($rowToSumEnd + 2) . ')';
                            $hoursOthersActivities[] = '=SUM(B' . ($rowToSumEnd + 3) . ':' . $colToEnd . ($rowToSumEnd + 3) . ')';
                            $hoursTotal[]            = '=SUM(B' . ($rowToSumEnd + 4) . ':' . $colToEnd . ($rowToSumEnd + 4) . ')';
                            $daysTotal[]             = '=SUM(B' . ($rowToSumEnd + 5) . ':' . $colToEnd . ($rowToSumEnd + 5) . ')';

                            $totalLinesColBySheet[$worksheetName] = Coordinate::stringFromColumnIndex(count($hoursLife));

                            $totals = [$hoursLife, $hoursOthersLife, $hoursOthersActivities, $hoursTotal, $daysTotal];

                            $worksheet->fromArray($totals, null, 'A' . ((int)$worksheet->getHighestDataRow() + 1));

                            // Keywords Names
                            $worksheet
                                ->getStyle('A' . $rowToBegin . ':A' . $worksheet->getHighestDataRow())
                                ->applyFromArray(array_merge(
                                    self::STYLE_TABLE,
                                    self::STYLE_ALIGN_LEFT
                                ));

                            // Data
                            $highestCol = count($lines[0]);
                            $col        = Coordinate::stringFromColumnIndex($highestCol - 2);
                            $worksheet
                                ->getStyle('B' . $rowToBegin . ':' . $col . $worksheet->getHighestDataRow())
                                ->applyFromArray(array_merge(
                                    self::STYLE_TABLE,
                                    self::STYLE_ALIGN_RIGHT
                                ));

                            // Bold
                            $worksheet
                                ->getStyle('A' . $rowToBegin . ':' . $col . $rowToBegin)
                                ->applyFromArray(self::STYLE_BOLD);
                            $lastRow = $worksheet->getHighestDataRow();
                            for ($i = $lastRow; $i > $lastRow - 5; $i--) {
                                $worksheet
                                    ->getStyle('A' . $i . ':' . $col . $i)
                                    ->applyFromArray(self::STYLE_BOLD);
                            }

                            // Total col
                            $col            = Coordinate::stringFromColumnIndex($highestCol);
                            $columnIterator = $worksheet->getColumnIterator($col);
                            $columnIterator->next();
                            $worksheet
                                ->getStyle($col . $rowToBegin . ':' . ($isReview ? $col : $columnIterator->key()) . $worksheet->getHighestDataRow())
                                ->applyFromArray(array_merge(
                                    self::STYLE_TABLE,
                                    self::STYLE_ALIGN_RIGHT,
                                    self::STYLE_BOLD
                                ));

                            if (!$isReview) {
                                for ($i = $rowToBegin; $i <= $worksheet->getHighestDataRow(); $i++) {
                                    $columnIterator = $worksheet->getColumnIterator($col);
                                    $columnIterator->next();
                                    $worksheet->mergeCells($col . $i . ':' . $columnIterator->key() . $i);
                                }
                            }
                            break;
                        // Footer
                        case 2:
                            $rowToBegin = (int)$worksheet->getHighestDataRow() + 2;
                            $worksheet->fromArray($lines, null, 'A' . $rowToBegin);

                            if ($isReview) {
                                // Syntheses
                                $rowToEnd = $rowToBegin + 4;
                                $worksheet->setCellValue('B' . ($rowToBegin + 1), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 6) . '/' . $this->dayHours);
                                $worksheet->setCellValue('B' . ($rowToBegin + 2), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 5) . '/' . $this->dayHours);
                                $worksheet->setCellValue('B' . ($rowToBegin + 3), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 4) . '/' . $this->dayHours);
                                $worksheet->setCellValue('B' . ($rowToBegin + 4), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 2));

                                $worksheet
                                    ->getStyle('A' . $rowToBegin . ':B' . $rowToEnd)
                                    ->applyFromArray(self::STYLE_TABLE);

                                $worksheet->mergeCells('A' . $rowToBegin . ':B' . $rowToBegin);
                                $worksheet
                                    ->getStyle('A' . $rowToBegin . ':B' . $rowToBegin)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_ALIGN_CENTER,
                                        self::STYLE_BOLD
                                    ));

                                for ($i = $rowToBegin + 1; $i <= $rowToEnd; $i++) {
                                    $worksheet
                                        ->getStyle('A' . $i)
                                        ->applyFromArray(self::STYLE_ALIGN_LEFT);
                                    $worksheet
                                        ->getStyle('B' . $i)
                                        ->applyFromArray(self::STYLE_ALIGN_RIGHT);
                                }

                                // Responsable
                                $rowToEnd = $rowToBegin + 5;
                                $worksheet
                                    ->getStyle('D' . $rowToBegin . ':F' . $rowToEnd)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_BOLD,
                                        self::STYLE_ALIGN_CENTER
                                    ));
                                $worksheet->mergeCells('D' . $rowToBegin . ':F' . ($rowToBegin + 1));
                                $worksheet
                                    ->getStyle('D' . $rowToBegin . ':F' . ($rowToBegin + 1))
                                    ->getAlignment()
                                    ->setWrapText(true);
                                $worksheet->mergeCells('D' . ($rowToBegin + 2) . ':F' . $rowToEnd);

                                // Salarié
                                $rowToEnd = $rowToBegin + 5;
                                $worksheet
                                    ->getStyle('H' . $rowToBegin . ':J' . $rowToEnd)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_BOLD,
                                        self::STYLE_ALIGN_CENTER
                                    ));
                                $worksheet->mergeCells('H' . $rowToBegin . ':J' . ($rowToBegin + 1));
                                $worksheet
                                    ->getStyle('H' . $rowToBegin . ':J' . ($rowToBegin + 1))
                                    ->getAlignment()
                                    ->setWrapText(true);
                                $worksheet->mergeCells('H' . ($rowToBegin + 2) . ':J' . $rowToEnd);
                            } else {
                                // Absences
                                $absenceTypeNumber = count($this->getAbsenceTypes());
                                $rowToEnd          = $rowToBegin + $absenceTypeNumber;
                                $worksheet
                                    ->getStyle('A' . $rowToBegin . ':B' . $rowToEnd)
                                    ->applyFromArray(self::STYLE_TABLE);

                                $worksheet->mergeCells('A' . $rowToBegin . ':B' . $rowToBegin);
                                $worksheet
                                    ->getStyle('A' . $rowToBegin . ':B' . $rowToBegin)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_ALIGN_CENTER,
                                        self::STYLE_BOLD
                                    ));

                                for ($i = $rowToBegin + 1; $i <= $rowToEnd; $i++) {
                                    $worksheet
                                        ->getStyle('A' . $i)
                                        ->applyFromArray(self::STYLE_ALIGN_LEFT);
                                    $worksheet
                                        ->getStyle('B' . $i)
                                        ->applyFromArray(self::STYLE_ALIGN_RIGHT);
                                }

                                // Syntheses
                                $rowToEnd = $rowToBegin + 4;

                                $worksheet->setCellValue('K' . ($rowToBegin + 1), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 6) . '/' . $this->dayHours);
                                $worksheet->setCellValue('K' . ($rowToBegin + 2), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 5) . '/' . $this->dayHours);
                                $worksheet->setCellValue('K' . ($rowToBegin + 3), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 4) . '/' . $this->dayHours);
                                $worksheet->setCellValue('K' . ($rowToBegin + 4), '=' . $totalLinesColBySheet[$worksheetName] . ($rowToBegin - 2));
                                $worksheet
                                    ->getStyle('D' . $rowToBegin . ':L' . $rowToEnd)
                                    ->applyFromArray(self::STYLE_TABLE);
                                $worksheet->mergeCells('D' . $rowToBegin . ':L' . $rowToBegin);
                                $worksheet
                                    ->getStyle('D' . $rowToBegin . ':L' . $rowToBegin)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_ALIGN_CENTER,
                                        self::STYLE_BOLD
                                    ));

                                for ($i = $rowToBegin + 1; $i <= $rowToEnd; $i++) {
                                    $worksheet->mergeCells('D' . $i . ':J' . $i);
                                    $worksheet->mergeCells('K' . $i . ':L' . $i);
                                    $worksheet
                                        ->getStyle('D' . $i)
                                        ->applyFromArray(self::STYLE_ALIGN_LEFT);
                                    $worksheet
                                        ->getStyle('K' . $i)
                                        ->applyFromArray(self::STYLE_ALIGN_RIGHT);
                                }

                                // Responsable
                                $rowToEnd = $rowToBegin + 5;
                                $worksheet
                                    ->getStyle('N' . $rowToBegin . ':V' . $rowToEnd)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_BOLD,
                                        self::STYLE_ALIGN_CENTER
                                    ));
                                $worksheet->mergeCells('N' . $rowToBegin . ':V' . ($rowToBegin + 1));
                                $worksheet
                                    ->getStyle('N' . $rowToBegin . ':V' . ($rowToBegin + 1))
                                    ->getAlignment()
                                    ->setWrapText(true);
                                $worksheet->mergeCells('N' . ($rowToBegin + 2) . ':V' . $rowToEnd);

                                // Salarié
                                $rowToEnd = $rowToBegin + 5;
                                $worksheet
                                    ->getStyle('X' . $rowToBegin . ':AF' . $rowToEnd)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_BOLD,
                                        self::STYLE_ALIGN_CENTER
                                    ));
                                $worksheet->mergeCells('X' . $rowToBegin . ':AF' . ($rowToBegin + 1));
                                $worksheet
                                    ->getStyle('X' . $rowToBegin . ':AF' . ($rowToBegin + 1))
                                    ->getAlignment()
                                    ->setWrapText(true);
                                $worksheet->mergeCells('X' . ($rowToBegin + 2) . ':AF' . $rowToEnd);
                            }
                            break;

                    }
                }
                $rowAfterDate = count($tables[0]) + 3;
                $worksheet
                    ->getStyle('A' . $rowAfterDate . ':' . $worksheet->getHighestDataColumn() . $worksheet->getHighestDataRow())
                    ->getNumberFormat()
                    ->setFormatCode('0.0');
                $worksheet
                    ->getColumnDimension('A')
                    ->setAutoSize(true);
                foreach ($worksheet->getColumnIterator('B') as $key => $col) {
                    $worksheet
                        ->getColumnDimension($key)
                        ->setWidth($isReview ? 15 : 5);
                }
            }

            foreach (array_reverse($reviewSheetNames) as $reviewSheetName) {
                $sheet = $spreadsheet->getSheetByName($reviewSheetName);
                $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($sheet));
                $spreadsheet->addSheet($sheet, 0);
            }

            // exit;

            $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($spreadsheet->getSheetByName('Worksheet')));
            $spreadsheet->setActiveSheetIndex(0);

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            header('Content-Disposition: attachment; filename="export.xlsx"');
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            ob_end_clean();
            $writer->save('php://output');
            exit;
        } catch (Exception $exception) {
            var_dump($exception->getMessage());
        }
    }

    /**
     * This function returns an array of timestamp corresponding to french holidays
     * @return array
     */
    protected function getHolidays()
    {
        $year = $this->currentStart->format('Y');

        $easterDate  = easter_date($year);
        $easterDay   = date('d', $easterDate);
        $easterMonth = date('m', $easterDate);
        $easterYear  = date('Y', $easterDate);

        $holidays = [
            // These days have a fixed date
            DateTime::createFromFormat('d/m/Y', 1 . '/' . 1 . '/' . $year)->setTime(0, 0),   // 1er janvier
            DateTime::createFromFormat('d/m/Y', 1 . '/' . 5 . '/' . $year)->setTime(0, 0),   // Fête du travail
            DateTime::createFromFormat('d/m/Y', 8 . '/' . 5 . '/' . $year)->setTime(0, 0),   // Victoire des alliés
            DateTime::createFromFormat('d/m/Y', 14 . '/' . 7 . '/' . $year)->setTime(0, 0),  // Fête nationale
            DateTime::createFromFormat('d/m/Y', 15 . '/' . 8 . '/' . $year)->setTime(0, 0),  // Assomption
            DateTime::createFromFormat('d/m/Y', 1 . '/' . 11 . '/' . $year)->setTime(0, 0),  // Toussaint
            DateTime::createFromFormat('d/m/Y', 11 . '/' . 11 . '/' . $year)->setTime(0, 0), // Armistice
            DateTime::createFromFormat('d/m/Y', 25 . '/' . 12 . '/' . $year)->setTime(0, 0), // Noel

            // These days have a date depending on easter
            DateTime::createFromFormat('d/m/Y', $easterDay + 2 . '/' . $easterMonth . '/' . $easterYear)->setTime(0, 0),
            DateTime::createFromFormat('d/m/Y', $easterDay + 40 . '/' . $easterMonth . '/' . $easterYear)->setTime(0, 0),
            DateTime::createFromFormat('d/m/Y', $easterDay + 50 . '/' . $easterMonth . '/' . $easterYear)->setTime(0, 0),
        ];

        sort($holidays);

        return $holidays;
    }

    protected function getAbsenceTypes()
    {
        $ret = [
            'Weekend'    => self::ABSENCE_WEEKEND,
            'Jour férié' => self::ABSENCE_HOLIDAYS,
        ];

        /** @var AbsenceType[] $absenceTypes */
        $absenceTypes = $this->em->getRepository('Time\Entity\AbsenceType')->findAll();

        foreach ($absenceTypes as $absenceType) {
            $ret[$absenceType->getName()] = $absenceType->getInitialLetters();
        }

        return $ret;
    }

    protected function getOtherLifeHours($otherLifeGroups)
    {
        if (!$otherLifeGroups) {
            return [];
        }

        $otherLifeProjects = $this->getOtherLifeProjects($otherLifeGroups);

        $otherLifeHours = [];
        $projectIds     = [];

        foreach ($otherLifeProjects as $project) {
            $projectIds[] = $project['id'];
        }

        $timesheets = $this->apiRequest->__invoke(
            'Time\Controller\API\Timesheet',
            Request::METHOD_GET,
            [
                'col'    => ['id', 'hours', 'start'],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'start',
                        'order'   => 'asc',
                        'filters' => [
                            'user'    => [
                                'op'  => 'eq',
                                'val' => $this->user->getId(),
                            ],
                            'project' => [
                                'op'  => 'eq',
                                'val' => $projectIds,
                            ],
                            'type'    => [
                                'op'  => 'eq',
                                'val' => 'done',
                            ],
                            'start'   => [
                                'op'  => 'period',
                                'val' => [
                                    'start' => $this->start->format('d/m/Y'),
                                    'end'   => $this->end->format('d/m/Y'),
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        )->dispatch()->getVariable('rows');

        foreach ($timesheets as $timesheet) {
            $date = DateTime::createFromFormat('d/m/Y H:i', $timesheet['start'])->setTime(0, 0);

            $day   = $date->format('j');
            $year  = $date->format('Y');
            $month = utf8_encode(ucfirst(strftime('%B', $date->getTimestamp())));

            $date = $day . ' ' . $month . ' ' . $year;

            if (!isset($otherLifeHours[$date])) {
                $otherLifeHours[$date] = $timesheet['hours'];
            } else {
                $otherLifeHours[$date] += $timesheet['hours'];
            }
        }

        return $otherLifeHours;
    }

    protected function getOtherLifeProjects($otherLifeGroups)
    {
        $otherLifeKeywords = [];

        foreach ($otherLifeGroups as $otherLifeGroup) {
            $keywords = $this->apiRequest->__invoke(
                'Keyword\Controller\API\Keyword',
                Request::METHOD_GET,
                [
                    'col'    => ['id'],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'id',
                            'order'   => 'asc',
                            'filters' => [
                                'group' => [
                                    'op'  => 'eq',
                                    'val' => $otherLifeGroup['id'],
                                ],
                            ],
                        ],
                    ],
                ]
            )->dispatch()->getVariable('rows');

            if (!$keywords) {
                continue;
            }

            $otherLifeKeywords[$otherLifeGroup['id']] = $keywords;
        }

        if (!$otherLifeKeywords) {
            return null;
        }

        $projects = [];

        foreach ($otherLifeKeywords as $group => $keywords) {
            $filters = [
                'publicationStatus' => [
                    'op'  => 'neq',
                    'val' => 'archived',
                ],
            ];

            $groupFilter = [
                'tree' => true,
                'op'   => 'eq',
                'val'  => [],
            ];

            foreach ($keywords as $keyword) {
                $groupFilter['val'][] = $keyword['id'];
            }

            $filters['keyword.' . $group] = $groupFilter;

            $filters['keyword.' . $this->group->getId()] = [
                'tree' => true,
                'op'   => 'isNull',
                'val'  => [],
            ];

            $projects = array_merge($projects, $this->apiRequest->__invoke(
                'Project\Controller\API\Project',
                Request::METHOD_GET,
                [
                    'col'    => ['id'],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'name',
                            'order'   => 'asc',
                            'filters' => $filters,
                        ],
                    ],
                ]
            )->dispatch()->getVariable('rows'));
        }

        return array_unique($projects, SORT_REGULAR);
    }
}