<?php

namespace Application\Controller\API;

use Core\Controller\BasicRestController;

class ConfigurationController extends BasicRestController
{
    protected $entityClass = 'Application\Entity\Configuration';
    protected $repository  = 'Application\Entity\Configuration';
    protected $entityChain = 'application:e:configuration';

    public function get($id, $callbacks = [])
    {
        $jsonModel = parent::get(1);
        $configuration = $jsonModel->getVariable('object');

        if (!$configuration) {
            $this->create([
                'id'           => 1,
                'modules'      => [],
                'translations' => [],
            ]);
            return parent::get(1);
        }

        return $jsonModel;
    }

    public function create($data)
    {
        $jsonModel = parent::get(1);
        $configuration = $jsonModel->getVariable('object');

        if (!$configuration) {
            return parent::create($data);
        }

        return parent::update(1, $data);
    }

    public function update($id, $data)
    {
        $data['id'] = 1;
        return parent::update(1, $data);
    }
}
