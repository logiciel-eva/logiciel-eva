<?php

namespace Export\Controller;

use Advancement\Entity\Advancement;
use Core\Controller\AbstractActionSLController;
use Export\Entity\Export;
use Project\Entity\Project;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
	public function indexAction()
	{
		if (!$this->acl()->isAllowed('export:e:export:r')) {
			return $this->notFoundAction();
		}

		return new ViewModel([]);
	}

	public function formExcelAction()
	{
		$id = $this->params()->fromRoute('id', false);

		if (!$this->acl()->basicFormAccess('export:e:export', $id)) {
			return $this->notFoundAction();
		}

		$export = $this->entityManager()
			->getRepository('Export\Entity\Export')
			->find($id);

		if (
			$export &&
			!$this->acl()->ownerControl('export:e:export:u', $export) &&
			!$this->acl()->ownerControl('export:e:export:r', $export)
		) {
			return $this->notFoundAction();
		}

		if ($id && !$export) {
			return $this->notFoundAction();
		}

		if ($export && $export->getType() !== Export::TYPE_EXCEL) {
			return $this->redirect()->toRoute('export/form/' . $export->getType(), [
				'id' => $export->getId(),
			]);
		}

		$fields = $this->getFormData();

		unset($fields['pictures']);

		foreach ($fields as $i => &$field) {
			unset($field['arrays']);
			unset($field['charts']);
			if (isset($field['data']) && count($field['data']) === 0) {
				unset($fields[$i]);
			}
		}

		return new ViewModel([
			'export' => $export,
			'fields' => $fields,
		]);
	}

	public function formWordAction()
	{
		$id = $this->params()->fromRoute('id', false);

		if (!$this->acl()->basicFormAccess('export:e:export', $id)) {
			return $this->notFoundAction();
		}

		$export = $this->entityManager()
			->getRepository('Export\Entity\Export')
			->find($id);

		if (
			$export &&
			!$this->acl()->ownerControl('export:e:export:u', $export) &&
			!$this->acl()->ownerControl('export:e:export:r', $export)
		) {
			return $this->notFoundAction();
		}

		if ($id && !$export) {
			return $this->notFoundAction();
		}

		if ($export && $export->getType() !== Export::TYPE_WORD) {
			return $this->redirect()->toRoute('export/form/' . $export->getType(), [
				'id' => $export->getId(),
			]);
		}

		$fields = $this->getFormData();

		return new ViewModel([
			'export' => $export,
			'fields' => $fields,
		]);
	}

	public function formPublipostagesAction() {

     	$id = $this->params()->fromRoute('id', false);

		if (!$this->acl()->basicFormAccess('export:e:export', $id)) {
			return $this->notFoundAction();
		}

		$export = $this->entityManager()
			->getRepository('Export\Entity\Export')
			->find($id);

		if (
			$export &&
			!$this->acl()->ownerControl('export:e:export:u', $export) &&
			!$this->acl()->ownerControl('export:e:export:r', $export)
		) {
			return $this->notFoundAction();
		}

		if ($id && !$export) {
			return $this->notFoundAction();
		}

		if ($export && $export->getType() !== Export::TYPE_PUBLIPOSTAGES) {
			return $this->redirect()->toRoute('export/form/' . $export->getType(), [
				'id' => $export->getId(),
			]);
		}

		$fields = $this->getPublipostageData();

		return new ViewModel([
			'export' => $export,
			'fields' => $fields,
		]);
	}

	/**
	 * Export map as png
	 */
	public function mapAction()
	{
		$class = $this->params()->fromQuery('class', null);
		$mode = $this->params()->fromQuery('mode', '');
		$id = $this->params()->fromQuery('id', null);

		if ($class && $id) {
			$object = $this->entityManager()
				->getRepository(urldecode($class))
				->find($id);
			if ($object) {
				$json = [];
				$territories = $object->{'getTerritories' . ($mode === 'arbo' ? 'Arbo' : '')}(
					$this->entityManager()
				);
				foreach ($territories as $territory) {
					$json[] = $territory->getJson();
				}

				$viewModel = new ViewModel([
					'json' => $json,
				]);

				$viewModel->setTerminal(true);

				return $viewModel;
			}
		}
		exit();
	}

	/**
	 * Export advancement chart as png
	 */
	public function advancementAction()
	{
		$class = $this->params()->fromQuery('class', null);
		$id = $this->params()->fromQuery('id', null);

		if ($class && $id) {
			$object = $this->entityManager()
				->getRepository(urldecode($class))
				->find($id);
			if ($object) {
				$viewModel = new ViewModel([
					'stack' => $object->getStackedAdvancement(),
				]);

				$viewModel->setTerminal(true);

				return $viewModel;
			}
		}
		exit();
	}

	protected function getPublipostageData(){
		$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
		$translator = $helperPluginManager->get('translate');
		$fields = [];
		//Structure
		$fields['structure'] = [
			'options' => [
				'icon' => 'structure',
				'title' => ucfirst($translator->__invoke('structure_entity')),
			],
			'data' => []
		];

		$fields['structure']['data']['structure.name'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_name'),
		];

		$fields['structure']['data']['structure.sigle'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_sigle'),
		];

		$fields['structure']['data']['structure.email'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_email'),
		];

		$fields['structure']['data']['structure.phone'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_phone'),
		];

		$fields['structure']['data']['structure.zipcode'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_zipcode'),
		];

		$fields['structure']['data']['structure.city'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_city'),
		];

		$fields['structure']['data']['structure.country'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_country'),
		];
		$fields['structure']['data']['structure.addressLine1'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_addressLine1'),
		];
		$fields['structure']['data']['structure.addressLine2'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_addressLine2'),
		];
		$fields['structure']['data']['structure.createdAt'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_createdAt'),
		];
		$fields['structure']['data']['structure.updatedAt'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_updatedAt'),
		];
		$fields['structure']['data']['structure.financer'] = [
			'text' =>ucfirst($translator->__invoke('structure_entity')).' '. $translator->__invoke('structure_field_financer'),
		];
		$fields['structure']['data']['structure.siret'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_siret'),
		];
		$fields['structure']['data']['structure.website'] = [
			'text' => ucfirst($translator->__invoke('structure_entity')).' '.$translator->__invoke('structure_field_website'),
		];

		$fields['contact'] = [
			'options' => [
				'icon' => 'structure',
				'title' => ucfirst($translator->__invoke('contact_entity')),
			],
			'data' => []
		];

		$fields['contact']['data']['contact.lastName'] = [
			'text' => $translator->__invoke('contact_field_lastName'),
		];
		$fields['contact']['data']['contact.firstName'] = [
			'text' => $translator->__invoke('contact_field_firstName'),
		];
		$fields['contact']['data']['contact.gender'] = [
			'text' => $translator->__invoke('contact_field_gender'),
		];
		$fields['contact']['data']['contact.vip'] = [
			'text' => $translator->__invoke('contact_field_vip'),
		];
		$fields['contact']['data']['contact.subscriptionStatus'] = [
			'text' => $translator->__invoke('contact_field_subscriptionStatus'),
		];
		$fields['contact']['data']['contact.subscriptionStatusUpdatedAt'] = [
			'text' => $translator->__invoke('contact_field_subscriptionStatusUpdatedAt'),
		];
		$fields['contact']['data']['contact.email'] = [
			'text' => $translator->__invoke('contact_field_email'),
		];
		$fields['contact']['data']['contact.phone'] = [
			'text' => $translator->__invoke('contact_field_phone'),
		];
		$fields['contact']['data']['contact.portable'] = [
			'text' => $translator->__invoke('contact_field_portable'),
		];
		$fields['contact']['data']['contact.zipcode'] = [
			'text' => $translator->__invoke('contact_field_zipcode'),
		];
		$fields['contact']['data']['contact.city'] = [
			'text' => $translator->__invoke('contact_field_city'),
		];
		$fields['contact']['data']['contact.country'] = [
			'text' => $translator->__invoke('contact_field_country'),
		];
		$fields['contact']['data']['contact.addressLine1'] = [
			'text' => $translator->__invoke('contact_field_addressLine1'),
		];
		$fields['contact']['data']['contact.addressLine2'] = [
			'text' => $translator->__invoke('contact_field_addressLine2'),
		];


		$fields['job'] = [
			'options' => [
				'icon' => 'job',
				'title' => ucfirst($translator->__invoke('job_entity')),
			],
			'data' => []
		];

		$fields['job']['data']['job.name'] = [
			'text' => $translator->__invoke('job_field_name'),
		];

		$groups = $this->allowedKeywordGroups('contactJobStructure', false);
		foreach ($groups as $group) {
			$fields['job']['data']['keywords' .'.'. $group->getId() . ''] = [
				'text' =>  $group->getName(),
			];
		}
		return $fields;

	}

	protected function getFormData()
	{
		$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
		$translator = $helperPluginManager->get('translate');
		$ht = $this->serviceLocator
			->get('MyModuleManager')
			->getClientModuleConfiguration('budget', 'ht');
		$budgetCode = $this->serviceLocator
			->get('MyModuleManager')
			->getClientModuleConfiguration('budget', 'code');
		$evolution = $this->serviceLocator
			->get('MyModuleManager')
			->getClientModuleConfiguration('budget', 'evolution');

		$fields = [];

		/**********************
		 *   GENERAL PROJET   *
		 **********************/
		$fields['project'] = [
			'options' => [
				'icon' => 'project',
				'title' => ucfirst($translator->__invoke('project_entity')),
				'onlyProject' => true,
			],
			'data' => [],
			'arrays' => [],
			'charts' => [],
		];

		foreach ($this->getEntityMeta('project')['inputFilter']->getInputs() as $input) {
			$key = $input->getName();
			$text = $translator->__invoke('project_field_' . $input->getName());

			$fields['project']['data'][$key] = [
				'text' => $text,
			];

			if ($key === 'members' || $key === 'managers' || $key === 'validators') {
				$fields['project']['arrays'][$key . 'Array'] = [
					'text' => $text,
				];

				$fields['project']['data'][$key]['help'] = 'Séparation : points-virgules';

				if ($key === 'members') {
					$fields['project']['arrays'][$key . 'Array']['help'] = 'Membre, rôle dans l\'équipe';
				}
			}
		}

		if ($budgetCode) {
			$fields['project']['data']['budgetCodes'] = [
				'text' => $translator->__invoke('project_field_budgetCodes'),
				'help' => 'Séparation : points-virgules',
			];
		}

		$fields['project']['data']['level'] = [
			'text' => $translator->__invoke('project_field_level'),
		];

		$fields['project']['data']['rateExpenseRealized'] = [
			'text' => $translator->__invoke('project_field_rateExpenseRealized'),
		];
		$fields['project']['data']['rateIncomeRealized'] = [
			'text' => $translator->__invoke('project_field_rateIncomeRealized'),
		];

		$fields['project']['data']['createdAt'] = [
			'text' => $translator->__invoke('project_field_createdAt'),
		];
		$fields['project']['data']['updatedAt'] = [
			'text' => $translator->__invoke('project_field_updatedAt'),
		];

		$fields['project']['data']['children'] = [
			'text' => 'Enfants',
			'params' => [
				'properties' => [
					'text' => 'Propriétés',
					'type' => 'multiple',
					'required' => true,
					'options' => [
						'code' => $translator->__invoke('project_field_code'),
						'name' => $translator->__invoke('project_field_name'),
					],
				],
				'depth' => [
					'text' => 'Profondeur de récupération des enfants',
					'type' => 'unique',
					'required' => true,
					'options' => [
						'direct' => 'Enfants directs',
						'all' => 'Tous les enfants',
					],
				],
				'format' => [
					'text' => 'Séparation des ' . $translator->__invoke('project_entities'),
					'type' => 'unique',
					'required' => true,
					'options' => [
						'comma' => 'Points-virgules',
						'list' => 'Liste',
					],
				],
			],
		];
		$fields['project']['data']['actors'] = [
            'text' => $translator->__invoke('project_field_actors'),
            'params' => [
                'columns' => [
                    'text' => $translator->__invoke('columns'),
                    'type' => 'multiple',
                    'options' => [
                        'name' => $translator->__invoke('structure_field_name'),
                        'sigle' => $translator->__invoke('structure_field_sigle'),
                        'email' => $translator->__invoke('structure_field_email'),
                        'phone' => $translator->__invoke('structure_field_phone'),
                        'zipcode' => $translator->__invoke('structure_field_zipcode'),
                        'addressLine1' => $translator->__invoke('structure_field_addressLine1'),
                        'addressLine2' => $translator->__invoke('structure_field_addressLine2'),
                        'city' => $translator->__invoke('structure_field_city'),
                        'country' => $translator->__invoke('structure_field_country'),
                        'financer' => $translator->__invoke('structure_field_financer'),
                        'siret' => $translator->__invoke('structure_field_siret'),
                        'keywords' => $translator->__invoke('structure_field_keywords'),
                    ],
                ],
            ],
			'help' => 'Séparation : points-virgules',
		];
        $fields['project']['arrays']['actorsArray'] = [
            'text' => $translator->__invoke('project_field_actors'),
            'params' => [
                'columns' => [
                    'text' => $translator->__invoke('columns'),
                    'type' => 'multiple',
                    'options' => [
                        'name' => $translator->__invoke('structure_field_name'),
                        'sigle' => $translator->__invoke('structure_field_sigle'),
                        'email' => $translator->__invoke('structure_field_email'),
                        'phone' => $translator->__invoke('structure_field_phone'),
                        'zipcode' => $translator->__invoke('structure_field_zipcode'),
                        'addressLine1' => $translator->__invoke('structure_field_addressLine1'),
                        'addressLine2' => $translator->__invoke('structure_field_addressLine2'),
                        'city' => $translator->__invoke('structure_field_city'),
                        'country' => $translator->__invoke('structure_field_country'),
                        'financer' => $translator->__invoke('structure_field_financer'),
                        'siret' => $translator->__invoke('structure_field_siret'),
                        'keywords' => $translator->__invoke('structure_field_keywords'),
                    ],
                ],
            ],
        ];
		$fields['project']['arrays']['actorsArrayKeywords'] = [
			'text' =>
				$translator->__invoke('project_field_actors') .
				' avec ' .
				$translator->__invoke('keyword_entities'),
			'help' => 'Acteur, mot-clefs',
		];

		if (isset($fields['project']['data']['parent'])) {
			$fields['project']['data']['parent']['params'] = [
				'arbo' => [
					'text' => 'Voir l\'arborescence',
					'type' => 'boolean',
				],
			];
		}

		/***********************
		 *   GENERAL KEYWORD   *
		 ***********************/
		if ($this->moduleManager()->isActive('keyword')) {
			$fields['keyword'] = [
				'options' => [
					'icon' => 'keyword',
					'title' => ucfirst($translator->__invoke('keyword_entity')),
					'onlyKeyword' => true,
				],
				'data' => [],
				'arrays' => [],
				'charts' => [],
			];
			foreach ($this->getEntityMeta('keyword')['inputFilter']->getInputs() as $input) {
				$key = $input->getName();
				$text = $translator->__invoke('keyword_field_' . $input->getName());
				if ($key === 'name' || $key === 'description') {
					$fields['keyword']['data'][$key] = [
						'text' => $text,
					];
				}
			}
			$fields['keyword']['data']['createdAt'] = [
				'text' => $translator->__invoke('keyword_field_createdAt'),
			];
			$fields['keyword']['data']['updatedAt'] = [
				'text' => $translator->__invoke('keyword_field_updatedAt'),
			];
		}

		/***********************
		 *       BUDGET        *
		 ***********************/
		if ($this->moduleManager()->isActive('budget')) {
			$fields['budget'] = [
				'options' => [
					'icon' => 'budget',
					'title' => ucfirst($translator->__invoke('budget_module_title')),
				],
				'data' => [
					'financer' => [
						'text' => ucfirst($translator->__invoke('envelope_field_financer')),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
							'type' => [
								'text' => $translator->__invoke('income_field_type'),
								'type' => 'unique',
								'options' => [
									'poste' =>
										ucfirst($translator->__invoke('poste_income_field_amount')) .
										' ' .
										$translator->__invoke('poste_entity'),
								],
							],
							'showPercent' => [
								'text' => 'Voir les pourcentages',
								'type' => 'boolean',
							],
							'showValue' => [
								'text' => 'Voir les valeurs ',
								'type' => 'boolean',
							],
						],
					],
					'amountPosteExpense' => [
						'text' =>
							ucfirst($translator->__invoke('poste_expense_entity')) .
							' - ' .
							$translator->__invoke('poste_expense_field_amount'),
					],
					'amountHTPosteExpense' => [
						'text' =>
							ucfirst($translator->__invoke('poste_expense_entity')) .
							' - ' .
							$translator->__invoke('poste_expense_field_amountHT'),
					],
					'amountPosteExpenseArbo' => [
						'text' =>
							ucfirst($translator->__invoke('poste_expense_entity')) .
							' - ' .
							$translator->__invoke('poste_expense_field_amountArbo'),
					],
					'amountHTPosteExpenseArbo' => [
						'text' =>
							ucfirst($translator->__invoke('poste_expense_entity')) .
							' - ' .
							$translator->__invoke('poste_expense_field_amountHTArbo'),
					],
					'amountPosteIncome' => [
						'text' =>
							ucfirst($translator->__invoke('poste_income_entity')) .
							' - ' .
							$translator->__invoke('poste_income_field_amount'),
					],
					'amountHTPosteIncome' => [
						'text' =>
							ucfirst($translator->__invoke('poste_income_entity')) .
							' - ' .
							$translator->__invoke('poste_income_field_amountHT'),
					],
					'amountPosteIncomeArbo' => [
						'text' =>
							ucfirst($translator->__invoke('poste_income_entity')) .
							' - ' .
							$translator->__invoke('poste_income_field_amountArbo'),
					],
					'amountHTPosteIncomeArbo' => [
						'text' =>
							ucfirst($translator->__invoke('poste_income_entity')) .
							' - ' .
							$translator->__invoke('poste_income_field_amountHTArbo'),
					],
				],
				'arrays' => [					
					'expensesArray' => [
						'text' => 'Liste des ' . $translator->__invoke('expense_entities'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
							'type' => [
								'text' => $translator->__invoke('expense_field_type'),
								'type' => 'multiple',
								'required' => false,
								'options' => [],
							],
							'columns' => [
								'text' => $translator->__invoke('columns'),
								'type' => 'multiple',
								'required' => false,
								'options' => [
									'name' => $translator->__invoke('expense_field_name'),
									'amount' => $translator->__invoke('expense_field_amount'),
									'type' => $translator->__invoke('expense_field_type'),
									'date' => $translator->__invoke('expense_field_date'),
									'tiers' => $translator->__invoke('expense_field_tiers'),
									'anneeExercice' => $translator->__invoke('expense_field_anneeExercice'),
									'bordereau' => $translator->__invoke('expense_field_bordereau'),
									'mandat' => $translator->__invoke('expense_field_mandat'),
									'engagement' => $translator->__invoke('expense_field_engagement'),
									'complement' => $translator->__invoke('expense_field_complement'),
									'dateFacture' => $translator->__invoke('expense_field_dateFacture'),
									'poste' => $translator->__invoke('expense_field_poste'),
									'project' => ucfirst($translator->__invoke('project_entity')),
								],
							],
							'total' => [
								'text' => 'Total',
								'type' => 'unique',
								'options' => [
									'true' => $translator->__invoke('yes'),
									'byProject' => $translator->__invoke('by_project'),
									'false' => $translator->__invoke('no'),
								],
							],
							'groupByPoste' => [
								'text' => 'Grouper par poste',
								'type' => 'boolean',
							],
							'posteColumns' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => $translator->__invoke('columns') . ' des lignes postes',
								'type' => 'multiple',
								'required' => false,
								'options' => [
									'account' => $translator->__invoke('poste_expense_field_account'),
									'name' => $translator->__invoke('poste_expense_field_name'),
									'amount' => $translator->__invoke('poste_expense_field_amount'),
									'amountHT' => $translator->__invoke('poste_expense_field_amountHT'),
									'amountArbo' => $translator->__invoke('poste_expense_field_amountArbo'),
									'amountHTArbo' => $translator->__invoke('poste_expense_field_amountHTArbo'),
								],
							],
							'hidePostesAmount0' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => $translator->__invoke('export_hide_zero_amount'),
								'type' => 'boolean',
								'required' => false,
							],
							'showOnlyTotal' => [
								'if' => 'self.variablesParams.total != \'false\'',
								'text' => $translator->__invoke('export_show_only_total'),
								'type' => 'boolean',
								'required' => false,
							],
							'groupByYear' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => $translator->__invoke('export_group_by_year'),
								'type' => 'boolean',
								'required' => false
							],
							'showOnlyPreviousAndCurrentYears' => [
								'if' => 'self.variablesParams.groupByYear == \'true\'',
								'text' => $translator->__invoke('export_prev_and_current_years'),
								'type' => 'boolean',
								'required' => false
							],
							'showOnlyPoste' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => $translator->__invoke('export_show_only_poste'),
								'type' => 'boolean',
								'required' => false
							]
						],
					],
					'incomesArray' => [
						'text' => 'Liste des ' . $translator->__invoke('income_entities'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
							'type' => [
								'text' => $translator->__invoke('income_field_type'),
								'type' => 'multiple',
								'required' => false,
								'options' => [],
							],
							'columns' => [
								'text' => $translator->__invoke('columns'),
								'type' => 'multiple',
								'required' => false,
								'options' => [
									'name' => $translator->__invoke('income_field_name'),
									'amount' => $translator->__invoke('income_field_amount'),
									'type' => $translator->__invoke('income_field_type'),
									'date' => $translator->__invoke('income_field_date'),
									'tiers' => $translator->__invoke('income_field_tiers'),
									'anneeExercice' => $translator->__invoke('income_field_anneeExercice'),
									'bordereau' => $translator->__invoke('income_field_bordereau'),
									'mandat' => $translator->__invoke('income_field_mandat'),
									'engagement' => $translator->__invoke('income_field_engagement'),
									'complement' => $translator->__invoke('income_field_complement'),
									'serie' => $translator->__invoke('income_field_serie'),
									'poste' => $translator->__invoke('income_field_poste'),
									'project' => ucfirst($translator->__invoke('project_entity')),
									'financer' => $translator->__invoke('envelope_field_financer'),
									'evolution' => $translator->__invoke('income_field_evolution'),
									'transfer.name' => $translator->__invoke('income_field_transfer_name'),
									'transfer.code' => $translator->__invoke('income_field_transfer_code')
								],
							],
							'total' => [
								'text' => 'Total',
								'type' => 'unique',
								'options' => [
									'true' => $translator->__invoke('yes'),
									'byProject' => $translator->__invoke('by_project'),
									'false' => $translator->__invoke('no'),
								],
							],
							'groupByPoste' => [
								'text' => 'Grouper par poste',
								'type' => 'boolean',
							],
							'posteColumns' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => $translator->__invoke('columns') . ' des lignes postes',
								'type' => 'multiple',
								'required' => false,
								'options' => [
									'account' => $translator->__invoke('poste_income_field_account'),
									'name' => $translator->__invoke('poste_income_field_name'),
									'amount' => $translator->__invoke('poste_income_field_amount'),
									'amountHT' => $translator->__invoke('poste_income_field_amountHT'),
									'amountArbo' => $translator->__invoke('poste_income_field_amountArbo'),
									'amountHTArbo' => $translator->__invoke('poste_income_field_amountHTArbo'),
									'percentage' => $translator->__invoke('poste_income_field_percentage'),
									'envelope' => $translator->__invoke('poste_income_field_envelope'),
									'autofinancement' => $translator->__invoke('poste_income_field_autofinancement'),
									'caduciteStart' => $translator->__invoke('poste_income_field_caduciteStart'),
									'caduciteEnd' => $translator->__invoke('poste_income_field_caduciteEnd'),
									'caduciteSendProof' => $translator->__invoke(
										'poste_income_field_caduciteSendProof'
									),
									'folderSendingDate' => $translator->__invoke(
										'poste_income_field_folderSendingDate'
									),
									'folderReceiptDate' => $translator->__invoke(
										'poste_income_field_folderReceiptDate'
									),
									'arrDate' => $translator->__invoke('poste_income_field_arrDate'),
									'arrNumber' => $translator->__invoke('poste_income_field_arrNumber'),
									'deliberationDate' => $translator->__invoke(
										'poste_income_field_deliberationDate'
									),
									'financer' => $translator->__invoke('envelope_field_financer'),
								],
							],
							'hidePostesAmount0' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => 'Cacher les postes dont le montant est égal à 0',
								'type' => 'boolean',
								'required' => false,
							],
							'showOnlyTotal' => [
								'if' => 'self.variablesParams.total != \'false\'',
								'text' => 'Afficher seulement les totaux',
								'type' => 'boolean',
								'required' => false,
							],
							'showTotalEnvelopes' => [
								'if' => 'self.variablesParams.total != \'false\'',
								'text' =>
									'Afficher le total des ' .
									$translator->__invoke('envelope_entities') .
									' par ' .
									$translator->__invoke('project_entity'),
								'type' => 'boolean',
								'required' => false,
							],
							'groupByYear' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => $translator->__invoke('export_group_by_year'),
								'type' => 'boolean',
								'required' => false
							],
							'showOnlyPreviousAndCurrentYears' => [
								'if' => 'self.variablesParams.groupByYear == \'true\'',
								'text' => $translator->__invoke('export_prev_and_current_years'),
								'type' => 'boolean',
								'required' => false
							],
							'showOnlyPoste' => [
								'if' => 'self.variablesParams.groupByPoste == \'true\'',
								'text' => $translator->__invoke('export_show_only_poste'),
								'type' => 'boolean',
								'required' => false
							]
						],
					],
				],
				'charts' => [
					'financerBudgetRepartitionChart' => [
						'text' => 'Graphique par ' . $translator->__invoke('envelope_field_financer'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
							'type' => [
								'text' => $translator->__invoke('income_field_type'),
								'type' => 'unique',
								'options' => [
									'poste' =>
										ucfirst($translator->__invoke('poste_income_field_amount')) .
										' ' .
										$translator->__invoke('poste_entity'),
								],
							],
							'showPercent' => [
								'text' => 'Voir les pourcentages',
								'type' => 'boolean',
							],
							'showValue' => [
								'text' => 'Voir les valeurs ',
								'type' => 'boolean',
							],
						],
					],
				],
			];

			foreach ($this->serviceLocator->get('expenseTypes') as $type) {
				$fields['budget']['data']['amountExpense' . ucfirst($type)] = [
					'text' =>
						ucfirst($translator->__invoke('expense_entity')) .
						' - ' .
						$translator->__invoke('expense_type_' . $type),
					'params' => [
						'arbo' => [
							'text' => 'Arborescent',
							'type' => 'boolean',
						],
					],
				];

				if ($evolution) {
					$fields['budget']['data']['amountExpense' . ucfirst($type)]['params']['evolution'] = [
						'text' => 'Type de montant par rapport aux évolutions',
						'required' => false,
						'type' => 'unique',
						'options' => [
							'without_evolutions' => 'Montant initial',
							'only_evolutions' => 'Montant de la révision',
							'with_evolutions' => 'Montant révisé',
						],
					];
				}

				$fields['budget']['arrays']['expensesArray']['params']['type']['options'][
					$type
				] = $translator->__invoke('expense_type_' . $type);

				if ($ht) {
					$fields['budget']['data']['amountHTExpense' . ucfirst($type)] = [
						'text' =>
							ucfirst($translator->__invoke('expense_entity')) .
							' - ' .
							$translator->__invoke('expense_type_' . $type . '_ht'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
						],
					];

					if ($evolution) {
						$fields['budget']['data']['amountHTExpense' . ucfirst($type)]['params']['evolution'] = [
							'text' => 'Type de montant par rapport aux évolutions',
							'required' => false,
							'type' => 'unique',
							'options' => [
								'without_evolutions' => 'Montant initial',
								'only_evolutions' => 'Montant de la révision',
								'with_evolutions' => 'Montant révisé',
							],
						];
					}

					$fields['budget']['arrays']['expensesArray']['params']['type']['options'][
						$type . '_ht'
					] = $translator->__invoke('expense_type_' . $type . '_ht');
				}
			}

			foreach ($this->serviceLocator->get('incomeTypes') as $type) {
				$fields['budget']['data']['amountIncome' . ucfirst($type)] = [
					'text' =>
						ucfirst($translator->__invoke('income_entity')) .
						' - ' .
						$translator->__invoke('income_type_' . $type),
					'params' => [
						'arbo' => [
							'text' => 'Arborescent',
							'type' => 'boolean',
						],
					],
				];

				$fields['budget']['arrays']['incomesArray']['params']['type']['options'][
					$type
				] = $translator->__invoke('income_type_' . $type);

				$fields['budget']['data']['financer']['params']['type']['options'][
					$type
				] = $translator->__invoke('income_type_' . $type);
				$fields['budget']['charts']['financerBudgetRepartitionChart']['params']['type']['options'][
					$type
				] = $translator->__invoke('income_type_' . $type);

				if ($ht) {
					$fields['budget']['data']['amountHTIncome' . ucfirst($type)] = [
						'text' =>
							ucfirst($translator->__invoke('income_entity')) .
							' - ' .
							$translator->__invoke('income_type_' . $type . '_ht'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
						],
					];

					$fields['budget']['arrays']['incomesArray']['params']['type']['options'][
						$type . '_ht'
					] = $translator->__invoke('income_type_' . $type . '_ht');
				}
			}
		}

		/***********************
		 *         TIME        *
		 ***********************/
		if ($this->moduleManager()->isActive('time')) {
			$fields['time'] = [
				'options' => [
					'icon' => 'time',
					'title' => ucfirst($translator->__invoke('time_module_title')),
				],
				'data' => [
					'timeSpent' => [
						'text' => $translator->__invoke('timesheet_type_done'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
						],
					],
					'timeTarget' => [
						'text' => $translator->__invoke('timesheet_type_target'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
						],
					],
					'timeLeft' => [
						'text' => $translator->__invoke('timesheet_type_left'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
						],
					],
				],
				'arrays' => [
					'userTimeRepartition' => [
						'text' => $translator->__invoke('user_time_repartition'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
							'columns' => [
								'text' => $translator->__invoke('columns'),
								'type' => 'multiple',
								'options' => [
									'done' => $translator->__invoke('timesheet_type_done'),
									'target' => $translator->__invoke('timesheet_type_target'),
									'left' => $translator->__invoke('timesheet_type_left'),
								],
							],
							'byYear' => [
								'text' => $translator->__invoke('export_userTimeRepartition_byYear'),
								'type' => 'boolean',
							],
							'yearMin' => [
								'if' => 'self.variablesParams.byYear === \'true\'',
								'text' => $translator->__invoke('export_userTimeRepartition_yearMin'),
								'type' => 'number',
								'required' => false,
							],
							'yearMax' => [
								'if' => 'self.variablesParams.byYear === \'true\'',
								'text' => $translator->__invoke('export_userTimeRepartition_yearMax'),
								'type' => 'number',
								'required' => false,
							],
							'convertToDays' => [
								'text' => $translator->__invoke('export_userTimeRepartition_convertToDays'),
								'type' => 'boolean',
							],
							'hoursByDay' => [
								'if' => 'self.variablesParams.convertToDays === \'true\'',
								'text' => $translator->__invoke('export_userTimeRepartition_hoursByDay'),
								'type' => 'number',
								'required' => false,
							],
						],
					],
				],
				'charts' => [
					'userTimeRepartitionChart' => [
						'text' => $translator->__invoke('user_time_repartition'),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
							'type' => [
								'text' => $translator->__invoke('timesheet_field_type'),
								'type' => 'unique',
								'options' => [
									'done' => $translator->__invoke('timesheet_type_done'),
									'target' => $translator->__invoke('timesheet_type_target'),
									'left' => $translator->__invoke('timesheet_type_left'),
								],
							],
							'showPercent' => [
								'text' => 'Voir les pourcentages',
								'type' => 'boolean',
							],
							'showValue' => [
								'text' => 'Voir les valeurs ',
								'type' => 'boolean',
							],
						],
					],
				],
			];

			if($this->moduleManager()->isActive('profile')) {

			    $columns = $fields['time']['arrays']['userTimeRepartition']['params']['columns']['options'];

                $fields['time']['arrays']['userTimeRepartition']['params']['columns']['options']['done_cost'] = $translator->__invoke('timesheet_type_done_cost') . ' €';
                $fields['time']['arrays']['userTimeRepartition']['params']['columns']['options']['target_cost'] = $translator->__invoke('timesheet_type_target_cost'). ' €';
                $fields['time']['arrays']['userTimeRepartition']['params']['columns']['options']['profile'] = $translator->__invoke('timesheet_type_profile');
                $fields['time']['arrays']['userTimeRepartition']['params']['columns']['options']['profile_cost'] = $translator->__invoke('timesheet_type_profile_cost');


                $fields['time']['data']['costSpent'] = [
                    'text' => $translator->__invoke('timesheet_type_done_cost'),
                    'params' => [
                        'arbo' => [
                            'text' => 'Arborescent',
                            'type' => 'boolean',
                        ],
                    ],
                ];

                $fields['time']['data']['costTarget'] = [
                    'text' => $translator->__invoke('timesheet_type_target_cost'),
                    'params' => [
                        'arbo' => [
                            'text' => 'Arborescent',
                            'type' => 'boolean',
                        ],
                    ],
                ];
            }
		}


		/***********************
		 *       FIELDS        *
		 ***********************/
		if ($this->moduleManager()->isActive('field')) {
			$fields['field'] = [
				'options' => [
					'icon' => 'field',
					'title' => ucfirst($translator->__invoke('field_module_title')),
					'onlyProject' => true,
				],
			];

			$groups = $this->allowedFieldGroups(Project::class);

			foreach ($groups as $group) {
				if ($group->getEnabled()) {
					$fields['field'][$group->getName()] = [];
					foreach ($group->getFields() as $field) {
						$fields['field'][$group->getName()]['customField(' . $field->getId() . ')'] = [
							'text' => $field->getName(),
						];
					}
				}
			}
		}

		/***********************
		 *       KEYWORDS      *
		 ***********************/
		if ($this->moduleManager()->isActive('keyword')) {
			$fields['keywords'] = [
				'options' => [
					'icon' => 'keyword',
					'title' => ucfirst($translator->__invoke('keyword_module_title')),
					'onlyProject' => true,
				],
				'data' => [],
				'arrays' => [],
				'charts' => [],
			];

			$groups = $this->allowedKeywordGroups('project', false);
			foreach ($groups as $group) {
				if ($group->getType() == 'reference') {
					$fields['keywords']['data']['keyword(' . $group->getId() . ')'] = [
						'text' => $group->getName(),
						'params' => [
							'arbo' => [
								'text' => 'Voir l\'arborescence',
								'type' => 'boolean',
							],
						],
					];
				} else {
					$fields['keywords']['data']['keyword(' . $group->getId() . ')'] = [
						'text' => $group->getName(),
					];
				}
			}

			$groups = $this->allowedKeywordGroups('actor', false);
			foreach ($groups as $group) {
				$fields['project']['data']['actorsKeywords_' . $group->getId() . ''] = [
					'text' => $translator->__invoke('project_field_actors') . ' avec ' . $group->getName(),
					'help' => 'Séparation : points-virgules',
				];
			}
		}

		/***********************
		 *         MAP         *
		 ***********************/
		if ($this->moduleManager()->isActive('map')) {
			$fields['map'] = [
				'options' => [
					'icon' => 'map',
					'title' => ucfirst($translator->__invoke('map_module_title')),
				],
				'data' => [
					'territories' => [
						'text' => ucfirst($translator->__invoke('territory_entities')),
						'help' => 'Séparation : points-virgules',
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
						],
					],
				],
				'arrays' => [
					'territoriesArray' => [
						'text' => ucfirst($translator->__invoke('territory_entities')),
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
							'columns' => [
								'text' => $translator->__invoke('columns'),
								'type' => 'multiple',
								'options' => [
									'name' => $translator->__invoke('territory_field_name'),
									'code' => $translator->__invoke('territory_field_code'),
								],
							],
						],
					],
				],
				'charts' => [
					'territoriesMap' => [
						'text' => ucfirst($translator->__invoke('territory_entities')),
						'help' => 'Carte 200x200',
						'params' => [
							'arbo' => [
								'text' => 'Arborescent',
								'type' => 'boolean',
							],
						],
					],
				],
			];
		}

		/***********************
		 *     INDICATORS      *
		 ***********************/
		if ($this->moduleManager()->isActive('indicator')) {
			$fields['indicator'] = [
				'options' => [
					'icon' => 'indicator',
					'title' => ucfirst($translator->__invoke('indicator_module_title')),
					'onlyProject' => true,
				],
				'data' => [
					'indicatorDone' => [
						'text' => $translator->__invoke('measure_type_done'),
						'params' => [
							'indicator' => [
								'text' => ucfirst($translator->__invoke('measure_field_indicator')),
								'type' => 'unique',
								'options' => [],
							],
						],
					],
					'indicatorTarget' => [
						'text' => $translator->__invoke('measure_type_target'),
						'params' => [
							'indicator' => [
								'text' => ucfirst($translator->__invoke('measure_field_indicator')),
								'type' => 'unique',
								'options' => [],
							],
						],
					],
					'indicatorRatio' => [
						'text' => 'Ratio',
						'params' => [
							'indicator' => [
								'text' => ucfirst($translator->__invoke('measure_field_indicator')),
								'type' => 'unique',
								'options' => [],
							],
						],
					],
				],
				'arrays' => [
					'indicatorRecap' => [
						'text' => 'Récapitulatif global',
						'params' => [
							'indicators' => [
								'text' => ucfirst($translator->__invoke('measure_field_indicator')),
								'type' => 'multiple',
								'options' => [],
							],
						],
					],
					'indicatorArray' => [
						'text' => 'Tableau des mesures',
						'params' => [
							'indicators' => [
								'text' => ucfirst($translator->__invoke('measure_field_indicator')),
								'type' => 'multiple',
								'options' => [],
							],
							'columns' => [
								'text' => $translator->__invoke('columns'),
								'type' => 'multiple',
								'options' => [
									'indicator' => $translator->__invoke('measure_field_indicator'),
									'period' => $translator->__invoke('measure_field_period'),
									'type' => $translator->__invoke('measure_field_type'),
									'start' => $translator->__invoke('measure_field_start'),
									'date' => $translator->__invoke('measure_field_date'),
									'comment' => $translator->__invoke('measure_field_comment'),
									'source' => $translator->__invoke('measure_field_source'),
									'value' => $translator->__invoke('measure_field_value'),
									'territories' => $translator->__invoke('measure_field_territories'),
								],
							],
						],
					],
				],
				'charts' => [
					'indicatorChart' => [
						'text' => 'Récapitulatif global',
						'params' => [
							'indicators' => [
								'text' => ucfirst($translator->__invoke('measure_field_indicator')),
								'type' => 'multiple',
								'options' => [],
							],
						],
					],
				],
			];

			$fields['indicator']['arrays']['indicatorRecap']['params']['indicators']['options'][
				'dynamic'
			] = 'Dynamique (liés à la fiche)';
			$fields['indicator']['arrays']['indicatorArray']['params']['indicators']['options'][
				'dynamic'
			] = 'Dynamique (liés à la fiche)';
			$fields['indicator']['charts']['indicatorChart']['params']['indicators']['options'][
				'dynamic'
			] = 'Dynamique (liés à la fiche)';


        	$indicators = $this->entityManager()
                ->getRepository('Indicator\Entity\Indicator')
                ->findAll();

			foreach ($indicators as $indicator) {
				$fields['indicator']['data']['indicatorDone']['params']['indicator']['options'][
					$indicator->getId()
				] = $indicator->getName();
				$fields['indicator']['data']['indicatorTarget']['params']['indicator']['options'][
					$indicator->getId()
				] = $indicator->getName();
				$fields['indicator']['data']['indicatorRatio']['params']['indicator']['options'][
					$indicator->getId()
				] = $indicator->getName();

				$fields['indicator']['arrays']['indicatorRecap']['params']['indicators']['options'][
					$indicator->getId()
				] = $indicator->getName();
				$fields['indicator']['arrays']['indicatorArray']['params']['indicators']['options'][
					$indicator->getId()
				] = $indicator->getName();

				$fields['indicator']['charts']['indicatorChart']['params']['indicators']['options'][
					$indicator->getId()
				] = $indicator->getName();
			}
		}

		/***********************
		 *     ADVANCEMENT     *
		 ***********************/
		if ($this->moduleManager()->isActive('advancement')) {
			$fields['advancement'] = [
				'options' => [
					'icon' => 'advancement',
					'title' => ucfirst($translator->__invoke('advancement_module_title')),
					'onlyProject' => true,
				],
				'data' => [],
				'arrays' => [
					'advancementArray' => [
						'text' => 'Avancement',
						'params' => [
							'columns' => [
								'text' => $translator->__invoke('columns'),
								'type' => 'multiple',
								'options' => [
									'date' => $translator->__invoke('advancement_field_date'),
									'type' => $translator->__invoke('advancement_field_type'),
									'value' => $translator->__invoke('advancement_field_value'),
									'name' => $translator->__invoke('advancement_field_name'),
									'description' => $translator->__invoke('advancement_field_description'),
								],
							],
						],
					],
				],
				'charts' => [
					'advancementChart' => [
						'text' => 'Avancement',
					],
				],
			];

			foreach (Advancement::getTypes() as $type) {
				$fields['advancement']['data']['advancement' . ucfirst($type)] = [
					'text' =>
						$translator->__invoke('advancement_type_' . $type) .
						' - ' .
						$translator->__invoke('advancement_field_value'),
					'params' => [
						'method' => [
							'text' => 'Méthode',
							'type' => 'unique',
							'options' => [
								'last' => 'Le dernier ajouté',
								'closest' => 'Le plus proche',
								'farthest' => 'Le plus lointain',
							],
						],
					],
				];

				$fields['advancement']['data']['advancement' . ucfirst($type) . 'Name'] = [
					'text' =>
						$translator->__invoke('advancement_type_' . $type) .
						' - ' .
						$translator->__invoke('advancement_field_name'),
					'params' => [
						'method' => [
							'text' => 'Méthode',
							'type' => 'unique',
							'options' => [
								'last' => 'Le dernier ajouté',
								'closest' => 'Le plus proche',
								'farthest' => 'Le plus lointain',
							],
						],
					],
				];

				$fields['advancement']['data']['advancement' . ucfirst($type) . 'Description'] = [
					'text' =>
						$translator->__invoke('advancement_type_' . $type) .
						' - ' .
						$translator->__invoke('advancement_field_description'),
					'params' => [
						'method' => [
							'text' => 'Méthode',
							'type' => 'unique',
							'options' => [
								'last' => 'Le dernier ajouté',
								'closest' => 'Le plus proche',
								'farthest' => 'Le plus lointain',
							],
						],
					],
				];

				$fields['advancement']['data']['advancement' . ucfirst($type) . 'Date'] = [
					'text' =>
						$translator->__invoke('advancement_type_' . $type) .
						' - ' .
						$translator->__invoke('advancement_field_date'),
					'params' => [
						'method' => [
							'text' => 'Méthode',
							'type' => 'unique',
							'options' => [
								'last' => 'Le dernier ajouté',
								'closest' => 'Le plus proche',
								'farthest' => 'Le plus lointain',
							],
						],
					],
				];
			}
		}

		/***********************
		 *     CONVENTIONS     *
		 ***********************/
		if ($this->moduleManager()->isActive('convention')) {
			$fields['convention'] = [
				'options' => [
					'icon' => 'convention',
					'title' => ucfirst($translator->__invoke('convention_module_title')),
					'onlyProject' => true,
				],
				'data' => [
                    'conventions' => [
                        'text' => $translator->__invoke('project_field_convention'),
                        'params' => [
                            'columns' => [
                                'text' => $translator->__invoke('columns'),
                                'type' => 'multiple',
                                'options' => [
                                    'name' => $translator->__invoke('convention_field_name'),
                                    'contractor' => $translator->__invoke('convention_field_contractor'),
                                    'percentage' => $translator->__invoke('convention_line_field_percentage'),
                                    'number' => $translator->__invoke('convention_field_number'),
                                    'numberArr' => $translator->__invoke('convention_field_numberArr'),
                                    'start' => $translator->__invoke('convention_field_start'),
                                    'end' => $translator->__invoke('convention_field_end'),
                                    'decisionDate' => $translator->__invoke('convention_field_decisionDate'),
                                    'notificationDate' => $translator->__invoke('convention_field_notificationDate'),
                                    'returnDate' => $translator->__invoke('convention_field_returnDate'),
                                    'amount' => $translator->__invoke('convention_field_amount'),
                                    'amountSubv' => $translator->__invoke('convention_field_amountSubv'),
                                    'keywords' => $translator->__invoke('convention_field_keywords'),
                                ],
                            ],
                        ],
                    ]
                ],
				'arrays' => [
					'conventionsArray' => [
						'text' => 'Convention',
						'params' => [
							'columns' => [
								'text' => $translator->__invoke('columns'),
								'type' => 'multiple',
								'options' => [
									'name' => $translator->__invoke('convention_field_name'),
									'contractor' => $translator->__invoke('convention_field_contractor'),
									'percentage' => $translator->__invoke('convention_line_field_percentage'),
									'number' => $translator->__invoke('convention_field_number'),
									'numberArr' => $translator->__invoke('convention_field_numberArr'),
									'start' => $translator->__invoke('convention_field_start'),
									'end' => $translator->__invoke('convention_field_end'),
									'decisionDate' => $translator->__invoke('convention_field_decisionDate'),
									'notificationDate' => $translator->__invoke('convention_field_notificationDate'),
									'returnDate' => $translator->__invoke('convention_field_returnDate'),
									'amount' => $translator->__invoke('convention_field_amount'),
									'amountSubv' => $translator->__invoke('convention_field_amountSubv'),
                                    'keywords' => $translator->__invoke('convention_field_keywords'),
                                ],
							],
						],
					],
				],
				'charts' => [],
			];
		}
        /*$groups = $this->allowedKeywordGroups('convention', false);
        foreach ($groups as $group) {
            $fields['convention']['data']['conventions']['params']['columns']['options']['keyword' . $group->getId()] =  $group->getName();
            $fields['convention']['arrays']['conventionArray']['params']['columns']['options']['keyword' . $group->getId()] =  $group->getName();
        }*/



		/***********************
		 *       IMAGES        *
		 ***********************/
		$fields['pictures'] = [
			'options' => [
				'icon' => 'picture',
				'title' => ucfirst($translator->__invoke('pictures_module_title')),
				'onlyProject' => true,
			],
			$translator->__invoke('pictures_module_project') => [],
			'charts' => [],
		];

		$query = $this->entityManager()->createQuery(
			'SELECT DISTINCT a.key FROM Attachment\Entity\Attachment a WHERE a.entity = :entity AND a.key != \'\''
		);
		$query->setParameter('entity', 'Project\Entity\Project');
		$keys = $query->getResult();
		foreach ($keys as $key) {
			$fields['pictures'][$translator->__invoke('pictures_module_project')][
				'picture(' . $key['key'] . ')'
			] = [
				'text' => $key['key'],
			];
		}

		return $fields;
	}

	protected function getEntityMeta($entityName)
	{
		$moduleManager = $this->serviceLocator->get('MyModuleManager');
		$entityClass = $moduleManager->getEntityAclForName($entityName);
		if (!class_exists($entityClass)) {
			return false;
		}

		$entity = new $entityClass();

		return [
			'entityName' => $entityName,
			'entityClass' => $entityClass,
			'inputFilter' => $entity->getInputFilter(
				$this->entityManager(),
				$this->authStorage()->getUser()
			),
		];
	}
}
