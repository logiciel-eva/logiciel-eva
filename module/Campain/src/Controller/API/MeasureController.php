<?php

namespace Campain\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Core\Filter\DateTimeFilter;
use DateTime;

class MeasureController extends BasicRestController
{
    protected $entityClass  = 'Campain\Entity\Measure';
    protected $repository   = 'Campain\Entity\Measure';
    protected $entityChain  = 'campain:e:campain';
    private $dateFilter;
    
    public function __construct()
    {
        $this->dateFilter = new DateTimeFilter();
    }

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false && $sort != 'ratio') {
            $sort = 'object.' . $sort;
        }
        $queryBuilder->join('object.campain', 'campain');
        $queryBuilder->join('object.indicator', 'indicator');
        $queryBuilder->join('indicator.users', 'user');
        $queryBuilder->join('object.targetMeasure', 'targetMeasure');
        $queryBuilder->join('object.realiseMeasure', 'realiseMeasure');
        $queryBuilder->join('realiseMeasure.indicator', 'realisedIndicator');
        $queryBuilder->leftJoin('realiseMeasure.groupIndicator', 'realisedgroupIndicator');
        $queryBuilder->leftJoin('targetMeasure.project', 'targetproject');
        $queryBuilder->leftJoin('realiseMeasure.project', 'realiseproject');

        if($sort == 'ratio'){
            $queryBuilder->addSelect('(realiseMeasure.value / targetMeasure.value) * 100 AS HIDDEN ratio');
        }
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'realisedIndicator.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }

        if (isset($data['filters']['territories']) && $data['filters']['territories']) {
            $territories = $data['filters']['territories'];

            $territoryRepository = $this->getEntityManager()->getRepository('Map\Entity\Territory');
            $territoriesValues = $territories['val'];
            $shouldTree = isset($territories['tree']) ? $territories['tree'] == 'true' : false;
            if ($shouldTree) {
                $territoriesValues = [];
                if (is_array($territories['val'])) {
                    foreach ($territories['val'] as $id) {
                        $territory = $territoryRepository->find($id);
                        if ($territory) {
                            $territoriesValues = array_merge($territoriesValues, $territory->getFlatTree(true));
                        }
                    }
                } else {
                    $territory = $territoryRepository->find($territories['val']);
                    if ($territory) {
                        $territoriesValues = array_merge($territoriesValues, $territory->getFlatTree(true));
                    }
                }
            }


            $queryBuilder->andWhere(
                '
                targetMeasure.id ' .
                    ($territories['op'] === 'neq' || $territories['op'] === 'isNull' ? 'NOT' : '') .
                    ' IN (SELECT terr.primary FROM Map\Entity\Association terr WHERE terr.entity = :terr_entity AND terr.primary = targetMeasure.id AND terr.territory IN (:terr_values))
            '
            );
            $queryBuilder->setParameter('terr_values', $territoriesValues);
            $queryBuilder->setParameter('terr_entity', 'Indicator\Entity\Measure');
            unset($data['filters']['territories']);
        }
    }

    public function update($id, $data)
    {
        $update = parent::update($id, $data);
        $measureCampain = $this->getEntityManager()
				->getRepository($this->repository)
				->find($id);
        if(isset($data['realiseMeasure']['territories']) && $measureCampain){
            $this->saveTerritories($measureCampain->getRealiseMeasure(), $data['realiseMeasure'], 'Indicator\Entity\Measure');
        }
        if(isset($data['targetMeasure']['territories']) && $measureCampain){
            $this->saveTerritories($measureCampain->getTargetMeasure(), $data['targetMeasure'], 'Indicator\Entity\Measure');
        }

        $campaignIndicator = $measureCampain->getIndicator();
        $indicator = $this->getEntityManager()->getRepository($campaignIndicator->getEntity())->findOneBy(['id' => $campaignIndicator->getPrimary()]);
        // update measures in master
        if ($update->getVariable('success', false) === true && $indicator->getMaster()) {
            $this->updateCampaignIndicatorMeasuresInMaster($update->getVariable('object'));
        }

        return $update;
    }

    private function updateCampaignIndicatorMeasuresInMaster($campaignMeasure)
    {
        $currentClient = $this->environment()->getClient();
        if(null === $currentClient->getNetwork())
        {
            return;
        }
        $masterClient = $currentClient->getNetwork()->getMaster();
        $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
        $masterEm = $entityManagerFactory->getEntityManager($masterClient);

        $measureRepository = $masterEm->getRepository('Indicator\Entity\Measure');

        $masterDoneMeasure = $measureRepository->findOneBy(['slave' => $campaignMeasure['realiseMeasure']['id']]);
        if(null !== $masterDoneMeasure) {
            $masterDoneMeasure->setValue($campaignMeasure['realiseMeasure']['value']);
            $masterDoneMeasure->setComment($campaignMeasure['realiseMeasure']['comment']);
            $masterDoneMeasure->setSource($campaignMeasure['realiseMeasure']['source']);
            if(array_key_exists('date', $campaignMeasure['realiseMeasure']) && null !== $campaignMeasure['realiseMeasure']['date'])
            {
                $masterDoneMeasure->setDate(DateTime::createFromFormat('d/m/Y H:i', $campaignMeasure['realiseMeasure']['date']));
            }
            if(array_key_exists('start', $campaignMeasure['realiseMeasure']) && null !== $campaignMeasure['realiseMeasure']['start'])
            {
                $masterDoneMeasure->setStart(DateTime::createFromFormat('d/m/Y H:i', $campaignMeasure['realiseMeasure']['start']));
            }
            $masterEm->persist($masterDoneMeasure);
        }
        $masterTargetMeasure = $measureRepository->findOneBy(['slave' => $campaignMeasure['targetMeasure']['id']]);
        if(null !== $masterTargetMeasure) {
            $masterTargetMeasure->setValue($campaignMeasure['targetMeasure']['value']);
            $masterTargetMeasure->setComment($campaignMeasure['targetMeasure']['comment']);
            $masterTargetMeasure->setSource($campaignMeasure['targetMeasure']['source']);
            if(array_key_exists('date', $campaignMeasure['targetMeasure']) && null !== $campaignMeasure['targetMeasure']['date'])
            {
                $masterTargetMeasure->setDate(DateTime::createFromFormat('d/m/Y H:i', $campaignMeasure['targetMeasure']['date']));
            }
            if(array_key_exists('start', $campaignMeasure['targetMeasure']) && null !== $campaignMeasure['targetMeasure']['start'])
            {
                $masterTargetMeasure->setStart(DateTime::createFromFormat('d/m/Y H:i', $campaignMeasure['targetMeasure']['start']));
            }
            $masterEm->persist($masterTargetMeasure);
        }
        $masterEm->flush();
    }

    protected function extract($object)
	{
		//$array = $this->getHydrator()->extract($object);
		$array = parent::extract($object);
        $entityClass = 'Indicator\Entity\Measure';
        $array['realiseMeasure']['territories'] = $this->getTerritories($object->getRealiseMeasure(), $entityClass);
        $array['targetMeasure']['territories'] = $this->getTerritories($object->getTargetMeasure(), $entityClass);
        return $array;
    }
    
    protected function processBodyContent($request){
        $data = parent::processBodyContent($request);
        $data = $this->applyDateFilter($data,'campain', 'startDate');
        $data = $this->applyDateFilter($data,'campain', 'endDate');
        $data = $this->applyDateFilter($data,'realiseMeasure', 'start');
        $data = $this->applyDateFilter($data,'realiseMeasure', 'date');
        $data = $this->applyDateFilter($data,'realiseMeasure', 'updatedAt');
        $data = $this->applyDateFilter($data,'targetMeasure', 'start');
        $data = $this->applyDateFilter($data,'targetMeasure', 'date');
        $data = $this->applyDateFilter($data,'targetMeasure', 'updatedAt');
        return $data;
    }

    private function applyDateFilter($data, $measurekey, $datekey ){
        if(isset($data[$measurekey][$datekey])){
            $data[$measurekey][$datekey] = $this->dateFilter->filter($data[$measurekey][$datekey]);
        }
        return $data;
    }

    public function delete($id)
    {
        // to prepare deletion in master
        $measureCampain = $this->getEntityManager()->getRepository($this->repository)->find($id);
        $campaignIndicator = $measureCampain->getIndicator();
        $indicator = $this->getEntityManager()->getRepository($campaignIndicator->getEntity())->findOneBy(['id' => $campaignIndicator->getPrimary()]);
        $measureDoneId = $measureCampain->getRealiseMeasure()->getId();
        $measureTargetId = $measureCampain->getTargetMeasure()->getId();

        $response = parent::delete($id);
        
        // delete measures in master
        if ($response->getVariable('success', false) === true && $indicator->getMaster()) {
            $this->deleteCampaignIndicatorMeasuresInMaster($measureDoneId, $measureTargetId);
        }

        return $response;
    }

    private function deleteCampaignIndicatorMeasuresInMaster($measureDoneId, $measureTargetId)
    {
        $currentClient = $this->environment()->getClient();
        if(null === $currentClient->getNetwork())
        {
            return;
        }
        $masterClient = $currentClient->getNetwork()->getMaster();
        $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
        $masterEm = $entityManagerFactory->getEntityManager($masterClient);

        $measureRepository = $masterEm->getRepository('Indicator\Entity\Measure');

        $masterDoneMeasure = $measureRepository->findOneBy(['slave' => $measureDoneId]);
        if(null !== $masterDoneMeasure) {
            $masterEm->remove($masterDoneMeasure);
        }
        $masterTargetMeasure = $measureRepository->findOneBy(['slave' => $measureTargetId]);
        if(null !== $masterTargetMeasure) {
            $masterEm->remove($masterTargetMeasure);
        }
        $masterEm->flush();
    }

}
