<?php

namespace Project\Service;

use User\Auth\AuthStorage;
use Project\Entity\Project;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Annotations\AnnotationReader;

class ProjectFieldDescriptorService
{
  private $entityManager;
  private $authStorage;


  /**
   * @param EntityManager entityManager
   * @param AuthStorage authStorage
   */
  public function __construct($entityManager, $authStorage)
  {
    $this->entityManager = $entityManager;
    $this->authStorage = $authStorage;
  }

  public function getAllFieldsKeys() {
      $em = $this->entityManager;
      $user = $this->authStorage->getUser();

      // TODO : getInputFilter should be in this service, not the project Entity
      $inputs = (new Project)->getInputFilter($em, $user)->getValues();

      if(!isset($inputs)) {
        $inputs = [];   // to avoid null issues with PHP functions
      }

      return array_keys($inputs);
  }

  public function getTextareaKeys() {
      $reflect = new \ReflectionClass(new Project);
      $annotationReader = new AnnotationReader();
      $fieldKeys = $this->getAllFieldsKeys();

      $textareaKeys = array_filter($fieldKeys, function ($key) use ($reflect, $annotationReader) {
          if ('publicationStatus' === $key) {
              // this is badly specific, but this property's column should have been defined as varchar in the first place
              return false;
          }

          try {
              $prop = $reflect->getProperty($key);
              $propInfo = $annotationReader->getPropertyAnnotations($prop);

              return isset($propInfo[0]->type)
                          ? 'text' === $propInfo[0]->type
                          : false;

          } catch (\Exception $e) {
              return false;
          }
      });

      return $textareaKeys;
  }
}
