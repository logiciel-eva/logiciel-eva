(function(app) {

    app.controller('groupIndicatorListController', [
        'groupIndicatorService', '$scope', 'translator',
        function groupIndicatorListController(groupIndicatorService, $scope, translator) {
            var self = this;

            self.options = {
                name: 'groupindicators-list',
                col: [
                    'id',
                    'name',
                    'operator',
                    'indicators.id',
                    'indicators.name',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    groupIndicatorService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };
            
            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(groupindicator) {
                self.editedLineErrors = {};
                self.editedLine       = groupindicator;
                self.editedLineFields = angular.copy(groupindicator);
            };
            self.getOperatorLabel = function getOperatorLabel(operator) {
                translator('indicator_operator_' + operator);
            };

            self.deleteGroup = function deleteGroup(group) {
                if (confirm(translator('group-indicator_question_delete').replace('%1', group.name))) {
                    groupIndicatorService.remove(group).then(function (res) {
                        if (res.success) {
                            self.groupindicators.splice(self.groupindicators.indexOf(group), 1);
                            flashMessenger.success(translator('group-indicator_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.saveEditedLine = function saveEditedLine(groupindicator) {
                self.editedLineErrors = {};
                console.log(groupindicator, self.editedLineFields);
                groupIndicatorService.save(angular.extend({}, groupindicator, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        groupindicator = angular.extend(groupindicator, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('group-indicator_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };
        }
    ])

})(window.EVA.app);
