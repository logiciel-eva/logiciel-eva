<?php

namespace Map\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Association
 * @package Map\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="map_territory_association")
 */
class Association extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Territory
     *
     * @ORM\ManyToOne(targetEntity="Map\Entity\Territory")
     */
    protected $territory;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $entity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="`primary`")
     */
    protected $primary;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Territory
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @param Territory $territory
     */
    public function setTerritory($territory)
    {
        $this->territory = $territory;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @param int $primary
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'entity',
                'required' => true,
            ],
            [
                'name'     => 'primary',
                'required' => true,
            ],
            [
                'name'     => 'territory',
                'required' => true,
            ],
        ]);

        return $inputFilter;
    }
}
