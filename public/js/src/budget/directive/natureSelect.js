(function(app) {
    app.directive('natureSelect', [
        'natureService',
        'selectFactoryService',
        function(natureService, selectFactoryService) {
            return selectFactoryService.create({
                service: {
                    object: natureService,
                    apiParams: {
                        col: ['id', 'code', 'name', 'parent.id'],
                    },
                    full: true,
                    tree: true,
                    treeOrder: 'code',
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['code', 'name'],
                    render: {
                        option: (data, escape) => `
                            <div class="option">
                                ${data.code ? `${escape(data.code)} -` : ''} ${escape(data.name)}
                            </div>
                        `,
                        item: (data, escape) => `
                            <div class="option">
                                ${data.code ? `${escape(data.code)} -` : ''} ${escape(data.name)}
                            </div>
                        `,
                    },
                },
            });
        },
    ]);
})(window.EVA.app);
