<?php

$time = [
    'icon'     => 'time',
    'module'   => 'time',
    'title'    => 'time_module_title',
    'right'    => 'time:e:absence-type:r',
    'href'     => 'timesheet/absence-type',
    'priority' => 2,
];

$list = [
    'icon'     => 'list',
    'right'    => 'time:e:timesheet:r',
    'title'    => 'timesheet_nav_list',
    'href'     => 'timesheet',
    'priority' => 10,
];

$weeklyAdd = [
    'icon'     => 'calendar',
    'right'    => 'time:e:timesheet:c',
    'title'    => 'timesheet_nav_weekly_add',
    'href'     => 'timesheet/weekly-add',
    'priority' => 9,
];

$synchronisation = [
    'icon'     => 'refresh',
    'right'    => 'time:e:synchronisation:r',
    'title'    => 'timesheet_nav_synchronisation',
    'href'     => 'timesheet/synchronisation',
    'priority' => 8,
];

$export = [
    'icon'    => 'excel',
    'right'   => 'time:e:export:r',
    'title'   => 'timesheet_nav_timeExport',
    'href'    => 'timesheet/export',
    'priority' => 7,
];

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'time' => $time,
                ],
            ],
            'timesheet' => [
                'icon'     => 'time',
                'right'    => 'time:e:timesheet:r',
                'title'    => 'time_module_title',
                'priority' => 1000,
                'children' => [
                    'list'            => $list,
                    'weekly-add'      => $weeklyAdd,
                    'synchronisation' => $synchronisation,
                    'export'          => $export,
                ],
            ],
        ],
        'client-menu-parc' => [
            'administration' => [
                'children' => [
                    'time' => array_replace($time, ['priority' => 97]),
                ],
            ],
            'suivi' => [
                'children' => [
                    'timesheet' => [
                        'icon'     => 'time',
                        'right'    => 'time:e:timesheet:r',
                        'title'    => 'time_module_title',
                        'priority' => 1000,
                        'children' => [
                            'list'            => $list,
                            'weekly-add'      => $weeklyAdd,
                            'synchronisation' => $synchronisation,
                            'export'          => $export
                        ],
                    ],
                ],
            ],
        ],
    ],
];
