<?php

namespace Map\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class TerritoryFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'name' => [
                'label' => $this->translator->__invoke('territory_field_name'),
                'type'  => 'string'
            ],
            'code' => [
                'label' => $this->translator->__invoke('territory_field_code'),
                'type'  => 'string'
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('indicator_field_createdAt'),
                'type'  => 'date'
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('indicator_field_updatedAt'),
                'type'  => 'date'
            ],
        ];

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('territory', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type'  => 'keyword-select',
                    'group' => $group->getId()
                ];
            }
        }

        $this->filters = $filters;
    }
}
