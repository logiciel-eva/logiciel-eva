<?php

namespace Import\Controller;

use Advancement\Entity\Advancement;
use Budget\Entity\BudgetCode;
use Convention\Entity\Line;
use Core\Controller\AbstractActionSLController;
use Directory\Entity\Structure;
use Doctrine\Laminas\Hydrator\DoctrineObject as DoctrineHydrator;
use Import\Core\CSVParser;
use Keyword\Entity\Association;
use Project\Entity\Actor;
use Project\Entity\Member;
use Keyword\Entity\Group;
use Keyword\Entity\Keyword;
use User\Entity\User;
use Laminas\Validator\InArray;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        $entityName = $this->params()->fromRoute('entity', false);
        $type = $this->params()->fromQuery('type', false);

        if (!$entityName) {
            return $this->notFoundAction();
        }
        $keyword = $entityName == 'keyword';

        $meta = $this->getEntityMeta($entityName);
        if (!$meta) {
            return $this->notFoundAction();
        }

        /* KEYWORDS */
        $groups = $this->entityManager()
            ->getRepository('Keyword\Entity\Group')
            ->findAll();
        $groupsArr = [];
        foreach ($groups as $group) {
            $groupsArr[$group->getId()] = $group->getName();
        }

        /* CUSTOM FIELDS */
        $fieldsGroups = $this->entityManager()->getRepository('Field\Entity\Group')->findAll();
        $customFields = [];
        foreach ($fieldsGroups as $fieldsGroup) {
            foreach ($fieldsGroup->getFields() as $field) {
                $customFields[$field->getId()] = $fieldsGroup->getName() . ' - ' . $field->getName();
            }
        }

        return new ViewModel([
            'entityName'        => $meta['entityName'],
            'type'              => $type,
            'inputs'            => $meta['inputFilter']->getInputs(),
            'groups'            => $groupsArr,
            'customFields'      => $customFields,
            'isKeyword'         => $keyword
        ]);
    }

    public function parseAction()
    {
        $entityName = $this->params()->fromRoute('entity', false);
        if (!$entityName) {
            return $this->notFoundAction();
        }

        $meta = $this->getEntityMeta($entityName);
        if (!$meta) {
            return $this->notFoundAction();
        }

        $success    = false;
        $lines      = [];
        $properties = [];

        try {
            $request = $this->getRequest();
            $data    = json_decode($request->getContent(), true);

            $file = base64_decode(preg_replace('#(.*base64,)#', '', $data['file']));
            $file = mb_convert_encoding($file, 'UTF-8', mb_detect_encoding($file, 'UTF-8, ISO-8859-1, ISO-8859-15', true));

            $parser = new CSVParser($file, $data['delimiter']);
            $lines  = $parser->parse();

            $maxLength = 0;
            foreach ($lines as $line) {
                $maxLength = max($maxLength, sizeOf($line));
            }

            $lines = array_map(function ($line) use ($maxLength) {
                return array_pad($line, $maxLength, null);
            }, $lines);

            $properties = array_fill(0, $maxLength, null);

            if ($data['firstLine']) {
                $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
                $translator          = $helperPluginManager->get('translate');

                /* CUSTOM FIELDS */
                $fieldsGroups = $this->entityManager()->getRepository('Field\Entity\Group')->findAll();
                $customFields = [];
                foreach ($fieldsGroups as $fieldsGroup) {
                    foreach ($fieldsGroup->getFields() as $field) {
                        $customFields[$fieldsGroup->getName() . ' - ' . $field->getName()] = $field->getId();
                    }
                }

                $firstLine = $lines[0];
                foreach ($firstLine as $i => $header) {
                    foreach ($meta['inputFilter']->getInputs() as $input) {
                        if (
                            $input->getName() == $header
                            || $translator->__invoke($meta['entityName'] . '_field_' . $input->getName()) == $header
                        ) {
                            $properties[$i] = $input->getName();
                        } elseif (array_key_exists($header, $customFields)) {
                            $properties[$i] = 'custom_fields_' . $customFields[$header];
                        } else {
                            $groups = $this->allowedKeywordGroups($meta['entityName'], false, [
                                'name' => $header
                            ]);
                            if ($groups) {
                                $properties[$i] = 'keywords_' . $groups[0]->getId();
                            }
                        }
                    }
                }

                array_splice($lines, 0, 1);
            }

            $success = true;
        } catch (\Exception $e) {
        } finally {
            return new JsonModel([
                'success'    => $success,
                'lines'      => $lines,
                'properties' => $properties,
            ]);
        }
    }

    public function validateAction()
    {
        $entityName = $this->params()->fromRoute('entity', false);
        if (!$entityName) {
            return $this->notFoundAction();
        }

        $meta = $this->getEntityMeta($entityName);
        if (!$meta) {
            return $this->notFoundAction();
        }
        $inputFilter = $meta['inputFilter'];

        $success = true;
        $errors  = [];

        $lines = $this->parseLines($meta);

        $post = json_decode($this->getRequest()->getContent(), true);
        foreach ($lines as $kl => $line) {
            //@TODO  Hack
            foreach ($line as $p => &$l) {
                if(strpos($p,'keywords_')  === 0) {
                    if (is_array($l) && isset($l[0]) && is_object($l[0])) {
                        $l = (method_exists($l[0], 'getId')) ? $l[0]->getId() : null;
                    } else {
                        $l = $post['lines'][$kl][array_search($p,$post['properties'])];
                    }
                } else if($p === 'managers' || $p === 'validators' || $p === 'members') {
                    if(is_array($l) && empty($l)) {
                        $l = $post['lines'][$kl][array_search($p,$post['properties'])];
                    }
                } else if($p === 'territories') {
                    if(is_array($l) && empty($l)) {
                        $l = $post['lines'][$kl][array_search($p,$post['properties'])];
                    }
                }
            }

            $inputFilter->setData($line);
            if ($inputFilter->isValid()) {
                $errors[] = null;
            } else {
                $success  = false;
                $errors[] = $inputFilter->getMessages();
            }
        }

        $translator = $this->serviceLocator->get('ViewHelperManager')->get('translate');
        foreach ($errors as $key => $errorLine) {
            if ($errorLine !== NULL) {
                foreach ($errorLine as $key2 => $value) {
                    if (array_key_exists('isEmpty', $value)) {
                        $errors[$key][$key2]['isEmpty'] = $translator('value_empty_exception');
                    }
                    if (array_key_exists('notInArray', $value)) {
                        $errors[$key][$key2]['notInArray'] = $translator('not_in_array_exception');
                    }
                    if (array_key_exists('callbackValue', $value)) {
                        $errors[$key][$key2]['callbackValue'] = $translator($value['callbackValue']);
                    }

                    $errors[$key][$key2]['__translate_field'] = $translator($entityName . '_field_' . $key2);
                }
            }
        }


        return new JsonModel([
            'success' => $success,
            'errors'  => $errors,
        ]);
    }

    protected function saveAction()
    {
        $em = $this->entityManager();

        $entityName = $this->params()->fromRoute('entity', false);
        if (!$entityName) {
            return $this->notFoundAction();
        }

        $meta = $this->getEntityMeta($entityName);
        if (!$meta) {
            return $this->notFoundAction();
        }
        $inputFilter = $meta['inputFilter'];
        $entityClass = $meta['entityClass'];

        $hydrator = new DoctrineHydrator($em);

        $success    = true;
        $errors     = [];
        $exceptions = [];

        $lines = $this->parseLines($meta);
        
        foreach ($lines as $line) {           
            $inputFilter->setData($line);
            if ($inputFilter->isValid()) {

                $object = new $entityClass();
                $values = $inputFilter->getValues();
                $hydrator->hydrate($values, $object);

                if ($entityName == 'convention' && isset($line['members'])) {
                    $object->setMembers([]);
                }
                if($entityName == 'measure' && isset($line['client'])){
                    foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                        if($client->getId() == $line['client']){
                            $object->setClient(array('id' => $client->getId(), 'name' => $client->getName()));
                        }
                    }
                }

                $em->persist($object);
                $errors[] = null;

                $exception = null;

                try {
                    $em->flush();

                    foreach ($line as $property => $value) {
                        if (preg_match('/^keywords_/', $property) && is_array($value)) {
                            foreach ($value as $keyword) {
                                $keywordAssociation = new \Keyword\Entity\Association();
                                $keywordAssociation->setKeyword($keyword);
                                $keywordAssociation->setEntity($entityClass);
                                $keywordAssociation->setPrimary($object->getId());
                                $em->persist($keywordAssociation);
                            }
                        } elseif (preg_match('/^custom_fields_/', $property)) {
                            $fieldId = intval(str_replace('custom_fields_', '', $property));
                            $field = $em->getRepository('Field\Entity\Field')->findOneBy(['id' => $fieldId]);
                            if (isset($field) && !empty($value)) {
                                $fieldValue = new \Field\Entity\Value();
                                $fieldValue->setEntity($entityClass);
                                $fieldValue->setPrimary($object->getId());
                                $fieldValue->setField($field);
                                $fieldValue->setValue($value);
                                $em->persist($fieldValue);
                            }
                        } else if (preg_match('/^wkf_fields_/', $property)) {
                            $fieldId = intval(str_replace('wkf_fields_', '', $property));
                            $field = $em->getRepository('Workflow\Entity\Field')->findOneBy(['id' => $fieldId]);
                            if (isset($field) && !empty($value)) {
                                $stage = $field->getStage();
                                if (array_key_exists($stage->getId(), $workflowsInstances)) {
                                    $instance = $workflowsInstances[$stage->getId()];
                                } else {
                                    $instance = new \Workflow\Entity\Instance();
                                    $instance->setEntity($entityClass);
                                    $instance->setPrimary($object->getId());
                                    $instance->setActive(true);
                                    $instance->setStage($field->getStage());
                                    $em->persist($instance);

                                    $workflowsInstances[$stage->getId()] = $instance;
                                }
                                $fieldValue = new \Workflow\Entity\Value();
                                $fieldValue->setInstance($instance);
                                $fieldValue->setField($field);

                                if($field->isMultiValues()) {
                                    $values = explode('--', $value);
                                    $fieldValue->setValue(json_encode($values));
                                } else {
                                    $fieldValue->setValue($value);
                                }
                                $em->persist($fieldValue);
                            }
                        } else if (preg_match('/^polluted/', $property)) {
                            $values = explode('--', $value);
                            foreach ($values as $val) {
                                $query = $this->entityManager()->createQuery();
                                $query->setDQL('SELECT t FROM Polluted\Entity\Polluted t
                                                WHERE t.id = :value
                                                OR t.name = :value');
                                $query->setParameter('value', trim($val));
                                try {
                                    $pollutedLine = new \Polluted\Entity\Line();
                                    $pollutedLine->setRoot(1);
                                    $pollutedLine->setPolluted($query->getOneOrNullResult());

                                    $rootParent = $object->getRootParent();
                                    if (null !== $rootParent) {
                                        $pollutedLine->setProject($rootParent);
                                    } else {
                                        $pollutedLine->setProject($object);
                                    }
                                    $em->persist($pollutedLine);
                                } catch (\Exception $e) {
                                }
                            }
                        }

                        if($entityName == 'measure' && $property == 'client'){
                            $controllerManager   = $this->serviceLocator->get('ControllerManager');
                            $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');
                            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                                if ($client->getId() == $line['client']){
                                    $controllerMeasure->setClient($client);
                                    $clientEM = $controllerMeasure->getEntityManager();
                                    $indicator = $clientEM->getRepository('Indicator\Entity\Indicator')
                                    ->findOneBy([
                                        'master' => $line['indicator']
                                    ]);
                                    file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . "found indicator - ". $indicator->getId() . PHP_EOL, FILE_APPEND);
                                    if($indicator){
                                        $childObject = new $entityClass();
                                        $hydrator->hydrate($values, $childObject);
                                        $childObject->setMaster($object->getId());
                                        $childObject->setProject(null);
                                        $childObject->setClient(null);
                                        $childObject->setIndicator($indicator);
                                        try {
                                            $clientEM->persist($childObject);
                                            $clientEM->flush();
                                        } catch (\Exception $e) {
                                        }
                                    }
                                
                                }
                            }   
                        }
                    }

                    if (isset($line['territories']) && is_array($line['territories'])) {
                        foreach ($line['territories'] as $territory) {
                            $territoryAssociation = new \Map\Entity\Association();
                            $territoryAssociation->setTerritory($territory);
                            $territoryAssociation->setEntity($entityClass);
                            $territoryAssociation->setPrimary($object->getId());
                            $em->persist($territoryAssociation);
                        }
                    }
                    if ($entityName == 'project' && isset($line['team_members'])) {
                        if (strlen($line['team_members']) > 0) {
                            foreach (explode(" -- ", $line['team_members']) as $val) {
                                if (strpos($val, ',') !== false) {
                                    $name = explode(",", trim($val))[0];
                                    $roleName = explode(",", trim($val))[1];
                                } else {
                                    $name = $val;
                                    $roleName = null;
                                }
                                $queryBuilder = $em->createQueryBuilder();
                                $queryBuilder
                                    ->select('u')
                                    ->from(User::class, 'u')
                                    ->where('LOWER(u.name) LIKE LOWER(:name)')
                                    ->setParameters([
                                        'name' => $name,
                                    ]);
                                $user = $queryBuilder->getQuery()->getOneOrNullResult();
                                $memberEntity = new Member();
                                $memberEntity->setProject($object);
                                $memberEntity->setUser($user);
                                $memberEntity->setRole('member');
                                $em->persist($memberEntity);
                                $em->flush();
                                if ($roleName) {
                                    $queryBuilder = $em->createQueryBuilder();

                                    $queryBuilder
                                        ->select('k')
                                        ->from(Keyword::class, 'k')
                                        ->leftJoin('k.group', 'g')
                                        ->where('LOWER(k.name) LIKE LOWER(:role)')
                                        ->andWhere('g.entities LIKE :type')
                                        ->setParameters([
                                            'role' => $roleName,
                                            'type' => '%member%',
                                        ]);
                                    $keyword = $queryBuilder->getQuery()->getOneOrNullResult();
                                    $keywordAssociationEntity = new Association();
                                    $keywordAssociationEntity->setEntity('Project\Entity\Member');
                                    $keywordAssociationEntity->setPrimary($memberEntity->getId());
                                    $keywordAssociationEntity->setKeyword($keyword);
                                    $em->persist($keywordAssociationEntity);
                                }
                            }
                        }
                    }
                    if ($entityName == 'project' && isset($line['actors'])) {
                        $structureNames = [];
                        foreach (explode(" -- ", $line['actors'] ) as $val2) {
                            if (strpos($val2, ',') !== false) {
                                $structureName = explode(",", trim($val2))[0];
                                $roleName2 = explode(",", trim($val2))[1];
                            } else {
                                $structureName = $val2;
                                $roleName2 = null;
                            }
                            if (!isset($structureNames[$structureName])) {
                                $queryBuilder = $em->createQueryBuilder();
                                $queryBuilder
                                    ->select('u')
                                    ->from(Structure::class, 'u')
                                    ->where('LOWER(u.name) LIKE LOWER(:name)')
                                    ->setParameters([
                                        'name' => $structureName,
                                    ]);
                                $structure = $queryBuilder->getQuery()->getOneOrNullResult();
                                $actorEntity = new Actor();
                                $actorEntity->setProject($object);
                                $actorEntity->setStructure($structure);
                                $em->persist($actorEntity);
                                $em->flush();
                                $structureNames[$structureName] = $actorEntity;
                            }
                            if ($roleName2) {
                                $queryBuilder = $em->createQueryBuilder();
                                $queryBuilder
                                    ->select('k')
                                    ->from(Keyword::class, 'k')
                                    ->leftJoin('k.group', 'g')
                                    ->where('LOWER(k.name) LIKE LOWER(:role)')
                                    ->andWhere('g.entities LIKE :type')
                                    ->setParameters([
                                        'role' => $roleName2,
                                        'type' => '%actor%',
                                    ]);
                                $keyword = $queryBuilder->getQuery()->getOneOrNullResult();
                                $keywordAssociationEntity = new Association();
                                $keywordAssociationEntity->setEntity('Project\Entity\Actor');
                                $keywordAssociationEntity->setPrimary($structureNames[$structureName]->getId());
                                $keywordAssociationEntity->setKeyword($keyword);
                                $em->persist($keywordAssociationEntity);
                            }
                        }
                    }

                    if ($entityName == 'convention' && isset($line['projects'])) {
                        if (strlen($line['projects']) > 0) {
                            foreach (explode(" -- ", $line['projects']) as $id) {
                                $project = $em->getRepository('Project\Entity\Project')->find($id);
                                $newLine = new Line();
                                $newLine->setConvention($object);
                                $newLine->setProject($project);
                                $newLine->setPercentage(100);
                                $em->persist($newLine);
                            }
                            $em->flush();
                        }
                    }

                    if ($entityName == 'convention' && isset($line['contractor'])) {
                        $contractor = null;
                        if (is_numeric($line['contractor'])) {
                            $contractor = $em->getRepository('Directory\Entity\Structure')->find($line['contractor']);
                        }
                        else if (is_string($line['contractor'])) {
                            $contractor = $em->getRepository('Directory\Entity\Structure')->findBy(array('name' => $line['contractor']))[0];
                        }
                        $object->setContractor($contractor);
                        $em->persist($object);
                        $em->flush();
                    }

                    if ($entityName == 'convention' && isset($line['members'])) {
                        if (is_array($line['members']) > 0) {
                            $members = [];
                            foreach ($line['members'] as $user) {
                                $members[] = $em->getRepository('User\Entity\User')->find($user['user']);
                            }
                            $object->setMembers($members);
                            $em->persist($object);
                            $em->flush();
                        }
                    }

                    if ($entityName == 'contactJobStructure' && isset($line['contact'])) {
                        $contact = null;
                        if (is_numeric($line['contact'])) {
                            $contact = $em->getRepository('Directory\Entity\Contact')->find($line['contact']);
                        }
                        else if (is_string($line['contact'])) {
                            $contact = $em->getRepository('Directory\Entity\Contact')->findOneBy(array('name' => $line['contact']));
                        }
                        $object->setContact($contact);
                        $em->persist($object);
                    }

                    if ($entityName == 'contactJobStructure' && isset($line['structure'])) {
                        $structure = null;
                        if (is_numeric($line['structure'])) {
                            $structure = $em->getRepository('Directory\Entity\Structure')->find($line['structure']);
                        }
                        else if (is_string($line['structure'])) {
                            $structure = $em->getRepository('Directory\Entity\Structure')->findOneBy(array('name' => $line['structure']));
                        }
                        $object->setStructure($structure);
                        $em->persist($object);
                    }

                    if ($entityName == 'contactJobStructure' && isset($line['job'])) {
                        $job = null;
                        if (is_numeric($line['job'])) {
                            $job = $em->getRepository('Directory\Entity\Job')->find($line['job']);
                        }
                        else if (is_string($line['job'])) {
                            $job = $em->getRepository('Directory\Entity\Job')->findOneBy(array('name' => $line['job']));
                        }
                        $object->setJob($job);
                        $em->persist($object);
                    }

                    if ($entityName == 'timesheet' && isset($line['project'])) {
                        $project = null;
                        if (is_numeric($line['project'])) {
                            $project = $em->getRepository('Project\Entity\Project')->find($line['project']);
                        }
                        else if (is_string($line['project'])) {
                            $project = $em->getRepository('Project\Entity\Project')->findBy(array('name' => $line['project']))[0];
                        }
                        $object->setProject($project);
                        $em->persist($object);
                        $em->flush();
                    }

                    if ($entityName == 'project' && isset($line['advancement'])) {
                        $date = str_replace(' ', '', explode(",", trim($line['advancement']))[0]);
                        $percentage = str_replace(' ', '', explode(",", trim($line['advancement']))[1]);
                        $percentNumber = (int)filter_var($percentage, FILTER_SANITIZE_NUMBER_INT);
                        $advancementEntity = new Advancement();
                        $advancementEntity->setProject($object);
                        $advancementEntity->setDate(new \DateTime($date));
                        $advancementEntity->setValue($percentNumber);
                        $advancementEntity->setType('done');
                        $em->persist($advancementEntity);
                    }

                    $em->flush();
                } catch (\Exception $e) {
                    $exception = $e->getMessage();
                    $em->clear($entityClass);
                    $em      = $em->create(
                        $em->getConnection(),
                        $em->getConfiguration()
                    );
                    $success = false;
                } finally {
                    $exceptions[] = $exception;
                }
            } else {
                $success      = false;
                $errors[]     = $inputFilter->getMessages();
                $exceptions[] = null;
            }
        }


        return new JsonModel([
            'success'    => $success,
            'errors'     => $errors,
            'exceptions' => $exceptions,
        ]);
    }

    /**
     * @return JsonModel|ViewModel
     * This method transform the keywords tree defined in the csv file into the data relational model and save it.
     */
    public function saveKeywordsAction ()
    {
        $em = $this->entityManager();


        $post = json_decode($this->getRequest()->getContent(), true);
        $type = $post['type'];

        $entityName = $this->params()->fromRoute('entity', false);

        if (!$entityName) {
            return $this->notFoundAction();
        }

        $meta = $this->getEntityMeta($entityName);
        if (!$meta) {
            return $this->notFoundAction();
        }
        $success = true;
        $errors = [];
        $exceptions = [];

        $groups = $this->parseGroups($meta);

        if ($type == 'keyword') {
            foreach ($groups as $group) {
                $groupEntity = new Group();
                $groupEntity->setType($type);
                $groupEntity->setName($group['name']);
                $groupEntity->setArchived(false);
                $groupEntity->setMultiple(true);
                $em->persist($groupEntity);
                $em->flush();
                if ($group['keywords']) {
                    foreach ($group['keywords'] as $key => $keyword) {
                        $keywordEntity = new Keyword();
                        $keywordEntity->setArchived(false);
                        $keywordEntity->setGroup($groupEntity);
                        $keywordEntity->setName($keyword['name']);
                        $keywordEntity->setOrder($key);
                        $em->persist($keywordEntity);
                        $this->keywordRecursive($keyword, $keywordEntity, $groupEntity, $em);
                    }
                    $em->flush();
                }
            }
        } else {
            foreach ($groups as $group) {
                $groupEntity = new Group();
                $groupEntity->setType($type);
                $groupEntity->setName($group['name']);
                $groupEntity->setArchived(false);
                $groupEntity->setMultiple(true);
                $groupEntity->setEntities(['project','indicator']);
                $em->persist($groupEntity);
                $em->flush();
                if ($group['keywords']) {
                    foreach ($group['keywords'] as $key => $keyword) {
                        $keywordEntity = new Keyword();
                        $keywordEntity->setArchived(false);
                        $keywordEntity->setGroup($groupEntity);
                        $keywordEntity->setName($keyword['name']);
                        $keywordEntity->setOrder($key);
                        $em->persist($keywordEntity);
                        $this->keywordRecursive($keyword, $keywordEntity, $groupEntity, $em);
                    }
                    $em->flush();
                }
            }
        }

        return new JsonModel([
            'success'    => $success,
            'errors'     => $errors,
            'exceptions' => $exceptions,
        ]);
    }

    /**
     * Recursive function to map tree to SQL model
     * @param $parent
     * @param $parentEntity
     * @param $groupEntity
     * @param $em
     * @return void
     */
    protected function keywordRecursive($parent, $parentEntity, $groupEntity, $em) {
        foreach ($parent['children'] as $childKey => $child) {
            if(isset($child['name'])) {
                $childEntity = new Keyword();
                $childEntity->setGroup($groupEntity);
                $childEntity->setArchived(false);
                $childEntity->setName($child['name']);
                $childEntity->setOrder($childKey);
                $em->persist($childEntity);
                $em->flush();
                $parentEntity->getChildren()->add($childEntity);
                if (count($child['children']) > 0) {
                    $this->keywordRecursive($child, $childEntity, $groupEntity, $em);
                }
            }
        }
    }

    protected function parseGroups($meta)
    {
        $post       = json_decode($this->getRequest()->getContent(), true);
        $properties = $post['properties'];
        $groups      = $post['groups'];

        $this->haystackControl($properties, $groups, $meta);

        $this->relationsControl($properties, $groups, $meta);

        return $groups;
    }


    protected function parseLines($meta)
    {
        $post       = json_decode($this->getRequest()->getContent(), true);
        $properties = $post['properties'];
        $lines      = $post['lines'];
        $entityName  = $meta['entityName'];

        foreach ($lines as $i => $line) {
            if ($entityName === 'timesheet'){
                $hoursIndex = array_search('hours',$properties);
                if(isset($line[$hoursIndex])) {
                    $line[$hoursIndex] = str_replace(',', '.', $line[$hoursIndex]);
			    }    
            }

            $lines[$i] = array_combine($properties, $line);
        }

        $this->haystackControl($properties, $lines, $meta);

        $this->relationsControl($properties, $lines, $meta);

        return $lines;
    }

    protected function getEntityMeta($entityName)
    {
        $moduleManager = $this->moduleManager();
        $entityClass   = $moduleManager->getEntityAclForName($entityName);
        if (!class_exists($entityClass)) {
            return false;
        }

        $aclString = $moduleManager->getAclEntityStringForName($entityName);
        if (!$this->acl()->isAllowed($aclString . ':c')) {
            return false;
        }

        $entity = new $entityClass();

        $inputFilter = $entity->getInputFilter($this->entityManager(), $this->authStorage()->getUser());

        if ($moduleManager->hasKeywords($entityClass)) {

            $groups = $this->entityManager()->getRepository('Keyword\Entity\Group')->findByEntity($entityName, false);

            foreach ($groups as $group) {
                $inputFilter->add([
                    'name'     => 'keywords_' . $group->getId(),
                    'group'    => $group,
                    'required' => false,
                    'validators' => [
                        [
                            'name' => 'Callback',
                            'options' => [
                                'callback' => function ($value) {
                                    return (is_int($value) ||
                                        ( is_array($value) &&
                                        isset($value[0]) &&
                                        is_object($value[0]) &&
                                        method_exists($value[0], 'getId') &&
                                        is_numeric($value[0]->getId())));
                                },
                                'message' => 'import_message_keywords_invalid',
                            ],
                        ],
                    ],
                ]);
            }
        }

        if (in_array($entityClass, [
            'Project\Entity\Project'
        ])) {
            $groups = $this->entityManager()->getRepository('Field\Entity\Group')->findByEntity($entityClass);
            foreach ($groups as $group) {
                foreach ($group->getFields() as $field) {
                    $inputFilter->add([
                        'name' => 'custom_fields_' . $field->getId(),
                        'group' => $group,
                        'required' => false
                    ]);
                }
            }
        }

        if (in_array($entityClass, [
            'Project\Entity\Project',
            'Time\Entity\Timesheet',
            'Indicator\Entity\Measure',
            'Convention\Entity\Convention',
        ])) {
            $inputFilter->add([
                'name'     => 'territories',
                'required' => false,
            ]);
        }
        if (in_array($entityClass, [
            'Indicator\Entity\Measure'
        ])) {
            $inputFilter->remove('period');
            $inputFilter->remove('campain');
            $inputFilter->remove('transverse');
            if(!$this->environment()->getClient()->isMaster()){
                    $inputFilter->remove('client');
                    $inputFilter->remove('slave');
            }

            $inputFilter->add([
                    'name'     => 'project_name',
                    'required' => false,
                    'validators' => [
                        [
                            'name' => 'Callback',
                            'options' => [
                            'callback' => function ($value)  {
                                $project = $this->entityManager()->getRepository('Project\Entity\Project')->findOneByName($value);
								return $project !== null;
							},
							'message' => 'measure_field_project_error_non_exists'
                            ],
                        ],
                    ],
                ]);
                $inputFilter->add([
                    'name'     => 'project_code',
                    'required' => false,
                    'validators' => [
                        [
                            'name' => 'Callback',
                            'options' => [
                            'callback' => function ($value)  {
                                $project = $this->entityManager()->getRepository('Project\Entity\Project')->findOneByCode($value);
                                    if (!$project) {
							           $budgetCode = $this->entityManager()
                                        ->getRepository(BudgetCode::class)
                                        ->findOneBy(['name' => $value]);

							            if ($budgetCode) {
								            $project = $budgetCode->getProject();
							            }
                                }
								return $project !== null;
							},
							'message' => 'measure_field_project_error_non_exists'
                            ],
                        ],
                    ],
                ]);
                $inputFilter->add([
                    'name'     => 'project_id',
                    'required' => false,
                    'validators' => [
                        [
                            'name' => 'Callback',
                            'options' => [
                            'callback' => function ($value)  {
                                $project = $this->entityManager()->getRepository('Project\Entity\Project')->find($value);
								return $project !== null;
							},
							'message' => 'measure_field_project_error_non_exists'
                            ],
                        ],
                    ],
                ]);
            
        }
        return [
            'entityName'  => $entityName,
            'entityClass' => $entityClass,
            'inputFilter' => $inputFilter,
        ];
    }

    protected function haystackControl($properties, &$lines, $meta)
    {
        $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
        $translator          = $helperPluginManager->get('translate');

        $inputFilter = $meta['inputFilter'];
        $entityName  = $meta['entityName'];

        $propertiesToTranslate = [];
        foreach ($properties as $property) {
            if ($property) {
                if ($inputFilter->has($property)) {
                    $input      = $inputFilter->get($property);
                    $validators = $input->getValidatorChain()->getValidators();
                    foreach ($validators as $validator) {
                        $instance = $validator['instance'];
                        if ($instance instanceof InArray) {
                            $propertiesToTranslate[$property] = $instance->getHaystack();
                        }
                    }
                }
            }
        }

        if ($propertiesToTranslate) {
            foreach ($lines as $i => $line) {
                foreach ($propertiesToTranslate as $property => $haystack) {
                    foreach ($haystack as $value) {
                        if ($line[$property] == $translator->__invoke($entityName . '_' . $property . '_' . $value)) {
                            $lines[$i][$property] = $value;
                        }
                    }
                }
            }
        }
    }

    protected function relationsControl($properties, &$lines, $meta)
    {
        $relations = [
            'project_id',
            'project_code',
            'project_name',
            'project',
            'user',
            'role',
            'territories',
            'managers',
            'validators',
            'members',
            'parent',
            'indicator',
            'client'
        ];

        $inputFilter = $meta['inputFilter'];

        foreach ($properties as $property) {
            if ($property) {
                if (
                    $inputFilter->has($property)
                    && (in_array($property, $relations)
                        || preg_match('/^keywords_/', $property))
                ) {
                    foreach ($lines as $i => $line) {
                        $value            = $line[$property];
                        $row              = null;
                        $propertyToUpdate = $property;
                        switch ($property) {
                            case 'project_id': 
                                try {
                                    $row = $this->entityManager()->getRepository('Project\Entity\Project')->find($line['project_id']);
                                    $line[$property] = null;
                                    $propertyToUpdate = 'project';
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                            case 'project_code': 
                                try {
                                    $row = $this->entityManager()->getRepository('Project\Entity\Project')->findOneByCode($line['project_code']);
                                    if (!$row) {
							           $budgetCode = $this->entityManager()
                                        ->getRepository(BudgetCode::class)
                                        ->findOneBy(['name' => $line['project_code']]);

							            if ($budgetCode) {
								            $row = $budgetCode->getProject();
							            }
						            }
                                    $line[$property] = null;
                                    $propertyToUpdate = 'project';
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                            case 'project_name': 
                                try {
                                    $row = $this->entityManager()->getRepository('Project\Entity\Project')->findOneByName($line['project_name']);
                                    $line[$property] = null;
                                    $propertyToUpdate = 'project';
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                                   
                            case 'project':
                                $query = $this->entityManager()->createQuery();
                                $query->setDQL('SELECT p FROM Project\Entity\Project p
                                                WHERE p.code = :value
                                                OR p.name = :value
                                                OR CONCAT(p.code, \' - \', p.name) = :value OR p.id = :value');
                                $query->setParameter('value', $value);
                                try {
                                    $row = $query->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                            case 'parent':
                                if ($meta['entityName'] === 'project') {
                                    $query = $this->entityManager()->createQuery();
                                    $query->setDQL('SELECT p FROM Project\Entity\Project p
                                                    WHERE p.code = :value
                                                    OR p.name = :value
                                                    OR CONCAT(p.code, \' - \', p.name) = :value OR p.id = :value');
                                    $query->setParameter('value', $value);
                                    try {
                                        $row = $query->getOneOrNullResult();
                                    } catch (\Exception $e) {
                                        $row = null;
                                    }
                                }
                                break;
                            case 'managers':
                            case 'validators':
                            case 'members':
                                $values = explode('--', $value);
                                $row = [];
                                $role = ($property === 'managers' ? Member::ROLE_MANAGER : ($property === 'validators' ? Member::ROLE_VALIDATOR : Member::ROLE_MEMBER));

                                foreach ($values as $val) {
                                    $query = $this->entityManager()->createQuery();
                                    $query->setDQL('SELECT u FROM User\Entity\User u
                                                WHERE u.firstName = :value
                                                OR u.lastName = :value
                                                OR u.name = :value OR u.id = :value');
                                    $query->setParameter('value', trim($val));
                                    try {
                                        $user = $query->getOneOrNullResult();
                                    } catch (\Exception $e) {
                                        $user = null;
                                    }

                                    if ($user) {
                                        $row[] = [
                                            'user' => $user->getId(),
                                            'role' => $role
                                        ];
                                    }
                                }

                                // on save quand même le resultat dans le bonne endroit afin que ca passe dans le validator.
                                if (!isset($line[$propertyToUpdate]) || !is_array($line[$propertyToUpdate])) {
                                    $lines[$i][$propertyToUpdate] = [];
                                }
                                foreach ($row as  $u) {
                                    if ($u) {
                                       $lines[$i][$propertyToUpdate][] = $u;
                                    }
                                }

                                $propertyToUpdate = 'members';
                                break;
                            case 'user':
                                $query = $this->entityManager()->createQuery();
                                $query->setDQL('SELECT u FROM User\Entity\User u
                                                WHERE u.firstName = :value
                                                OR u.lastName = :value
                                                OR u.name = :value OR u.id = :value');
                                $query->setParameter('value', $value);
                                try {
                                    $row = $query->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                            case 'role':
                                $query = $this->entityManager()->createQuery();
                                $query->setDQL('SELECT r FROM User\Entity\Role r WHERE r.id = :value OR r.name = :value');
                                $query->setParameter('value', $value);
                                try {
                                    $row = $query->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                            case 'indicator':
                                $query = $this->entityManager()->createQuery();
                                $query->setDQL('SELECT i FROM Indicator\Entity\Indicator i WHERE i.id = :value OR i.name = :value');
                                $query->setParameter('value', $value);
                                try {
                                    $row = $query->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                            case 'measure':
                                $query = $this->entityManager()->createQuery();
                                $query->setDQL('SELECT i FROM Indicator\Entity\Measure m WHERE m.id = :value');
                                $query->setParameter('value', $value);
                                try {
                                    $row = $query->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $row = null;
                                }
                                break;
                            case (preg_match('/^keywords_/', $property) ? true : false):
                                $groups = $this->allowedKeywordGroups($meta['entityName'], false, [
                                    'id' => str_replace('keywords_', '', $property)
                                ]);
                                $groupsId = '';
                                foreach ($groups as $j => $group) {
                                    if ($j !== 0) {
                                        $groupsId .= ',';
                                    }
                                    $groupsId .= $group->getId();
                                }

                                $values = explode('--', $value);
                                $row = [];
                                foreach ($values as $value) {
                                    $query = $this->entityManager()->createQuery();
                                    $query->setDQL('SELECT k FROM Keyword\Entity\Keyword k
                                                    WHERE k.group IN (:groupsId)
                                                    AND (k.id = :value
                                                      OR k.name = :value)');
                                    $query->setParameter('value', trim($value));
                                    $query->setParameter('groupsId', $groupsId);
                                    try {
                                        $row[] = $query->getOneOrNullResult();
                                    } catch (\Exception $e) {
                                        $row[] = null;
                                    }
                                }
                                break;
                            case 'territories':
                                $values = explode('--', $value);
                                $row = [];
                                foreach ($values as $value) {
                                    $query = $this->entityManager()->createQuery();
                                    $query->setDQL('SELECT t FROM Map\Entity\Territory t
                                                    WHERE t.id = :value
                                                    OR t.name = :value
                                                    OR CONCAT(t.code, \' - \', t.name) = :value');
                                    $query->setParameter('value', trim($value));
                                    try {
                                        $row[] = $query->getOneOrNullResult();
                                    } catch (\Exception $e) {
                                        $row[] = null;
                                    }
                                }
                                break;
                            case 'client':
                                foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                                    if($client->getId() == $value || $client->getName() == $value){
                                        $row = $client;
                                    }
                                }
                                break;
                        }

                        if (is_array($row)) {
                            if (!isset($lines[$i][$propertyToUpdate]) || !is_array($lines[$i][$propertyToUpdate])) {
                                $lines[$i][$propertyToUpdate] = [];
                            }
                            foreach ($row as $j => $value) {
                                if ($value) {
                                    $lines[$i][$propertyToUpdate][] = $value;
                                }
                            }
                        } else if ($row) {
                            $lines[$i][$propertyToUpdate] = $row->getId();
                        }
                    }
                }
            }
        }
    }
}
