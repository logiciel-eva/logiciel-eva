<?php
/**
 * Created by PhpStorm.
 * User: Curtis
 * Date: 07/06/2016
 * Time: 16:37
 */

namespace Project\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Project\Entity\Member;

class MemberController extends BasicRestController
{
    protected $entityClass = 'Project\Entity\Member';
    protected $repository  = 'Project\Entity\Member';
    protected $entityChain = 'project:e:project';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->join('object.project', 'project');
        $queryBuilder->join('object.user', 'user');

        $queryBuilder->orderBy('LENGTH(' . $sort . ')', $order);
        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'user.name LIKE :f_full',
                    'project.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
