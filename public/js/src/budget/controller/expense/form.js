(function(app) {

    app.controller('expenseFormController', [
        'expenseService', 'flashMessenger', 'translator', '$scope',
        function expenseFormController(expenseService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal = angular.element('#expenseFormModal');
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'poste.id',
                    'date',
                    'amount',
                    'amountHT',
                    'type',
                    'keywords',
                    'createdAt',
                    'updatedAt',
                    'tiers',
                    'anneeExercice',
                    'bordereau',
                    'mandat',
                    'complement',
                    'engagement',
                    'dateFacture',
                    'project.id',
                    'project.name',
                    'managementUnit.id',
                    'age',
                    'evolution',
                    'account.id',
                    'account.code',
                    'account.name',
                ]
            };

            self.expense = {};

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.object  = null;
            self.from    = {
                type: null,
                id: null
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id   = id;
                if (self.from.type == 'project') {
                    self.object = $scope.$parent.$parent.$parent.self[self.from.type];
                }
            };

            self.saveExpense = function saveExpense() {

                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.expense.id;

                var row = angular.copy(self.expense);
                row.poste = row.poste.id;
                expenseService.save(row, apiParams).then(function(res) {
                    /**
                    if (isCreation) {
                        self.poste.expensesArbo.push(res.object);
                    } else {

                        for(var i in self.poste.expensesArbo){
                            if(self.poste.expensesArbo[i].id == res.object.id){
                                var posteOldAmount = 'amountArbo' + self.poste.expensesArbo[i].type.toCamelCase().ucfirst();
                                self.poste[posteOldAmount] = parseFloat(self.poste[posteOldAmount]) - parseFloat(self.poste.expensesArbo[i].amount);
                                self.poste.expensesArbo[i] = res.object;
                                break;
                            }
                    }

                    var posteAmount = 'amountArbo' + res.object.type.toCamelCase().ucfirst();
                    if (typeof self.poste[posteAmount] == 'undefined') { // if the poste is newly created
                        self.poste[posteAmount] = 0;
                    }
                    self.poste[posteAmount] = parseFloat(self.poste[posteAmount]) + parseFloat(res.object.amount);
                    **/
                    var reload = [res.object.poste.id];
                    if (self.poste.id !== res.object.poste.id) {
                        reload.push(self.poste.id );
                    }
                    $scope.$emit('reload-poste-expense', reload);

                    flashMessenger.success(translator('expense_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.updateExerciseYear = function() {
                if(undefined === self.expense || null === self.expense
                    || undefined === self.expense.date || null === self.expense.date
                    || (!!self.expense.anneeExercice && '' !== self.expense.anneeExercice)) {
                    return;
                }
                try {
                    self.expense.anneeExercice = moment(self.expense.date, 'DD/MM/YYYY').format('YYYY');
                } catch(e) {
                    console.error(e);
                }
            };

            $scope.$on('create-expense', function (event, poste) {
                self.poste = poste;
                self.expense = {
                    _rights: {
                        update: true
                    },
                    amount: 0,
                    amountHT: 0,
                    date: null,
                    poste: poste,
                    project: {
                        id: self.from.id
                    }
                };

                self.errors = {
                    fields: {}
                };

                //self.panelTitle = translator('expense_nav_form');
                $modal.modal('show');
            });

            $scope.$on('edit-expense', function (event, params) {
                self.poste   = params.poste;
                self.expense = angular.copy(params.expense);
                if (self.poste.id === self.expense.poste.id) {
                    self.expense.poste = self.poste;
                }
                //self.expense._rights = angular.copy(self.poste._rights);
                //self.expense.poste = self.poste.id;

                self.errors = {
                    fields: {}
                };

                self.updateExerciseYear();

                self.panelTitle = params.expense.name;
                $modal.modal('show');
            });

            $scope.$on('duplicate-expense', function (event, params) {
                self.poste   = params.poste;
                self.expense = angular.copy(params.expense);
                self.expense.id = null;
                self.expense._rights = angular.copy(self.poste._rights);
                //self.expense.poste = self.poste.id;

                self.errors = {
                    fields: {}
                };

                self.panelTitle = params.expense.name;
                $modal.modal('show');
            });
        }
    ]);

})(window.EVA.app);
