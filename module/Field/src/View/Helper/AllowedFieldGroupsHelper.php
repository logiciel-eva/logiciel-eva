<?php

namespace Field\View\Helper;

use Field\Controller\Plugin\AllowedFieldGroups;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class AllowedFieldGroupsHelper
 *
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AllowedFieldGroupsHelper extends AbstractHelper
{
    /**
     * @var AllowedFieldGroups
     */
    protected $allowedFieldGroups;

    /**
     * @param AllowedFieldGroups $allowedFieldGroups
     */
    public function __construct(AllowedFieldGroups $allowedFieldGroups)
    {
        $this->allowedFieldGroups = $allowedFieldGroups;
    }

    public function __invoke($entity)
    {
        return $this->allowedFieldGroups->findByEntity($entity);
    }
}
