(function(app) {

    app.factory('incomeService', [
        'restFactoryService',
        function incomeService(restFactoryService) {
            return restFactoryService.create('/api/budget/income');
        }
    ])

})(window.EVA.app);
