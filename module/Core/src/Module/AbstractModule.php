<?php

namespace Core\Module;

use ReflectionClass;
use Laminas\Stdlib\ArrayUtils;

/**
 * Class AbstractModule
 *
 * An abstract Zend Module, allows us to avoid rewriting same code for autoloading and configuration loading.
 * Applications modules should extends this class.
 *
 * A module can be "functional" if it owns a specific formatted configuration.
 * See the readme file.
 *
 * @package Core\Module
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
abstract class AbstractModule
{
    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $dir;

    /**
     * @var array
     */
    protected $config;

    /**
     * We're using the ReflectionClass to store the current dir and the namespace of the file even with inheritance.
     */
    public function __construct()
    {
        $reflection = new ReflectionClass($this);

        $this->dir       = dirname($reflection->getFileName());
        $this->namespace = $reflection->getNamespaceName();
    }

    /**
     * Will loads all the files suffixed by the string ".config.php" contained in the "config" module directory
     * if the $configurable property is set to true.
     *
     * @return array
     */
    public function getConfig()
    {
        if ($this->config === null) {
            $this->config = [];
            foreach (glob($this->dir . '/../config/*.config.php') as $filename) {
                $this->config = ArrayUtils::merge($this->config, include $filename);
            }
        }

        return $this->config;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getModuleConfig()
    {
        if ($this->isFunctional()) {
            $key = $this->getKey();
            return $this->getConfig()['module'][$key];
        }

        throw new \Exception('You are trying to access a configuration of a non functional module');
    }

    /**
     * A module is functional if it contains its own configuration
     * @return bool
     */
    public function isFunctional()
    {
        $config = $this->getConfig();
        $key    = $this->getKey();

        return isset($config['module'][$key]);
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return lcfirst($this->namespace);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isActive()
    {
        return $this->getModuleConfig()['active'];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isActivable()
    {
        return ($this->isRequired() ? false : (isset($this->getModuleConfig()['activable']) ? $this->getModuleConfig()['activable'] : true));
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isRequired()
    {
        return $this->getModuleConfig()['required'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAclConfiguration()
    {
        return $this->getModuleConfig()['acl'];
    }

    /**
     * @return array|false
     * @throws \Exception
     */
    public function getModuleConfiguration()
    {
        return isset($this->getModuleConfig()['configuration']) ? $this->getModuleConfig()['configuration'] : false;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getEnvironments()
    {
        return $this->getModuleConfig()['environments'];
    }

    /**
     * @param string $environmentName
     * @return bool
     */
    public function isAvailableForEnvironment($environmentName)
    {
        return in_array($environmentName, $this->getEnvironments());
    }

    /**
     * @param $entityClass
     * @return bool
     */
    public function entityHasKeyword($entityClass)
    {
        $entitiesConfiguration = isset($this->getAclConfiguration()['entities']) ? $this->getAclConfiguration()['entities'] : [];
        foreach ($entitiesConfiguration as $configuration) {
            if ($configuration['class'] === $entityClass) {
                return isset($configuration['keywords']) ? $configuration['keywords'] : false;
            }
        }

        return false;
    }

    public function hasMenu($menuName)  {
       return isset($this->getConfig()['menu']) && isset($this->getConfig()['menu'][$menuName]);
    }

    public function getMenuConfiguration($menuName)  {
        if(!$this->hasMenu($menuName)) return [];
        $allMenuEntries = $this->getConfig()['menu'][$menuName];
        return  $this->checkControlsAndGet($allMenuEntries);
    }

    protected function checkControlsAndGet($menuEntries) {

        $filtredEntries = [];
        foreach ($menuEntries as $entryKey => $entryValue) {
            if($this->checkControl($entryValue)) {
                if(isset($entryValue['children'])) {
                   $entryValue['children'] = $this->checkControlsAndGet($entryValue['children']);
                }

                $filtredEntries[$entryKey]= $entryValue;
            }
        }
        return $filtredEntries;
   }

    protected function checkControl($menuEntry) {
        if (!isset($menuEntry['control'])) {
            return true;
        }

        foreach ($menuEntry['control'] as $conditionKey => $conditionValue) { 
            if($this->getModuleConfiguration()[$conditionKey] !== $conditionValue) {
               return false;
            }           
        }

        return true;
    }
}
