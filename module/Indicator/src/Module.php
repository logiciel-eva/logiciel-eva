<?php

namespace Indicator;

use Core\Module\AbstractModule;
use Indicator\View\Helper\GroupIndicatorFilters;
use Indicator\View\Helper\IndicatorFilters;
use Indicator\View\Helper\MeasureFilters;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'indicatorFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    return new IndicatorFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                },
                'measureFilters'   => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    $client              = $serviceManager->get('Environment')->getClient();
                    return new MeasureFilters($client, $translateViewHelper, $moduleManager, $entityManager);
                },
                'groupIndicatorFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    return new GroupIndicatorFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                }
            ],
        ];
    }
}
