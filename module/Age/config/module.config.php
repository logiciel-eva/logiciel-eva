<?php

use Budget\Entity\Expense;
use Budget\Entity\Income;

return [
	'module' => [
		'age' => [
			'environments' => ['client'],

			'active' => false,
			'required' => false,
			'activable' => false,

			'acl' => [
				'entities' => [],
			],

			'configuration' => [
				'dbType' => [
					'type'    => 'unique',
					'text'    => 'age_db_type_configuration',
					'options' => [
						[
							'value' => 'mysql',
							'text' => 'MySQL',
						],
						[
							'value' => 'sql-server',
							'text' => 'SQL Server',
						],
					],
				],
				'dbHost' => [
					'type' => 'text',
					'text' => 'age_db_host_configuration',
				],
				'dbPort' => [
					'type' => 'text',
					'text' => 'age_db_port_configuration',
				],
				'dbUser' => [
					'type' => 'text',
					'text' => 'age_db_user_configuration',
				],
				'dbPassword' => [
					'type' => 'text',
					'text' => 'age_db_password_configuration',
				],
				'dbName' => [
					'type' => 'text',
					'text' => 'age_db_name_configuration',
				],
				'portfolioActive' => [
					'type' => 'boolean',
					'text' => 'age_portfolio_active'
				],
				'masterDbHost' => [
					'type' => 'text',
					'text' => 'age_master_db_host_configuration',
				],
				'masterDbPort' => [
					'type' => 'text',
					'text' => 'age_master_db_port_configuration',
				],
				'masterDbName' => [
					'type' => 'text',
					'text' => 'age_master_db_name_configuration',
				],
				'identifier' => [
					'type' => 'text',
					'text' => 'age_identifier_configuration',
				],
				'firstYear' => [
					'type' => 'text',
					'text' => 'age_import_first_year'
				],
				'lastYear' => [
					'type' => 'text',
					'text' => 'age_import_last_year'
				],
				'allowedExpenseTypes' => [
					'type'    => 'multiple',
                    'text'    => 'age_import_allowed_expenseTypes',
                    'options' => array_map(function ($type) {
                        return [
                            'value' => $type,
                            'text'  => 'expense_type_' . $type,
                        ];
                    }, Expense::getTypes())
				],
				'allowedIncomeTypes' => [					
                    'type'    => 'multiple',
                    'text'    => 'age_import_allowed_incomeTypes',
                    'options' => array_map(function ($type) {
                        return [
                            'value' => $type,
                            'text'  => 'income_type_' . $type,
                        ];
                    }, Income::getTypes())
				],
				'apiUri' => [
					'type' => 'text',
					'text' => 'age_api_uri_configuration'
				],
				'apiAuthToken' => [
					'type' => 'text',
					'text' => 'age_api_auth_token'
				],
				'switchCode' => [
					'type' => 'boolean',
					'text' => 'age_api_switch_code'
				],
				'level' => [
					'type' => 'text',
					'text' => 'age_api_level'
				]
			],
		],
	],
	'view_manager' => [
		'template_path_stack' => [__DIR__ . '/../view'],
	],
	'translator' => [
		'translation_file_patterns' => [
			[
				'type' => 'phparray',
				'base_dir' => __DIR__ . '/../language',
				'pattern' => '%s.php',
			],
		],
	],
];
