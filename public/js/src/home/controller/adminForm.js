(function (app) {

    app.controller('homeAdminFormController', [
        'homeAdminService', 'homeContainerService', 'attachmentService', 'queryService', 'projectService', 'timesheetService', 'flashMessenger', 'translator', '$sce', 'FileUploader',
        function homeAdminFormController(homeAdminService, homeContainerService, attachmentService, queryService, projectService, timesheetService, flashMessenger, translator, $sce, FileUploader) {
            var self = this;

            var rowWidth = 0;

            var $modal = angular.element('#admin_containerFormModal');

            var apiParams = {
                col  : [
                    'id',
                    'enabled',
                    'containers.id',
                    'containers.title',
                    'containers.order',
                    'containers.type',
                    'containers.graphicType',
                    'containers.width',
                    'containers.content',
                    'containers.entity',
                    'containers.query.id',
                    'containers.query.name',
                    'containers.query.list',
                    'containers.query.filters',
                    'containers.query.columns',
                    'containers.columns'
                ],
                force: true
            };

            var containerApiParams = {
                col: [
                    'id',
                    'title',
                    'order',
                    'type',
                    'graphicType',
                    'width',
                    'content',
                    'entity',
                    'query.id',
                    'query.name',
                    'query.list',
                    'query.filters',
                    'columns'
                ]
            };

            var attachmentApiParams = {
                right : 'project:e:project',
                col   : [
                    'id',
                    'name',
                    'url'
                ],
                search: {
                    type: 'list',
                    data: {
                        filters: {
                            entity: {
                                op : 'eq',
                                val: 'Bootstrap\\Entity\\Client'
                            }
                        },
                        sort   : 'id',
                        order  : 'asc'
                    }
                }
            };

            var queryApiParams = {
                col: [
                    'id', 'name', 'filters', 'list', 'columns'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function (id, idClient) {
                homeAdminService.find(id, apiParams).then(function (res) {
                    self.home = res.object;
                });

                attachmentService.findAll(attachmentApiParams).then(function (res) {
                    self.picturesGlobal = res.rows;
                });

                self.pictureGlobal = {
                    type   : 'file',
                    entity : 'Bootstrap\\Entity\\Client',
                    primary: idClient,
                };

                queryService.findAll(queryApiParams).then(function (res) {
                    self.queries = res.rows;
                });
            };

            self.isNewLine = function (containerWidth, $index) {
                if ($index == 0) {
                    rowWidth = 0;
                }

                rowWidth += eval(containerWidth);

                if (rowWidth > 1) {
                    rowWidth = eval(containerWidth);
                    return true;
                }

                return false;
            };

            self.addContainer = function () {
                self.container = {
                    homeAdmin: {id: self.home.id},
                    order    : 0,
                    type     : null,
                    width    : null,
                    entity   : null,
                    query    : null,
                };
                $modal.modal('show');
            };

            self.editContainer = function (container) {
                self.container = angular.copy(container);
                $modal.modal('show');
            };

            self.deleteContainer = function (container) {
                if (confirm(translator('container_question_delete'))) {
                    homeContainerService.remove(container).then(function (res) {
                        if (res.success) {
                            self.home.containers.splice(self.home.containers.indexOf(container), 1);
                            flashMessenger.success(translator('container_message_deleted'));
                        } else {
                            flashMessenger.success(translator('error_occured'));
                        }
                    });
                }
            };

            self.saveContainer = function () {
                self.loading.save = true;
                var isCreation    = !self.container.id;

                homeContainerService.save(self.container, containerApiParams).then(function (res) {
                    if (isCreation) {
                        if (!angular.isArray(self.home.containers)) {
                            self.home.containers = [];
                        }

                        self.home.containers.push(res.object);
                    } else {
                        for (var i in self.home.containers) {
                            if (self.home.containers[i].id == res.object.id) {
                                self.home.containers[i] = res.object;
                            }
                        }
                    }

                    $modal.modal('hide');
                    flashMessenger.success(translator('container_message_saved'));
                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                });
            };

            self.savePictureGlobal = function () {
                self.loading.save = true;

                attachmentService.save(self.pictureGlobal, {
                    right: 'project:e:project',
                    col  : [
                        'id',
                        'name',
                        'url',
                    ]
                }).then(function (res) {
                    self.picturesGlobal.push(res.object);

                    self.pictureGlobal.file     = null;
                    self.pictureGlobal.filename = null;
                    self.pictureGlobal.name     = null;
                    self.pictureGlobal.size     = null;
                    self.pictureGlobal.type     = null;

                    flashMessenger.success(translator('picture_message_saved'));

                    self.loading.save = false;

                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                });
            };

            self.enableHome = function () {
                homeAdminService.save(self.home, apiParams).then(function (res) {
                    if (res.success) {
                        self.home = angular.extend(self.home, res.object);

                        if (self.home.enabled == true) {
                            flashMessenger.success(translator('home_message_enabled'));
                        } else {
                            flashMessenger.success(translator('home_message_disabled'));
                        }
                    }
                });
            };

            self.variablesParams    = {};
            self.configuredVariable = null;
            self.configureVariable  = function configureVariable(variable) {
                var $modal = angular.element('#admin_params_' + variable.replace(/\(.*\)/, ''));

                if ($modal.length > 0) {
                    self.configuredVariable = variable;
                    self.variablesParams    = {};

                    var params = $modal.find('input, select');
                    for (var i = 0; i < params.length; i++) {
                        var name                   = $(params[i]).attr('name');
                        self.variablesParams[name] = null;
                    }

                    $modal.modal('show');
                }
            };

            self.insertImage = function (variable) {
                var editor = self.getEditor();
                editor.insertContent('<img src="' + variable + '" class="img-responsive">');
            };

            self.getEditor = function () {
                var id = $('textarea[ui-tinymce]').attr('id');
                return tinyMCE.get(id);
            };

            self.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };

            self.uploader = new FileUploader();
            self.uploader.filters.push({
                name: 'imageChecker',
                fn  : function (item, options) {
                    return self.fileIsImage(item.name);
                }
            });

            self.reader = new FileReader();

            self.reader.onloadend = function (e) {
                self.pictureGlobal.file = e.target.result;
            };

            self.uploader.onAfterAddingFile = function (fileItem) {
                self.reader.readAsDataURL(fileItem._file);
                self.pictureGlobal.filename      = fileItem.file.name;
                self.pictureGlobal.size          = fileItem.file.size;
                self.errors.fields['attachment'] = null;
            };

            self.fileIsImage = function (filename) {
                var type = '|' + filename.slice(filename.lastIndexOf('.') + 1).toLowerCase() + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            };
        }
    ]);

})(window.EVA.app);
