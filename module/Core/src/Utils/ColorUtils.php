<?php

namespace Core\Utils;

/**
 * Class ColorUtils
 *
 * @package Core\Utils
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class ColorUtils
{
    public static function gradient($from, $to, $steps)
    {
        $fromDecimal = hexdec($from);
        $toDecimal   = hexdec($to);

        $r0 = ($fromDecimal & 0xff0000) >> 16;
        $g0 = ($fromDecimal & 0x00ff00) >> 8;
        $b0 = ($fromDecimal & 0x0000ff) >> 0;

        $r1 = ($toDecimal & 0xff0000) >> 16;
        $g1 = ($toDecimal & 0x00ff00) >> 8;
        $b1 = ($toDecimal & 0x0000ff) >> 0;

        $gradient = [];
        for ($i = 0; $i <= $steps - 1; $i++) {
            $r = self::interpolate($r0, $r1, $i, $steps - 1);
            $g = self::interpolate($g0, $g1, $i, $steps - 1);
            $b = self::interpolate($b0, $b1, $i, $steps - 1);

            $color = ((($r << 8) | $g) << 8) | $b;
            $gradient[] = str_pad(dechex($color), 6, '0', STR_PAD_LEFT);
        }

        return $gradient;
    }

    public static function interpolate($pBegin, $pEnd, $pStep, $pMax) {
        if ($pBegin < $pEnd) {
            return (($pEnd - $pBegin) * ($pStep / $pMax)) + $pBegin;
        } else {
            return (($pBegin - $pEnd) * (1 - ($pStep / $pMax))) + $pEnd;
        }
    }
}
