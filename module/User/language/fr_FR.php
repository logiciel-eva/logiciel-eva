<?php

return [
    'menu_profils_title' => 'profils',

	/*** USERS ***/
	'user_module_title' => '[[ user_entities | ucfirst ]]',

	'user_entity' => 'utilisateur',
	'user_entities' => 'utilisateurs',

	'user_nav_list' => 'Liste des {{ __tb.total }} [[ user_entities ]]',
	'user_nav_form' => 'Créer un [[ user_entity ]]',
	'user_nav_logout' => 'Quitter',

	'user_field_id' => 'Identifiant',
	'user_field_id_short' => 'ID',
	'user_field_gender' => 'Civilité',
	'user_field_firstName' => 'Prénom',
	'user_field_lastName' => 'Nom',
	'user_field_name' => 'Nom',
	'user_field_login' => 'Nom d\'utilisateur',
	'user_field_login_error_unique' => 'Ce nom d\'utilisateur est déjà pris',
	'user_field_password' => 'Mot de passe',
	'user_field_role' => '[[ role_entity | ucfirst ]]',
	'user_field_role_error_non_exists' => 'Ce [[ role_entity ]] n\'existe pas ou plus',
	'user_field_createdAt' => '[[ field_created_at_m ]]',
	'user_field_updatedAt' => '[[ field_updated_at_m ]]',
	'user_field_avatar' => 'Avatar',
	'user_field_email' => 'Adresse email',
	'user_field_color' => 'Couleur',
	'user_field_disabled' => 'Inactif',
	'user_field_services' => '[[ service_entities | ucfirst ]]',
	'user_field_resizable_table' => 'Tableau redimensionnable',
    'user_field_resizable_table_info' => 'La taille des colonnes varie au passage de la souris',

    'user_field_disabled_tooltip' => 'L\'utilisateur ne peut plus se connecter à EVA',

	'user_tab_profile' => 'Profil',
	'user_tab_custom' => 'Personnalisation',

	'user_gender_m' => 'M.',
	'user_gender_mme' => 'Mme.',
	'user_gender_neutral' => 'Non binaire',

	'user_message_saved' => '[[ user_entity | ucfirst ]] enregistré',
	'user_message_deleted' => '[[ user_entity | ucfirst ]] supprimé',
	'user_question_delete' => 'Voulez-vous réellement supprimer l\'[[ user_entity ]] %1 ?',
	'user_message_disabled_true' => 'L\'[[ user_entity ]] est inactif',
	'user_message_disabled_false' => 'L\'[[ user_entity ]] est actif',

	'user_form_login_error' => 'Les identifiants fournis n\'ont pas permis de vous authentifier.',
	'user_form_login_submit' => 'Connexion',
	'user_form_login_expired' => 'Votre session a expirée, veuillez vous reconnecter',
	'user_form_recover_submit' => 'Générer un nouveau mot de passe',
	'user_form_recover_info' =>
		'Saisissez votre nom d\'utilisateur puis validez. <br />Vous recevrez un nouveau mot de passe à l\'adresse e-mail associée à votre compte.',
	'user_form_recover_error' => 'Nom d\'utilisateur non reconnu.',
	'user_form_recover_success' => 'Votre nouveau mot de passe vous a été envoyé par e-mail',

	/*** ROLES ***/
	'role_module_title' => '[[ role_entities | ucfirst]]',

	'role_entity' => 'rôle',
	'role_entities' => 'rôles',

	'role_nav_list' => 'Liste des {{ __tb.total }} [[ role_entities ]]',
	'role_nav_form' => 'Créer un [[ role_entity ]]',

    'role_field_id' => 'Id',
    'role_field_name' => 'Nom',
	'role_field_name_error_unique' => 'Ce [[ role_entity ]] existe déjà',
	'role_field_createdAt' => '[[ field_created_at_m ]]',
	'role_field_updatedAt' => '[[ field_updated_at_m ]]',

	'role_field_rights_owner_all' => 'Tous',
	'role_field_rights_owner_own' => 'Propriétaire',
	'role_field_rights_owner_service' => '[[ service_entity | ucfirst ]]',

	'role_message_saved' => '[[ role_entity | ucfirst ]] enregistré',
	'role_message_deleted' => '[[ role_entity | ucfirst ]] supprimé',
	'role_question_delete' => 'Voulez-vous réellement supprimer le [[ role_entity ]] %1 ?',

	'right_errors' => 'Erreurs de droits',
	'right_update_denied' => 'Vous n\'avez pas les droits de modification',

	/*** OTHER ***/
	'user_nav_import' => 'Importer des [[ user_entities ]]',
];
