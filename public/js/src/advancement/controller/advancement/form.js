(function(app) {

    app.controller('advancementFormController', [
        'advancementService', 'flashMessenger', 'translator', '$scope',
        function advancementFormController(advancementService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal = angular.element('#advancementFormModal');
            var apiParams = {
                col: [
                    'id',
                    'type',
                    'date',
                    'value',
                    'name',
                    'description',
                    'createdAt',
                    'updatedAt',
                    'project.id'
                ]
            };

            self.advancement = null;
            self.from    = {
                type: null,
                id: null
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id   = id;
            };

            self.saveAdvancement = function saveAdvancement() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.advancement.id;

                advancementService.save(self.advancement, apiParams).then(function(res) {
                    if (isCreation) {
                        $scope.$parent.self.advancements.push(res.object);
                    } else {
                        for(var i in $scope.$parent.self.advancements){
                            if($scope.$parent.self.advancements[i].id == res.object.id){
                                $scope.$parent.self.advancements[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('advancement_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            $scope.$on('create-advancement', function (e, value) {
                self.advancement = {
                    date: moment().format('DD/MM/YYYY'),
                    _rights: {
                        update: true
                    }
                };

                if (value) {
                    self.advancement.value = value;
                }

                if (self.from.type == 'project') {
                    self.advancement.project = $scope.$parent.$parent.$parent.self[self.from.type].id;
                }

                self.panelTitle = translator('advancement_nav_form');
                $modal.modal('show');
            });

            $scope.$on('edit-advancement', function (event, advancement) {
                self.editedAdvancement = advancement;
                self.advancement = angular.copy(advancement);
                self.panelTitle = moment(self.advancement.date, 'DD/MM/YYYY HH:mm').format('DD/MM/YYYY');
                $modal.modal('show');
            });
        }
    ]);

})(window.EVA.app);
