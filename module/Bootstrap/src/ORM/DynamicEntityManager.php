<?php

namespace Bootstrap\ORM;

use Bootstrap\Entity\Client;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use Doctrine\DBAL\DriverManager;
use DoctrineORMModule\Service\ConfigurationFactory;
use Laminas\ServiceManager\ServiceLocatorAwareInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Syslogic\DoctrineJsonFunctions\Query\AST\Functions\Mysql as DqlFunctions;

/**
 * Class DynamicEntityManager
 *
 * Creates a Doctrine Entity Manager based on the 'orm_environment' configuration for a specific Client.
 *
 * @package Bootstrap\ORM
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class DynamicEntityManager
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @param Client $client
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(Client $client, $otherClient = false)
    {
        $factory = new ConfigurationFactory($otherClient ? 'orm_client' : 'orm_environment');
        $config  = $factory($this->serviceLocator, $otherClient ? 'orm_client' : 'orm_environment');
        $config->addCustomStringFunction(DqlFunctions\JsonExtract::FUNCTION_NAME, DqlFunctions\JsonExtract::class);
        //$config = $factory->createService($this->serviceLocator);

        $em = EntityManager::create([
            'driverClass'   => \Doctrine\DBAL\Driver\PDO\MySQL\Driver::class,
            'host'          => $client->getDbHost(),
            'port'          => $client->getDbPort(),
            'user'          => $client->getDbUser(),
            'password'      => $client->getDbPassword(),
            'dbname'        => $client->getDbName(),
            'driverOptions' => [
                1002 => 'SET NAMES utf8'
            ],
        ], $config);

        return $em;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }
}
