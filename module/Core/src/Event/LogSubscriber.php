<?php

namespace Core\Event;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\onFlushEventArgs;
use Laminas\ServiceManager\ServiceLocatorInterface;

class LogSubscriber implements EventSubscriber
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        $user = $this->serviceLocator->get('AuthStorage')->getUser();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof BasicRestEntityAbstract) {
                $entity->setCreatedAt(new \Datetime());
                $entity->setCreatedBy($user);
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof BasicRestEntityAbstract) {
                $entity->setUpdatedAt(new \Datetime());
                $entity->setUpdatedBy($user);
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }
    }

    public function getSubscribedEvents()
    {
        return [
            Events::onFlush
        ];
    }
}
