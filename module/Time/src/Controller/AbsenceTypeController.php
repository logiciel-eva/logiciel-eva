<?php

namespace Time\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

/**
 * Class AbsenceTypeController
 * @package Time\Controller
 */
class AbsenceTypeController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('time:e:absence-type:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }
}