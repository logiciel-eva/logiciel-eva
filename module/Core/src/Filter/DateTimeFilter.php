<?php

namespace Core\Filter;

use Laminas\Filter\FilterInterface;

class DateTimeFilter implements FilterInterface
{
    public function filter($value)
    {
        if ($value) {
            if ($value instanceof \DateTime) {
                $valueFiltered = $value;
            } else if ($this->isFrenchFormatWithTime($value)) {
                $valueFiltered = \DateTime::createFromFormat('d/m/Y H:i', $value);
            } elseif ($this->isFrenchFormat($value)) {
                $valueFiltered = \DateTime::createFromFormat('d/m/Y', $value);
            } else {
                try {
                    $valueFiltered = new \DateTime($value);
                } catch(\Exception $e) {
                    $valueFiltered = $value;
                }
            }
            return $valueFiltered;
        }

        return null;
    }

    public function isFrenchFormatWithTime($value)
    {
        return preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $value);
    }

    public function isFrenchFormat($value)
    {
        return preg_match('#\d{2}/\d{2}/\d{4}#', $value);
    }
}
