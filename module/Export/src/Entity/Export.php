<?php

namespace Export\Entity;

use BadMethodCallException;
use Core\Entity\BasicRestEntityAbstract;
use Core\Export\ExcelExporter;
use Core\Utils\StringUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use DOMDocument;
use Keyword\Entity\Keyword;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpWord\Element\Chart;
use PhpOffice\PhpWord\Element\Image;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Element\Text;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\Style as PhpWordStyle;
use PhpOffice\PhpWord\Style\Language;
use PhpOffice\PhpWord\TemplateProcessor;
use Project\Entity\Project;
use Laminas\Filter\Boolean;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\Factory;

/**
 * Class Export
 *
 * @package Convention\Entity
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 * @ORM\Entity
 * @ORM\Table(name="export_export")
 */
class Export extends BasicRestEntityAbstract
{
    const TYPE_WORD = 'word';
    const TYPE_EXCEL = 'excel';
    const TYPE_PUBLIPOSTAGES = 'publipostages';

    const EXPORT_REFERENCE_INITIAL = 'initial';
    const EXPORT_REFERENCE_KEYWORD = 'keyword';

    const SHOW_ARBO_NO = 'no';
    const SHOW_ARBO_INDENT = 'indent';
    const SHOW_ARBO_COLUMNS = 'columns';

    const DATA_ARBO_CODE = 'code';
    const DATA_ARBO_NAME = 'name';
    const DATA_ARBO_CODE_NAME = 'code-name';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Export\Entity\Template", mappedBy="export", cascade={"all"})
     */
    protected $templates;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $reference;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $year;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default": "word"})
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $showArbo;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $dataArbo;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $groupByKeywords;

    /**
     * @var array
     * @ORM\Column(type="json", nullable=true)
     */
    protected $groupByKeywordsTitles;

    public function __construct()
    {
        $this->templates = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @param ArrayCollection $templates
     */
    public function setTemplates($templates)
    {
        $this->templates = $templates;
    }

    /**
     * @param ArrayCollection $templates
     */
    public function addTemplates($templates)
    {
        foreach ($templates as $template) {
            $this->templates->add($template);
            $template->setExport($this);
        }
    }

    /**
     * @param ArrayCollection $templates
     */
    public function removeTemplates($templates)
    {
        foreach ($templates as $template) {
            $this->templates->removeElement($template);
            $template->setExport(null);
        }
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        if (!$year) {
            $year = null;
        }

        $this->year = $year;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getShowArbo()
    {
        return $this->showArbo;
    }

    /**
     * @param string $showArbo
     */
    public function setShowArbo($showArbo)
    {
        $this->showArbo = $showArbo;
    }

    /**
     * @return string
     */
    public function getDataArbo()
    {
        return $this->dataArbo;
    }

    /**
     * @param string $dataArbo
     */
    public function setDataArbo($dataArbo)
    {
        $this->dataArbo = $dataArbo;
    }

    /**
     * @return bool
     */
    public function isGroupByKeywords()
    {
        return $this->groupByKeywords;
    }

    /**
     * @return bool
     */
    public function getGroupByKeywords()
    {
        return $this->groupByKeywords;
    }

    /**
     * @param bool $groupByKeywords
     */
    public function setGroupByKeywords($groupByKeywords)
    {
        $this->groupByKeywords = $groupByKeywords;
    }

    /**
     * @return array
     */
    public function getGroupByKeywordsTitles()
    {
        return $this->groupByKeywordsTitles;
    }

    /**
     * @param array $groupByKeywordsTitles
     */
    public function setGroupByKeywordsTitles($groupByKeywordsTitles)
    {
        $this->groupByKeywordsTitles = $groupByKeywordsTitles;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name' => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name' => 'reference',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getReferences(),
                        ],
                    ],
                ],
            ],
            [
                'name' => 'year',
                'required' => false,
                'filters' => [
                    ['name' => 'NumberParse'],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) {
                                if (!$value) {
                                    return true;
                                }

                                return strlen(str_replace('_', '', $value)) == 4;
                            },
                            'message' => 'export_field_year_invalid',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'templates',
                'required' => false,
            ],
            [
                'name' => 'type',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getTypes(),
                        ],
                    ],
                ],
            ],
            [
                'name' => 'showArbo',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getShowArboChoices(),
                        ],
                    ],
                ],
            ],
            [
                'name' => 'dataArbo',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getDataArboChoices(),
                        ],
                    ],
                ],
            ],
            [
                'name' => 'groupByKeywords',
                'required' => false,
                'allow_empty' => true,
                'filters' => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name' => 'groupByKeywordsTitles',
                'required' => false,
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_WORD,
            self::TYPE_EXCEL,
            self::TYPE_PUBLIPOSTAGES,
        ];
    }

    /**
     * @return array
     */
    public static function getReferences()
    {
        return [
            self::EXPORT_REFERENCE_INITIAL,
            self::EXPORT_REFERENCE_KEYWORD,
        ];
    }

    public static function getShowArboChoices()
    {
        return [
            self::SHOW_ARBO_NO,
            self::SHOW_ARBO_INDENT,
            self::SHOW_ARBO_COLUMNS,
        ];
    }

    public static function getDataArboChoices()
    {
        return [
            self::DATA_ARBO_CODE,
            self::DATA_ARBO_NAME,
            self::DATA_ARBO_CODE_NAME,
        ];
    }

    public function exportWord($tree, $em, $translator)
    {
        $identifier = uniqid();

        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        $variables = [];

        // Create template file
        $filename = $this->createTemplate($tree, $em, $translator, $variables, $identifier);

        // Process the template
        $templateProcessor = new TemplateProcessor($filename);
        foreach ($variables as $variable) {
            $variableName = $variable['name'];

            switch ($variable['type']) {
                case 'chart':
                    /*
                    'data' => [
                    OK  'type' => 'pie',
                    OK  'data' => $chart,
                    'chartAlign' => 'center',
                    OK  'showPercent' => $showPercent,
                    OK  'showValue' => $showValue,
                    ]
                     */
                    $data = $variable['data']['data'];
                    if (count($data) > 0) {
                        if ($variable['data']['type'] == 'pie') {
                            $values = array_values($data);
                            $values = array_map(function ($value) {
                                if (is_array($value)) {
                                    return reset($value);
                                }
                                return $value;
                            }, $values);
                            $chart = new Chart(
                                $variable['data']['type'],
                                array_keys($data),
                                $values
                            );
                            $chart->getStyle()
                                ->setWidth(Converter::cmToEmu(21 * 0.8))
                                ->setHeight(Converter::cmToEmu(5))
                                ->setDataLabelOptions([
                                    'showPercent' => $variable['data']['showPercent'],
                                    'showVal' => $variable['data']['showValue'],
                                ]);
                        }
                        if ($variable['data']['type'] == 'bar') {
                            $legend = $data['legend'];
                            unset($data['legend']);

                            $series = [];
                            foreach (range(0, count($legend) - 1) as $i) {
                                foreach ($data as $dataItem) {
                                    $series[$i][] = $dataItem[$i];
                                }
                            }

                            $chart = new Chart(
                                $variable['data']['type'],
                                array_keys($data),
                                isset($series[0])? $series[0] : [],
                                null,
                                $legend[0]
                            );
                            foreach (range(1, count($legend) - 1) as $i) {
                                $chart->addSeries(array_keys($data), isset($series[$i])? $series[$i] : [], $legend[$i]);
                            }

                            $chart->getStyle()
                                ->setShowGridX(false)
                                ->setShowGridY(false)
                                ->setShowAxisLabels(true)
                                ->setShowLegend(true)
                                ->setWidth(Converter::cmToEmu(21 * 0.8))
                                ->setHeight(Converter::cmToEmu(2 * count($data)))
                                ->setDataLabelOptions([
                                    'showCatName' => false,
                                    'showVal' => false,
                                ]);
                        }
                        $templateProcessor->setChart($variableName, $chart);
                    } else {
                        $templateProcessor->setValue($variableName, '');
                    }
            }
        }
        $templateProcessor->saveAs($filename);

        // Add headers HTTP
        header('Content-Type: application/vnd.ms-word');
        header('Content-Length: ' . filesize($filename));
        header('Content-Disposition: attachment; filename=export.docx');
        header('Content-Transfer-Encoding: binary');

        // Send file to browser
        ob_clean();
        flush();
        readfile($filename);

        // Remove temporary file
        unlink($filename);
    }

    /**
     * @param $tree
     * @param $em
     * @param $translator
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportExcel($tree, $em, $translator)
    {
        $stripTags = new StripTags();
        /** @var Template $template */
        $template = $this->templates[0];
        $identifier = uniqid();
        $variables = [];

        if ($this->getReference() !== self::EXPORT_REFERENCE_KEYWORD) {
            foreach ($tree as $node) {
                if (isset($node['__type'])) {
                    $this->setReference(self::EXPORT_REFERENCE_KEYWORD);
                }

                break;
            }
        }

        $content = [];

        foreach ($tree as $node) {
            $level = 0;
            foreach ($this->nodeToPage($node, $em, $translator, $variables, $identifier, $template, true, $level) as $project) {
                $content[] = $project;
            }
        }

        $levelMax = 0;
        foreach ($content as $i => $project) {
            foreach ($project as $j => $col) {
                if (!in_array($j, ['arbo', 'level', 'type', 'parent_id'])) {
                    $col = $stripTags->filter(html_entity_decode($col));
                }

                if ($this->getShowArbo() === self::SHOW_ARBO_INDENT) {
                    $col = str_repeat('    ', $project['level']) . $col;
                    $col = str_replace("\n", "\n" . str_repeat('    ', $project['level']), $col);
                } else if ($this->getShowArbo() === self::SHOW_ARBO_COLUMNS) {
                    if ($levelMax < $project['level'] + 1) {
                        $levelMax = $project['level'] + 1;
                    }
                }

                $content[$i][$j] = $col;
            }
        }

        preg_match_all('/\$(.*)\$/U', $template->getContent(), $variables);

        foreach ($variables[1] as $i => $variable) {
            $variables[1][$i] = explode('***', explode('|||', $variable)[0])[1];
        }

        $data = array_merge([$variables[1]], $content);

        if ($this->getReference() === self::EXPORT_REFERENCE_KEYWORD && $this->getGroupByKeywords()) {
            $this->exportGroupByKeyword($data);
        } else if ($this->getShowArbo() === self::SHOW_ARBO_COLUMNS && $this->getDataArbo()) {
            foreach ($data as $i => $line) {
                $arbo = array_fill(0, $levelMax, '');

                if (isset($line['arbo']) && isset($line['level'])) {
                    $arbo[$line['level']] = $line['arbo'];
                }

                if (isset($line['arbo'])) {
                    unset($line['arbo']);
                }

                if (isset($line['level'])) {
                    unset($line['level']);
                }

                $newLine = [];
                foreach ($arbo as $v) {
                    $newLine[] = $v;
                }

                foreach ($line as $v) {
                    $newLine[] = $v;
                }

                $data[$i] = array_merge($arbo, $line);
            }
        } else {
            foreach ($data as $i => $line) {
                if (isset($data[$i]['level'])) {
                    unset($data[$i]['level']);
                }
            }
        }

        foreach ($data as $i => $row) {
            if (is_array($row)) {
                foreach ($row as $j => $value) {
                    $data[$i][$j] = ltrim($value, '=');
                }
            }
        }

        ExcelExporter::download($data);
    }

    public function ExportPublipostage($items, $em, $translator)
    {
        $stripTags = new StripTags();
        /** @var Template $template */
        $template = $this->templates[0];
        $identifier = uniqid();
        $variables = [];
        $labels = [];
        $keys = [];
        $content = [];

        // headers
        preg_match_all('/\$(.*)\$/U', $template->getContent(), $variables);

        foreach ($variables[1] as $v => $variable) {
            $labels[1][$v] = explode('***', explode('|||', $variable)[0])[1];
            $keys[1][$v] = explode('***', explode('|||', $variable)[1])[0];
        }

        foreach ($keys[1] as $key) {
            foreach ($items as $i => $row) {
                foreach ($row as $j => $value) {
                    if (is_array($value)) {
                        foreach ($value as $k => $propValue) {
                            if ($j . '.' . $k === $key) {
                                if ($j == 'keywords') {
                                    $content[$i][] = $propValue[0]['name'];
                                } else {
                                    $content[$i][] = $propValue;
                                }
                            }
                        }
                    } else {
                        if ($j === $key) {
                            $content[$i][] = $value;
                        }
                    }
                }
            }
        }

        $data = array_merge([$labels[1]], $content);

        foreach ($data as $i => $row) {
            if (is_array($row)) {
                foreach ($row as $j => $value) {
                    $data[$i][$j] = ltrim($value, '=');
                }
            }
        }

        ExcelExporter::download($data);
    }
    /**
     * @param $data
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Exception
     */
    protected function exportGroupByKeyword($data)
    {
        $newData = [];
        $lastLevel = null;
        $numberKeywords = 0;
        $group = [];
        $mergeCells = [];

        foreach ($data as $i => $line) {
            if ($i === 0) {
                continue;
            }

            if ($line['type'] === 'keyword') {
                $group = $this->insertKeyword($group, $line);

                if ($line['level'] + 1 > $numberKeywords) {
                    $numberKeywords = $line['level'] + 1;
                }
            } else if ($line['type'] === 'project') {
                $group = $this->insertProject($group, $line);
            }
        }

        $mergeCells[$numberKeywords] = [];
        foreach ($group as $keyword) {
            $newLine = array_fill(0, $numberKeywords, '');
            $this->insertKeywordProjects($numberKeywords, $mergeCells, $newData, $newLine, $keyword);
        }

        $titles = $this->getGroupByKeywordsTitles();
        $titlesArray = array_fill(0, $numberKeywords + 1, '');

        if (isset($titles['keywords']) && is_array($titles['keywords'])) {
            foreach ($titles['keywords'] as $key => $title) {
                if (!isset($titlesArray[$key])) {
                    break;
                }

                $titlesArray[$key] = $title;
            }
        }

        if (isset($titles['parentProjects'])) {
            $titlesArray[count($titlesArray) - 1] = $titles['parentProjects'];
        }

        array_unshift($newData, array_merge($titlesArray, $data[0]));

        $data = $newData;

        if (is_array($data)) {
            foreach ($data as $i => $row) {
                if (is_array($row)) {
                    foreach ($row as $j => $value) {
                        if (is_numeric($value) && $value == 0) {
                            $data[$i][$j] = '0';
                        }
                    }
                }
            }
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($data, null, 'A1');

        foreach ($mergeCells as $i => $col) {
            $column = Coordinate::stringFromColumnIndex($i + 1);

            $line = 1;

            foreach ($col as $number) {
                if ($number === 0) {
                    continue;
                }

                $startLine = $line + 1;
                $endLine = $line + $number;

                $sheet->mergeCells($column . $startLine . ':' . $column . $endLine);
                $line = $endLine;
            }
        }

        $allLines = $sheet->getStyle('A1:' . $sheet->getHighestColumn() . $sheet->getHighestDataRow());
        $allLines->getAlignment()->setWrapText(true);
        $allLines->getAlignment()->setVertical(Style\Alignment::VERTICAL_CENTER);
        $allLines->applyFromArray([
            'borders' => [
                'outline' => [
                    'borderStyle' => Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
                'inside' => [
                    'borderStyle' => Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
        ]);

        $headerLine = $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . '1');

        $headerLine->getFont()->setBold(true);
        $headerLine
            ->getAlignment()
            ->setHorizontal(Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(Style\Alignment::VERTICAL_CENTER);

        foreach ($sheet->getColumnIterator('A', $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col->getColumnIndex())->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
        header('Content-Disposition: attachment; filename="export.xlsx"');
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        ob_end_clean();
        $writer->save('php://output');
        exit();
    }

    /**
     * @param $group
     * @param $keyword
     * @return array
     * @throws \Exception
     */
    protected function insertKeyword($group, $keyword)
    {
        if (count($group) === 0 || $group[0]['level'] === $keyword['level']) {
            $group[] = [
                'name' => $keyword['name'],
                'type' => 'keyword',
                'level' => $keyword['level'],
                'children' => [],
                'parentProjects' => [],
            ];

            return $group;
        }

        $found = false;
        $lastGroup = &$group[count($group) - 1];

        while (!$found) {
            if ($lastGroup['level'] === $keyword['level'] - 1) {
                $found = true;
                $lastGroup['children'][] = [
                    'name' => $keyword['name'],
                    'type' => 'keyword',
                    'level' => $keyword['level'],
                    'children' => [],
                    'parentProjects' => [],
                ];
            } else {
                if (count($lastGroup['children']) === 0) {
                    throw new \Exception('Keyword not inserted');
                }

                $lastGroup = &$lastGroup['children'][count($lastGroup['children']) - 1];
            }
        }

        return $group;
    }

    /**
     * @param $group
     * @param $project
     * @return mixed
     */
    protected function insertProject($group, $project)
    {
        $found = false;
        $lastGroup = &$group[count($group) - 1];

        while (!$found) {
            if (count($lastGroup['children']) > 0) {
                $lastGroup = &$lastGroup['children'][count($lastGroup['children']) - 1];
                continue;
            }

            $found = true;
        }

        if (!isset($lastGroup['parentProjects'][$project['parent_id']])) {
            $lastGroup['parentProjects'][$project['parent_id']] = [
                'code' => $project['parent_code'],
                'name' => $project['parent_name'],
                'type' => 'parentProject',
                'children' => [],
            ];
        }

        $dataProject = [];

        foreach ($project as $property => $value) {
            if (in_array($property, ['level', 'type', 'parent_id', 'parent_code', 'parent_name'], true)) {
                continue;
            }

            $dataProject[] = $value;
        }

        $lastGroup['parentProjects'][$project['parent_id']]['children'][] = $dataProject;

        return $group;
    }

    /**
     * @param     $levelParentProject
     * @param     $mergeCells
     * @param     $newData
     * @param     $newLine
     * @param     $keyword
     * @return int
     */
    protected function insertKeywordProjects($levelParentProject, &$mergeCells, &$newData, $newLine, $keyword)
    {
        $totalNumberProjects = 0;

        if (!isset($mergeCells[$keyword['level']])) {
            $mergeCells[$keyword['level']] = [];
        }

        $newLine[$keyword['level']] = $keyword['name'];

        foreach ($keyword['parentProjects'] as $parentProject) {
            $parentProjectLine = $newLine;
            $parentProjectLine[] = ($parentProject['code'] !== ' ' ? $parentProject['code'] . '- ' : '') . $parentProject['name'];

            if (count($parentProject['children']) > 0) {
                foreach ($parentProject['children'] as $project) {
                    $projectLine = $parentProjectLine;

                    foreach ($project as $value) {
                        $projectLine[] = $value;
                    }

                    $newData[] = $projectLine;
                    ++$totalNumberProjects;
                }

                $mergeCells[$levelParentProject][] = count($parentProject['children']);
            }
        }

        if ($totalNumberProjects > 0 && $levelParentProject > $keyword['level'] + 1) {
            for ($level = $keyword['level']; $levelParentProject > $level + 1; ++$level) {
                if (!isset($mergeCells[$level + 1])) {
                    $mergeCells[$level + 1] = [];
                }

                $mergeCells[$level + 1][] = $totalNumberProjects;
            }
        }

        foreach ($keyword['children'] as $child) {
            $totalNumberProjects += $this->insertKeywordProjects($levelParentProject, $mergeCells, $newData, $newLine, $child);
        }

        $mergeCells[$keyword['level']][] = $totalNumberProjects;

        return $totalNumberProjects;
    }

    /**
     * @param array                     $node
     * @param EntityManager             $em
     * @param                           $translator
     * @param array                     $variables
     * @param string                    $identifier
     * @param null                      $template
     * @param bool                      $excel
     * @param                           $level
     * @return string|array
     */
    protected function nodeToPage($node, $em, $translator, &$variables, $identifier, $template = null, $excel = false, &$level = null)
    {
        $useTemplate = $template ? true : false;
        if (!$useTemplate) {
            $template = $this->getTemplateFromNode($node);
        }
        $object = $this->getNodeAsObject($node, $em);

        if ($excel) {
            $content = [];
            if (isset($level)) {
                $content[0] = ['level' => $level];
                $content[0] = array_merge($content[0], $template->parseContent($object, $em, $translator, $variables, $identifier, $excel));

                if ($this->getReference() === self::EXPORT_REFERENCE_KEYWORD && $this->getGroupByKeywords()) {
                    $content[0]['type'] = $object instanceof Project ? 'project' : 'keyword';
                    if ($object instanceof Project) {
                        $content[0]['parent_id'] = $object->getParent() ? $object->getParent()->getId() : 'null';
                        $content[0]['parent_code'] = $object->getParent() ? $object->getParent()->getCode() : null;
                        $content[0]['parent_name'] = $object->getParent() ? $object->getParent()->getName() : null;
                    } else {
                        $content[0]['name'] = $object->getName();
                    }
                } else if ($this->getShowArbo() === self::SHOW_ARBO_COLUMNS && $this->getDataArbo()) {
                    if ($object instanceof Project) {
                        switch ($this->getDataArbo()) {
                            case Export::DATA_ARBO_CODE:
                                $content[0]['arbo'] = $object->getCode();
                                break;
                            case Export::DATA_ARBO_NAME:
                                $content[0]['arbo'] = $object->getName();
                                break;
                            case Export::DATA_ARBO_CODE_NAME:
                                $content[0]['arbo'] = $object->getCode() ? $object->getCode() . ' - ' . $object->getName() : $object->getName();
                                break;
                        }
                    } else {
                        $content[0]['arbo'] = $object->getName();
                    }
                }
            }
        } else {
            $content = $template->parseContent($object, $em, $translator, $variables, $identifier, $excel);
        }

        if (isset($node['children']) && $node['children']) {
            $level++;
            foreach ($node['children'] as $children) {
                if ($useTemplate) {
                    foreach ($this->nodeToPage($children, $em, $translator, $variables, $identifier, $template, $excel, $level) as $project) {
                        $content[] = $project;
                    }
                } else {
                    $content .= $this->nodeToPage($children, $em, $translator, $variables, $identifier);
                }
            }
            $level--;
        }

        return $content;
    }

    /**
     * @param array         $tree
     * @param EntityManager $em
     * @param               $translator
     * @param array         $variables
     * @param string        $identifier
     * @return string
     */
    protected function createTemplate($tree, $em, $translator, &$variables, $identifier)
    {
        $content = '<?xml encoding="utf-8" ?><body>';
        foreach ($tree as $node) {
            $content .= $this->nodeToPage($node, $em, $translator, $variables, $identifier);
        }

        $content = preg_replace('#<body><div style="page-break-after:always;"></div>#', '<body>', $content);
        // Supprime les doubles sauts de page
        $content = preg_replace(
            '#<div style="page-break-after:always;"></div><div style="page-break-after:always;"></div>#',
            '<div style="page-break-after:always;"></div>',
            $content
        );
        // Supprime le dernier saut de page
        $content = preg_replace('#<div style="page-break-after:always;"></div>$#', '', $content);
        // Remplace les sauts de page
        $content = preg_replace(
            '#<div style="page-break-after:always;"></div>#',
            '<p style="page-break-after:always;"></p>',
            $content
        );
        // Remplace les caractères HTML d'espace par un vrai espace
        $content = str_replace('&nbsp;', ' ', $content);
        // Supprime les paragraphes vides
        $content = preg_replace('#<p></p>#', '', $content);
        // Supprime les tags DIV
        $content = preg_replace('#<div[^>]+>#', '', $content);
        $content = preg_replace('#</div>#', '', $content);
        // Supprime les tags HEADER
        $content = preg_replace('#<header[^>]+>#', '', $content);
        $content = preg_replace('#</header>#', '', $content);
        // Supprime les retours à la ligne
        $content = str_replace(PHP_EOL, '', $content);
        // Supprime les paragraphes dans les paragraphes
        $content = preg_replace('#<p>[\s]*<p>#', '<p>', $content);
        $content = preg_replace('#</p>[\s]*</p>#', '</p>', $content);
        // Supprime les spans dans les titres
        $content = preg_replace('#<h([1-5])><span#', '<h$1', $content);
        $content = preg_replace('#</span></h([1-5])>#', '</h$1>', $content);
        $content = preg_replace('#<br[\s/]+></h([1-5])>#', '</h$1>', $content);
        // Transforme les tableaux embarqués dans des paragraphes en tableaux
        $content = preg_replace('#<p><table#', '<table', $content);
        $content = preg_replace('#</table></p>#', '</table>', $content);
        // Transforme les tableaux embarqués dans des style inlines
        $content = preg_replace('#<strong><table#', '<table style="font-weight:bold"', $content);
        $content = preg_replace('#</table></strong>#', '</table>', $content);
        $content = preg_replace('#<span><table#', '<table', $content);
        $content = preg_replace('#<span[\s]+style="([^"]+)"><table#', '<table style="$1"', $content);
        $content = preg_replace('#</table></span>#', '</table>', $content);
        // Transforme les paragraphes en mode bold en paragraphe
        $content = preg_replace('#<p><strong>#', '<p style="font-weight:bold">', $content);
        $content = preg_replace('#</strong>([a-z])*</p>#', '$1</p>', $content);
        // Supprime les paragraphes dans les strongs
        $content = preg_replace('#<strong><p>#', '<strong>', $content);
        $content = preg_replace('#</p></strong>#', '</strong>', $content);

        // Transforme les paragraphes simples en conteneur inline
        //$content = preg_replace('#<p>([^<]+)</p>#', '<br /><span>$1</span>', $content);
        // Remplace le chemin des images : URL => Local Path
        $host = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : 'http://' . $_SERVER['HTTP_HOST'];
        $content = preg_replace('#<img src="' . $host . '#', '<img src="' . getcwd() . '/public', $content);
        // Remplace le chemin des images : Scheme file://
        $content = str_replace('<img src="file://', '<img src="', $content);
        // Close the body tag
        $content .= '</body>';

        // -- Clean images
        $dom = new DOMDocument();
        @$dom->loadHTML($content);
        // Fetch all images
        $imgs = $dom->getElementsByTagName('img');
        foreach ($imgs as $img) {
            // Remove if the image doesn't exist
            $pathFile = $img->getAttribute('src');
            if (!file_exists($pathFile)) {
                $img->parentNode->removeChild($img);
            }
        }

        // Clean nested lists
        do {
            $numNestedLists = $this->cleanNestedList($dom);
        } while ($numNestedLists > 0);
        // Clean nested paragraph
        do {
            $numNestedP = $this->cleanNestedParagraph($dom);
        } while ($numNestedP > 0);

        // Output only the body
        $bodyElements = $dom->getElementsByTagName('body');
        $content = $dom->saveXml($bodyElements->item(0));

        // echo $content; die; // -> to display export in local

        Settings::setOutputEscapingEnabled(true);
        Settings::setDefaultPaper('A4');
        $phpWord = new PhpWord();
        $phpWord->getSettings()->setThemeFontLang(new Language(Language::FR_FR));
        $section = $phpWord->addSection();
        $section->getStyle()
            ->setMarginTop(850)
            ->setMarginRight(850)
            ->setMarginBottom(850)
            ->setMarginLeft(850);
        try {
            Html::addHtml($section, $content, true);
        } catch (BadMethodCallException $e) {
            if (strpos($e->getMessage(), 'Cannot add') === 0) {
                throw new \Exception(
                    'Invalid HTML : ' . $content,
                    0,
                    $e
                );
            } else {
                throw $e;
            }
        }

        $ratioTwipsPixels = 1 / Converter::INCH_TO_TWIP * Converter::INCH_TO_PIXEL;
        foreach ($section->getElements() as $sectionElement) {
            if ($sectionElement instanceof Table) {
                if ($sectionElement->getStyle()->getStyleName() == 'table') {
                    $sectionElement->getStyle()
                        ->setBorderColor('9c9c9c')
                        ->setBorderSize(Converter::pixelToTwip(1))
                    ;

                    foreach ($sectionElement->getRows() as $numRow => $row) {
                        foreach ($row->getCells() as $cell) {
                            if (!$cell->getStyle()->getBorderTopColor()) {
                                $cell->getStyle()
                                    ->setBorderColor('9c9c9c')
                                    ->setBorderSize(Converter::pixelToTwip(1))
                                ;
                            }
                            // First row
                            if ($numRow == 0) {
                                $cell->getStyle()->setBgColor('DEDEDE');
                            }

                            foreach ($cell->getElements() as $cellElement) {
                                if ($cellElement instanceof Table) {
                                    $cellElement->getStyle()
                                        ->setWidth(100 * 50)
                                    // it's fiftieth (1/50) of a percent (1% = 50 unit)
                                        ->setUnit(TblWidth::PERCENT);
                                    foreach ($cellElement->getRows() as $cellElementRow) {
                                        $numCells = count($cellElementRow->getCells());
                                        $widthCells = (int) round(100 / $numCells, 0, PHP_ROUND_HALF_DOWN);
                                        foreach ($cellElementRow->getCells() as $cellElementRowCell) {
                                            $cellElementRowCell->getStyle()
                                                ->setWidth($widthCells * 50)
                                            // it's fiftieth (1/50) of a percent (1% = 50 unit)
                                                ->setUnit(TblWidth::PERCENT);
                                        }
                                    }
                                }
                                if ($cellElement instanceof TextRun) {
                                    // First row
                                    if ($numRow == 0) {
                                        if ($cellElement->getParagraphStyle() instanceof Paragraph) {
                                            $cellElement->getParagraphStyle()
                                                ->setAlignment(Jc::CENTER)
                                            ;
                                        }
                                    }
                                    foreach ($cellElement->getElements() as $textRunElement) {
                                        if ($textRunElement instanceof Text) {
                                            $textRunElement->getFontStyle()
                                                ->setSize(9)
                                                ->setBgColor('auto')
                                            ;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                foreach ($sectionElement->getRows() as $sectionElementRow) {
                    foreach ($sectionElementRow->getCells() as $sectionElementCell) {
                        // Get width of the cell in table
                        $widthCell = $sectionElementCell->getWidth();
                        // Percent values are in 50th of percent
                        if ($sectionElementCell->getStyle()->getUnit() == TblWidth::PERCENT) {
                            $widthCell /= 50;
                            $tableWidth = $sectionElement->getWidth() ?? (50 * 100);
                            if ($sectionElement->getStyle()->getUnit() == TblWidth::PERCENT) {
                                $tableWidth /= 50;
                                // Twips
                                $sectionSize = $section->getStyle()->getPageSizeW();
                                // Twips => Pixels
                                $sectionSize = (int) ceil($sectionSize * $ratioTwipsPixels);
                                // - Margin Left
                                $sectionSize -= (int) ceil($section->getStyle()->getMarginLeft() * $ratioTwipsPixels);
                                // - Margin Right
                                $sectionSize -= (int) ceil($section->getStyle()->getMarginRight() * $ratioTwipsPixels);
                                // - Page Gutter
                                $sectionSize -= 2 * (int) ceil($section->getStyle()->getGutter() * $ratioTwipsPixels);
                                // - ColsSpace
                                $sectionSize -= 2 * (int) ceil($section->getStyle()->getColsSpace() * $ratioTwipsPixels);

                                // Size in pixels of the table given in percent
                                $tableWidth = (int) (($sectionSize * $tableWidth) / 100);
                            }

                            // Size in pixels of the cell given in percent
                            $widthCell = (int) (($tableWidth * $widthCell) / 100);
                        }

                        foreach ($sectionElementCell->getElements() as $sectionElementCellElement) {
                            if ($sectionElementCellElement instanceof TextRun) {
                                foreach ($sectionElementCellElement->getElements() as $sectionElementCellSubElement) {
                                    if ($sectionElementCellSubElement instanceof Image) {
                                        $sectionElementCellSubElement->getStyle()
                                            ->setHPosRelTo(PhpWordStyle\Image::POSITION_RELATIVE_TO_CHAR)
                                        ;

                                        // Original Size of the element Image
                                        $widthImage = $sectionElementCellSubElement->getStyle()->getWidth();
                                        $heightImage = $sectionElementCellSubElement->getStyle()->getHeight();
                                        if ($sectionElementCellSubElement->getStyle()->getUnit() == PhpWordStyle\Image::UNIT_PT) {
                                            $widthImage = Converter::pointToPixel($widthImage);
                                            $heightImage = Converter::pointToPixel($heightImage);
                                        }

                                        if ($widthImage > $widthCell) {
                                            $sectionElementCellSubElement->getStyle()
                                                ->setWidth($widthCell)
                                                ->setHeight(($heightImage * $widthCell) / $widthImage)
                                                ->setUnit(PhpWordStyle\Image::UNIT_PX);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } elseif ($sectionElement instanceof TextRun) {
                foreach ($sectionElement->getElements() as $textRunElement) {
                    if ($textRunElement instanceof Text) {
                        $text = trim($textRunElement->getText());
                        foreach ($variables as $variable) {
                            if ($text == '$' . $variable['name'] . '$') {
                                $textRunElement->setText('${' . $variable['name'] . '}');
                            }
                        }
                    }
                }
            } elseif ($sectionElement instanceof Text) {
                $text = trim($sectionElement->getText());
                foreach ($variables as $variable) {
                    if ($text == '$' . $variable['name'] . '$') {
                        $sectionElement->setText('${' . $variable['name'] . '}');
                    }
                }
            }

        }

        // die();

        $filename = getcwd() . '/data/phpdocx/temp/' . $identifier . '.docx';

        $phpWord->save($filename, 'Word2007');

        return $filename;
    }

    private function cleanNestedList(\DOMDocument $dom)
    {
        $spans = $dom->getElementsByTagName('span');
        $spanLists = array();
        foreach ($spans as $span) {
            $hasULChild = false;
            foreach ($span->childNodes as $child) {
                if ($child->nodeName == 'ul') {
                    $hasULChild = true;
                }
            }
            if (!$hasULChild) {
                continue;
            }
            $spanLists[] = $span;
        }
        // If nested unordered lists, move to a paragraph
        foreach ($spanLists as $span) {
            $spanChildren = array();
            foreach ($span->childNodes as $child) {
                $spanChildren[] = $child;
            }

            foreach ($spanChildren as $spanChild) {
                if ($spanChild->nodeName === 'ul') {
                    $child = $span->ownerDocument->importNode($spanChild, true);
                    $span->parentNode->insertBefore($child, $span->nextSibling);
                }
            }

            $p = $dom->createElement('p');
            foreach ($span->attributes as $spanAttr) {
                $p->setAttribute($spanAttr->nodeName, $spanAttr->nodeValue);
            }
            foreach ($spanChildren as $spanChild) {
                if ($spanChild->nodeName !== 'ul') {
                    $child = $span->ownerDocument->importNode($spanChild, true);
                    $p->appendChild($child);
                }
            }
            $span->parentNode->replaceChild($p, $span);
        }

        return count($spanLists);
    }

    private function cleanNestedParagraph(\DOMDocument $dom)
    {
        // Fetch all paragraphs
        $ps = $dom->getElementsByTagName('p');
        $pLists = array();
        foreach ($ps as $p) {
            $hasPChild = false;
            foreach ($p->childNodes as $child) {
                if ($child->nodeName == 'p') {
                    $hasPChild = true;
                }
            }
            if (!$hasPChild) {
                continue;
            }
            $pLists[] = $p;
        }
        // If nested paragraphs, move to upper level
        foreach ($pLists as $p) {
            $pChildren = array();
            foreach ($p->childNodes as $child) {
                $pChildren[] = $child;
            }

            foreach (array_reverse($pChildren) as $pChild) {
                $child = $p->ownerDocument->importNode($pChild, true);
                $p->parentNode->insertBefore($child, $p->nextSibling);
            }

            $p->parentNode->removeChild($p);
        }

        return count($pLists);
    }

    /**
     * @param array         $node
     * @param EntityManager $em
     * @return Project|Keyword|Object
     */
    protected function getNodeAsObject($node, $em)
    {
        $nodeType = $this->getNodeType($node);

        return $em->getRepository($nodeType === 'project' ? 'Project\Entity\Project' : 'Keyword\Entity\Keyword')->find($node['id']);
    }

    /**
     * @param array $node
     * @return Template|null
     */
    protected function getTemplateFromNode(&$node)
    {
        $nodeType = $this->getNodeType($node);

        // keyword node management
        if ('keyword' === $nodeType) {
            return $this->getKeywordTemplateFromNode($node);
        }
        // project node management
        else {
            // specific for referential keyword export
            if ($this->reference === self::EXPORT_REFERENCE_KEYWORD && 'project' === $nodeType) {
                return $this->getProjectKeywordTemplateFromNode($node);
            }
            // default
            else {
                return $this->getProjectTemplateFromNode($node);
            }
        }
    }

    private function getKeywordTemplateFromNode(&$node)
    {
        $hasChildren = isset($node['children']) && count($node['children']) !== 0;
        $type = 'node';
        if (!isset($node['parents'])) {
            $type = 'parent';
        } else if (!$hasChildren || $this->hasOnlyProjectChildren($node)) {
            $type = 'leaf';
        }

        // update children parent type to have the final template (cf getProjectKeywordTemplateFromNode method)
        if (isset($node['children'])) {
            foreach ($node['children'] as $key => $child) {
                if (!isset($child['parent'])) {
                    $node['children'][$key]['parent'] = array('__type' => $type);
                } else {
                    $node['children'][$key]['parent']['__type'] = $type;
                }
            }
        }
        return $this->getTemplateByType($type);
    }

    private function hasOnlyProjectChildren($node)
    {
        if (!isset($node['children'])) {
            throw new \Exception("No children to check node");
        }
        $childTypes = array();
        foreach ($node['children'] as $child) {
            $childTypes[] = $this->getNodeType($child);
        }
        $childTypes = array_unique($childTypes, SORT_STRING);
        return count($childTypes) === 1 && in_array('project', $childTypes, true);
    }

    private function getProjectKeywordTemplateFromNode(&$node)
    {
        $parentType = $this->getNodeType($node['parent']);
        // to be able to manage projects hierarchy
        // keep the same project template that its parent
        $templateType = StringUtils::strEndsWith($parentType, '_project') ? $parentType : $parentType . '_project';

        // update children projects (if exist) parent type to have the final template (cf getProjectKeywordTemplateFromNode method)
        if (isset($node['children'])) {
            foreach ($node['children'] as $key => $child) {
                if (!isset($child['parent'])) {
                    $node['children'][$key]['parent'] = array('__type' => $templateType);
                } else {
                    $node['children'][$key]['parent']['__type'] = $templateType;
                }
            }
        }
        return $this->getTemplateByType($templateType);
    }

    private function getProjectTemplateFromNode($node)
    {
        if ((!isset($node['parent']) || !$node['parent']) && (isset($node['children']) && $node['children'])) {
            $type = 'parent';
        } else if (!isset($node['children']) || !$node['children']) {
            $type = 'leaf';
        } else {
            $type = 'node';
        }
        return $this->getTemplateByType($type);
    }

    /**
     * @param array $node
     * @return string
     */
    protected function getNodeType($node)
    {
        return isset($node['__type']) ? $node['__type'] : 'project';
    }

    /**
     * @param $type string
     * @return Template|null
     */
    protected function getTemplateByType($type)
    {
        foreach ($this->templates as $template) {
            if ($template->getType() === $type) {
                return $template;
            }
        }

        return null;
    }
}
