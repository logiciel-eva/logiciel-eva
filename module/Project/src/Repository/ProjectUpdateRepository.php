<?php

namespace Project\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ProjectUpdateRepository
 */
class ProjectUpdateRepository extends EntityRepository
{
    public function findByProjectId($projectId)
    {
      return $this->createQueryBuilder('u')
        ->where('u.project = :projectId')
        ->setParameter('projectId', $projectId)
        ->orderBy('u.date', 'DESC')
        ->getQuery()
        ->getResult();
    }
}
