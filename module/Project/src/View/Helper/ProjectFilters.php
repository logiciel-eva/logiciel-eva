<?php

namespace Project\View\Helper;

use Application\Entity\Configuration;
use Bootstrap\Entity\Client;
use Budget\View\Helper\ExpenseFilters;
use Budget\View\Helper\IncomeFilters;
use Budget\View\Helper\PosteExpenseFilters;
use Budget\View\Helper\PosteIncomeFilters;
use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use Field\Entity\Field;
use Project\Entity\AgeSynchronisation;
use Project\Entity\Project;
use Time\View\Helper\TimesheetFilters;

class ProjectFilters extends EntityFilters
{
	public function __construct(
		Translate $translator,
		MyModuleManager $moduleManager,
		EntityManager $entityManager,
		Configuration $configuration,
		Client $client,
		$expenseTypes,
		$incomeTypes,
		$allowedKeywordGroups,
		$allowedFieldGroups
	) {
		parent::__construct($translator, $moduleManager, $entityManager);

		$publicationStatusOptions = [];
		foreach (Project::getPublicationStatutes() as $publicationStatus) {
			$publicationStatusOptions[] = [
				'value' => $publicationStatus,
				'text' => $this->translator->__invoke('project_publicationStatus_' . $publicationStatus),
			];
		}

		$ageSynchronisationStatusOptions = [];
		foreach (AgeSynchronisation::getAgeSynchronisationStatutes() as $ageSynchronisationStatus) {
			$ageSynchronisationStatusOptions[] = [
				'value' => $ageSynchronisationStatus,
				'text' => $this->translator->__invoke('age_synchro_state_' . $ageSynchronisationStatus),
			];
		}

		$levels = $configuration->getModules()['project']['levels'] ?? [];
		$levelsOptions = [];
		foreach ($levels as $i => $level) {
			$levelsOptions[] = [
				'value' => $i,
				'text' => $level['name'],
			];
		}


		$filters = [
			'level' => [
				'label' => $this->translator->__invoke('project_field_level'),
				'type' => 'manytoone',
				'options' => $levelsOptions,
			],
			'code' => [
				'label' => $this->translator->__invoke('project_field_code'),
				'type' => 'string',
			],
			'name' => [
				'label' => $this->translator->__invoke('project_field_name'),
				'type' => 'string',
			],
			'publicationStatus' => [
				'label' => $this->translator->__invoke('project_field_publicationStatus'),
				'type' => 'manytoone',
				'options' => $publicationStatusOptions,
			],
			'managers' => [
				'label' => $this->translator->__invoke('project_field_managers'),
				'type' => 'user-select',
			],
			'validators' => [
				'label' => $this->translator->__invoke('project_field_validators'),
				'type' => 'user-select',
			],
			'members' => [
				'label' => $this->translator->__invoke('project_field_members'),
				'type' => 'user-select',
			],
			'id' => [
				'label' => $this->translator->__invoke('project_field_projects'),
				'type' => 'project-select',
			],
			'actors' => [
				'label' => $this->translator->__invoke('project_field_actors'),
				'type' => 'structure-select',
			],
			'description' => [
				'label' => $this->translator->__invoke('project_field_description'),
				'type' => 'string',
			],
			'result' => [
				'label' => $this->translator->__invoke('project_field_result'),
				'type' => 'string',
			],
			'programmingDate' => [
				'label' => $this->translator->__invoke('project_field_programmingDate'),
				'type' => 'date',
			],
			'plannedDateStart' => [
				'label' => $this->translator->__invoke('project_field_plannedDateStart'),
				'type' => 'date',
			],
			'plannedDateEnd' => [
				'label' => $this->translator->__invoke('project_field_plannedDateEnd'),
				'type' => 'date',
			],
			'realDateStart' => [
				'label' => $this->translator->__invoke('project_field_realDateStart'),
				'type' => 'date',
			],
			'realDateEnd' => [
				'label' => $this->translator->__invoke('project_field_realDateEnd'),
				'type' => 'date',
			],
			'external' => [
				'label' => $this->translator->__invoke('project_field_external'),
				'type' => 'boolean',
			],
			'networkAccessible' => [
				'label' => $this->translator->__invoke('project_field_networkAccessible'),
				'type' => 'boolean',
			],
			'ageSynchronisation' => [
				'label' => $this->translator->__invoke('project_field_ageSynchronisation'),
				'type' => 'manytoone',
				'options' => $ageSynchronisationStatusOptions,
			],
			'ownership' => [
				'label' => $this->translator->__invoke('project_field_ownership'),
				'type' => 'structure-select',
			],
			'parent' => [
				'label' => $this->translator->__invoke('project_field_parent'),
				'type' => 'project-select',
			],
			'template' => [
				'label' => $this->translator->__invoke('project_field_template'),
				'type' => 'template-select',
			],
			'createdAt' => [
				'label' => $this->translator->__invoke('project_field_createdAt'),
				'type' => 'datetime',
			],
			'updatedAt' => [
				'label' => $this->translator->__invoke('project_field_updatedAt'),
				'type' => 'datetime',
			],
		];

		if ($this->moduleManager->isActive('budget')) {
			$filters['budgetStatus'] = [
				'label' => $this->translator->__invoke('project_field_budgetStatus'),
				'type' => 'budget-status-select',
			];

			$filters['virtual.caduciteStart'] = [
				'label' => $this->translator->__invoke('poste_income_field_caduciteStart'),
				'type' => 'date',
			];

			$filters['virtual.caduciteEnd'] = [
				'label' => $this->translator->__invoke('poste_income_field_caduciteEnd'),
				'type' => 'date',
			];

			$filters['virtual.financer'] = [
				'label' => $this->translator->__invoke('envelope_field_financer'),
				'type' => 'financer-select',
			];

			$filters['rateExpenseRealized'] = [
				'label' => $this->translator->__invoke('project_field_rateExpenseRealized'),
				'type' => 'number',
			];

			$filters['rateIncomeRealized'] = [
				'label' => $this->translator->__invoke('project_field_rateIncomeRealized'),
				'type' => 'number',
			];

			if ($this->moduleManager->getClientModuleConfiguration('budget', 'code')) {
				$filters['budgetCodes'] = [
					'label' => $this->translator->__invoke('project_field_budgetCodes'),
					'type' => 'budget-code-select',
				];
			}
		}

		if ($this->moduleManager->isActive('advancement')) {
			$filters['virtual.advancement'] = [
				'label' => ucfirst($this->translator->__invoke('advancement_entity')),
				'type' => 'number',
			];
		}

		if ($this->moduleManager->isActive('keyword')) {

			$groups = $entityManager->getRepository('Keyword\Entity\Group')->findByEntity('member', false);
			if (1 === count($groups)) {
				$group = $groups[0];
				$keywords = $group->getKeywords();
				foreach($keywords as $keyword) {
					$filters['member-function.' . $keyword->getId()] = [
						'label' => $keyword->getName(),
						'type' => 'simple-user-select'
					];
				}
			}


			$groups = $allowedKeywordGroups('project', false);
			foreach ($groups as $group) {
				if (!$group->isArchived()) {
					$filters['keyword.' . $group->getId()] = [
						'label' => $group->getName(),
						'type' => 'keyword-select',
						'group' => $group->getId(),
					];
				}
			}
		}

		if ($this->moduleManager->isActive('field')) {
			$fieldsGroup = $allowedFieldGroups('Project\Entity\Project');
			foreach ($fieldsGroup as $group) {
				if ($group->isEnabled()) {
					foreach ($group->getFields() as $field) {
						switch ($field->getType()) {
							case Field::TYPE_TEXTAREA:
							case Field::TYPE_TEXT:
							case Field::TYPE_AMOUNT:
								$fieldFilter = [
									'label' => $field->getName(),
									'type' => 'string',
								];
								$filters['customField.' . $field->getId()] = $fieldFilter;
								break;
							case Field::TYPE_RADIO:
							case Field::TYPE_CHECKBOX:
							case Field::TYPE_SELECT:
								$fieldFilter = [
									'label' => $field->getName(),
									'type' => 'select',
								];
								$filters['customField.' . $field->getId()] = $fieldFilter;
								break;
						}
					}
				}
			}
		}

		if ($this->moduleManager->isActive('map')) {
			$filters['territory'] = [
				'label' => ucfirst($this->translator->__invoke('territory_entity')),
				'type' => 'territory-select',
			];
		}

		if ($client->isMaster()) {
			$clientsOptions = [];

			foreach ($client->getNetwork()->getClients() as $client) {
				if (!$client->isMaster()) {
					$clientsOptions[] = [
						'value' => $client->getId(),
						'text' => $client->getName(),
					];
				}
			}

			$filters['_client'] = [
				'label' => ucfirst($this->translator->__invoke('client_entity')),
                'type' => 'manytoone',
				'options' => $clientsOptions,
			];
		}

		if ($this->moduleManager->isActive('time')) {
			$timesheetFilters = new TimesheetFilters(
				$this->translator,
				$this->moduleManager,
				$this->entityManager,
				$allowedKeywordGroups
			);
			$timesheetFilters = $timesheetFilters(['project']);

			foreach ($timesheetFilters as $key => $filter) {
				$filters['timesheet-' . $key] = $filter;
			}
		}

		if ($this->moduleManager->isActive('budget')) {
			$posteExpenseFilters = (new PosteExpenseFilters(
				$this->translator,
				$this->moduleManager,
				$this->entityManager,
				$allowedKeywordGroups
			))->__invoke();

			foreach ($posteExpenseFilters as $key => $filter) {
				$filters['posteExpense-' . $key] = $filter;
			}

			$posteIncomeFilters = (new PosteIncomeFilters(
				$this->translator,
				$this->moduleManager,
				$this->entityManager,
				$allowedKeywordGroups
			))->__invoke();

			foreach ($posteIncomeFilters as $key => $filter) {
				$filters['posteIncome-' . $key] = $filter;
			}

			$expenseFilters = (new ExpenseFilters(
				$this->translator,
				$this->moduleManager,
				$this->entityManager,
				$expenseTypes,
				$allowedKeywordGroups
			))->__invoke();

			foreach ($expenseFilters as $key => $filter) {
				$filters['expense-' . $key] = $filter;
			}

			$incomeFilters = (new IncomeFilters(
				$this->translator,
				$this->moduleManager,
				$this->entityManager,
				$incomeTypes,
				$allowedKeywordGroups
			))->__invoke();

			foreach ($incomeFilters as $key => $filter) {
				$filters['income-' . $key] = $filter;
			}
		}

		$this->filters = $filters;
	}
}
