(function(app) {

    app.factory('synchronisationService', [
        'restFactoryService',
        function synchronisationService(restFactoryService) {
            return restFactoryService.create('/api/timesheet/synchronisation');
        }
    ])

})(window.EVA.app);
