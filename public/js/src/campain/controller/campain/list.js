(function(app) {

    app.controller('campainListController', [
        'campainService', '$scope', 'translator',
        function groupIndicatorListController(campainService, $scope, translator) {
            var self = this;
            self.campains = [];
            self.options = {
                name: 'campains-list',
                col: [
                    'id',
                    'name',
                    'description',
                    'evaluationManagers.name',
                    'evaluationManagers.id',
                    'startDate',
                    'endDate'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    campainService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };
            
            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(campain) {
                self.editedLineErrors = {};
                self.editedLine       = campain;
                self.editedLineFields = angular.copy(campain);
            };

            self.saveEditedLine = function saveEditedLine(campain) {
                self.editedLineErrors = {};
                campainService.save(angular.extend({}, campain, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        campain = angular.extend(campain, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('measure-campain_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };
            self.deleteCampain = function deleteCampain(campain) {
                if (confirm(translator('campain_question_delete').replace('%1', campain.name))) {
                    campainService.remove(campain).then(function (res) {
                        if (res.success) {
                            self.campains.splice(self.campains.indexOf(campain), 1);
							flashMessenger.success(translator('campain_message_deleted'));
						} else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
