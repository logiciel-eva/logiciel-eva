(function (app) {
	app.controller('projectFilterController', [
		'$scope',
		'$timeout',
		'$rootScope',
		function ($scope, $timeout, $rootScope) {
			var self = this;

			self.filtersKey = null;
			self.variable = null;
			self.defaultFilter = true;	// define whether the publication status filter must be initialized

			self.init = function (filtersKey, variable, defaultFilter = true) {
				self.filtersKey = filtersKey;
				self.variable = variable;
				self.defaultFilter = defaultFilter;
			};

			var events = [
				'analysis-time-query-loaded',
				'budget-analysis-subvention-list-query-loaded',
				'budget-analysis-gbcp-list-query-loaded',
				'analysis-budget-expense-query-loaded',
				'analysis-budget-income-query-loaded',
				'analysis-budget-financer-query-loaded',
				'analysis-budget-keywords-query-loaded',
				'analysis-budget-year-query-loaded',
				'analysis-indicator-recap-query-loaded',
				'analysis-indicator-recap-with-measures-query-loaded',
				'analysis-map-query-loaded',
			];

			events.forEach(function (event) {
				$rootScope.$on(event, function (e, filters) {
					showFilters(filters[self.filtersKey]);
				});
			});

			$timeout(function () {
				if (typeof $scope.__filters === 'undefined') {
					$scope.__filters = {};
				}

				if (self.variable) {
					if (self.defaultFilter) {
						self.variable.publicationStatus = {
							op: 'neq',
							val: ['archived'],
						};
						$scope.__filters.publicationStatus = true;
					}
				} else if (typeof $scope.$parent.__tb !== 'undefined') {
					if (self.defaultFilter) {
						$scope.$parent.__tb.apiParams.search.data.filters.publicationStatus = {
							op: 'neq',
							val: ['archived'],
						};
					}

					for (var i in $scope.$parent.__tb.apiParams.search.data.filters) {
						$scope.__filters[i] = true;
					}
				} else {
					if (self.defaultFilter) {
						$scope.self.filters.project.publicationStatus = {
							op: 'neq',
							val: ['archived'],
						};
					}

					for (var i in $scope.self.filters.project) {
						$scope.__filters[i] = true;
					}
				}
			});

			$scope.init = function (filters) {
				if (filters) {
					$timeout(function () {
						if (typeof $scope.__filters === 'undefined') {
							$scope.__filters = {};
						}

						for (var i in filters) {
							if (self.variable) {
								self.variable[i] = filters[i];
								$scope.__filters[i] = true;
							} else if (typeof $scope.$parent.__tb !== 'undefined') {
								$scope.$parent.__tb.apiParams.search.data.filters[i] = filters[i];

								for (var i in $scope.$parent.__tb.apiParams.search.data.filters) {
									$scope.__filters[i] = true;
								}
							} else {
								$scope.self.filters.project[i] = filters[i];

								for (var i in $scope.self.filters.project) {
									$scope.__filters[i] = true;
								}
							}
						}
					});
				}
			};

			function showFilters(filters) {
				$scope.__filters = {};

				for (var i in filters) {
					if (
						filters[i] !== null &&
						(typeof filters[i].val !== 'undefined' || filters[i].op === 'isNull')
					) {
						$scope.__filters[i] = true;
					} else {
						$scope.__filters[i] = false;
					}
				}
			}
		},
	]);
})(window.EVA.app);
