<?php

return [
    'module' => [
        'alert' => [
            'environments' => [
                'client'
            ],
            'active'   => true,
            'required' => false,
            'acl' => [
                'entities' => [
                    [
                        'name'     => 'alert',
                        'class'    => 'Alert\Entity\Alert',
                        'owner' => []
                    ],
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
