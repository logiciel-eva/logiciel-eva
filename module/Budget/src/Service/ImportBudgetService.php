<?php

namespace Budget\Service;

use Core\Email\Email;
use Core\Utils\MailUtils;
use Laminas\I18n\Translator\TranslatorInterface;
use Laminas\View\Model\JsonModel;
use Laminas\Mime\Mime;

final class ImportBudgetService
{
    public ImportBudgetLoggerService $logger;

    public function __construct(
        private string $importFile,
        private string $donePath,
        private TranslatorInterface $translator,
        private int $processedLines = 0,
        private int $errorLines = 0,
    )
    {
        $this->logger = new ImportBudgetLoggerService(
            $this->getLogFilePath()
        );

        $this->logger->info(sprintf($translator->translate('budget_automated_import_file_start'), $this->importFile));
    }

    public function moveFileToDonePath()
    {
        if(!is_dir($this->donePath)){
            mkdir($this->donePath);
        }

        rename(
            $this->importFile,
            $this->donePath . DIRECTORY_SEPARATOR . basename($this->importFile)
        );
    }

    private function getLogFilePath()
    {
        return $this->donePath
            . DIRECTORY_SEPARATOR
            . str_replace(
                '.csv',
                '.' . time() . '.log',
                basename($this->importFile)
            );
    }

    public function writeLogs(JsonModel $response){
        if(isset($response->errors) && $response->errors !== []){
            foreach($response->errors as $line => $error){
                if($error === null){
                    continue;
                }
                ++$this->errorLines;
                $this->logger->error(
                    sprintf('L%s: %s', $line, is_array($error) ? json_encode($error) : $error)
                );
            }
        }

        $this->logger->notice(sprintf($this->translator->translate('budget_automated_import_lines_processed'), $this->processedLines));
        $this->logger->notice(sprintf($this->translator->translate('budget_automated_import_lines_errors'), $this->errorLines));

        if(isset($response->success) && $response->success === true){
            $this->logger->info(
                sprintf($this->translator->translate('budget_automated_import_file_success'),$this->importFile)
            );
        }
    }

    public function getCsvData(string $delimiter = ';'): \Traversable
    {
        $file = fopen($this->importFile, 'r');

        if($file === false) {
            $this->logger->error($this->translator->translate('budget_automated_import_file_read_error'));
            return [];
        }

        $this->logger->info($this->translator->translate('budget_automated_import_file_read_started'));

        while (($data = fgetcsv(stream: $file, separator: $delimiter)) !== false) {
            ++$this->processedLines;
            yield $data;
        }

        fclose($file);
    }

    public function sendEmail($client, $clientPrivateConfig) {
        $config = $clientPrivateConfig->getBudgetAutomatedImport();
        if(!isset($config)) {
           return;
        }

        $emailFrom= $config['notification_email_from'];
        $emailTo= $config['notification_email_to'];

        if(empty($emailFrom) || empty($emailTo)) {
           return;
        }

        $email = new Email($client);
        $email->setSubject($this->translator->translate('budget_automated_import_email_subject'));
        $email->addHtmlContent($this->translator->translate('budget_automated_import_email_content'));

        $email->setFrom($emailFrom, 'Notification');
        $email->addAttachment(fopen($this->logger->getLogFile(), 'r'), 
                             time() . '.log',
                              Mime::TYPE_TEXT);

        $email->sendTo($emailTo);
    }
}