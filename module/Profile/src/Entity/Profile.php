<?php

namespace Profile\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="profile_profile")
 */
class Profile extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;


    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $price_per_hour;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $price_per_day;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $hour_per_day;


    /**
     * @ORM\OneToMany(targetEntity="User\Entity\User", mappedBy="profile")
     */
    protected $users;


    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
                'validators' => [
                ],
            ],

            [
                'name'     => 'price_per_hour',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse'],
                ],
                'validators' => [
                ],
            ],
            [
                'name'     => 'price_per_day',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse'],
                ],
                'validators' => [
                ],
            ],
            [
                'name'     => 'hour_per_day',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse'],
                ],
                'validators' => [
                    [
                        'name'    => 'Between',
                        'options' => [
                            'min' => 1,
                            'max' => 24,
                        ],
                    ],
                ],
            ],


        ]);

        return $inputFilter;
    }


    /**
     * @return float
     */
    public function getPrice_per_hour(): float
    {
        return $this->price_per_hour;
    }

    /**
     * @param float $price_per_hour
     */
    public function setPricePerHour($price_per_hour): void
    {
        $this->price_per_hour = $price_per_hour;
    }

    /**
     * @return float
     */
    public function getPrice_per_day(): float
    {
        return $this->price_per_day;
    }

    /**
     * @param float $price_per_day
     */
    public function setPricePerDay($price_per_day): void
    {
        $this->price_per_day = $price_per_day;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function addUsers(ArrayCollection $users)
    {
        foreach ($users as $user) {
            $this->getUsers()->add($user);
        }
    }

    /**
     * @param ArrayCollection $users
     */
    public function removeUsers(ArrayCollection $users)
    {
        foreach ($users as $user) {
            $this->getUsers()->removeElement($user);
        }
    }

    /**
     * @return float
     */
    public function getHour_per_day()
    {
        return $this->hour_per_day;
    }

    /**
     * @param $hour_per_day
     */
    public function setHourPerDay($hour_per_day)
    {
        $this->hour_per_day = $hour_per_day;
    }
}
