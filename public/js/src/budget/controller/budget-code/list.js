(function(app) {
	app.controller('budgetCodeListController', [
		'flashMessenger',
		'translator',
		'$rootScope',
		'$scope',
		'budgetCodeService',
		function(flashMessenger, translator, $rootScope, $scope, budgetCodeService) {
			const self = this;

			self.modal = $('#budgetCode-form');

			self.project_id = null;

			self.budgetCodes = [];
			self.budgetCode = {
				_rights: {
					update: true,
				},
			};

			self.errors = {
				fields: {},
			};

			self.loading = {
				save: false,
			};

			self.apiParams = {
				col: ['id', 'name', 'createdAt', 'updatedAt'],
			};

			self.options = {
				name: 'budgetCode-list',
				col: self.apiParams.col,
				searchType: 'list',
				sort: 'name',
				order: 'asc',
				pager: true,
				getItems: function(params, callback, abort) {
					params.search.data.filters['project.id'] = {
						op: 'eq',
						val: self.project_id,
					};

					budgetCodeService.findAll(params, abort.promise).then(function(res) {
						callback(res);
					});
				},
			};

			$scope.$watch(
				() => self.budgetCodes,
				() => {
					$rootScope.$broadcast('loadProject');
				},
			);

			self.init = function(project_id) {
				self.project_id = project_id;
			};

			self.openModal = function(budgetCode) {
				self.budgetCode = budgetCode
					? angular.copy(budgetCode)
					: {
							_rights: {
								update: true,
							},
					  };

				self.panelTitle = self.budgetCode.name || '';

				self.modal.modal('show');
			};

			self.save = function() {
				self.errors.fields = {};
				self.loading.save = true;

				const isCreation = !self.budgetCode.id;

				budgetCodeService
					.save({ ...self.budgetCode, project: self.project_id }, self.apiParams)
					.then(
						res => {
							self.budgetCode = res.object;
							self.panelTitle = self.budgetCode.name;
							self.loading.save = false;

							flashMessenger.success(translator('budgetCode_message_saved'));
							self.modal.modal('hide');

							if (isCreation) {
								self.budgetCodes = [...self.budgetCodes, self.budgetCode];

								return;
							}

							self.budgetCodes = self.budgetCodes.map(budgetCode =>
								budgetCode.id === self.budgetCode.id ? self.budgetCode : budgetCode,
							);
						},
						err => {
							self.errors.fields = err.fields;
							self.loading.save = false;

							flashMessenger.error(translator('error_occured'));
						},
					);
			};

			self.delete = function(budgetCodeToDelete) {
				if (
					confirm(translator('budgetCode_question_delete').replace('%1', budgetCodeToDelete.name))
				) {
					budgetCodeService.remove(budgetCodeToDelete).then(
						res => {
							if (!res.success) {
								flashMessenger.error(translator('error_occured'));

								return;
							}

							self.budgetCodes = self.budgetCodes.filter(
								budgetCode => budgetCode.id !== budgetCodeToDelete.id,
							);

							flashMessenger.success(translator('budgetCode_message_deleted'));
						},
						() => {
							flashMessenger.error(translator('error_occured'));
						},
					);
				}
			};

			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};

			self.editLine = function(budgetCode) {
				self.editedLineErrors = {};
				self.editedLine = budgetCode;
				self.editedLineFields = angular.copy(budgetCode);
			};

			self.saveEditedLine = function(budgetCode) {
				self.editedLineErrors = {};

				budgetCodeService
					.save(
						angular.extend({}, budgetCode, self.editedLineFields, { project: self.project_id }),
						{
							col: self.options.col,
						},
					)
					.then(
						res => {
							if (!res.success) {
								flashMessenger.error(translator('error_occured'));

								return;
							}

							budgetCode = angular.extend(budgetCode, res.object);

							self.editedLine = null;
							self.editedLineFields = null;

							flashMessenger.success(translator('budgetCode_message_saved'));
						},
						err => {
							for (let field in err.fields) {
								self.editedLineErrors[field] = err.fields[field];
							}

							flashMessenger.error(translator('error_occured'));
						},
					);
			};

			self.fullTextSearch = function() {
				$scope.__tb.resetFilters();
				$scope.__tb.apiParams.search.data.full = self.fullText;
				$scope.__tb.loadData();
			};

			self.resetfullTextSearch = function() {
				self.fullText = '';
				delete $scope.__tb.apiParams.search.data.full;
			};

			angular.element('#full-text').on('keypress', function(event) {
				if (event.which == 13) {
					self.fullTextSearch();
				}
			});
		},
	]);
})(window.EVA.app);
