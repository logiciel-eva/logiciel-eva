<?php

namespace PostParc\Helper;

use Directory\Entity\Contact;
use Ramsey\Uuid\Uuid;

class DataMapper
{
    private const POST_PARC_GENDER_MADAME = "Madame";
    private const POST_PARC_GENDER_MONSIEUR = "Monsieur";

    private const PERSON_GENDER_M = 'm';
    private const PERSON_GENDER_MME = 'mme';

    public function mapStructure($postParcOrganisation, $structure)
    {
        $structure->setName($postParcOrganisation->name);
        $structure->setSigle($postParcOrganisation->abbreviation);

        if (isset($postParcOrganisation->coordinate)) {
            $structure->setEmail($postParcOrganisation->coordinate->email);
            $structure->setPhone($postParcOrganisation->coordinate->phone);
            $structure->setPortable($postParcOrganisation->coordinate->mobilePhone);
            $structure->setAddressLine1($postParcOrganisation->coordinate->addressLine1);
            $structure->setAddressLine2($postParcOrganisation->coordinate->addressLine2);
            $structure->setZipcode($postParcOrganisation->coordinate->cedex);
            $structure->setCity($postParcOrganisation->coordinate->city);
            $structure->setWebsite($postParcOrganisation->coordinate->webSite);
        }
    }

    public function mapContact($postPerson, $contact)
    {
        $contact->setUuid(Uuid::uuid4());
        $contact->setVip(false);
        $contact->setSubscriptionStatus(Contact::SUBSCRIPTION_STATUS_EMPTY);
        $contact->setGender($this->mapGender($postPerson->civility));
        if (null === $postPerson->firstName) {
            $contact->setFirstName('');
        } else {
            $contact->setFirstName($postPerson->firstName);
        }

        $contact->setLastName($postPerson->name);

        if (isset($postPerson->coordinate)) {
            $contact->setEmail($postPerson->coordinate->email);
            $contact->setPhone($postPerson->coordinate->phone);
            $contact->setPortable($postPerson->coordinate->mobilePhone);
            $contact->setAddressLine1($postPerson->coordinate->addressLine1);
            $contact->setAddressLine2($postPerson->coordinate->addressLine2);
            $contact->setZipcode($postPerson->coordinate->cedex);
            $contact->setCity($postPerson->coordinate->city);
        }

    }

    private function mapGender($postParcGender)
    {
        if ($postParcGender == self::POST_PARC_GENDER_MADAME) {
            return self::PERSON_GENDER_MME;
        }

        if ($postParcGender == self::POST_PARC_GENDER_MONSIEUR) {
            return self::PERSON_GENDER_M;
        }

        return null;
    }
}
