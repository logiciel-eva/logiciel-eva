(function(app) {

    app.directive('contactSelect', [
        'contactService', 'selectFactoryService',
        function contactSelect(contactService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: contactService
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name'],
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' +
                                '<span class="name">' + escape(data.id) + ' - ' + escape(data.name) + '</span>' +
                                '</div>';
                        },
                        item: function (data, escape) {
                            return '<div class="option">' +
                                '<span class="name">' + escape(data.id) + ' - ' + escape(data.name) + '</span>' +
                                '</div>';
                        }
                    }
                }
            });
        }
    ])

})(window.EVA.app);
