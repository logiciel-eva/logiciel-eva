<?php

namespace User\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class UserController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('user:e:user:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('user', false);
        }

        return new ViewModel([
            'groups' => $groups
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('user:e:user', $id)) {
            return $this->notFoundAction();
        }

        $user = $this->entityManager()->getRepository('User\Entity\User')->find($id);

        if ($user && !$this->acl()->ownerControl('user:e:user:u', $user) && !$this->acl()->ownerControl('user:e:user:r', $user)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'user' => $user
        ]);
    }
}
