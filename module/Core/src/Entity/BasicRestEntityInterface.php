<?php

namespace Core\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use User\Entity\User;
use Laminas\InputFilter\InputFilter;

interface BasicRestEntityInterface
{
    /**
     * @return InputFilter
     */
    public function getInputFilter(EntityManager $entityManager);

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * @return User
     */
    public function getCreatedBy();

    /**
     * @return User
     */
    public function getUpdatedBy();

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $user
     * @param string $owner
     */
    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '');
}
