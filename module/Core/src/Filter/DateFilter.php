<?php

namespace Core\Filter;

use Laminas\Filter\FilterInterface;

class DateFilter implements FilterInterface
{
    public function filter($value)
    {
        if ($value) {
            if ($this->isFrenchFormat($value)) {
                $valueFiltered = \DateTime::createFromFormat('d/m/Y', $value);
            } else {
                $valueFiltered = new \DateTime($value);
            }
            return $valueFiltered;
        }

        return null;
    }

    public function isFrenchFormat($value)
    {
        return preg_match('#\d{2}/\d{2}/\d{4}#', $value);
    }
}
