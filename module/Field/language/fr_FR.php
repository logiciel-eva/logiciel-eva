<?php

return [
    'field_module_title' => 'Champs personnalisables',

    'fieldGroup_entity'   => 'groupe de [[ field_entities ]]',
    'fieldGroup_entities' => 'groupes de [[ field_entities ]]',

    'field_entity'   => 'champ',
    'field_entities' => 'champs',

    'field_field_name'  => 'Titre',
    'field_field_help'  => 'Aide',
    'field_field_placeholder'  => 'Placeholder',
    'field_field_type'  => 'Type',
    'field_field_order' => 'Ordre',
    'field_field_template' => 'Modèles de fiches',

    'field_type_text'           => 'Ligne de texte',
    'field_type_textarea'       => 'Bloc de texte',
    'field_type_radio'          => 'Sélection (radio)',
    'field_type_select'         => 'Sélection (liste)',
    'field_type_checkbox'       => 'Sélection multiple',
    'field_type_date'           => 'Date',
    'field_type_datetime'       => 'Date avec Heures',
    'field_type_consent'        => 'Consentement',
    'field_type_amount'         => 'Montant',
    'field_field_meta_required' => 'Obligatoire ?',
    'field_field_meta_unique'   => 'Doit-il être unique ?',

    'field_field_group_error_non_exists' => 'Ce [[ fieldGroup_entity ]] n\'existe pas ou plus',

    'field_nav_list' => 'Liste des {{ __tb.total }} [[ field_entities ]]',
    'field_nav_form' => 'Créer un [[ field_entity ]]',

    'field_add_value' => 'Créer une valeur',

    'fieldGroup_nav_form' => 'Créer un [[ fieldGroup_entity ]]',

    'fieldGroup_enabled' => 'Activé ?',
    'fieldGroup_englet' => 'Onglet',
    'fieldGroup_englet_team' => 'Équipe',
    'fieldGroup_englet_indicateur' => 'Indicateurs',
    'fieldGroup_englet_territoire' => 'Territoires',
    'fieldGroup_englet_acteur' => 'Acteurs',
    'fieldGroup_englet_document' => 'Documents',
    'fieldGroup_englet_fiche' => 'Fiches',

    // project
    'fieldGroup_tab_project' => 'Fiche',
    'fieldGroup_tab_members' => 'Equipe',
    'fieldGroup_tab_hierarchy' => 'Plan d\'action',
    'fieldGroup_tab_field' => 'Champs personnalisables',
    'fieldGroup_tab_actors' => 'Acteurs',
    'fieldGroup_tab_territory' => 'Territoires',
    'fieldGroup_tab_time' => 'Temps',
    'fieldGroup_tab_indicator' => 'Indicateurs',
    'fieldGroup_tab_budget' => 'Budget',
    'fieldGroup_tab_convention' => 'Conventions',
    'fieldGroup_tab_advancement' => 'Avancement',
    'fieldGroup_tab_task' => 'Tâches',
    'fieldGroup_tab_attachments' => 'Documents',

    'fieldGroup_message_saved'   => '[[ fieldGroup_entity | ucfirst ]] enregistré',
    'fieldGroup_question_delete' => 'Voulez-vous réellement supprimer le [[ fieldGroup_entity ]] %1 ?',
    'fieldGroup_message_deleted' => '[[ fieldGroup_entity | ucfirst ]] supprimé',
    'fieldGroup_message_enabled' => '[[ fieldGroup_entity | ucfirst ]] activé',
    'fieldGroup_message_disabled' => '[[ fieldGroup_entity | ucfirst ]] désactivé',

    'field_message_saved'   => '[[ field_entity | ucfirst ]] enregistré',
    'fields_message_saved' => '[[ field_module_title ]] enregistrés',
    'field_question_delete' => 'Voulez-vous réellement supprimer le [[ field_entity ]] %1 ?',
    'field_message_deleted' => '[[ field_entity | ucfirst ]] supprimé',

    'fieldValue_field_field_error_non_exists' => 'Ce [[ field_entity ]] n\'existe pas ou plus',
    'fieldValue_field_value_should_be_unique' => 'Cette valeur est déjà utilisée',
    'fieldValue_field_value_is_required' => 'Cette valeur est requise',
    'fieldValue_field_value_is_null' => 'value_not_created',

    'reset_value'                              => 'Enlever la valeur',
];
