(function(app) {
	app.factory('importConfigurationService', [
		'restFactoryService', '$http', '$q',
		function (restFactoryService ,$http, $q) {
			let url = '/api/budget/import-configuration';

			let methods = restFactoryService.create(url);

			methods.run = function run(id) {
				let deferred = $q.defer();

				$http({
					method: 'POST',
					url: url + '/' + id + '/run',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).then(function(res) {
					deferred.resolve(res);
				}, function(err) {
					deferred.reject(err);
				});

				return deferred.promise;
			};

			return methods;
		},
	]);
})(window.EVA.app);
