<?php

namespace Age\Model;

use JsonSerializable;

class AgeCheckRequest implements JsonSerializable
{


    private $Reference;
    private $Etablissement;
    private $ListeIdentifiants;



    /**
     * Get the value of reference
     */
    public function getReference()
    {
        return $this->Reference;
    }

    /**
     * Set the value of reference
     *
     * @return  self
     */
    public function setReference($reference)
    {
        $this->Reference = $reference;

        return $this;
    }

    /**
     * Get the value of etablissement
     */
    public function getEtablissement()
    {
        return $this->Etablissement;
    }

    /**
     * Set the value of etablissement
     *
     * @return  self
     */
    public function setEtablissement($etablissement)
    {
        $this->Etablissement = $etablissement;

        return $this;
    }

    /**
     * Get the value of listeIdentifiants
     */
    public function getListeIdentifiants()
    {
        return $this->ListeIdentifiants;
    }

    /**
     * Set the value of listeIdentifiants
     *
     * @return  self
     */
    public function setListeIdentifiants($listeIdentifiants)
    {
        $this->ListeIdentifiants = $listeIdentifiants;

        return $this;
    }

    public function addIdentifiant($identifiant)
    {
        if (null === $this->ListeIdentifiants) {
            $this->ListeIdentifiants = [];
        }
        $this->ListeIdentifiants[] = $identifiant;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
