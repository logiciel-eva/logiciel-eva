(function(app) {

    app.factory('projectService', [
        '$http', '$q', 'restFactoryService',
        function projectService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/project'), {
                getTree: function getTree(params) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/project/tree',
                        data: $.param(params),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                }
            });
        }
    ])

})(window.EVA.app);
