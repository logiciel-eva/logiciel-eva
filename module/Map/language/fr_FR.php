<?php

return [
    'map_module_title' => 'Cartographie',

    'territory_module_title' => '[[ territory_entities | ucfirst ]]',
    'territory_nav_list'     => 'Liste des {{ __tb.total }} [[ territory_entities ]]',
    'territory_nav_import'   => 'Importer des [[ territory_entities ]]',
    'territory_nav_form'     => 'Créer un [[ territory_entity ]]',
    'territory_fusion_title' => 'Fusionner des [[ territory_entities ]]',

    'territory_field_name' => 'Nom',
    'territory_field_code' => 'Code',
    'territory_field_createdAt' => '[[ field_created_at_m ]]',
    'territory_field_updatedAt' => '[[ field_updated_at_m ]]',
    'territory_field_json_upload_file' => 'Utiliser un fichier pour tracer les contours du [[ territory_entity ]] (*.geojon, *.json, *.kml, *.gpx)',
    'territory_field_children' => '[[ territory_entities | ucfirst ]] enfants',
    'territory_field_services' => '[[ service_entities | ucfirst ]]',

    'territory_import_upload_file'  => 'Sélectionnez un fichier (*.geojon, *.json, *.kml, *.gpx)',
    'territory_import_wrong_format' => '[[ territory_field_json_wrong_format ]]',
    'territory_import_focus' => 'Zoomer sur le [[ territory_entity ]]',
    'territory_some_names_are_null' => 'Enregistrement impossible : certains territoires n\'ont pas de nom',
    'territory_error_on_some_territories' => 'Certains territoires n\'ont pas pu être enregistrés',
    'territories_message_saved' => '[[ territory_entities | ucfirst ]] enregistrés',

    'territory_message_saved'   => '[[ territory_entity | ucfirst ]] enregistré',
    'territory_message_deleted'   => '[[ territory_entity | ucfirst ]] supprimé',
    'territory_question_delete' => 'Voulez-vous réellement supprimer le [[ territory_entity ]] %1 ?',

    'territory_field_json_wrong_format' => 'Ce format de fichier n\'est pas prise en charge',

    'territory_entity'   => 'territoire',
    'territory_entities' => 'territoires',

    'tooltip_switch_view_territory_tree' => 'Vue arborescente',
    'tooltip_switch_view_territory_list' => 'Vue liste',
];
