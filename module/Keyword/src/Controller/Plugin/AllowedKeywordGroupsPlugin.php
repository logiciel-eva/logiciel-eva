<?php

namespace Keyword\Controller\Plugin;

use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class AllowedKeywordGroupsPlugin
 *
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AllowedKeywordGroupsPlugin extends AbstractPlugin
{
    /**
     * @var AllowedKeywordGroups
     */
    protected $allowedKeywordGroups;

    /**
     * @param AllowedKeywordGroups $allowedKeywordGroups
     */
    public function __construct(AllowedKeywordGroups $allowedKeywordGroups)
    {
        $this->allowedKeywordGroups = $allowedKeywordGroups;
    }

    public function __invoke($entity, $archived = null, $search = null)
    {
        return $this->allowedKeywordGroups->findByEntity($entity, $archived, $search);
    }
}
