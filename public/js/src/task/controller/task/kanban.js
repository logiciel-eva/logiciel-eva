(function(app) {

    app.controller('taskKanbanController', [
        '$scope', 'groupService', 'taskService',
        function taskKanbanController($scope, groupService, taskService) {
            var self = this;

            self.groups      = [];
            self.group       = null;
            self.tasks       = [];
            self.unaffecteds = [];

            groupService.findAll({
                col: [
                    'id',
                    'name',
                    'keywords.id',
                    'keywords.name'
                ],
                search : {
                    type: 'list',
                    data: {
                        sort: 'name',
                        order: 'asc',
                        filters: {
                            multiple: {
                                op: 'eq',
                                val: '0'
                            },
                            entities: {
                                op: 'sa_cnt',
                                val: 'task'
                            }
                        }
                    }
                }
            }).then(function(res) {
                self.groups = res.rows;
                if (self.groups.length > 0) {
                    self.group = self.groups[0];
                }
            });

            $scope.$watch(function() {
                return $scope.$parent.$parent.self.tasks
            }, function () {
                self.tasks = $scope.$parent.$parent.self.tasks;
                self.distribute();
            });

            $scope.$watch(function() {
                return self.group;
            }, function () {
                self.distribute();
            });

            $scope.sortableOptions = {
                placeholder: 'task-card-empty',
                connectWith: '.body',
                receive: function (event, ui) {
                    var task = ui.item.sortable.model;

                    var from = $(ui.item.sortable.sourceList).data('keyword');
                    var to   = $(ui.item.sortable.droptargetList).data('keyword');

                    if (angular.isArray(task.keywords)) {
                        task.keywords = {};
                    }

                    if (from !== null) {
                        for (var i in task.keywords[self.group.id]) {
                            if (task.keywords[self.group.id][i].id === from) {
                                task.keywords[self.group.id].splice(i, 1);
                            }
                        }
                    }

                    if (to !== null) {
                        if (typeof task.keywords[self.group.id] === 'undefined') {
                            task.keywords[self.group.id] = [];
                        }

                        task.keywords[self.group.id].push({
                            id: to
                        })
                    }

                    taskService.save(task).then(function(res) {

                    })
                }
            };

            self.distribute = function () {
                for (var i in self.group.keywords) {
                    self.group.keywords[i].tasks = [];
                    self.group.keywords[i].filteredTasks = [];
                }

                self.unaffecteds = [];

                for (var i in self.tasks) {
                    var task = self.tasks[i];
                    if (typeof task.keywords[self.group.id] === 'undefined') {
                        self.unaffecteds.push(task);
                    } else {
                        for (var j in task.keywords[self.group.id]) {
                            var keyword = task.keywords[self.group.id][j];
                            for (var k in self.group.keywords) {
                                if (self.group.keywords[k].id === keyword.id) {
                                    self.group.keywords[k].tasks.push(task);
                                }
                            }
                        }
                    }
                }
            };

            self.customFilter = function (task, filter) {
                if (!filter) {
                    return true;
                }

                return task.name.toLowerCase().contains(filter.toLowerCase());
            };
        }
    ])

})(window.EVA.app);
