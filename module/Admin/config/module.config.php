<?php

namespace Admin;

return [
    'module' => [
        'admin' => [
            'environments' => [
                
            ],

            'active'   => true,
            'required' => true,

            'acl' => [
                'entities' => [
                    [
                        'name'  => 'client',
                        'class' => 'Bootstrap\Entity\Client',
                        'owner' => []
                    ],
                    [
                        'name'  => 'network',
                        'class' => 'Bootstrap\Entity\Network',
                        'owner' => []
                    ]
                ]
            ]
        ]
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ]
];
