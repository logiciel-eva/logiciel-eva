(function(app) {

    app.factory('indicatorService', [
        'restFactoryService',
        function indicatorService(restFactoryService) {
            return restFactoryService.create('/api/indicator');
        }
    ])

})(window.EVA.app);
