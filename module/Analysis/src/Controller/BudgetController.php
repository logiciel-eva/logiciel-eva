<?php

/**
 * Attention, il manque pas mal de contrôle d'existance de variables et tableaux qui renvoi des NOTICES.
 * Ca passe en PHP prod, mais pas en PHP dev.
 */

namespace Analysis\Controller;

use Budget\Entity\Expense;
use Budget\Entity\Income;
use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use DateTime;
use Export\Entity\Custom;
use Export\Entity\Export;
use Keyword\Entity\Group;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Stdlib\Parameters;
use Laminas\View\HelperPluginManager;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class BudgetController extends AbstractActionSLController
{
	/**
	 * @var boolean
	 */
	protected $isMaster;

	/**
	 * @var HelperPluginManager
	 */
	protected $helper;

	/**
	 * @var array
	 */
	protected $expenseTypes;

	/**
	 * @var array
	 */
	protected $incomeTypes;

	/**
	 * @var array
	 */
	protected $posteExpenseBalances;

	/**
	 * @var array
	 */
	protected $posteIncomeBalances;

	public function onDispatch(MvcEvent $e)
	{
		ini_set('memory_limit', -1);
		ini_set('max_execution_time', -1);

		return parent::onDispatch($e);
	}

	public function indexAction()
	{
		$helper = $this->serviceLocator->get('ViewHelperManager');

		$projectGroups = [];
        $expenseGroups = [];
        $posteExpenseGroups = [];
        $incomeGroups = [];
        $posteIncomeGroups = [];
		if ($this->moduleManager()->isActive('keyword')) {
			$projectGroups = $this->allowedKeywordGroups('project', false);
            $expenseGroups = $this->allowedKeywordGroups('expense', false);
	        $posteExpenseGroups = $this->allowedKeywordGroups('posteExpense', false);
            $incomeGroups = $this->allowedKeywordGroups('income', false);
            $posteIncomeGroups = $this->allowedKeywordGroups('posteIncome', false);
		}

		$customExportsWord = [];
		$customExportsExcel = [];
		$customExports = Custom::getExports(
			$this->environment()
				->getClient()
				->getKey(),
			'analysis/budget'
		);

		/** @var Custom $export */
		foreach ($customExports as $export) {
			if ($export->getType() === Export::TYPE_WORD) {
				$customExportsWord[] = $export;
			} elseif ($export->getType() === Export::TYPE_EXCEL) {
				$customExportsExcel[] = $export;
			}
		}

		return new ViewModel([
			'isMaster' => $this->environment()
				->getClient()
				->isMaster(),
			'ht' => $this->moduleManager()->getClientModuleConfiguration('budget', 'ht'),
			'gbcp' => $this->moduleManager()->getClientModuleConfiguration('budget', 'gbcp'),
			'expenseTypes' => $helper->get('expenseTypes')(),
			'incomeTypes' => $helper->get('incomeTypes')(),
			'posteIncomeBalances' => $helper->get('posteIncomeBalances')(),
			'posteExpenseBalances' => $helper->get('posteExpenseBalances')(),
			'projectGroups' => $projectGroups,
            'expenseGroups' => $expenseGroups,
            'posteExpenseGroups'=> $posteExpenseGroups,
            'incomeGroups'=> $incomeGroups,
            'posteIncomeGroups'=> $posteIncomeGroups,
			'customExportsWord' => $customExportsWord,
			'customExportsExcel' => $customExportsExcel,
		]);
	}

	public function expenseAction()
	{
		$this->isMaster = $this->environment()
			->getClient()
			->isMaster();

		$this->helper = $this->serviceLocator->get('ViewHelperManager');
		$this->expenseTypes = $this->helper->get('expenseTypes')();
		$this->posteExpenseBalances = $this->helper->get('posteExpenseBalances')();

		$filters = $this->params()->fromQuery('filters', []);
		$columns = $this->params()->fromQuery('columns', []);
		$export = $this->params()->fromQuery('export', false);
		$arbo = $this->params()->fromQuery('arbo', false) === 'true';
		$isSubvention = $this->params()->fromQuery('isSubvention', false) === 'true';
		
		foreach (['project', 'posteExpense', 'expense'] as $filter) {
			if (!isset($filters[$filter])) {
				$filters[$filter] = [];
			}
		}

		if ($export === 'analysis/budget_erd') {
			if (!$filters['expense']) {
				$filters['expense'] = [];
			}

			$filters['expense']['type'] = [
				'op' => 'in',
				'val' => ['cpCommitted', 'cpEstimated'],
			];
		}

		$projects = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
			'col' => $this->expenseApiCol($arbo, $export),
			'search' => [
				'type' => 'list',
				'data' => [
					'sort' => 'code',
					'order' => 'asc',
					'filters' => $filters['project'],
				],
			],
			'posteExpenseFilters' => $filters['posteExpense'],
			'expenseFilters' => $filters['expense'],
			'master' => $this->isMaster,
		])
			->dispatch()
			->getVariable('rows');

		if (count($projects) === 0) {
			return new JsonModel([]);
		}

		$projects = $this->normalizeProjects($projects, $arbo);
		$projects = $this->makeTree($projects);
		$projects = $this->flatTree($projects, [], 0);

		if (!$export) {
			return new JsonModel($projects);
		}

		$this->expenseExport($projects, $columns, $export, $isSubvention);

		exit();
	}

	public function incomeAction()
	{
		$this->isMaster = $this->environment()
			->getClient()
			->isMaster();

		$this->helper = $this->serviceLocator->get('ViewHelperManager');
		$this->incomeTypes = $this->helper->get('incomeTypes')();
		$this->posteIncomeBalances = $this->helper->get('posteIncomeBalances')();

		$filters = $this->params()->fromQuery('filters', []);
		$columns = $this->params()->fromQuery('columns', []);
		$export = $this->params()->fromQuery('export', false);
		$arbo = $this->params()->fromQuery('arbo', false) === 'true';

		foreach (['project', 'posteIncome', 'income'] as $filter) {
			if (!isset($filters[$filter])) {
				$filters[$filter] = [];
			}
		}

		$projects = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
			'col' => $this->incomeApiCol($arbo, $export),
			'search' => [
				'type' => 'list',
				'data' => [
					'sort' => 'code',
					'order' => 'asc',
					'filters' => $filters['project'],
				],
			],
			'posteIncomeFilters' => $filters['posteIncome'],
			'incomeFilters' => $filters['income'],
			'master' => $this->isMaster,
		])
			->dispatch()
			->getVariable('rows');

		if (count($projects) === 0) {
			return new JsonModel([]);
		}

		$projects = $this->normalizeProjects($projects, $arbo);
		$projects = $this->makeTree($projects);
		$projects = $this->flatTree($projects, [], 0);

		if (!$export) {
			return new JsonModel($projects);
		}

		$this->incomeExport($projects, $columns, $export);

		exit();
	}

	public function gbcpAction()
	{
		$filters = $this->params()->fromQuery('filters', []);
		$columns = $this->params()->fromQuery('columns', []);
		$export = $this->params()->fromQuery('export', false);

		$gbcpExpenseTypes = [
			Expense::TYPE_AE,
			Expense::TYPE_AE_COMMITTED,
			Expense::TYPE_AE_ESTIMATED,
			Expense::TYPE_CP,
			Expense::TYPE_CP_COMMITTED,
			Expense::TYPE_CP_ESTIMATED,
		];

		$data = [];
		$byPass = false;

		$projects = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
			'col' => [
				'id',
				'code',
				'name',
				'posteExpensesArbo.id',
				'posteExpensesArbo.name',
				'posteExpensesArbo.nature.id',
				'posteExpensesArbo.nature.code',
				'posteExpensesArbo.nature.name',
				'posteExpensesArbo.organization.id',
				'posteExpensesArbo.organization.code',
				'posteExpensesArbo.organization.name',
				'posteExpensesArbo.destination.id',
				'posteExpensesArbo.destination.code',
				'posteExpensesArbo.destination.name',
				'posteExpensesArbo.expensesArbo.id',
				'posteExpensesArbo.expensesArbo.type',
				'posteExpensesArbo.expensesArbo.name',
				'posteExpensesArbo.expensesArbo.amount',
				'posteExpensesArbo.expensesArbo.amountHT',
				'posteExpensesArbo.expensesArbo.managementUnit.name',
				'posteIncomesArbo.id',
				'posteIncomesArbo.name',
				'posteIncomesArbo.nature.id',
				'posteIncomesArbo.nature.code',
				'posteIncomesArbo.nature.name',
				'posteIncomesArbo.organization.id',
				'posteIncomesArbo.organization.code',
				'posteIncomesArbo.organization.name',
				'posteIncomesArbo.destination.id',
				'posteIncomesArbo.destination.code',
				'posteIncomesArbo.destination.name',
				'posteIncomesArbo.incomesArbo.id',
				'posteIncomesArbo.incomesArbo.type',
				'posteIncomesArbo.incomesArbo.name',
				'posteIncomesArbo.incomesArbo.amount',
				'posteIncomesArbo.incomesArbo.amountHT',
				'posteIncomesArbo.incomesArbo.managementUnit.name',
			],
			'search' => [
				'type' => 'list',
				'data' => [
					'sort' => 'id',
					'order' => 'asc',
					'filters' => $filters['project'],
				],
			],
			'posteExpenseFilters' => $filters['posteExpense'],
			'posteIncomeFilters' => $filters['posteIncome'],
			'expenseFilters' => $filters['expense'],
			'incomeFilters' => $filters['income'],
		])
			->dispatch()
			->getVariable('rows');

		if (count($projects) === 0) {
			$byPass = true;
		}

		if (!$byPass) {
			foreach ($projects as $project) {
				foreach ($project['posteExpensesArbo'] as $posteExpense) {
					if (
						!$posteExpense['nature'] ||
						!$posteExpense['organization'] ||
						!$posteExpense['destination']
					) {
						continue;
					}

					$key = $this->gbcpGetKey($project, $posteExpense);

					if (!isset($data[$key])) {
						$data[$key] = $this->gbcpGetDataLine($project, $posteExpense, $gbcpExpenseTypes);
					}

					foreach ($posteExpense['expensesArbo'] as $expense) {
						if (!in_array($expense['type'], $gbcpExpenseTypes)) {
							continue;
						}

						$data[$key]['transactions'][] = [
							'id' => $expense['id'],
							'type' => $expense['type'],
							'name' => $expense['name'],
							'amount' => $expense['amount'],
							'amountHT' => $expense['amountHT'],
							'poste' => [
								'id' => $posteExpense['id'],
								'name' => $posteExpense['name'],
							],
						];

						$data[$key]['total'][$expense['type']] += $expense['amount'];
						$data[$key]['total']["{$expense['type']}_ht"] += $expense['amountHT'];
					}
				}

				foreach ($project['posteIncomesArbo'] as $posteIncome) {
					if (
						!$posteIncome['nature'] ||
						!$posteIncome['organization'] ||
						!$posteIncome['destination']
					) {
						continue;
					}

					$key = $this->gbcpGetKey($project, $posteIncome);

					if (!isset($data[$key])) {
						$data[$key] = $this->gbcpGetDataLine($project, $posteIncome, $gbcpExpenseTypes);
					}

					foreach ($posteIncome['incomesArbo'] as $income) {
						$data[$key]['transactions'][] = [
							'id' => $income['id'],
							'type' => 'income',
							'name' => $income['name'],
							'amount' => $income['amount'],
							'amountHT' => $income['amountHT'],
							'poste' => [
								'id' => $posteIncome['id'],
								'name' => $posteIncome['name'],
							],
						];

						$data[$key]['total']['income'] += $income['amount'];
						$data[$key]['total']['income_ht'] += $income['amountHT'];
					}
				}
			}

			foreach ($data as $key => $datum) {
				foreach ($datum['total'] as $type => $value) {
					if (!preg_match('/_ht$/', $type) && $value > 0) {
						continue 2;
					}
				}

				unset($data[$key]);
			}
		}

		$data = array_values($data);

		if (!$export) {
			return new JsonModel($data);
		}

		$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
		$translator = $helperPluginManager->get('translate');

		$total = $this->gbcpGetTotalLine($gbcpExpenseTypes);

		$linesBolded = [];
		$xlsLines = [];
		$header = [];

		if (in_array('project.code', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_code'));
		}

		if (in_array('project.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_name'));
		}

		if (in_array('nature.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('nature_entity'));
		}

		if (in_array('organization.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('organization_entity'));
		}

		if (in_array('destination.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('destination_entity'));
		}

		if (in_array('transaction.poste.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_entity'));
		}

		if (in_array('transaction.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('transaction_entity'));
		}

		foreach ($gbcpExpenseTypes as $type) {
			if (in_array("transaction.type.$type", $columns)) {
				$header[] = ucfirst($translator->__invoke("expense_type_$type"));
			}

			if (in_array("transaction.type.{$type}_ht", $columns)) {
				$header[] = ucfirst($translator->__invoke("expense_type_{$type}_ht"));
			}
		}

		if (in_array('transaction.type.income', $columns)) {
			$header[] = ucfirst($translator->__invoke('income_entity'));
		}

		if (in_array('transaction.type.income_ht', $columns)) {
			$header[] = ucfirst($translator->__invoke('income_entity_ht'));
		}

		$xlsLines[] = $header;

		foreach ($data as $i => $row) {
			$rowLine = [];

			if (in_array('project.code', $columns)) {
				$rowLine[] = $row['project']['code'];
			}

			if (in_array('project.name', $columns)) {
				$rowLine[] = $row['project']['name'];
			}

			if (in_array('nature.name', $columns)) {
				$rowLine[] = $row['nature']['name'];
			}

			if (in_array('organization.name', $columns)) {
				$rowLine[] = $row['organization']['name'];
			}

			if (in_array('destination.name', $columns)) {
				$rowLine[] = $row['destination']['name'];
			}

			if (in_array('transaction.poste.name', $columns)) {
				$rowLine[] = '';
			}

			if (in_array('transaction.name', $columns)) {
				$rowLine[] = '';
			}

			foreach ($gbcpExpenseTypes as $type) {
				if (in_array("transaction.type.$type", $columns)) {
					$rowLine[] = $row['total'][$type];
				}

				if (in_array("transaction.type.{$type}_ht", $columns)) {
					$rowLine[] = $row['total']["{$type}_ht"];
				}
			}

			if (in_array('transaction.type.income', $columns)) {
				$rowLine[] = $row['total']['income'];
			}

			if (in_array('transaction.type.income_ht', $columns)) {
				$rowLine[] = $row['total']['income_ht'];
			}

			$xlsLines[] = $rowLine;

			foreach ($row['total'] as $type => $value) {
				$total[$type] += $value;
			}

			if (
				!in_array('transaction.poste.name', $columns) &&
				!in_array('transaction.name', $columns)
			) {
				continue;
			}

			$linesBolded[] = count($xlsLines);

			foreach ($row['transactions'] as $transaction) {
				$transactionLine = [];

				if (in_array('project.code', $columns)) {
					$transactionLine[] = $project['code'];
				}

				if (in_array('project.name', $columns)) {
					$transactionLine[] = '';
				}

				if (in_array('nature.name', $columns)) {
					$transactionLine[] = '';
				}

				if (in_array('organization.name', $columns)) {
					$transactionLine[] = '';
				}

				if (in_array('destination.name', $columns)) {
					$transactionLine[] = '';
				}

				if (in_array('transaction.poste.name', $columns)) {
					$transactionLine[] = $transaction['poste']['name'];
				}

				if (in_array('transaction.name', $columns)) {
					$transactionLine[] = $transaction['name'];
				}

				foreach ($gbcpExpenseTypes as $type) {
					if (in_array("transaction.type.$type", $columns)) {
						$transactionLine[] = $transaction['type'] === $type ? $transaction['amount'] : '';
					}

					if (in_array("transaction.type.{$type}_ht", $columns)) {
						$transactionLine[] = $transaction['type'] === $type ? $transaction['amountHT'] : '';
					}
				}

				if (in_array('transaction.type.income', $columns)) {
					$transactionLine[] = $transaction['type'] === 'income' ? $transaction['amount'] : '';
				}

				if (in_array('transaction.type.income_ht', $columns)) {
					$transactionLine[] = $transaction['type'] === 'income' ? $transaction['amountHT'] : '';
				}

				$xlsLines[] = $transactionLine;
			}
		}

		$totalLine = [];
		$totalInserted = false;

		if (in_array('project.code', $columns)) {
			$totalLine[] = 'Total';
			$totalInserted = true;
		}

		if (in_array('project.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('nature.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('organization.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('destination.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('transaction.poste.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('transaction.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
		}

		foreach ($gbcpExpenseTypes as $type) {
			if (in_array("transaction.type.$type", $columns)) {
				$totalLine[] = $total[$type];
			}

			if (in_array("transaction.type.{$type}_ht", $columns)) {
				$totalLine[] = $total["{$type}_ht"];
			}
		}

		if (in_array('transaction.type.income', $columns)) {
			$totalLine[] = $total['income'];
		}

		if (in_array('transaction.type.income_ht', $columns)) {
			$totalLine[] = $total['income_ht'];
		}

		$xlsLines[] = $totalLine;

		$linesBolded[] = count($xlsLines);

		ExcelExporter::download($xlsLines, $linesBolded);

		exit();
	}

	public function financerAction()
	{
		$filters = $this->params()->fromQuery('filters', []);
		$export = $this->params()->fromQuery('export', false);
		$groupBy = $filters['group'];

		if ($groupBy == 'keyword') {
			return $this->financerGroupedByKeywords($filters, $export);
		}

		$isMaster = $this->environment()
			->getClient()
			->isMaster();
		if ($isMaster) {
			$clients = $this->environment()
				->getClient()
				->getNetwork()
				->getClients();
			if (!isset($filters['project'])) {
				$filters['project'] = [];
			}

			$filters['project']['networkAccessible'] = [
				'op' => 'eq',
				'val' => 1,
			];
		} else {
			$clients = [$this->environment()->getClient()];
		}

		$controllerManager = $this->serviceLocator->get('ControllerManager');
		$controllerProject = $controllerManager->get('Project\Controller\API\Project');
		$controllerStructure = $controllerManager->get('Directory\Controller\API\Structure');
		$controllerPosteIncome = $controllerManager->get('Budget\Controller\API\PosteIncome');

		$data = [
			'projects' => [],
			'financers' => [],
		];

		foreach ($clients as $client) {
			if ($client->isMaster()) {
				continue;
			}

			$bypass = false;

			$res = [
				'projects' => [],
				'financers' => [],
			];

			$posteIncomesFilters = [];

			if (isset($filters['project'])) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'code', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['project'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$e->setParam('master', $isMaster);
				$controllerProject->setEvent($e);

				$controllerProject->setClient($client);
				$projects = $controllerProject->dispatch($request)->getVariable('rows');

				if (sizeOf($projects) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['project'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($projects as $project) {
					$posteIncomesFilters['project']['val'][] = $project['id'];
					$project['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
					$project['data'] = [];
					$res['projects'][$project['id']] = $project;
				}
			}

			if (isset($filters['structure'])) {
				$filters['structure']['financer'] = [
					'op' => 'eq',
					'val' => 1,
				];
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['structure'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerStructure->setEvent($e);

				$controllerStructure->setClient($client);
				$financers = $controllerStructure->dispatch($request)->getVariable('rows');

				if (sizeOf($financers) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['financer.id'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($financers as $financer) {
					$posteIncomesFilters['financer.id']['val'][] = $financer['id'];
					$financer['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
					$res['financers'][$financer['id']] = $financer;
				}
			}

			if (!$bypass) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => [
							'id',
							'name',
							'project.id',
							'project.name',
							'envelope.financer.id',
							'envelope.financer.name',
							'incomesArbo.amount',
							'incomesArbo.type',
						],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $posteIncomesFilters,
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerPosteIncome->setEvent($e);

				$controllerPosteIncome->setClient($client);
				$posteIncomes = $controllerPosteIncome->dispatch($request)->getVariable('rows');
				foreach ($posteIncomes as $posteIncome) {
					$projectId = $posteIncome['project']['id'];
					$financerId = $posteIncome['envelope']['financer']['id'];

					if (!$financerId) {
						$financerId = 'na';
						$posteIncome['envelope']['financer'] = [
							'id' => 'na',
							'name' => 'N/A',
						];
					}

					$identifier = $client->getId() . '_' . $financerId;

					if (!array_key_exists($projectId, $res['projects'])) {
						$posteIncome['project']['_client'] = [
							'id' => $client->getId(),
							'name' => $client->getName(),
						];
						$res['projects'][$projectId] = $posteIncome['project'];
					}

					if (!array_key_exists($financerId, $res['financers'])) {
						$posteIncome['envelope']['financer']['_client'] = [
							'id' => $client->getId(),
							'name' => $client->getName(),
						];
						$res['financers'][$financerId] = $posteIncome['envelope']['financer'];
					}

					if (!array_key_exists('data', $res['projects'][$projectId])) {
						$res['projects'][$projectId]['data'] = [];
					}

					if (!array_key_exists('data', $res['financers'][$financerId])) {
						$res['financers'][$financerId]['data'] = [];
						foreach (\Budget\Entity\Income::getTypes() as $type) {
							$res['financers'][$financerId]['data'][$type] = 0;
						}
					}

					if (!array_key_exists($identifier, $res['projects'][$projectId]['data'])) {
						$res['projects'][$projectId]['data'][$identifier] = [];
						foreach (\Budget\Entity\Income::getTypes() as $type) {
							$res['projects'][$projectId]['data'][$identifier][$type] = 0;
						}
					}

					if ($posteIncome['incomesArbo']) {
						foreach ($posteIncome['incomesArbo'] as $income) {
							$res['projects'][$projectId]['data'][$identifier][$income['type']] +=
								$income['amount'];
							$res['financers'][$financerId]['data'][$income['type']] += $income['amount'];
						}
					}
				}

				$data['financers'] = array_merge($data['financers'], $res['financers']);
				$data['projects'] = array_merge($data['projects'], $res['projects']);

				foreach ($data['projects'] as $key => $project) {
					if (count($project['data']) === 0) {
						unset($data['projects'][$key]);
						continue;
					}

					$noValues = true;
					foreach ($project['data'] as $financer => $values) {
						foreach ($values as $type => $value) {
							if ($value !== 0) {
								$noValues = false;
								break 2;
							}
						}
					}

					if ($noValues) {
						unset($data['projects'][$key]);
					}
				}
			}
		}

		$data['financers'] = array_values($data['financers']);

		return $export ? $this->financerExport($data, ['first_header_key' => 'project_entity', 'data_key' => 'projects']) : new JsonModel($data);
	}

	public function financerGroupedByKeywords($filters, $export = false)
	{
		$isMaster = $this->environment()
			->getClient()
			->isMaster();
		if ($isMaster) {
			$clients = $this->environment()
				->getClient()
				->getNetwork()
				->getClients();
			if (!isset($filters['project'])) {
				$filters['project'] = [];
			}

			$filters['project']['networkAccessible'] = [
				'op' => 'eq',
				'val' => 1,
			];
		} else {
			$clients = [$this->environment()->getClient()];
		}

		$data = [
			'projects' => [],
			'financers' => [],
		];

		$controllerManager = $this->serviceLocator->get('ControllerManager');
		$controllerProject = $controllerManager->get('Project\Controller\API\Project');
		$controllerStructure = $controllerManager->get('Directory\Controller\API\Structure');
		$controllerPosteIncome = $controllerManager->get('Budget\Controller\API\PosteIncome');

		$groups = $this->allowedKeywordGroups('structure', false);
		$data['financers'][0] = [
			'id' => 0,
			'name' => 'N/A',
			'_client' => [
				'id' => 'kw',
			],
			'data' => [],
		];
		foreach (\Budget\Entity\Income::getTypes() as $type) {
			$data['financers'][0]['data'][$type] = 0;
		}
		foreach ($groups as $group) {
			foreach ($group->getKeywords() as $keyword) {
				$data['financers'][$keyword->getId()] = [
					'id' => $keyword->getId(),
					'name' => $keyword->getName(),
					'_client' => [
						'id' => 'kw',
					],
					'data' => [],
				];
				foreach (\Budget\Entity\Income::getTypes() as $type) {
					$data['financers'][$keyword->getId()]['data'][$type] = 0;
				}
			}
		}

		foreach ($clients as $client) {
			if ($client->isMaster()) {
				continue;
			}

			$bypass = false;

			$res = [
				'projects' => [],
			];

			$posteIncomesFilters = [];

			if (isset($filters['project'])) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'code', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['project'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$e->setParam('master', $isMaster);
				$controllerProject->setEvent($e);

				$controllerProject->setClient($client);
				$projects = $controllerProject->dispatch($request)->getVariable('rows');

				if (sizeOf($projects) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['project'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($projects as $project) {
					$posteIncomesFilters['project']['val'][] = $project['id'];
					$project['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
					$project['data'] = [];
					$res['projects'][$project['id']] = $project;
				}
			}

			if (isset($filters['structure'])) {
				$filters['structure']['financer'] = [
					'op' => 'eq',
					'val' => 1,
				];
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['structure'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerStructure->setEvent($e);

				$controllerStructure->setClient($client);
				$financers = $controllerStructure->dispatch($request)->getVariable('rows');

				if (sizeOf($financers) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['financer.id'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($financers as $financer) {
					$posteIncomesFilters['financer.id']['val'][] = $financer['id'];
					$financer['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
					$res['financers'][$financer['id']] = $financer;
				}
			}

			if (!$bypass) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => [
							'id',
							'name',
							'project.id',
							'project.name',
							'envelope.financer.id',
							'envelope.financer.name',
							'envelope.financer.keywords',
							'incomesArbo.amount',
							'incomesArbo.type',
						],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $posteIncomesFilters,
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerPosteIncome->setEvent($e);

				$controllerPosteIncome->setClient($client);

				$clientEM = $controllerPosteIncome->getEntityManager();

				$posteIncomes = $controllerPosteIncome->dispatch($request)->getVariable('rows');
				foreach ($posteIncomes as $posteIncome) {
					$projectId = $posteIncome['project']['id'];

					if (!array_key_exists($projectId, $res['projects'])) {
						$posteIncome['project']['_client'] = [
							'id' => $client->getId(),
							'name' => $client->getName(),
						];
						$res['projects'][$projectId] = $posteIncome['project'];
					}

					if (!array_key_exists('data', $res['projects'][$projectId])) {
						$res['projects'][$projectId]['data'] = [];
					}

					if ($posteIncome['incomesArbo']) {
						$financer = $posteIncome['envelope']['financer'];

						if (!$financer) {
							continue;
						}
						$keywordID = 0;
						foreach ($data['financers'] as $keyword) {
							foreach ($financer['keywords'] as $group) {
								foreach ($group as $association) {
									if (!$isMaster) {
										if ($association['id'] == $keyword['id']) {
											$keywordID = $keyword['id'];
										}
									} else {
										$keywordObject = $clientEM
											->getRepository('Keyword\Entity\Keyword')
											->findOneByMaster($keyword['id']);
										if ($keywordObject) {
											if ($association['id'] == $keywordObject->getId()) {
												$keywordID = $keyword['id'];
											}
										}
									}
								}
							}
						}

						$identifier = 'kw_' . $keywordID;
						if (!array_key_exists($identifier, $res['projects'][$projectId]['data'])) {
							$res['projects'][$projectId]['data'][$identifier] = [];
							foreach (\Budget\Entity\Income::getTypes() as $type) {
								$res['projects'][$projectId]['data'][$identifier][$type] = 0;
							}
						}

						foreach ($posteIncome['incomesArbo'] as $income) {
							if (
								isset($data['financers'][$keywordID]) &&
								isset($data['financers'][$keywordID]['data'][$income['type']])
							) {
								$res['projects'][$projectId]['data'][$identifier][$income['type']] +=
									$income['amount'];
								$data['financers'][$keywordID]['data'][$income['type']] += $income['amount'];
							}
						}
					}
				}
				$data['projects'] = array_merge($data['projects'], $res['projects']);
			}
		}

		$data['financers'] = array_values($data['financers']);

		return $export ? $this->financerExport($data, ['first_header_key' => 'project_entity', 'data_key' => 'projects']) : new JsonModel($data);
	}

	public function financerExport($data, $exportConfiguration)
	{
		$isMaster = $this->environment()
			->getClient()
			->isMaster();

		$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
		$translator = $helperPluginManager->get('translate');

		$xlsLines = [];

		$header = [];
		if ($isMaster) {
			$header[] = ucfirst($translator->__invoke('client'));
		}
		$header[] = ucfirst($translator->__invoke($exportConfiguration['first_header_key']));
		foreach (Income::getTypes() as $type) {
			$header[] = ucfirst($translator->__invoke('income_type_' . $type));
			for ($i = 0; $i < sizeOf($data['financers']) - 1; $i++) {
				$header[] = '';
			}
		}
		$xlsLines[] = $header;

		$subHeader = [''];
		if ($isMaster) {
			$subHeader[] = '';
		}

		foreach (Income::getTypes() as $type) {
			foreach ($data['financers'] as $financer) {
				$subHeader[] = $financer['name'];
			}
		}
		$xlsLines[] = $subHeader;

		$clientsLines = [];
		foreach ($data[$exportConfiguration['data_key']] as $project) {
			if ($isMaster && !array_key_exists($project['_client']['name'], $clientsLines)) {
				$clientsLines[$project['_client']['name']] = [];
			}

			$projectLine = [];
			if ($isMaster) {
				// colonne client
				$projectLine[] = '';
			}
			$projectLine[] = $project['name'];

			foreach (Income::getTypes() as $type) {
				foreach ($data['financers'] as $financer) {
					$identifier = $financer['_client']['id'] . '_' . $financer['id'];
					if (isset($project['data'][$identifier])) {
						$amount = $project['data'][$identifier][$type];
						$projectLine[] = $amount > 0 ? $amount : '0';
					} else {
						$projectLine[] = '0';
					}
				}
			}

			if ($isMaster) {
				$clientsLines[$project['_client']['name']][] = $projectLine;
			} else {
				$clientsLines[] = $projectLine;
			}
		}

		if ($isMaster) {
			foreach ($clientsLines as $client => $lines) {
				$xlsLines[] = [$client];
				$xlsLines = array_merge($xlsLines, $lines);
			}
		} else {
			$xlsLines = array_merge($xlsLines, $clientsLines);
		}

		ExcelExporter::download($xlsLines);

		return;
	}

	public function keywordsAction()
	{
		$em = $this->entityManager();
		$res = [
			'keywords' => [],
		];

		$filters = $this->params()->fromQuery('filters', []);
		$export = $this->params()->fromQuery('export', false);

		$controllerManager = $this->serviceLocator->get('ControllerManager');
		$controllerProject = $controllerManager->get('Project\Controller\API\Project');
		$request = new Request();
		$request->setMethod(Request::METHOD_GET);

		$group = null;
		if ($filters['group']) {
			$group = $em->getRepository('Keyword\Entity\Group')->find($filters['group']);
		}

		$data = $filters['data'];
		if (preg_match('/subv_/', $data)) {
			$data = str_replace('subv_', '', $data);
			if (!isset($filters['project'])) {
				$filters['project'] = [];
			}
			$filters['project']['external'] = [
				'op' => 'eq',
				'val' => 1,
			];
		} elseif (preg_match('/exp_/', $data)) {
			$data = str_replace('exp_', '', $data);
			if (!isset($filters['project'])) {
				$filters['project'] = [];
			}
			$filters['project']['external'] = [
				'op' => 'eq',
				'val' => 0,
			];
		}

		if ($group && $data) {
			foreach ($group->getKeywords() as $keyword) {
				$res['keywords'][$keyword->getId()] = [
					'id' => $keyword->getId(),
					'name' => $keyword->getName(),
					'parents' => [],
					'data' => 0,
				];

				foreach ($keyword->getParents() as $parent) {
					$res['keywords'][$keyword->getId()]['parents'][] = [
						'id' => $parent->getId(),
					];
				}
			}

			$routeMatch = new RouteMatch([]);
			$e = new MvcEvent();
			$e->setRouteMatch($routeMatch);
			$controllerProject->setEvent($e);

			$request->setQuery(
				new Parameters([
					'col' => ['id', 'keywords', $data],
					'search' => [
						'type' => 'list',
						'data' => [
							'sort' => 'id',
							'order' => 'asc',
							'filters' => isset($filters['project']) ? $filters['project'] : [],
						],
					],
				])
			);

			$projects = $controllerProject->dispatch($request)->getVariable('rows');
			foreach ($projects as $project) {
				if (isset($project['keywords'][$group->getId()])) {
					foreach ($project['keywords'][$group->getId()] as $_keyword) {
						$res['keywords'][$_keyword['id']]['data'] += $project[$data];
					}
				}
			}

			foreach ($res['keywords'] as $keyword) {
				$this->recursiveAggregateMulti($res['keywords'], $keyword, $keyword['parents']);
			}
		}

		if ($export != false) {
			$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
			$translator = $helperPluginManager->get('translate');

			$array = [
				[
					ucfirst($translator->__invoke('keyword_entity')),
					ucfirst($translator->__invoke('analysis_budget_keywords_data')),
				],
			];
			$parent = $this->params()->fromQuery('parent', null);
			$objects = [];
			foreach ($res['keywords'] as $keyword) {
				if (($keyword['parents'] == null || sizeOf($keyword['parents']) == 0) && !$parent) {
					$objects[] = $keyword;
				} else {
					foreach ($keyword['parents'] as $_parent) {
						if ($_parent['id'] == $parent) {
							$objects[] = $keyword;
						}
					}
				}
			}

			foreach ($objects as $object) {
				$objectArray = [];
				$objectArray[] = $object['name'];
				$objectArray[] = floatval($object['data']);

				$array[] = $objectArray;
			}

			ExcelExporter::download($array);
			die();
		}

		return new JsonModel([
			'res' => $res,
		]);
	}

	public function yearAction()
	{
		$filters = $this->params()->fromQuery('filters', []);
		$export = $this->params()->fromQuery('export', false);
		$groupBy = $filters['group'];

		if ($groupBy == 'keyword') {
			return $this->yearGroupedByKeywords($filters, $export);
		}

		$isMaster = $this->environment()
			->getClient()
			->isMaster();
		if ($isMaster) {
			$clients = $this->environment()
				->getClient()
				->getNetwork()
				->getClients();
			if (!isset($filters['project'])) {
				$filters['project'] = [];
			}

			$filters['project']['networkAccessible'] = [
				'op' => 'eq',
				'val' => 1,
			];
		} else {
			$clients = [$this->environment()->getClient()];
		}

		$controllerManager = $this->serviceLocator->get('ControllerManager');
		$controllerProject = $controllerManager->get('Project\Controller\API\Project');
		$controllerStructure = $controllerManager->get('Directory\Controller\API\Structure');
		$controllerPosteIncome = $controllerManager->get('Budget\Controller\API\PosteIncome');

		$data = [
			'years' => [],
			'financers' => []
		];

		foreach ($clients as $client) {
			if ($client->isMaster()) {
				continue;
			}

			$bypass = false;

			$res = [
				'years' => [],
				'financers' => [],
			];

			$posteIncomesFilters = [];

			if (isset($filters['project'])) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'code', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['project'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$e->setParam('master', $isMaster);
				$controllerProject->setEvent($e);

				$controllerProject->setClient($client);
				$projects = $controllerProject->dispatch($request)->getVariable('rows');

				if (sizeOf($projects) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['project'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($projects as $project) {
					$posteIncomesFilters['project']['val'][] = $project['id'];
					$project['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
				}
			}

			if (isset($filters['structure'])) {
				$filters['structure']['financer'] = [
					'op' => 'eq',
					'val' => 1,
				];
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['structure'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerStructure->setEvent($e);

				$controllerStructure->setClient($client);
				$financers = $controllerStructure->dispatch($request)->getVariable('rows');

				if (sizeOf($financers) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['financer.id'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($financers as $financer) {
					$posteIncomesFilters['financer.id']['val'][] = $financer['id'];
					$financer['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
					$res['financers'][$financer['id']] = $financer;
				}
			}

			if (!$bypass) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => [
							'id',
							'name',
							'project.id',
							'project.name',
							'envelope.financer.id',
							'envelope.financer.name',
							'incomesArbo.amount',
							'incomesArbo.type',
							'incomesArbo.date',
						],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $posteIncomesFilters,
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerPosteIncome->setEvent($e);

				$controllerPosteIncome->setClient($client);
				$posteIncomes = $controllerPosteIncome->dispatch($request)->getVariable('rows');
				foreach ($posteIncomes as $posteIncome) {
					$financerId = $posteIncome['envelope']['financer']['id'];

					if (!$financerId) {
						$financerId = 'na';
						$posteIncome['envelope']['financer'] = [
							'id' => 'na',
							'name' => 'N/A',
						];
					}

					$identifier = $client->getId() . '_' . $financerId;

					if (!array_key_exists($financerId, $res['financers'])) {
						$posteIncome['envelope']['financer']['_client'] = [
							'id' => $client->getId(),
							'name' => $client->getName(),
						];
						$res['financers'][$financerId] = $posteIncome['envelope']['financer'];
					}

					if (!array_key_exists('data', $res['financers'][$financerId])) {
						$res['financers'][$financerId]['data'] = [];
						foreach (\Budget\Entity\Income::getTypes() as $type) {
							$res['financers'][$financerId]['data'][$type] = 0;
						}
						$res['financers'][$financerId]['data']['total'] = 0;
					}

					if ($posteIncome['incomesArbo']) {
						foreach ($posteIncome['incomesArbo'] as $income) {
							$year = DateTime::createFromFormat('d/m/Y H:i', $income['date'])->format('Y');

							if (!array_key_exists($year, $res['years'])) {
								$res['years'][$year] = [
									'name' => $year,
									'data' => [],
									'total' => [],
								];

								foreach (\Budget\Entity\Income::getTypes() as $type) {
									$res['years'][$year]['total'][$type] = 0;
								}
							}

							if (!array_key_exists($identifier, $res['years'][$year]['data'])) {
								$res['years'][$year]['data'][$identifier] = [];
								foreach (\Budget\Entity\Income::getTypes() as $type) {
									$res['years'][$year]['data'][$identifier][$type] = 0;
								}
							}

							$res['years'][$year]['total'][$income['type']] += $income['amount'];
							$res['years'][$year]['data'][$identifier][$income['type']] += $income['amount'];
							$res['financers'][$financerId]['data'][$income['type']] += $income['amount'];
							$res['financers'][$financerId]['data']['total'] += $income['amount'];
						}
					}
				}

				$data['financers'] = array_merge($data['financers'], $res['financers']);
				$data['years'] = array_merge($data['years'], $res['years']);
			}
		}

		$data['financers'] = array_values($data['financers']);

		// Supprime les financeurs qui ont un total de 0€
		foreach ($data['financers'] as $key => $financer) {
			if ($financer['data']['total'] == 0) {
				unset($data['financers'][$key]);
			}
		}

		// Ordonne les années par ordre croissant
		usort($data['years'], function ($a, $b) {
			if ($a['name'] == $b['name']) {
				return 0;
			}

			return $a['name'] < $b['name'] ? -1 : 1;
		});

		return $export ? $this->financerExport($data, ['first_header_key' => 'year', 'data_key' => 'years']) : new JsonModel($data);
	}

	private function yearGroupedByKeywords($filters, $export)
	{
		$isMaster = $this->environment()
			->getClient()
			->isMaster();
		if ($isMaster) {
			$clients = $this->environment()
				->getClient()
				->getNetwork()
				->getClients();
			if (!isset($filters['project'])) {
				$filters['project'] = [];
			}

			$filters['project']['networkAccessible'] = [
				'op' => 'eq',
				'val' => 1,
			];
		} else {
			$clients = [$this->environment()->getClient()];
		}

		$data = [
			'years' => [],
			'financers' => [],
		];

		$controllerManager = $this->serviceLocator->get('ControllerManager');
		$controllerProject = $controllerManager->get('Project\Controller\API\Project');
		$controllerStructure = $controllerManager->get('Directory\Controller\API\Structure');
		$controllerPosteIncome = $controllerManager->get('Budget\Controller\API\PosteIncome');


		$groups = $this->allowedKeywordGroups('structure');
		$data['financers'][0] = [
			'id' => 0,
			'name' => 'N/A',
			'_client' => [
				'id' => 'kw',
			],
			'data' => [],
		];
		foreach (\Budget\Entity\Income::getTypes() as $type) {
			$data['financers'][0]['data'][$type] = 0;
		}
		$data['financers'][0]['data']['total'] = 0;

		foreach ($groups as $group) {
			foreach ($group->getKeywords() as $keyword) {
				$data['financers'][$keyword->getId()] = [
					'id' => $keyword->getId(),
					'name' => $keyword->getName(),
					'_client' => [
						'id' => 'kw',
					],
					'data' => [],
				];
				foreach (\Budget\Entity\Income::getTypes() as $type) {
					$data['financers'][$keyword->getId()]['data'][$type] = 0;
				}
				$data['financers'][$keyword->getId()]['data']['total'] = 0;
			}
		}

		foreach ($clients as $client) {
			if ($client->isMaster()) {
				continue;
			}

			$bypass = false;

			$res = [
				'years' => [],
				'financers' => [],
			];

			$posteIncomesFilters = [];

			if (isset($filters['project'])) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'code', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['project'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$e->setParam('master', $isMaster);
				$controllerProject->setEvent($e);

				$controllerProject->setClient($client);
				$projects = $controllerProject->dispatch($request)->getVariable('rows');

				if (sizeOf($projects) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['project'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($projects as $project) {
					$posteIncomesFilters['project']['val'][] = $project['id'];
					$project['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
					$project['data'] = [];
				}
			}

			if (isset($filters['structure'])) {
				$filters['structure']['financer'] = [
					'op' => 'eq',
					'val' => 1,
				];
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => ['id', 'name'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $filters['structure'],
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerStructure->setEvent($e);

				$controllerStructure->setClient($client);
				$financers = $controllerStructure->dispatch($request)->getVariable('rows');

				if (sizeOf($financers) == 0) {
					$bypass = true;
				}

				$posteIncomesFilters['financer.id'] = [
					'op' => 'eq',
					'val' => [],
				];

				foreach ($financers as $financer) {
					$posteIncomesFilters['financer.id']['val'][] = $financer['id'];
					$financer['_client'] = [
						'id' => $client->getId(),
						'name' => $client->getName(),
					];
					$res['financers'][$financer['id']] = $financer;
				}
			}

			if (!$bypass) {
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setQuery(
					new Parameters([
						'col' => [
							'id',
							'name',
							'project.id',
							'project.name',
							'envelope.financer.id',
							'envelope.financer.name',
							'envelope.financer.keywords',
							'incomesArbo.amount',
							'incomesArbo.type',
							'incomesArbo.date',
						],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $posteIncomesFilters,
							],
						],
					])
				);

				$routeMatch = new RouteMatch([]);
				$e = new MvcEvent();
				$e->setRouteMatch($routeMatch);
				$controllerPosteIncome->setEvent($e);

				$controllerPosteIncome->setClient($client);

				$clientEM = $controllerPosteIncome->getEntityManager();

				$posteIncomes = $controllerPosteIncome->dispatch($request)->getVariable('rows');

				foreach ($posteIncomes as $posteIncome) {
					if ($posteIncome['incomesArbo']) {
						$financer = $posteIncome['envelope']['financer'];

						if (!$financer) {
							continue;
						}
						$keywordID = 0;

						foreach ($data['financers'] as $keyword) {
							foreach ($financer['keywords'] as $group) {
								foreach ($group as $association) {
									if (!$isMaster) {
										if ($association['id'] == $keyword['id']) {
											$keywordID = $keyword['id'];
										}
									} else {
										$keywordObject = $clientEM
											->getRepository('Keyword\Entity\Keyword')
											->findOneByMaster($keyword['id']);
										if ($keywordObject) {
											if ($association['id'] == $keywordObject->getId()) {
												$keywordID = $keyword['id'];
											}
										}
									}
								}
							}
						}

						$identifier = 'kw_' . $keywordID;

						foreach ($posteIncome['incomesArbo'] as $income) {
							$year = DateTime::createFromFormat('d/m/Y H:i', $income['date'])->format('Y');

							if (!array_key_exists($year, $res['years'])) {
								$res['years'][$year] = [
									'name' => $year,
									'data' => [],
									'total' => [],
								];

								foreach (\Budget\Entity\Income::getTypes() as $type) {
									$res['years'][$year]['total'][$type] = 0;
								}
							}

							if (!array_key_exists($identifier, $res['years'][$year]['data'])) {
								$res['years'][$year]['data'][$identifier] = [];
								foreach (\Budget\Entity\Income::getTypes() as $type) {
									$res['years'][$year]['data'][$identifier][$type] = 0;
								}
							}

							$res['years'][$year]['total'][$income['type']] += $income['amount'];
							$res['years'][$year]['data'][$identifier][$income['type']] += $income['amount'];
							$data['financers'][$keywordID]['data'][$income['type']] += $income['amount'];
							$data['financers'][$keywordID]['data']['total'] += $income['amount'];
						}
					}
				}
				$data['years'] = array_merge($data['years'], $res['years']);
			}
		}
		$data['financers'] = array_values($data['financers']);

		// Supprime les keywords qui ont un total de 0€
		foreach ($data['financers'] as $key => $keyword) {
			if ($keyword['data']['total'] == 0) {
				unset($data['financers'][$key]);
			}
		}

		// Ordonne les années par ordre croissant
		usort($data['years'], function ($a, $b) {
			if ($a['year'] == $b['year']) {
				return 0;
			}

			return $a['year'] < $b['year'] ? -1 : 1;
		});

		return $export ? $this->financerExport($data, ['first_header_key' => 'project_entity', 'data_key' => 'years']) : new JsonModel($data);
	}

	protected function recursiveAggregateMulti(&$objects, $object, $parents)
	{
		foreach ($parents as $i => $parent) {
			$objects[$parent['id']]['data'] += $object['data'];
			$this->recursiveAggregateMulti($objects, $object, $objects[$parent['id']]['parents']);
		}
	}

	/**
	 * @param array  $projects
	 * @param array  $columns
	 * @param string $export
	 * @param bool   $isSubvention
	 *
	 * @return void
	 */
	protected function expenseExport($projects, $columns, $export, $isSubvention)
	{
		$translator = $this->helper->get('translate');
		$isCustomExport = $export !== 'default';

		if ($isCustomExport) {
			Custom::doExport(
				$this->environment()->getClient(),
				$export,
				[],
				$projects,
				$this->entityManager(),
				$translator,
				$this->serviceLocator->get('ControllerPluginManager')->get('apiRequest')
			);

			exit();
		}

		/** @var Group[] $allProjectGroups */
		$allProjectGroups = $this->allowedKeywordGroups('project', false);
        $allExpenseGroups = $this->allowedKeywordGroups('expense', false);
	    $allPosteExpenseGroups = $this->allowedKeywordGroups('posteExpense', false);

		$posteExpenseGroups = [];
		$expenseGroups = [];

		$projectGroups = [];
		foreach ($columns as $column) {
			if (preg_match('/^project.keyword./', $column)) {
				$id = str_replace('project.keyword.', '', $column);

				foreach ($allProjectGroups as $group) {
					if ($group->getId() == $id) {
						$projectGroups[] = $group;
						break;
					}
				}
			}

	        if (preg_match('/^posteExpenses.keyword/', $column)) {
				$id = str_replace('posteExpenses.keyword', '', $column);

				foreach ($allPosteExpenseGroups as $group) {
					if ($group->getId() == $id) {
						$posteExpenseGroups[] = $group;
						break;
					}
				}
			}
            if (preg_match('/^posteExpenses.expenses.keyword/', $column)) {
				$id = str_replace('posteExpenses.expenses.keyword', '', $column);

				foreach ($allExpenseGroups as $group) {
					if ($group->getId() == $id) {
						$expenseGroups[] = $group;
						break;
					}
				}
			}
		}


		$total = $this->getTotalArray();

		$xlsLines = [];

		$clientLines = [];
		$clientLinesBolded = [];
		$clientTotal = [];

		$linesBolded = [];

		$header = [];

		if ($this->isMaster) {
			$header[] = ucfirst($translator->__invoke('client'));
		}

		if(null !== $projectGroups) {
			foreach ($projectGroups as $group) {
				$header[] = $group->getName();
			}
		}

		if (in_array('project.code', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_code'));
		}
		if (in_array('project.publicationStatus', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_publicationStatus'));
		}

		if (in_array('project.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_name'));
		}

		if (in_array('posteExpenses.account.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('account_entity'));
		}

		if (in_array('posteExpenses.name', $columns)) {
			$header[] = $isSubvention
				? ucfirst($translator->__invoke('poste_subvention_entity'))
				: ucfirst($translator->__invoke('poste_expense_entity'));
		}

		if(null !== $posteExpenseGroups) {
			foreach ($posteExpenseGroups as $group) {
				$header[] = $group->getName();
			}
		}

		if (in_array('posteExpenses.nature.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_expense_field_nature'));
		}

		if (in_array('posteExpenses.expenses.name', $columns)) {
			$header[] = $isSubvention
				? ucfirst($translator->__invoke('subvention_entity'))
				: ucfirst($translator->__invoke('expense_entity'));
		}

		if(null !== $expenseGroups) {
			foreach ($expenseGroups as $group) {
				$header[] = $group->getName();
			}
		}

		if (in_array('posteExpenses.amount', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_expense_field_amount'));
		}

		if (in_array('posteExpenses.amountHT', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_expense_field_amountHT'));
		}


		if (in_array('posteExpenses.expenses.anneeExercice', $columns)) {
			$header[] = $translator->__invoke("expense_field_anneeExercice");
		}

		if (in_array('posteExpenses.expenses.mandat', $columns)) {
			$header[] = $translator->__invoke("expense_field_mandat");
		}

		if (in_array('posteExpenses.expenses.bordereau', $columns)) {
			$header[] = $translator->__invoke("expense_field_bordereau");
		}

		if (in_array('posteExpenses.expenses.tiers', $columns)) {
			$header[] = $translator->__invoke("expense_field_tiers");
		}

		if (in_array('posteExpenses.expenses.complement', $columns)) {
			$header[] = $translator->__invoke("expense_field_complement");
		}

		if (in_array('posteExpenses.expenses.engagement', $columns)) {
			$header[] = $translator->__invoke("expense_field_engagement");
		}

		if (in_array('posteExpenses.expenses.dateFacture', $columns)) {
			$header[] = $translator->__invoke("expense_field_dateFacture");
		}

		if (in_array('posteExpenses.expenses.createdAt', $columns)) {
			$header[] = $translator->__invoke("expense_field_createdAt");
		}

		if (in_array('posteExpenses.expenses.updatedAt', $columns)) {
			$header[] = $translator->__invoke("expense_field_updatedAt");
		}

		if(in_array('posteExpenses.expenses.managementUnit.name', $columns)) {
			$header[] = $translator->__invoke("expense_field_managementUnit");
		}

		foreach ($this->expenseTypes as $type) {
			if (preg_match('/^ae/', $type)) {
				if (in_array("posteExpenses.expenses.type.$type", $columns)) {
					$header[] = ucfirst($translator->__invoke("expense_type_$type"));
				}

				if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
					$header[] = ucfirst($translator->__invoke("expense_type_{$type}_ht"));
				}
			}
		}

		foreach ($this->posteExpenseBalances as $balance) {
			if (preg_match('/^ae/', $balance) && $balance !== 'aeCommittedCpCommittedBalance') {
				if (in_array("posteExpenses.balance.$balance", $columns)) {
					$header[] = ucfirst($translator->__invoke("poste_expense_balance_$balance"));
				}

				if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
					$header[] = ucfirst($translator->__invoke("poste_expense_balance_{$balance}_ht"));
				}
			}
		}

		foreach ($this->expenseTypes as $type) {
			if (!preg_match('/^ae/', $type) && !preg_match('/^cp/', $type)) {
				if (in_array("posteExpenses.expenses.type.$type", $columns)) {
					$header[] = ucfirst($translator->__invoke("expense_type_$type"));
				}

				if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
					$header[] = ucfirst($translator->__invoke("expense_type_{$type}_ht"));
				}
			}
		}

		foreach ($this->posteExpenseBalances as $balance) {
			if (!preg_match('/^ae/', $balance) && !preg_match('/^cp/', $balance)) {
				if (in_array("posteExpenses.balance.$balance", $columns)) {
					$header[] = ucfirst($translator->__invoke("poste_expense_balance_$balance"));
				}

				if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
					$header[] = ucfirst($translator->__invoke("poste_expense_balance_{$balance}_ht"));
				}
			}
		}

		foreach ($this->expenseTypes as $type) {
			if (preg_match('/^cp/', $type)) {
				if (in_array("posteExpenses.expenses.type.$type", $columns)) {
					$header[] = ucfirst($translator->__invoke("expense_type_$type"));
				}

				if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
					$header[] = ucfirst($translator->__invoke("expense_type_{$type}_ht"));
				}
			}
		}

		foreach ($this->posteExpenseBalances as $balance) {
			if (preg_match('/^cp/', $balance) || $balance === 'aeCommittedCpCommittedBalance') {
				if (in_array("posteExpenses.balance.$balance", $columns)) {
					$header[] = ucfirst($translator->__invoke("poste_expense_balance_$balance"));
				}

				if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
					$header[] = ucfirst($translator->__invoke("poste_expense_balance_{$balance}_ht"));
				}
			}
		}

		if (in_array('posteExpenses.expenses.date', $columns)) {
			$header[] = ucfirst($translator->__invoke("expense_field_date"));
		}

		$xlsLines[] = $header;

		foreach ($projects as $i => $project) {
			$client = $project['_client']['name'] ?? '';

			if ($this->isMaster && !isset($clientLines[$client])) {
				$clientLines[$client] = [];
				$clientTotal[$client] = $this->getTotalArray();
				$clientLinesBolded[$client] = [];
			}

			$projectLines = [];
			$projectLine = [];
			$projectTotal = [];

			$showPosteLines =
				in_array('posteExpenses.name', $columns) ||
				in_array('posteExpenses.account.name', $columns);

			$showExpenseLines = in_array('posteExpenses.expenses.name', $columns) 
			|| in_array('posteExpenses.expenses.tiers', $columns)
			|| in_array('posteExpenses.expenses.anneeExercice', $columns)
			|| in_array('posteExpenses.expenses.complement', $columns);

			if ($this->isMaster) {
				$projectLine[] = '';
			}

			if(null !== $projectGroups) {
				foreach ($projectGroups as $group) {
					$value = '';

					if ($projectGroup = $project['keywords'][$group->getId()]) {
						foreach ($projectGroup as $index => $keyword) {
							if ($index !== 0) {
								$value .= '; ';
							}

							$value .= $keyword['name'];
						}
					}

					$projectLine[] = $value;
				}
			}

			if (in_array('project.code', $columns)) {
				$projectLine[] = $project['code'];
			}

			if (in_array('project.publicationStatus', $columns)) {
				$projectLine[] = ucfirst($translator->__invoke('project_publicationStatus_' . $project['publicationStatus']));
			}

			if (in_array('project.name', $columns)) {
				$projectLine[] = str_repeat('    ', $project['_level']) . $project['name'];
			}

			if (in_array('posteExpenses.account.name', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteExpenses.name', $columns)) {
				$projectLine[] = '';
			}

			if(null !== $posteExpenseGroups) {
				foreach ($posteExpenseGroups as $group) {
					$projectLine[] = '';
				}
			}

			if (in_array('posteExpenses.nature.name', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteExpenses.expenses.name', $columns)) {
				$projectLine[] = '';
			}

			if(null !== $expenseGroups) {
				foreach ($expenseGroups as $group) {
					$projectLine[] = '';
				}
			}

			if (in_array('posteExpenses.amount', $columns)) {
				$projectLine[] = $project['amountPosteExpenseArbo'];

				if ($project['_level'] === 0) {
					if (!isset($projectTotal['poste'])) {
						$projectTotal['poste'] = 0;
					}
					if (!isset($total['poste'])) {
						$total['poste'] = 0;
					}
					$projectTotal['poste'] += $project['amountPosteExpenseArbo'];
					$total['poste'] += $project['amountPosteExpenseArbo'];
				}
			}

			if (in_array('posteExpenses.amountHT', $columns)) {
				$projectLine[] = $project['amountHTPosteExpenseArbo'];

				if ($project['_level'] === 0) {
					$projectTotal['poste_ht'] += $project['amountHTPosteExpenseArbo'];
					$total['poste_ht'] += $project['amountHTPosteExpenseArbo'];
				}
			}

			if (in_array('posteExpenses.expenses.anneeExercice', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteExpenses.expenses.mandat', $columns)) {
				$projectLine[] = '';
			}
	
			if (in_array('posteExpenses.expenses.bordereau', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteExpenses.expenses.tiers', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteExpenses.expenses.complement', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteExpenses.expenses.engagement', $columns)) {
				$projectLine[] = '';
			}
	
			if (in_array('posteExpenses.expenses.dateFacture', $columns)) {
				$projectLine[] = '';
			}
	
			if (in_array('posteExpenses.expenses.createdAt', $columns)) {
				$projectLine[] = '';
			}
	
			if (in_array('posteExpenses.expenses.updatedAt', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteExpenses.expenses.managementUnit.name', $columns)) {
				$projectLine[] = '';
			}

			foreach ($this->expenseTypes as $type) {
				if (preg_match('/^ae/', $type)) {
					if (in_array("posteExpenses.expenses.type.$type", $columns)) {
						$projectLine[] = $project['amountExpenseArbo' . ucfirst($type)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal[$type])) {
								$projectTotal[$type] = 0;
							}
							if (!isset($total[$type])) {
								$total[$type] = 0;
							}
							$projectTotal[$type] += $project['amountExpenseArbo' . ucfirst($type)];
							$total[$type] += $project['amountExpenseArbo' . ucfirst($type)];
						}
					}

					if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
						$projectLine[] = $project['amountHTExpenseArbo' . ucfirst($type)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal["{$type}_ht"])) {
								$projectTotal["{$type}_ht"] = 0;
							}
							if (!isset($total["{$type}_ht"])) {
								$total["{$type}_ht"] = 0;
							}
							$projectTotal["{$type}_ht"] += $project['amountHTExpenseArbo' . ucfirst($type)];
							$total["{$type}_ht"] += $project['amountHTExpenseArbo' . ucfirst($type)];
						}
					}
				}
			}

			foreach ($this->posteExpenseBalances as $balance) {
				if (preg_match('/^ae/', $balance) && $balance !== 'aeCommittedCpCommittedBalance') {
					if (in_array("posteExpenses.balance.$balance", $columns)) {
						$projectLine[] = $project['amountPosteExpenseArbo' . ucfirst($balance)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal[$balance])) {
								$projectTotal[$balance] = 0;
							}
							if (!isset($total[$balance])) {
								$total[$balance] = 0;
							}
							$projectTotal[$balance] += $project['amountPosteExpenseArbo' . ucfirst($balance)];
							$total[$balance] += $project['amountPosteExpenseArbo' . ucfirst($balance)];
						}
					}

					if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
						$projectLine[] = $project['amountHTPosteExpenseArbo' . ucfirst($balance)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal["{$balance}_ht"])) {
								$projectTotal["{$balance}_ht"] = 0;
							}
							if (!isset($total["{$balance}_ht"])) {
								$total["{$balance}_ht"] = 0;
							}
							$projectTotal["{$balance}_ht"] +=
								$project['amountHTPosteExpenseArbo' . ucfirst($balance)];
							$total["{$balance}_ht"] += $project['amountHTPosteExpenseArbo' . ucfirst($balance)];
						}
					}
				}
			}

			foreach ($this->expenseTypes as $type) {
				if (!preg_match('/^ae/', $type) && !preg_match('/^cp/', $type)) {
					if (in_array("posteExpenses.expenses.type.$type", $columns)) {
						$projectLine[] = $project['amountExpenseArbo' . ucfirst($type)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal[$type])) {
								$projectTotal[$type] = 0;
							}
							if (!isset($total[$type])) {
								$total[$type] = 0;
							}
							$projectTotal[$type] += $project['amountExpenseArbo' . ucfirst($type)];
							$total[$type] += $project['amountExpenseArbo' . ucfirst($type)];
						}
					}

					if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
						$projectLine[] = $project['amountHTExpenseArbo' . ucfirst($type)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal["{$type}_ht"])) {
								$projectTotal["{$type}_ht"] = 0;
							}
							if (!isset($total["{$type}_ht"])) {
								$total["{$type}_ht"] = 0;
							}
							$projectTotal["{$type}_ht"] += $project['amountHTExpenseArbo' . ucfirst($type)];
							$total["{$type}_ht"] += $project['amountHTExpenseArbo' . ucfirst($type)];
						}
					}
				}
			}

			foreach ($this->posteExpenseBalances as $balance) {
				if (!preg_match('/^ae/', $balance) && !preg_match('/^cp/', $balance)) {
					if (in_array("posteExpenses.balance.$balance", $columns)) {
						$projectLine[] = $project['amountPosteExpenseArbo' . ucfirst($balance)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal[$balance])) {
								$projectTotal[$balance] = 0;
							}
							if (!isset($total[$balance])) {
								$total[$balance] = 0;
							}
							$projectTotal[$balance] += $project['amountPosteExpenseArbo' . ucfirst($balance)];
							$total[$balance] += $project['amountPosteExpenseArbo' . ucfirst($balance)];
						}
					}

					if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
						$projectLine[] = $project['amountHTPosteExpenseArbo' . ucfirst($balance)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal["{$balance}_ht"])) {
								$projectTotal["{$balance}_ht"] = 0;
							}
							if (!isset($total["{$balance}_ht"])) {
								$total["{$balance}_ht"] = 0;
							}

							$projectTotal["{$balance}_ht"] +=
								$project['amountHTPosteExpenseArbo' . ucfirst($balance)];
							$total["{$balance}_ht"] += $project['amountHTPosteExpenseArbo' . ucfirst($balance)];
						}
					}
				}
			}

			foreach ($this->expenseTypes as $type) {
				if (preg_match('/^cp/', $type)) {
					if (in_array("posteExpenses.expenses.type.$type", $columns)) {
						$projectLine[] = $project['amountExpenseArbo' . ucfirst($type)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal[$type])) {
								$projectTotal[$type] = 0;
							}
							if (!isset($total[$type])) {
								$total[$type] = 0;
							}
							$projectTotal[$type] += $project['amountExpenseArbo' . ucfirst($type)];
							$total[$type] += $project['amountExpenseArbo' . ucfirst($type)];
						}
					}

					if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
						$projectLine[] = $project['amountHTExpenseArbo' . ucfirst($type)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal["{$type}_ht"])) {
								$projectTotal["{$type}_ht"] = 0;
							}
							if (!isset($total["{$type}_ht"])) {
								$total["{$type}_ht"] = 0;
							}
							$projectTotal["{$type}_ht"] += $project['amountHTExpenseArbo' . ucfirst($type)];
							$total["{$type}_ht"] += $project['amountHTExpenseArbo' . ucfirst($type)];
						}
					}
				}
			}

			foreach ($this->posteExpenseBalances as $balance) {
				if (preg_match('/^cp/', $balance) || $balance === 'aeCommittedCpCommittedBalance') {
					if (in_array("posteExpenses.balance.$balance", $columns)) {
						$projectLine[] = $project['amountPosteExpenseArbo' . ucfirst($balance)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal[$balance])) {
								$projectTotal[$balance] = 0;
							}
							if (!isset($total[$balance])) {
								$total[$balance] = 0;
							}
							$projectTotal[$balance] += $project['amountPosteExpenseArbo' . ucfirst($balance)];
							$total[$balance] += $project['amountPosteExpenseArbo' . ucfirst($balance)];
						}
					}

					if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
						$projectLine[] = $project['amountHTPosteExpenseArbo' . ucfirst($balance)];

						if ($project['_level'] === 0) {
							if (!isset($projectTotal["{$balance}_ht"])) {
								$projectTotal["{$balance}_ht"] = 0;
							}
							if (!isset($total["{$balance}_ht"])) {
								$total["{$balance}_ht"] = 0;
							}
							$projectTotal["{$balance}_ht"] +=
								$project['amountHTPosteExpenseArbo' . ucfirst($balance)];
							$total["{$balance}_ht"] += $project['amountHTPosteExpenseArbo' . ucfirst($balance)];
						}
					}
				}
			}

			if (in_array('posteExpenses.expenses.date', $columns)) {
				$projectLine[] = '';
			}

			$projectLines[] = $projectLine;

			if ($showPosteLines) {
				if (!$this->isMaster) {
					$linesBolded[] = count($xlsLines) + 1;
				}

				foreach ($project['posteExpenses'] as $poste) {
					$posteLine = [];

					if ($this->isMaster) {
						$posteLine[] = '';
					}

					if(null !== $projectGroups) {
						foreach ($projectGroups as $group) {
							$posteLine[] = '';
						}
					}

					if (in_array('project.code', $columns)) {
						$posteLine[] = $project['code'];
					}

					if (in_array('project.publicationStatus', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('project.name', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('posteExpenses.account.name', $columns)) {
						$posteLine[] = $poste['account']
							? $poste['account']['code'] . ' - ' . $poste['account']['name']
							: '';
					}

					if (in_array('posteExpenses.name', $columns)) {
						$posteLine[] = $poste['name'];
					}

					if(null !== $posteExpenseGroups) {
						foreach ($posteExpenseGroups as $group) {
							$value = '';
							if ($group = $poste['keywords'][$group->getId()]) {
								foreach ($group as $index => $keyword) {
									if ($index !== 0) {
										$value .= '; ';
									}
									$value .= $keyword['name'];
								}
							}
							$posteLine[] = $value;
						}
					}

					if (in_array('posteExpenses.nature.name', $columns)) {
						$posteLine[] = $poste['nature']['name'];
					}

					if (in_array('posteExpenses.expenses.name', $columns)) {
						$posteLine[] = '';
					}

					if(null !== $expenseGroups) {
						foreach ($expenseGroups as $group) {
							$posteLine[] = '';
						}
					}

					if (in_array('posteExpenses.amount', $columns)) {
						$posteLine[] = $poste['amount'];
					}

					if (in_array('posteExpenses.amountHT', $columns)) {
						$posteLine[] = $poste['amountHT'];
					}

					if (in_array('posteExpenses.expenses.anneeExercice', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('posteExpenses.expenses.mandat', $columns)) {
						$posteLine[] = '';
					}
			
					if (in_array('posteExpenses.expenses.bordereau', $columns)) {
						$posteLine[] = '';
					}
		
					if (in_array('posteExpenses.expenses.tiers', $columns)) {
						$posteLine[] = '';
					}
		
					if (in_array('posteExpenses.expenses.complement', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('posteExpenses.expenses.engagement', $columns)) {
						$posteLine[] = '';
					}
			
					if (in_array('posteExpenses.expenses.dateFacture', $columns)) {
						$posteLine[] = '';
					}
			
					if (in_array('posteExpenses.expenses.createdAt', $columns)) {
						$posteLine[] = '';
					}
			
					if (in_array('posteExpenses.expenses.updatedAt', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('posteExpenses.expenses.managementUnit.name', $columns)) {
						$posteLine[] = '';
					}

					foreach ($this->expenseTypes as $type) {
						if (preg_match('/^ae/', $type)) {
							if (in_array("posteExpenses.expenses.type.$type", $columns)) {
								$posteLine[] = $poste['amount' . ucfirst($type)];
							}

							if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
								$posteLine[] = $poste['amountHT' . ucfirst($type)];
							}
						}
					}

					foreach ($this->posteExpenseBalances as $balance) {
						if (preg_match('/^ae/', $balance) && $balance !== 'aeCommittedCpCommittedBalance') {
							if (in_array("posteExpenses.balance.$balance", $columns)) {
								$posteLine[] = $poste['amount' . ucfirst($balance)];
							}

							if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
								$posteLine[] = $poste['amountHT' . ucfirst($balance)];
							}
						}
					}

					foreach ($this->expenseTypes as $type) {
						if (!preg_match('/^ae/', $type) && !preg_match('/^cp/', $type)) {
							if (in_array("posteExpenses.expenses.type.$type", $columns)) {
								$posteLine[] = $poste['amount' . ucfirst($type)];
							}

							if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
								$posteLine[] = $poste['amountHT' . ucfirst($type)];
							}
						}
					}

					foreach ($this->posteExpenseBalances as $balance) {
						if (!preg_match('/^ae/', $balance) && !preg_match('/^cp/', $balance)) {
							if (in_array("posteExpenses.balance.$balance", $columns)) {
								$posteLine[] = $poste['amount' . ucfirst($balance)];
							}

							if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
								$posteLine[] = $poste['amountHT' . ucfirst($balance)];
							}
						}
					}

					foreach ($this->expenseTypes as $type) {
						if (preg_match('/^cp/', $type)) {
							if (in_array("posteExpenses.expenses.type.$type", $columns)) {
								$posteLine[] = $poste['amount' . ucfirst($type)];
							}

							if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
								$posteLine[] = $poste['amountHT' . ucfirst($type)];
							}
						}
					}

					foreach ($this->posteExpenseBalances as $balance) {
						if (preg_match('/^cp/', $balance) || $balance === 'aeCommittedCpCommittedBalance') {
							if (in_array("posteExpenses.balance.$balance", $columns)) {
								$posteLine[] = $poste['amount' . ucfirst($balance)];
							}

							if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
								$posteLine[] = $poste['amountHT' . ucfirst($balance)];
							}
						}
					}

					if (in_array('posteExpenses.expenses.date', $columns)) {
						$posteLine[] = '';
					}
					$projectLines[] = $posteLine;
					
					if ($showExpenseLines) {
						foreach ($poste['expenses'] as $expense) {
							$expenseLine = [];
							if ($this->isMaster) {
								$expenseLine[] = '';
							}

							if(null !== $projectGroups) {
								foreach ($projectGroups as $group) {
									$expenseLine[] = '';
								}
							}

							if (in_array('project.code', $columns)) {
								$expenseLine[] = $project['code'];
							}

							if (in_array('project.publicationStatus', $columns)) {
								$expenseLine[] = '';
							}

							if (in_array('project.name', $columns)) {
								$expenseLine[] = '';
							}

							if (in_array('posteExpenses.account.name', $columns)) {
								$expenseLine[] = '';
							}

							if (in_array('posteExpenses.name', $columns)) {
								$expenseLine[] = '';
							}

							if(null !== $posteExpenseGroups) {
								foreach ($posteExpenseGroups as $group) {
									$expenseLine[] = '';
								}
							}

							if (in_array('posteExpenses.nature.name', $columns)) {
								$expenseLine[] = '';
							}

							if (in_array('posteExpenses.expenses.name', $columns)) {
								$expenseLine[] = $expense['name'];
							}

							if(null !== $expenseGroups) {
								foreach ($expenseGroups as $group) {
									$value = '';
									if ($group = $expense['keywords'][$group->getId()]) {
										foreach ($group as $index => $keyword) {
											if ($index !== 0) {
												$value .= '; ';
											}
											$value .= $keyword['name'];
										}
									}
									$expenseLine[] = $value;
								}
							}

							if (in_array('posteExpenses.amount', $columns)) {
								$expenseLine[] = '';
							}

							if (in_array('posteExpenses.amountHT', $columns)) {
								$expenseLine[] = '';
							}

							if (in_array('posteExpenses.expenses.anneeExercice', $columns)) {
								$expenseLine[] = $expense['anneeExercice'];
							}

							if (in_array('posteExpenses.expenses.mandat', $columns)) {
								$expenseLine[] = $expense['mandat'];
							}
					
							if (in_array('posteExpenses.expenses.bordereau', $columns)) {
								$expenseLine[] = $expense['bordereau'];
							}

							if (in_array('posteExpenses.expenses.tiers', $columns)) {
								$expenseLine[] = $expense['tiers'];
							}
				
							if (in_array('posteExpenses.expenses.complement', $columns)) {
								$expenseLine[] = $expense['complement'];
							}

							if (in_array('posteExpenses.expenses.engagement', $columns)) {
								$expenseLine[] = $expense['engagement'];
							}
					
							if (in_array('posteExpenses.expenses.dateFacture', $columns)) {
								$expenseLine[] = $expense['dateFacture'];
							}
					
							if (in_array('posteExpenses.expenses.createdAt', $columns)) {
								$expenseLine[] = $expense['createdAt'];
							}
					
							if (in_array('posteExpenses.expenses.updatedAt', $columns)) {
								$expenseLine[] = $expense['updatedAt'];
							}

							if (in_array('posteExpenses.expenses.managementUnit.name', $columns)) {
								$expenseLine[] = $expense['managementUnit']['name'];
							}

							foreach ($this->expenseTypes as $type) {
								if (preg_match('/^ae/', $type)) {
									if (in_array("posteExpenses.expenses.type.$type", $columns)) {
										$expenseLine[] = $expense['type'] === $type ? $expense['amount'] : '';
									}

									if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
										$expenseLine[] = $expense['type'] === $type ? $expense['amountHT'] : '';
									}
								}
							}

							foreach ($this->posteExpenseBalances as $balance) {
								if (preg_match('/^ae/', $balance) && $balance !== 'aeCommittedCpCommittedBalance') {
									if (in_array("posteExpenses.balance.$balance", $columns)) {
										$expenseLine[] = '';
									}

									if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
										$expenseLine[] = '';
									}
								}
							}

							foreach ($this->expenseTypes as $type) {
								if (!preg_match('/^ae/', $type) && !preg_match('/^cp/', $type)) {
									if (in_array("posteExpenses.expenses.type.$type", $columns)) {
										$expenseLine[] = $expense['type'] === $type ? $expense['amount'] : '';
									}

									if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
										$expenseLine[] = $expense['type'] === $type ? $expense['amountHT'] : '';
									}
								}
							}

							foreach ($this->posteExpenseBalances as $balance) {
								if (!preg_match('/^ae/', $balance) && !preg_match('/^cp/', $balance)) {
									if (in_array("posteExpenses.balance.$balance", $columns)) {
										$expenseLine[] = '';
									}

									if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
										$expenseLine[] = '';
									}
								}
							}

							foreach ($this->expenseTypes as $type) {
								if (preg_match('/^cp/', $type)) {
									if (in_array("posteExpenses.expenses.type.$type", $columns)) {
										$expenseLine[] = $expense['type'] === $type ? $expense['amount'] : '';
									}

									if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
										$expenseLine[] = $expense['type'] === $type ? $expense['amountHT'] : '';
									}
								}
							}

							foreach ($this->posteExpenseBalances as $balance) {
								if (preg_match('/^cp/', $balance) || $balance === 'aeCommittedCpCommittedBalance') {
									if (in_array("posteExpenses.balance.$balance", $columns)) {
										$expenseLine[] = '';
									}

									if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
										$expenseLine[] = '';
									}
								}
							}

							if (in_array('posteExpenses.expenses.date', $columns)) {
								$expenseLine[] = DateTime::createFromFormat('d/m/Y H:i', $expense['date'])->format('d/m/Y');
							}


							$projectLines[] = $expenseLine;
						}
					}
				}
			}

			if ($this->isMaster) {
				if ($showPosteLines) {
					$clientLinesBolded[$client][] = count($clientLines[$client]) + 1;
				}

				$clientLines[$client] = array_merge($clientLines[$client], $projectLines);

				foreach ($projectTotal as $key => $value) {
					$clientTotal[$client][$key] += $value;
				}
			} else {
				$xlsLines = array_merge($xlsLines, $projectLines);
			}
		}

		if ($this->isMaster) {
			foreach ($clientLines as $client => $lines) {
				$clientTotalLine = [$client];

				if(null !== $projectGroups) {
					foreach ($projectGroups as $group) {
						$clientTotalLine[] = '';
					}
				}

				if (in_array('project.code', $columns)) {
					$clientTotalLine[] = $project['code'];
				}

				if (in_array('project.publicationStatus', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('project.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteExpenses.account.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteExpenses.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if(null !== $posteExpenseGroups) {
					foreach ($posteExpenseGroups as $group) {
						$clientTotalLine[] = '';
					}
				}

				if (in_array('posteExpenses.nature.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteExpenses.expenses.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if(null !== $expenseGroups) {
					foreach ($expenseGroups as $group) {
						$clientTotalLine[] = '';
					}
				}

				if (in_array('posteExpenses.amount', $columns)) {
					$clientTotalLine[] = $clientTotal[$client]['poste'];
				}

				if (in_array('posteExpenses.amountHT', $columns)) {
					$clientTotalLine[] = $clientTotal[$client]['poste_ht'];
				}

				if (in_array('posteExpenses.expenses.anneeExercice', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteExpenses.expenses.mandat', $columns)) {
					$clientTotalLine[] = '';
				}
		
				if (in_array('posteExpenses.expenses.bordereau', $columns)) {
					$clientTotalLine[] = '';
				}
	
				if (in_array('posteExpenses.expenses.tiers', $columns)) {
					$clientTotalLine[] = '';
				}
	
				if (in_array('posteExpenses.expenses.complement', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteExpenses.expenses.engagement', $columns)) {
					$clientTotalLine[] = '';
				}
		
				if (in_array('posteExpenses.expenses.dateFacture', $columns)) {
					$clientTotalLine[] = '';
				}
		
				if (in_array('posteExpenses.expenses.createdAt', $columns)) {
					$clientTotalLine[] = '';
				}
		
				if (in_array('posteExpenses.expenses.updatedAt', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteExpenses.expenses.managementUnit.name', $columns)) {
					$clientTotalLine[] = '';
				}

				

				foreach ($this->expenseTypes as $type) {
					if (preg_match('/^ae/', $type)) {
						if (in_array("posteExpenses.expenses.type.$type", $columns)) {
							$clientTotalLine[] = $clientTotal[$client][$type];
						}

						if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
							$clientTotalLine[] = $clientTotal[$client]["{$type}_ht"];
						}
					}
				}

				foreach ($this->posteExpenseBalances as $balance) {
					if (preg_match('/^ae/', $balance) && $balance !== 'aeCommittedCpCommittedBalance') {
						if (in_array("posteExpenses.balance.$balance", $columns)) {
							$clientTotalLine[] = $clientTotal[$client][$balance];
						}

						if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
							$clientTotalLine[] = $clientTotal[$client]["{$balance}_ht"];
						}
					}
				}

				foreach ($this->expenseTypes as $type) {
					if (!preg_match('/^ae/', $type) && !preg_match('/^cp/', $type)) {
						if (in_array("posteExpenses.expenses.type.$type", $columns)) {
							$clientTotalLine[] = $clientTotal[$client][$type];
						}

						if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
							$clientTotalLine[] = $clientTotal[$client]["{$type}_ht"];
						}
					}
				}

				foreach ($this->posteExpenseBalances as $balance) {
					if (!preg_match('/^ae/', $balance) && !preg_match('/^cp/', $balance)) {
						if (in_array("posteExpenses.balance.$balance", $columns)) {
							$clientTotalLine[] = $clientTotal[$client][$balance];
						}

						if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
							$clientTotalLine[] = $clientTotal[$client]["{$balance}_ht"];
						}
					}
				}

				foreach ($this->expenseTypes as $type) {
					if (preg_match('/^cp/', $type)) {
						if (in_array("posteExpenses.expenses.type.$type", $columns)) {
							$clientTotalLine[] = $clientTotal[$client][$type];
						}

						if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
							$clientTotalLine[] = $clientTotal[$client]["{$type}_ht"];
						}
					}
				}

				foreach ($this->posteExpenseBalances as $balance) {
					if (preg_match('/^cp/', $balance) || $balance === 'aeCommittedCpCommittedBalance') {
						if (in_array("posteExpenses.balance.$balance", $columns)) {
							$clientTotalLine[] = $clientTotal[$client][$balance];
						}

						if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
							$clientTotalLine[] = $clientTotal[$client]["{$balance}_ht"];
						}
					}
				}

				if (in_array('posteExpenses.expenses.date', $columns)) {
					$clientTotalLine[] = '';
				}

				$xlsLines[] = $clientTotalLine;
				$linesBolded[] = count($xlsLines);
				$xlsLines = array_merge($xlsLines, $lines);

				$indexClient = $linesBolded[count($linesBolded) - 1];
				foreach ($clientLinesBolded[$client] as $line) {
					$linesBolded[] = $indexClient + $line;
				}
			}
		}

		$totalLine = [];
		$totalInserted = false;

		if ($this->isMaster) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if(null !== $projectGroups) {
			foreach ($projectGroups as $group) {
				$totalLine[] = !$totalInserted ? 'Total' : '';
				$totalInserted = true;
			}
		}

		if (in_array('project.code', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('project.publicationStatus', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('project.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteExpenses.account.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteExpenses.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if(null !== $posteExpenseGroups) {
			foreach ($posteExpenseGroups as $group) {
				$totalLine[] = !$totalInserted ? 'Total' : '';
				$totalInserted = true;
			}
		}

		if (in_array('posteExpenses.nature.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteExpenses.expenses.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
		}

		if(null !== $expenseGroups) {
			foreach ($expenseGroups as $group) {
				$totalLine[] = !$totalInserted ? 'Total' : '';
				$totalInserted = true;
			}
		}

		if (in_array('posteExpenses.amount', $columns)) {
			$totalLine[] = $total['poste'];
		}

		if (in_array('posteExpenses.amountHT', $columns)) {
			$totalLine[] = $total['poste_ht'];
		}

		if (in_array('posteExpenses.expenses.anneeExercice', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.mandat', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.bordereau', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.tiers', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.complement', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.engagement', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.dateFacture', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.createdAt', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.updatedAt', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteExpenses.expenses.managementUnit.name', $columns)) {
			$totalLine[] = '';
		}

		foreach ($this->expenseTypes as $type) {
			if (preg_match('/^ae/', $type)) {
				if (in_array("posteExpenses.expenses.type.$type", $columns)) {
					$totalLine[] = $total[$type];
				}

				if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
					$totalLine[] = $total["{$type}_ht"];
				}
			}
		}

		foreach ($this->posteExpenseBalances as $balance) {
			if (preg_match('/^ae/', $balance) && $balance !== 'aeCommittedCpCommittedBalance') {
				if (in_array("posteExpenses.balance.$balance", $columns)) {
					$totalLine[] = $total[$balance];
				}

				if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
					$totalLine[] = $total["{$balance}_ht"];
				}
			}
		}

		foreach ($this->expenseTypes as $type) {
			if (!preg_match('/^ae/', $type) && !preg_match('/^cp/', $type)) {
				if (in_array("posteExpenses.expenses.type.$type", $columns)) {
					$totalLine[] = $total[$type];
				}

				if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
					$totalLine[] = $total["{$type}_ht"];
				}
			}
		}

		foreach ($this->posteExpenseBalances as $balance) {
			if (!preg_match('/^ae/', $balance) && !preg_match('/^cp/', $balance)) {
				if (in_array("posteExpenses.balance.$balance", $columns)) {
					$totalLine[] = $total[$balance];
				}

				if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
					$totalLine[] = $total["{$balance}_ht"];
				}
			}
		}

		foreach ($this->expenseTypes as $type) {
			if (preg_match('/^cp/', $type)) {
				if (in_array("posteExpenses.expenses.type.$type", $columns)) {
					$totalLine[] = $total[$type];
				}

				if (in_array("posteExpenses.expenses.type.{$type}_ht", $columns)) {
					$totalLine[] = $total["{$type}_ht"];
				}
			}
		}

		foreach ($this->posteExpenseBalances as $balance) {
			if (preg_match('/^cp/', $balance) || $balance === 'aeCommittedCpCommittedBalance') {
				if (in_array("posteExpenses.balance.$balance", $columns)) {
					$totalLine[] = $total[$balance];
				}

				if (in_array("posteExpenses.balance.{$balance}_ht", $columns)) {
					$totalLine[] = $total["{$balance}_ht"];
				}
			}
		}

		if (in_array('posteExpenses.expenses.date', $columns)) {
			$totalLine[] = '';
		}

		$xlsLines[] = $totalLine;

		$linesBolded[] = count($xlsLines);

		ExcelExporter::download($xlsLines, $linesBolded);
	}

	/**
	 * @param array  $projects
	 * @param array  $columns
	 * @param string $export
	 *
	 * @return void
	 */
	protected function incomeExport($projects, $columns, $export)
	{
		$translator = $this->helper->get('translate');
		$isCustomExport = $export !== 'default';

		if ($isCustomExport) {
			Custom::doExport(
				$this->environment()->getClient(),
				$export,
				[],
				$projects,
				$this->entityManager(),
				$translator,
				$this->serviceLocator->get('ControllerPluginManager')->get('apiRequest')
			);

			exit();
		}

		/** @var Group[] $allProjectGroups */
		$allProjectGroups = $this->allowedKeywordGroups('project', false);
        $allIncomeGroups = $this->allowedKeywordGroups('income', false);
        $allPosteIncomeGroups = $this->allowedKeywordGroups('posteIncome', false);

		$projectGroups = [];
		foreach ($columns as $column) {
			if (preg_match('/^project.keyword./', $column)) {
				$id = str_replace('project.keyword.', '', $column);

				foreach ($allProjectGroups as $group) {
					if ($group->getId() == $id) {
						$projectGroups[] = $group;
						break;
					}
				}
			}

            if (preg_match('/^posteIncomes.keyword/', $column)) {
				$id = str_replace('posteIncomes.keyword', '', $column);

				foreach ($allPosteIncomeGroups as $group) {
					if ($group->getId() == $id) {
						$posteIncomeGroups[] = $group;
						break;
					}
				}
			}

            if (preg_match('/^posteIncomes.incomes.keyword/', $column)) {
				$id = str_replace('posteIncomes.incomes.keyword', '', $column);

				foreach ($allIncomeGroups as $group) {
					if ($group->getId() == $id) {
						$incomeGroups[] = $group;
						break;
					}
				}
			}
		}

		$total = $this->getTotalArray();

		$xlsLines = [];

		$clientLines = [];
		$clientLinesBolded = [];
		$clientTotal = [];

		$linesBolded = [];

		$header = [];

		if ($this->isMaster) {
			$header[] = ucfirst($translator->__invoke('client'));
		}

		foreach ($projectGroups as $group) {
			$header[] = $group->getName();
		}

		if (in_array('project.code', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_code'));
		}

		if (in_array('project.publicationStatus', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_publicationStatus'));
		}

		if (in_array('project.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('project_field_name'));
		}

		if (in_array('posteIncomes.account.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('account_entity'));
		}

		if (in_array('posteIncomes.envelope.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('envelope_entity'));
		}

		if (in_array('posteIncomes.envelope.financer.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('envelope_field_financer'));
		}

		if (in_array('posteIncomes.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_income_entity'));
		}

	    foreach ($posteIncomeGroups as $group) {
			$header[] = $group->getName();
		}

		if (in_array('posteIncomes.nature.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_income_field_nature'));		
		}

		if (in_array('posteIncomes.subventionAmount', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_income_field_subventionAmount'));		
		}

		if (in_array('posteIncomes.incomes.name', $columns)) {
			$header[] = ucfirst($translator->__invoke('income_entity'));
		}

        foreach ($incomeGroups as $group) {
			$header[] = $group->getName();
		}

		if (in_array('posteIncomes.amount', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_income_field_amount'));
		}

		if (in_array('posteIncomes.amountHT', $columns)) {
			$header[] = ucfirst($translator->__invoke('poste_income_field_amountHT'));
		}

		if (in_array('posteIncomes.incomes.anneeExercice', $columns)) {
			$header[] = $translator->__invoke("income_field_anneeExercice");
		}

		if (in_array('posteIncomes.incomes.tiers', $columns)) {
			$header[] = $translator->__invoke("income_field_tiers");
		}

		if (in_array('posteIncomes.incomes.complement', $columns)) {
			$header[] = $translator->__invoke("income_field_complement");
		}

		if (in_array('posteIncomes.incomes.managementUnit.name', $columns)) {
			$header[] = $translator->__invoke("income_field_managementUnit");
		}

		foreach ($this->incomeTypes as $type) {
			if (in_array("posteIncomes.incomes.type.$type", $columns)) {
				$header[] = ucfirst($translator->__invoke("income_type_$type"));
			}

			if (in_array("posteIncomes.incomes.type.{$type}_ht", $columns)) {
				$header[] = ucfirst($translator->__invoke("income_type_{$type}_ht"));
			}
		}

		foreach ($this->posteIncomeBalances as $balance) {
			if (in_array("posteIncomes.balance.$balance", $columns)) {
				$header[] = ucfirst($translator->__invoke("poste_income_balance_$balance"));
			}

			if (in_array("posteIncomes.balance.{$balance}_ht", $columns)) {
				$header[] = ucfirst($translator->__invoke("poste_income_balance_{$balance}_ht"));
			}
		}

		if (in_array('posteIncomes.incomes.date', $columns)) {
			$header[] = ucfirst($translator->__invoke('income_field_date'));
		}

		$xlsLines[] = $header;

		foreach ($projects as $i => $project) {
			$client = $project['_client']['name'];

			if ($this->isMaster && !isset($clientLines[$client])) {
				$clientLines[$client] = [];
				$clientTotal[$client] = $this->getTotalArray();
				$clientLinesBolded[$client] = [];
			}

			$projectLines = [];
			$projectLine = [];
			$projectTotal = [];

			$showPosteLines =
				in_array('posteIncomes.name', $columns) ||
				in_array('posteIncomes.account.name', $columns) ||
				in_array('posteIncomes.envelope.name', $columns) ||
				in_array('posteIncomes.envelope.financer.name', $columns);

			$showIncomeLines = in_array('posteIncomes.incomes.name', $columns) ||
			in_array('posteIncomes.incomes.tiers', $columns)||
			in_array('posteIncomes.incomes.anneeExercice', $columns) ||
			in_array('posteIncomes.incomes.complement', $columns);

			if ($this->isMaster) {
				$projectLine[] = '';
			}

			foreach ($projectGroups as $group) {
				$value = '';

				if ($projectGroup = $project['keywords'][$group->getId()]) {
					foreach ($projectGroup as $index => $keyword) {
						if ($index !== 0) {
							$value .= '; ';
						}

						$value .= $keyword['name'];
					}
				}

				$projectLine[] = $value;
			}

			if (in_array('project.code', $columns)) {
				$projectLine[] = $project['code'];
			}

			if (in_array('project.publicationStatus', $columns)) {
				$projectLine[] = ucfirst($translator->__invoke('project_publicationStatus_' . $project['publicationStatus']));
			}

			if (in_array('project.name', $columns)) {
				$projectLine[] = str_repeat('    ', $project['_level']) . $project['name'];
			}

			if (in_array('posteIncomes.account.name', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.envelope.name', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.envelope.financer.name', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.name', $columns)) {
				$projectLine[] = '';
			}

            foreach ($posteIncomeGroups as $group) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.nature.name', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.nature.subventionAmount', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.incomes.name', $columns)) {
				$projectLine[] = '';
			}

            foreach ($incomeGroups as $group) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.amount', $columns)) {
				$projectLine[] = $project['amountPosteIncomeArbo'];

				if ($project['_level'] === 0) {
					$projectTotal['poste'] += $project['amountPosteIncomeArbo'];
					$total['poste'] += $project['amountPosteIncomeArbo'];
				}
			}

			if (in_array('posteIncomes.amountHT', $columns)) {
				$projectLine[] = $project['amountHTPosteIncomeArbo'];

				if ($project['_level'] === 0) {
					$projectTotal['poste_ht'] += $project['amountHTPosteIncomeArbo'];
					$total['poste_ht'] += $project['amountHTPosteIncomeArbo'];
				}
			}
			
			if (in_array('posteIncomes.expenses.anneeExercice', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.expenses.tiers', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.expenses.complement', $columns)) {
				$projectLine[] = '';
			}

			if (in_array('posteIncomes.incomes.managementUnit.name', $columns)) {
				$projectLine[] = '';
			}

			foreach ($this->incomeTypes as $type) {
				if (in_array("posteIncomes.incomes.type.$type", $columns)) {
					$projectLine[] = $project['amountIncomeArbo' . ucfirst($type)];

					if ($project['_level'] === 0) {
						$projectTotal[$type] += $project['amountIncomeArbo' . ucfirst($type)];
						$total[$type] += $project['amountIncomeArbo' . ucfirst($type)];
					}
				}

				if (in_array("posteIncomes.incomes.type.{$type}_ht", $columns)) {
					$projectLine[] = $project['amountHTIncomeArbo' . ucfirst($type)];

					if ($project['_level'] === 0) {
						$projectTotal["{$type}_ht"] += $project['amountHTIncomeArbo' . ucfirst($type)];
						$total["{$type}_ht"] += $project['amountHTIncomeArbo' . ucfirst($type)];
					}
				}
			}

			foreach ($this->posteIncomeBalances as $balance) {
				if (in_array("posteIncomes.balance.$balance", $columns)) {
					$projectLine[] = $project['amountPosteIncomeArbo' . ucfirst($balance)];

					if ($project['_level'] === 0) {
						$projectTotal[$balance] += $project['amountPosteIncomeArbo' . ucfirst($balance)];
						$total[$balance] += $project['amountPosteIncomeArbo' . ucfirst($balance)];
					}
				}

				if (in_array("posteIncomes.balance.{$balance}_ht", $columns)) {
					$projectLine[] = $project['amountHTPosteIncomeArbo' . ucfirst($balance)];

					if ($project['_level'] === 0) {
						$projectTotal["{$balance}_ht"] +=
							$project['amountHTPosteIncomeArbo' . ucfirst($balance)];
						$total["{$balance}_ht"] += $project['amountHTPosteIncomeArbo' . ucfirst($balance)];
					}
				}
			}

			if (in_array('posteIncomes.incomes.date', $columns)) {
				$projectLine[] = '';
			}

			$projectLines[] = $projectLine;

			if ($showPosteLines) {
				if (!$this->isMaster) {
					$linesBolded[] = count($xlsLines) + 1;
				}

				foreach ($project['posteIncomes'] as $poste) {
					$posteLine = [];

					if ($this->isMaster) {
						$posteLine[] = '';
					}

					foreach ($projectGroups as $group) {
						$posteLine[] = '';
					}

					if (in_array('project.code', $columns)) {
						$posteLine[] = $project['code'];
					}

					if (in_array('project.publicationStatus', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('project.name', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('posteIncomes.account.name', $columns)) {
						$posteLine[] = $poste['account']
							? $poste['account']['code'] . ' - ' . $poste['account']['name']
							: '';
					}

					if (in_array('posteIncomes.envelope.name', $columns)) {
						$posteLine[] = $poste['envelope'] ? $poste['envelope']['name'] : '';
					}

					if (in_array('posteIncomes.envelope.financer.name', $columns)) {
						$posteLine[] = $poste['envelope'] ? $poste['envelope']['financer']['name'] : '';
					}

					if (in_array('posteIncomes.name', $columns)) {
						$posteLine[] = $poste['name'];
					}

                    foreach ($posteIncomeGroups as $group) {
				        $value = '';
                        if ($group = $poste['keywords'][$group->getId()]) {
					         foreach ($group as $index => $keyword) {
						        if ($index !== 0) {
							        $value .= '; ';
						        }
                                $value .= $keyword['name'];
					        }
				        }
                        $posteLine[] = $value;
			        }

					if (in_array('posteIncomes.nature.name', $columns)) {
						$posteLine[] = $poste['nature']['name'];
					}

					if (in_array('posteIncomes.subventionAmount', $columns)) {
						$posteLine[] = $poste['subventionAmount'];
					}

					if (in_array('posteIncomes.incomes.name', $columns)) {
						$posteLine[] = '';
					}

                    foreach ($incomeGroups as $group) {
                        $posteLine[] = '';
			        }

					if (in_array('posteIncomes.amount', $columns)) {
						$posteLine[] = $poste['amount'];
					}

					if (in_array('posteIncomes.amountHT', $columns)) {
						$posteLine[] = $poste['amountHT'];
					}
					
					if (in_array('posteIncomes.incomes.anneeExercice', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('posteIncomes.incomes.tiers', $columns)) {
						$posteLine[] = '';
					}
		
					if (in_array('posteIncomes.incomes.complement', $columns)) {
						$posteLine[] = '';
					}

					if (in_array('posteIncomes.incomes.managementUnit.name', $columns)) {
						$posteLine[] = '';
					}

					foreach ($this->incomeTypes as $type) {
						if (in_array("posteIncomes.incomes.type.$type", $columns)) {
							$posteLine[] = $poste['amount' . ucfirst($type)];
						}

						if (in_array("posteIncomes.incomes.type.{$type}_ht", $columns)) {
							$posteLine[] = $poste['amountHT' . ucfirst($type)];
						}
					}

					foreach ($this->posteIncomeBalances as $balance) {
						if (in_array("posteIncomes.balance.$balance", $columns)) {
							$posteLine[] = $poste['amount' . ucfirst($balance)];
						}

						if (in_array("posteIncomes.balance.{$balance}_ht", $columns)) {
							$posteLine[] = $poste['amountHT' . ucfirst($balance)];
						}
					}

					if (in_array('posteIncomes.incomes.date', $columns)) {
						$posteLine[] = '';
					}
					$projectLines[] = $posteLine;

					if ($showIncomeLines) {
						if (!$this->isMaster) {
							$linesBolded[] = count($xlsLines) + 1;
						}
						foreach ($poste['incomes'] as $income) {
							$incomeLine = [];

							if ($this->isMaster) {
								$incomeLine[] = '';
							}

							foreach ($projectGroups as $group) {
								$incomeLine[] = '';
							}

							if (in_array('project.code', $columns)) {
								$incomeLine[] = $project['code'];
							}

							if (in_array('project.publicationStatus', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('project.name', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.account.name', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.envelope.name', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.envelope.financer.name', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.name', $columns)) {
								$incomeLine[] = '';
							}

                            foreach ($posteIncomeGroups as $group) {
                                $incomeLine[] = '';
			                }

							if (in_array('posteIncomes.nature.name', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.subventionAmount', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.incomes.name', $columns)) {
								$incomeLine[] = $income['name'];
							}

                            foreach ($incomeGroups as $group) {
				                $value = '';
                                if ($group = $income['keywords'][$group->getId()]) {
					                foreach ($group as $index => $keyword) {
						                if ($index !== 0) {
							                $value .= '; ';
						                }
                                        $value .= $keyword['name'];
					               }
				                }
                                $incomeLine[] = $value;
			                }

							if (in_array('posteIncomes.amount', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.amountHT', $columns)) {
								$incomeLine[] = '';
							}

							if (in_array('posteIncomes.incomes.anneeExercice', $columns)) {
								$incomeLine[] = $income['anneeExercice'];
							}

							if (in_array('posteIncomes.incomes.tiers', $columns)) {
								$incomeLine[] = $income['tiers'];
							}
				
							if (in_array('posteIncomes.incomes.complement', $columns)) {
								$incomeLine[] = $income['complement'];
							}

							if (in_array('posteIncomes.incomes.managementUnit.name', $columns)) {
								$incomeLine[] = $income['managementUnit']['name'];
							}

							foreach ($this->incomeTypes as $type) {
								if (in_array("posteIncomes.incomes.type.$type", $columns)) {
									$incomeLine[] = $income['type'] === $type ? $income['amount'] : '';
								}

								if (in_array("posteIncomes.incomes.type.{$type}_ht", $columns)) {
									$incomeLine[] = $income['type'] === $type ? $income['amountHT'] : '';
								}
							}

							foreach ($this->posteIncomeBalances as $balance) {
								if (in_array("posteIncomes.balance.$balance", $columns)) {
									$incomeLine[] = '';
								}

								if (in_array("posteIncomes.balance.{$balance}_ht", $columns)) {
									$incomeLine[] = '';
								}
							}

							if (in_array('posteIncomes.incomes.date', $columns)) {
								$incomeLine[] = DateTime::createFromFormat('d/m/Y H:i', $income['date'])->format('d/m/Y');;
							}

							$projectLines[] = $incomeLine;
						}
					}
				}
			}
			
			if ($this->isMaster) {
				if ($showPosteLines) {
					$clientLinesBolded[$client][] = count($clientLines[$client]) + 1;
				}

				$clientLines[$client] = array_merge($clientLines[$client], $projectLines);

				foreach ($projectTotal as $key => $value) {
					$clientTotal[$client][$key] += $value;
				}
			} else {
				$xlsLines = array_merge($xlsLines, $projectLines);
			}
		}

		if ($this->isMaster) {
			foreach ($clientLines as $client => $lines) {
				$clientTotalLine = [$client];

				foreach ($projectGroups as $group) {
					$clientTotalLine[] = '';
				}

				if (in_array('project.code', $columns)) {
					$clientTotalLine[] = '';
				}
				
				if (in_array('project.publicationStatus', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('project.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.account.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.envelope.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.envelope.financer.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.nature.name', $columns)) {
					$clientTotalLine[] = '';
				}


				if (in_array('posteIncomes.subventionAmount', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.incomes.name', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.amount', $columns)) {
					$clientTotalLine[] = $clientTotal[$client]['poste'];
				}

				if (in_array('posteIncomes.amountHT', $columns)) {
					$clientTotalLine[] = $clientTotal[$client]['poste_ht'];
				}

				if (in_array('posteIncomes.incomes.anneeExercice', $columns)) {
					$clientTotalLine[] = '';
				}
	
				if (in_array('posteIncomes.incomes.tiers', $columns)) {
					$clientTotalLine[] = '';
				}
	
				if (in_array('posteIncomes.incomes.complement', $columns)) {
					$clientTotalLine[] = '';
				}

				if (in_array('posteIncomes.incomes.managementUnit.name', $columns)) {
					$clientTotalLine[] = '';
				}


				foreach ($this->incomeTypes as $type) {
					if (in_array("posteIncomes.incomes.type.$type", $columns)) {
						$clientTotalLine[] = $clientTotal[$client][$type];
					}

					if (in_array("posteIncomes.incomes.type.{$type}_ht", $columns)) {
						$clientTotalLine[] = $clientTotal[$client]["{$type}_ht"];
					}
				}

				foreach ($this->posteIncomeBalances as $balance) {
					if (in_array("posteIncomes.balance.$balance", $columns)) {
						$clientTotalLine[] = $clientTotal[$client][$balance];
					}

					if (in_array("posteIncomes.balance.{$balance}_ht", $columns)) {
						$clientTotalLine[] = $clientTotal[$client]["{$balance}_ht"];
					}
				}

				if (in_array('posteIncomes.incomes.date', $columns)) {
					$clientTotalLine[] = '';
				}

				$xlsLines[] = $clientTotalLine;
				$linesBolded[] = count($xlsLines);
				$xlsLines = array_merge($xlsLines, $lines);

				$indexClient = $linesBolded[count($linesBolded) - 1];
				foreach ($clientLinesBolded[$client] as $line) {
					$linesBolded[] = $indexClient + $line;
				}
			}
		}

		$totalLine = [];
		$totalInserted = false;

		if ($this->isMaster) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		foreach ($projectGroups as $group) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('project.code', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}
		
		if (in_array('project.publicationStatus', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('project.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteIncomes.account.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteIncomes.envelope.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteIncomes.envelope.financer.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteIncomes.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteIncomes.nature.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteIncomes.subventionAmount', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
			$totalInserted = true;
		}

		if (in_array('posteIncomes.incomes.name', $columns)) {
			$totalLine[] = !$totalInserted ? 'Total' : '';
		}

		if (in_array('posteIncomes.amount', $columns)) {
			$totalLine[] = $total['poste'];
		}

		if (in_array('posteIncomes.amountHT', $columns)) {
			$totalLine[] = $total['poste_ht'];
		}

		if (in_array('posteIncomes.incomes.anneeExercice', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteIncomes.incomes.tiers', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteIncomes.incomes.complement', $columns)) {
			$totalLine[] = '';
		}

		if (in_array('posteIncomes.incomes.managementUnit.name', $columns)) {
			$totalLine[] = '';
		}

		foreach ($this->incomeTypes as $type) {
			if (in_array("posteIncomes.incomes.type.$type", $columns)) {
				$totalLine[] = $total[$type];
			}

			if (in_array("posteIncomes.incomes.type.{$type}_ht", $columns)) {
				$totalLine[] = $total["{$type}_ht"];
			}
		}

		foreach ($this->posteIncomeBalances as $balance) {
			if (in_array("posteIncomes.balance.$balance", $columns)) {
				$totalLine[] = $total[$balance];
			}

			if (in_array("posteIncomes.balance.{$balance}_ht", $columns)) {
				$totalLine[] = $total["{$balance}_ht"];
			}
		}

		if (in_array('posteIncomes.incomes.date', $columns)) {
			$totalLine[] = '';
		}

		$xlsLines[] = $totalLine;

		$linesBolded[] = count($xlsLines);

		ExcelExporter::download($xlsLines, $linesBolded);
	}

	/**
	 * @param bool $a
	 * @param string export
	 *
	 * @return array
	 */
	protected function expenseApiCol($a, $export)
	{
			$ret = [
			'id',
			'name',
			'code',
			'publicationStatus',
			'parent.id',
			'children.id',
			'amountPosteExpenseArbo',
			'amountHTPosteExpenseArbo',
			"posteExpenses{$this->a($a)}.id",
            "posteExpenses{$this->a($a)}.keywords",
			"posteExpenses{$this->a($a)}.name",
			"posteExpenses{$this->a($a)}.amount",
			"posteExpenses{$this->a($a)}.amountHT",
			"posteExpenses{$this->a($a)}.nature.name",
			"posteExpenses{$this->a($a)}.account.id",
			"posteExpenses{$this->a($a)}.account.code",
			"posteExpenses{$this->a($a)}.account.name",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.id",
            "posteExpenses{$this->a($a)}.expenses{$this->a($a)}.keywords",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.name",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.type",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.amount",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.amountHT",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.date",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.complement",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.mandat",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.bordereau",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.engagement",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.dateFacture",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.createdAt",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.updatedAt",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.anneeExercice",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.tiers",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.amount",
			"posteExpenses{$this->a($a)}.expenses{$this->a($a)}.managementUnit.name",
			'keywords',
		];

		foreach ($this->expenseTypes as $type) {
			$ret[] = 'amountExpenseArbo' . ucfirst($type);
			$ret[] = 'amountHTExpenseArbo' . ucfirst($type);
			$ret[] = "posteExpenses{$this->a($a)}.amount{$this->a($a)}" . ucfirst($type);
			$ret[] = "posteExpenses{$this->a($a)}.amountHT{$this->a($a)}" . ucfirst($type);
		}

		foreach ($this->posteExpenseBalances as $balance) {
			$ret[] = 'amountPosteExpenseArbo' . ucfirst($balance);
			$ret[] = 'amountHTPosteExpenseArbo' . ucfirst($balance);
			$ret[] = "posteExpenses{$this->a($a)}.amount{$this->a($a)}" . ucfirst($balance);
			$ret[] = "posteExpenses{$this->a($a)}.amountHT{$this->a($a)}" . ucfirst($balance);
		}

		if ($export === 'analysis/budget_erd') {
			$ret[] = "posteExpenses{$this->a($a)}.expenses{$this->a($a)}.tiers";
			$ret[] = "posteExpenses{$this->a($a)}.expenses{$this->a($a)}.mandat";
			$ret[] = "posteExpenses{$this->a($a)}.expenses{$this->a($a)}.bordereau";
			$ret[] = "posteExpenses{$this->a($a)}.expenses{$this->a($a)}.complement";
			$ret[] = "posteExpenses{$this->a($a)}.expenses{$this->a($a)}.dateFacture";
		}

		return $ret;
	}

	/**
	 * @param bool $a
	 * @param string export
	 *
	 * @return array
	 */
	protected function incomeApiCol($a, $export)
	{
		$ret = [
			'id',
			'name',
			'code',
			'publicationStatus',
			'parent.id',
			'children.id',
			'amountPosteIncomeArbo',
			'amountHTPosteIncomeArbo',
			"posteIncomes{$this->a($a)}.id",
            "posteIncomes{$this->a($a)}.keywords",
			"posteIncomes{$this->a($a)}.name",
			"posteIncomes{$this->a($a)}.amount",
			"posteIncomes{$this->a($a)}.amountHT",
			"posteIncomes{$this->a($a)}.nature.name",
			"posteIncomes{$this->a($a)}.subventionAmount",
			"posteIncomes{$this->a($a)}.account.id",
			"posteIncomes{$this->a($a)}.account.code",
			"posteIncomes{$this->a($a)}.account.name",
			"posteIncomes{$this->a($a)}.envelope.id",
			"posteIncomes{$this->a($a)}.envelope.name",
			"posteIncomes{$this->a($a)}.envelope.financer.id",
			"posteIncomes{$this->a($a)}.envelope.financer.name",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.id",
            "posteIncomes{$this->a($a)}.incomes{$this->a($a)}.keywords",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.name",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.type",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.amount",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.amountHT",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.date",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.anneeExercice",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.tiers",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.amount",
			"posteIncomes{$this->a($a)}.incomes{$this->a($a)}.managementUnit.name",

			'keywords',
		];

		foreach ($this->incomeTypes as $type) {
			$ret[] = 'amountIncomeArbo' . ucfirst($type);
			$ret[] = 'amountHTIncomeArbo' . ucfirst($type);
			$ret[] = "posteIncomes{$this->a($a)}.amount{$this->a($a)}" . ucfirst($type);
			$ret[] = "posteIncomes{$this->a($a)}.amountHT{$this->a($a)}" . ucfirst($type);
		}

		foreach ($this->posteIncomeBalances as $balance) {
			$ret[] = 'amountPosteIncomeArbo' . ucfirst($balance);
			$ret[] = 'amountHTPosteIncomeArbo' . ucfirst($balance);
			$ret[] = "posteIncomes{$this->a($a)}.amount{$this->a($a)}" . ucfirst($balance);
			$ret[] = "posteIncomes{$this->a($a)}.amountHT{$this->a($a)}" . ucfirst($balance);
		}

		if ($export === 'analysis/budget_suivi-recette') {
			$ret[] = "posteIncomes{$this->a($a)}.folderSendingDate";
			$ret[] = "posteIncomes{$this->a($a)}.folderReceiptDate";
			$ret[] = "posteIncomes{$this->a($a)}.deliberationDate";
			$ret[] = "posteIncomes{$this->a($a)}.arrDate";
			$ret[] = "posteIncomes{$this->a($a)}.arrNumber";
			$ret[] = "posteIncomes{$this->a($a)}.subventionAmount";
			$ret[] = "posteIncomes{$this->a($a)}.subventionAmountHT";
			$ret[] = "posteIncomes{$this->a($a)}.caduciteStart";
			$ret[] = "posteIncomes{$this->a($a)}.caduciteEnd";
			$ret[] = "posteIncomes{$this->a($a)}.caduciteSendProof";
			$ret[] = "posteIncomes{$this->a($a)}.project.id";
			$ret[] = "posteIncomes{$this->a($a)}.project.programmingDate";
			$ret[] = "posteIncomes{$this->a($a)}.project.code";
			$ret[] = "posteIncomes{$this->a($a)}.project.name";
			$ret[] = "posteIncomes{$this->a($a)}.incomes{$this->a($a)}.conventionTitle";
			$ret[] = "posteIncomes{$this->a($a)}.incomes{$this->a($a)}.engagement";
		}

		return $ret;
	}

	/**
	 * @return array
	 */
	protected function getTotalArray()
	{
		$total = [
			'poste' => 0,
			'poste_ht' => 0,
		];

		if ($this->expenseTypes) {
			foreach ($this->expenseTypes as $type) {
				$total[$type] = 0;
				$total["{$type}_ht"] = 0;
			}
		}

		if ($this->incomeTypes) {
			foreach ($this->incomeTypes as $type) {
				$total[$type] = 0;
				$total["{$type}_ht"] = 0;
			}
		}

		if ($this->posteExpenseBalances) {
			foreach ($this->posteExpenseBalances as $balance) {
				$total[$balance] = 0;
				$total["{$balance}_ht"] = 0;
			}
		}

		if ($this->posteIncomeBalances) {
			foreach ($this->posteIncomeBalances as $balance) {
				$total[$balance] = 0;
				$total["{$balance}_ht"] = 0;
			}
		}

		return $total;
	}

	/**
	 * @param array $tree
	 * @param array $ret
	 * @param int   $level
	 *
	 * @return array
	 */
	protected function flatTree($tree, $ret, $level)
	{
		foreach ($tree as $node) {
			$node['_level'] = $level;
			$children = $node['children'];
			unset($node['children']);

			$ret[] = $node;

			if ($children) {
				$ret = $this->flatTree($children, $ret, $level + 1);
			}
		}

		return $ret;
	}

	/**
	 * @param bool $arbo
	 *
	 * @return string
	 */
	protected function a($arbo)
	{
		return $arbo ? 'Arbo' : '';
	}

	/**
	 * @param array $projects
	 * @param bool  $arbo
	 *
	 * @return array
	 */
	protected function normalizeProjects($projects, $arbo)
	{
		if (!$arbo) {
			return $projects;
		}

		$ret = [];

		foreach ($projects as $project) {
			$projectNormalized = [];

			foreach ($project as $key => $value) {
				if ($key === 'posteExpensesArbo' || $key === 'posteIncomesArbo') {
					$key = str_replace('Arbo', '', $key);
				}

				$projectNormalized[$key] = $value;
			}

			foreach (['posteExpenses', 'posteIncomes'] as $property) {
				$postes = [];

				if (!isset($projectNormalized[$property])) {
					continue;
				}

				foreach ($projectNormalized[$property] as $poste) {
					$posteNormalized = [];

					foreach ($poste as $key => $value) {
						if (preg_match('/Arbo/', $key)) {
							$key = str_replace('Arbo', '', $key);
						}

						$posteNormalized[$key] = $value;
					}

					$postes[] = $posteNormalized;
				}

				$projectNormalized[$property] = $postes;
			}

			$ret[] = $projectNormalized;
		}

		return $ret;
	}

	/**
	 * @param array $projects
	 *
	 * @return array
	 */
	protected function makeTree($projects)
	{
		foreach ($projects as $index => &$project) {
			if (!isset($projects[$index])) {
				continue;
			}

			$project['children'] = $this->getChildren($project, $projects);

			$project = $this->cleanPostes($project);

			if (!isset($project['posteExpenses']) || !$project['posteExpenses']) {
				$project['posteExpenses'] = null;
			}

			if (!isset($project['posteIncomes']) || !$project['posteIncomes']) {
				$project['posteIncomes'] = null;
			}

			if (!$project['children'] && !$project['posteExpenses'] && !$project['posteIncomes']) {
				unset($projects[$index]);
			}
		}

		return array_values($projects);
	}

	/**
	 * @param array $project
	 * @param array $projects
	 *
	 * @return array
	 */
	protected function getChildren($project, &$projects)
	{
		$children = [];

		if ($project['children']) {
			foreach ($project['children'] as $child) {
				if (array_key_exists('name', $child) && $child['name']) {
					return $project['children'];
				}

				$childIndex = $this->findProjectIndex(
					$child['id'],
					$projects,
					$this->isMaster ? $project['_client']['id'] : null
				);

				if ($childIndex === null) {
					continue;
				}

				$child = $projects[$childIndex];
				unset($projects[$childIndex]);

				$child['children'] = $this->getChildren($child, $projects);
				$children[] = $child;
			}

			foreach ($children as $index => &$child) {
				$child = $this->cleanPostes($child);

				if (!$child['children'] && (!array_key_exists('posteExpenses', $child) || !$child['posteExpenses']) && (!array_key_exists('posteIncomes', $child) || !$child['posteIncomes'])) {
					unset($children[$index]);
				}
			}

			usort($children, function ($a, $b) {
				return strcmp($a['code'], $b['code']) > 0 ? 1 : -1;
			});
		}

		return array_values($children);
	}

	/**
	 * @param int        $id
	 * @param array      $projects
	 * @param int | null $client_id
	 *
	 * @return int | null
	 */
	protected function findProjectIndex($id, $projects, $client_id)
	{
		foreach ($projects as $index => $project) {
			if ($project['id'] === $id) {
				if (!$client_id) {
					return $index;
				} elseif ($client_id === $project['_client']['id']) {
					return $index;
				}
			}
		}

		return null;
	}

	/**
	 * @param array $project
	 *
	 * @return array
	 */
	protected function cleanPostes($project)
	{
		foreach (['expenses', 'incomes'] as $property) {
			$posteProperty = 'poste' . ucfirst($property);
			if (isset($project[$posteProperty])) {
				foreach ($project[$posteProperty] as $index => $poste) {
					if ($poste['amount'] === 0.0 && !$poste[$property]) {
						unset($project[$posteProperty][$index]);
					}
				}

				if (!$project[$posteProperty]) {
					$project[$posteProperty] = null;
				}
			}
		}

		return $project;
	}

	/**
	 * @param array $project
	 * @param array $poste
	 *
	 * @return string
	 */
	protected function gbcpGetKey($project, $poste)
	{
		return $project['id'] .
			'-' .
			$poste['nature']['id'] .
			'-' .
			$poste['organization']['id'] .
			'-' .
			$poste['destination']['id'];
	}

	/**
	 * @param array $project
	 * @param array $poste
	 * @param array $expenseTypes
	 *
	 * @return array
	 */
	protected function gbcpGetDataLine($project, $poste, $expenseTypes)
	{
		return [
			'project' => [
				'id' => $project['id'],
				'code' => $project['code'],
				'name' => $project['name'],
			],
			'nature' => [
				'id' => $poste['nature']['id'],
				'code' => $poste['nature']['code'],
				'name' => $poste['nature']['name'],
			],
			'organization' => [
				'id' => $poste['organization']['id'],
				'code' => $poste['organization']['code'],
				'name' => $poste['organization']['name'],
			],
			'destination' => [
				'id' => $poste['destination']['id'],
				'code' => $poste['destination']['code'],
				'name' => $poste['destination']['name'],
			],
			'transactions' => [],
			'total' => $this->gbcpGetTotalLine($expenseTypes),
		];
	}

	/**
	 * @param array $expenseTypes
	 *
	 * @return array
	 */
	protected function gbcpGetTotalLine($expenseTypes)
	{
		$ret = [];

		foreach ($expenseTypes as $type) {
			$ret[$type] = 0;
			$ret["{$type}_ht"] = 0;
		}

		$ret['income'] = 0;
		$ret['income_ht'] = 0;

		return $ret;
	}
}
