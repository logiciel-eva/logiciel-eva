<?php

namespace Campain\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('campain:e:campain:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('campain:e:campain', $id)) {
            return $this->notFoundAction();
        }

        $campain = $this->entityManager()->getRepository('Campain\Entity\Campain')->find($id);

        if ($campain && !$this->acl()->ownerControl('campain:e:campain:u', $campain) && !$this->acl()->ownerControl('campain:e:campain:r', $campain)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
			'isMaster' => $this->environment()
				->getClient()
				->isMaster(),
            'campain' => $campain
        ]);
    }
}
