<?php

namespace Directory\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class JobFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'id' => [
                'label' => $this->translator->__invoke('job_field_id'),
                'type'  => 'string'
            ],
            'name' => [
                'label' => $this->translator->__invoke('job_field_name'),
                'type'  => 'string'
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('indicator_field_createdAt'),
                'type'  => 'date'
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('indicator_field_updatedAt'),
                'type'  => 'date'
            ],
        ];

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('job', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type'  => 'keyword-select',
                    'group' => $group->getId()
                ];
            }
        }

        $this->filters = $filters;
    }
}
