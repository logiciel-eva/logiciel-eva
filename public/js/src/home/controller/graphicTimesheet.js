(function (app) {

    app.controller('graphicTimesheetController', [
        '$timeout', '$scope', 'translator', '$http',
        function graphicTimesheetController($timeout, $scope, translator, $http) {
            var self = this;

            self.datas = [];

            self.filters = {
                user: {},
                project: {},
                timesheet: {},
                group: null
            };

            $scope.chart = {
                months: {
                    api: {},
                    options: {
                        chart: {
                            type: 'multiBarChart',
                            showControls: false,
                            showValues: true,
                            stacked: false,
                            height: 400,
                            labelsOutside: true,
                            x: function (d) {
                                return d.label;
                            },
                            y: function (d) {
                                return d.value;
                            },
                            yAxis: {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                }
                            }
                        }
                    },
                    data: [
                        {
                            'key': translator('timesheet_type_done'),
                            'color': '#4f99b4',
                            'values': []
                        }
                    ]
                }
            };

            self.init = function (query) {
                $http({
                    url: '/analysis/time/userMonths?' + $.param({ filters: query ? query.filters : {} }),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;

                    $scope.chart.months.data = []

                    if (typeof self.datas.res !== 'undefined') {
                        for (var clientId in self.datas.res.usersMonths) {
                            var users = self.datas.res.usersMonths[clientId];
                            var i = 0;
                            for (var user in users) {
                                $scope.chart.months.data[i] = {
                                    'key': user,
                                    'color': users[user]['color'],
                                    'values': []
                                }
                                for (var month in users[user]['values']) {
                                    $scope.chart.months.data[i].values.push({
                                        label: month,
                                        value: users[user]['values'][month]
                                    });
                                }
                                i++;
                            }
                        }
                    }
                }, function () { });
            };
        }
    ]);
})(window.EVA.app);
