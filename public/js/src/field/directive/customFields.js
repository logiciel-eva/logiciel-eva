(function(app) {
	app.directive('customFields', [
		'fieldGroupService',
		'fieldValueService',
		'flashMessenger',
		'translator',
		function customFields(fieldGroupService, fieldValueService, flashMessenger, translator) {
			return {
				restrict: 'E',
				scope: {
					entity: '@entity',
					tab: '@tab',
					ngModel: '=ngModel',
					translations: '=?translations',
					right: '@right',
					cascadeSave: '=cascadeSave'
				},
				replace: false,
				transclude: true,
				templateUrl: '/js/src/field/template/custom-fields.html',
				link: function(scope) {
					scope.groups = [];
					scope.values = {};
					scope.errors = {};
					valTab = scope.tab;
					if (scope.tab == 4){
						valTab = [0, 4];
					}

					scope.$on("custom-fields-save", function(event, data) {
                        if(scope.cascadeSave.id===undefined && data.id!==null)
                            scope.cascadeSave.id = data.id;

						// only save the matching custom fields
						// because can listen to a broadcasted event
						if(!!scope.cascadeSave && scope.cascadeSave.entity === data.entity && scope.cascadeSave.id === data.id) {
							scope.save();
						}
					});

					scope.saveRequired = function saveRequired() {
						return !scope.cascadeSave;
					};

					fieldGroupService
						.findAll({
							col: [
								'id',
								'name',
								'enabled',
								'tab',
								'fields.id',
								'fields.name',
								'fields.help',
								'fields.type',
								'fields.meta',
								'fields.order',
								'fields.placeholder',
							],
							search: {
								type: 'list',
								data: {
									sort: 'name',
									order: 'asc',
									filters: {
										entity: {
											op: 'eq',
											val: scope.entity,
										},
										enabled: {
											op: 'eq',
											val: '1',
										},
                                        tab: {
                                            op: 'eq',
                                            val: valTab,
                                        },
									},
								},
							},
						})
						.then(
							function(res) {
								scope.groups = res.rows;
								for (var i in res.rows) {
									var group = res.rows[i];
									for (var j in group.fields) {
										var field = group.fields[j];
										scope.values[field.id] = {
											id: null,
											entity: scope.entity,
											primary: scope.ngModel.id,
											field: field.id,
											value: null,
										};
									}
								}

								fieldValueService
									.findAll({
										right: scope.right,
										col: ['id', 'value', 'field.id', 'createdAt', 'updatedAt'],
										search: {
											type: 'list',
											data: {
												sort: 'id',
												order: 'asc',
												filters: {
													entity: {
														op: 'eq',
														val: scope.entity,
													},
													primary: {
														op: 'eq',
														val: scope.ngModel.id,
													},
												},
											},
										},
									})
									.then(
										function(res) {
											for (var i in res.rows) {
												var fieldValue = res.rows[i];
												if (typeof scope.values[fieldValue.field.id] !== 'undefined') {
													scope.values[fieldValue.field.id].value = fieldValue.value;
													scope.values[fieldValue.field.id].id = fieldValue.id;
													scope.values[fieldValue.field.id].createdAt = fieldValue.createdAt;
													scope.values[fieldValue.field.id].updatedAt = fieldValue.updatedAt;
												}
											}
										},
										function() {},
									);
							},
							function() {},
						);

					scope.toggleCheckbox = function toggleCheckbox(value, fieldValue) {
						if (!angular.isArray(fieldValue.value)) {
							fieldValue.value = [];
						}

						var idx = fieldValue.value.indexOf(value);
						if (idx > -1) {
							fieldValue.value.splice(idx, 1);
						} else {
							fieldValue.value.push(value);
						}
					};

					scope.consent = function(fieldValue, value) {
						fieldValue.value = value;

						if (!fieldValue.id) {
							fieldValue.createdAt = moment().format('DD/MM/YYYY HH:mm');
						} else {
							fieldValue.updatedAt = moment().format('DD/MM/YYYY HH:mm');
						}
					};

					scope.save = function save() {
						scope.loading = true;
						scope.errors = {};

                        if(scope.cascadeSave)
                            Object.keys(scope.values).forEach(key => {
                                scope.values[key].primary = scope.cascadeSave.id;
                            });

						fieldValueService
							.saveAll(scope.values, {
								right: scope.right,
								col: ['id', 'value', 'field.id'],
							})
							.then(
								function(res) {
									var i = 0;
									for (var field in scope.values) {
										var value = res[i];

										if (value.success === true) {
											scope.values[field].id = value.object.id;
											scope.values[field].value = value.object.value;
										} else {
											if (value.errors.fields.value.callbackValue !== 'value_not_created') {
												scope.errors[field] = value.errors;
											}
										}

										i++;
									}

									if (Object.keys(scope.errors).length > 0) {
										flashMessenger.error(translator('error_occured'));
									} else {
										flashMessenger.success(translator('fields_message_saved'));
									}

									scope.loading = false;
								},
								function() {},
							);
					};

					scope.resetRadio = function(field) {
						field.value = null;
					};
				},
			};
		},
	]);
})(window.EVA.app);
