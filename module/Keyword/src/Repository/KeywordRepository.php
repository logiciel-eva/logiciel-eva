<?php

namespace Keyword\Repository;

use Doctrine\ORM\EntityRepository;

class KeywordRepository extends EntityRepository {


    public function findByGroupEntity($entity)
    {
        return $this->createQueryBuilder('k')
            ->select('k')
            ->innerJoin('k.group', 'g')            
            ->where('g.entities like :entity1 or g.entities like :entity2 or g.entities like :entity3 or g.entities like :entity4')
            ->setParameter('entity1', $entity)
            ->setParameter('entity2', '%,' . $entity)
            ->setParameter('entity3', $entity . ',%')
            ->setParameter('entity4', '%,' . $entity . ',%')
            ->getQuery()
            ->getResult();
    }

}