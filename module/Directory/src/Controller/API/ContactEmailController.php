<?php

namespace Directory\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class ContactEmailController extends BasicRestController
{
    protected $entityClass = 'Directory\Entity\ContactEmail';
    protected $repository  = 'Directory\Entity\ContactEmail';
    protected $entityChain = 'directory:e:contactEmail';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->leftJoin('object.contact', 'contact');

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
