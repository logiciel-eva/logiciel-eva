(function(app) {

    app.factory('linkService', [
        'restFactoryService',
        function linkService(restFactoryService) {
            return restFactoryService.create('/api/directory/link');
        }
    ])

})(window.EVA.app);
