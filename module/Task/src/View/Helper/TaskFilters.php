<?php

namespace Task\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class TaskFilters extends EntityFilters
{

    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'name' => [
                'label' => $this->translator->__invoke('task_field_name'),
                'type'  => 'string'
            ]
        ];

        if ($this->moduleManager->isActive('project')) {
            $filters['project'] = [
                'label' => $this->translator->__invoke('task_field_project'),
                'type'  => 'project-select'
            ];
        }

        $filters['users.id'] = [
            'label' => $this->translator->__invoke('task_field_users'),
            'type'  => 'user-select'
        ];

        $filters = array_merge($filters, [
            'start' => [
                'label' => $this->translator->__invoke('task_field_start'),
                'type'  => 'date'
            ],
            'end' => [
                'label' => $this->translator->__invoke('task_field_end'),
                'type'  => 'date'
            ],
            'progress' => [
                'label' => $this->translator->__invoke('task_field_progress'),
                'type'  => 'number'
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('task_field_createdAt'),
                'type'  => 'date'
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('task_field_updatedAt'),
                'type'  => 'date'
            ],
        ]);

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('task', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type'  => 'keyword-select',
                    'group' => $group->getId(),
                ];
            }
        }


        $this->filters = $filters;
    }
}
