(function(app) {

  app.service('projectUpdateService', [
      '$http',
      function ($http) {
        return {
          findByProject: function (projectId) {
            return $http({
              method: 'GET',
              url: '/api/project/' + projectId + '/update-history'
            })
            .then(
              function (res) {
                if (res.data && res.data.rows) {
                  var rows = res.data.rows;
                  rows = rows.map(function (row) {
                    row.date = new Date(row.date);
                    return row;
                  });
                  return rows;
                }

                throw new Error('Invalid response for project updates history');
              }
            );
          }
        }
      }
  ])

})(window.EVA.app);
