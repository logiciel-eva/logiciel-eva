<?php

namespace Directory;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'directory' => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/directory',
                    // 'defaults' => [
                    //     'controller' => 'directory\Controller\Index',
                    //     'action'     => 'index',
                    // ],
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'contact'   => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/contact',
                            'defaults' => [
                                'controller' => 'Directory\Controller\Contact',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form'        => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/form[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\Contact',
                                        'action'     => 'form',
                                    ],
                                ],
                            ],
                            'duplicate'   => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/duplicate/:id',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\Contact',
                                        'action'     => 'duplicate',
                                    ],
                                ],
                            ],
                            'unsubscribe' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/unsubscribe/:id',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\Contact',
                                        'action'     => 'unsubscribe',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'structure' => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/structure',
                            'defaults' => [
                                'controller' => 'Directory\Controller\Structure',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form'      => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/form[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\Structure',
                                        'action'     => 'form',
                                    ],
                                ],
                            ],
                            'duplicate' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/duplicate/:id',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\Structure',
                                        'action'     => 'duplicate',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'job' => [
                        'type' => 'Literal',
                        'options' => [
                            'route'    => '/job',
                            'defaults' => [
                                'controller' => 'Directory\Controller\Job',
                                'action'     => 'index'
                            ]
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/form[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\Job',
                                        'action'     => 'form',
                                    ],
                                ],
                            ],
                            'import' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/import',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\Job',
                                        'action'     => 'import',
                                    ],
                                ],
                            ],
                        ]
                    ]
                ],
            ],
            'api'       => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'directory' => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route' => '/directory',
                        ],
                        'may_terminate' => false,
                        'child_routes'  => [
                            'contact'             => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/contact[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\API\Contact',
                                    ],
                                ],
                                'may_terminate' => true,
                                'child_routes'  => [
                                    'form' => [
                                        'type'    => 'Segment',
                                        'options' => [
                                            'route'    => '/fusion',
                                            'defaults' => [
                                                'controller' => 'Directory\Controller\API\Contact',
                                                'action'     => 'fusion',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            'contactJobStructure' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/link[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\API\ContactJobStructure',
                                    ],
                                ],
                            ],
                            'contactEmail'             => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/contact-email[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\API\ContactEmail',
                                    ],
                                ],
                            ],
                            'job' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/job[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\API\Job'
                                    ]
                                ],
                                'may_terminate' => true,
                                'child_routes'  => [
                                    'form' => [
                                        'type'    => 'Segment',
                                        'options' => [
                                            'route'    => '/fusion',
                                            'defaults' => [
                                                'controller' => 'Directory\Controller\API\Job',
                                                'action'     => 'fusion',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            'structure'           => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/structure[/:id]',
                                    'defaults' => [
                                        'controller' => 'Directory\Controller\API\Structure',
                                    ],
                                ],
                                'may_terminate' => true,
                                'child_routes'  => [
                                    'form' => [
                                        'type'    => 'Segment',
                                        'options' => [
                                            'route'    => '/fusion',
                                            'defaults' => [
                                                'controller' => 'Directory\Controller\API\Structure',
                                                'action'     => 'fusion',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\ContactController::class                 => ServiceLocatorFactory::class,
            Controller\StructureController::class               => ServiceLocatorFactory::class,
            Controller\JobController::class                      => ServiceLocatorFactory::class,
            Controller\API\ContactController::class             => ServiceLocatorFactory::class,
            Controller\API\ContactJobStructureController::class => ServiceLocatorFactory::class,
            Controller\API\ContactEmailController::class               => ServiceLocatorFactory::class,
            Controller\API\JobController::class                 => ServiceLocatorFactory::class,
            Controller\API\StructureController::class           => ServiceLocatorFactory::class,
        ],
        'aliases'   => [
            'Directory\Controller\Contact'                 => Controller\ContactController::class,
            'Directory\Controller\Structure'               => Controller\StructureController::class,
            'Directory\Controller\Job'               => Controller\JobController::class,
            'Directory\Controller\API\Contact'             => Controller\API\ContactController::class,
            'Directory\Controller\API\ContactJobStructure' => Controller\API\ContactJobStructureController::class,
            'Directory\Controller\API\ContactEmail'               => Controller\API\ContactEmailController::class,
            'Directory\Controller\API\Job'                 => Controller\API\JobController::class,
            'Directory\Controller\API\Structure'           => Controller\API\StructureController::class,
        ],
    ],
];
