<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 04/08/16
 * Time: 13:44
 */

namespace Time\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use User\Entity\User;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\Factory;

/**
 * Class Bookmark
 * @package Time\Entity
 * @author Curtis Pelissier <curtis.pelissier@laposte.net>
 *
 * @ORM\Entity
 * @ORM\Table(name="time_bookmark")
 */
class Bookmark extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * Datas to save in configuration
     * @var array
     * @ORM\Column(type="json", nullable=true)
     */
    protected $config;

    /**
     * Bookmark's User
     * @var User
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'config',
                'required' => false,
            ],
            [
                'name'     => 'user',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $user = $entityManager->getRepository('User\Entity\User')->find($value);

                                return $user !== null;
                            },
                            'message' => 'timesheet_field_user_error_non_exists'
                        ],
                    ]
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return $this->user == $user;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $user
     * @param string $owner
     */
    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        $queryBuilder->andWhere('object.user = :ownerControl');
        $queryBuilder->setParameter('ownerControl', $user);
    }
}
