<?php

namespace Core\View\Helper;

use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class Icon
 *
 * @package Core\View\Helper
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class Icon extends AbstractHelper
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var array
     */
    protected $icons;

    /**
     * @var string
     */
    protected $env;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->icons          = $this->serviceLocator->get('MergedConfig')['icons'];
        $this->env            = getenv('APP_ENV') ? : 'development';
    }

    /**
     * @param string $key
     * @param string $classes
     * @param array $attrs
     * @return string
     */
    public function __invoke($key, $classes = '', $attrs = [])
    {
        $icon = '';

        if ($this->env == 'development' && !isset($attrs['title'])) {
            $attrs['title'] = 'icon : ' . $key;
        }

        if (isset($this->icons[$key])) {
            switch ($this->icons[$key]['type']) {
                case 'css':
                    $element     = $this->icons[$key]['element'];
                    $classes    = $this->icons[$key]['classes'] . ' ' . $classes;
                    $attributes = [];

                    foreach ($attrs as $attrName => $attrValue) {
                        $attributes[] = $attrName . '="' . $attrValue . '"';
                    }

                    $icon = '<' . $element . ' class="' . $classes . '" ' . implode(' ', $attributes) . '></' . $element . '>';
                    break;
            }
        }

        return $icon;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
