<?php

namespace User\Export;

use Core\Export\IExporter;

abstract class User implements IExporter
{
    public static function getConfig()
    {
        return [
            'role.name' => [
                'name' => 'role',
            ],
            'gender' => [
                'translation' => 'user_field_gender',
                'format' => function ($value, $row, $translator) {
                    if ($value) {
                        return $translator('user_gender_' . $value);
                    } else {
                        return '';
                    }
                }
            ],
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
