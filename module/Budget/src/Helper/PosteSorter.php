<?php

namespace Budget\Helper;

use DateTime;

class PosteSorter
{

    private static $SORTING_PROPERTIES = ['createdAt', 'updatedAt', 'name'];

    public static function sortPostes($postes, $search, $type)
    {
        $sort = $search['data']['sort'];
        $direction = $search['data']['order'];

        $specificExpenseSort = preg_match('/^' . $type . '/', $sort);
        $computedExpenseSort = preg_match('/^computed./', $sort);

        if (!$specificExpenseSort && !$computedExpenseSort && !in_array($sort, PosteSorter::$SORTING_PROPERTIES)) {
            return $postes;
        }
        if ($specificExpenseSort) { // remove 'expenses.' from sorting property
            $sort = str_replace($type . '.', '', $sort);
        }
        if ($computedExpenseSort) { // remove 'computed.' from sorting property (on arbo amounts)
            $sort = str_replace('computed.', '', $sort);
        }

        // amount property to sort
        $amountType = null;
        if (preg_match('/^amountHTArbo/', $sort)) {
            $amountType = lcfirst(str_replace('amountHTArbo', '', $sort));
            $sort = 'amountHT';
        } elseif (preg_match('/^amountArbo/', $sort)) {
            $amountType = lcfirst(str_replace('amountArbo', '', $sort));
            $sort = 'amount';
        }

        $sortedPostes = [];
        foreach ($postes as $poste) {
            $poste[$type . 'Arbo'] = isset($amountType)
                ? PosteSorter::sortAmount($sort, $direction, $poste[$type . 'Arbo'], $amountType)
                : PosteSorter::sort($sort, $direction, $poste[$type . 'Arbo']);
            $sortedPostes[] = $poste;
        }
        return $sortedPostes;
    }


    private static function sortAmount($property, $direction, $array, $type)
    {
        if (!isset($array)) {
            return $array;
        }
        usort($array, function ($a, $b) use ($type, $property, $direction) {
            $result = 0;
            // both with the wrong amount type
            if ($a['type'] !== $type && $b['type'] !== $type) {
                $result = 0;
            } elseif ($a['type'] !== $type) {
                $result = -1;
            } elseif ($b['type'] !== $type) {
                $result = 1;
            } elseif (is_numeric($a[$property]) && is_numeric($b[$property])) {
                $result = $a[$property] - $b[$property];
            }
            // apply direction
            return 'desc' === $direction ? -$result : $result;
        });
        return $array;
    }

    private static function sort($property, $direction, $array)
    {
        if (!isset($array)) {
            return $array;
        }
        usort($array, function ($a, $b) use ($property, $direction) {
            $result = 0;
            // both null
            if (!isset($a[$property]) && !isset($b[$property])) {
                $result = 0;
            }
            // first null
            elseif (!isset($a[$property])) {
                $result = -1;
            }
            // second null
            elseif (!isset($b[$property])) {
                $result = 1;
            }
            // integer or float comparison
            elseif (is_numeric($a[$property]) && is_numeric($b[$property])) {
                $result = $a[$property] - $b[$property];
            }
            // date comparison
            elseif (preg_match('/^date/', $property) || preg_match('/At$/', $property)) {
                $dateA = DateTime::createFromFormat('d/m/Y H:i', $a[$property]);
                $dateB = DateTime::createFromFormat('d/m/Y H:i', $b[$property]);
                if (!$dateA || !$dateB) {
                    return 0;
                }
                $result = $dateA->getTimestamp() - $dateB->getTimestamp();
            }
            // string comparison
            elseif (is_string($a[$property]) && is_string($b[$property])) {
                $result = strcasecmp($a[$property], $b[$property]);
            }
            // apply direction
            return 'desc' === $direction ? -$result : $result;
        });
        return $array;
    }
}
