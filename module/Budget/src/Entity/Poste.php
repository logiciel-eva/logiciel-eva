<?php

namespace Budget\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Project;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Poste
 *
 * @package Budget\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\MappedSuperclass
 */
abstract class Poste extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Account")
     */
    protected $account;

    /**
     * @var Nature
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Nature")
     */
    protected $nature;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Organization")
     */
    protected $organization;

    /**
     * @var Destination
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Destination")
     */
    protected $destination;
    
    /**
     * @var Project
     */
    protected $project;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $amount;

    /**
     * @var float
     *
     * @ORM\Column(type="float", options={"default" : 0})
     */
    protected $amountHT = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return Nature
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * @param Nature $nature
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param Destination $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getAmountHT()
    {
        return $this->amountHT;
    }

    /**
     * @param float $amountHT
     */
    public function setAmountHT($amountHT)
    {
        $this->amountHT = $amountHT;
    }

    abstract function findParent();

    abstract function getAmountArbo();

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'      => 'account',
                'required'  => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $account = $entityManager->getRepository('Budget\Entity\Account')->find($value);


                                return $account !== null;
                            },
                            'message' => 'poste_field_account_error_non_exists'
                        ],
                    ],
                ],
            ],
            [
                'name'      => 'nature',
                'required'  => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                return $entityManager->getRepository(Nature::class)->find($value) !== null;
                            },
                            'message' => 'poste_field_nature_error_non_exists'
                        ],
                    ],
                ],
            ],
            [
                'name'      => 'organization',
                'required'  => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                return $entityManager->getRepository(Organization::class)->find($value) !== null;
                            },
                            'message' => 'poste_field_organization_error_non_exists'
                        ],
                    ],
                ],
            ],
            [
                'name'      => 'destination',
                'required'  => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                return $entityManager->getRepository(Destination::class)->find($value) !== null;
                            },
                            'message' => 'poste_field_destination_error_non_exists'
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'project',
                'required' => true,
                'filters'  => [
                    ['name' => 'Core\Filter\IdFilter'],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $project = $entityManager->getRepository('Project\Entity\Project')->find($value);

                                return $project !== null;
                            },
                            'message' => 'poste_field_project_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'amount',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse']
                ]
            ],
            [
                'name'     => 'amountHT',
                'required' => false,
                'filters'  => [
                    ['name' => 'NumberParse']
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        if ($this->getProject()) {
            return $this->getProject()->ownerControl($user, $crudAction, $owner, $entityManager);
        }

        return false;
    }
}
