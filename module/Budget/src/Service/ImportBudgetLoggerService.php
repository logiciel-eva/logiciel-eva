<?php

namespace Budget\Service;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class ImportBudgetLoggerService implements LoggerInterface
{
    public function __construct(private string $logFile)
    {
    }

    public function getLogfile() : String {
        return $this->logFile;
    }

    public function emergency($message, array $context = array()): void
    {
        $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    public function alert($message, array $context = array()): void
    {
        $this->log(LogLevel::ALERT, $message, $context);
    }

    public function critical($message, array $context = array()): void
    {
        $this->log(LogLevel::CRITICAL, $message, $context);
    }

    public function error($message, array $context = array()): void
    {
        $this->log(LogLevel::ERROR, $message, $context);
    }

    public function warning($message, array $context = array()): void
    {
        $this->log(LogLevel::WARNING, $message, $context);
    }

    public function notice($message, array $context = array()): void
    {
        $this->log(LogLevel::NOTICE, $message, $context);
    }

    public function info($message, array $context = array()): void
    {
        $this->log(LogLevel::INFO, $message, $context);
    }

    public function debug($message, array $context = array()): void
    {
        $this->log(LogLevel::DEBUG, $message, $context);
    }

    public function log($level, $message, array $context = array()): void
    {
        file_put_contents(
            $this->logFile,
            sprintf(
                "[%s][%s] %s\n",
                (new \DateTimeImmutable())->format('Y-m-d H:i:s'),
                $level,
                $message
            ),
            FILE_APPEND
        );

        if($context !== []){
            file_put_contents(
                $this->logFile,
                sprintf(
                    "%s\n",
                    implode("\n\t",$context)
                ),
                FILE_APPEND
            );
        }
    }
}