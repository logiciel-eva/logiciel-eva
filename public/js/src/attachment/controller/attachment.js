(function(app) {

    app.controller('attachmentController', [
        'attachmentService', '$scope', 'FileUploader', 'flashMessenger', 'translator',
        function attachmentController(attachmentService, $scope, FileUploader, flashMessenger, translator) {
            var self = this;

            self.attachments = [];
            self.options = {
                name: 'attachments-list',
                col: [
                    'id',
                    'name',
                    'type',
                    'size',
                    'entity',
                    'primary',
                    'url',
                    'user.id',
                    'user.avatar',
                    'user.name',
                    'createdAt',
                    'updatedAt',
                    'key'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: false,
                getItems: function getItems(params, callback, abort) {
                    if (self.from.id) {

                        params.search.data.filters.entity = {
                            op: 'eq',
                            val: self.from.type
                        };
                        params.search.data.filters.primary = {
                            op: 'eq',
                            val: self.from.id
                        };

                        params.right = self.from.right;

                        attachmentService.findAll(params, abort.promise).then(function (res) {
                            callback(res);
                        });
                    } else {
                        callback({rows: []});
                    }
                }
            };

            self.from = {
                type: null,
                id: null,
                right: null
            };

            self.init = function init(from, right, id, model_id) {
                self.from.type  = from;
                self.from.id    = id;
                self.from.right = right;

                $scope.$watch(function () { return model_id }, function (newVal, oldVal) {
                    if (newVal !== null) {
                        self.id = newVal;
                    }
                });
            };

            var $modal = angular.element('#attachmentFormModal');
            self.attachment = null;
            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };
            self.panelTitle = '';
            self.createAttachment = function createAttachment() {
                
                self.errors = {
                    fields: {}
                };
                self.attachment = {
                    entity: self.from.type,
                    primary: self.id,
                    right: self.right,
                    file: null,
                    fromDrop: false
                };

                self.panelTitle = translator('attachment_nav_form');
                self.uploader.cancelAll();
                self.uploader.clearQueue();

                self.isImage = false;

                $modal.modal('show');
            };

            self.editAttachment = function editAttachment(attachment) {
                self.errors = {
                    fields: {}
                };
                self.attachment = angular.copy(attachment);
                self.panelTitle = attachment.name;
                self.uploader.cancelAll();
                self.uploader.clearQueue();
                self.isImage = self.fileIsImage(attachment.url);
                $modal.modal('show');
            };
            
            $scope.$on('save-attachment', function (event, primary) {
                if(self.attachment !== null){
                    if($scope.attachment.primary === null && primary !== null){
                        $scope.attachment.primary = primary;
                    }

                    self.saveAttachment();
                }
            });

            self.saveAttachment = function saveAttachment() {
                var isCreation = self.attachment.id == null;
                self.loading.save = true;
                self.attachment.entity = self.from.type;
                self.attachment.primary = self.id;

                attachmentService.save(self.attachment, {
                    col: self.options.col,
                    right: self.from.right
                }).then(function(res) {
                    if (res.success) {
                        if (isCreation) {
                            self.attachments.push(res.object);
                        } else {
                            for (var i in self.attachments) {
                                if (self.attachments[i].id == res.object.id) {
                                    self.attachments[i] = res.object;
                                }
                            }
                        }
                        $modal.modal('hide');
                    }

                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                });
            };

            self.isImage = false;
            self.uploader = new FileUploader();
            self.uploader.filters.push({
                name: 'imageChecker',
                fn: function(item, options) {
                    self.isImage = self.fileIsImage(item.name);
                    return true;
                }
            });

            self.reader   = new FileReader();
            self.reader.onloadend = function(e) {
                $scope.$apply(function() {
                    self.attachment.file = e.target.result;
                })
            };

            self.uploader.onAfterAddingFile = function(fileItem) {
                self.reader.readAsDataURL(fileItem._file);
                self.attachment.filename = fileItem.file.name;
                self.attachment.size = fileItem.file.size;
                self.errors.fields['file'] = null;
            };

            self.deleteAttachment = function (attachment) {
                if (confirm(translator('attachment_question_delete').replace('%1', attachment.name))) {
                    attachmentService.remove(attachment, {
                        right: self.from.right
                    }).then(function (res) {
                        if (res.success) {
                            self.attachments.splice(self.attachments.indexOf(attachment), 1);
                            flashMessenger.success(translator('attachment_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            $scope.$on('edit-attachement', function(event, id) {
                self.init(self.from.type, self.from.right, id, id);
            });

            var $dropZone = angular.element('.drop-zone');
            $dropZone.on('dragover', function(e) {

                e.preventDefault();
                e.stopPropagation();

                $dropZone.addClass('drag-over');

                e.originalEvent.dataTransfer.dropEffect = 'copy';
            });

            $dropZone.on('dragleave', function(e) {
                $dropZone.removeClass('drag-over');
            });

            $dropZone.on('drop', function(e) {

                e.preventDefault();
                e.stopPropagation();

                self.createAttachment();
                self.attachment.type = 'file';
                var files = e.originalEvent.dataTransfer.files;
                for (var i = 0; i <= files.length; i++) {
                    var file = files[i];
                    self.attachment.name = file.name;
                    self.uploader.addToQueue(file);
                    break;
                }

                self.attachment.fromDrop = true;
                $dropZone.removeClass('drag-over');
            });

            self.fileIsImage = function (filename) {
                var type = '|' + filename.slice(filename.lastIndexOf('.') + 1).toLowerCase() + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            };
        }
    ])

})(window.EVA.app);
