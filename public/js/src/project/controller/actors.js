(function(app) {

    app.controller('actorsController', [
        'actorService', '$scope', 'flashMessenger', 'translator', '$timeout',
        function actorsController(actorService, $scope, flashMessenger, translator, $timeout) {
            var self = this;

            self.from   = {
                type: null,
                id: null
            };
            self.object = null;

            var $modal = angular.element('#actorFormModal');

            self.actors = [];
            self.actor = {};
            self.loading = {
                save: false
            };

            self.options = {
                name: 'actors-list',
                col: [
                    'id',
                    'project.id',
                    'structure.id',
                    'structure.name',
                    'structure.sigle',
                    'structure.phone2',
                    'structure.financer',
                    'structure.email',
                    'structure.phone',
                    'structure.zipcode',
                    'structure.addressLine1',
                    'structure.addressLine2',
                    'structure.city',
                    'structure.country',
                    'structure.siret',
                    'structure.keywords',
                    'keywords',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'structure.name',
                order: 'asc',
                pager: false,
                getItems: function getItems(params, callback, abort) {
                    if ($scope.$parent.self[self.from.type].id || self.from.id) {
                        if (self.from.type == 'project') {
                            params.search.data.filters['project.id'] = {
                                op: 'eq',
                                val: self.object.id || self.from.id
                            };
                        }

                        actorService.findAll(params, abort.promise).then(function (res) {
                            callback(res);
                        });
                    } else {
                        callback({rows: []});
                    }
                }
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id   = id;
                if (self.from.type == 'project') {
                    self.object = $scope.$parent.self[self.from.type]
                }
            };

            self.editActor = function editActor(actor) {
                self.editedActor = actor;
                self.actor = angular.copy(actor);
                self.panelTitle = self.actor.structure.name;

                self.errors = {
                    fields: {}
                };

                $modal.modal('show');
            };

            self.createActor = function createActor() {
                self.actor = {
                    _rights: {
                        update: true
                    }
                };

                self.errors = {
                    fields: {}
                };

                if (self.from.type == 'project') {
                    self.actor.project = self.object.id;
                }
                $modal.modal('show');
            };

            self.deleteActor = function deleteActor(actor) {
                if (confirm(translator('actor_question_delete').replace('%1', actor.structure.name))) {
                    actorService.remove(actor).then(function (res) {
                        if (res.success) {
                            self.actors.splice(self.actors.indexOf(actor), 1);
                            flashMessenger.success(translator('actor_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.saveActor = function saveActor() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.actor.id;

                actorService.save(self.actor, {col: self.options.col}).then(function(res) {
                    if (isCreation) {
                        self.actors.push(res.object);
                    } else {
                        for(var i in self.actors){
                            if(self.actors[i].id == res.object.id){
                                self.actors[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('actor_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };
        }
    ])

})(window.EVA.app);
