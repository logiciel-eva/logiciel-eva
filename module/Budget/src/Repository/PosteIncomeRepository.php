<?php

namespace Budget\Repository;

use Budget\Entity\Envelope;
use Doctrine\ORM\EntityRepository;

class PosteIncomeRepository extends EntityRepository {

    public function getAmountByEnvelope(Envelope $envelope) {
        return $this->createQueryBuilder('pi')
            ->select('SUM(pi.amount) as amount')
            ->where('pi.envelope = :envelope')
            ->setParameter('envelope', $envelope)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAllByEnvelope(Envelope $envelope) {
        return $this->createQueryBuilder('pi')
            ->where('pi.envelope = :envelope')
            ->setParameter('envelope', $envelope)
            ->groupBy('pi.project')
            ->getQuery()
            ->getResult();
    }

}