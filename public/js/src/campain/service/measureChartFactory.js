(function (app) {
	app.factory('measureChartFactory', ['$filter',
		function measureChartFactory($filter) {
			var self = this;

            self.createForMeasureGroups = function (title, groupedMeasures) {
                var chart = {
                    options: self.getChartOptions(title, 800),
                    api: null,
                    data: self.getDataByGroups(groupedMeasures)
                };


				return chart;
            };

			self.createForMeasures = function (title, measures) {
				var chart = {
                        options: self.getChartOptions(title, 400),
                        api: null,
                        data: self.getDataByMeasure(measures, '#c3c3c3', '#97bbcd', '', '')
                    };


				return chart;
			}

            self.getDataByGroups= function(groupedMeasures) {
                data = [];
                for (var groupName in groupedMeasures) {
                    var groupData = self.getDataByMeasure(groupedMeasures[groupName], null, null, groupName)
                    data = data.concat(groupData);
                }

                return data;
            }

            self.getDataByMeasure = function(measures, plannedColor, doneColor, title) {
                 var data = [{
                            values: [],
                            key:  'Prévu ' + title,
                        },
                        {
                            values: [],
                            key: 'Réalisé ' + title,
                        }
                ];
                
                if (plannedColor) {
                    data[0].color = plannedColor;
                }

                if (doneColor) {
                    data[1].color = doneColor;
                }

                var measures = $filter('groupBy')(measures, function(measure) {
                    return moment(measure.realiseMeasure.date, 'DD/MM/YYYY HH:mm').format('X')
                });

                var lastTarget = null;
                var lastDone = null;
                for (var time in measures) {
                    var target = measures[time][0].targetMeasure;
                    var done = measures[time][0].realiseMeasure;

                    if (target) {
                        lastTarget = target;
                    }

                    if (done) {
                        lastDone = done;
                    }


                    data[0].values.push({
                        value: lastTarget ? lastTarget.value : 0,
                        label: parseInt(time)
                    });


                    data[1].values.push({
                        value: lastDone ? lastDone.value : 0,
                        label: parseInt(time)
                    });
                }

                return data;
            }


           	self.getChartOptions = function (title, height) {
				return  {
                            chart: {
                                type: 'lineChart',
                                height: height,
                                useInteractiveGuideline: true,
                                x: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.label;
                                    }
                                    return null;
                                },
                                y: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.value;
                                    }
                                    return null;
                                },
                                xAxis: {
                                    tickFormat: function(d) {
                                        return moment(d, 'X').format('DD/MM/YYYY')
                                    }
                                }
                            },
                            title: {
                                enable: true,
                                text: title
                            }
                        };
			}

			return {
				createForMeasures: function createForMeasures(title, measures) {
					return self.createForMeasures(title, measures);
				},
                createForMeasureGroups: function createForMeasureGroups(title, groupedMeasures) {
					return self.createForMeasureGroups(title, groupedMeasures);
				} 
			};

		}
	])

})(window.EVA.app);
