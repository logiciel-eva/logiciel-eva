(function(app) {

    app.factory('posteExpenseService', [
        'restFactoryService',
        function posteExpenseService(restFactoryService) {
            return restFactoryService.create('/api/budget/poste-expense');
        }
    ])

})(window.EVA.app);
