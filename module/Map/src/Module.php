<?php

namespace Map;

use Core\Module\AbstractModule;
use Map\View\Helper\TerritoryFilters;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories'  => [
                'territoryFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    return new TerritoryFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                },
            ],
        ];
    }
}
