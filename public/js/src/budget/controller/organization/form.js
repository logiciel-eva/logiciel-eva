(function(app) {
    app.controller('organizationFormController', [
        'organizationService',
        'flashMessenger',
        'translator',
        function (organizationService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: ['id', 'code', 'name', 'parent.id', 'parent.code', 'parent.name'],
            };

            self.loading = {
                save: false,
            };

            self.errors = {
                fields: {},
            };

            self.organization = {
                _rights: {
                    update: true,
                },
            };

            self.init = function (id) {
                if (id) {
                    organizationService.find(id, apiParams).then(function(res) {
                        self.organization = res.object;
                        self.panelTitle = self.organization.name;
                    });
                }
            };

            self.saveOrganization = function () {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.organization.id;

                organizationService.save(self.organization, apiParams).then(
                    function(res) {
                        self.organization = res.object;
                        self.panelTitle = self.organization.name;
                        self.loading.save = false;

                        if (isCreation) {
                            var url = window.location.pathname + '/' + self.organization.id;
                            history.replaceState('', '', url);
                        }

                        flashMessenger.success(translator('organization_message_saved'));
                    },
                    function(err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }

                        self.loading.save = false;
                    },
                );
            };

            self.deleteOrganization = function () {
                if (confirm(translator('organization_question_delete').replace('%1', self.organization.name))) {
                    organizationService.remove(self.organization).then(function(res) {
                        if (res.success) {
                            window.location = '/budget/organization';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        },
    ]);
})(window.EVA.app);
