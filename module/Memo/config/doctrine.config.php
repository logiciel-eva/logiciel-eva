<?php

namespace Memo;

return [
    'doctrine' => [
        'driver' => [
            'memo_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Memo\Entity' => 'memo_entities'
                ]
            ]
        ]
    ]
];
