<?php

namespace Keyword\Controller\API;

use Core\Controller\BasicRestController;
use Core\Controller\BasicRestSyncController;
use Keyword\Entity\Association;
use Keyword\Entity\Keyword;
use Laminas\View\Model\JsonModel;

class KeywordController extends BasicRestSyncController
{
    protected $entityClass = 'Keyword\Entity\Keyword';
    protected $repository  = 'Keyword\Entity\Keyword';
    protected $entityChain = 'keyword:e:keyword';

    protected function searchList($queryBuilder, &$data)
    {
        if (!isset($data['filters']['archived'])) {
            $data['filters']['archived'] = [
                'op'  => 'eq',
                'val' => false,
            ];
        }

        $sort  = isset($data['sort']) ? $data['sort'] : 'object.name';
        $order = isset($data['order']) ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->join('object.group', 'group');
        $queryBuilder->leftJoin('object.parents', 'parents');

        $queryBuilder->orderBy('LENGTH(' . $sort . ')', $order);
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    public function create($data)
    {
        $response = BasicRestController::create($data);
        $this->networkSync($response);

        return $response;
    }

    public function update($id, $data)
    {
        $response = BasicRestController::update($id, $data);
        $this->networkSync($response);

        return $response;
    }

    public function networkSync($response)
    {
        $currentClient = $this->environment()->getClient();
        if ($currentClient->hasNetwork() && $currentClient->isMaster() && $response->getVariable('success') === true) {
            $object = $response->getVariable('object');

            $group    = $object['group']['id'];
            $keywords = $this->getEntityManager()->getRepository($this->repository)->findByGroup($group);

            $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
            foreach ($currentClient->getNetwork()->getClients() as $client) {
                if (!$client->isMaster()) {
                    $clientEM = $entityManagerFactory->getEntityManager($client);
                    $this->setEntityManager($clientEM);

                     $cGroup = $clientEM->getRepository('Keyword\Entity\Group')->findOneByMaster($group);
                    if ($cGroup) {
                        foreach ($keywords as $keyword) {
                            $data = [
                                'name'    => $keyword->getName(),
                                'order'   => $keyword->getOrder(),
                                'master'  => $keyword->getId(),
                                'group'   => [
                                    'id' => $cGroup->getId() // On change l'association pour le groupe client
                                ],
                                'parents' => [],
                            ];

                            if ($keyword->getParents()->count() > 0) {
                                foreach ($keyword->getParents() as $parent) {
                                    $cParent           = $clientEM->getRepository($this->repository)->findOneByMaster($parent->getId());
                                    $data['parents'][] = [
                                        'id' => $cParent->getId() // On remet l'association pour le parent client
                                    ];
                                }


                            }

                            $cKeywordExists = $clientEM->getRepository($this->repository)->findOneByMaster($keyword->getId());
                            if ($cKeywordExists) {
                                $data['id'] = $cKeywordExists->getId();
                                BasicRestController::update($cKeywordExists->getId(), $data);
                            } else {
                                BasicRestController::create($data);
                            }
                        }
                    } // Sinon il y a une erreur quelque part là, le groupe ne devrait pas être supprimé chez les clients
                }
            }

            $this->setEntityManager($this->entityManager());
        }
    }

    public function fusionAction()
    {
        $keywordToKeep_id   = $this->params()->fromRoute('id');
        $keywordToFusion_id = $this->params()->fromPost('keywordToFusion_id', null);
        $type               = $this->params()->fromPost('type', null);

        if (!$keywordToFusion_id ||
            $keywordToKeep_id == $keywordToFusion_id ||
            !$this->acl()->isAllowed('keyword:e:keyword:d') ||
            !in_array($type, ['duplicate', 'transfer'])
        ) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Keyword $keywordToKeep */
        $keywordToKeep = $this->getEntityManager()
            ->getRepository('Keyword\Entity\Keyword')
            ->find($keywordToKeep_id);

        if (!$keywordToKeep) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Association[] $associationsToKeep */
        $associationsToKeep = $this->getEntityManager()
            ->getRepository('Keyword\Entity\Association')
            ->findBy(['keyword' => $keywordToKeep]);

        /** @var Association[] $associationsToChange */
        $associationsToChange = $this->getEntityManager()
            ->getRepository('Keyword\Entity\Association')
            ->findBy(['keyword' => $keywordToFusion_id]);

        foreach ($associationsToChange as $association) {
            foreach ($associationsToKeep as $associationToKeep) {
                if (
                    $associationToKeep->getEntity() === $association->getEntity() &&
                    $associationToKeep->getPrimary() === $association->getPrimary()
                ) {
                    if ($type === 'transfer') {
                        $this->getEntityManager()->remove($association);
                    }
                    continue 2;
                }
            }

            if ($type === 'transfer') {
                $association->setKeyword($keywordToKeep);
                $this->getEntityManager()->persist($association);
            } else {
                $newAssociation = new Association();
                $newAssociation->setEntity($association->getEntity());
                $newAssociation->setPrimary($association->getPrimary());
                $newAssociation->setKeyword($keywordToKeep);
                $this->getEntityManager()->persist($newAssociation);
            }
        }

        $this->getEntityManager()->flush();

        return new JsonModel([
            'success' => true,
        ]);
    }
}
