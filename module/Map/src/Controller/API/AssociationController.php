<?php

namespace Map\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Laminas\Mvc\MvcEvent;

class AssociationController extends BasicRestController
{
    protected $entityClass  = 'Map\Entity\Association';
    protected $repository   = 'Map\Entity\Association';
    protected $entityChain = '';
    protected $isGeneric = true;

    public function onDispatch(MvcEvent $event)
    {
        $this->entityChain = $this->params()->fromQuery('right', null);
        if ($this->entityChain === null) {
            $request = $event->getRequest();
            $data = $this->processBodyContent($request);
            if (!isset($data['right'])) {
                return $this->notFoundAction();
            }
            $this->entityChain = $data['right'];
        }
        return parent::onDispatch($event);
    }

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
    }
}
