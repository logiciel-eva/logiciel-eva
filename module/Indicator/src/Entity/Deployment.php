<?php

namespace Indicator\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Keyword\Entity\Group;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Deployment
 *
 * @package Indicator\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="indicator_deployment")
 */
class Deployment extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Indicator
     *
     * @ORM\ManyToOne(targetEntity="Indicator\Entity\Indicator")
     */
    protected $indicator;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="Keyword\Entity\Group")
     */
    protected $group;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $level;

    public function __construct()
    {

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Indicator
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * @param Indicator $indicator
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'indicator',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $indicator = $entityManager->getRepository('Indicator\Entity\Indicator')->find($value);

                                return $indicator !== null;
                            },
                            'message' => 'deployment_field_indicator_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'group',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $group = $entityManager->getRepository('Keyword\Entity\Group')->find($value);

                                return $group !== null;
                            },
                            'message' => 'deployment_field_group_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'       => 'level',
                'required'   => true,
                'filters'    => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                ],
            ],
        ]);

        return $inputFilter;
    }

    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return true;
    }

    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        
    }
}
