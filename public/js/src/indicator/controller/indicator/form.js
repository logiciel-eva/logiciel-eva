(function(app) {

    app.controller('indicatorFormController', [
        'indicatorService', 'flashMessenger', 'translator',
        function indicatorFormController(indicatorService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'uom',
                    'description',
                    'definition',
                    'method',
                    'interpretation',
                    'type',
                    'operator',
                    'values',
                    'keywords',
                    'archived'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.indicator = {
                values: [],
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    indicatorService.find(id, apiParams).then(function (res) {
                        self.indicator = res.object;
                        self.panelTitle = self.indicator.name;
                    });
                }
            };

            self.saveIndicator = function saveIndicator() {
                self.errors.fields = {};
                self.loading.save = true;
                self.indicator.archived = self.indicator.archived == true ? 1 : 0;

                var isCreation = !self.indicator.id;

                indicatorService.save(self.indicator, apiParams).then(function (res) {
                    self.indicator = res.object;
                    self.panelTitle = self.indicator.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.indicator.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('indicator_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteIndicator = function deleteIndicator() {
                if (confirm(translator('indicator_question_delete').replace('%1', self.indicator.name))) {
                    indicatorService.remove(self.indicator).then(function (res) {
                        if (res.success) {
                            window.location = '/indicator';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.addValue = function addValue() {
                self.indicator.values.push({
                    value: null,
                    text: null,
                    color: null
                });
            };

            self.removeValue = function removeValue(value) {
                self.indicator.values.splice(self.indicator.values.indexOf(value), 1);
            };
        }
    ]);

})(window.EVA.app);

$(function () {
    $('[data-toggle="popover"]').popover()
})