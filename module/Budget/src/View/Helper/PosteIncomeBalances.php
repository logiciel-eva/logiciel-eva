<?php namespace Budget\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class PosteIncomeBalances extends AbstractHelper
{
    protected $balances;

    public function __construct($balances)
    {
        $this->balances = $balances;
    }

    public function __invoke()
    {
        return $this->balances;
    }
}
