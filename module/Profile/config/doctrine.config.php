<?php

namespace Profile;

return [
    'doctrine' => [
        'driver' => [
            'profile_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Profile\Entity' => 'profile_entities'
                ]
            ]
        ]
    ]
];
