(function(app) {

    app.controller('exportListController', [
        'exportService', '$scope', 'flashMessenger', 'translator',
        function exportListController(exportService, $scope, flashMessenger, translator) {
            var self = this;

            self.type = null;

            self.exports = [];
            self.options = {
                name: 'exports-list',
                col: [
                    'id',
                    'name',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    if (self.type) {
                        params.search.data.filters.type = {
                            op: 'eq',
                            val: self.type
                        };
                    }

                    exportService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.init = function (type) {
                self.type = type;
            };

            self.filterType = function (type) {
                self.type = type;
                $scope.__tb.apiParams.search.data.filters.type.val = type;
                $scope.__tb.loadData();
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(_export) {
                self.editedLineErrors = {};
                self.editedLine       = _export;
                self.editedLineFields = angular.copy(_export);
            };

            self.saveEditedLine = function saveEditedLine(_export) {
                self.editedLineErrors = {};
                exportService.save(angular.extend({}, _export, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        _export = angular.extend(_export, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('export_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteExport = function deleteExport(_export) {
                if (confirm(translator('export_question_delete').replace('%1', _export.name))) {
                    exportService.remove(_export).then(function(res) {
                        if (res.success) {
                            self.exports.splice(self.exports.indexOf(_export), 1);
                            flashMessenger.success(translator('export_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
