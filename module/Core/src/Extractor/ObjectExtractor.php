<?php

namespace Core\Extractor;

use Core\Export\IExporter;
use Core\View\Helper\Translate;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Keyword\Entity\Keyword;
use Laminas\Filter\StripTags;
use Laminas\Mvc\I18n\Translator;

class ObjectExtractor
{
    /**
     * Exporter
     * @var string
     */
    protected $exporter;

    /**
     * Object
     * @var mixed
     */
    protected $object;

    /**
     * Translator
     * @var Translator
     * @ORM\
     */
    protected $translator;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct()
    {
        $this->exporter = [
            'exporter' => null,
            'config'   => [],
            'aliases'  => [],
        ];
    }

    public function setExporter($class)
    {
        $this->exporter['exporter'] = $class;

        if (class_exists($class) && class_implements($class, IExporter::class)) {
            $this->exporter['config'] = call_user_func([$class, 'getConfig']);
            $this->exporter['aliases'] = call_user_func([$class, 'getAliases']);
        }
    }

    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @param Translate $translator
     */
    public function setTranslator(Translate $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Use the format function of the config.
     *
     * @param $value
     * @param $config
     * @return null|string
     */
    public function format($value, $config, $variableStr)
    {
        if (isset($config['format'])) {
            $format = $config['format'];
            if ($format instanceof \Closure) {
                $params = [$value, $this->object, $this->translator];
                if (isset($config['property']) && 'members.id' === $config['property']) {
                    $members = $this->object->getMembers();
                    $keywordsByMemberId = [];
                    foreach ($members as $member) {
                        $keywordsByMemberId[$member->getId()] = $this->getKeywords($member, 'Project\Entity\Member', 'member');
                    }
                    $params[] = $keywordsByMemberId;
                }
                if (isset($config['property']) && 'actors.structure.name' === $config['property']) {
                    $actors = $this->object->getActors();
                    $keywordsByActorsId = [];
                    foreach ($actors as $actor) {
                        $keywordsByActorsId[$actor->getId()] = $this->getKeywords($actor, 'Project\Entity\Actor', 'actor');
                    }
                    $params[] = $keywordsByActorsId;
                    $params[] = $variableStr;
                }

                if (isset($config['property']) && 'conventions.id' === $config['property']) {
                    $conventions = $this->object->getConventions();
                    $keywordsByConventionId = [];
                    foreach ($conventions as $convention) {
                        $keywordsByConventionId[$convention->getConvention()->getId()] = $this->getKeywords($convention->getConvention(), 'Convention\Entity\Convention', 'convention');
                    }
                    $params[] = $keywordsByConventionId;
                    $params[] = $variableStr;
                }
                return call_user_func_array($format, $params);
            } else {
                switch ($format)
                {
                    case 'integer':
                        if (is_int($value) || is_numeric($value)) {
                            return intval($value);
                        }
                        break;
                    case 'decimal':
                        if (is_double($value) || is_numeric($value)) {
                            return doubleval($value);
                        }
                        break;
                    case 'datetime':
                        if ($value instanceof \DateTime) {
                            return $value->format('Y-m-d H:i:s');
                        }
                        break;
                    case 'date':
                        if ($value instanceof \DateTime) {
                            return $value->format('Y-m-d');
                        }
                        break;
                    case 'time':
                        if ($value instanceof \DateTime) {
                            return $value->format('H:i:s');
                        }
                        break;
                    case 'string':
                        if (is_string($value)) {
                            return $value;
                        }
                        break;
                }
            }
        }
        return $value;
    }

    /**
     * Extract the data of a column in the object.
     *
     * @param $col
     * @param null $default
     * @param string
     * @return mixed|null
     */
    public function extract($col, $default = null, $separator = ', ', $variableStr = [])
    {
        $stripTags = new StripTags();
        $result = $default;

        $config = $this->exporter['config'];
        $aliases = $this->exporter['aliases'];

        $configRegexes = [];
        foreach ($config as $key => $value) {
            if (strpos($key, '/') === 0) {
                $configRegexes[$key] = $value;
            }
        }
        try {
            $arr = [];
            $val = $default;

            if (isset($aliases[$col]) && isset($config[$aliases[$col]])) {
                $localConfig = $config[$aliases[$col]];

                if (isset($localConfig['property'])) {
                    $val = $this->extractInternalToString($this->object, $localConfig['property'], $arr, $separator);
                } else {
                    $val = $this->extractInternalToString($this->object, $aliases[$col], $arr, $separator);
                }

                $val = $this->format($val, $localConfig, $variableStr);
            } elseif (isset($aliases[$col])) {
                $val = $this->extractInternalToString($this->object, $aliases[$col], $arr, $separator);
            } elseif (isset($config[$col])) {
                $localConfig = $config[$col];

                if (isset($localConfig['property'])) {
                    $val = $this->extractInternalToString($this->object, $localConfig['property'], $arr, $separator);
                } else {
                    $val = $this->extractInternalToString($this->object, $col, $arr, $separator);
                }

                $val = $this->format($val, $localConfig, $variableStr);
            } else {
                $val = $this->extractInternalToString($this->object, $col, $arr, $separator);
            }
            foreach ($configRegexes as $regex => $configRegex) {
                if (preg_match($regex, $col)) {
                    if (isset($configRegex['property'])) {
                        $val = $this->extractInternalToString($this->object, $configRegex['property'], $arr, $separator);
                    } else {
                        $val = $this->extractInternalToString($this->object, $col, $arr, $separator);
                    }

                    $val = $this->format($val, $configRegex, $variableStr);
                    break;
                }
            }

            if (preg_match('/customField\d+$/', $col)) {
                $result = $stripTags->filter(html_entity_decode($val));
            } else {
                $result = $val;
            }
        } catch (\Exception $e) {
            $result = $e->getMessage();
        }
        $result = trim(str_replace(', ,', '', $result));

        return $result;
    }

    /**
     * @param $object
     * @param $column
     * @param $array
     * @param string
     * @return mixed
     */
    public function extractInternalToString($object, $column, &$array, $separator)
    {
        $obj = $this->extractInternal($object, $column, $array);

        if (is_array($obj)) {
            $obj = implode($separator, $this->arrayToFlat($obj, []));
        }

        return $obj;
    }

    public function arrayToFlat($obj, $array)
    {
        if (is_array($obj) || $obj instanceof Collection) {
            foreach ($obj as $o) {
                $array = $this->arrayToFlat($o, $array);
            }
        } else {
            $array[] = $obj;
        }

        return $array;
    }

    /**
     * The internal extractor.
     *
     * @param mixed $object
     * @param string $column
     * @param array $array
     * @return mixed
     */
    public function extractInternal($object, $column, &$array)
    {
        $chain = explode('.', $column);

        $getter = 'get' . ucfirst($chain[0]);
        $property = $chain[0];
        if ($object !== null) {
			if ($object instanceof Collection) {
				$i = 0;
				$idGroup = null;
				foreach ($object as $item) {
                    // For keywords hierarchySup
					if ($item instanceof Keyword && (strpos($property, 'hierarchySup') !== false) && (strpos($property, strval($item->getGroup()->getId())) !== false)){
                        $hierarchies = [];
                        $hierarchies[0] = ['name'=> $item->getName()];
						foreach($item->getHierarchySup() as $key => $hierarchy){
							$hierarchies[$key + 1] = ['name' => $hierarchy->getName()];
						}
						$array[$idGroup][$i][$property] = $hierarchies;
					} else {
						if (!isset($array[$i])) {
							$array[$i] = [];
						}
						$this->extractInternal($item, implode('.', $chain), $array[$i]);
					}
					$i++;
				}
				return $array;
			}

            if (sizeOf($chain) > 1) { // It's not finished, recall extractInternal with the value as $object
                array_shift($chain);
                $this->extractInternal($object->$getter($this->entityManager), implode('.', $chain), $array[$property]);
            } else {
                if ($property === 'territories') {
                    $array[$property] = $this->getTerritories($object, str_replace('DoctrineORMModule\Proxy\__CG__\\', '', get_class($object)));
                } elseif (preg_match('/keyword[0-9]+/' ,$property)) {
                    $array[$property] = $this->getKeywords($object, str_replace('DoctrineORMModule\Proxy\__CG__\\', '', get_class($object)), str_replace('keyword', '', $property));
                } elseif (preg_match('/customField[0-9]+/' ,$property)) {
                    $array[$property] = $this->getCustomField($object, str_replace('DoctrineORMModule\Proxy\__CG__\\', '', get_class($object)), str_replace('customField', '', $property));
                } elseif (preg_match('/indicator[0-9]+/' ,$property)) {
                    $array[$property] = $this->getIndicator($object, str_replace('DoctrineORMModule\Proxy\__CG__\\', '', get_class($object)), str_replace('indicator', '', $property));
                } else {
                    $value = $object->$getter();
                    $array[$property] = $value instanceof \DateTime ? $value->format('d/m/Y H:i') : $value;
                }
            }
        }

        return $array;
    }

    /**
     * Get the array of keywords associated with a record grouped by Group.
     *
     * @param mixed $object
     * @return array
     */
    public function getKeywords($object, $entityClass, $group): array
    {

        $keywords = [];
        $queryBuilder = $this->entityManager->getRepository('Keyword\Entity\Association')->createQueryBuilder('a');
        if (is_numeric($group)) {
            $queryBuilder->select('a')
                ->join('a.keyword', 'k')
                ->join('k.group', 'g')
                ->where(
                    'a.entity = :entity AND a.primary = :primary AND g.id = :group'
                )
                ->setParameters([
                    'entity' => $entityClass,
                    'primary' => $object->getId(),
                    'group' => $group,
                ]);
        } else {
            $queryBuilder->select('a')
                ->join('a.keyword', 'k')
                ->join('k.group', 'g')
                ->where(
                    'a.entity LIKE :entity AND a.primary = :primary'
                )
                ->setParameters([
                    'entity' => explode('\\', $entityClass)[0] . "%",
                    'primary' => $object->getId(),
                ]);
        }
        $associations = $queryBuilder->getQuery()->getResult();
        foreach ($associations as $association) {
            $keyword = $association->getKeyword();
            $keywords[] = $keyword->getName();
        }

        return $keywords;
    }

    public function getIndicator($object, $entityClass, $indicator)
    {
        if ($entityClass === 'Project\Entity\Project') {
            $measures = $object->getMeasuresT();
            if (isset($measures[$indicator])) {
                return $measures[$indicator]['text'];
            }
        }

        return null;
    }

    public function getCustomField($object, $entityClass, $field)
    {
        $queryBuilder = $this->entityManager->getRepository('Field\Entity\Value')->createQueryBuilder('v');
        $queryBuilder->select('v')
            ->where(
                'v.entity = :entity AND v.primary = :primary AND v.field = :field'
            )
            ->setParameters([
                'entity'  => $entityClass,
                'primary' => $object->getId(),
                'field'   => $field,
            ]);

        try {
            $value = $queryBuilder->getQuery()->getOneOrNullResult();
        } catch (\Exception $e) {
            $value = null;
        }

        if ($value) {
            return $value->getValue();
        }

        return '';
    }

    /**
     * Get the array of territories associated with a record
     *
     * @param mixed $object
     * @return array
     */
    public function getTerritories($object, $entityClass)
    {
        $territories = [];

        $associations = $this->entityManager->getRepository('Map\Entity\Association')->findBy([
            'entity'  => $entityClass,
            'primary' => $object->getId(),
        ]);

        foreach ($associations as $association) {
            $territory = $association->getTerritory();
            $territories[] = $territory->getName();
        }

        return $territories;
    }

}
