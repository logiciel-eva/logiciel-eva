(function(app) {

    app.directive('fieldErrors', [
        function fieldErrors() {
            return {
                restrict: 'E',
                scope: {
                    errors: '=errors',
                    field: '@field'
                },
                replace: false,
                transclude: true,
                template: '<div class="errors-group" ng-if="errors[field]"><ul><li ng-repeat="(key, error) in errors[field]" ng-if="key!=\'__translate_field\'" >{{error}}</li></ul></div>'            }
        }
    ])

})(window.EVA.app);
