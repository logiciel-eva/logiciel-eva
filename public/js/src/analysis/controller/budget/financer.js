(function (app) {

    app.controller('financerController', [
        '$http', 'queryService', '$rootScope',
        function ($http, queryService, $rootScope) {
            var self = this;

            self.projects  = [];
            self.financers = [];
            self.nbFilters = 1;

            self.filters = {
                project  : {},
                structure: {},
                group    : 'financer'
            };

            self.loading = false;

            self.getPartialTotal = function (client, identifier, type) {
                var total = 0;

                for (var i in self.projects) {
                    var project = self.projects[i];
                    if (project._client.id == client && typeof project.data[identifier] !== 'undefined') {
                        total += project.data[identifier][type];
                    }
                }

                return total;
            };

            self.applyFilters = function () {
                self.loading = true;
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.structure).length;

                $http({
                    url   : '/analysis/budget/financer?' + $.param({filters: self.filters}),
                    method: 'GET',
                }).then(function (res) {
                    self.projects  = res.data.projects;
                    self.financers = res.data.financers;

                    self.loading = false;
                });
            };

            self.resetFilters = function () {
                self.filters.project   = {};
                self.filters.structure = {};
                self.applyFilters();
            };

            var cache        = {};
            self.columnEmpty = function (financer, type) {
                var key = financer._client.id + '_' + financer.id;

                if (typeof cache[key] == 'undefined') {
                    cache[key] = {};
                }

                if (typeof cache[key][type] == 'undefined') {
                    cache[key][type] = true;
                } else {
                    return cache[key][type];
                }

                for (var i in self.projects) {
                    if (typeof  self.projects[i].data[key] !== 'undefined') {
                        if (self.projects[i].data[key][type] !== 0) {
                            cache[key][type] = false;
                        }
                    }
                }

                return cache[financer._client.id + '_' + financer.id][type];
            };

            self.getColspan = function (type) {
                var colspan = self.financers.length;
                for (var i in self.financers) {
                    if (self.columnEmpty(self.financers[i], type)) {
                        colspan--;
                    }
                }
                return colspan;
            };

            self.getProjectsNumber = function () {
                var ret = 0;

                for (var i in self.projects) {
                    if (self.isMaster) {
                        for (var j in self.projects[i]) {
                            ret++;
                        }
                    } else {
                        ret++;
                    }
                }

                return ret;
            };

            self.export = function () {
                window.open('/analysis/budget/financer?' + $.param({filters: self.filters, export: true}));
            };

            // QUERIES
            self.queries          = [];
            self.query            = {
                list: 'analysis-budget-financer'
            };
            self.errors           = {
                fields: {}
            };
            self.loadingQueries   = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries        = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                if (angular.isArray(self.filters.project)) {
                    self.filters.project = {};
                }
                if (angular.isArray(self.filters.structure)) {
                    self.filters.structure = {};
                }

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };
        }
    ]);

})(window.EVA.app);
