<?php

namespace Convention\Export;

use Core\Export\IExporter;

abstract class Convention implements IExporter
{
    public static function getConfig()
    {
        return [
            'contractor.name' => [
                'name' => 'contractor',
            ],
            'partTotalProjects' => [
                'name' => 'part_amount_projects'
            ],
            'timeSpent' => [
                'name' => 'time_spent'
            ],
        ];
    }

    public static function getAliases()
    {
        return [
            'members'                  => 'members.name',
            'project.id'               => 'lines.project.id',
            'project.name'             => 'lines.project.name',
        ];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
