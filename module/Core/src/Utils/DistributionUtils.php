<?php

namespace Core\Utils;

/**
 * Class DistributionUtils
 *
 * @package Core\Utils
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class DistributionUtils
{
    const DISTRIBUTION_CONSTANT_AMPLITUDE          = 'constant_amplitude';
    const DISTRIBUTION_NESTED_AVERAGE              = 'nested_average';
    const DISTRIBUTION_ARITHMETICAL_PROGRESSION    = 'arithmetical_progression';
    const DISTRIBUTION_GEOMETRICAL_PROGRESSION     = 'geometrical_progression';
    const DISTRIBUTION_QUANTILES                   = 'quantiles';
    const DISTRIBUTION_STANDARDIZED_DISCRETIZATION = 'standardized_discretization';

    public static function distribute($data, $method, $steps)
    {
        sort($data);

        foreach ($data as $i => $v) {
           if ($v === null) {
               $data[$i] = 0;
           }
        }

        $min   = min($data);
        $max   = max($data);
        $count = sizeOf($data);

        $distribution = [];

        switch ($method) {
            case self::DISTRIBUTION_CONSTANT_AMPLITUDE:
                $amplitude = ($max - $min) / $steps;

                $class = $min;
                for ($i = 0; $i <= $steps; $i++) {
                    $distribution[] = $class;
                    $class = $class + $amplitude;
                }
                break;
            case self::DISTRIBUTION_NESTED_AVERAGE:
                $distribution = self::nestedAverage($data, $steps);
                break;
            case self::DISTRIBUTION_ARITHMETICAL_PROGRESSION:
                $expand = $max - $min;
                $div    = 0;
                for ($i = 1; $i <= $steps; $i++) {
                    $div += $i;
                }

                $progression = $expand / $div;
                $class = $min;
                for ($i = 2; $i <= $steps + 2; $i++) {
                    $distribution[] = $class;
                    $class = $class + ($i * $progression);
                }
                break;
            case self::DISTRIBUTION_GEOMETRICAL_PROGRESSION:
                $progression = ( log10($max) - log10(($min == 0 ? 1 : $min)) ) / $steps;
                $progression = pow(10, $progression);

                $class = $min;
                for ($i = 0; $i <= $steps; $i++) {
                    $distribution[] = $class;
                    $class = ($class == 0 ? 1 : $class) * $progression;
                }
                break;
            case self::DISTRIBUTION_QUANTILES:
                $n = round($count / $steps);
                $i = $j = 0;
                $distribution[] = 0;
                foreach ($data as $value) {
                    $i++;
                    $j++;
                    if($i == $n) {
                        $distribution[] = $j;
                        $i = 0;
                    }
                }
                if ($count % $steps != 0) {
                    $distribution[] = $count;
                }
                break;
            case self::DISTRIBUTION_STANDARDIZED_DISCRETIZATION:
                $tmp = 0;
                foreach($data as $value) {
                    $tmp += ($value * $value);
                }

                $avg    = array_sum($data) / $count;
                $avgTmp = $tmp / $count;

                $ecartType = sqrt($avgTmp - ($avg * $avg));

                for($i = - ($steps / 2) - 1; $i < ($steps / 2); $i++) {
                    $distribution[] = $avg + ($i * $ecartType);
                }

                break;
        }

        return $distribution;
    }

    private static function nestedAverage($data, $steps, $index = 0)
    {
        sort($data);

        $avg  = array_sum($data) / sizeOf($data);
        $left = $result = $right = [];

        foreach ($data as $value) {
            if ($value < $avg) {
                $left[] = $value ;
            } else {
                $right[] = $value;
            }
        }

        $index += 2;
        if($index >= $steps) {
            $result = array_merge([$avg]);
        } else {
            $result = array_merge(
                [$avg],
                self::nestedAverage($left, $steps, $index + 2),
                self::nestedAverage($right, $steps, $index + 2)
            );
        }

        if($index == 2) {
            $result = array_merge($result, [max($data), min($data)]);
        }

        sort($result);

        return $result;
    }

    public static function getDistributionsMethod()
    {
        return [
            self::DISTRIBUTION_CONSTANT_AMPLITUDE,
            self::DISTRIBUTION_NESTED_AVERAGE,
            self::DISTRIBUTION_ARITHMETICAL_PROGRESSION,
            self::DISTRIBUTION_GEOMETRICAL_PROGRESSION,
            self::DISTRIBUTION_QUANTILES,
            self::DISTRIBUTION_STANDARDIZED_DISCRETIZATION,
        ];
    }
}
