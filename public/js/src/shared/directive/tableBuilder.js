(function (app) {

    app.directive('tableBuilder', [
        '$compile', '$q', '$timeout', 'userConfigurationService', 'queryService', 
        function tableBuilder($compile, $q, $timeout, userConfigurationService, queryService) {
            return {
                restrict: 'EA',
                scope: {
                    options: '=options',
                    items: '=items',
                    pp: '=pp'
                },
                controller: function ($scope, $element) {
                    var self = this;
                    self.isDuplicateChecked = false;
                    self.duplicateColumns = [];

                    self.columns = {};
                    self.options = $scope.options;
                    self.selection = false;
                    self.items = [];
                    self.itemsRemoved = [];
                    self.pp = $scope.pp;

                    if (typeof $scope.options.name !== 'string' || $scope.options.name === '') {
                        throw 'Please, define a name in the table-builder options.';
                    }

                    if (typeof $scope.options.getItems !== 'function') {
                        throw 'Please, define the getItems(params, callback) function in the table-builder options.';
                    }

                    var deferredAbort = $q.defer();
                    self.loadData = function loadData() {
                        $scope.__tb.loading = true;
                        deferredAbort.resolve();
                        deferredAbort = $q.defer();

                        if ($scope.options.pager === true && (typeof self.apiParams.search.data.page === 'undefined' || typeof self.apiParams.search.data.limit === 'undefined')) {
                            self.apiParams.search.data.page = 0;
                            self.apiParams.search.data.limit = 20;
                        } else if ($scope.options.pager === false) {
                            delete self.apiParams.search.data.page;
                            delete self.apiParams.search.data.limit;
                        }

                        if (self.duplicateColumns.length > 0) {
                            self.apiParams.duplicate = self.duplicateColumns;
                        } else {
                            delete self.apiParams.duplicate;
                        }


                        $scope.options.getItems(self.apiParams, function (res) {
                            self.itemsRemoved = [];
                            $scope.items = res.rows;
                            self.items = $scope.items;
                            $scope.__tb.total = res.total;
                            $scope.__tb.loading = false;
                            if ($scope.options.pager == true || $scope.pp) {
                                $scope.__tb.pager();
                            }

                        }, deferredAbort);
                    };

                    $scope.$watch(function () {
                        return $scope.__tb.selection;
                    }, function () {
                        for (var i in $scope.items) {
                            $scope.items[i].selected = $scope.__tb.selection;
                        }
                    });

                    self.removeFromList = function () {
                        var i = $scope.items.length;
                        while (i--) {
                            if ($scope.items[i].selected) {
                                self.itemsRemoved.push($scope.items[i].id);
                                $scope.items.splice(i, 1);
                            }
                        }
                    };

                    self.isAnItemSelected = function () {
                        var ret = false;

                        for (var i in self.items) {
                            if (self.items[i].selected) {
                                ret = true;
                                break;
                            }
                        }

                        return ret;
                    };

                    self.sort = function sort(column) {
                        self.apiParams.search.data.sort = column.name;

                        var order = self.apiParams.search.data.order;
                        if (self.apiParams.search.data.sort !== column.name) {
                            order = 'asc';
                        } else {
                            order = order == 'asc' ? 'desc' : 'asc';
                        }

                        self.apiParams.search.data.order = order;

                        self.loadData();
                        self.saveTableConfiguration();
                    };

                    self.resetFilters = function resetFilters() {
                        self.apiParams.search.data.filters = {};
                    };

                    self.exportExcel = function exportExcel() {
                        //$scope.__tb.loading = true;
                        deferredAbort.resolve();
                        deferredAbort = $q.defer();

                        var params = angular.copy(self.apiParams);
                        params.excel = true;
                        params.colsExport = [];
                        params.exportName = $scope.options.name;
                        delete params.search.data.page;
                        delete params.search.data.limit;

                        var itemsSelected = [];
                        for (var i in self.items) {
                            if (self.items[i].selected) {
                                itemsSelected.push(self.items[i].id);
                            }
                        }

                        if (itemsSelected.length > 0) {
                            params.itemsSelected = itemsSelected;
                        }

                        if (self.itemsRemoved.length > 0) {
                            params.itemsRemoved = self.itemsRemoved;
                        }

                        for (var i in $scope.__tb.columns) {
                            var col = $scope.__tb.columns[i];
                            if (col.exportable === false) {
                                continue;
                            }
                            if (col.visible && i != 'selection' && i != 'actions' && i != 'tree') {
                                var columnName = i.replace(/^computed\./, '');    // to remove the specific chain added for financial tables sorting purposes
                                params.colsExport.push(columnName);
                            }
                        }

                        $scope.options.getItems(params, function (res) {
                        }, deferredAbort);
                    };

                    self.saveTableConfiguration = function saveTableConfiguration() {
                        $element.find('[table-builder-table]').trigger('DOMSubtreeModified');
                        var cols = {
                            __sort: {
                                sort: self.apiParams.search.data.sort,
                                order: self.apiParams.search.data.order
                            }
                        };
                        for (var i in self.columns) {
                            var column = self.columns[i];
                            cols[column.name] = column.visible;
                        }

                        userConfigurationService.saveTable(self.options.name, cols).then(function (res) {
                        });
                    };

                    self.apiParams = {
                        col: $scope.options.col,
                        search: {
                            type: $scope.options.searchType || null,
                            data: {
                                filters: {},
                                sort: $scope.options.sort || null,
                                order: $scope.options.order || null
                            }
                        }
                    };

                    self.selectAll = function () {
                        for (var i in self.items) {
                            self.items[i].selected = !self.items[i].selected;
                        }
                    };

                    self.itemsSelected = function () {
                        var itemsSelected = [];

                        for (var i in self.items) {
                            if (self.items[i].selected) {
                                itemsSelected.push(self.items[i].id);
                            }
                        }

                        return itemsSelected;
                    };

                    $scope.__tb = {
                        loadData: self.loadData,
                        exportExcel: self.exportExcel,
                        columns: self.columns,
                        apiParams: self.apiParams,
                        sort: self.sort,
                        isDuplicateChecked: false,
                        resetFilters: self.resetFilters,
                        saveTableConfiguration: self.saveTableConfiguration,
                        selection: self.selection,
                        items: self.items,
                        removeFromList: self.removeFromList,
                        isAnItemSelected: self.isAnItemSelected,
                        pp: self.pp,
                        selectAll: self.selectAll,
                        itemsRemoved: self.itemsRemoved,
                        itemsSelected: self.itemsSelected
                    };

                    if ($scope.options.pager == true || $scope.pp) {
                        $element.find('[table-builder-table]').after($compile('<table-builder-pager></table-builder-pager>')($scope));
                    }
                    $element.find('[table-builder-table] tbody').prepend($compile('<tr class="loading"><td ng-show="__tb.loading" colspan=42 class="text-center"><i class="fa fa-spin fa-circle-o-notch"></i></td></tr>')($scope));

                    $timeout(function () {
                        userConfigurationService.getConfigurationWithTable(self.options.name).then(function (res) {
                            if (!res.resizableTable) {
                                $element.find('[table-builder-table]').addClass('table-no-resize');
                            }
                            for (var i in res.table) {
                                if (i !== '__sort' && typeof self.columns[i] !== 'undefined') {
                                    self.columns[i].visible = res.table[i] === 'true' || res.table[i] === true;
                                } else {
                                    self.apiParams.search.data.sort = res.table[i].sort;
                                    self.apiParams.search.data.order = res.table[i].order;
                                }
                            }

                            var autoload = true;
                            if (typeof $scope.options.autoload === 'boolean') {
                                autoload = $scope.options.autoload;
                            }

                            if (autoload) {
                                self.loadData();
                            }
                        });


                    });
                },
                link: function (scope, $element) {
                    scope.$parent.__tb = scope.__tb;
                    // FIXED HEADER --->
                    var $window = $('.content-scroller');
                    var $header = $('.header-container');
                    var $table = $element.find('[table-builder-table]');

                    if (!$table.hasClass('no-fixed-header')) {
                        var $thead = $table.find('thead');
                        var headerHeight = $header.height();
                        if ($('#__theme').text() === 'parc') {
                            headerHeight += $('.nav-container').height();
                        }

                        var $clone = $thead.clone();
                        $clone.insertAfter($thead);

                        $clone.css({
                            position: 'absolute',
                            display: 'none'
                        });

                        function resizeClone() {
                            $clone.find('th').each(function (index, th) {
                                var $th = $(th);
                                var width = $thead.find('th:eq(' + index + ')').width();
                                $th.width(width);
                            });
                            $clone.width($table.width());

                            $clone.css({
                                top: ($table.offset().top - headerHeight) * -1
                            });
                        }

                        function virtualScroller() {
                            var bottomContainer = $window.height() + $window.offset().top;
                            var bottomTable = $table.height() + $table.offset().top;

                            if (bottomTable > bottomContainer && $table.offset().top < $window.height()) {
                                var $leScroll = $virtualScroller.find('div');
                                $virtualScroller.css({
                                    maxWidth: $scroller.width()
                                });
                                $leScroll.width($table.width());
                                $virtualScroller.css('display', 'block');
                            } else {
                                $virtualScroller.css('display', 'none');
                            }
                        }

                        $window.on('scroll', function () {
                            var offsetTop = $table.offset().top + $window.scrollTop() - headerHeight;
                            var offsetBottom = $table.height() + offsetTop - $thead.height();
                            if ($window.scrollTop() >= offsetTop && $window.scrollTop() <= offsetBottom) {
                                resizeClone();
                                $clone.css('display', 'block');
                            } else {
                                $clone.css({
                                    display: 'none'
                                });
                            }
                            virtualScroller();
                        });
                    }
                    // <--

                    $compile($element.find('[table-builder-table]'))(scope);
                    $element.find('[table-builder-table]').wrap('<div class="table-scroller"></div>');

                    // FIXED HEADER --->
                    if (!$table.hasClass('no-fixed-header')) {
                        var $scroller = $table.parent('.table-scroller');
                        var $virtualScroller = $('<div class="virtual-scroller"><div></div></div>');
                        $scroller.append($virtualScroller);

                        var timeout;

                        function syncVirtual(e) {
                            clearTimeout(timeout);
                            $scroller.off('scroll');
                            $scroller.scrollLeft($virtualScroller.scrollLeft());

                            timeout = setTimeout(function () {
                                $scroller.on('scroll', syncScroller);
                            }, 100);
                        }

                        function syncScroller(e) {
                            clearTimeout(timeout);
                            $virtualScroller.off('scroll');
                            $virtualScroller.scrollLeft($scroller.scrollLeft());

                            timeout = setTimeout(function () {
                                $virtualScroller.on('scroll', syncVirtual);
                            }, 100);
                        }

                        $virtualScroller.on('scroll', syncVirtual);

                        $scroller.on('mouseover', function () {
                            resizeClone();
                            virtualScroller();
                        }).on('scroll', syncScroller);
                    }
                    // <--
                }
            }
        }
    ]);

    app.directive('tableBuilderColumns', [
        'userConfigurationService', 'queryService', 'guid',
        function tableBuilderColumns(userConfigurationService, queryService, guid) {
            return {
                restrict: 'EA',
                require: '^?table-builder',
                replace: false,
                transclude: false,
                templateUrl: '/js/src/shared/template/table-builder-columns.html',
                compile: function () {
                    return function link($scope, $element, attrs, $controller) {
                        if($scope.__tb) {
                           $scope.__tb.withDuplicate = attrs.withDuplicate;
                        }
                       
                        $scope.isDuplicateChecked = false;
                        $scope.tooltip = attrs.tooltip;
                        $scope.columns = $controller.columns;
                        $scope.name = $controller.options.name + '-' + guid.create();// Date.now();

                        if (attrs.query) {
                            queryService.find(attrs.query, {
                                col: [
                                    'id', 'name', 'filters', 'columns', 'shared', 'sharedService', 'list'
                                ],
                            } ).then(function(res) {
                                if (res.object.columns.length > 0) {
                                    Object.keys($scope.columns).forEach(sCol => {
                                        $scope.columns[sCol].visible = res.object.columns.includes($scope.columns[sCol].name);
                                    })
                                } else {
                                    userConfigurationService.getTable(attrs.list).then(function (res2) {
                                        for (const i in res2) {
                                            if (typeof $scope.columns[i] !== 'undefined') {
                                                $scope.columns[i].visible = res2[i] === 'true' || res2[i] === true;
                                            }
                                        }
                                    });

                                }
                            })
                        }
                        else {
                            userConfigurationService.getTable($controller.options.name).then(function (res) {
                                for (const i in res) {
                                    if (typeof $scope.columns[i] !== 'undefined') {
                                        $scope.columns[i].visible = res[i] === 'true' || res[i] === true;
                                    }
                                }
                            });
                        }


                        $scope.saveTableConfiguration = function saveTableConfiguration() {
                            $element.parents('table-builder').find('[table-builder-table]').trigger('DOMSubtreeModified');

                            var cols = {};
                            for (var i in $scope.columns) {
                                var column = $scope.columns[i];
                                cols[column.name] = column.visible;
                            }

                            userConfigurationService.saveTable($controller.options.name, cols).then(function () {
                            });
                        }
                        $scope.__tb.lookForDuplicateEntry = async function lookForDuplicateEntry() {
                            if ($scope.__tb.isDuplicateChecked) {
                                $controller.duplicateColumns = Object.entries($scope.columns)
                                    .map(([k, v]) => {
                                        if (v.visible) {
                                            return k
                                        }
                                    })
                                    .filter(x => x !== undefined
                                        && x !== 'selection'
                                        && x !== 'actions' && x !== 'id'
                                        && !x.includes('keyword'));
                            } else {
                                $controller.duplicateColumns = [];
                            }
                            $controller.loadData();
                        }

                    }
                }
            }
        }
    ]);

    app.directive('tbColumn', [
        function tbColumn() {
            return {
                require: '^?table-builder',
                scope: {
                    name: '@tbName',
                    sortable: '=tbSortable',
                    togglable: '=tbToggle',
                    exportable: '=tbExportable',
                    hidden: '=tbHidden'
                },
                restrict: 'A',
                compile: function compile($element, $attrs) {
                    $element.attr('ng-show', '__tb.columns[\'' + $attrs.tbName + '\'].visible');
                    $element.parents('table')
                        .find('tbody tr:not(.loading) td:nth-child(' + ($element.index() + 1) + ')')
                        .attr('ng-show', '__tb.columns[\'' + $attrs.tbName + '\'].visible');
                    $element.parents('table')
                        .find('tfoot tr td:nth-child(' + ($element.index() + 1) + ')')
                        .attr('ng-show', '__tb.columns[\'' + $attrs.tbName + '\'].visible');

                    return function link(scope, $element, $attrs, $controller) {
                        var column = {
                            name: scope.name,
                            sortable: scope.sortable,
                            togglable: scope.togglable,
                            exportable: scope.exportable,
                            visible: !scope.hidden,
                            index: $element.index(),
                            content: $element.html()
                        };

                        if (column.sortable === true) {
                            $element.addClass('table-builder-sortable');
                            $element.attr('ng-click', '__tb.sort(__tb.columns[\'' + column.name + '\'])');
                            $element.attr('ng-class', '{' +
                                '"table-builder-sorting-asc" : __tb.apiParams.search.data.sort == "' + column.name + '" && __tb.apiParams.search.data.order == "asc",' +
                                '"table-builder-sorting-desc" : __tb.apiParams.search.data.sort == "' + column.name + '" && __tb.apiParams.search.data.order == "desc",' +
                                '}');
                        }

                        if ($controller) {
                            $controller.columns[column.name] = column;
                        }
                    }
                }
            }
        }
    ]);

    app.directive('tableBuilderPager', [
        '$compile',
        function tableBuilderPager() {
            return {
                restrict: 'E',
                require: '^?table-builder',
                replace: true,
                transclude: false,
                templateUrl: '/js/src/shared/template/table-builder-pager.html',
                compile: function () {
                    return function link($scope) {
                        $scope.__tb.isPagerFront = !!$scope.__tb.pp;
                        $scope.__tb.currentPage = 0;

                        $scope.__tb.pager = function pager() {
                            $scope.pages = [];

                            var limit = $scope.__tb.isPagerFront ? $scope.__tb.pp : $scope.__tb.apiParams.search.data.limit;
                            var total = Math.ceil($scope.__tb.total / limit);
                            var current = $scope.__tb.currentPage + 1;

                            var start = 1;
                            var end = total;

                            if (current > 3) {
                                start = current - 2;
                                end = current + 2;
                            } else {
                                end = current + (5 - current);
                            }

                            if (end > total) {
                                end = total;
                            }

                            if (current > total - 3) {
                                start = total - 4;
                            }

                            if (start < 1) {
                                start = 1;
                            }

                            if (current >= 4 && total > 5) {
                                $scope.pages.push({
                                    text: '<i class="fa fa-arrow-circle-left"></i>',
                                    page: (current - 5 < 0 ? 1 : current - 4)
                                });
                            }

                            for (var i = start; i <= end; i++) {
                                $scope.pages.push({
                                    page: i - 1,
                                    text: i + ""
                                });
                            }

                            if (total > end) {
                                $scope.pages.push({
                                    text: '<i class="fa fa-arrow-circle-right"></i>',
                                    page: (current + 5 > total ? total - 1 : current + 4)
                                });
                            }
                        };

                        $scope.__tb.changePage = function (page) {
                            $scope.__tb.currentPage = page;

                            if ($scope.__tb.isPagerFront) {
                                $scope.__tb.pager();
                            } else {
                                $scope.__tb.apiParams.search.data.page = page;
                                $scope.__tb.loadData();
                            }
                        };
                    }
                }
            }
        }
    ]);

    app.directive('tableBuilderQueries', [
        'queryService', '$timeout', '$http', '$rootScope',
        function tableBuilderQueries(queryService, $timeout, $http, $rootScope) {
            return {
                restrict: 'EA',
                scope: {
                    headers: '=?headers',
                    name: '@?name'
                },
                require: '^?table-builder',
                replace: false,
                transclude: false,
                templateUrl: '/js/src/shared/template/table-builder-queries.html',
                compile: function ($element, attrs) {
                    return function link($scope, $element, attrs, $controller) {
                        $scope.name = attrs.name || $controller.options.name;

                        $scope.loading = true;
                        queryService.findAll({
                            col: [
                                'id', 'name', 'filters', 'columns', 'shared', 'sharedService', 'list'
                            ],
                            search: {
                                data: {
                                    filters: {
                                        list: {
                                            op: 'eq',
                                            val: $scope.name
                                        }
                                    }
                                }
                            }
                        }).then(function (res) {
                            $scope.loading = false;

                            $scope.queries = res.rows;
                            $scope.service = res.service;

                            var matches = window.location.search.match(/q=([0-9]+)/);
                            if (matches !== null) {
                                for (var i in $scope.queries) {
                                    var query = $scope.queries[i];
                                    if (query.id == matches[1]) {
                                        $scope.load(query);

                                        break;
                                    }
                                }
                            }
                        }, function () {
                            $scope.loading = false;
                        });

                        $scope.load = function (query) {
                            if ($scope.headers) {
                                for (var i in $scope.headers) {
                                    if (typeof query.filters[i] !== 'undefined') {
                                        $scope.headers[i] = query.filters[i];
                                    } else {
                                        $scope.headers[i] = null;
                                    }
                                }
                            } else {
                                $scope.$parent.__filters = {};
                                for (var i in query.filters) {
                                    //if (query.filters[i] !== null && (typeof query.filters[i].val !== 'undefined' || query.filters[i].op == 'isNull')) {
                                        $scope.$parent.__filters[i] = true;
                                    //} else {
                                    //    $scope.$parent.__filters[i] = false;
                                    //}
                                }

                                $timeout(function () {
                                    $controller.apiParams.search.data.filters = angular.copy(query.filters);
                                    $controller.loadData();
                                });
                            }
                            $rootScope.$broadcast($scope.name+'-table-builder--query-loaded', query);
                        };
                        $scope.$parent.load = $scope.load;

                        /*** Query Form ***/
                        $scope.query = {};

                        $scope.errors = {
                            fields: {}
                        };
                        $scope.save = function save() {
                            $scope.loadingSave = true;

                            $scope.query.filters = {
                                id: {
                                    op: 'eq',
                                    val: []
                                }
                            };

                            if ($controller) {
                                for (var i in $controller.items) {
                                    if ($controller.items[i].selected) {
                                        $scope.query.filters.id.val.push($controller.items[i].id);
                                    }
                                }
                                if ($scope.query.filters.id.val.length === 0) {
                                    $scope.query.filters = $controller.apiParams.search.data.filters;
                                }
                            }

                            if ($scope.headers) {
                                $scope.query.filters = $scope.headers;
                            }

                            $scope.query.list = $scope.name;

                            queryService.save($scope.query, {
                                col: [
                                    'id', 'name', 'filters', 'list', 'shared', 'sharedService', 'columns'
                                ]
                            }).then(function (res) {
                                $scope.queries.push(res.object);

                                $scope.query = {};
                                $scope.loadingSave = false;
                            }, function (err) {
                                for (var field in err.fields) {
                                    $scope.errors.fields[field] = err.fields[field];
                                }
                                $scope.loadingSave = false;
                            })
                        };

                        $scope.deleteQuery = function deleteQuery(query) {
                            if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                                $scope.loadingSave = true;
                                queryService.remove(query).then(function () {
                                    $scope.queries.splice($scope.queries.indexOf(query), 1);
                                    $scope.loadingSave = false;
                                }, function () {
                                    $scope.loadingSave = false;
                                })
                            }
                        };
                    }
                }
            }
        }
    ]);


})(window.EVA.app);
