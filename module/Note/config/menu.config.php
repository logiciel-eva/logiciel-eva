<?php

$note = [
    'icon'     => 'note',
    'module'   => 'note',
    'title'    => 'note_module_title',
    'href'     => 'note',
    'priority' => 1001,
    'right'    => 'project:e:project:r'
];

return [
    'menu' => [
        'client-menu' => [
            'note' => $note,
        ],
        'client-menu-parc' => [
            'suivi' => [
                'children' => [
                    'note' => $note,
                ],
            ],
        ],
    ],
];
