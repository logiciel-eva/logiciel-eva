<?php

namespace Project\Export;

use Core\Export\IExporter;

abstract class Project implements IExporter
{
    public static function getConfig()
    {
        return [
            'id'                    => [
                'format' => 'integer',
            ],
            'name'                  => [
                'format' => 'string',
            ],
            'code'                  => [
                'format' => 'string',
            ],
            'ownership'             => [
                'property' => 'ownership.name',
                'format'   => 'string',
            ],
            'parent' => [
                'property' => 'parent.name',
                'format'   => 'string',
            ],
            'template' => [
                'property' => 'template.name',
                'format'   => 'string',
            ],
            'validators.user.name'  => [
                'name'     => 'validators',
                'property' => 'validators.user.name',
            ],
            'budgetCodes'  => [
                'property' => 'budgetCodes.name',
            ],
            'validatorsArray' => [
                'property' => 'validators.id',
                'format' => function ($value, $row, $translator) {
                    $table = '<table class="table">';
                        $table .= '<thead>';
                            $table .= '<tr>';
                                $table .= '<th>' . $translator('project_field_validators') . '</th>';
                            $table .= '</tr>';
                        $table .= '</thead>';
                        $table .= '<tbody>';
                            foreach ($row->getValidators() as $member) {
                                $table .= '<tr>';
                                    $table .= '<td>' . $member->getUser()->getName() . '</td>';
                                $table .= '</tr>';
                            }
                        $table .= '</tbody>';
                    $table .= '</table>';

                    return $table;
                },
            ],
            'managers.user.name'    => [
                'name'     => 'managers',
                'property' => 'managers.user.name',
            ],
            'managersArray' => [
                'property' => 'managers.id',
                'format' => function ($value, $row, $translator) {
                    $table = '<table class="table">';
                        $table .= '<thead>';
                            $table .= '<tr>';
                                $table .= '<th>' . $translator('project_field_managers') . '</th>';
                            $table .= '</tr>';
                        $table .= '</thead>';
                        $table .= '<tbody>';
                            foreach ($row->getManagers() as $member) {
                                $table .= '<tr>';
                                    $table .= '<td>' . $member->getUser()->getName() . '</td>';
                                $table .= '</tr>';
                            }
                        $table .= '</tbody>';
                    $table .= '</table>';

                    return $table;
                },
            ],
            'membersArray' => [
                'property' => 'members.id',
                'format' => function ($value, $row, $translator, $roleKeywordsByMemberId) {
                    $table = '<table class="table">';
                        $table .= '<thead>';
                            $table .= '<tr>';
                                $table .= '<th>' . $translator('project_member_field_user') . '</th>';
                                $table .= '<th>' . $translator('project_member_field_role') . '</th>';
                            $table .= '</tr>';
                        $table .= '</thead>';
                        $table .= '<tbody>';
                        foreach ($row->getMembers() as $member) {
                            $memberRoleKeywords = $roleKeywordsByMemberId[$member->getId()];
                            $role = count($memberRoleKeywords)
                                    ? join(', ', $memberRoleKeywords)
                                    : $translator($member->getRole());

                            $table .= '<tr>';
                                $table .= '<td>' . $member->getUser()->getName() . '</td>';
                                $table .= '<td>' . $role . '</td>';
                            $table .= '</tr>';
                        }
                        $table .= '</tbody>';
                    $table .= '</table>';

                    return $table;
                },
            ],
            'actors' => [
                'property' => 'actors.structure.name',
                'format' => function ($value, $row, $translator, $roleKeywordsByActorId, $variableStr) {
                    $str = [];
                    foreach ($row->getActors() as $actor) {
                        if ($actor->getStructure()) {
                            $strTmp = "";
                            $memberRoleKeywords = $roleKeywordsByActorId[$actor->getId()];
                            $role = sizeof($memberRoleKeywords) > 0
                                ? join(', ', $memberRoleKeywords)
                                : '';
                            if (strpos($variableStr, 'name'))
                                $strTmp .= $actor->getStructure()->getName() . ',';
                            if (strpos($variableStr, 'sigle'))
                                $strTmp .= $actor->getStructure()->getSigle() . ',';
                            if (strpos($variableStr, 'email'))
                                $strTmp .= $actor->getStructure()->getEmail() . ',';
                            if (strpos($variableStr, 'phone'))
                                $strTmp .= $actor->getStructure()->getPhone() . ',';
                            if (strpos($variableStr, 'zipcode'))
                                $strTmp .= $actor->getStructure()->getZipCode() . ',';
                            if (strpos($variableStr, 'country'))
                                $strTmp .= $actor->getStructure()->getCountry() . ',';
                            if (strpos($variableStr, 'city'))
                                $strTmp .= $actor->getStructure()->getCity() . ',';
                            if (strpos($variableStr, 'addressLine1'))
                                $strTmp .= $actor->getStructure()->getAddressLine1() . ',';
                            if (strpos($variableStr, 'addressLine2'))
                                $strTmp .= $actor->getStructure()->getAddressLine2() . ',';
                            if (strpos($variableStr, 'financer'))
                                $strTmp .= $actor->getStructure()->getFinancer() . ',';
                            if (strpos($variableStr, 'siret'))
                                $strTmp .= $actor->getStructure()->getSiret() . ',';
                            if (strpos($variableStr, 'keywords'))
                                $strTmp .= $role;
                            $str[] = rtrim($strTmp, ',');
                        }
                    }
                    $allEmpty = true;
                    foreach ($str as $item) {
                        if (strlen($item) !== 0) {
                            $allEmpty = false;
                            break;
                        }
                    }
                    if (!$allEmpty)
                        return implode('; ', $str);
                    else {
                        return '';
                    }

                },
            ],
            'actorsArray' => [
                'property' => 'actors.structure.name',
                'format' => function ($value, $row, $translator, $roleKeywordsByActorId, $variableStr) {
                    $table = '<table class="table">';
                    $table .= '<thead>';
                    $table .= '<tr>';
                    if (strpos($variableStr, 'name'))
                        $table .= '<th>' . $translator('structure_field_name') . '</th>';
                    if (strpos($variableStr, 'sigle'))
                        $table .= '<th>' . $translator('structure_field_sigle') . '</th>';
                    if (strpos($variableStr, 'email'))
                        $table .= '<th>' . $translator('structure_field_email') . '</th>';
                    if (strpos($variableStr, 'phone'))
                        $table .= '<th>' . $translator('structure_field_phone') . '</th>';
                    if (strpos($variableStr, 'zipcode'))
                        $table .= '<th>' . $translator('structure_field_zipcode') . '</th>';
                    if (strpos($variableStr, 'country'))
                        $table .= '<th>' . $translator('structure_field_country') . '</th>';
                    if (strpos($variableStr, 'city'))
                        $table .= '<th>' . $translator('structure_field_city') . '</th>';
                    if (strpos($variableStr, 'addressLine1'))
                        $table .= '<th>' . $translator('structure_field_addressLine1') . '</th>';
                    if (strpos($variableStr, 'addressLine1'))
                        $table .= '<th>' . $translator('structure_field_addressLine2') . '</th>';
                    if (strpos($variableStr, 'financer'))
                        $table .= '<th>' . $translator('structure_field_financer') . '</th>';
                    if (strpos($variableStr, 'siret'))
                        $table .= '<th>' . $translator('structure_field_siret') . '</th>';
                    if (strpos($variableStr, 'keywords'))
                        $table .= '<th>' . $translator('structure_field_keywords') . '</th>';
                    $table .= '</tr>';
                    $table .= '</thead>';
                    $table .= '<tbody>';
                    foreach ($row->getActors() as $actor) {
                        if ($actor->getStructure()) {
                            $memberRoleKeywords = $roleKeywordsByActorId[$actor->getId()];
                            $role = sizeof($memberRoleKeywords) > 0
                                ? join(', ', $memberRoleKeywords)
                                : '';
                            $table .= '<tr>';
                            if (strpos($variableStr, 'name'))
                                $table .= '<td>' . $actor->getStructure()->getName() . '</td>';
                            if (strpos($variableStr, 'sigle'))
                                $table .= '<td>' . $actor->getStructure()->getSigle() . '</td>';
                            if (strpos($variableStr, 'email'))
                                $table .= '<td>' . $actor->getStructure()->getEmail() . '</td>';
                            if (strpos($variableStr, 'phone'))
                                $table .= '<td>' . $actor->getStructure()->getPhone() . '</td>';
                            if (strpos($variableStr, 'zipcode'))
                                $table .= '<td>' . $actor->getStructure()->getZipCode() . '</td>';
                            if (strpos($variableStr, 'country'))
                                $table .= '<td>' . $actor->getStructure()->getCountry() . '</td>';
                            if (strpos($variableStr, 'city'))
                                $table .= '<td>' . $actor->getStructure()->getCity() . '</td>';
                            if (strpos($variableStr, 'addressLine1'))
                                $table .= '<td>' . $actor->getStructure()->getAddressLine1() . '</td>';
                            if (strpos($variableStr, 'addressLine1'))
                                $table .= '<td>' . $actor->getStructure()->getAddressLine2() . '</td>';
                            if (strpos($variableStr, 'financer'))
                                $table .= '<td>' . $actor->getStructure()->getFinancer() . '</td>';
                            if (strpos($variableStr, 'siret'))
                                $table .= '<td>' . $actor->getStructure()->getSiret() . '</td>';
                            if (strpos($variableStr, 'keywords'))
                                $table .= '<td>' . $role . '</td>';
                            $table .= '</tr>';
                        }
                    }
                    $table .= '</tbody>';
                    $table .= '</table>';

                    return $table;
                },
            ],
            'publicationStatus'     => [
                'format' => function ($value, $row, $translator) {
                    return $translator('project_publicationStatus_' . $value);
                },
            ],
            'external' => [
                'format' => function ($value, $row) {
                    return $row->getExternal() ? 'Oui' : 'Non';
                },
            ],
            'networkAccessible' => [
                'format' => function ($value, $row) {
                    return $row->getNetworkAccessible() ? 'Oui' : 'Non';
                },
            ],
            'programmingDate' => [
                'format' => function ($value, $row) {
                    return $row->getProgrammingDate() ? $row->getProgrammingDate()->format('d/m/Y') : '';
                },
            ],
            'plannedDateStart' => [
                'format' => function ($value, $row) {
                    return $row->getPlannedDateStart() ? $row->getPlannedDateStart()->format('d/m/Y') : '';
                },
            ],
            'plannedDateEnd' => [
                'format' => function ($value, $row) {
                    return $row->getPlannedDateEnd() ? $row->getPlannedDateEnd()->format('d/m/Y') : '';
                },
            ],
            'realDateStart' => [
                'format' => function ($value, $row) {
                    return $row->getRealDateStart() ? $row->getRealDateStart()->format('d/m/Y') : '';
                },
            ],
            'realDateEnd' => [
                'format' => function ($value, $row) {
                    return $row->getRealDateEnd() ? $row->getRealDateEnd()->format('d/m/Y') : '';
                },
            ],
            'budgetStatus.name' => [
                'name'     => 'budgetStatus',
            ],
            'virtual.caduciteStart' => [
                'name'     => 'caducite_start',
                'property' => 'caduciteStart',
            ],
            'virtual.caduciteEnd'   => [
                'name'     => 'caducite_end',
                'property' => 'caduciteEnd',
            ],
            'virtual.advancement' => [
                'name' => 'advancement',
                'property' => 'stackedAdvancement',
                'format' => function ($value) {
                    return trim(explode(', ', $value)[0]) . '%';
                }
            ],
            'rateExpenseRealized' => [
                'format' => function ($value) {
                    return round($value, 2) . '%';
                }
            ],
            'rateIncomeRealized' => [
                'format' => function ($value) {
                    return round($value, 2) . '%';
                }
            ],
            'conventions' => [
                'property' => 'conventions.id',
                'format' => function ($value, $row, $translator, $roleKeywordsByConventionId, $variableStr) {
                    $str = [];
                    foreach ($row->getConventions() as $convention) {
                        $strTmp = "";
                        $role = '';
                        if($roleKeywordsByConventionId[$convention->getConvention()->getId()]) {
                            $memberRoleKeywords = $roleKeywordsByConventionId[$convention->getConvention()->getId()];
                            $role = sizeof($memberRoleKeywords) > 0
                                ? join(', ', $memberRoleKeywords)
                                : '';
                        }
                        if (strpos($variableStr, 'name'))
                            $strTmp .= $convention->getConvention()->getName() . ',';
                        if (strpos($variableStr, 'number') && !strpos($variableStr, 'numberArr'))
                            $strTmp .= $convention->getConvention()->getNumber() . ',' ;
                        if (strpos($variableStr, 'numberArr'))
                            $strTmp .= $convention->getConvention()->getNumberArr() . ',';
                        if (strpos($variableStr, 'contractor'))
                            if ($convention->getConvention()->getContractor())
                                $strTmp .= $convention->getConvention()->getContractor()->getName() . ',';
                            else
                                $strTmp .= ',';
                        if (strpos($variableStr, 'percentage'))
                            $strTmp .= $convention->getPercentage() . ',';
                        if (strpos($variableStr, 'start'))
                            if ($convention->getConvention()->getStart())
                                $strTmp .= $convention->getConvention()->getStart()->format('d/m/Y') . ',';
                            else
                                $strTmp .= ',';
                        if (strpos($variableStr, 'end'))
                            if ($convention->getConvention()->getEnd())
                                $strTmp .= $convention->getConvention()->getEnd()->format('d/m/Y') . ',';
                            else
                                $strTmp .= ',';
                        if (strpos($variableStr, 'decisionDate'))
                            if ($convention->getConvention()->getDecisionDate())
                                $strTmp .= $convention->getConvention()->getDecisionDate()->format('d/m/Y') . ',';
                            else
                                $strTmp .= ',';
                        if (strpos($variableStr, 'notificationDate'))
                            if ($convention->getConvention()->getNotificationDate())
                                $strTmp .= $convention->getConvention()->getNotificationDate()->format('d/m/Y') . ',';
                            else
                                $strTmp .= ',';
                        if (strpos($variableStr, 'returnDate'))
                            if ($convention->getConvention()->getReturnDate())
                                $strTmp .= $convention->getConvention()->getReturnDate()->format('d/m/Y') . ',';
                            else
                                $strTmp .= ',';
                        if (strpos($variableStr, 'amount') && !strpos($variableStr, 'amountSubv'))
                            $strTmp .= $convention->getConvention()->getAmount() . '€,';
                        if (strpos($variableStr, 'amountSubv'))
                            $strTmp .= $convention->getConvention()->getAmountSubv() . '€,';
                        if (strpos($variableStr, 'keywords'))
                            $strTmp .= $role;
                        $str[] = rtrim($strTmp, ',');
                    }
                    $allEmpty = true;
                    foreach ($str as $item) {
                        if (strlen($item) !== 0) {
                            $allEmpty = false;
                            break;
                        }
                    }
                    if (!$allEmpty)
                        return implode('; ', $str);
                    else {
                        return '';
                    }
                },
            ],
            'conventionsArray' => [
                'property' => 'conventions.id',
                'format' => function ($value, $row, $translator, $roleKeywordsByConventionId, $variableStr) {
                    $table = '<table class="table">';
                    $table .= '<thead>';
                    $table .= '<tr>';
                    if (strpos($variableStr, 'name'))
                        $table .= '<th>' . $translator('convention_field_name') . '</th>';
                    if (strpos($variableStr, 'number') && !strpos($variableStr, 'numberArr'))
                        $table .= '<th>' . $translator('convention_field_number') . '</th>';
                    if (strpos($variableStr, 'numberArr'))
                        $table .= '<th>' . $translator('convention_field_numberArr') . '</th>';
                    if (strpos($variableStr, 'percentage'))
                        $table .= '<th>' . $translator('convention_line_field_percentage') . '</th>';
                    if (strpos($variableStr, 'contractor'))
                        $table .= '<th>' . $translator('convention_field_contractor') . '</th>';
                    if (strpos($variableStr, 'start'))
                        $table .= '<th>' . $translator('convention_field_start') . '</th>';
                    if (strpos($variableStr, 'end'))
                        $table .= '<th>' . $translator('convention_field_end') . '</th>';
                    if (strpos($variableStr, 'decisionDate'))
                        $table .= '<th>' . $translator('convention_field_decisionDate') . '</th>';
                    if (strpos($variableStr, 'notificationDate'))
                        $table .= '<th>' . $translator('convention_field_notificationDate') . '</th>';
                    if (strpos($variableStr, 'returnDate'))
                        $table .= '<th>' . $translator('convention_field_returnDate') . '</th>';
                    if (strpos($variableStr, 'amount') && !strpos($variableStr, 'amountSubv'))
                        $table .= '<th>' . $translator('convention_field_amount') . '</th>';
                    if (strpos($variableStr, 'amountSubv'))
                        $table .= '<th>' . $translator('convention_field_amountSubv') . '</th>';
                    if (strpos($variableStr, 'keywords'))
                        $table .= '<th>' . $translator('convention_field_keywords') . '</th>';
                    $table .= '</tr>';
                    $table .= '</thead>';
                    $table .= '<tbody>';
                    foreach ($row->getConventions() as $convention) {
                        $role = '';
                        if($roleKeywordsByConventionId[$convention->getConvention()->getId()]) {
                            $memberRoleKeywords = $roleKeywordsByConventionId[$convention->getConvention()->getId()];
                            $role = sizeof($memberRoleKeywords) > 0
                                ? join(', ', $memberRoleKeywords)
                                : '';
                        }
                        $table .= '<tr>';
                        if (strpos($variableStr, 'name'))
                            $table .= '<td>' . $convention->getConvention()->getName() . '</td>';
                        if (strpos($variableStr, 'number') && !strpos($variableStr, 'numberArr'))
                            $table .= '<td>' . $convention->getConvention()->getNumber() . '</td>';
                        if (strpos($variableStr, 'numberArr'))
                            $table .= '<td>' . $convention->getConvention()->getNumberArr() . '</td>';
                        if (strpos($variableStr, 'contractor'))
                            if ($convention->getConvention()->getContractor())
                                $table .= '<td>' . $convention->getConvention()->getContractor()->getName() . '</td>';
                            else
                                $table .= '<td></td>';
                        if (strpos($variableStr, 'percentage'))
                            $table .= '<td>' . $convention->getPercentage() . '</td>';
                        if (strpos($variableStr, 'start'))
                            if ($convention->getConvention()->getStart())
                                $table .= '<td>' . $convention->getConvention()->getStart()->format('d/m/Y') . '</td>';
                            else
                                $table .= '<td></td>';
                        if (strpos($variableStr, 'end'))
                            if ($convention->getConvention()->getEnd())
                                $table .= '<td>' . $convention->getConvention()->getEnd()->format('d/m/Y') . '</td>';
                            else
                                $table .= '<td></td>';
                        if (strpos($variableStr, 'decisionDate'))
                            if ($convention->getConvention()->getDecisionDate())
                                $table .= '<td>' . $convention->getConvention()->getDecisionDate()->format('d/m/Y') . '</td>';
                            else
                                $table .= '<td></td>';
                        if (strpos($variableStr, 'notificationDate'))
                            if ($convention->getConvention()->getNotificationDate())
                                $table .= '<td>' . $convention->getConvention()->getNotificationDate()->format('d/m/Y') . '</td>';
                            else
                                $table .= '<td></td>';
                        if (strpos($variableStr, 'returnDate'))
                            if ($convention->getConvention()->getReturnDate())
                                $table .= '<td>' . $convention->getConvention()->getReturnDate()->format('d/m/Y') . '</td>';
                            else
                                $table .= '<td></td>';
                        if (strpos($variableStr, 'amount') && !strpos($variableStr, 'amountSubv'))
                            $table .= '<td>' . $convention->getConvention()->getAmount() . '€</td>';
                        if (strpos($variableStr, 'amountSubv'))
                            $table .= '<td>' . $convention->getConvention()->getAmountSubv() . '€</td>';
                        if (strpos($variableStr, 'keywords'))
                            $table .= '<td>' . $role . '</td>';
                        $table .= '</tr>';
                    }
                    $table .= '</tbody>';
                    $table .= '</table>';
                    return $table;
                },
            ]
        ];
    }

    public static function getAliases()
    {
        return [
            'virtual.ownership.name' => 'ownership',
            'ownership.name'         => 'ownership',
            'parent.name'            => 'parent',
            'template.name'          => 'template',
            'validator.user.name'    => 'validators.user.name',
            'validators'             => 'validators.user.name',
            'validators_array'       => 'validatorsArray',
            'manager.user.name'      => 'managers.user.name',
            'managers'               => 'managers.user.name',
            'managers_array'         => 'managersArray',
            'budgetStatus'           => 'budgetStatus.name',
            'members'                => 'members.user.name',
            'members_array'          => 'membersArray',
        ];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
