<?php

namespace Budget\Helper;

use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Doctrine\ORM\EntityRepository;

class PosteFinder
{

    /**
     * Find a "poste" recursively from a parent account
     * 
     * @param Account               $account
     * @param Project               $project
     * @param EntityRepository      $posteRepository
     *
     * @return PosteExpense|PosteIncome|NULL
     */
    public static function findPosteByParentAccount($account, $project, $posteRepository)
    {
        if (!isset($account)) {
            return null;
        }
        $poste = $posteRepository->findOneBy([
            'account' => $account,
            'project' => $project,
        ]);
        // trying to raise in hierarchy if the current parent account has no "poste"
        if (!isset($poste)) {
            $poste = PosteFinder::findPosteByParentAccount($account->getParent(), $project, $posteRepository);
        }
        return $poste;
    }
}
