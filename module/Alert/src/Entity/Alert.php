<?php

namespace Alert\Entity;

use Application\Entity\Query;
use Core\Email\Email;
use Core\Entity\BasicRestEntityAbstract;
use Core\Utils\MailUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Member;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Alert
 *
 * @package Project\Entity
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="alert_alert")
 */
class Alert extends BasicRestEntityAbstract
{
    const PERIODICITY_DAILY   = 'daily';
    const PERIODICITY_WEEKLY  = 'weekly';
    const PERIODICITY_MONTHLY = 'monthly';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var Query
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Query")
     */
    protected $query;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $category;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User\Entity\User")
     */
    protected $recipients;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $toManagers;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $toValidators;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $toMembers;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $subject;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $periodicity;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Indicator\Entity\Indicator")
     */
    protected $indicators;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Indicator\Entity\GroupIndicator")
     */
    protected $groupIndicators;

    public function __construct()
    {
        $this->recipients = new ArrayCollection();
        $this->indicators = new ArrayCollection();
        $this->groupIndicators = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
    * @return string
    */
   public function getCategory()
   {
       return $this->category;
   }

   /**
    * @param string $category
    */
   public function setCategory($category)
   {
       $this->category = $category;
   }

    /**
     * @return Query
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param Query $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return ArrayCollection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param ArrayCollection $recipients
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    }

    /**
     * @param ArrayCollection $recipients
     */
    public function addRecipients(ArrayCollection $recipients)
    {
        foreach ($recipients as $recipient) {
            $this->getRecipients()->add($recipient);
        }
    }

    /**
     * @param ArrayCollection $recipients
     */
    public function removeRecipients(ArrayCollection $recipients)
    {
        foreach ($recipients as $recipient) {
            $this->getRecipients()->removeElement($recipient);
        }
    }

    /**
     * @return boolean
     */
    public function isToManagers()
    {
        return $this->toManagers;
    }

    /**
     * @return boolean
     */
    public function getToManagers()
    {
        return $this->isToManagers();
    }

    /**
     * @param boolean $toManagers
     */
    public function setToManagers($toManagers)
    {
        $this->toManagers = $toManagers;
    }

    /**
     * @return boolean
     */
    public function isToValidators()
    {
        return $this->toValidators;
    }

    /**
     * @return boolean
     */
    public function getToValidators()
    {
        return $this->isToValidators();
    }

    /**
     * @param boolean $toValidators
     */
    public function setToValidators($toValidators)
    {
        $this->toValidators = $toValidators;
    }

    /**
     * @return boolean
     */
    public function isToMembers()
    {
        return $this->toMembers;
    }

    /**
     * @return boolean
     */
    public function getToMembers()
    {
        return $this->isToMembers();
    }

    /**
     * @param boolean $toMembers
     */
    public function setToMembers($toMembers)
    {
        $this->toMembers = $toMembers;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }

    /**
     * @return string
     */
    public function getPeriodicity()
    {
        return $this->periodicity;
    }

    /**
     * @param string $periodicity
     */
    public function setPeriodicity($periodicity)
    {
        $this->periodicity = $periodicity;
    }

    /**
     * @return indicators[]|ArrayCollection
     */
    public function getIndicators()
    {
        return $this->indicators;
    }

    /**
     * @param Indicator[]|ArrayCollection $indicators
     */
    public function setIndicators($indicators)
    {
        $this->indicators = $indicators;
    }

    /**
     * @param ArrayCollection|Indicator[] $indicators
     */
    public function addIndicators(ArrayCollection $indicators)
    {
        foreach ($indicators as $indicator) {
            $this->getIndicators()->add($indicator);
        }
    }

    /**
     * @param ArrayCollection|Indicator[] $indicators
     */
    public function removeIndicators(ArrayCollection $indicators)
    {
        foreach ($indicators as $indicator) {
            $this->getIndicators()->removeElement($indicator);
        }
    }


    /**
     * @return indicators[]|ArrayCollection
     */
    public function getGroupIndicators()
    {
        return $this->groupIndicators;
    }

    /**
     * @param GroupIndicator[]|ArrayCollection $groupIndicators
     */
    public function setGroupIndicators($groupIndicators)
    {
        $this->groupIndicators = $groupIndicators;
    }

    /**
     * @param ArrayCollection|GroupIndicator[] $groupIndicators
     */
    public function addGroupIndicators(ArrayCollection $groupIndicators)
    {
        foreach ($groupIndicators as $groupIndicator) {
            $this->getGroupIndicators()->add($groupIndicator);
        }
    }

    /**
     * @param ArrayCollection|GroupIndicator[] $groupIndicators
     */
    public function removeGroupIndicators(ArrayCollection $groupIndicators)
    {
        foreach ($groupIndicators as $groupIndicator) {
            $this->getGroupIndicators()->removeElement($groupIndicator);
        }
    }


    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'query',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                $query = $entityManager->getRepository('Application\Entity\Query')->find($value);

                                return $query !== null;
                            },
                            'message'  => 'alert_field_query_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'category',
                'required' => false,
            ],
            [
                'name'     => 'recipients',
                'required' => false,
            ],
			[
				'name'     => 'indicators',
				'required' => false,
                'allow_empty' => true,
			],
			[
				'name'     => 'groupIndicators',
				'required' => false,
                'allow_empty' => true,
			],
            [
                'name'        => 'toValidators',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'        => 'toManagers',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'        => 'toMembers',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'     => 'subject',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'content',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ],
            [
                'name'        => 'enabled',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'      => 'periodicity',
                'required'  => true,
                'validator' => [
                    [
                        'name'    => 'InArray',
                        'options' => [
                            'haystack' => self::getPeriodicities(),
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }

    public function send($object, $client, $em,  $console = false)
    {
        
        if($this->getQuery()->getList() == 'projects-list')
        {
            return self::sendProjectEmail($object, $client, $console);
        }else if($this->getQuery()->getList() == 'campain-measures-list'){
            return self::sendCampainMeasureEmail($object, $client, $em, $console);
        }  
    }

    private function sendProjectEmail($projects, $client, $console = false){
        $mails = [];
        foreach ($this->recipients as $recipient) {
            if ($recipient->getEmail()) {
                $mails['user' . $recipient->getId()] = [
                    'name'     => $recipient->getName(),
                    'at'       => $recipient->getEmail(),
                    'projects' => [],
                ];

                foreach ($projects as $project) {
                    $mails['user' . $recipient->getId()]['projects'][$project['id']] = $project;
                }
            }
        }
        if($this->getQuery()->getList() == 'projects-list')
        {
            foreach ($projects as $project) {
                if ($project['members']) {
                    foreach ($project['members'] as $member) {
                        if ($member['user']['email']) {
                            $send = false;
                            if ($member['role'] === Member::ROLE_MANAGER && $this->isToManagers()) {
                                $send = true;
                            } else if ($member['role'] === Member::ROLE_VALIDATOR && $this->isToValidators()) {
                                $send = true;
                            } else if ($member['role'] === Member::ROLE_MEMBER && $this->isToMembers()) {
                                $send = true;
                            }

                            if ($send) {
                                if (!isset($mails['user' . $member['user']['id']])) {
                                    $mails['user' . $member['user']['id']] = [
                                        'name'     => $member['user']['name'],
                                        'at'       => $member['user']['email'],
                                        'projects' => [],
                                    ];
                                }

                                $mails['user' . $member['user']['id']]['projects'][$project['id']] = $project;
                            }
                        }
                    }
                }
            }
        }


        foreach ($mails as $mail) {
            $subject = $this->getSubject();
            $content = $this->getContent();

            $projectsVar = '<table>';
            foreach ($mail['projects'] as $project) {
                if($this->getQuery()->getList() == 'projects-list')
                {
                    $projectsVar .= '<tr>';
                    $projectsVar .= '<td>';
                    $projectsVar .= '<a href="http://' . $client->getHost() . '/project/form/' . $project['id'] . '">';
                    $projectsVar .= ($project['code'] ? $project['code'] . ' - ' : '') . $project['name'];
                    $projectsVar .= '</a>';
                    $projectsVar .= '</td>';
                    $projectsVar .= '</tr>';
                }
            }
            $projectsVar .= '</table>';
            if($this->getQuery()->getList() == 'projects-list')
            {
                $subject = str_replace(['$projects$', '$recipient$'], [$projectsVar, $mail['name']], $subject);
                $content = str_replace(['$projects$', '$recipient$'], [$projectsVar, $mail['name']], $content);
            }
            $email = new Email($client);
            $email->setSubject($subject);
            $email->addHtmlContent($content);
            $email->setFrom(MailUtils::getFrom($client, 'alerte'), 'Alerte');
            $email->sendTo($mail['at']);

            if ($console) {
                echo PHP_EOL . 'Mail sent to : ' . $mail['at'] . ' at ' . date('d/m/Y H:i:s');
            }
        }

        return true;
    }

    private function sendCampainMeasureEmail($measures, $client, $em, $console = false){
        $mails = [];
        $campains =[];
        
        $userRepository = $em->getRepository('User\Entity\User');
        //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " .  json_encode($measures) . PHP_EOL, FILE_APPEND);
        foreach($measures as $measure){
            $currentCampain = $measure['campain'];
            if(!isset($campains[$currentCampain['id']])){
                $campains[$currentCampain['id']] = [];
                $campains[$currentCampain['id']]['campain'] = $currentCampain;
                $campains[$currentCampain['id']]['indicators'] = [];
            }

            $currentIndicator = $measure["realiseMeasure"]["indicator"];
            
            if(!isset($campains[$currentCampain['id']]['indicators'][$currentIndicator['id']])){
                $campains[$currentCampain['id']]['indicators'][$currentIndicator['id']] = $currentIndicator;
            }
            
            if(!isset($campains[$currentCampain['id']]['users'])){
                $campains[$currentCampain['id']]['users'] = [];
            }

            if($this->isToValidators()){
                $responsibles = $measure["indicator"]["users"];
                foreach($responsibles as $userResponsible){
                    if(!isset($campains[$currentCampain['id']]['users'][$userResponsible['id']])){
                        $campains[$currentCampain['id']]['users'][$userResponsible['id']] = [
                            'id' => $userResponsible['id'],
                            'indicators' => [
                                $currentIndicator['id'] => $currentIndicator
                            ]
                        ];
                    }else{
                        if(!isset($campains[$currentCampain['id']]['users'][$userResponsible['id']]['indicators'][$currentIndicator['id']])){
                            $campains[$currentCampain['id']]['users'][$userResponsible['id']]['indicators'][$currentIndicator['id']] = $currentIndicator;
                        }
                    }
                }
            }
            if($this->isToManagers()){
                $evaluationManagers = $currentCampain["evaluationManagers"];
                foreach($evaluationManagers as $evaluationManager){
                    if(!isset($campains[$currentCampain['id']]['users'][$evaluationManager['id']])){
                        $campains[$currentCampain['id']]['users'][$evaluationManager['id']] = [
                            'id' => $evaluationManager['id'],
                            'indicators' => [
                                $currentIndicator['id'] => $currentIndicator
                            ]
                        ];
                    }else{
                        if(!isset($campains[$currentCampain['id']]['users'][$evaluationManager['id']]['indicators'][$currentIndicator['id']])){
                            $campains[$currentCampain['id']]['users'][$evaluationManager['id']]['indicators'][$currentIndicator['id']] = $currentIndicator;
                        }
                    }
                }
            }
        }
        
        foreach ($this->recipients as $recipient) {
            if ($recipient->getEmail()) {
                $mails['user' . $recipient->getId()] = [
                    'name'     => $recipient->getName(),
                    'at'       => $recipient->getEmail()
                ];
            }
        }
        
        //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " .  json_encode($campains) . PHP_EOL, FILE_APPEND);
        foreach($campains as $campain){
            
            $indicatorsVar = '<table>';
            foreach ($campain['indicators'] as $indicator) {
                    $indicatorsVar .= '<tr>';
                    $indicatorsVar .= '<td>';
                    $indicatorsVar .= '<a href="http://' . $client->getHost() . '/campain/form/' . $campain['campain']['id'] .'?q=' . $this->getQuery()->getId() . '#measure">';
                    $indicatorsVar .= $indicator['name'];
                    $indicatorsVar .= '</a>';
                    $indicatorsVar .= '</td>';
                    $indicatorsVar .= '</tr>';
            }
            $indicatorsVar .= '</table>';
            // sending global emails defined in alert
            foreach ($mails as $mail) {
                $subject = $this->getSubject();
                $content = $this->getContent();
                $subject = str_replace(['$campainName$'], [$campain['campain']['name']], $subject);
                $content = str_replace(['$indicators$', '$campainName$', '$recipient$'], [$indicatorsVar, $campain['campain']['name'], $mail['name']], $content);
                $email = new Email($client);
                $email->setSubject($subject);
                $email->addHtmlContent($content);
                $email->setFrom(MailUtils::getFrom($client, 'alerte'), 'Alerte');
                //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . 'Global - Mail sent for Campain: ' . $campain['campain']['name'] . ' to : ' . $mail['at'] . " content " . $content . PHP_EOL, FILE_APPEND);
                $email->sendTo($mail['at']);
                if ($console) {
                    echo PHP_EOL . 'Mail sent for Campain: ' . $campain['campain']['name'] . ' to : ' . $mail['at'] . ' at ' . date('d/m/Y H:i:s') ;
                }
            }
            
            // sending responsible emails defined in campain indicators
            foreach($campain['users'] as $userc){
                //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . json_encode($userc) . PHP_EOL, FILE_APPEND);
                
                $indicatorsVar = '<table>';
                foreach ($userc['indicators'] as $indicator) {
                    if(isset($indicator['name'])){
                        //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - indicators" . json_encode($indicator) . PHP_EOL, FILE_APPEND);
                        $indicatorsVar .= '<tr>';
                        $indicatorsVar .= '<td>';
                        $indicatorsVar .= '<a href="http://' . $client->getHost() . '/campain/form/' . $campain['campain']['id'] .'?q=' . $this->getQuery()->getId() . '#measure">';
                        $indicatorsVar .= $indicator['name'];
                        $indicatorsVar .= '</a>';
                        $indicatorsVar .= '</td>';
                        $indicatorsVar .= '</tr>';
                    }
                }
                $indicatorsVar .= '</table>';

                $user = $userRepository->find($userc['id']);
                
                $subject = $this->getSubject();
                $content = $this->getContent();
                
                $subject = str_replace(['$campainName$'], [$campain['campain']['name']], $subject);
                $content = str_replace(['$indicators$', '$campainName$', '$recipient$'], [$indicatorsVar, $campain['campain']['name'], $user->getName()], $content);
                $email = new Email($client);
                $email->setSubject($subject);
                $email->addHtmlContent($content);
                $email->setFrom(MailUtils::getFrom($client, 'alerte'), 'Alerte');
                //file_put_contents("debug_sql.log", date("Y-m-d H:i:s") . " - " . 'Referent - Mail sent for Campain: ' . $campain['campain']['name'] . ' to : ' . $user->getEmail() . " content " . $content . PHP_EOL, FILE_APPEND);
                $email->sendTo($user->getEmail());
                if ($console) {
                    echo PHP_EOL . 'Mail sent for Campain: ' . $campain['campain']['name'] .
                    ' on indicators'. json_encode($user['indicators']) . 
                    ' to : ' . $user->getEmail() . ' at ' . date('d/m/Y H:i:s') ;
                }
            }
        }

        return true;
    }

    public function isSendable()
    {
        if ($this->isEnabled()) {
            $date = new \DateTime();

            if ($this->getPeriodicity() === Alert::PERIODICITY_DAILY ||
                ($this->getPeriodicity() === Alert::PERIODICITY_WEEKLY && $date->format('l') === 'Monday') ||
                ($this->getPeriodicity() === Alert::PERIODICITY_MONTHLY && $date->format('l') === 'Monday' && $date->format('d') <= 7)
            ) {
                return true;
            }
        }

        return false;
    }

    public static function getPeriodicities()
    {
        return [
            self::PERIODICITY_DAILY,
            self::PERIODICITY_WEEKLY,
            self::PERIODICITY_MONTHLY,
        ];
    }
}

