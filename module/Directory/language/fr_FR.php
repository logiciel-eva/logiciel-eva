<?php

return [
    /* Directory */
    'directory_module_title' => 'Annuaire',
    'directory_address' => 'Adresse',
    'directory_field_email' => 'Adresse email',
    'directory_field_portable' => 'Portable',
    'directory_field_phone' => 'Numéro de téléphone',
    'directory_field_zipcode' => 'Code postal',
    'directory_field_country' => 'Pays',
    'directory_field_city' => 'Ville',
    'directory_field_addressLine1' => 'Adresse, ligne 1',
    'directory_field_addressLine2' => 'Adresse, ligne 2',

    /* Contact */
    'contact_entity' => 'contact',
    'contact_entities' => 'contacts',
    'contact_module_title' => '[[ contact_entities | ucfirst ]]',
    'contact_nav_list' => 'Liste des {{ __tb.total }} [[ contact_entities ]]',
    'contact_nav_form' => 'Créer un [[ contact_entity ]]',
    'contact_nav_import' => 'Importer des [[ contact_entities ]]',
    'contact_job_form' => 'Ajouter un [[ job_entity ]]',
    'contact_field_id' => 'ID',
    'contact_field_name' => 'Nom',
    'contact_field_email' => 'Adresse email',
    'contact_field_phone' => 'Téléphone',
    'contact_field_portable' => 'Portable',
    'contact_field_zipcode' => 'Code postal',
    'contact_field_addressLine1' => 'Adresse, ligne 1',
    'contact_field_addressLine2' => 'Adresse, ligne 2',
    'contact_field_city' => 'Ville',
    'contact_field_country' => 'Pays',
    'contact_field_lastName' => 'Nom',
    'contact_field_firstName' => 'Prénom',
    'contact_field_gender' => 'Civilité',
    'contact_field_vip' => 'VIP',
    'contact_field_linksStr' => 'Fonctions',
    'contact_field_contactEmails' => 'Emails',
    'contact_field_job' => '[[ job_entity | ucfirst ]]',
    'contact_field_createdAt' => '[[ field_created_at_m ]]',
    'contact_field_updatedAt' => '[[ field_updated_at_m ]]',
    'contact_field_subscriptionStatus' => 'Statut de diffusion',
    'contact_field_subscriptionStatusUpdatedAt' => 'Date du statut de diffusion',

    'contact_message_saved' => '[[ contact_entity | ucfirst ]] enregistré',
    'contact_message_deleted' => '[[ contact_entity | ucfirst ]] supprimé',
    'contact_question_delete' => 'Voulez-vous réellement supprimer le [[ contact_entity ]] %1 ?',
    'contact_message_saved_public' => 'Vos informations ont bien été mises à jour',

    'contact_fusion_title' => 'Fusionner des [[ contact_entities ]]',
    'structure_fusion_title' => 'Fusionner des [[ structure_entities ]]',

    'contact_gender_' => '',
    'contact_gender_null' => '',
    'contact_gender_m' => 'M.',
    'contact_gender_mme' => 'Mme.',
    'contact_gender_neutral' => 'Non binaire',

    'contact_subscription_status_' => 'Non renseigné',
    'contact_subscription_status_empty' => 'Non renseigné',
    'contact_subscription_status_null' => 'Non renseigné',
    'contact_subscription_status_accepted' => 'Accepté',
    'contact_subscription_status_declined' => 'Refusé',
    'accepted' => 'Accepté',
    'declined' => 'Refusé',

    /* Job */
    'job_field_contact' => 'Employé(e)',
    'job_field_structure' => '[[ structure_entity | ucfirst ]] associée',

    'job' => '[[ job_entity | ucfirst ]]',
    'jobs' => '[[ job_entities | ucfirst ]]',

    'job_adresses_module_title' => 'Adresses',

    'job_module_title' => '[[ job_entities | ucfirst ]]',
    'job_fusion_title' => 'Fusionner des [[ job_entities ]]',

    'job_new_title' => 'Nouvelle [[ job_entity ]]',

    'job_nav_list' => 'Liste des [[ job_entities ]]',
    'job_nav_form' => 'Créer une [[ job_entity ]]',
    'job_nav_import' => 'Importer des [[ job_entities ]]',

    'job_entity' => 'fonction',
    'job_entities' => 'fonctions',

    'job_field_id' => 'ID',
    'job_field_name' => 'Fonction',
    'job_field_description' => 'Description',
    'job_field_parents' => 'Parents',
    'job_field_order' => 'Ordre',
    'job_field_createdAt' => '[[ field_created_at_m ]]',
    'job_field_updatedAt' => '[[ field_updated_at_m ]]',

    'job_question_delete' => 'Voulez-vous réellement supprimer le [[ job_entity ]] %1 ?',
    'job_message_deleted' => '[[ job_entity | ucfirst ]] supprimé',

    'job_field_group_error_non_exists' => 'Ce [[ group_entity ]] n\'existe pas ou plus',

    'job_message_saved' => '[[ job_entity | ucfirst ]] enregistré',
    'job_message_archived' => '[[ job_entity | ucfirst ]] archivé',
    'job_message_unarchived' => '[[ job_entity | ucfirst ]] désarchivé',

    'fusion_field_jobToKeep' => '[[ job_entity | ucfirst ]] cible de la fusion',
    'fusion_field_type' => 'Type de fusion',

    'contactJobStructure_entity' => 'fonction Contact',
    'contactJobStructure_entites' => 'fonctions contact',

    /* Structure */
    'structure_entity' => 'structure',
    'structure_entities' => 'structures',

    'structure_module_title' => '[[ structure_entity | ucfirst ]]',
    'structure_nav_form' => 'Créer une [[ structure_entity ]]',
    'structure_nav_list' => 'Liste des {{ __tb.total }} [[ structure_entities ]]',
    'structure_nav_import' => 'Importer des [[ structure_entities ]]',
    'structure_nav_import_links' => 'Importer des liens entre [[ contact_entities ]] - [[ structure_entities ]] - [[ job_entities ]]',
    'structure_job_list' => 'Liste des employés',
    'structure_job_form' => 'Ajouter un employé',
    'structure_field_id' => 'ID',
    'structure_field_name' => 'Nom',
    'structure_field_email' => 'Adresse email',
    'structure_field_sigle' => 'Sigle',
    'structure_field_phone' => 'Numéro de téléphone',
    'structure_field_portable' => 'Portable',
    'structure_field_phone2' => 'Téléphone secondaire',
    'structure_field_zipcode' => 'Code postal',
    'structure_field_country' => 'Pays',
    'structure_field_city' => 'Ville',
    'structure_field_addressLine1' => 'Adresse, ligne 1',
    'structure_field_addressLine2' => 'Adresse, ligne 2',
    'structure_field_financer' => 'Financeur',
    'structure_field_siret' => 'SIRET',
    'structure_field_website' => 'Site Web',
    'structure_field_keywords' => 'Mots-clés',
    'structure_field_createdAt' => '[[ field_created_at_f ]]',
    'structure_field_updatedAt' => '[[ field_updated_at_f ]]',
    'structure_field_type_boolean' => 'Les valeurs attendu sont : ("oui", "non", 1, 0)',


    'structure_message_saved' => '[[ structure_entity | ucfirst ]] enregistrée',
    'structure_message_deleted' => '[[ structure_entity | ucfirst ]] supprimée',
    'structure_question_delete' => 'Voulez-vous réellement supprimer la [[ structure_entity ]] %1 ?',

    'contact_unsubscribe_message' => 'Voulez-vous réellement vous désinscrire de cette liste de diffusion ? ',
    'contact_unsubscribe_btn_confirm' => 'Je confirme',
    'contact_unsubscribe_success' => 'Votre désinscription a correctement été prise en compte.',

    'contactEmail_entity' => 'email',
    'contactEmail_entities' => 'emails',

    'contactEmail_nav_form' => 'Créer un [[ contactEmail_entity ]]',
    'contactEmail_nav_list' => 'Liste des [[ contactEmail_entities ]]',

    'contactEmail_field_name' => 'Nom',
    'contactEmail_field_email' => 'Email',

    'contactEmail_message_saved' => '[[ contactEmail_entity | ucfirst ]] enregistré',
    'contactEmail_message_deleted' => '[[ contactEmail_entity | ucfirst ]] supprimé',
    'contactEmail_question_delete' => 'Voulez-vous réellement supprimer l\'[[ contactEmail_entity ]] %1 ?',

    'contactJobStructure_field_contact' => '[[ contact_entity ]]',
    'contactJobStructure_field_structure' => '[[ structure_entity ]]',
    'contactJobStructure_field_job' => '[[ job_entity ]]',

    'contact_field_error_non_exists' => 'ce [[ contact_entity ]] n\'existe pas',
    'structure_field_error_non_exists' => 'cette [[ structure_entity ]] n\'existe pas',
    'job_field_error_non_exists' => 'ce [[ job_entity ]] n\'existe pas',
    'job_structure_contact_field_error_already_exists' => 'Ces relations existent déjà !',

    'duplicate_tooltip' =>
    'Pour rechercher des doublons : cocher les colonnes que vous souhaitez comparer puis cocher
    la case de recherche de doublon.
    Décocher et recocher la case après chaque changement du choix des colonnes.
    Ne prend pas en compte les ID et dates.',
];
