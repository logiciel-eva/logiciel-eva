(function (app) {

    app.controller('yearController', [
        '$http', 'queryService', '$rootScope',
        function yearController($http, queryService, $rootScope) {
            var self = this;

            self.years     = [];
            self.financers = [];

            self.filters = {
                project  : {},
                structure: {},
                group    : 'financer'
            };
            self.nbFilters = 1;

            self.loading = false;

            self.getPartialTotal = function (_year, _client, _type) {
                var clientReg = new RegExp('^' + _client + '_');
                var total     = 0;

                for (var i in self.years) {
                    var year = self.years[i];
                    if (year.year == _year) {
                        for (var j in year.data) {
                            if (j.match(clientReg)) {
                                var data = year.data[j];
                                total += data[_type];
                            }
                        }
                    }
                }

                return total;
            };

            self.applyFilters = function () {
                self.loading = true;
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.structure).length;

                $http({
                    url   : '/analysis/budget/year?' + $.param({filters: self.filters}),
                    method: 'GET'
                }).then(function (res) {
                    self.years     = res.data.years;
                    self.financers = res.data.financers;

                    self.loading = false;
                });
            };

            self.resetFilters = function () {
                self.filters.project   = {};
                self.filters.structure = {};
                self.applyFilters();
            };

            var cache        = {};
            self.columnEmpty = function (financer, type) {
                var key = financer._client.id + '_' + financer.id;

                if (typeof cache[key] == 'undefined') {
                    cache[key] = {};
                }

                if (typeof cache[key][type] == 'undefined') {
                    cache[key][type] = true;
                } else {
                    return cache[key][type];
                }

                for (var i in self.years) {
                    if (typeof self.years[i].data[key] !== 'undefined') {
                        if (self.years[i].data[key][type] !== 0) {
                            cache[key][type] = false;
                        }
                    }
                }

                return cache[financer._client.id + '_' + financer.id][type];
            };

            self.getColspan = function (type) {
                var colspan = self.financers.length;
                for (var i in self.financers) {
                    if (self.columnEmpty(self.financers[i], type)) {
                        colspan--;
                    }
                }
                return colspan;
            };

            self.export = function () {
                window.open('/analysis/budget/year?' + $.param({filters: self.filters, export: true}));
            };

            // QUERIES
            self.queries          = [];
            self.query            = {
                list: 'analysis-budget-year'
            };
            self.errors           = {
                fields: {}
            };
            self.loadingQueries   = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries        = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };
        }
    ]);
})(window.EVA.app);
