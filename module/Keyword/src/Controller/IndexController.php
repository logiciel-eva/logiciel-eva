<?php

namespace Keyword\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('keyword:e:group:r')) {
            return $this->notFoundAction();
        }

        $entities = [];
        $moduleManager = $this->moduleManager();

        $currentClient = $this->environment()->getClient();
        if ($currentClient->hasNetwork() && $currentClient->isMaster()) {
            $modules = $moduleManager->getModules();
        } else {
            $modules = $moduleManager->getActiveModules();
        }

        foreach ($modules as $module) {
            $aclConfiguration = $module->getAclconfiguration();
            foreach ($aclConfiguration['entities'] as $entity) {
                if (isset($entity['keywords']) && $entity['keywords'] === true) {
                    $entities[] = $entity;
                }
            }
        }

        sort($entities);

        return new ViewModel([
            'entities' => $entities
        ]);
    }
}
