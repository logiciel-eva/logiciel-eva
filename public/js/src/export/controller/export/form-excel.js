(function (app) {

    app.controller('exportFormExcelController', [
        'exportService', 'attachmentService', '$scope', 'flashMessenger', 'translator',
        function exportFormExcelController(exportService, attachmentService, $scope, flashMessenger, translator) {
            var self       = this;
            var setByWatch = true;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'templates.id',
                    'templates.content',
                    'reference',
                    'year',
                    'type',
                    'showArbo',
                    'dataArbo',
                    'groupByKeywords',
                    'groupByKeywordsTitles'
                ]
            };

            self.cols = [];

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(id, idClient, type) {
                if (id) {
                    setByWatch = true;
                    exportService.find(id, apiParams).then(function (res) {
                        self.export = res.object;
                        contentToCols(self.export.templates[0].content);
                        self.panelTitle = self.export.name;

                        self.currentTemplate = self.export.templates[0];
                    })
                } else {
                    self.export          = {
                        _rights  : {
                            update: true
                        },
                        reference: 'initial',
                        templates: [{}],
                        type     : type,
                        showArbo : 'no',
                        dataArbo : null,
                        groupByKeywords: false,
                        groupByKeywordsTitles: null
                    };
                    self.currentTemplate = self.export.templates[0];
                }
            };

            self.saveExport = function saveExport() {
                self.errors.fields = {};
                self.loading.save  = true;
                self.save();
            };

            self.save = function save() {
                var isCreation = !self.export.id;

                if (self.export.year == 0) {
                    self.export.year = '';
                }

                self.export.templates[0].content = '';
                for (var i in self.cols) {
                    self.export.templates[0].content += objToVar(self.cols[i]);
                }

                exportService.save(self.export, apiParams).then(function (res) {
                    self.export       = res.object;
                    self.panelTitle   = self.export.name;
                    self.loading.save = false;
                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.export.id;
                        history.replaceState('', '', url);
                    }

                    self.currentTemplate = self.export.templates[0];

                    flashMessenger.success(translator('export_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteExport = function deleteExport() {
                if (confirm(translator('export_question_delete').replace('%1', self.export.name))) {
                    exportService.remove(self.export).then(function (res) {
                        if (res.success) {
                            window.location = '/export';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.initGroupByKeywordsTitles = function () {
                if (self.export.groupByKeywords && !self.export.groupByKeywordsTitles) {
                    self.export.groupByKeywordsTitles = {
                        keywords: [''],
                        parentProjects: ''
                    };
                }
            };

            self.addGroupByKeywordTitleKeyword = function () {
                self.export.groupByKeywordsTitles.keywords.push('');
            };

            self.variablesParams     = {};
            self.configuredVariable  = null;
            self.variableTranslation = null;
            self.configureVariable   = function configureVariable(variable, translation) {
                var $modal = angular.element('#params_' + variable.replace(/(\()(.*)(\))/, '_$2').replace(',', '_'));

                if ($modal.length > 0) {
                    self.configuredVariable  = variable;
                    self.variablesParams     = {};
                    self.variableTranslation = translation;

                    var params = $modal.find('input, select');
                    for (var i = 0; i < params.length; i++) {
                        var name                   = $(params[i]).attr('name');
                        self.variablesParams[name] = null;
                    }

                    $modal.modal('show');
                } else {
                    self.insertVariable(translation + '***' + translation + '|||' + variable);
                    $modal.modal('hide');
                }
            };

            self.insertConfiguredVariable = function insertConfiguredVariable() {
                var variable = self.configuredVariable;
                var params   = [];
                for (var i in self.variablesParams) {
                    var param = self.variablesParams[i];
                    if (angular.isArray(param)) {
                        param = '[' + param.join(',') + ']'
                    }
                    params.push(param || '');
                }
                // Check if the variable has already parameters and if it's the case, it adds the new parameters
                if (variable.substr(variable.length - 1) === ')') {
                    variable = variable.replace(/.$/, ",");
                    self.insertVariable(self.variableTranslation + '***' + self.variableTranslation + '|||' + variable + params.join(',') + ')');
                } else {
                    self.insertVariable(self.variableTranslation + '***' + self.variableTranslation + '|||' + variable + '(' + params.join(',') + ')');
                }
            };

            self.insertVariable = function insertVariable(variable) {
                var obj = varToObj(variable);

                // Add the Type of the Indicator in the name and translation
                if (obj.identifier === 'indicatorDone' || obj.identifier === 'indicatorTarget' || obj.identifier === 'indicatorRatio') {
                    obj.name += ' - ' + $('#params_' + obj.identifier + ' select option[value=' + obj.params + ']').text();
                    obj.translation = obj.name;
                }

                self.cols.push(obj);
            };

            self.deleteCol = function (index) {
                self.cols.splice(index, 1);
            };

            // Transform a variable of the DB to an object for the table
            function varToObj(variable) {
                var obj = {};

                variable = variable.split('|||');

                variable[0]     = variable[0].split('***');
                obj.name        = variable[0][0];
                obj.translation = variable[0][1];

                variable[1]    = variable[1].split('(');
                obj.identifier = variable[1][0];

                obj.params     = null;

                if (typeof variable[1][1] !== 'undefined') {
                    obj.params = variable[1][1].slice(0, -1);
                }

                return obj;
            }

            // Transform an obj of the table to a variable for the DB
            function objToVar(obj) {
                var variable = '$' + obj.name + '***' + obj.translation + '|||' + obj.identifier;

                if (obj.params) {
                    variable += '(' + obj.params + ')';
                }

                return variable + '$';
            }

            function contentToCols(content) {
                content = content.split('$');

                for (var i in content) {
                    if (content[i] !== '') {
                        self.cols.push(varToObj(content[i]));
                    }
                }
            }
        }
    ]);

    app.directive('filterList', function () {
        return {
            link: function (scope, element, attrs) {
                var lists = element.find('ul');

                function filterBy(value) {
                    var listsHidden = 0;
                    for (var i = 0; i < lists.length; i++) {
                        var list           = lists[i];
                        var elements       = lists[i].children;
                        var elementsHidden = 0;
                        for (var j = 0; j < elements.length; j++) {
                            var el       = elements[j];
                            var show     = el.textContent.toLowerCase().indexOf(value.toLowerCase()) !== -1;
                            el.className = show ? '' : 'ng-hide';
                            if (!show) {
                                elementsHidden++;
                            }
                        }

                        if (elementsHidden == elements.length) {
                            $(list).parent().addClass('ng-hide');
                            listsHidden++;
                        } else {
                            $(list).parent().removeClass('ng-hide');
                        }
                    }

                    if (listsHidden == lists.length) {
                        element.addClass('ng-hide');
                    } else {
                        element.removeClass('ng-hide');
                    }
                }

                scope.$watch(attrs.filterList, function (newVal, oldVal) {
                    if (newVal !== oldVal) {
                        filterBy(newVal);
                    }
                });
            }
        };
    });

})(window.EVA.app);
