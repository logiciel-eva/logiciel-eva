(function(app) {
	app.controller('jobController', [
		'jobService',
		'linkService',
		'flashMessenger',
		'translator',
		'$scope',
		'$timeout',
		function jobController(jobService, linkService, flashMessenger, translator, $scope, $timeout) {
			var self = this;

			self.objType = 'structure';
			self.object = $scope.$parent.$parent.self.structure;
			if (typeof self.object === 'undefined') {
				self.objType = 'contact';
				self.object = $scope.$parent.$parent.self.contact;
			}

			self.link = null;
			$scope.links = [];

			for (var i in self.object.links) {
				$scope.links.push(self.object.links[i]);
			}


			self.errors = {
				job: {
					fields: {},
				},
				structure: {
					fields: {},
				},
				contact: {
					fields: {},
				},
			};

			self.loading = {
				save: false,
			};

			/**
			 * Ce watcher permet de savoir si la variable job
			 * a été créée à la main ou récupérée via le select
			 * factory. On regarde si la variable change et si
			 * ce n'est pas un objet, alors on récupère soit
			 * l'id soit le job name.
			 */
			$scope.$watch(
				function() {
					return self.link;
				},
				function(newVal, oldVal) {
					if (self.link != null) {
						newVal = self.link.job;
						if (typeof newVal !== 'object') {
							self.link.job = {};

							if (Number.isInteger(+newVal)) {
								self.link.job.id = newVal;
							} else {
								self.link.job.name = newVal;
							}
						}
					}
				},
				true,
			);

			self.openLinkModal = function openLinkModal(link) {
				if (typeof link !== 'object') {
					link = {
						job: {},
					};
				}

				$timeout(function() {
					var selectize = $('#linkJob selectize')[0].selectize;
					selectize.clearOptions();

					self.link = {
						id: link.id,
						job: link.job,
						structure: link.structure,
						contact: link.contact,
						keywords: link.keywords,
						_rights: {
							update: true
						}
					};

					self.link[self.objType] = self.object;

					self.updatePanel();
				});

				$('#linkModal').modal('show');
			};

			self.saveLink = function saveLink() {
				self.errors.job.fields = {};
				self.loading.save = true;

				var isCreation = !self.link.id;

				var send = self.link;
				send[self.objType].links = [];

				linkService
					.save(send, {
						col: [
							'id',
							'job.id',
							'job.name',
							'structure.id',
							'structure.name',
							'contact.name',
							'contact.id',
							'keywords',
						],
					})
					.then(
						function(res) {
							self.loading.save = false;
							self.link = {};

							if (isCreation) {
								self.object.links.push(res.object);
								$scope.links.push(res.object);
							} else {
								for (var i in $scope.links) {
									if ($scope.links[i].id == res.object.id) {
										$scope.links[i] = res.object;
										break;
									}
								}
							}

							$('#linkModal').modal('hide');
							flashMessenger.success(translator('job_message_saved'));
						},
						function(err) {
							for (var field in err.fields) {
								self.errors.job.fields[field] = err.fields[field];
							}

							self.loading.save = false;
						},
					);
			};

			self.deleteLink = function deleteLink(link) {
				if (confirm(translator('job_question_delete').replace('%1', link.job.name))) {
					linkService.remove(link).then(function(res) {
						angular.forEach($scope.links, function(val, key) {
							if (val.id == link.id) {
								$scope.links.splice(key, 1);
							}
						});
						flashMessenger.success(translator('job_message_deleted'));
					});
				}
			};

			self.jobCreate = function jobCreate(input) {
				var selectize = this,
					exists = false;

				for (var i in selectize.options) {
					var option = selectize.options[i];
					if (option.name == input) {
						selectize.addItem(i);
						exists = true;
						break;
					}
				}

				if (!exists) {
					self.link.job = { name: input };
					return {
						name: input,
						id: input,
					};
				} else {
					return {};
				}
			};

			self.updatePanel = function updatePanel() {
				if (self.link.id) {
					self.panelTitle = self.link.job.name + ' - ' + self.link.contact.name;
				} else {
					self.panelTitle = translator(self.objType + '_job_form');
				}
			};
		},
	]);
})(window.EVA.app);
