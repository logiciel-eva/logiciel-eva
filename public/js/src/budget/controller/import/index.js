(function (app) {

    app.controller('importBudgetController', [
        'importConfigurationService', 'FileUploader', '$rootScope', '$scope', '$http', 'flashMessenger', 'translator', '$controller',
        function importBudgetController(importConfigurationService, FileUploader, $rootScope, $scope, $http, flashMessenger, translator, $controller) {
            var self = this;

            self.url = '';

            self.entity  = null;
            self.loading = {
                upload: false,
                parse: false,
                validate: false,
                save: false
            };
            self.options = {
                delimiter: ';',
                firstLine: false,
                file: null,
                isParsed: false,
                isValidated: false,
            };
            self.importConfiguration = {
                path: null,
                frequency: null,
                queryId: false,
            };
            self.data = {};
            self.uploader = new FileUploader();
            self.reader   = new FileReader();

            self.inPosteAccountParent = false;

            self.uploader.filters.push({
                name: 'csvFilter',
                fn: function(item) {
                    //var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    var type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase() + '|';
                    return '|csv|vnd.ms-excel|'.indexOf(type) !== -1;
                }
            });
            self.uploader.onAfterAddingFile = function(fileItem) {
                self.loading.upload = true;
                self.reader.readAsDataURL(fileItem._file);
            };
            self.uploader.onWhenAddingFileFailed  = function(fileItem) {
                self.loading.upload      = false;
                self.options.isParsed    = false;
                self.options.isValidated = false;
                self.options.file        = null;
            };


            self.reader.onloadend = function(e) {
                $scope.$apply(function() {
                    self.options.file = e.target.result;
                    self.loading.upload      = false;
                    self.options.isParsed    = false;
                    self.options.isValidated = false;
                });
            };

            self.parse = function parse() {
                self.loading.parse = true;
                self.errors        = [];
                $http({
                    method: 'POST',
                    url: self.url + '/parse',
                    data: self.options
                }).then(function(res) {
                    if (res.data.success) {
                        self.options.isParsed    = true;
                        self.options.isValidated = false;
                        self.lines               = res.data.lines;
                        self.properties          = res.data.properties;
                    } else {
                        flashMessenger.error(translator('import_message_file_not_valid'))
                    }

                    self.loading.parse = false;
                }, function(err) {
                    self.loading.parse = false;
                    flashMessenger.error(translator('import_message_file_not_valid'))
                });
            };

            self.validate = function validate() {
                self.loading.validate = true;
                self.errors           = [];
                $http({
                    method: 'POST',
                    url: self.url + '/validate',
                    data: {
                        options: {
                            type: self.type
                        },
                        properties: self.properties,
                        lines: self.lines
                    }
                }).then(function(res) {
                    self.options.isValidated = true;

                    if (res.data.success) {

                    } else {
                        self.errors = res.data.errors;
                    }

                    self.loading.validate = false;
                }, function(err) {
                    self.loading.validate = false;
                });
            };

            self.addLine = function addLine() {
                var line = [];
                var errors = [];
                for (var i in self.properties) {
                    line.push(null);
                    errors.push(null);
                }

                self.lines.push(line);
                self.errors.push(errors);
            };

            self.addColumn = function addColumn() {
                self.properties.push(null);
                for (var i in self.lines) {
                    self.lines[i].push(null);
                    self.errors[i].push(null);
                }
            };

            self.deleteLine = function deleteLine(index) {
                if (confirm(translator('import_question_delete_line'))) {
                    self.lines.splice(index, 1);
                    self.errors.splice(index, 1);
                }
            };

            self.save = function save() {
                if (confirm(translator('import_question_lets_save').replace('%1', self.lines.length))) {
                    self.loading.save = true;
                    $http({
                        method: 'POST',
                        url: self.url + '/save',
                        data: {
                            options: {
                                type: self.type,
                                mode: self.mode,
                                year: self.year,
                                status: self.status,
                                inPosteAccountParent: self.inPosteAccountParent,
                            },
                            properties: self.properties,
                            lines: self.lines
                        }
                    }).then(function (res) {
                        self.errors = res.data.errors;

                        var saved = 0;
                        var total = 0;
                        var ok    = [];
                        for (var i = 0; i < self.lines.length; i++) {
                            if (!!!res.data.errors[i] && !!!res.data.exceptions[i]) {
                                ok.push(i);
                                saved++;
                            }
                            total++;
                        }

                        var deleted = 0;
                        for (var i = 0; i < ok.length; i++) {
                            var index = ok[i] -= deleted;
                            self.lines.splice(index, 1);
                            self.errors.splice(index, 1);

                            deleted++;
                        }

                        self.loading.save = false;
                        alert(translator('import_message_lines_saved').replace('%1', total).replace('%2', saved));
                    }, function (err) {
                        self.loading.save = false;
                    });
                }
            };

            self.canSaveImportConfiguration = function canSaveImportConfiguration(){
                //return (self.options.isValidated === true && self.errors.length === 0 && self.importConfiguration.queryId !== false);
                return true;
            };

            self.saveImportConfiguration = function saveImportConfiguration() {
                let row  = {
                    budgetEntity: self.entity,
                    budgetType: self.type,
                    importType: self.mode,
                    year: self.year,
                    budgetStatus: (self.status !== undefined) ? {id: self.status} : null,
                    inPosteAccountParent: (self.inPosteAccountParent === true) ? 1: 0,
                    delimiter: self.options.delimiter,
                    ignoreFirstLine: (self.options.firstLine === true) ? 1: 0,
                    path: self.importConfiguration.path,
                    frequency: self.importConfiguration.frequency,
                    query: {id: self.importConfiguration.queryId}
                };

                let apiParams = {
                    col: [
                        'budgetEntity',
                        'budgetType',
                        'importType',
                        'year',
                        'budgetStatus',
                        'inPosteAccountParent',
                        'delimiter',
                        'ignoreFirstLine',
                        'path',
                        'frequency',
                        'query.name',
                        'lastImportAt',
                        'createdAt',
                        'updatedAt'
                    ]
                };

                importConfigurationService.save(row, apiParams).then(function() {
                    window.location.href = '/budget/import-configuration';
                });
            };

            $rootScope.$on('budget-import-table-builder--query-loaded', function (e, filters) {
                self.importConfiguration.queryId = filters.id;
            });

            $scope.$watch(function () { return self.entity }, function () {
                self.url = '/budget/import/' + self.entity;
                self.lines      = [];
                self.errors     = [];
                self.properties = [];
                self.loading.upload      = false;
                self.options.isParsed    = false;
                self.options.isValidated = false;
            });

            self.disableOption = function (index, option) {
                return (self.properties.indexOf(option) > -1 && self.properties.indexOf(option) !== index);
            };
        }
    ]);

})(window.EVA.app);
