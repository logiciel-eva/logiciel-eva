<?php

namespace Bootstrap\ORM;

use Bootstrap\Entity\Client;
use Doctrine\ORM\EntityManager;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Class EntityManagerFactory
 *
 * Responsible to create/returns the default Doctrine Entity Manager based or the client specific one.
 * See DynamicEntityManager.
 *
 * @package Bootstrap\ORM
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class EntityManagerFactory
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @param Client|null $client
     * @return EntityManager
     */
    public function getEntityManager(Client $client = null, $otherClient = false)
    {
        $em = $this->serviceLocator->get('DefaultEntityManager');

        if ($client !== null) {
            $dynamicEntityManager =$this->serviceLocator->get('DynamicEntityManager');
            $em = $dynamicEntityManager->create($client, $otherClient);
        }

        return $em;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }
}
