<?php

namespace Indicator\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Keyword\Entity\Association;
use Keyword\Entity\Group;

class DeploymentController extends BasicRestController
{
    protected $entityClass  = 'Indicator\Entity\Deployment';
    protected $repository   = 'Indicator\Entity\Deployment';
    protected $entityChain  = 'indicator:e:indicator';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        if (isset($data['filters'])) {
            foreach ($data['filters'] as $property => $filter) {
                if ($property === 'id') {
                    if ($filter['op'] === 'project') {
                        $project = $filter['val'];

                        $associations = $this->getEntityManager()->getRepository('Keyword\Entity\Association')->findBy([
                            'entity'  => 'Project\Entity\Project',
                            'primary' => $project,
                        ]);
                        $filter = [];
                        /** @var Association $association */
                        foreach ($associations as $association) {
                            if ($association->getKeyword()->getGroup()->getType() === Group::GROUP_TYPE_REFERENCE) {
                                if (!isset($filter[$association->getKeyword()->getGroup()->getId()])) {
                                    $filter[$association->getKeyword()->getGroup()->getId()] = [];
                                }
                                $levels = $association->getKeyword()->getLevels();
                                foreach ($levels as $level) {
                                    $filter[$association->getKeyword()->getGroup()->getId()][] = $level;
                                }
                            }
                        }

                        $i = 0;
                        foreach ($filter as $group => $levels) {
                            $queryBuilder->orWhere('object.group = :group'.$i . ' AND object.level IN (:levels'.$i.')');
                            $queryBuilder->setParameter('group'.$i, $group);
                            $queryBuilder->setParameter('levels'.$i, $levels);
                            $queryBuilder->groupBy('object.indicator');
                            $i++;
                        }

                        if (!$filter) {
                            $queryBuilder->andWhere('FALSE = TRUE');
                        }

                        unset($data['filters'][$property]);
                        continue;
                    }
                }
            }
        }
    }
}
