<?php

namespace Attachment\Core;

class Uploader
{
    public static function uploadFromBase64($data, $client)
    {
        $dirName = str_replace('\\', '-', $data['entity']);
        $dirName = strtolower($dirName);
        self::createDirs($dirName, $data['primary'], $client);

        $path         = '/clients/' . $client->getKey(). '/attachments/' . $dirName . '/' . $data['primary'] . '/';
        $completePath = getcwd() . '/public' . $path;

        $data['filename'] = str_replace(' ', '-', $data['filename']);

        if (file_exists($completePath . $data['filename'])) {
            $i = 0;
            $ext = pathinfo($data['filename'], PATHINFO_EXTENSION);

            do  {
                $i++;
                $fileName = basename($data['filename'], ".$ext") . '-' . $i . '.' . $ext;
            } while (file_exists($completePath . $fileName));

            $data['filename'] = $fileName;
        }

        $filePath = $path . self::fileSystemEncode($data['filename']);
        $filePath = str_replace('?', ' ', $filePath);

        touch(getcwd() . '/public' . $filePath);

        $file   = fopen(getcwd() . '/public' . $filePath, 'wb');

        $base64 = explode(',', $data['file']);
        $base64Data = $base64[0];
        if(count($base64)>1){
            $base64Data = $base64[1];
        }
        fwrite($file, base64_decode($base64Data));
        fclose($file);

        return $filePath;
    }

    public static function uploadFromUrl($data, $client)
    {
        $dirName = str_replace('\\', '-', $data['entity']);
        $dirName = strtolower($dirName);
        self::createDirs($dirName, $data['primary'], $client);

        $path         = '/clients/' . $client->getKey(). '/attachments/' . $dirName . '/' . $data['primary'] . '/';
        $completePath = getcwd() . '/public' . $path;

        $data['filename'] = str_replace(' ', '-', $data['filename']);

        if (file_exists($completePath . $data['filename'])) {
            $i = 0;
            $ext = pathinfo($data['filename'], PATHINFO_EXTENSION);

            do  {
                $i++;
                $fileName = basename($data['filename'], ".$ext") . '-' . $i . '.' . $ext;
            } while (file_exists($completePath . $fileName));

            $data['filename'] = $fileName;
        }

        $filePath = $path . self::fileSystemEncode($data['filename']);
        $filePath = str_replace('?', ' ', $filePath);

        touch(getcwd() . '/public' . $filePath);

        copy(str_replace(' ', '%20', $data['url']), getcwd() . '/public' . $filePath);

//         $file   = fopen(getcwd() . '/public' . $filePathEnc, 'wb');
//         fwrite($file, file_get_contents(urlencode($data['url'])));
//         fclose($file);

        return $filePath;
    }

    protected static function createDirs($entity, $primary, $client)
    {
        $clientDir = $client->getFolder();
        if (!file_exists($clientDir)) {
            mkdir($clientDir, 0777);
        }

        $attachmentsDir = $clientDir . '/attachments';
        if (!file_exists($attachmentsDir)) {
            mkdir($attachmentsDir, 0777);
        }

        $entitiesDir = $attachmentsDir . '/' . $entity;
        if (!file_exists($entitiesDir)) {
            mkdir($entitiesDir, 0777);
        }

        $entityDir = $entitiesDir . '/' . $primary;
        if (!file_exists($entityDir)) {
            mkdir($entityDir, 0777);
        }
    }

    protected static function fileSystemEncode($fileName)
    {

        $fileName = urldecode($fileName);

        if(substr(strtolower(PHP_OS), 0, 3) === 'win') {
            $fileName = mb_convert_encoding($fileName, 'ISO-8859-1', 'UTF-8');
        }

        return $fileName;
    }
}
