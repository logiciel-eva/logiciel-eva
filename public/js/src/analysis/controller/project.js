(function(app) {

    app.controller('projectController', [
        '$scope',
        function projectController($scope) {
            var self = this;

            self.init = function init(isMaster) {
                $scope.isMaster = isMaster;
            };
        }
    ])

})(window.EVA.app);
