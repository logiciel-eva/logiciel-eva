(function(app) {

    app.controller('conventionLineFormController', [
        'conventionLineService', 'flashMessenger', 'translator', '$scope', '$filter',
        function conventionLineFormController(conventionLineService, flashMessenger, translator, $scope, $filter) {
            var self = this;

            var $modal = angular.element('#conventionLineFormModal');
            var apiParams = {
                col: [
                    'id',
                    'percentage',
                    'project.id',
                    'project.code',
                    'project.name',
                    'project.financerRepartitionIncomeArboRequested',
                    'project.timeSpent',
                    'project.amountIncomeAutofinancementArboRequested',
                    'project.amountIncomeArboRequested',
                    'project.financers.id',
                    'project.financers.name',
                    'convention.id'
                ]
            };

            self.line = null;

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.from = {
                type: null,
                id: null,
                object: null
            };

            self.init = function init(from, id, expenseTypes) {
                for (var i in expenseTypes) {
                    apiParams.col.push('project.amountExpenseArbo' + expenseTypes[i].ucfirst());
                    apiParams.col.push('project.amountHTExpenseArbo' + expenseTypes[i].ucfirst());
                }

                self.from.type = from;
                self.from.id   = id;
            };

            self.saveLine = function saveLine() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.line.id;

                conventionLineService.save(self.line, apiParams).then(function(res) {
                    if (isCreation) {
                        if (self.from.type == 'convention') {
                            if (!angular.isArray(self.from.object.lines)) {
                                self.from.object.lines = [];
                            }
                            self.from.object.lines.push(res.object);
                        }
                    } else {

                    }

                    if (self.from.type == 'convention') {
                        if (!angular.isArray(self.from.object.financers)) {
                            self.from.object.financers = [];
                        }
                        for (var i in res.object.project.financers) {
                            if($filter('filter')(self.from.object.financers, function(item) {
                                return item.id === res.object.project.financers[i].id; 
                            }).length == 0) {
                                self.from.object.financers.push(res.object.project.financers[i]);
                            }
                        }
                    }

                    flashMessenger.success(translator('convention_line_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            $scope.$on('create-convention-line', function (event, params) {
                self.line = {
                    _rights: {
                        update: true
                    }
                };

                if (self.from.type == 'convention') {
                    self.line.convention = params.convention.id;
                    self.from.object     = params.convention;
                }

                self.panelTitle = translator('convention_line_nav_form');
                $modal.modal('show');
            });

            $scope.$on('edit-convention-line', function (event, params) {

                if (self.from.type == 'convention') {
                    self.line.convention = params.convention.id;
                    self.from.object     = params.convention;
                }

                $modal.modal('show');
            });
        }
    ]);

})(window.EVA.app);
