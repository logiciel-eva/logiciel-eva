<?php

namespace Budget\View\Helper;

use Budget\Entity\Account;
use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class PosteExpenseFilters extends EntityFilters
{
	public function __construct(
		Translate $translator,
		MyModuleManager $moduleManager,
		EntityManager $entityManager,
        $allowedKeywordGroups
	) {
		parent::__construct($translator, $moduleManager, $entityManager);

		$filters = [
			'name' => [
				'label' => $this->translator->__invoke('poste_expense_field_name'),
				'type' => 'string',
			],
			'amount' => [
				'label' => $this->translator->__invoke('poste_expense_field_amount'),
				'type' => 'number',
			],
			'account' => [
				'label' => $this->translator->__invoke('poste_expense_field_account'),
				'type' => 'account-select',
				'accountType' => Account::TYPE_EXPENSE,
			],
			'project' => [
				'label' => $this->translator->__invoke('poste_expense_field_project'),
				'type' => 'project-select',
			],
			'createdAt' => [
				'label' => $this->translator->__invoke('poste_expense_field_createdAt'),
				'type' => 'date',
			],
			'updatedAt' => [
				'label' => $this->translator->__invoke('poste_expense_field_updatedAt'),
				'type' => 'date',
			],
		];

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'ht')) {
			$filters['amountHT'] = [
				'label' => $this->translator->__invoke('poste_expense_field_amountHT'),
				'type' => 'number',
			];
		}

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'gbcp')) {
			$filters['nature'] = [
				'label' => $this->translator->__invoke('poste_expense_field_nature'),
				'type' => 'nature-select',
			];
			$filters['organization'] = [
				'label' => $this->translator->__invoke('poste_expense_field_organization'),
				'type' => 'organization-select',
			];
			$filters['destination'] = [
				'label' => $this->translator->__invoke('poste_expense_field_destination'),
				'type' => 'destination-select',
			];
		}

		if ($this->moduleManager->isActive('keyword')) {
			$groups = $allowedKeywordGroups('posteExpense', false);;

			foreach ($groups as $group) {
				$filters['keyword.' . $group->getId()] = [
					'label' => $group->getName(),
					'type' => 'keyword-select',
					'group' => $group->getId(),
				];
			}
		}

		$this->filters = $filters;
	}
}
