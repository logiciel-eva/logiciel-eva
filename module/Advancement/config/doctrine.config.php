<?php

namespace Advancement;

return [
    'doctrine' => [
        'driver' => [
            'advancement_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Advancement\Entity' => 'advancement_entities'
                ]
            ]
        ]
    ]
];
