<?php
namespace Time\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

/**
 * Class AbsenceTypeController
 * @package Time\Controller\API
 */
class AbsenceTypeController extends BasicRestController
{
    protected $entityClass = 'Time\Entity\AbsenceType';
    protected $repository  = 'Time\Entity\AbsenceType';
    protected $entityChain = 'time:e:absence-type';

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
