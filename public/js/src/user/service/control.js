(function(app) {

    app.factory('controlService', [
        '$http', '$compile', '$rootScope',
        function controlService($http, $compile, $rootScope) {

            // Repeat every 15 min
            var interval = 15 * 60 * 1000;
            var run = true;

            var start = function start() {
                run = true;
                control();
            };

            var control = function control() {
                if (run) {
                    $http.get('/user/auth/control').then(function(response) {
                        if (response.data.control === false && run) {
                            $http.get('/user/auth/login?partial=true').then(function(response) {
                                $('body').append($compile('<div class="partial-login">' + response.data + '</div>')($rootScope.$new()));
                                $('.partial-login input:first').focus();
                            })
                        } else {
                            setTimeout(control, interval)
                        }
                    });
                }
            };

            return {
                start: start,
                desactivate: function() {
                    run = false;
                }
            }
        }
    ])

})(window.EVA.app);