(function(app) {

    app.factory('structureService', [
        'restFactoryService', '$http', '$q',
        function structureService(restFactoryService, $http, $q) {
            var url = '/api/directory/structure';

            var methods = restFactoryService.create(url);

            methods.fusion = function (structureToKeep_id, structureToFusion_id, type) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: url + '/' + structureToKeep_id + '/fusion',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ structureToFusion_id : structureToFusion_id, type: type })
                }).then(function(res) {
                    deferred.resolve(res.data)
                }, function(err) {
                    deferred.reject(err)
                });

                return deferred.promise;
            };

            return methods;
        }
    ])
})(window.EVA.app);