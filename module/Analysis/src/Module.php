<?php

namespace Analysis;

use Core\Module\AbstractModule;
use Analysis\View\Helper\MeasureFilters;
use Analysis\Service\TimeAnalysisService;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{


    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'measureAnalysisFilters'   => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    $client              = $serviceManager->get('Environment')->getClient();
                    return new MeasureFilters($client, $translateViewHelper, $moduleManager, $entityManager);
                }
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'timeAnalysisService' => function (ServiceManager $serviceManager) {
                    $environment = $serviceManager->get('Environment');
                    return new TimeAnalysisService(
                        $serviceManager,
                        $serviceManager->get('EntityManagerFactory'),
                        $serviceManager->get('simpleFiltersHelper'),
                        $serviceManager->get('AuthStorage'),
                    );
                },
            ]
        ];
    }
}
