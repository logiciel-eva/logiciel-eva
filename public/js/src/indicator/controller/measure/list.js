(function(app) {

    app.controller('measureListController', [
        'measureService', 'campain-measureService', '$scope', 'flashMessenger', 'translator', '$filter', '$timeout', '$rootScope',
        function measureListController(measureService, campainMeasureService, $scope, flashMessenger, translator, $filter, $timeout, $rootScope) {
            var self = this;

            self.from = {
                type: null,
                id: null
            };
            self.object = null;

            self.measures = [];
            self.options = {
                name: 'measures-list',
                col: [
                    'id',
                    'period',
                    'type',
                    'start',
                    'date',
                    'value',
                    'campainMeasure.id',
                    'campainMeasure.supposedDate',
                    'campainMeasure.campain.id',
                    'campainMeasure.indicator.id',
                    'campainMeasure.realiseMeasure.id',
                    'campainMeasure.realiseMeasure.fixedValue',
                    'campainMeasure.realiseMeasure.value',
                    'campainMeasure.realiseMeasure.type',
                    'campainMeasure.realiseMeasure.comment',
                    'campainMeasure.realiseMeasure.source',
                    'campainMeasure.realiseMeasure.groupIndicator.id',
                    'campainMeasure.realiseMeasure.groupIndicator.name',
                    'campainMeasure.realiseMeasure.indicator.id',
                    'campainMeasure.realiseMeasure.indicator.name',
                    'campainMeasure.realiseMeasure.indicator.type',
                    'campainMeasure.realiseMeasure.indicator.values',
                    'campainMeasure.realiseMeasure.indicator.groupIndicators.id',
                    'campainMeasure.realiseMeasure.indicator.groupIndicators.name',
                    'campainMeasure.realiseMeasure.project.id',
                    'campainMeasure.realiseMeasure.project.name',
                    'campainMeasure.realiseMeasure.territories',
                    'campainMeasure.realiseMeasure.start',
                    'campainMeasure.realiseMeasure.date',
                    'campainMeasure.realiseMeasure.updatedAt',
                    'campainMeasure.targetMeasure.id',
                    'campainMeasure.targetMeasure.value',
                    'campainMeasure.targetMeasure.type',
                    'campainMeasure.targetMeasure.comment',
                    'campainMeasure.targetMeasure.source',
                    'campainMeasure.targetMeasure.fixedValue',
                    'campainMeasure.targetMeasure.groupIndicator.id',
                    'campainMeasure.targetMeasure.groupIndicator.name',
                    'campainMeasure.targetMeasure.indicator.id',
                    'campainMeasure.targetMeasure.indicator.name',
                    'campainMeasure.targetMeasure.indicator.type',
                    'campainMeasure.targetMeasure.indicator.values',
                    'campainMeasure.targetMeasure.indicator.groupIndicators.id',
                    'campainMeasure.targetMeasure.indicator.groupIndicators.name',
                    'campainMeasure.targetMeasure.project.id',
                    'campainMeasure.targetMeasure.project.name',
                    'campainMeasure.targetMeasure.territories',
                    'campainMeasure.targetMeasure.start',
                    'campainMeasure.targetMeasure.date',
                    'campainMeasure.targetMeasure.updatedAt',
                    'fixedValue',
                    'territories',
                    'project.id',
                    'project.name',
                    'campain.id',
                    'campain.name',
                    'comment',
                    'source',
                    'transverse',
                    'client',
                    'master',
                    'slave',
                    'createdAt',
                    'updatedAt',
                    'indicator.name'
                ],
                searchType: 'list',
                sort: 'date',
                order: 'asc',
                pager: false,
                getItems: function getData(params, callback, abort) {
                    if ($scope.$parent.self[self.from.type].id || self.from.id) {
                        if(self.from.type == 'indicator'){
                            params.search.data.filters['indicator.id'] = {
                                op: 'eq',
                                val: $scope.$parent.self[self.from.type].id || self.from.id
                            };
                        } else if(self.from.type == 'project'){
                            params.search.data.filters['project.id'] = {
                                op: 'eq',
                                val: $scope.$parent.self[self.from.type].id || self.from.id
                            };
                        }
                        measureService.findAll(params, abort.promise).then(function(res) {
                            callback(res);
                        })
                    } else {
                        callback({ rows: [] });
                    }
                }
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id = id;

                if (self.from.type == 'project') {
                    self.options.col.push('indicator.id');
                    self.options.col.push('indicator.name');
                    self.options.col.push('indicator.type');
                    self.options.col.push('indicator.values');
                    self.options.col.push('indicator.operator');
                    self.options.col.push('indicator.description');
                    self.options.col.push('indicator.method');
                    self.options.col.push('indicator.definition');
                }
            };

            self.editMeasure = function editMeasure(measure) {
                if (self.from.type == 'indicator') {
                    measure.indicator = $scope.$parent.self[self.from.type];
                }
                if(null !== measure.campainMeasure) {
                    measure.campainMeasure._rights = measure._rights;   // hack to make modale editable (not the good rights)
                    $scope.$broadcast('edit-measure-campain', measure.campainMeasure);
                    $scope.$broadcast('edit-attachement', measure.campainMeasure.id);
                } else {
                    $scope.$broadcast('edit-measure', measure);
                    $scope.$broadcast('edit-attachement', measure.id);
                }
            };

            self.createMeasure = function createMeasure() {
                $scope.$broadcast('create-measure');
            };

            self.deleteMeasure = function deleteMeasure(measure) {
                var deletionMessage = (null !== measure.campainMeasure) ? 'campain_measure_question_delete' : 'measure_question_delete' ;
                if (confirm(translator(deletionMessage))) {
                    measureService.remove(measure).then(function(res) {
                        if (res.success) {
                            $scope.__tb.loadData();
                            flashMessenger.success(translator('measure_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(measure) {
                if (self.from.type == 'indicator') {
                    measure.indicator = $scope.$parent.self[self.from.type];
                }
                self.editedLineErrors = {};
                self.editedLine = measure;
                self.editedLineFields = angular.copy(measure);
            };

            self.saveEditedLine = function saveEditedLine(measure) {
                self.editedLineErrors = {};
                measureService.save(angular.extend({}, measure, self.editedLineFields), {
                    col: self.options.col
                }).then(function(res) {
                    if (res.success) {
                        measure = angular.extend(measure, res.object);
                        self.editedLine = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('measure_message_saved'));
                    }
                }, function(err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.getRatio = function(indicator, measures) {
                var done = self.getValue(indicator, measures, 'done');
                var target = self.getValue(indicator, measures, 'target');

                return (target > 0 ? (done / target) * 100 : 0);
            };

            self.getValue = function getValue(indicator, measures, type) {
                var value = null;
                var values = [];

                for (var i in measures) {
                    if (measures[i].type == type) {
                        values.push(measures[i].value);
                    }
                }

                switch (indicator.operator) {
                    case 'sum':
                        var sum = 0;
                        for (var _i = 0; _i < values.length; _i++) {
                            sum += values[_i];
                        }

                        value = sum;
                        break;
                    case 'avg':
                        var sum = 0;
                        for (var _i = 0; _i < values.length; _i++) {
                            sum += values[_i];
                        }

                        value = sum / values.length;
                        break;
                    case 'med':
                        var count = values.length;
                        var middle = Math.floor(count / 2);
                        values.sort(function(a, b) {
                            return a - b;
                        });
                        var median = values[middle];
                        if (count % 2 == 0) {
                            median = (median + values[middle - 1]) / 2;
                        }
                        value = median;
                        break;
                    case 'max':
                        var max = null;
                        for (var _i = 0; _i < values.length; _i++) {
                            if (max == null || values[_i] > max) {
                                max = values[_i];
                            }
                        }

                        value = max;
                        break;
                    case 'min':
                        var min = null;
                        for (var _i = 0; _i < values.length; _i++) {
                            if (min == null || values[_i] < min) {
                                min = values[_i];
                            }
                        }

                        value = min;
                        break;
                }

                return value;
            };

            self.isManager = function isManager(user) {
                for (var i in $scope.$parent.self.project.members) {
                    var member = $scope.$parent.self.project.members[i];
                    if (member.id && member.user.id == user && member.role == 'manager') {
                        return true;
                    }
                }

                return false;
            };


            self.charts = [];
            self.loadCharts = function() {

                self.charts = [];

                var filteredMeasures = [];
                for (let i= 0; i<self.measures.length; i++) {
                    if (!!self.measures[i].date) {
                        filteredMeasures.push(self.measures[i])
                    }
                }

                var grouped = $filter('groupBy')(filteredMeasures, 'indicator.name');
                for (var indicator in grouped) {
                    var chart = {
                        options: {
                            chart: {
                                type: 'lineChart',
                                height: 400,
                                useInteractiveGuideline: true,
                                x: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.label;
                                    }
                                    return null;
                                },
                                y: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.value;
                                    }
                                    return null;
                                },
                                xAxis: {
                                    tickFormat: function(d) {
                                        return moment(d, 'X').format('DD/MM/YYYY')
                                    }
                                }
                            },
                            title: {
                                enable: true,
                                text: indicator
                            }
                        },
                        api: null,
                        data: [{
                            values: [],
                            key: 'Prévu',
                            color: '#c3c3c3',
                        },
                            {
                                values: [],
                                key: 'Réalisé',
                                color: '#97bbcd'
                            }
                        ]
                    };

                    var measures = $filter('groupBy')(grouped[indicator], function(measure) {
                        return moment(measure.date, 'DD/MM/YYYY HH:mm').format('X')
                    });

                    var lastTarget = null;
                    var lastDone = null;
                    for (var time in measures) {
                        var target = null;
                        var done = null;

                        for (var j in measures[time]) {
                            var measure = measures[time][j];
                            if (measure.type == 'done' && (done == null || measure.value > done.value)) {
                                done = measure;
                            }

                            if (measure.type == 'target' && (target == null || measure.value > target.value)) {
                                target = measure;
                            }
                        }

                        if (target) {
                            lastTarget = target;
                        }

                        if (done) {
                            lastDone = done;
                        }


                        chart.data[0].values.push({
                            value: lastTarget ? lastTarget.value : 0,
                            label: parseInt(time)
                        });


                        chart.data[1].values.push({
                            value: lastDone ? lastDone.value : 0,
                            label: parseInt(time)
                        });
                    }

                    self.charts.push(chart);
                }
            };

            $scope.$watch(function() { return self.measures }, function() {
                self.loadCharts();
            }, true);

            $scope.$watch(function() { return $scope.showChart }, function() {
                self.loadCharts();
                $timeout(function() {
                    for (var i in self.charts) {
                        self.charts[i].api.refresh();
                    }
                });
            });

            $rootScope.$on('filter-transverse-measure', function(event, data) {
                $scope.load({
                    filters: {
                        transverse: {
                            op: 'eq',
                            val: '1'
                        },
                        indicator: {
                            op: 'eq',
                            val: data.id
                        }
                    }
                });
            });
        }
    ])

})(window.EVA.app);