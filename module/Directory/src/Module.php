<?php

namespace Directory;

use Core\Module\AbstractModule;
use Core\Utils\RequestUtils;
use Directory\Event\ContactSubscriber;
use Directory\View\Helper\ContactFilters;
use Directory\View\Helper\StructureFilters;
use Directory\View\Helper\JobFilters;
use Doctrine\Common\EventManager;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        $request     = $application->getRequest();

        if (!RequestUtils::isCommandRequest($request)) {
            /** @var EventManager $eventManager */
            $eventManager = $application
                ->getServiceManager()
                ->get('Environment')
                ->getEntityManager()
                ->getEventManager();

            $eventManager->addEventsubscriber(new ContactSubscriber());
        }
    }

    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories'  => [
                'structureFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $allowedFieldGroupsViewHelper = $helperPluginManager->get('allowedFieldGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    return new StructureFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper, $allowedFieldGroupsViewHelper );
                },
                'contactFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $allowedFieldGroupsViewHelper = $helperPluginManager->get('allowedFieldGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();

                    return new ContactFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper, $allowedFieldGroupsViewHelper);
                },
                'jobFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();

                    return new JobFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                },
            ],
        ];
    }
}
