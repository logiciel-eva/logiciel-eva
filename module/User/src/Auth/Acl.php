<?php

namespace User\Auth;

use Bootstrap\Environment\Environment;
use Core\Entity\BasicRestEntityAbstract;
use Core\Module\MyModuleManager;
use Project\Entity\Project;
use User\Entity\User;

/**
 * Class Acl
 *
 * @package User\Auth
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class Acl
{
    /**
     * @var MyModuleManager
     */
    protected $moduleManager;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Environment
     */
    protected $environment;

    /**
     * @var boolean
     */
    protected $isMaster;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param MyModuleManager $moduleManager
     * @param User|null $user
     */
    public function __construct(MyModuleManager $moduleManager, Environment $environment, User $user = null)
    {
        $this->moduleManager = $moduleManager;
        $this->user          = $user;
        $this->environment   = $environment;
        $this->isMaster      = false;

        if ($this->environment->getName() == 'client') {
            $this->isMaster = $this->environment->getClient()->isMaster();
        }
    }

    /**
     * @param $rightChain
     * @return bool
     */
    public function isAllowed($rightChain)
    {
        if ($this->user) {
            $moduleKey = $this->explodeRightChain($rightChain)['module'];
            if ($this->moduleManager->isActive($moduleKey)) {
                $role = $this->user->getRole();
                if (array_key_exists($rightChain, $role->getRights())) {
                    return $role->getRights()[$rightChain]['value'] === true;
                }
            }
        }
        return false;
    }

    /**
     * @param $rightChain
     * @param BasicRestEntityAbstract|null $entity
     * @return bool
     */
    public function ownerControl($rightChain, BasicRestEntityAbstract $entity = null)
    {
        $rights = $this->user->getRole()->getRights();
        $owner  = isset($rights[$rightChain]['owner']) ? $rights[$rightChain]['owner'] : 'all';
        $crudAction = $this->explodeRightChain($rightChain)['action'];

        if ($this->explodeRightChain($rightChain)['module'] === 'force') {
            return true;
        }

        // le cas ou on update les bd avec le superadmin
        if ($this->environment->getName() === 'admin' && $this->environment->getClient() !== null) {
            return true;
        }

        if (method_exists($entity, 'getMaster') && strpos($rightChain, 'admin:e:client') === false 
            && strpos($rightChain, 'admin:e:network') === false && strpos($rightChain, 'indicator:e:indicator:r') === false) {
            
            if ($entity->getMaster() && !$this->isMaster) {
                return false;
            }
        }

        if ($owner === 'all') {
            return $this->isAllowed($rightChain) && $this->conditionControl($this->user, $entity, $rightChain);
        }

        return $this->isAllowed($rightChain) && $entity->ownerControl($this->user, $crudAction, $owner, $this->environment->getEntityManager()) && $this->conditionControl($this->user, $entity, $rightChain);
    }

    /**
     * @param string $rightChain
     * @return bool
     */
    public function onlyOwned($rightChain)
    {
        return $this->getOwningRight($rightChain) !== false;
    }

    public function getOwningRight($rightChain)
    {
        $rights = $this->user->getRole()->getRights();
        if (isset($rights[$rightChain]['owner'])) {
            if ($rights[$rightChain]['owner'] === 'own') {
                return $rights[$rightChain]['owner'];
            }
        }

        return false;
    }

    public function getStatusRight($rightChain)
    {
        $rights = $this->user->getRole()->getRights();
        if (isset($rights[$rightChain]['status'])) {
            foreach ($rights[$rightChain]['status'] as $right) {
                if (in_array($right, Project::getPublicationStatutes())) {
                    return $rights[$rightChain]['status'];
                }
            }
        }

        return false;
    }
    /**
     * @param string $entityChain
     * @param string $id
     * @return bool
     */
    public function basicFormAccess($entityChain, $id)
    {
        $canRead   = $this->isAllowed($entityChain . ':r');
        $canUpdate = $this->isAllowed($entityChain . ':u');
        $canCreate = $this->isAllowed($entityChain . ':c');

        if ($id !== false && $canUpdate) {
            return true;
        }

        if ($id === false && $canCreate) {
            return true;
        }

        return ($canRead && $id !== false);
    }

    /**
     * @param string $rightChain
     * @return array
     */
    protected function explodeRightChain($rightChain)
    {
        $rightChainArray = explode(':', $rightChain);

        return [
            'module' => $rightChainArray[0],
            'type'   => $rightChainArray[1],
            'entity' => $rightChainArray[2],
            'action' => $rightChainArray[3],
        ];
    }

    protected function conditionControl($user, $entity, $rightChain)
    {
        if ($entity && $user) {
            $exploded = $this->explodeRightChain($rightChain);
            $module = $this->moduleManager->getModule($exploded['module']);

            $aclConfiguration = $module->getAclconfiguration();
            if (!isset($aclConfiguration['entities']) || !$aclConfiguration['entities']) {
                return true;
            }

            foreach ($aclConfiguration['entities'] as $entityConfiguration) {
                if ($entityConfiguration['name'] !== $exploded['entity']) {
                    continue;
                }

                if (!isset($entityConfiguration['conditions']) || !is_array($entityConfiguration['conditions']) || count($entityConfiguration['conditions']) === 0) {
                    return true;
                }


                $rights = $user->getRole()->getRights();

                $res = true;
                foreach ($entityConfiguration['conditions'] as $condition) {
                    $haystack = [];
                    if (
                        isset($rights[$exploded['module'] . ':c:' . $exploded['entity'] . ':' . $condition['property'] . ':' . $exploded['action']])
                        && isset($rights[$exploded['module'] . ':c:' . $exploded['entity'] . ':' . $condition['property'] . ':' . $exploded['action']]['value'])
                    ) {
                        $haystack = $rights[$exploded['module'] . ':c:' . $exploded['entity'] . ':' . $condition['property'] . ':' . $exploded['action']]['value'];
                    }

                    $value = $entity->{'get' . ucfirst($condition['property'])}();
                    $res = $res && in_array($value, $haystack);
                }
                return $res;
            }
        }

        return true;
    }
}
