<?php

$analysisProject = [
    'icon' => 'project',
    'href' => 'analysis/project',
    'title' => 'analysis_project_module_title',
    'module' => 'project',
    'force-master' => true,
    'only-master' => true,
    'priority' => 6
];

$analysisBudget = [
    'icon' => 'budget',
    'href' => 'analysis/budget',
    'title' => 'analysis_budget_module_title',
    'module' => 'budget',
    'force-master' => true,
    'priority' => 5
];

$analysisTime = [
    'icon' => 'time',
    'href' => 'analysis/time',
    'title' => 'analysis_time_module_title',
    'module' => 'time',
    'force-master' => true,
    'priority' => 4
];

$analysisIndicator = [
    'icon' => 'indicator',
    'href' => 'analysis/indicator',
    'title' => 'analysis_indicator_module_title',
    'module' => 'indicator',
    'force-master' => true,
    'priority' => 3
];

$analysisConvention = [
    'icon' => 'convention',
    'href' => 'analysis/convention',
    'title' => 'analysis_convention_module_title',
    'module' => 'convention',
    'force-master' => true,
    'priority' => 2
];

$analysisMap = [
    'icon' => 'map',
    'href' => 'analysis/map',
    'title' => 'analysis_map_module_title',
    'module' => 'map',
    'force-master' => true,
    'priority' => 0
];

return [
    'menu' => [
        'client-menu' => [
            'analysis' => [
                'icon' => 'analysis',
                'title' => 'analysis_module_title',
                'children' => [
                    'analysis-project' => $analysisProject,
                    'analysis-budget' => $analysisBudget,
                    'analysis-time' => $analysisTime,
                    'analysis-indicator' => $analysisIndicator,
                    'analysis-convention' => $analysisConvention,
                    'analysis-map' => $analysisMap,
                ],
            ],
        ],
        'client-menu-parc' => [
            'analysis' => [
                'icon' => 'analysis',
                'title' => 'analysis_module_title',
                'priority' => 9998,
                'children' => [
                    'analysis-project' => $analysisProject,
                    'analysis-budget' => $analysisBudget,
                    'analysis-time' => $analysisTime,
                    'analysis-indicator' => $analysisIndicator,
                    'analysis-convention' => $analysisConvention,
                    'analysis-map' => $analysisMap,
                ],
            ],
        ],
    ],
];
