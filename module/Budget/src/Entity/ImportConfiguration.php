<?php

namespace Budget\Entity;

use Application\Entity\Query;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Expense
 *
 * @package Budget\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="budget_import_configuration")
 */
class ImportConfiguration extends BasicRestEntityAbstract
{
    public const DAILY = 0;
    public const WEEKLY = 1;
    public const MONTHLY = 2;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $budgetEntity;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $budgetType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $importType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $year;

    /**
     * @var BudgetStatus
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\BudgetStatus")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $budgetStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $inPosteAccountParent = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $delimiter = ';';

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $ignoreFirstLine = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $path;

    /**
     * @var Query
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Query")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $query;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $frequency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastImportAt;

    /**
     * @return string
     */
    public function getBudgetEntity(): string
    {
        return $this->budgetEntity;
    }

    /**
     * @param string $budgetEntity
     */
    public function setBudgetEntity(string $budgetEntity): void
    {
        $this->budgetEntity = $budgetEntity;
    }

    /**
     * @return string
     */
    public function getBudgetType(): string
    {
        return $this->budgetType;
    }

    /**
     * @param string $budgetType
     */
    public function setBudgetType(string $budgetType): void
    {
        $this->budgetType = $budgetType;
    }

    /**
     * @return string
     */
    public function getImportType(): string
    {
        return $this->importType;
    }

    /**
     * @param string $importType
     */
    public function setImportType(string $importType): void
    {
        $this->importType = $importType;
    }

    /**
     * @return string
     */
    public function getYear(): ?string
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear(?string $year): void
    {
        $this->year = $year;
    }

    /**
     * @return BudgetStatus
     */
    public function getBudgetStatus(): ?BudgetStatus
    {
        return $this->budgetStatus;
    }

    /**
     * @param BudgetStatus $budgetStatus
     */
    public function setBudgetStatus(BudgetStatus $budgetStatus): void
    {
        $this->budgetStatus = $budgetStatus;
    }

    /**
     * @return bool
     */
    public function getInPosteAccountParent(): bool
    {
        return $this->inPosteAccountParent;
    }

    /**
     * @param bool $inPosteAccountParent
     */
    public function setInPosteAccountParent(bool $inPosteAccountParent): void
    {
        $this->inPosteAccountParent = $inPosteAccountParent;
    }

    /**
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter): void
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @return bool
     */
    public function getIgnoreFirstLine(): bool
    {
        return $this->ignoreFirstLine;
    }

    /**
     * @param bool $ignoreFirstLine
     */
    public function setIgnoreFirstLine(bool $ignoreFirstLine): void
    {
        $this->ignoreFirstLine = $ignoreFirstLine;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return Query
     */
    public function getQuery(): Query
    {
        return $this->query;
    }

    /**
     * @param Query $query
     */
    public function setQuery(Query $query): void
    {
        $this->query = $query;
    }


    /**
     * @return int
     */
    public function getFrequency(): int
    {
        return $this->frequency;
    }

    /**
     * @param int $frequency
     */
    public function setFrequency(int $frequency): void
    {
        if(!in_array($frequency, [self::DAILY, self::WEEKLY, self::MONTHLY])) {
            $frequency = self::DAILY;
        }

        $this->frequency = $frequency;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'       => 'query',
                'required'   => false,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                $query = $entityManager->getRepository('Application\Entity\Query')->find($value);

                                return $query !== null;
                            },
                            'message'  => 'alert_field_query_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'       => 'budgetStatus',
                'required'   => false,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                $query = $entityManager->getRepository('Budget\Entity\BudgetStatus')->find($value);

                                return $query !== null;
                            },
                            'message'  => 'alert_field_budgetStatus_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'budgetEntity',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'budgetType',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'importType',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'path',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'frequency',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                    [
                        'name'    => 'AllowList',
                        'options' => [
                            'list' => [self::DAILY, self::WEEKLY, self::MONTHLY],
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'delimiter',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                    [
                        'name'    => 'AllowList',
                        'options' => [
                            'list' => [',',';',"\t"],
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'year',
                'required' => false,
            ],
            [
                'name'     => 'inPosteAccountParent',
                'required' => false,
            ],
            [
                'name'     => 'ignoreFirstLine',
                'required' => false,
            ]
        ]);

        return $inputFilter;
    }

    /**
     * @return \DateTime
     */
    public function getLastImportAt(): ?\DateTime
    {
        return $this->lastImportAt;
    }

    /**
     * @param \DateTime $lastImportAt
     */
    public function setLastImportAt(\DateTime $lastImportAt): void
    {
        $this->lastImportAt = $lastImportAt;
    }

    public function shouldRun()
    {
        if($this->lastImportAt === null){
            $refDate = new \DateTime('1970-01-01');
        }else{
            $refDate = clone $this->lastImportAt;
        }

        $refDate->setTime(0, 0, 0);
        
        return match ($this->frequency) {
            self::DAILY => $refDate->add(new \DateInterval('P1D')) <= new \DateTime(),
            self::WEEKLY => $refDate->add(new \DateInterval('P7D')) <= new \DateTime(),
            self::MONTHLY => $refDate->add(new \DateInterval('P30D')) <= new \DateTime(),
            default => false,
        };
    }

    public function getFrequencyLabel(): string
    {
        return match ($this->frequency) {
            self::DAILY => 'Daily',
            self::WEEKLY => 'Weekly',
            self::MONTHLY => 'Monthly',
        };
    }
}