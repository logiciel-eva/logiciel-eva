<?php

namespace Age\Model;

use JsonSerializable;

class AgeRequest implements JsonSerializable
{

    private $etablissement;
    private $exercice;
    private $importPartiel;
    private $wfEtapeInitiale;
    private $wfTransition;
    private $natureImport;
    private $referenceExterne;
    private $operations;

    public function __construct()
    {
        $this->operations = array();
    }


    public function getEtablissement()
    {
        return $this->etablissement;
    }

    public function setEtablissement($etablissement)
    {
        $this->etablissement = $etablissement;
    }

    public function getExercice()
    {
        return $this->exercice;
    }

    public function setExercice($exercice)
    {
        $this->exercice = $exercice;
    }

    public function getImportPartiel()
    {
        return $this->importPartiel;
    }

    public function setImportPartiel($importPartiel)
    {
        $this->importPartiel = $importPartiel;
    }

    public function getWfEtapeInitiale()
    {
        return $this->wfEtapeInitiale;
    }

    public function setWfEtapeInitiale($wfEtapeInitiale)
    {
        return $this->wfEtapeInitiale = $wfEtapeInitiale;
    }

    public function getWfTransition()
    {
        return $this->wfTransition;
    }

    public function setWfTransition($wfTransition)
    {
        $this->wfTransition = $wfTransition;
    }

    public function getNatureImport()
    {
        return $this->natureImport;
    }

    public function setNatureImport($natureImport)
    {
        $this->natureImport = $natureImport;
    }

    public function getReferenceExterne()
    {
        return $this->referenceExterne;
    }

    public function setReferenceExterne($referenceExterne)
    {
        $this->referenceExterne = $referenceExterne;
    }

    public function getOperations()
    {
        return $this->operations;
    }

    public function setOperations($operations)
    {
        $this->operations = $operations;
    }

    public function addOperation($operation)
    {
        if (null === $this->operations) {
            $this->operations = array();
        }
        $this->operations[] = $operation;
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
