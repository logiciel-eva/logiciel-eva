(function(app) {
    app.controller('filterController', [  
		'$scope',
		'$timeout',
		 function ($scope, $timeout) {
			var self = this;
	
			self.init = function () {
					$timeout(function () {
						if (typeof $scope.__filters === 'undefined') {
							$scope.__filters = {};
						}
					});
			};
		}
		]);
        
    app.controller('measureCampainFormController', [
        'campain-measureService', '$scope', 'flashMessenger', 'translator', '$filter', '$timeout', '$rootScope',
        function measureListController(measureService, $scope, flashMessenger, translator, $filter, $timeout, $rootScope) {
            var self = this;
            const $modal = angular.element('#measure-campain-modal');
            self.measureCampain = null;
            self.externalProperties = {
                start: null,
                date: null,
                comment: null,
                source: null,
                indicateur : null,
                indicator: null,
                groupIndicator : null,
                project: null,
                territories: null,
                _rights : {
                    update: true
                }
            };
            self.from = {
                type: null,
                id: null
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };
            
            var apiParams = {
                col: [
                    'id',
                    'supposedDate',
                    'campain.id',
                    'indicator.id',
                    'realiseMeasure.id',
                    'realiseMeasure.fixedValue',
                    'realiseMeasure.value',
                    'realiseMeasure.type',
                    'realiseMeasure.comment',
                    'realiseMeasure.source',
                    'realiseMeasure.groupIndicator.id',
                    'realiseMeasure.groupIndicator.name',
                    'realiseMeasure.indicator.id',
                    'realiseMeasure.indicator.name',
                    'realiseMeasure.indicator.type',
                    'realiseMeasure.indicator.values',
                    'realiseMeasure.indicator.groupIndicators.id',
                    'realiseMeasure.indicator.groupIndicators.name',
                    'realiseMeasure.project.id',
                    'realiseMeasure.project.name',
                    'realiseMeasure.territories',
                    'realiseMeasure.start',
                    'realiseMeasure.date',
                    'realiseMeasure.updatedAt',
                    'targetMeasure.id',
                    'targetMeasure.value',
                    'targetMeasure.type',
                    'targetMeasure.comment',
                    'targetMeasure.source',
                    'targetMeasure.fixedValue',
                    'targetMeasure.groupIndicator.id',
                    'targetMeasure.groupIndicator.name',
                    'targetMeasure.indicator.id',
                    'targetMeasure.indicator.name',
                    'targetMeasure.indicator.type',
                    'targetMeasure.indicator.values',
                    'targetMeasure.indicator.groupIndicators.id',
                    'targetMeasure.indicator.groupIndicators.name',
                    'targetMeasure.project.id',
                    'targetMeasure.project.name',
                    'targetMeasure.territories',
                    'targetMeasure.start',
                    'targetMeasure.date',
                    'targetMeasure.updatedAt'
                ]
            };
           
            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id = id;
            };
            self.transfertData = function(measure, externalProperties){
                measure.indicator = externalProperties.indicator;
                measure.groupIndicator = externalProperties.groupIndicator;
                measure.start = externalProperties.start;
                measure.date = externalProperties.date;
                measure.comment = externalProperties.comment;
                measure.source = externalProperties.source;
                measure.project = externalProperties.project;
                measure.territories = externalProperties.territories;
            };
            self.saveMeasureCampain = function saveMeasureCampain() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.measureCampain.id;
                self.transfertData(self.measureCampain.realiseMeasure, self.externalProperties);
                self.transfertData(self.measureCampain.targetMeasure, self.externalProperties);
                $scope.$broadcast('save-attachment');
                measureService.save(self.measureCampain, apiParams).then(function(res) {
                    if (isCreation) {
                        $scope.$parent.self.measures.push(res.object);
                    } else {
                        for(var i in $scope.$parent.self.measures){
                            if($scope.$parent.self.measures[i].id == res.object.id){
                                $scope.$parent.self.measures[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('measures_campain_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            $scope.$on('create-measure-campain', function (event) {
                self.errors.fields = {};
                self.loading.save = false;
                self.externalProperties = {
                    start: null,
                    date: null,
                    comment: null,
                    source: null,
                    indicateur : null,
                    groupIndicator : null,
                    project: null,
                    territories: null,
                    attachmentPrimary: null,
                    _rights : {
                        update: true
                    }
                };
                self.measureCampain = {
                    campain : {
                        id: self.from.id
                    },
                    realiseMeasure : {
                        type: "done"
                    },
                    targetMeasure : {
                        type: "target"
                    },
                    _rights: {
                        update: true
                    }
                };
                $modal.modal('show');
            });

            $scope.$on('edit-measure-campain', function (event, measureCampain) {
                self.errors.fields = {};
                self.loading.save = false;
                self.measureCampain = angular.copy(measureCampain);
                self.externalProperties.indicator = measureCampain.realiseMeasure.indicator;
                self.externalProperties.groupIndicator = measureCampain.realiseMeasure.groupIndicator;
                self.externalProperties.start = measureCampain.realiseMeasure.start;
                self.externalProperties.date = measureCampain.realiseMeasure.date;
                self.externalProperties.comment = measureCampain.realiseMeasure.comment;
                self.externalProperties.source = measureCampain.realiseMeasure.source;
                self.externalProperties.project = measureCampain.targetMeasure.project;
                self.externalProperties.territories = measureCampain.realiseMeasure.territories;
                self.externalProperties.attachmentPrimary = measureCampain.realiseMeasure.id;
                $modal.modal('show');
            });
        }
    ])

})(window.EVA.app);