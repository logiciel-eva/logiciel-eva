<?php namespace Budget\Export;

use Budget\Entity\Expense;
use Core\Export\IExporter;
use Core\Extractor\ObjectExtractor;

abstract class PosteExpense implements IExporter
{
    public static function getConfig()
    {
        $arr = [
            'project.name'           => [
                'name' => 'project',
            ],
            'account.name'           => [
                'name' => 'account',
            ],
            'expenses.tiers'         => [
                'translation' => 'expense_field_tiers',
            ],
            'expenses.anneeExercice' => [
                'translation' => 'expense_field_anneeExercice',
            ],
            'expenses.bordereau'     => [
                'translation' => 'expense_field_bordereau',
            ],
            'expenses.mandat'        => [
                'translation' => 'expense_field_mandat',
            ],
            'expenses.engagement'    => [
                'translation' => 'expense_field_engagement',
            ],
            'expenses.complement'    => [
                'translation' => 'expense_field_complement',
            ],
            'expenses.dateFacture'   => [
                'translation' => 'expense_field_dateFacture',
            ],
            'expenses.date'          => [
                'translation' => 'expense_field_date',
            ],
        ];

        foreach (Expense::getTypes() as $type) {
            $arr['amountArbo' . ucfirst($type)] = [
                'translation' => 'expense_type_' . $type,
                'property'    => 'amountArbo' . ucfirst($type),
            ];
            $arr['amountHTArbo' . ucfirst($type)] = [
                'translation' => 'expense_type_' . $type . '_ht',
                'property'    => 'amountHTArbo' . ucfirst($type),
            ];
        }

        foreach (\Budget\Entity\PosteExpense::getBalances() as $balance) {
            $arr['amountArbo' . ucfirst($balance)] = [
                'translation' => 'poste_expense_balance_' . $balance,
                'property'    => 'amountArbo' . ucfirst($balance),
            ];
            $arr['amountHTArbo' . ucfirst($balance)] = [
                'translation' => 'poste_expense_balance_' . $balance . '_ht',
                'property'    => 'amountHTArbo' . ucfirst($balance),
            ];
        }

        return $arr;
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        $linesBolded = [];
        $total = [];

        foreach ($cols as $col) {
            if (preg_match('/^amount/', $col)) {
                $total[] = 0;
            } else if (preg_match('/^id$/', $col)) {
                $total[] = 'Total';
            } else {
                $total[] = '';
            }
        }

        foreach ($objects as $object) {
            if (!in_array($object->getId(), $itemsRemoved) && ($itemsSelected === [] xor in_array($object->getId(), $itemsSelected))) {
                $row = [];
                $extractor->setObject($object);

                foreach ($cols as $i => $col) {
                    if (/*!preg_match('/^project/', $col) &&*/
                        !preg_match('/^expenses./', $col)
                    ) {
                        $row[] = $extractor->extract($col, $translator->__invoke('exporter_error_value'));
                        if (preg_match('/^amount/', $col)) {
                            $total[$i] += $extractor->extract($col, $translator->__invoke('exporter_error_value'));
                        }
                    } else  {
                        $row[] = '';
                    }
                }

                $rows[] = $row;
                end($rows);
                $linesBolded[] = key($rows) + 1;
                reset($rows);

                foreach ($object->getExpensesArbo() as $expense) {
                    $row = [];
                    $extractor->setObject($expense);

                    $type = $expense->getType();
                    foreach ($cols as $col) {
                        if (!preg_match('/^account/', $col) &&
                            !preg_match('/^amount/', $col) &&
                            !preg_match('/^expenses./', $col)
                        ) {
                            $row[] = $extractor->extract($col, $translator->__invoke('exporter_error_value'));
                        } else if (preg_match('/^amountArbo' . ucfirst($type) . '$/', $col)) {
                            $row[] = $extractor->extract('amount', $translator->__invoke('exporter_error_value'));
                        } else if (preg_match('/^amountHTArbo' . ucfirst($type) . '$/', $col)) {
                            $row[] = $extractor->extract('amountHT', $translator->__invoke('exporter_error_value'));
                        } else if (preg_match('/^expenses./', $col)) {
                            $row[] = $extractor->extract(str_replace('expenses.', '', $col), $translator->__invoke('exporter_error_value'));
                        } else {
                            $row[] = '';
                        }
                    }
                    $rows[] = $row;
                }
            }
        }

        $rows[] = $total;
        end($rows);
        $linesBolded[] = key($rows) + 1;
        reset($rows);

        return [
            'rows' => $rows,
            'linesBolded' => $linesBolded
        ];
    }
}