<?php

use Time\Command\ImportCalendars;
use Time\Command\ImportCalendarsFactory;

/**
 * Inject configuations on global scope.
 */
return [
    'service_manager' => [
        'factories' => [
            ImportCalendars::class => ImportCalendarsFactory::class,
        ],
    ],
    'laminas-cli'     => [
        'commands' => [
            'time:import-calendars' => ImportCalendars::class,
        ],
    ],
];
