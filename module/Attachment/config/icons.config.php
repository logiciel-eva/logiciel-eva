<?php

return [
    'icons' => [
        'attachment' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-paperclip',
        ],
    ]
];
