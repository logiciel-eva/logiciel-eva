(function(app) {

    app.factory('roleService', [
        'restFactoryService',
        function roleService(restFactoryService) {
            return restFactoryService.create('/api/role');
        }
    ])

})(window.EVA.app);
