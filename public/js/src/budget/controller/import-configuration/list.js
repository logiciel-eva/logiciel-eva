(function (app) {
	app.controller('importConfigurationListController', [
		'importConfigurationService', '$scope', 'flashMessenger', 'translator',
		function importConfigurationListController(importConfigurationService, $scope, flashMessenger, translator) {
			var self = this;

			self.importConfiguration = {};
			self.importConfiguration.selected = false;

			self.importConfigurations = [];
			let apiParams = {
				col: [
					'id',
					'budgetEntity',
					'budgetType',
					'importType',
					'year',
					'budgetStatus.id',
					'budgetStatus.name',
					'inPosteAccountParent',
					'delimiter',
					'ignoreFirstLine',
					'path',
					'frequency',
					'query.id',
					'query.name',
					'lastImportAt',
					'createdAt',
					'updatedAt'
				]
			};

			self.options     = {
				name      : 'importConfigurations-list',
				col				: apiParams.col,
				searchType: 'list',
				sort      : 'name',
				order     : 'asc',
				pager     : true,
				getItems  : function getData(params, callback, abort) {
					importConfigurationService.findAll(params, abort.promise).then(function (res) {
						callback(res);
					});
				}
			};

			self.getFrequencyLabel = function getFrequencyLabel(frequency){
				switch(frequency) {
					case 1: return translator('import_configuration_frequency_weekly');
					case 2: return translator('import_configuration_frequency_monthly');
					default: return translator('import_configuration_frequency_daily');
				}
			};

			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};

			self.editLine = function editLine(sync) {
				sync.managers = [];

				self.editedLineErrors = {};
				self.editedLine = sync;
				self.editedLineFields = angular.copy(sync);
			};

			self.saveEditedLine = function saveEditedLine(sync) {
				self.editedLineErrors = {};
				importConfigurationService.save(angular.extend({}, sync, self.editedLineFields), {
					col: self.options.col
				}).then(function (res) {
					if (res.success) {
						sync = angular.extend(sync, res.object);
						self.editedLine = null;
						self.editedLineFields = null;
					}
				}, function (err) {
					for (var field in err.fields) {
						self.editedLineErrors[field] = err.fields[field];
					}
				});
			};

			self.run = function run(id){
				importConfigurationService.run(id);
				importConfigurationService.findAll(apiParams).then(function (res) {
					self.importConfigurations = res.rows;
				});
			};

			self.deleteImport = function deleteImport(importConfiguration){
				if (confirm(translator('budget_automated_import_question_delete'))) {
					importConfigurationService.remove(importConfiguration).then(function (res) {
						if (res.success) {
							self.importConfigurations.splice(self.importConfigurations.indexOf(importConfiguration), 1);
							flashMessenger.success(translator('budget_automated_import_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				};
			};

		}
	]);
})(window.EVA.app);
