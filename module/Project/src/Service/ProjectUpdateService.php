<?php

namespace Project\Service;

use User\Auth\AuthStorage;
use Project\Entity\Project;
use Project\Entity\ProjectUpdate;
use Project\Repository\ProjectUpdateRepository;
use Doctrine\ORM\EntityManager;

class ProjectUpdateService
{
  const UPDATE_HISTORY_SIZE = 10;

  private $entityManager;
  private $updateRepo;
  private $authStorage;


  /**
   * @param EntityManager entityManager
   * @param ProjectRepository projectUpdateRepository
   * @param AuthStorage authStorage
   */
  public function __construct(
      $entityManager,
      $projectUpdateRepository,
      $authStorage
    )
  {
    $this->entityManager = $entityManager;
    $this->updateRepo = $projectUpdateRepository;
    $this->authStorage = $authStorage;
  }


  public function logProjectUpdate($project)
  {

    $projectUpdates = $this->updateRepo->findByProjectId($project->getId());
    $currentUser = $this->authStorage->getUser();

    if (count($projectUpdates) && $projectUpdates[0]->getUser() === $currentUser) {
      // if last entry belongs to current user, modifies its date property
      $projectUpdates[0]->setDate(new \DateTime);
    } else {
      // otherwise create new one
      $currentUserUpdate = (new ProjectUpdate)
        ->setUser($currentUser)
        ->setProject($project)
        ->setDate(new \DateTime);
      array_unshift($projectUpdates, $currentUserUpdate);
      $this->entityManager->persist($currentUserUpdate);

      // if there are too many entries for this project, delete the oldest ones
      $nbUpdates = count($projectUpdates);
      if (count($projectUpdates) > self::UPDATE_HISTORY_SIZE) {
        for ($i = self::UPDATE_HISTORY_SIZE; $i < $nbUpdates; $i++) {
          $this->entityManager->remove($projectUpdates[$i]);
        }
      }
    }

    $this->entityManager->flush();
  }


}
