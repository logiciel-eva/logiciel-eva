(function (app) {

    app.controller('graphicProjectTimePieController', [
        '$timeout', 'treeBuilderService', '$scope', 'translator', '$http',
        function graphicProjectTimePieController($timeout, treeBuilderService, $scope, translator, $http) {
            var self = this;

            self.datas = [];

            self.filters = {
                user: {},
                project: {},
                timesheet: {},
                group: null
            };
            self.total = [];
            self.label = [];
            $scope.chart = {
                months: {
                    api: {},
                    options: {
                        chart: {
                            type: 'pieChart',
                            showControls: false,
                            showValues: true,
                            showLabels: false,
                            stacked: true,
                            height: 400,
                            labelsOutside: true,
                            x: function (d) {
                                return d.label;
                            },
                            y: function (d) {
                                return d.value;
                            },
                            yAxis: {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                }
                            },
                            tooltip: {
                                valueFormatter: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                },
                            }
                        }
                    },
                    data: [
                    ]
                }
            };

            self.init = function (query) {
                $http({
                    url: '/analysis/time/projects?' + $.param({ filters: query ? query.filters : {}, projectAggregate: true }),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;
                    $scope.chart.months.data = []
                    self.total.totalDone = [];
                    self.datas = res.data;
                    for (const i in self.datas.res.projects) {
                        self.datas.res.projects[i] = treeBuilderService.toTree(self.datas.res.projects[i]);
                        for (const j in self.datas.res.projects) {
                            for (const k in self.datas.res.projects[j]) {
                                if (!self.total.totalDone[k]) {
                                    self.total.totalDone[k] = 0;
                                }
                                const project = self.datas.res.projects[j][k];
                                self.label[k] = project.name;
                                self.total.totalDone[k] += project.done;
                            }
                        }
                    }
                    for (let i = 0; i < self.label.length; i++) {
                        $scope.chart.months.data.push({
                            label: self.label[i],
                            value: self.total.totalDone[i]
                        });
                    }
                }, function () { });
            };
        }
    ]);
})(window.EVA.app);
