<?php

namespace Application;

use Application\Controller\Plugin\ClientPrivateConfigurationPlugin;
use Application\View\Helper\ClientPrivateConfigurationHelper;
use Core\Module\AbstractModule;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return [
            'factories'  => [
                'ClientPrivateConfiguration' => function (ServiceManager $serviceManager) {
                    $entityManager = $serviceManager->get('Environment')->getEntityManager();
                    $configuration = $entityManager->getRepository('Application\Entity\Configuration')->find(1);
                    return $configuration;
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories'  => [
                'clientPrivateConfiguration' => function (ServiceManager $serviceManager) {
                    $configuration = $serviceManager->get('ClientPrivateConfiguration');
                    return new ClientPrivateConfigurationHelper($configuration);
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public function getControllerPluginConfig()
    {
        return [
            'factories'  => [
                'clientPrivateConfiguration' => function (ServiceManager $serviceManager) {
                    $configuration = $serviceManager->get('ClientPrivateConfiguration');
                    return new ClientPrivateConfigurationPlugin($configuration);
                },
            ],
        ];
    }
}
