(function (app) {

    app.controller('projectController', [
        '$timeout', '$scope', '$http', 'treeBuilderService',
		'configurationService', 'translator',
        function projectController($timeout, $scope, $http, treeBuilderService, configurationService, translator) {
            var self         = this;
            var shouldReload = false;
            self.code = null;
            self.name = null;
            self.chartTypes = ['pie', 'sunburst', 'bar'];
            self.j = 0;
            self.datas = [];
            self.configuration = {};
            self.showChildren = true;
            $scope.chart = {
                projects: []
            };
            self.total = [];
            self.barChart = {
                api    : {},
                options: {
                    chart: {
                        type         : 'multiBarChart',
                        showControls : false,
                        showValues   : true,
                        stacked      : true,
                        height       : 400,
                        labelsOutside: true,
                        x            : function (d) {
                            return d.label;
                        },
                        y            : function (d) {
                            return d.value;
                        },
                        yAxis        : {
                            tickFormat: function (d) {
                                return d3.format(',')(d) + ' h.';
                            }
                        }
                    }
                },
                data   : [
                    {
                        'key'   : translator('timesheet_type_done'),
                        'color' : '#4f99b4',
                        'values': []
                    },
                    {
                        'key'   : translator('timesheet_type_left'),
                        'color' : '#d67777',
                        'values': []
                    }
                ]
            };
            var apiParams = {
                col: [
                    'id',
                    'modules',
                    'translations',
                    'code',
                    'timeGraph'
                ]
            };


			self.showConfigModal = () => {
				angular.element('#configGraph').modal('show');
            };
            
            self.sunburst = {
                api    : {},
                options: {
                    chart: {
                        groupColorByParent: false,
                        type    : 'sunburstChart',
                        height  : 400,
                        color   : d3.scale.category20(),
                        duration: 500,
                        mode    : 'size',
                        tooltip : {
                            contentGenerator: function (obj) {
                                return '' +
                                    '<table>' +
                                    '    <tr>' +
                                    '        <td class="legend-color-guide">' +
                                    '            <div style="background-color:' + obj.color + '"></div>' +
                                    '        </td>' +
                                    '        <td class="key">' + obj.data.name + '</td>' +
                                    '        <td class="value">' + obj.data.realSize + '</td>' +
                                    '    </tr>' +
                                    '</table>';
                            }
                        },
                        key     : function (d) {
                            return d.name + d.i;
                        }
                    }
                }
            };
            self.projectAggregate = false;

            self.aggregateProject = function () {
                $scope.$emit('aggregateProject', {projectAggregate: self.projectAggregate});
            };

            $scope.$on('switchTab', function (event, data) {
                if (!self.configuration.timeGraph) {
                    configurationService.find(1, apiParams).then(res =>{
                        self.configuration =  res.object;
                        if (self.configuration.timeGraph.length == 0){
                            self.configuration.timeGraph = {};
                        }
                    })
                }
                if (data.active === 'project' && shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                }
            });

            self.saveConfiguration = function saveConfiguration () {
                configurationService.save(self.configuration, apiParams).then(function(res) {
                    self.configuration = res.object;
                    angular.element('#configGraph').modal('hide');
                    self.updateChart();

                }, function (err) {
                    self.loading.save = false;
                })
            };

            $scope.$on('applyFilters', function (event, data) {
                if ((data.active === 'project' && data.shouldReload) || (typeof data.projectAggregate !== 'undefined')) {
                    shouldReload = false;
                    self.applyFilters(data);
                } else if (data.shouldReload) {
                    shouldReload = true;
                }
            });
            self.order = 1;
            self.sort = function(column) {
                self[column] = self[column] != 'asc' ? 'asc' : 'desc';
                if (column == 'name') {
                    self.code = null;
                } else if (column == 'code') {
                    self.name = null;
                }
                self.order *= -1;
                for (var i in self.datas.res.projects) {
                    self.datas.res.projects[i] = self.datas.res.projects[i].sort((a,b) => (a[column] > b[column]) ? (1 * self.order) : ((b[column] > a[column]) ? ((-1)* self.order) : 0))
                    self.datas.res.projects[i].forEach(element =>{
                        element = self.recSort(element, column);
                    })
                }
                self.updateChart();
            }
            self.recSort = function(element, column) {
                element.children = element.children.sort((a,b) => (a[column] > b[column]) ? (1 * self.order) : ((b[column] > a[column]) ? ((-1)* self.order) : 0))
                element.children.forEach(child => {
                    if (child.children){
                        child = self.recSort(child, column);
                    }    
                })
                return element;
            }
            self.applyFilters = function (data) {
                self.i = 0;
                self.loadingOn();
                $http({
                    url   : '/analysis/time/projects?' + $.param({filters: data.filters, projectAggregate: true}),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;
                    for (var i in self.datas.res.projects) {
                        self.datas.res.projects[i] = treeBuilderService.toTree(self.datas.res.projects[i]);
                        self.total.totalDone = 0;
                        self.total.totalTarget = 0;
                        for (var i in self.datas.res.projects) {
                            for (var j in self.datas.res.projects[i]) {
                                var project = self.datas.res.projects[i][j];
                                self.total.totalDone += project.done;
                                self.total.totalTarget += project.target;
                            }
                        }
                    }

                    if (self.chartTypes[self.j] == 'pie') {
                        self.loadChart(null, 0);
                        self.updateChart();
                    }
                    else if (self.chartTypes[self.j] == 'sunburst') {
                        self.loadSunburstChart();
                    }
                    else {
                        self.loadBarChart();
                    }
                }, function () {});
            };

            self.switchChart = function() {
                self.j = self.j == 2 ? 0 : self.j += 1;
                if (self.chartTypes[self.j] == 'sunburst') {
                    if (self.datas.res){
                        // fix to have the good width for the graph
                        self.sunburst.options.chart.width = angular.element(document.getElementById('projectController')).prop('offsetWidth');
                        self.loadSunburstChart();
                    }
                } else if (self.chartTypes[self.j] == 'pie'){
                    if (self.datas.res){
                        self.loadChart(null, 0);
                        self.updateChart();
                    }
                }else {
                    if (self.datas.res){
                        // fix to have the good width for the graph
                        self.barChart.options.chart.width = angular.element(document.getElementById('projectController')).prop('offsetWidth');
                        self.loadBarChart();
                    }
                }
            };

            self.loadChart = function (parent, level) {
                var legendHeight = 0;
                for (var i in self.datas.res.projects) {
                    if (self.datas.res.projects.hasOwnProperty(i)) {
                        legendHeight += (self.datas.res.projects[i].length / 3) * 20;
                    }
                }
                $scope.chart.projects.splice(level);
                $scope.chart.projects.push({
                    api    : {},
                    parent : parent,
                    level  : level,
                    options: {
                        chart: {
                            type                   : 'pieChart',
                            showControls           : false,
                            showValues             : true,
                            stacked                : true,
                            height                 : legendHeight + 400,
                            useInteractiveGuideline: true,
                            labelsOutside          : true,
                            x                      : function (d) {
                                return d.label;
                            },
                            y                      : function (d) {
                                return d.value;
                            },
                            yAxis                  : {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                }
                            },
                            // tooltip                : {
                            //     valueFormatter: function (d) {
                            //         return d3.format(',')(d) + ' h.';
                            //     }
                            // },
                            callback               : function (chart) {
                                chart.pie.dispatch.on('elementClick', function (e) {
                                    $(e.element).parents('svg').find('.slice-selected').removeClass('slice-selected');
                                    $scope.$apply(function () {
                                        if (typeof e.data.project.children !== 'undefined') {
                                            if (e.data.project.children.length > 0) {
                                                $(e.element).find('path').addClass('slice-selected');
                                                self.loadChart(e.data.project, level + 1);
                                                self.updateChart();
                                            } else {
                                                $scope.chart.projects.splice(level + 1);
                                            }
                                        }
                                    });
                                });
                            }
                        }
                    },
                    data   : []
                });
            };

            self.updateChart = function () {
                for (var i in $scope.chart.projects) {
                    var chart  = $scope.chart.projects[i];
                    chart.data = [];
                    chart.totalDone = 0;
                    chart.totalTarget = 0;
                    if (chart.parent == null) {
                        for (var i in self.datas.res.projects) {
                            for (var j in self.datas.res.projects[i]) {
                                var project = self.datas.res.projects[i][j];
                                chart.data.push({
                                    code   : project.code,
                                    label  : project.name,
                                    value  : project.done,
                                    done   : project.done,
                                    target : project.target,
                                    project: project
                                });
                                chart.totalDone += project.done;
                                chart.totalTarget += project.target;
                            }
                        }
                    } else {
                        for (var i in chart.parent.children) {
                            var project = chart.parent.children[i];
                            chart.data.push({
                                code   : project.code,
                                label  : project.name,
                                value  : project.done,
                                done   : project.done,
                                target : project.target,
                                project: project
                            });
                            chart.totalDone += project.done;
                            chart.totalTarget += project.target;
                        }
                    }
                    chart.options.chart.x = function(d){
                        var value = [];
                        if (self.configuration.timeGraph.code) {
                            value.push(d.code);
                        }
                        if (self.configuration.timeGraph.title) {
                            value.push(d.label);
                        }
                        return value.join(' - ')
                    };
                    chart.options.chart.tooltip = {
                        valueFormatter : function(d){
                            var value = [];
                            if (self.configuration.timeGraph.percent) {
                                value.push(Math.round(d/chart.totalDone * 10000) /100 + '%');
                            }
                            if (self.configuration.timeGraph.hours) {
                                value.push(d3.format(',')(d) + ' h.');
                            }
                            return value.join(' - ')
                    }}
                    $timeout(function () {
                        chart.api.refresh();
                        self.loadingOff();
                    });
                }
            };

            self.loadSunburstChart = function () {
                self.loadingOn();
                self.sunburst.data = [{
                    realSize: 0,
                    name: '',
                    group_id: 0,
                    children: [],
                    i       : self.i
                }];

                self.i++;

                for (var i in self.datas.res.projects) {
                    for (var j in self.datas.res.projects[i]) {
                        var project = self.datas.res.projects[i][j];
                        self.sunburst.data[0].children.push(loadSunburstData(project, [])[0]);
                        if (self.divideTimes) {
                            self.sunburst.data[0].realSize += project.done;
                        }
                        else if (project.done > 0) {
                            self.sunburst.data[0].realSize = project.done;
                        }    
                    }
                }

                self.sunburst.data[0].children = removeProjectSize0(self.sunburst.data[0].children);

                self.loadingOff();
            };

            self.loadBarChart = function () {
                self.loadingOn();

                self.barChart.options.chart.type = 'multiBarChart';
                self.barChart.data = [
                    {
                        'key'   : translator('timesheet_type_done'),
                        'color' : '#4f99b4',
                        'values': []
                    },
                    {
                        'key'   : translator('timesheet_type_left'),
                        'color' : '#d67777',
                        'values': []
                    }
                ];

                if (typeof self.datas.res !== 'undefined') {
                    for (var i in self.datas.res.projects) {
                        for (var j in self.datas.res.projects[i]) {
                            var project = self.datas.res.projects[i][j];
                            self.barChart.data[0].values.push({
                                'label': project.name,
                                'value': project.done
                            });
                            self.barChart.data[1].values.push({
                                'label': project.name,
                                'value': project.target - project.done
                            });
                        }
                    }
                }
                self.loadingOff();
            };

            function removeProjectSize0(projects) {
                var i = projects.length;

                while (i--) {
                    if (projects[i].realSize === 0) {
                        projects.splice(projects.indexOf(projects[i]), 1);
                    } else if (projects[i].children.length > 0) {
                        projects[i].children = removeProjectSize0(projects[i].children);
                    }
                }

                return projects;
            }
            function loadSunburstData(project, res) {
                res.push({
                    name    : project.name,
                    size    : project.done,
                    children: [],
                    realSize: project.done,
                    i       : self.i
                });

                self.i++;
                var last_id = res.length - 1;

                if (project.children.length > 0) {
                    for (var i in project.children) {
                        loadSunburstData(project.children[i], res[last_id].children);
                    }
                }

                return res;
            }

            self.export = function (type, parent, group) {
                $scope.$emit('export', {type: type, parent: parent, group: group});
            };

            self.loadingOn = function () {
                self.loading = true;
                $scope.$emit('loadingOn');
            };

            self.loadingOff = function () {
                self.loading = false;
                $scope.$emit('loadingOff');
            };
            
            self.recExpandable = function(element, data){
                element.children.forEach(children =>{
                    if (children.id == data.id){
                        data.arrow = !data.arrow;
                        children.children.forEach(children2 =>{
                            children2.show = !children2.show;
                        })
                        return element;
                    } else {
                        children = self.recExpandable(children, data);
                    }
                });
                return element;
            }

            self.expandable = function(data, client){
                for (let element of self.datas.res.projects[client.id]) {
                    if (element.id == data.id){
                        data.arrow = !data.arrow;
                        for(let children of element.children){
                            children.show = !children.show;
                        }
                        break;
                    }
                    else { 
                        element = self.recExpandable(element, data);
                    }
                }
            }
        }
    ]);

})(window.EVA.app);
