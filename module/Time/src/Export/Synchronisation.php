<?php

namespace Time\Export;

use Core\Export\IExporter;

abstract class Synchronisation implements IExporter
{
    public static function getConfig()
    {
        return [
            'user.name' => [
                'name' => 'user',
            ],
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
