<?php

namespace Analysis\Service;


use Map\Entity\Territory;
use Map\Entity\Association as TerritoryAssociation; 
use Time\Entity\Timesheet;
use Keyword\Entity\Association;
use Keyword\Entity\Keyword;
use Analysis\Utils\MathsUtils;
use Core\Utils\DateUtils;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\Expr\Join;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Stdlib\Parameters;
use User\Auth\AuthStorage;

class TimeAnalysisService
{

     /**
     * @var AuthStorage
     */
    private $_authStorage;
    private $_entityManagerFactory;
    private $_simpleFiltersHelper;
    private $_serviceLocator;

    public function __construct($serviceLocator, $entityManagerFactory, $simpleFiltersHelper, AuthStorage $authStorage)
    {
        $this->_authStorage = $authStorage;
        $this->_serviceLocator = $serviceLocator;
        $this->_entityManagerFactory = $entityManagerFactory;
        $this->_simpleFiltersHelper = $simpleFiltersHelper;
    }


    public function getTotalsByClient($client, $filters) {
        $totals = [
            Timesheet::TYPE_DONE   => 0,
            Timesheet::TYPE_TARGET => 0
        ];

        $entityClass = 'Time\Entity\Timesheet';

        $entityManager = $this->_entityManagerFactory->getEntityManager($client);

        $queryBuilder = $entityManager->getRepository($entityClass)
        ->createQueryBuilder('object')
        ->select('object.type, SUM(object.hours) AS total')
        ->join('object.user', 'user')
        ->leftJoin('object.project', 'project')
        ->leftJoin('project.parent', 'project_parent')
        ->groupBy('object.type');
 

        $this->applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager);

        $result = $queryBuilder->getQuery()->getResult();

        if ($result) {
            foreach ($result as $r) {
                $totals[$r['type']] = $r['total'];                
            }
        }
        
        return $totals;
    }

    public function getByUser($client, $filters) {
        $entityClass = 'Time\Entity\Timesheet';
        $entityManager = $this->_entityManagerFactory->getEntityManager($client);
        $queryBuilder = $entityManager->getRepository($entityClass)
        ->createQueryBuilder('object')
        ->select('object.type,user.id,user.name,user.color')
        ->addSelect('SUM(CASE WHEN object.type = \'done\' THEN object.hours ELSE 0 END) AS done')
        ->addSelect('SUM(CASE WHEN object.type = \'target\' THEN object.hours ELSE 0 END) AS target')
        ->join('object.user', 'user')
        ->leftJoin('object.project', 'project')
        ->leftJoin('project.parent', 'project_parent')
        ->groupBy('user.id');

        $this->applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager);

        return  $queryBuilder->getQuery()->getResult();
    }


    public function getDoneByMonth($client, $filters)  {
        $entityClass = 'Time\Entity\Timesheet';
        $entityManager = $this->_entityManagerFactory->getEntityManager($client);
        $queryBuilder = $entityManager->getRepository($entityClass)
        ->createQueryBuilder('object')
        ->select('SUM(object.hours)  AS done,DATE_FORMAT(object.start, \'%Y-%m\') as month, DATE_FORMAT(object.start, \'%m/%Y\') as monthLabel ')
        ->where('object.type =\'done\'')
        ->join('object.user', 'user')
        ->leftJoin('object.project', 'project')
        ->leftJoin('project.parent', 'project_parent')
        ->groupBy('month')
        ->orderBy('month', 'ASC'); 
  
        $this->applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager);

        $rows =  $queryBuilder->getQuery()->getResult();

        $result = array_reduce($rows, function($res, $row) {
            $res[$row['monthLabel']] = (float) $row['done'];
            return $res;
          }, []);

          return $result;
    }

    public function getByProjects($client, $filters) {
        $entityClass = 'Time\Entity\Timesheet';
        $entityManager = $this->_entityManagerFactory->getEntityManager($client);
        $queryBuilder = $entityManager->getRepository($entityClass)
        ->createQueryBuilder('object')
        ->select('project.id', 'project.name', 'project.code', 'parent.id as parentId')
        ->addSelect('SUM(CASE WHEN object.type = \'done\' THEN object.hours ELSE 0 END) AS done')
        ->addSelect('SUM(CASE WHEN object.type = \'target\' THEN object.hours ELSE 0 END) AS target')
        ->leftJoin('object.project', 'project')
        ->leftJoin('project.parent', 'parent')
        ->join('object.user', 'user')
        ->groupBy('project.id');

        $this->applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager);

        $rows =  $queryBuilder->getQuery()->getResult();
        $result = [];
        foreach ($rows as &$row) {
            $row['parent'] = [];
            if (isset($row['parentId'])) {
                $row['parent'] = ['id' => (int)$row['parentId']]; 
            }
        }

        return $rows; 
    }

    public function getByProjectsKeywords($client, $filters, $divideTimes) {
        $entityClass = 'Time\Entity\Timesheet';
        $entityManager = $this->_entityManagerFactory->getEntityManager($client);
        $queryBuilder = $entityManager->getRepository($entityClass)
        ->createQueryBuilder('object')
        ->select('keyword.id', 'keyword.name', 'g.id as groupId')
        ->addSelect('GROUP_CONCAT(DISTINCT  p.id) as parents')
        ->addSelect('(SUM(CASE WHEN object.type = \'done\' THEN object.hours ELSE 0 END) ) AS done')
        ->addSelect('(SUM(CASE WHEN object.type = \'target\' THEN object.hours ELSE 0 END) ) AS target')
        ->leftJoin('object.project', 'project')
        ->leftJoin('project.parent', 'project_parent')
        ->leftJoin(
           Association::class,
           'keywordAssociation',
           Join::WITH,
           'project.id = keywordAssociation.primary  AND keywordAssociation.entity = \'Project\Entity\Project\''
       )
       ->leftJoin(Keyword::class, 
       'keyword',
       Join::WITH,
        'keywordAssociation.keyword = keyword.id'
       )
        ->leftJoin('keyword.parents', 'p')
       ->join('keyword.group', 'g')
       ->join('object.user', 'user')
       ->groupBy('keyword.id');

       $this->applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager);

       $rows =  $queryBuilder->getQuery()->getResult();

       $result = [];
       foreach ($rows as &$row) {
        $parentIds = $row['parents'] ? explode(',', $row['parents']) : [];
        $row['parents'] = array_map(function($id) {
            return ['id' => (int)$id];
        }, $parentIds); 
       
         $result[$row['groupId']][$row['id']] = $row;
       }
       
       return $result;
    }

    public function getByKeywords($client, $filters) {
        $entityClass = 'Time\Entity\Timesheet';
        $entityManager = $this->_entityManagerFactory->getEntityManager($client);
        $queryBuilder = $entityManager->getRepository($entityClass)
        ->createQueryBuilder('object')
        ->select('keyword.id', 'keyword.name', 'g.id as groupId')
        ->addSelect('GROUP_CONCAT(DISTINCT  p.id) as parents')
        ->addSelect('SUM(CASE WHEN object.type = \'done\' THEN object.hours ELSE 0 END) AS done')
        ->addSelect('SUM(CASE WHEN object.type = \'target\' THEN object.hours ELSE 0 END) AS target')
        ->leftJoin(
           Association::class,
           'keywordAssociation',
           Join::WITH,
           'object.id = keywordAssociation.primary  AND keywordAssociation.entity = \'Time\Entity\Timesheet\''
       )
       ->leftJoin(Keyword::class, 
       'keyword',
       Join::WITH,
        'keywordAssociation.keyword = keyword.id'
        )
        ->leftJoin('keyword.parents', 'p')
       ->join('keyword.group', 'g')
       ->leftJoin('object.project', 'project')
       ->leftJoin('project.parent', 'project_parent')
       ->join('object.user', 'user')
       ->groupBy('keyword.id');

       $this->applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager);

        $rows =  $queryBuilder->getQuery()->getResult();
       
        $result = [];
        foreach ($rows as &$row) {
            $parentIds = $row['parents'] ? explode(',', $row['parents']) : [];
            $row['parents'] = array_map(function($id) {
                return ['id' => (int)$id];
            }, $parentIds); 
           
            $result[$row['groupId']][$row['id']] = $row;
        }

        return $result;         
    }

    public function getByTerritories($client, $filters) {
        $entityClass = 'Time\Entity\Timesheet';
        $entityManager = $this->_entityManagerFactory->getEntityManager($client);
        $queryBuilder = $entityManager->getRepository($entityClass)
        ->createQueryBuilder('object')
        ->select('territory.id', 'territory.name')
        ->addSelect('GROUP_CONCAT(DISTINCT  p.id) as parents')
        ->addSelect('SUM(CASE WHEN object.type = \'done\' THEN object.hours ELSE 0 END) AS done')
        ->addSelect('SUM(CASE WHEN object.type = \'target\' THEN object.hours ELSE 0 END) AS target')
        ->leftJoin(
            TerritoryAssociation::class,
           'territoryAssociation',
           Join::WITH,
           'object.id = territoryAssociation.primary  AND territoryAssociation.entity = \'Time\Entity\Timesheet\''
       )
       ->leftJoin(Territory::class, 
       'territory',
       Join::WITH,
        'territoryAssociation.territory = territory.id'
       )
       ->leftJoin(
        Association::class,
        'keywordAssociation',
        Join::WITH,
        'object.id = keywordAssociation.primary  AND keywordAssociation.entity = \'Time\Entity\Timesheet\''
        )
       ->leftJoin(Keyword::class, 
       'keyword',
       Join::WITH,
        'keywordAssociation.keyword = keyword.id'
       )
        ->leftJoin('territory.parents', 'p')
       ->join('keyword.group', 'g')
       ->leftJoin('object.project', 'project')
       ->leftJoin('project.parent', 'project_parent')
       ->join('object.user', 'user')
       ->groupBy('territory.id');

       $this->applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager);

        $rows =  $queryBuilder->getQuery()->getResult();

        $result = [];
        foreach ($rows as &$row) {
          $parentIds = $row['parents'] ? explode(',', $row['parents']) : [];
          $row['parents'] = array_map(function($id) {
             return ['id' => (int)$id];
          }, $parentIds); 
        
           $result[$row['id']] = $row;
        }

        return $result;
    }

    private function applyFilters($client, $queryBuilder, $filters, $entityClass, $entityManager) {
        if(!isset($filters)) {
            return;
        }

        if((isset($filters['project']))) {
            $this->addProjectFilters($client, $queryBuilder, $filters['project']);
        }

        $queryFilters = [];

        if (isset($filters['timesheet'])) {
            $queryFilters = array_merge($queryFilters, $filters['timesheet']);
        }


        if (isset($filters['user'])) {
            $userFilters = array_combine(
                array_map(function ($key) {
                    return 'user.' . $key;
                }, array_keys($filters['user'])),
                $filters['user']
            );

            $userFilters = $this->changeCurrentUserFilter($userFilters);
            $queryFilters = array_merge($queryFilters, $userFilters);
        }

        $this->_simpleFiltersHelper->setEntityManager($entityManager);
        $this->_simpleFiltersHelper->apply($queryBuilder, $entityClass, $client->isMaster(), $queryFilters, $filters['mode'] ?? 'and');      
    }

    private function addProjectFilters($client, $queryBuilder, $projectFilters) {
        $projectIds = $this->getProjectsIds($client, $projectFilters);
  
        $queryBuilder->andWhere('project.id IN (:projectIds)')
        ->setParameter('projectIds', $projectIds);
    }



    protected function getProjectsIds($client, $projectFilters)
    {
        $isProjectActive = $this->_serviceLocator->get('MyModuleManager')->isActive('project');
        $isMaster        = $client->isMaster();

        $controllerProject = $this->_serviceLocator->get('ControllerManager')->get('Project\Controller\API\Project');

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);

        if ($isProjectActive || $isMaster) {
            $routeMatch = new RouteMatch([]);
            $e          = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $controllerProject->setEvent($e);
            $controllerProject->setClient($client);

            if ($isMaster) {
                $$projectFilters['networkAccessible'] = [
                    'op'  => 'eq',
                    'val' => 1,
                ];
            }

            $request->setQuery(new Parameters([
                    'col'    => [
                        'id'
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'id',
                            'order'   => 'asc',
                            'filters' => $projectFilters,
                        ],
                    ],
                ]));

            $projects = $controllerProject->dispatch($request)->getVariable('rows');

            $projectIds = array_map(function($project) {
                return $project['id'];
            }, $projects);

            return $projectIds;    
        }
    }

    protected function changeCurrentUserFilter($userFilters)
	{
		if (!isset($userFilters)) {
			return $userFilters;
		}

		foreach ($userFilters as &$filter) {
			if (!isset($filter['val'])) {
				continue;
			}

			if ($filter['val'] === 'current-user') {
				$filter['val'] = $this->_authStorage
					->getUser()
					->getId();
				continue;
			}

			if (is_array($filter['val']) && in_array('current-user', $filter['val'])) {
				foreach ($filter['val'] as &$value) {
					if ($value === 'current-user') {
						$value = $this->_authStorage
							->getUser()
							->getId();
					}
				}
			}
		}

		return $userFilters;
	}
}
