(function(app) {

    app.factory('envelopeService', [
        '$http', '$q', 'restFactoryService',
        function envelopeService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/budget/envelope'), {
                checkAmount: function checkAmount($id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/api/budget/envelope/'+$id+'/check'
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                }
            });
        }
    ])

})(window.EVA.app);
