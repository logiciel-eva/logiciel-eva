<?php

use Bootstrap\Entity\Client;
use Core\Controller\Plugin\ApiRequestPlugin;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Export
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translate
     */
    protected $translator;

    /**
     * @var ApiRequestPlugin
     */
    protected $apiRequest;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Export constructor.
     *
     * @param EntityManager    $em
     * @param Translate        $translator
     * @param ApiRequestPlugin $apiRequest
     * @param Client           $client
     */
    public function __construct(EntityManager $em, Translate $translator, ApiRequestPlugin $apiRequest, Client $client)
    {
        $this->em         = $em;
        $this->translator = $translator;
        $this->apiRequest = $apiRequest;
        $this->client     = $client;
    }

	public function doExport($params, $data)
	{
		ini_set('memory_limit', -1);
		ini_set('max_execution_time', -1);
		setlocale(LC_TIME, 'fr_FR', 'French_France', 'French');

		$lines = [];

        $project = $this->em->getRepository('Project\Entity\Project')->find($data['id']);
        /**
         * Get all keywords of the project
         */
        $associations = $this->em->getRepository('Keyword\Entity\Association')->findBy([
            'entity' => 'Project\Entity\Project',
            'primary' => $project->getId(),
        ]);

		$headerLine = [
            'Code projet',
            'Nom projet',
            'Statut fiche',
        ];
        $keyWordsValues = [];
        // Mettre les id des groupes à garder ici et décommenter cette ligne
        // $groupIds = [28];
        foreach($associations as $association) {
            $group = $association->getKeyword()->getGroup();
            // Décommenter cette ligne
            // if (in_array($group->getId(), $groupIds)){
            if (isset($keyWordsValues[$group->getId()])){
                $keyWordsValues[$group->getId()] .= ", " . $association->getKeyword()->getName(); 
    
            }
            else {
                $headerLine[] = $group->getName();
                $keyWordsValues[$group->getId()] = $association->getKeyword()->getName(); 
            }
            // Décommenter cette ligne
            // }
        }
        $headerLine = array_merge($headerLine, [
            'Type de ligne',
            'Nom nature',
            'Taux de financement',
            'bailleur',
            'Type de dépense',
            'Exercice',
            'Complément',
            'Montant'
        ]);
        $lines[] = $headerLine;
        $posteIncomesArbo = $project->getPosteIncomesArbo();
        $lineBeginning = array(
            $project->getCode(),
            $project->getName(),
            $this->translator->__invoke('project_publicationStatus_' . $project->getPublicationStatus())
        );
        foreach($keyWordsValues as $keyword){
            $lineBeginning[] = $keyword;
        };
        foreach($posteIncomesArbo as $posteIncomeArbo) { 
            foreach($posteIncomeArbo->getIncomes() as $income){
                $line = $lineBeginning;
                $line[] = $this->translator->__invoke('income_type_' . $income->getType());
                $line[] = $posteIncomeArbo->getAccount()->getName();
                $line[] = $posteIncomeArbo->getSubventionAmount();
                $line[] = $income->getTiers();
                $line[] = $posteIncomeArbo->getNature() ? $posteIncomeArbo->getNature()->getName() : '';
                $line[] = $income->getAnneeExercice();
                $line[] = $income->getComplement();
                $line[] = $income->getAmount();
                
                $lines[] = $line;
                }
        }
        
		$this->download($lines);
	}

    protected function getNumberIncomes($account)
    {
        $number = 0;

        foreach ($account as $posteIncome) {
            $number += count($posteIncome['incomes']);
        }

        return $number;
    }

    protected function download($array, $linesBolded = [])
    {
        try {
            foreach ($array as $i => $line) {
                foreach ($line as $j => $value) {
                    if (is_numeric($value) && $value == 0) {
                        $array[$i][$j] = '0';
                    }
                }
            }

            $spreadsheet = new Spreadsheet();
            $sheet       = $spreadsheet->getActiveSheet();

            $sheet->fromArray($array);

            foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
                $sheet->getColumnDimension($col)->setAutoSize(true);
            }

            $headerLine = $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . '1');
            $headerLine
                ->getFont()
                ->setBold(true);
            $headerLine
                ->getAlignment()
                ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                ->setVertical(Alignment::VERTICAL_CENTER);

            $sheet
                ->getStyle('A1:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())
                ->getAlignment()
                ->setWrapText(true);

            foreach ($linesBolded as $line) {
                $headerLine = $sheet->getStyle('A' . $line . ':' . $sheet->getHighestDataColumn() . $line);
                $headerLine->getFont()->setBold(true);
            }

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            header('Content-Disposition: attachment; filename="export.xlsx"');
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            ob_end_clean();
            $writer->save('php://output');
            exit;
        } catch (Exception $exception) {
            var_dump($exception->getMessage());
        }
    }
}
