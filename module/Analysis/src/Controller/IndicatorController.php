<?php

namespace Analysis\Controller;

use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use Indicator\Entity\Indicator;
use Indicator\Entity\Measure;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Stdlib\Parameters;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class IndicatorController extends AbstractActionSLController
{
    //const TOTAL = 'Total';      --> #510 (QPilote)
    const NaN = 'NaN';

    public function onDispatch(MvcEvent $e)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $groups = [];
        if ($this->serviceLocator->get('MyModuleManager')->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('indicator', false);
        }

        return new ViewModel([
            'groups' => $groups,
        ]);
    }

    public function recapAction()
    {
        $filters = $this->params()->fromQuery('filters', []);
        $groupBy = $filters['group'];
        $export = $this->params()->fromQuery('export', false);

        $data = [
            'indicators' => [],
            'groupIndicators' => [],
            'years' => [],
        ];

        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $controllerIndicator = $controllerManager->get('Indicator\Controller\API\Indicator');
   
    
        $_indicators = $this->getIndicators(isset($filters['indicator']) ? $filters['indicator'] : []);
  
        foreach ($_indicators as $_indicator) {

            $indicator = $this->entityManager()->getRepository('Indicator\Entity\Indicator')->find($_indicator['id']);
 
            $associations = $this->entityManager()->getRepository('Keyword\Entity\Association')->findBy([
                'primary' => $_indicator['id'],
                'entity' => 'Indicator\Entity\Indicator',
            ]);
            $data['indicators'][$indicator->getId()] = [
                'id' => $indicator->getId(),
                'name' => $indicator->getName(),
                'uom' => $indicator->getUom(),
                'type' => $indicator->getType(),
                'archived' => $indicator->getArchived(),
                'description' => $indicator->getDescription(),
                'definition' => $indicator->getDefinition(),
                'method' => $indicator->getMethod(),
                'interpretation' => $indicator->getInterpretation(),
                'operator' => $indicator->getOperator(),
                'createdAt' => null != $indicator->getCreatedAt() ? $indicator->getCreatedAt()->format('d/m/Y H:i') : null,
                'updatedAt' => null != $indicator->getUpdatedAt() ? $indicator->getUpdatedAt()->format('d/m/Y H:i') : null,
                'indicator' => $indicator,
                'data' => [],
            ];

            foreach ($indicator->getGroupIndicators() as $groupIndicator) {
                if (!isset($data['groupIndicators'][$groupIndicator->getId()])) {
                    $data['groupIndicators'][$groupIndicator->getId()] = [];
                    $data['groupIndicators'][$groupIndicator->getId()]['name'] = $groupIndicator->getName();
                    $data['groupIndicators'][$groupIndicator->getId()]['type'] = $indicator->getType();
                    $data['groupIndicators'][$groupIndicator->getId()]['operator'] = $groupIndicator->getOperator();
                    $data['groupIndicators'][$groupIndicator->getId()]['data'] = [];
                }
                if (!isset($data['groupIndicators'][$groupIndicator->getId()]['indicators'])) {
                    $data['groupIndicators'][$groupIndicator->getId()]['indicators'] = [];
                }
                $data['groupIndicators'][$groupIndicator->getId()]['indicators'][] = $indicator->getId();
                if (!isset($data['indicators'][$indicator->getId()]['groupIndicators'])) {
                    $data['indicators'][$indicator->getId()]['groupIndicators'] = [];
                }
                $data['indicators'][$indicator->getId()]['groupIndicators'][] = [
                    'id' => $groupIndicator->getId(),
                ];
            }
            if ($indicator->getGroupIndicators()->count() == 0) {
                if (!isset($data['groupIndicators']['-1']['indicators'])) {
                    $data['groupIndicators']['-1']['indicators'] = [];
                }
                $data['groupIndicators']['-1']['indicators'][] = $indicator->getId();

                if (!isset($data['indicators'][$indicator->getId()]['groupIndicators'])) {
                    $data['indicators'][$indicator->getId()]['groupIndicators'] = [];
                }
                $data['indicators'][$indicator->getId()]['groupIndicators'][] = [
                    'id' => '-1',
                ];
            }
            foreach ($associations as $association) {
                $keyword = $association->getKeyword();
                $group = $keyword->getGroup();
                $indicatorKeyword['id'] = $keyword->getId();
                $indicatorKeyword['name'] = $keyword->getName();
                if (!isset($data['indicators'][$indicator->getId()]['keywords'][$group->getId()])) {
                    $data['indicators'][$indicator->getId()]['keywords'][$group->getId()] = [];
                }
                $data['indicators'][$indicator->getId()]['keywords'][$group->getId()][] = $indicatorKeyword;
            }

            $data['indicators'][$indicator->getId()]['data'] = [];
        }

        $isMaster = $this->environment()->getClient()->isMaster();
        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            if (!isset($filters['measure'])) {
                $measuresFilters['measure'] = [];
            } else {
                $measuresFilters = $filters['measure'];
            }

            if (!$isMaster) {
                $measuresFilters['indicator.id'] = [
                    'op' => 'eq',
                    'val' => array_keys($data['indicators']),
                ];
            } else {
                $request = new Request();
                $request->setMethod(Request::METHOD_GET);
                $request->setQuery(new Parameters([
                    'col' => [
                        'id',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort' => 'id',
                            'order' => 'asc',
                            'filters' => [
                                'master' => [
                                    'op' => 'eq',
                                    'val' => array_keys($data['indicators']),
                                ],
                            ],
                        ],
                    ],
                ]));

                $routeMatch = new RouteMatch([]);
                $e = new MvcEvent();
                $e->setRouteMatch($routeMatch);
                $e->setParam('master', $isMaster);
                $controllerIndicator->setEvent($e);

                $controllerIndicator->setClient($client);
                $_indicators = $controllerIndicator->dispatch($request)->getVariable('rows');

                $measuresFilters['indicator.id'] = [
                    'op' => 'eq',
                    'val' => [],
                ];

                foreach ($_indicators as $_indicator) {
                    $measuresFilters['indicator.id']['val'][] = $_indicator['id'];
                }
            }

            if (isset($filters['project'])) {
                $projects = $this->getProjects( $client ,$filters['project']);

                $measuresFilters['project.id'] = [
                    'op' => 'eq',
                    'val' => [],
                ];

                foreach ($projects as $project) {
                    $measuresFilters['project.id']['val'][] = $project['id'];
                }
            }

            $measures = $this->getMeasures($client, $measuresFilters);

            foreach ($measures as $measure) {
                $value = is_array($measure) ? $measure['value'] : $measure->getValue();
                if(!isset($value) ){
                    continue;
                }

                $indicator = $isMaster ? $measure['indicator']['master'] : $measure['indicator']['id'];
                if (isset($measure['date'])) {
                    $date = \DateTime::createFromFormat('d/m/Y H:i', $measure['date']);
                    if ($date) {
                        $year = $date->format('Y');
                        $month = $date->format('n');
                        $quarter = ceil($month / 3);

                        foreach ($data['groupIndicators'] as $groupIndicator => $groupIndicatorData) {
                            if (!isset($data['groupIndicators'][$groupIndicator]['data'][$year])) {
                                $data['groupIndicators'][$groupIndicator]['data'][$year] = [];
                            }
                        }

                        if (!isset($data['indicators'][$indicator]['data'][$year])) {
                            $data['indicators'][$indicator]['data'][$year] = [
                                'number' => 0,
                            ];
                            foreach (Measure::getTypes() as $type) {
                                $data['indicators'][$indicator]['data'][$year][$type] = [
                                    'value' => null,
                                    'measures' => [],
                                ];
                            }
                            $data['indicators'][$indicator]['data'][$year]['months'] = [];
                            $data['indicators'][$indicator]['data'][$year]['quarters'] = [];
                            $data['years'][] = $year;
                        }

                        if (!isset($data['indicators'][$indicator]['data'][$year]['months'][$month])) {
                            $data['indicators'][$indicator]['data'][$year]['months'][$month] = [
                                'value' => null,
                                'measures' => [],
                            ];
                            foreach (Measure::getTypes() as $type) {
                                $data['indicators'][$indicator]['data'][$year]['months'][$month][$type] = [
                                    'value' => null,
                                    'measures' => [],
                                ];
                            }
                        }
                        if (!isset($data['indicators'][$indicator]['data'][$year]['quarters'][$quarter])) {
                            $data['indicators'][$indicator]['data'][$year]['quarters'][$quarter] = [
                                'value' => null,
                                'measures' => [],
                            ];
                            foreach (Measure::getTypes() as $type) {
                                $data['indicators'][$indicator]['data'][$year]['quarters'][$quarter][$type] = [
                                    'value' => null,
                                    'measures' => [],
                                ];
                            }
                        }

                        $data['indicators'][$indicator]['data'][$year][$measure['type']]['measures'][] = $measure;
                        $data['indicators'][$indicator]['data'][$year]['number']++;

                        $data['indicators'][$indicator]['data'][$year]['months'][$month][$measure['type']]['measures'][] = $measure;
                        $data['indicators'][$indicator]['data'][$year]['quarters'][$quarter][$measure['type']]['measures'][] = $measure;

                        foreach ($data['groupIndicators'] as $groupIndicator => $groupIndicatorData) {
                            if (!isset($data['groupIndicators'][$groupIndicator]['data'][$year])) {
                                $data['groupIndicators'][$groupIndicator]['data'][$year] = [];
                                $data['groupIndicators'][$groupIndicator]['data'][$year]['months'] = [];
                                $data['groupIndicators'][$groupIndicator]['data'][$year]['quarters'] = [];
                            }

                            if (!isset($data['groupIndicators'][$groupIndicator]['data'][$year]['months'][$month])) {
                                $data['groupIndicators'][$groupIndicator]['data'][$year]['months'][$month] = [
                                    'value' => null,
                                ];
                                foreach (Measure::getTypes() as $type) {
                                    $data['groupIndicators'][$groupIndicator]['data'][$year]['months'][$month][$type] = [
                                        'value' => null,
                                    ];
                                }
                            }
                            if (!isset($data['groupIndicators'][$groupIndicator]['data'][$year]['quarters'][$quarter])) {
                                $data['groupIndicators'][$groupIndicator]['data'][$year][$quarter] = [
                                    'value' => null,
                                ];
                                foreach (Measure::getTypes() as $type) {
                                    $data['groupIndicators'][$groupIndicator]['data'][$year]['quarters'][$quarter][$type] = [
                                        'value' => null,
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($groupBy == 'territory') {
            $data['territories'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                foreach ($dataIndicator['data'] as $period => $dataPeriod) {
                    foreach ($dataPeriod as $year => $dataYear) {
                        foreach (Measure::getTypes() as $type) {
                            foreach ($dataYear[$type]['measures'] as $measure) {
                                if ($measure['territories']) {
                                    foreach ($measure['territories'] as $territory) {
                                        $identifier = $id . '-' . $territory['id'];

                                        if (!isset($data['territories'][$identifier])) {
                                            $data['territories'][$identifier] = $dataIndicator;
                                            $data['territories'][$identifier]['territory'] = $territory['name'];

                                            foreach ($dataIndicator['data'] as $_period => $_dataPeriod) {
                                                foreach ($_dataPeriod as $_year => $_dataYear) {
                                                    $data['territories'][$identifier]['data'][$_period][$_year]['number'] = 0;
                                                    foreach (Measure::getTypes() as $_type) {
                                                        $data['territories'][$identifier]['data'][$_period][$_year][$_type]['measures'] = [];
                                                    }
                                                }
                                            }
                                        }

                                        $data['territories'][$identifier]['data'][$_period][$year][$type]['measures'][] = $measure;
                                        $data['territories'][$identifier]['data'][$_period][$year]['number']++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            usort($data['territories'], function ($a, $b) {
                $groupA = $a['territory'];
                $groupB = $b['territory'];

                return $groupA < $groupB ? -1 : 1;
            });

            $data['indicators'] = $data['territories'];
            unset($data['territories']);
        }

        foreach ($data['indicators'] as $identifier => $dataIndicator) {
            $totalDone = null;
            $totalTarget = null;
            $indicator = $dataIndicator['indicator'];
            $hasValue = false;

            foreach ($data['indicators'][$identifier]['data'] as $year => $yearData) {
                foreach (Measure::getTypes() as $type) {
                    $measures = $yearData[$type]['measures'];
                    $value = $indicator->getValue($measures);
                    if ($value !== null) {
                        $hasValue = true;
                    }
                    $data['indicators'][$identifier]['data'][$year][$type]['value'] = $value;
                    $data['indicators'][$identifier]['data'][$year][$type]['content'] = $value;
                    if ($indicator->getType() === Indicator::TYPE_FIXED) {
                        $data['indicators'][$identifier]['data'][$year][$type]['content'] = $indicator->getFixedValue($value);
                    }
                    unset($data['indicators'][$identifier]['data'][$year][$type]['measures']);
                }

                foreach ($data['indicators'][$identifier]['data'][$year]['months'] as $month => $monthData) {
                    foreach (Measure::getTypes() as $type) {
                        $measures = $monthData[$type]['measures'];
                        $value = $indicator->getValue($measures);
                        if ($value !== null) {
                            $hasValue = true;
                        }
                        $data['indicators'][$identifier]['data'][$year]['months'][$month][$type]['value'] = $value;
                        $data['indicators'][$identifier]['data'][$year]['months'][$month][$type]['content'] = $value;
                        if ($indicator->getType() === Indicator::TYPE_FIXED) {
                            $data['indicators'][$identifier]['data'][$year]['months'][$month][$type]['content'] = $indicator->getFixedValue($value);
                        }
                        unset($data['indicators'][$identifier]['data'][$year]['months'][$month][$type]['measures']);
                    }

                    $data['indicators'][$identifier]['data'][$year]['months'][$month]['ratio'] = $this->getRatio(
                        $indicator->getType(),
                        $data['indicators'][$identifier]['data'][$year]['months'][$month]['done']['value'],
                        $data['indicators'][$identifier]['data'][$year]['months'][$month]['target']['value']
                    );

                }

                foreach ($data['indicators'][$identifier]['data'][$year]['quarters'] as $quarter => $quarterData) {
                    foreach (Measure::getTypes() as $type) {
                        $measures = $quarterData[$type]['measures'];
                        $value = $indicator->getValue($measures);
                        if ($value !== null) {
                            $hasValue = true;
                        }
                        $data['indicators'][$identifier]['data'][$year]['quarters'][$quarter][$type]['value'] = $value;
                        $data['indicators'][$identifier]['data'][$year]['quarters'][$quarter][$type]['content'] = $value;
                        if ($indicator->getType() === Indicator::TYPE_FIXED) {
                            $data['indicators'][$identifier]['data'][$year]['quarters'][$quarter][$type]['content'] = $indicator->getFixedValue($value);
                        }
                        unset($data['indicators'][$identifier]['data'][$year]['quarters'][$quarter][$type]['measures']);
                    }

                    $data['indicators'][$identifier]['data'][$year]['quarters'][$quarter]['ratio'] = $this->getRatio(
                        $indicator->getType(),
                        $data['indicators'][$identifier]['data'][$year]['quarters'][$quarter]['done']['value'],
                        $data['indicators'][$identifier]['data'][$year]['quarters'][$quarter]['target']['value']
                    );

                }

                $data['indicators'][$identifier]['data'][$year]['ratio'] = $this->getRatio(
                    $indicator->getType(),
                    $data['indicators'][$identifier]['data'][$year]['done']['value'],
                    $data['indicators'][$identifier]['data'][$year]['target']['value']
                );

                //   --> #510 (QPilote)
                /*if (isset($data['indicators'][$identifier]['data'][$year]['done']['value'])) {
                    if (null == $totalDone) {
                        $totalDone = 0;
                    }

                    $totalDone += $data['indicators'][$identifier]['data'][$year]['done']['value'];
                }

                if (isset($data['indicators'][$identifier]['data'][$year]['target']['value'])) {
                    if (null == $totalTarget) {
                        $totalTarget = 0;
                    }

                    $totalTarget += $data['indicators'][$identifier]['data'][$year]['target']['value'];
                }*/
            }

            //   --> #510 (QPilote)
            /*$totalRatio = $this->getRatio($indicator->getType(), $totalDone, $totalTarget);
            $data['indicators'][$identifier]['data'][$this::TOTAL] = [
                'done' => ['value' => $totalDone],
                'target' => ['value' => $totalTarget],
                'ratio' => $totalRatio,
            ];*/

            unset($data['indicators'][$identifier]['indicator']);
            if (!$hasValue) {
                unset($data['indicators'][$identifier]);
                foreach ($data['groupIndicators'] as $groupIndicatorId => $groupIndicatorData) {
                    $data['groupIndicators'][$groupIndicatorId]['indicators'] = array_diff($data['groupIndicators'][$groupIndicatorId]['indicators'], [$identifier]);
                }
            }
        }

        foreach ($data['groupIndicators'] as $groupIndicatorId => $groupIndicatorData) {
            if(!array_key_exists('data', $groupIndicatorData)) {
                continue;
            }
            foreach ($groupIndicatorData['data'] as $years => $yearsData) {
                foreach ($groupIndicatorData['indicators'] as $indicatorId) {
                    if (isset($data['indicators'][$indicatorId]['data'][$years])) {

                        foreach ($data['indicators'][$indicatorId]['data'][$years] as $type => $typeData) {

                            if (isset($data['groupIndicators'][$groupIndicatorId]['type']) &&
                                ($type === 'number' || $type === 'done' || $type === 'target')) {
                                if (!isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$type])) {
                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type] = [];
                                }

                                if ($type === 'number') {
                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type] = $typeData;
                                }

                                if ($data['groupIndicators'][$groupIndicatorId]['type'] == \Indicator\Entity\Indicator::TYPE_FREE) {
                                    if (isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'])) {
                                        if (isset($typeData['value'])) {
                                            if ($groupIndicatorData['operator'] === 'sum') {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] += $typeData['value'];
                                            } else if ($groupIndicatorData['operator'] === 'avg') {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] += ($typeData['value'] / count($groupIndicatorData['indicators']));
                                            } else if ($groupIndicatorData['operator'] === 'med') {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] += ($typeData['value'] / count($groupIndicatorData['indicators']));
                                            } else if ($groupIndicatorData['operator'] === 'max') {
                                                if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] < $typeData['value']) {
                                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] = $typeData['value'];
                                                }
                                            } else if ($groupIndicatorData['operator'] === 'min') {
                                                if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] > $typeData['value']) {
                                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] = $typeData['value'];
                                                }
                                            } else if ($groupIndicatorData['operator'] === 'last') {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] = $typeData['value'];
                                            }
                                        }
                                    } else {
                                        $value = $typeData;
                                        if (is_array($value)) {
                                            $value = $typeData['value'];
                                        }

                                        if (is_numeric($value) && !is_nan($value)) {
                                            if ($groupIndicatorData['operator'] === 'avg') {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type] = [];
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] = ($value / count($groupIndicatorData['indicators']));
                                            } else {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type] = $typeData;
                                            }
                                        }
                                    }
                                }

                                if ($data['groupIndicators'][$groupIndicatorId]['type'] == \Indicator\Entity\Indicator::TYPE_FIXED) {
                                    if (isset($typeData['value'])) {
                                        if (!isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'])) {
                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] = $typeData['value'];
                                        } else if ($groupIndicatorData['operator'] === 'max') {
                                            if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] < $typeData['value']) {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type] = $typeData;
                                            }
                                        } else if ($groupIndicatorData['operator'] === 'min') {
                                            if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$type]['value'] > $typeData['value']) {
                                                $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type] = $typeData;
                                            }
                                        } else if ($groupIndicatorData['operator'] === 'last') {
                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$type] = $typeData;
                                        }
                                    }
                                }
                            }
                            if ($type == 'quarters') {
                                foreach ($typeData as $quarters => $quarterData) {

                                    if (isset($data['groupIndicators'][$groupIndicatorId]['type']) &&
                                        ($type === 'number' || $type === 'done' || $type === 'target')) {
                                        if (!isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type])) {
                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type] = [];
                                        }

                                        if ($type === 'number') {
                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type] = $quarterData;
                                        }

                                        if ($data['groupIndicators'][$groupIndicatorId]['type'] == \Indicator\Entity\Indicator::TYPE_FREE) {

                                            if (isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'])) {
                                                if (isset($quarterData['value'])) {
                                                    if ($groupIndicatorData['operator'] === 'sum') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] += $quarterData['value'];
                                                    } else if ($groupIndicatorData['operator'] === 'avg') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] += ($quarterData['value'] / count($groupIndicatorData['indicators']));
                                                    } else if ($groupIndicatorData['operator'] === 'med') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] += ($quarterData['value'] / count($groupIndicatorData['indicators']));
                                                    } else if ($groupIndicatorData['operator'] === 'max') {
                                                        if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] < $quarterData['value']) {
                                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] = $quarterData['value'];
                                                        }
                                                    } else if ($groupIndicatorData['operator'] === 'min') {
                                                        if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] > $quarterData['value']) {
                                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] = $quarterData['value'];
                                                        }
                                                    } else if ($groupIndicatorData['operator'] === 'last') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] = $quarterData['value'];
                                                    }
                                                }
                                            } else {
                                                $value = $quarterData;
                                                if (is_array($value)) {
                                                    $value = $quarterData[$type]['value'];
                                                }

                                                if (is_numeric($value) && !is_nan($value)) {
                                                    if ($groupIndicatorData['operator'] === 'avg') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type] = [];
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] = ($value / count($groupIndicatorData['indicators']));
                                                    } else {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters] = $quarterData;
                                                    }
                                                }
                                            }
                                        }

                                        if ($data['groupIndicators'][$groupIndicatorId]['type'] == \Indicator\Entity\Indicator::TYPE_FIXED) {
                                            if (isset($typeData['value'])) {
                                                if (!isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'])) {
                                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] = $typeData['value'];
                                                } else if ($groupIndicatorData['operator'] === 'max') {
                                                    if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] < $typeData['value']) {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type] = $typeData;
                                                    }
                                                } else if ($groupIndicatorData['operator'] === 'min') {
                                                    if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type]['value'] > $typeData['value']) {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type] = $typeData;
                                                    }
                                                } else if ($groupIndicatorData['operator'] === 'last') {
                                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$quarters][$type] = $typeData;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if ($type == 'months') {
                                foreach ($typeData as $months => $monthsData) {
                                    if (isset($data['groupIndicators'][$groupIndicatorId]['type']) &&
                                        ($type === 'number' || $type === 'done' || $type === 'target')) {
                                        if (!isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type])) {
                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type] = [];
                                        }

                                        if ($type === 'number') {
                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type] = $monthsData;
                                        }

                                        if ($data['groupIndicators'][$groupIndicatorId]['type'] == \Indicator\Entity\Indicator::TYPE_FREE) {
                                            if (isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'])) {
                                                if (isset($monthsData['value'])) {
                                                    if ($groupIndicatorData['operator'] === 'sum') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] += $monthsData['value'];
                                                    } else if ($groupIndicatorData['operator'] === 'avg') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] += ($monthsData['value'] / count($groupIndicatorData['indicators']));
                                                    } else if ($groupIndicatorData['operator'] === 'med') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] += ($monthsData['value'] / count($groupIndicatorData['indicators']));
                                                    } else if ($groupIndicatorData['operator'] === 'max') {
                                                        if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] < $monthsData['value']) {
                                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] = $monthsData['value'];
                                                        }
                                                    } else if ($groupIndicatorData['operator'] === 'min') {
                                                        if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] > $monthsData['value']) {
                                                            $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] = $monthsData['value'];
                                                        }
                                                    } else if ($groupIndicatorData['operator'] === 'last') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] = $monthsData['value'];
                                                    }
                                                }
                                            } else {
                                                $value = $monthsData;
                                                if (is_array($value)) {
                                                    $value = $monthsData['value'];
                                                }

                                                if (is_numeric($value) && !is_nan($value)) {
                                                    if ($groupIndicatorData['operator'] === 'avg') {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type] = [];
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] = ($value / count($groupIndicatorData['indicators']));
                                                    } else {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type] = $monthsData;
                                                    }
                                                }
                                            }
                                        }

                                        if ($data['groupIndicators'][$groupIndicatorId]['type'] == \Indicator\Entity\Indicator::TYPE_FIXED) {
                                            if (isset($monthsData['value'])) {
                                                if (!isset($data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'])) {
                                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] = $monthsData['value'];
                                                } else if ($groupIndicatorData['operator'] === 'max') {
                                                    if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] < $monthsData['value']) {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type] = $monthsData;
                                                    }
                                                } else if ($groupIndicatorData['operator'] === 'min') {
                                                    if ($data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type]['value'] > $monthsData['value']) {
                                                        $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type] = $monthsData;
                                                    }
                                                } else if ($groupIndicatorData['operator'] === 'last') {
                                                    $data['groupIndicators'][$groupIndicatorId]['data'][$years][$months][$type] = $monthsData;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //$data['years'][] = $this::TOTAL;  --> #510 (QPilote)
        $data['years'] = array_unique($data['years']);

        sort($data['years']);

        if ($groupBy == 'keyword') {
            $data['keywords'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                $associations = $this->entityManager()->getRepository('Keyword\Entity\Association')->findBy([
                    'primary' => $id,
                    'entity' => 'Indicator\Entity\Indicator',
                ]);
                foreach ($associations as $association) {
                    $keyword = $association->getKeyword();
                    $group = $keyword->getGroup();

                    if (
                        !$group->isArchived()
                        && !$keyword->isArchived()
                    ) {
                        $dataIndicator['group'] = $group->getName();
                        $dataIndicator['keyword'] = $keyword->getName();

                        $data['keywords'][] = $dataIndicator;
                    }
                }
            }

            usort($data['keywords'], function ($a, $b) {
                $groupA = $a['group'];
                $groupB = $b['group'];

                $keywordA = $a['keyword'];
                $keywordB = $b['keyword'];

                if ($groupA == $groupB) {
                    if ($keywordA == $keywordB) {
                        return 0;
                    }

                    return $keywordA < $keywordB ? -1 : 1;
                }

                return $groupA < $groupB ? -1 : 1;
            });

            $data['indicators'] = $data['keywords'];
            unset($data['keywords']);
        }
        if (isset($data['clients'])) {
            foreach ($data['clients'] as $key => $client) {
                $data['clients'][$key]['has'] = [];
                $data['clients'][$key]['has']['start'] = false;
                $data['clients'][$key]['has']['end'] = false;
                foreach ($data['years'] as $year) {
                    $data['clients'][$key]['has'][$year] = false;
                }
                foreach ($data['indicators'] as $indicator) {
                    $indicatorData = $indicator['data'][$data['clients'][$key]['id']];
                    if ($indicatorData['start']['done']['value'] || $indicatorData['start']['target']['value']) {
                        $data['clients'][$key]['has']['start'] = true;
                    }
                    if ($indicatorData['end']['done']['value'] || $indicatorData['end']['target']['value']) {
                        $data['clients'][$key]['has']['end'] = true;
                    }
                    foreach ($data['years'] as $year) {
                        if (isset($indicatorData[$year])
                            && $indicatorData[$year]
                            && ($indicatorData[$year]['done']['value']
                                || $indicatorData[$year]['target']['value'])) {
                            $data['clients'][$key]['has'][$year] = true;
                        }
                    }
                }
            }
            foreach ($data['clients'] as $key => $client) {
                $data['clients'][$key]['total'] = 0;
                foreach ($data['clients'][$key]['has'] as $keyHas => $has) {
                    if ($data['clients'][$key]['has'][$keyHas]) {
                        $data['clients'][$key]['total']++;
                    }

                }
            }
        }

        if ($export) {
            $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
            $translator = $helperPluginManager->get('translate');
            $columns = $this->params()->fromQuery('columns', []);
            $aggregateBy = $this->params()->fromQuery('aggregateBy', false);

            $xlsLines = [];

            $header = [];
            $subHeader = [];
            if ($groupBy == 'keyword') {
                $header[] = '';
                $header[] = '';
                $subHeader[] = ucfirst($translator->__invoke('group_entity'));
                $subHeader[] = ucfirst($translator->__invoke('keyword_entity'));
            }
            if ($groupBy == 'territory') {
                $header[] = '';
                $subHeader[] = ucfirst($translator->__invoke('territory_entity'));
            }

            $header[] = ''; // group
            $header[] = ''; // indicator
            if (in_array('type', $columns)) {
                $header[] = '';
            }

            if (in_array('description', $columns)) {
                $header[] = '';
            }
            if (in_array('definition', $columns)) {
                $header[] = '';
            }
            if (in_array('method', $columns)) {
                $header[] = '';
            }
            if (in_array('interpretation', $columns)) {
                $header[] = '';
            }
            if (in_array('nom', $columns)) {
                $header[] = '';
            }
            if (in_array('operator', $columns)) {
                $header[] = '';
            }
            if (in_array('createdAt', $columns)) {
                $header[] = '';
            }
            if (in_array('updatedAt', $columns)) {
                $header[] = '';
            }

            $subHeader[] = ucfirst($translator->__invoke('group-indicator_entity'));
            $subHeader[] = ucfirst($translator->__invoke('indicator_entity'));
            if (in_array('type', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_type'));
            }
            if (in_array('description', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_description'));
            }
            if (in_array('definition', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_definition'));
            }
            if (in_array('method', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_method'));
            }
            if (in_array('interpretation', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_interpretation'));
            }
            if (in_array('nom', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_uom'));
            }
            if (in_array('operator', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_operator'));
            }
            if (in_array('createdAt', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_createdAt'));
            }
            if (in_array('updatedAt', $columns)) {
                $subHeader[] = ucfirst($translator->__invoke('indicator_field_updatedAt'));
            }

            //$header[]    = ucfirst($translator->__invoke('measure_period_start'));
            //$header[]    = '';
            //$header[]    = '';
            //$subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
            //$subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
            //$subHeader[] = '%';

            foreach ($data['years'] as $year) {
                $header[] = $year;
                $header[] = '';
                $header[] = '';
                $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                $subHeader[] = '%';
            }

            //$header[]    = ucfirst($translator->__invoke('measure_period_end'));
            //$header[]    = '';
            //$header[]    = '';
            //$subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
            //$subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
            //$subHeader[] = '%';

            $xlsLines[] = $header;
            $xlsLines[] = $subHeader;

            foreach ($data['groupIndicators'] as $groupIndicatorData) {
                $groupIndicatorLine = [];
                $groupIndicatorLine[] = $groupIndicatorData['name'];
                if (in_array('type', $columns)) {
                    $groupIndicatorLine[] = '';
                }

                if (in_array('description', $columns)) {
                    $groupIndicatorLine[] = '';
                }
                if (in_array('definition', $columns)) {
                    $groupIndicatorLine[] = '';
                }
                if (in_array('method', $columns)) {
                    $groupIndicatorLine[] = '';
                }
                if (in_array('interpretation', $columns)) {
                    $groupIndicatorLine[] = '';
                }
                if (in_array('nom', $columns)) {
                    $groupIndicatorLine[] = '';
                }
                if (in_array('operator', $columns)) {
                    $groupIndicatorLine[] = '';
                }
                if (in_array('createdAt', $columns)) {
                    $groupIndicatorLine[] = '';
                }
                if (in_array('updatedAt', $columns)) {
                    $groupIndicatorLine[] = '';
                }

                foreach ($data['years'] as $year) {
                    if (isset($groupIndicatorData['data'][$year])) {
                        $groupIndicatorLine[] = $groupIndicatorData['type'] == Indicator::TYPE_FIXED ? $groupIndicatorData['data'][$year]['target']['content']['text'] : $groupIndicatorData['data'][$year]['target']['value'];
                        $groupIndicatorLine[] = $groupIndicatorData['type'] == Indicator::TYPE_FIXED ? $groupIndicatorData['data'][$year]['done']['content']['text'] : $groupIndicatorData['data'][$year]['done']['value'];
                        $groupIndicatorLine[] = $groupIndicatorData['data'][$year]['ratio'] !== null && $groupIndicatorData['data'][$year]['ratio'] !== $this::NaN ? $groupIndicatorData['data'][$year]['ratio'] . '%' : '';
                    } else {
                        $groupIndicatorLine[] = '';
                        $groupIndicatorLine[] = '';
                        $groupIndicatorLine[] = '';
                    }
                }

                $xlsLines[] = $groupIndicatorLine;

                foreach ($groupIndicatorData['indicators'] as $indicatorId) {
                    $indicatorData = $data['indicators'][$indicatorId];
                    $indicatorLine = [];
                    $indicatorLine[] = ''; // group
                    if ($groupBy == 'keyword') {
                        $indicatorLine[] = $indicatorData['group'];
                        $indicatorLine[] = $indicatorData['keyword'];
                    }
                    if ($groupBy == 'territory') {
                        $indicatorLine[] = $indicatorData['territory'];
                    }

                    $indicatorLine[] = $indicatorData['name'];

                    if (in_array('type', $columns)) {
                        $indicatorLine[] = $translator->__invoke('indicator_type_' . $indicatorData['type']);
                    }
                    if (in_array('description', $columns)) {
                        $indicatorLine[] = $indicatorData['description'];
                    }
                    if (in_array('definition', $columns)) {
                        $indicatorLine[] = $indicatorData['definition'];
                    }
                    if (in_array('method', $columns)) {
                        $indicatorLine[] = $indicatorData['method'];
                    }
                    if (in_array('interpretation', $columns)) {
                        $indicatorLine[] = $indicatorData['interpretation'];
                    }
                    if (in_array('nom', $columns)) {
                        $indicatorLine[] = $indicatorData['uom'];
                    }
                    if (in_array('operator', $columns)) {
                        $indicatorLine[] = $translator->__invoke('indicator_operator_' . $indicatorData['operator']);
                    }
                    if (in_array('createdAt', $columns)) {
                        $indicatorLine[] = $indicatorData['createdAt'];
                    }
                    if (in_array('updatedAt', $columns)) {
                        $indicatorLine[] = $indicatorData['updatedAt'];
                    }

                    //$indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['start']['target']['content']['text'] : $indicatorData['data']['start']['target']['value'];
                    //$indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['start']['done']['content']['text'] : $indicatorData['data']['start']['done']['value'];
                    //$indicatorLine[] = $indicatorData['data']['start']['ratio'] !== null ? $indicatorData['data']['start']['ratio'] . '%' : '';

                    foreach ($data['years'] as $year) {
                        if (isset($indicatorData['data'][$year])) {
                            $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$year]['target']['content']['text'] : $indicatorData['data'][$year]['target']['value'];
                            $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$year]['done']['content']['text'] : $indicatorData['data'][$year]['done']['value'];
                            $indicatorLine[] = $indicatorData['data'][$year]['ratio'] !== null && $indicatorData['data'][$year]['ratio'] !== $this::NaN ? $indicatorData['data'][$year]['ratio'] . '%' : '';
                        } else {
                            $indicatorLine[] = '';
                            $indicatorLine[] = '';
                            $indicatorLine[] = '';
                        }
                    }

                    //$indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['end']['target']['content']['text'] : $indicatorData['data']['end']['target']['value'];
                    //$indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['end']['done']['content']['text'] : $indicatorData['data']['end']['done']['value'];
                    //$indicatorLine[] = $indicatorData['data']['end']['ratio'] !== null ? $indicatorData['data']['end']['ratio'] . '%' : '';

                    $xlsLines[] = $indicatorLine;
                }
            }

            ExcelExporter::download($xlsLines);
            return null;
        } else {
            return new JsonModel($data);
        }
    }

    public function getClientsAction()
    {
        $isMaster = $this->environment()->getClient()->isMaster();
        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
            $clients[] = $this->environment()->getClient();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            $clientMeasures[] = array('id' => $client->getId(), 'name' => $client->getName());
        }

        return new JsonModel(array('rows' => $clientMeasures));
    }

    public function recapWithMeasuresAction()
    {
        $filters = $this->params()->fromQuery('filters', []);

        $export = $this->params()->fromQuery('export', false);
        $measuresFilters = [];
        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $controllerIndicator = $controllerManager->get('Indicator\Controller\API\Indicator');
  
        $isMaster = $this->environment()->getClient()->isMaster();
        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        $measures = [];

        foreach ($clients as $client) {
            if ($client->isMaster()) {
                continue;
            }
            if ((isset($filters['measure']['client'])
                && $filters['measure']['client']['op'] == 'neq'
                && in_array($client->getId(), $filters['measure']['client']['val']))
                ||
                (isset($filters['measure']['client'])
                    && $filters['measure']['client']['op'] == 'eq'
                    && !in_array($client->getId(), $filters['measure']['client']['val']))
                ||
                (isset($filters['project']['_client'])
                    && $filters['project']['_client']['op'] == 'neq'
                    && in_array($client->getId(), $filters['project']['_client']['val']))
                ||
                (isset($filters['project']['_client'])
                    && $filters['project']['_client']['op'] == 'eq'
                    && !in_array($client->getId(), $filters['project']['_client']['val']))) {
                continue;
            }
            if (!isset($filters['measure'])) {
                $measuresFilters['measure'] = [];
            } else {
                foreach ($filters['measure'] as $key => $filter) {
                    if ($key != "client") {
                        $measuresFilters[$key] = $filter;
                    }
                }
            }

            // project filters
            if (isset($filters['project'])) {
                $projects = $this->getProjects( $client,$filters['project']);

                if (isset($projects)) {
                    $measuresFilters['project.id'] = [
                        'op' => 'eq',
                        'val' => [],
                    ];
                    foreach ($projects as $project) {
                        $measuresFilters['project.id']['val'][] = $project['id'];
                    }
                }
            }
            // end of project filters

            // indicator filters
            if (isset($filters['indicator'])) {
                $indicatorRequest = new Request();
                $indicatorRequest->setMethod(Request::METHOD_GET);
                $indicatorRequest->setQuery(new Parameters([
                    'col' => [
                        'id',
                        'name',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort' => 'name',
                            'order' => 'asc',
                            'filters' => $filters['indicator'],
                        ],
                    ],
                ]));
                $routeMatch = new RouteMatch([]);
                $e = new MvcEvent();
                $e->setRouteMatch($routeMatch);
                $controllerIndicator->setEvent($e);
                $controllerIndicator->setClient($client);
                $indicators = $controllerIndicator->dispatch($indicatorRequest)->getVariable('rows');

                if (isset($indicators)) {
                    $measuresFilters['indicator.id'] = [
                        'op' => 'eq',
                        'val' => [],
                    ];
                    foreach ($indicators as $indicator) {
                        $measuresFilters['indicator.id']['val'][] = $indicator['id'];
                    }
                }
            }
            // end of indicator filters
            if (isset($filters['measure']) && !isset($filters['measure']['client'])) {
                $measuresFilters += $filters['measure'];
            }

            // measures filter
            $clientMeasures = $this->getMeasures($client, $measuresFilters);
            for ($i = 0;isset($clientMeasures[$i]); $i++) {
                $clientMeasures[$i]['client'] = array('id' => $client->getId(), 'name' => $client->getName());
            }

            if (isset($clientMeasures)) {
                $measures = array_merge($measures, $clientMeasures);
            }
        }

        if ($export) {
            $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
            $translator = $helperPluginManager->get('translate');

            $xlsLines = [];
            $header = [];

            $header[] = ucfirst($translator->__invoke('indicator_entity'));
            $header[] = ucfirst($translator->__invoke('indicator_field_description'));
            $header[] = ucfirst($translator->__invoke('measure_field_period'));
            $header[] = ucfirst($translator->__invoke('measure_field_type'));
            $header[] = ucfirst($translator->__invoke('measure_field_date'));
            $header[] = ucfirst($translator->__invoke('measure_field_value'));
            $header[] = ucfirst($translator->__invoke('measure_field_project'));
            $header[] = ucfirst($translator->__invoke('measure_field_territories'));
            $header[] = ucfirst($translator->__invoke('measure_field_comment'));
            $header[] = ucfirst($translator->__invoke('measure_field_source'));

            $xlsLines[] = $header;

            $measuresGroupByIndicator = [];
            foreach ($measures as $measure) {
                $index = -1;
                $inArray = false;

                foreach ($measuresGroupByIndicator as $item) {
                    if (isset($item['name']) && $item['name'] == $measure['indicator']['name']) {
                        $inArray = true;
                    }
                    $index++;
                }

                $territories = '';
                if ($measure['territories']) {
                    foreach ($measure['territories'] as $territory) {
                        $territories .= $territory['name'] . ' ';
                    }
                }

                if (!$inArray) {
                    $measuresGroupByIndicator[] = [
                        'name' => $measure['indicator']['name'],
                        'description' => $measure['indicator']['description'],
                        'uom' => $measure['indicator']['uom'],
                        'measures' => [
                            [
                                'period' => $measure['period'],
                                'type' => $measure['type'],
                                'date' => $measure['date'],
                                'value' => $measure['fixedValue']['text'],
                                'project' => $measure['project']['name'],
                                'territories' => $territories,
                                'comment' => $measure['comment'],
                                'source' => $measure['source'],
                            ],
                        ],
                    ];
                } else {
                    $measuresGroupByIndicator[$index]['measures'][] = [
                        'period' => $measure['period'],
                        'type' => $measure['type'],
                        'date' => $measure['date'],
                        'value' => $measure['fixedValue']['text'],
                        'project' => $measure['project']['name'],
                        'territories' => $territories,
                        'comment' => $measure['comment'],
                        'source' => $measure['source'],
                    ];
                }
            }

            foreach ($measuresGroupByIndicator as $indicator) {

                $indicatorLine = [];
                $indicatorLine[] = $indicator['name'] . ($indicator['uom'] !== null ? ' (' . $indicator['uom'] . ')' : '');
                $indicatorLine[] = $indicator['description'];
                $xlsLines[] = $indicatorLine;

                foreach ($indicator['measures'] as $measure) {
                    $measureLine = [];
                    $measureLine[] = '';
                    $measureLine[] = '';
                    $measureLine[] = $translator->__invoke('measure_period_' . $measure['period']);
                    $measureLine[] = $translator->__invoke('measure_type_' . $measure['type']);
                    //$date = new \DateTime($measure['date']);
                    if (isset($measure['date'])) {
                        $date = \DateTime::createFromFormat('d/m/Y H:i', $measure['date']);
                        $measureLine[] = $date->format('d/m/Y');
                    } else {
                        $measureLine[] = '';
                    }
          
                    $measureLine[] = $measure['value'];
                    $measureLine[] = $measure['project'] !== null ? $measure['project'] : '';
                    $measureLine[] = $measure['territories'];
                    $measureLine[] = $measure['comment'] !== null ? preg_replace('/[^A-Za-z0-9\-2éèàùêô]+/', ' ', $measure['comment']) : '';
                    $measureLine[] = $measure['source'] !== null ? preg_replace('/[^A-Za-z0-9\-2éèàùêô]+/', ' ', $measure['source']) : '';
                    $xlsLines[] = $measureLine;
                }
            }

            ExcelExporter::download($xlsLines);

            return null;
        } else {
            $data['measures'] = $measures;
            return new JsonModel($data);
        }
    }

    public function recapByClientAction()
    {
        $filters = $this->params()->fromQuery('filters', []);
        $groupBy = $filters['group'];
        $export = $this->params()->fromQuery('export', false);

        $data = [
            'indicators' => [],
            'years' => [],
            'clients' => [],
        ];

        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $controllerIndicator = $controllerManager->get('Indicator\Controller\API\Indicator');
        $controllerMeasure = $controllerManager->get('Indicator\Controller\API\Measure');

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters([
            'col' => [
                'id',
            ],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort' => 'id',
                    'order' => 'asc',
                    'filters' => isset($filters['indicator']) ? $filters['indicator'] : [],
                ],
            ],
        ]));

        $routeMatch = new RouteMatch([]);
        $e = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $controllerIndicator->setEvent($e);
        $_indicators = $controllerIndicator->dispatch($request)->getVariable('rows');
        $indicators = [];
        $clients = $this->environment()->getClient()->getNetwork()->getClients();
        foreach ($_indicators as $_indicator) {

            $indicator = $this->entityManager()->getRepository('Indicator\Entity\Indicator')->find($_indicator['id']);
            $indicators[] = $indicator;

            $data['indicators'][$indicator->getId()] = [
                'id' => $indicator->getId(),
                'name' => $indicator->getName(),
                'uom' => $indicator->getUom(),
                'type' => $indicator->getType(),
                'description' => $indicator->getDescription(),
                'method' => $indicator->getMethod(),
                'definition' => $indicator->getDefinition(),
                'interpretation' => $indicator->getInterpretation(),
                'createdAt' => null != $indicator->getCreatedAt() ? $indicator->getCreatedAt()->format('d/m/Y H:i') : null,
                'updatedAt' => null != $indicator->getUpdatedAt() ? $indicator->getUpdatedAt()->format('d/m/Y H:i') : null,
                'indicator' => $indicator,
                'data' => [],
            ];

            foreach ($clients as $client) {
                $data['indicators'][$indicator->getId()]['data'][$client->getId()] = [];
            }
        }

        foreach ($clients as $client) {
            $bypass = false;
            if (isset($filters['measure']['client'])
                && $filters['measure']['client']['op'] == 'neq'
                && in_array($client->getId(), $filters['measure']['client']['val'])) {
                continue;
            }
            if (isset($filters['measure']['client'])
                && $filters['measure']['client']['op'] == 'eq'
                && !in_array($client->getId(), $filters['measure']['client']['val'])) {
                continue;
            }
            $data['clients'][$client->getId()] = array('id' => $client->getId(), 'name' => $client->getName());

            if (!isset($filters['measure'])) {
                $measuresFilters['measure'] = [];
            } else {
                foreach ($filters['measure'] as $key => $filter) {
                    if ($key != "client") {
                        $measuresFilters[$key] = $filter;
                    }
                }
            }

            $request = new Request();
            $request->setMethod(Request::METHOD_GET);
            $request->setQuery(new Parameters([
                'col' => [
                    'id',
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort' => 'id',
                        'order' => 'asc',
                        'filters' => [
                            'master' => [
                                'op' => 'eq',
                                'val' => array_keys($data['indicators']),
                            ],
                        ],
                    ],
                ],
            ]));

            $routeMatch = new RouteMatch([]);
            $e = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $e->setParam('master', true);
            $controllerIndicator->setEvent($e);

            $controllerIndicator->setClient($client);
            $_indicators = $controllerIndicator->dispatch($request)->getVariable('rows');

            $measuresFilters['indicator.id'] = [
                'op' => 'eq',
                'val' => [],
            ];

            foreach ($_indicators as $_indicator) {
                $measuresFilters['indicator.id']['val'][] = $_indicator['id'];
            }

            $request = new Request();
            $request->setMethod(Request::METHOD_GET);
            $request->setQuery(new Parameters([
                'col' => [
                    'id',
                    'indicator.id',
                    'indicator.master',
                    'period',
                    'type',
                    'date',
                    'value',
                    'territories',
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort' => 'id',
                        'order' => 'asc',
                        'filters' => $measuresFilters,
                    ],
                ],
            ]));

            $routeMatch = new RouteMatch([]);
            $e = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $controllerMeasure->setEvent($e);

            $controllerMeasure->setClient($client);
            $measures = $controllerMeasure->dispatch($request)->getVariable('rows');
            foreach ($measures as $measure) {
                //TODO not sure here
                // $indicator = $isMaster ? $measure['indicator']['master'] : $measure['indicator']['id'];
                $indicator = $measure['indicator']['master'];

                $date = \DateTime::createFromFormat('d/m/Y H:i', $measure['date']);
                if ($date) {
                    $year = $date->format('Y');
                    if (!isset($data['indicators'][$indicator]['data'][$client->getId()][$year])) {
                        $data['indicators'][$indicator]['data'][$client->getId()][$year] = [
                            'number' => 0,
                        ];
                        foreach (Measure::getTypes() as $type) {
                            $data['indicators'][$indicator]['data'][$client->getId()][$year][$type] = [
                                'value' => null,
                                'measures' => [],
                            ];
                        }
                        $data['years'][] = $year;
                    }
                    $data['indicators'][$indicator]['data'][$client->getId()][$year][$measure['type']]['measures'][] = $measure;
                    $data['indicators'][$indicator]['data'][$client->getId()][$year]['number']++;
                }
            }
        }

        if ($groupBy == 'territory') {
            $data['territories'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                foreach ($dataIndicator['data'] as $period => $dataPeriod) {
                    foreach ($dataPeriod as $year => $dataYear) {
                        foreach (Measure::getTypes() as $type) {
                            foreach ($dataYear[$type]['measures'] as $measure) {
                                if ($measure['territories']) {
                                    foreach ($measure['territories'] as $territory) {
                                        $identifier = $id . '-' . $territory['id'];

                                        if (!isset($data['territories'][$identifier])) {
                                            $data['territories'][$identifier] = $dataIndicator;
                                            $data['territories'][$identifier]['territory'] = $territory['name'];

                                            foreach ($dataIndicator['data'] as $_period => $_dataPeriod) {
                                                foreach ($_dataPeriod as $_year => $_dataYear) {
                                                    $data['territories'][$identifier]['data'][$_period][$_year]['number'] = 0;
                                                    foreach (Measure::getTypes() as $_type) {
                                                        $data['territories'][$identifier]['data'][$_period][$_year][$_type]['measures'] = [];
                                                    }
                                                }
                                            }
                                        }

                                        $data['territories'][$identifier]['data'][$period][$year][$type]['measures'][] = $measure;
                                        $data['territories'][$identifier]['data'][$period][$year]['number']++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            usort($data['territories'], function ($a, $b) {
                $groupA = $a['territory'];
                $groupB = $b['territory'];

                return $groupA < $groupB ? -1 : 1;
            });
            unset($data['territories']);
        }

        foreach ($data['indicators'] as $identifier => $dataIndicator) {
            foreach ($clients as $client) {
                //$identifier = $_indicator->getId();
                $indicator = $dataIndicator['indicator'];
                if ($indicator != null) {

                    foreach ($data['indicators'][$identifier]['data'][$client->getId()] as $year => $yearData) {
                        foreach (Measure::getTypes() as $type) {
                            $measures = $yearData[$type]['measures'];
                            $value = $indicator->getValue($measures);

                            $data['indicators'][$identifier]['data'][$client->getId()][$year][$type]['value'] = $value;
                            $data['indicators'][$identifier]['data'][$client->getId()][$year][$type]['content'] = $value;
                            if ($indicator->getType() === Indicator::TYPE_FIXED) {
                                $data['indicators'][$identifier]['data'][$client->getId()][$year][$type]['content'] = $indicator->getFixedValue($value);
                            }

                            unset($data['indicators'][$identifier]['data'][$client->getId()][$year][$type]['measures']);
                        }

                        if ($data['indicators'][$identifier]['data'][$client->getId()][$year]['target']['value'] > 0) {
                            $data['indicators'][$identifier]['data'][$client->getId()][$year]['ratio'] = round($data['indicators'][$identifier]['data'][$client->getId()][$year]['done']['value'] * 100 / $data['indicators'][$identifier]['data'][$client->getId()][$year]['target']['value']);
                        } else {
                            $data['indicators'][$identifier]['data'][$client->getId()][$year]['ratio'] = $data['indicators'][$identifier]['data'][$client->getId()][$year]['done']['value'] > 0 ? 100 : null;
                        }
                    }
                    unset($data['indicators'][$identifier]['indicator']);
                }

            }
        }

        $data['years'] = array_unique($data['years']);
        sort($data['years']);

        if ($groupBy == 'keyword') {
            $data['keywords'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                $associations = $this->entityManager()->getRepository('Keyword\Entity\Association')->findBy([
                    'primary' => $id,
                    'entity' => 'Indicator\Entity\Indicator',
                ]);
                foreach ($associations as $association) {
                    $keyword = $association->getKeyword();
                    $group = $keyword->getGroup();

                    if (
                        !$group->isArchived()
                        && !$keyword->isArchived()
                    ) {
                        $dataIndicator['group'] = $group->getName();
                        $dataIndicator['keyword'] = $keyword->getName();

                        $data['keywords'][] = $dataIndicator;
                    }
                }

                /*if (!$associations) {
            $dataIndicator['group']   = '';
            $dataIndicator['keyword'] = '';
            $data['keywords'][]       = $dataIndicator;
            }*/
            }

            usort($data['keywords'], function ($a, $b) {
                $groupA = $a['group'];
                $groupB = $b['group'];

                $keywordA = $a['keyword'];
                $keywordB = $b['keyword'];

                if ($groupA == $groupB) {
                    if ($keywordA == $keywordB) {
                        return 0;
                    }

                    return $keywordA < $keywordB ? -1 : 1;
                }

                return $groupA < $groupB ? -1 : 1;
            });

            $data['indicators'] = $data['keywords'];
            unset($data['keywords']);
        }
        foreach ($data['clients'] as $key => $client) {
            $data['clients'][$key]['has'] = [];
            foreach ($data['years'] as $year) {
                $data['clients'][$key]['has'][$year] = false;
            }
            foreach ($data['indicators'] as $indicator) {
                $indicatorData = $indicator['data'][$data['clients'][$key]['id']];
                if (isset($indicatorData['start'])) {
                    if ($indicatorData['start']['done']['value'] || $indicatorData['start']['target']['value']) {
                        $data['clients'][$key]['has']['start'] = true;
                    }
                }

                if (isset($indicatorData['start'])) {
                    if ($indicatorData['end']['done']['value'] || $indicatorData['end']['target']['value']) {
                        $data['clients'][$key]['has']['end'] = true;
                    }
                }
                foreach ($data['years'] as $year) {
                    if (isset($indicatorData[$year])
                        && $indicatorData[$year]
                        && ($indicatorData[$year]['done']['value']
                            || $indicatorData[$year]['target']['value'])) {
                        $data['clients'][$key]['has'][$year] = true;
                    }
                }
            }
        }
        foreach ($data['clients'] as $key => $client) {
            $data['clients'][$key]['total'] = 0;
            foreach ($data['clients'][$key]['has'] as $keyHas => $has) {
                if ($data['clients'][$key]['has'][$keyHas]) {
                    $data['clients'][$key]['total']++;
                }

            }
        }

        if ($export) {
            $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
            $translator = $helperPluginManager->get('translate');

            $xlsLines = [];

            $aboveHeader = [];
            $header = [];
            $subHeader = [];
            if ($groupBy == 'keyword') {
                $aboveHeader[] = '';
                $aboveHeader[] = '';
                $header[] = '';
                $header[] = '';
                $subHeader[] = ucfirst($translator->__invoke('group_entity'));
                $subHeader[] = ucfirst($translator->__invoke('keyword_entity'));
            }
            if ($groupBy == 'territory') {
                $aboveHeader[] = '';
                $header[] = '';
                $subHeader[] = ucfirst($translator->__invoke('territory_entity'));
            }
            $aboveHeader[] = '';
            $header[] = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_entity'));

            $aboveHeader[] = '';
            $header[] = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_description'));

            $aboveHeader[] = '';
            $header[] = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_definition'));

            $aboveHeader[] = '';
            $header[] = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_method'));

            $aboveHeader[] = '';
            $header[] = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_interpretation'));

            foreach ($data['clients'] as $client) {
                if ($client['total'] > 0) {
                    $aboveHeader[] = $client['name'];
                    $aboveHeader[] = '';
                    $aboveHeader[] = '';
                    $header[] = ucfirst($translator->__invoke('measure_period_start'));
                    $header[] = '';
                    $header[] = '';
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                    $subHeader[] = '%';

                    foreach ($data['years'] as $year) {
                        if ($client['has'][$year]) {
                            $aboveHeader[] = '';
                            $aboveHeader[] = '';
                            $aboveHeader[] = '';
                            $header[] = $year;
                            $header[] = '';
                            $header[] = '';
                            $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                            $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                            $subHeader[] = '%';
                        }
                    }

                    $aboveHeader[] = '';
                    $aboveHeader[] = '';
                    $aboveHeader[] = '';
                    $header[] = ucfirst($translator->__invoke('measure_period_end'));
                    $header[] = '';
                    $header[] = '';
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                    $subHeader[] = '%';
                }
            }
            $xlsLines[] = $aboveHeader;
            $xlsLines[] = $header;
            $xlsLines[] = $subHeader;

            foreach ($data['indicators'] as $indicatorData) {
                $indicatorLine = [];
                if ($groupBy == 'keyword') {
                    $indicatorLine[] = $indicatorData['group'];
                    $indicatorLine[] = $indicatorData['keyword'];
                }
                if ($groupBy == 'territory') {
                    $indicatorLine[] = $indicatorData['territory'];
                }
                $indicatorLine[] = $indicatorData['name'];
                $indicatorLine[] = $indicatorData['description'];
                $indicatorLine[] = $indicatorData['definition'];
                $indicatorLine[] = $indicatorData['method'];
                $indicatorLine[] = $indicatorData['interpretation'];
                foreach ($data['clients'] as $client) {
                    if ($client['total'] > 0) {
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['start']['target']['content']['text'] : $indicatorData['data'][$client['id']]['start']['target']['value'];
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['start']['done']['content']['text'] : $indicatorData['data'][$client['id']]['start']['done']['value'];
                        $indicatorLine[] = $indicatorData['data'][$client['id']]['start']['ratio'] !== null ? $indicatorData['data'][$client['id']]['start']['ratio'] . '%' : '';

                        foreach ($data['years'] as $year) {
                            if ($client['has'][$year]) {
                                if (isset($indicatorData['data'][$client['id']][$year])) {
                                    $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']][$year]['target']['content']['text'] : $indicatorData['data'][$client['id']][$year]['target']['value'];
                                    $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']][$year]['done']['content']['text'] : $indicatorData['data'][$client['id']][$year]['done']['value'];
                                    $indicatorLine[] = $indicatorData['data'][$client['id']][$year]['ratio'] !== null ? $indicatorData['data'][$client['id']][$year]['ratio'] . '%' : '';
                                } else {
                                    $indicatorLine[] = '';
                                    $indicatorLine[] = '';
                                    $indicatorLine[] = '';
                                }
                            }
                        }

                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['end']['target']['content']['text'] : $indicatorData['data'][$client['id']]['end']['target']['value'];
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['end']['done']['content']['text'] : $indicatorData['data'][$client['id']]['end']['done']['value'];
                        $indicatorLine[] = $indicatorData['data'][$client['id']]['end']['ratio'] !== null ? $indicatorData['data'][$client['id']]['end']['ratio'] . '%' : '';
                    }
                }
                $xlsLines[] = $indicatorLine;
            }

            ExcelExporter::download($xlsLines);
            return null;
        } else {
            return new JsonModel($data);
        }
    }

    private function getRatio($indicatorType, $done, $target)
    {
        if ($indicatorType === Indicator::TYPE_FIXED) {
            return $this::NaN;
        }
        if (!isset($target) && !isset($done)) {
            return null;
        } else if (!isset($target) || !isset($done)) {
            return $this::NaN;
        } else if ($target > 0) {
            return round($done * 100 / $target);
        } else {
            return $done > 0 ? 100 : null;
        }
    }

    private function getIndicators($indicatorsFilters) {
        
        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $controllerIndicator = $controllerManager->get('Indicator\Controller\API\Indicator');
        $request = new Request();
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters([
            'col' => [
                'id',
            ],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort' => 'id',
                    'order' => 'asc',
                    'filters' => $indicatorsFilters,
                ],
            ],
        ]));

        $routeMatch = new RouteMatch([]);
        $e = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $controllerIndicator->setEvent($e);
        return  $controllerIndicator->dispatch($request)->getVariable('rows');
    }

    private function getProjects($client, $projectFilters) {
        $isMaster = $this->environment()->getClient()->isMaster();
        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $controllerProject = $controllerManager->get('Project\Controller\API\Project');

        if ($isMaster) {
            if (!isset($filters['project'])) {
                $filters['project'] = [];
            }

            $filters['project']['networkAccessible'] = [
                'op'  => 'eq',
                'val' => 1,
            ];
        }

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters([
            'col' => [
                'id', 'code', 'name',
            ],
            'search' => [
            'type' => 'list',
            'data' => [
                    'sort' => 'id',
                    'order' => 'asc',
                    'filters' => $projectFilters,
                    ],
                ],
            ]));

        $routeMatch = new RouteMatch([]);
        $e = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $e->setParam('master', $isMaster);
        $controllerProject->setEvent($e);

        $controllerProject->setClient($client);
        return  $controllerProject->dispatch($request)->getVariable('rows');
    }

    private function getMeasures($client, $measuresFilters) {
        $controllerManager = $this->serviceLocator->get('ControllerManager');
        $controllerMeasure = $controllerManager->get('Indicator\Controller\API\Measure');
        $measureRequest = new Request();
        $measureRequest->setMethod(Request::METHOD_GET);
        $measureRequest->setQuery(new Parameters([
            'col' => [
                    'id',
                    'fixedValue',
                    'value',
                    'period',
                    'type',
                    'start',
                    'date',
                    'comment',
                    'source',
                    'indicator.id',
                    'indicator.name',
                    'indicator.description',
                    'indicator.master',
                    'indicator.uom',
                    'project.id',
                    'project.name',
                    'territories'
                ],
            'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort' => 'indicator.id',
                        'order' => 'asc',
                        'filters' => $measuresFilters,
                    ],
            ],
        ]));
        $routeMatch = new RouteMatch([]);
        $e = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $controllerMeasure->setEvent($e);
        $controllerMeasure->setClient($client);
         return $controllerMeasure->dispatch($measureRequest)->getVariable('rows');
    }

}
