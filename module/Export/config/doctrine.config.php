<?php

namespace Export;

return [
    'doctrine' => [
        'driver' => [
            'export_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Export\Entity' => 'export_entities'
                ]
            ]
        ]
    ]
];
